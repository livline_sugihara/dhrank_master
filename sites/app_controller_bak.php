<?php
class AppController extends Controller {
	var $uses = array('LargeArea','Girl','Shop','Review','Access','MShopsBusinessCategory','MNumericValue','Reviewer','BookmarkShop','BookmarkGirl');
	var $helpers = array('Html','Form','Text','ImgCommon','LinkCommon','TextCommon','BreadcrumbList','Js' => array('jquery'),'Session');
	var $components = array('Cookie','MediaInfo','Email','Auth','Session');

	var $device_file_name = '';
	var $device_path = '';
	var $parent_area = '';
	var $pcount = array();
	var $start_time;
	var $parent_reviewer = array();
	var $title_tag_common = '';
	var $meta_description_common = '';
	var $meta_keywords_common = '';
	var $h1_tag_common = '';

	// パンくず情報を保持
	public $breadcrumb = array();


	function beforefilter(){
		$this->start_time = microtime(TRUE);

		//サブドメインのサイトの場合はリダイレクト先URLはそのまま、フォルダ形式の場合はリダイレクト先URLからフォルダ部分を除外
		$base_url = '';
		if($this->webroot == '/'){
			$base_url = Router::url(null, false);
		}else{
			$base_url = substr(Router::url(null, false),mb_strlen($this->webroot)-1);
		}
		$this->set('base_url', $base_url);

		//端末振り分け
		$mediaCategory = $this->MediaInfo->getMediaCategory();
		$this->set('mediaCategory', $mediaCategory);
		//携帯もしくは携帯クローラーからのアクセス
		//ユーザーエージェントが取れない場合のエラー対応
		if($mediaCategory == 'i' || (!empty($_SERVER['HTTP_USER_AGENT']) && preg_match("/(Googlebot-Mobile|Y\!J-SRD\/1.0|Y\!J-MBS\/1.0|mobile goo)/i",$_SERVER['HTTP_USER_AGENT']))){
			if(isset($this->params['prefix'])){
				//スマートフォンサイトにアクセスしている場合
				if($this->params['prefix'] == 's'){
					$this->redirect('/i' . substr($base_url,2));
				}
			}else{
				//PCサイトにアクセスしている場合
				$this->redirect('/i' . $base_url);
			}
		}elseif($mediaCategory == 's' && ($this->name != 'Reviewers' && $this->name != 'Accounts')){
			//スマートフォンからのアクセス
			if(isset($this->params['prefix'])){
				//携帯サイトにアクセスしている場合
				if($this->params['prefix'] == 'i'){
					$this->redirect('/s' . substr($base_url,2));
				}elseif($this->params['prefix'] == 's'){
					//スマートフォンサイトにアクセスしている場合
					//PC→スマートフォンサイト移動時
					if(preg_match("/view:s/", Router::url(null, false))){
						//クッキー削除
						$this->Cookie->delete('view');
						//start---2013/3/11 障害No.3-0009修正
						//$this->redirect(str_replace('/view:s','',$base_url));
						$redirect_to = str_replace('/view:s','',$base_url);
						if($redirect_to != '/s'){
							$this->redirect($redirect_to);
						}else{
							$this->redirect('/s/');
						}
						//end---2013/3/11 障害No.3-0009修正
					}elseif($this->Cookie->read('view') == 'pc'){
						//PCサイト状態時
						$this->redirect(substr($base_url,2));
					}
				}
			}else{
				//PCサイトへアクセスしている場合
				//スマートフォン→PCサイト移動時
				if(preg_match("/view:pc/", Router::url(null, false))){
					//クッキー作成
					$this->Cookie->write('view', 'pc', false, 30 * 24 * 60 * 60);
					//start---2013/3/11 障害No.3-0009修正
					//$this->redirect(str_replace('/view:pc','',$base_url));
					$redirect_to = str_replace('/view:pc','',$base_url);
					if($redirect_to != ''){
						$this->redirect($redirect_to);
					}else{
						$this->redirect('/');
					}
					//end---2013/3/11 障害No.3-0009修正
				}elseif($this->Cookie->read('view') != 'pc'){
					//PCサイト状態時でない
					$this->redirect('/s' . $base_url);
				}
			}
		}
		//全て認証なしでアクセス可能
		$this->Auth->allow('*');

		//start---2013/3/13 障害No.1-4-0010修正
		//レスポンスヘッダにキャッシュさせない情報を付加
		$this->disableCache();
		//end---2013/3/13 障害No.1-4-0010修正

		//表示件数セット
		if(!isset($this->params['prefix'])){
			//PC版
			$this->pcount['news_list'] = APP_PCOUNT_NEWS_LIST;
			$this->pcount['top_reviews'] = APP_PCOUNT_TOP_REVIEWS;
			$this->pcount['top_rankings'] = APP_PCOUNT_TOP_RANKINGS;
			$this->pcount['rankings'] = APP_PCOUNT_RANKINGS;
			$this->pcount['users_top_reviews'] = APP_PCOUNT_USERS_TOP_REVIEWS;
			$this->pcount['users_girls'] = APP_PCOUNT_USERS_GIRLS;
			$this->pcount['reviews_list_post'] = APP_PCOUNT_REVIEWS_LIST_POST;
			$this->pcount['boards_list'] = APP_PCOUNT_BOARDS_LIST;
			$this->pcount['boards_detail'] = APP_PCOUNT_BOARDS_DETAIL;
			$this->pcount['boards_detail_res'] = APP_PCOUNT_BOARDS_DETAIL_RES;

			$this->pcount['categorylist'] = 20;

		}else{
			if($this->params['prefix'] == 'i'){
				//携帯版
				$this->pcount['news_list'] = APP_PCOUNT_NEWS_LIST_I;
				$this->pcount['top_reviews'] = APP_PCOUNT_TOP_REVIEWS_I;
				$this->pcount['rankings'] = APP_PCOUNT_RANKINGS_I;
				$this->pcount['users_top_reviews'] = APP_PCOUNT_USERS_TOP_REVIEWS_I;
				$this->pcount['users_girls'] = APP_PCOUNT_USERS_GIRLS_I;
				$this->pcount['reviews_list_post'] = APP_PCOUNT_REVIEWS_LIST_POST_I;
				$this->pcount['boards_detail'] = APP_PCOUNT_BOARDS_DETAIL_I;
				$this->pcount['boards_view_res'] = APP_PCOUNT_BOARDS_VIEW_RES_I;

				$this->pcount['categorylist'] = 20;

			}elseif($this->params['prefix'] == 's'){
				//スマートフォン
				$this->pcount['news_list'] = APP_PCOUNT_NEWS_LIST_S;
				$this->pcount['top_reviews'] = APP_PCOUNT_TOP_REVIEWS_S;
				$this->pcount['rankings'] = APP_PCOUNT_RANKINGS_S;
				$this->pcount['users_top_reviews'] = APP_PCOUNT_USERS_TOP_REVIEWS_S;
				$this->pcount['users_girls'] = APP_PCOUNT_USERS_GIRLS_S;
				$this->pcount['reviews_list_post'] = APP_PCOUNT_REVIEWS_LIST_POST_S;
				$this->pcount['boards_detail'] = APP_PCOUNT_BOARDS_DETAIL_S;
				$this->pcount['boards_view_res'] = APP_PCOUNT_BOARDS_VIEW_RES_S;

				$this->pcount['categorylist'] = 20;
			}
		}
		$this->set('pcount', $this->pcount);

		if(isset($this->params['prefix'])){
			if($this->params['prefix'] == 'i'){
				$this->device_file_name = 'i_';
				$this->device_path = '/i';
			}elseif($this->params['prefix'] == 's'){
				$this->device_file_name = 's_';
				$this->device_path = '/s';
			}
		}

		// 入力データの文字コード変換
		if(isset($this->params['prefix'])){
			if($this->params['prefix'] == 'i'){
				if(!empty($this->data)){
					array_walk_recursive($this->data, 'AppController::encode_mobile');
				}
			}
		}

		// 地域を検索
		$this->parent_area = $this->getLargeArea();
		$this->set('parent_area', $this->parent_area);

		// アクセス加算
		$access = $this->Access->find('first', array('conditions' => array('Access.large_area_id' => $this->parent_area['LargeArea']['id'], 'Access.date' => date('Y-m-d'))));
		if(!empty($access)){
			$access['Access']['count'] += 1;
			$this->Access->save($access);
		}else{
			$access['Access']['large_area_id'] = $this->parent_area['LargeArea']['id'];
			$access['Access']['date'] = date('Y-m-d');
			$access['Access']['count'] = 1;
			$access['Access']['rand'] = rand(35,96);
/*			$access['Access']['rand'] = rand(352,960); */
			$this->Access->save($access);
		}

		//新着情報取得
//		$this->getNews();

		// カテゴリ
		$this->MShopsBusinessCategory->bindModel(array('belongsTo' => array('Shop' => array(
			'className'=>'Shop',
			'type'=>'inner',
			'fields' => array('ifnull(count(Shop.id),0) as cnt'),
			'conditions' => array(
				'Shop.shops_business_category_id = MShopsBusinessCategory.id'
			),
			'foreignKey' => false,))),false);
		$this->MShopsBusinessCategory->bindModel(array('belongsTo' => array('User' => array(
			'className'=>'User',
			'type'=>'inner',
			'conditions' => array(
				'Shop.user_id = User.id',
				'User.large_area_id' => $this->parent_area['LargeArea']['id'],
			),
			'foreignKey' => false,))),false);
		$m_shop_business_category = $this->MShopsBusinessCategory->find('all', array(
			'order' => 'MShopsBusinessCategory.id',
			'group' => array('MShopsBusinessCategory.id HAVING cnt > 0'),
		));
		$this->set('categories', $m_shop_business_category); 

		// カテゴリの値-名前リスト
		$arrCategoryValueList = array();
		foreach($m_shop_business_category AS $key => $category)
		{
			$arrCategoryValueList[$category['MShopsBusinessCategory']['id']] = $category['MShopsBusinessCategory']['name'];
		}
		$this->set('categories_value_list', $arrCategoryValueList); 

		$this->MShopsBusinessCategory->unbindModel(array('hasOne' => array('Shop')));
		$this->MShopsBusinessCategory->unbindModel(array('hasOne' => array('User')));

		// 店舗抽出（ランダム）
		$this->Shop->unbindModel(array('hasOne' => array('Review')));
		$recommend_shop = $this->Shop->find('all', array(
			'conditions' => array($this->getCurrentAreaCondition(), 'User.is_show_recommend_shop' => 1),
			'order' =>'rand()',
		));
		$this->set('recommend_shop', $recommend_shop);

/*
		if(!isset($this->params['prefix'])){
			//アクセスランキング
			$m_shops_business_categories = $this->MShopsBusinessCategory->find('all');
			$ranking_sidebar = array();
			for($i = 0; $i < count($m_shops_business_categories); $i++){
				$business_category_name = Set::extract ('/MShopsBusinessCategory[id=' . (string)($m_shops_business_categories[$i]['MShopsBusinessCategory']['id']) . ']/name', $m_shops_business_categories);
				array_push($ranking_sidebar,$this->get_ranking($i+1, $business_category_name[0]));
			}
			$this->set('ranking_sidebar', $ranking_sidebar);
		}
*/

		//start---2013/3/5 障害No.3-0002修正
		//大エリアフッターリンク取得
		$this->set('large_area_list', $this->LargeArea->find('all', array(
				'conditions' => array('LargeArea.is_display_footer' => 1),
				//'order' => 'LargeArea.show_order ASC',
				'order' => 'LargeArea.show_order ASC, LargeArea.created ASC',
		)));
		//end---2013/3/5 障害No.3-0002修正

		//業種一覧取得
//		$this->set('m_shops_business_categories', $this->MShopsBusinessCategory->find('all'));

/*
		//口コミ点数
		$numeric_values = $this->MNumericValue->find('all');
		$this->set('m_score',Set::Combine(Set::extract('/MNumericValue[id>=1][id<=5]', $numeric_values),'{n}.MNumericValue.id', null));
*/

		// イチオシのお店
		$this->Shop->unbindModel(array('hasOne' => array('Review')));
		$topic_shop = $this->Shop->find('all', array(
			'conditions' => array(
				$this->getCurrentAreaCondition(),
				'User.is_show_pickup_shop' => 1),
			'order' =>'rand()',
			'limit' => 10
		));
		$this->set('topic_shop', $topic_shop);

		// パンくずに追加
		$this->addBreadCrumbs("全国" , APP_ALLAREA_URL);
		$this->addBreadCrumbs($this->parent_area['LargeArea']['name'], $this->parent_area['LargeArea']['url'] . 'top');

		//AUTH関連設定
		$this->Auth->authError = "ログインをお願いします。";
		$this->Auth->loginError = "ログインに失敗しました。";
		$this->Auth->userModel = 'Reviewer';
		$this->Auth->autoRedirect = false;
		$this->Auth->loginAction = array('controller' => 'accounts','action' => 'login',);
		$this->Auth->userScope = array('Reviewer.large_area_id' => $this->parent_area['LargeArea']['id']);

		//レビュアーデータ
		$this->parent_reviewer = $this->Reviewer->findbyId($this->Auth->user('id'));
		$this->set('parent_reviewer',$this->parent_reviewer);


		//ページングURL設定
		$user_id = '';
		if(isset($this->params['user_id'])){
			$user_id = $this->params['user_id'];
		}
		$girl_id = '';
		if(isset($this->params['girl_id'])){
			$girl_id = $this->params['girl_id'];
		}
		$board_id = '';
		if(isset($this->params['board_id'])){
			$board_id = $this->params['board_id'];
		}
		//start---2013/3/11 障害No.1-4-0002修正
		$business_category_id = '';
		if(isset($this->params['business_category_id'])){
			$business_category_id = $this->params['business_category_id'];
		}
		$reviewer_id = '';
		if(isset($this->params['reviewer_id'])){
			$reviewer_id = $this->params['reviewer_id'];
		}
		//end---2013/3/11 障害No.1-4-0002修正
		$phpsessid = '';
		if(isset($this->params['url']['PHPSESSID'])){
			$phpsessid = $this->params['url']['PHPSESSID'];
		}
		$this->set('paginate_url_param',array(
				'user_id' => $user_id,
				'board_id' => $board_id,
				'girl_id' => $girl_id,
				//start---2013/3/11 障害No.1-4-0002修正
				'business_category_id' => $business_category_id,
				'reviewer_id' => $reviewer_id,
				//end---2013/3/11 障害No.1-4-0002修正
				'?' => array('PHPSESSID' => $phpsessid),
		));

		//QRコードを読み込んだときに表示される内容
		$qr_code_data = $this->parent_area['LargeArea']['url'];
		//QRコードのサイズ
		$qr_code_size = "2";
		//QRコードの画像URL
		$qr_code_url  = $this->parent_area['LargeArea']['url'] . "webroot/qr_code/php/qr_img.php?t=J&d=" . $qr_code_data ."&s=".$qr_code_size;
		$this->set('qr_code_url',$qr_code_url);

		//タイトルタグ・meta・H1設定
		$this->title_tag_common = 'ランキングと口コミで探せるデリヘル情報サイト[' . $this->parent_area['LargeArea']['name'] . 'デリヘル人気口コミランキング] ';
		$this->meta_description_common = 'ランキングと口コミで探せる' . $this->parent_area['LargeArea']['name'] . 'のデリヘル情報サイト[' . $this->parent_area['LargeArea']['name'] . 'デリヘル人気口コミランキング] ';
		$this->meta_keywords_common = $this->parent_area['LargeArea']['name'] . ',ランキング,人気,口コミ,デリヘル';
		$this->h1_tag_common = 'ランキングと口コミで探せるデリヘル情報サイト[' . $this->parent_area['LargeArea']['name'] . 'デリヘル人気口コミランキング] ';
	}

	//文字コード変換←ココ
	function encode_mobile(&$item, $key){
		//echo "$key holds $item\n";
		$item = mb_convert_encoding($item, 'UTF-8', 'SJIS');
		//echo "$key =&gt; $item\n";
	}
	function beforeRender(){
		if(isset($this->params['prefix'])){
			if($this->params['prefix'] == 'i'){
				$this->layout = 'i_'.$this->layout;
			}elseif($this->params['prefix'] == 's'){
				$this->layout = 's_'.$this->layout;
			}
		}

		// パンくず
		$this->set('breadcrumb', $this->breadcrumb);
	}

	function afterFilter(){
		parent::afterFilter();
		if(isset($this->params['prefix'])){
			//携帯の場合
			if($this->params['prefix'] == 'i'){
				// 全角文字の変換
				//$this->output = mb_convert_kana($this->output, "rak", 'UTF-8');
				// 出力文字コードの変換
				$this->output = mb_convert_encoding($this->output, "SJIS", 'UTF-8');
			}elseif($this->params['prefix'] == 's'){
			}
		}
		echo (Configure::read('debug'))?(microtime(TRUE) - $this->start_time)*1000:'';
	}

	function __construct(){
		// /i/がURLの中にあった場合、MOBILE変数を設定。このフラグを各クラスで判別
		if(ereg("^/i/",$_SERVER['REQUEST_URI'])){ // もし、URLが/<em>servicename</em>/m/だったら、"^/<em>servicename</em>/m/"
			define('i',1);
		}
		parent::__construct();
	}

	//redirect メソッド書き換え
	function redirect($url,$status = null){
		if(defined('i')){
			$url = $url."?".session_name()."=".session_id(); //転送先URLにセッションIDを自動付加
		}
		parent::redirect($url,$status);
	}

/*
	//新着情報取得
	function getNews(){
		$minus_14_day = date('Y-m-d', strtotime('-14 day'));

		//新規店舗
		$new_shops = $this->Shop->find('all', array(
				'fields' => array('User.id','Shop.name', 'Shop.created', '"Shop" as id', 'LargeArea.url'),
				'conditions' => array($this->getCurrentAreaCondition(),'DATE_FORMAT(Shop.created,"%Y-%m-%d") >= ' => $minus_14_day),
				'group' => 'Shop.id',
				'order' => 'Shop.created desc'));
		foreach($new_shops as $key => $value){
			$new_shops[$key]['0']['news_modified'] = $value['Shop']['created'];
		}

		//新人の女の子
		$new_girls = $this->Girl->find('all', array(
				'fields' => array('User.id','Shop.name','Girl.id', 'Girl.name', 'Girl.created', '"Girl" as id', 'LargeArea.url'),
				'conditions' => array($this->getCurrentAreaCondition(),'DATE_FORMAT(Girl.created,"%Y-%m-%d") >= ' => $minus_14_day),
				'group' => 'Girl.id',
				'order' => 'Girl.created desc'));
		foreach($new_girls as $key => $value){
			$new_girls[$key]['0']['news_modified'] = $value['Girl']['created'];
		}

		//最新口コミ
		$new_reviews = $this->Review->find('all', array(
				'fields' => array('User.id','Shop.name','Girl.id', 'Girl.name', 'Review.created', '"Review" as id', 'LargeArea.url', 'Review.id'),
				'conditions' => array(
						$this->getCurrentAreaCondition(),
						'DATE_FORMAT(Review.created,"%Y-%m-%d") >= ' => $minus_14_day,
				),
				'group' => 'Review.id',
				'order' => 'Review.created desc'));
		foreach($new_reviews as $key => $value){
			$new_reviews[$key]['0']['news_modified'] = $value['Review']['created'];
		}


		//全てのデータをマージ
		$news_list = array_merge($new_shops, $new_girls, $new_reviews);
		//日付降順でソート
		$news_list = Set::sort($news_list, '{n}.0.news_modified', 'desc');
		array_splice($news_list, $this->pcount['news_list']);
		$this->set('news_list', $news_list);
	}
*/
	//conditions当該地域
	function getCurrentAreaCondition(){
		return array('User.large_area_id' => $this->parent_area['LargeArea']['id'], 'Shop.is_created' => 1);
	}

	//conditions当該地域(レビュアー用)
	function getCurrentAreaConditionForReviewer(){
		return array('Reviewer.large_area_id' => $this->parent_area['LargeArea']['id']);
	}

	//ランキング順
	function setOrderByRanking($model){
		$model->bindModel(array('hasOne' => array('Review' => array(
				'className'=>'Review',
				'conditions' => array('User.id = Review.user_id'),
				'order' => array('(ifnull(SUM(Review.score_shop_tel),0)+ifnull(SUM(Review.score_shop_time),0)+ifnull(SUM(Review.score_shop_cost),0)+' .
						'ifnull(SUM(Review.score_girl_first_impression),0)+ifnull(SUM(Review.score_girl_word),0)+ifnull(SUM(Review.score_girl_service),0)+' .
						//start---2013/4/30 障害No.4-0003修正
						//'ifnull(SUM(Review.score_girl_sensitivity),0)+ifnull(SUM(Review.score_girl_style),0)+ifnull(SUM(Review.score_girl_voice),0))/count(Review.id) DESC'),
						'ifnull(SUM(Review.score_girl_sensitivity),0)+ifnull(SUM(Review.score_girl_style),0)+ifnull(SUM(Review.score_girl_voice),0))/count(Review.id) DESC',
						'User.id ASC',
						),
						//end---2013/4/30 障害No.4-0003修正
				'foreignKey' => false))),false);
	}

	//ランキング取得
	private function get_ranking($business_category_id, $business_category_name){
		$this->setOrderByRanking($this->Shop);
		$ranking = $this->Shop->find('all', array(
				'fields' => array('User.id','Shop.name', 'Shop.user_id', 'Shop.banner', 'LargeArea.url'),
				'conditions' => array(
						$this->getCurrentAreaCondition(),
						'Shop.shops_business_category_id' => $business_category_id,
				),
				'group' => 'User.id',
				'limit' => $this->pcount['top_rankings']));
		$ranking['business_category_name'] = $business_category_name;
		$ranking['business_category_id'] = $business_category_id;
		return $ranking;
	}

// -------------------------------------------------------------------
// 追加機能分
// -------------------------------------------------------------------
	function addBreadCrumbs($name, $link) {
		$this->breadcrumb[] = array("name" => $name , "link" => $link);
	}

	function clearBreadCrumbs() {
		$this->breadcrumb = array();
	}

	function getLargeArea() {

		$url = explode(':', env('HTTP_HOST'));
		$url = $url[0];

		// 地域を検索
		return $this->LargeArea->find('first', array('conditions' => array('LargeArea.url LIKE' => '%' . $url . '%')));
	}

	function get_device_path($non_s_plefix = null){
		if(!empty($this->params['prefix'])){
			if($this->params['prefix'] == 's' && $non_s_plefix == true){
				return '';
			}else{
				return '/' . $this->params['prefix'];
			}
		}
	}

	// ----- パーツデータ取得
	// 新着口コミ
	function get_newest_review() {
		//口コミ情報取得
		$new_review = $this->Review->find('all', array(
				'fields' => array('LargeArea.url','User.id','Girl.id','Girl.name','Girl.age','Review.id','Review.comment','Shop.name','Review.created','Review.post_name','Review.reviewer_id','Girl.image_1_s','Girl.image_1_m','Girl.image_1_l',
						'Review.score_girl_first_impression','Review.score_girl_word','Review.score_girl_service','Review.score_girl_sensitivity','Review.score_girl_style','Review.score_girl_voice',
						'(Review.score_girl_first_impression + Review.score_girl_word + Review.score_girl_service + Review.score_girl_sensitivity + Review.score_girl_style + Review.score_girl_voice) / 6  as girl_avg'),
				'conditions' => array($this->getCurrentAreaCondition(),),
				'group' => array('Review.id'),
				'order' => array('Review.created DESC')));
		$this->set('new_review', $new_review);
	}
}

// 店舗ページ用コントローラ
class UserController extends AppController {
	var $parent_shop = null;

	function beforefilter(){
		parent::beforefilter();

		//存在しない、店舗登録を行なっていないショップの場合はエラー画面へ
		$shop_present = $this->Shop->find('count',array('conditions' => array('Shop.user_id' => $this->params['user_id'], 'Shop.is_created' => 1)));
		
		if($shop_present == 0){
			$this->cakeError('error404');
		}

		//ショップ情報
		$this->parent_shop = $this->Shop->find('first',array('conditions' => array('Shop.user_id' => $this->params['user_id'])));
		$this->set('parent_shop',$this->parent_shop);

		// ショップに在籍する女の子の数をカウント
		$cntGirl = $this->Girl->find('count', array(
				'conditions' => array('Girl.user_id' => $this->params['user_id']),
		));
		$this->set('count_girls', $cntGirl);

		//口コミデータ
		$review_data = $this->Review->find('first', array(
				'fields' => array(
						'ifnull(SUM(Review.score_shop_tel),0) / count(Review.id) as score_shop_tel',
						'ifnull(SUM(Review.score_shop_time),0) / count(Review.id) as score_shop_time',
						'ifnull(SUM(Review.score_shop_cost),0) / count(Review.id) as score_shop_cost',
						'ifnull(SUM(Review.score_girl_first_impression),0) / count(Review.id) as score_girl_first_impression',
						'ifnull(SUM(Review.score_girl_word),0) / count(Review.id) as score_girl_word',
						'ifnull(SUM(Review.score_girl_service),0) / count(Review.id) as score_girl_service',
						'ifnull(SUM(Review.score_girl_sensitivity),0) / count(Review.id) as score_girl_sensitivity',
						'ifnull(SUM(Review.score_girl_style),0) / count(Review.id) as score_girl_style',
						'ifnull(SUM(Review.score_girl_voice),0) / count(Review.id) as score_girl_voice',
						'count(Review.id) as count'
				),
				'conditions' => array(
						$this->getCurrentAreaCondition(),
						'User.id' => $this->params['user_id'],
				),
				'group' => array('User.id'),
		));
		if(empty($review_data[0]['count'])){
			$review_data[0]['count'] = 0;
			$review_data[0]['total'] = 0;
			$review_data[0]['score_shop_tel'] = 0;
			$review_data[0]['score_shop_time'] = 0;
			$review_data[0]['score_shop_cost'] = 0;
			$review_data[0]['score_girl_first_impression'] = 0;
			$review_data[0]['score_girl_word'] = 0;
			$review_data[0]['score_girl_service'] = 0;
			$review_data[0]['score_girl_sensitivity'] = 0;
			$review_data[0]['score_girl_style'] = 0;
			$review_data[0]['score_girl_voice'] = 0;
		}else{
			$review_data[0]['total'] = ($review_data[0]['score_shop_tel']+$review_data[0]['score_shop_time']+$review_data[0]['score_shop_cost']+
					$review_data[0]['score_girl_first_impression']+$review_data[0]['score_girl_word']+$review_data[0]['score_girl_service']+
					$review_data[0]['score_girl_sensitivity']+$review_data[0]['score_girl_style']+$review_data[0]['score_girl_voice']) / 9;
		}
		$this->set('review_data', $review_data);

		//口コミデータ（点数別）
		$review_data_marks = $this->Review->find('all', array(
				'fields' => array(
						'TRUNCATE((Review.score_shop_tel + Review.score_shop_time + Review.score_shop_cost + ' .
						'Review.score_girl_first_impression + Review.score_girl_word + Review.score_girl_service + ' .
						'Review.score_girl_sensitivity + Review.score_girl_style + Review.score_girl_voice) / 9 + 0.5, 0) as avg',
						'count(Review.id) as count'
				),
				'conditions' => array(
						$this->getCurrentAreaCondition(),
						'User.id' => $this->params['user_id'],
				),
				'group' => array(
						'TRUNCATE((Review.score_shop_tel + Review.score_shop_time + Review.score_shop_cost + ' .
						'Review.score_girl_first_impression + Review.score_girl_word + Review.score_girl_service + ' .
						'Review.score_girl_sensitivity + Review.score_girl_style + Review.score_girl_voice) / 9 + 0.5, 0)',),
		));
		$count = 0;
		foreach($review_data_marks as $key => $record){
			if($record[0]['count'] > $count){
				$count = $record[0]['count'];
			}
		}
		foreach($review_data_marks as $key => $record){
			$review_data_marks[$key][0]['max'] = $count;
		}
		$this->set('review_data_marks', $review_data_marks);

		//QRコードを読み込んだときに表示される内容
		$qr_code_data = $this->parent_shop['LargeArea']['url'] . $this->parent_shop['Shop']['user_id'];
		//QRコードのサイズ
		$qr_code_size = "2";
		//QRコードの画像URL
		$qr_code_url  = $this->parent_area['LargeArea']['url'] . "webroot/qr_code/php/qr_img.php?t=J&d=" . $qr_code_data ."&s=".$qr_code_size;
		$this->set('qr_code_url',$qr_code_url);
		//mailtoのURL
		$mailto = 'mailto:?body=' . $this->parent_area['LargeArea']['url'] . $this->parent_shop['User']['id'];
		$this->set('mailto', $mailto);

		//ブックマークデータ(店舗)
		$this->set('bookmark_shop', $this->BookmarkShop->find('count', array('conditions' => array('BookmarkShop.reviewer_id' => $this->Auth->user('id'), 'BookmarkShop.user_id' => $this->params['user_id']))));

	}
	function beforeRender(){
		parent::beforeRender();

	}

}

//レビュアー部分の共通コントローラー
class ReviewersController extends AppController {

	function beforeFilter(){
		parent::beforeFilter();
		//start---2013/3/13 障害No.1-4-0010修正
		//レスポンスヘッダにキャッシュさせない情報を付加
		//$this->disableCache();
		//end---2013/3/13 障害No.1-4-0010修正
	}

}