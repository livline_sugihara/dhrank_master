<?php

ini_set('display_errors', 1);

class AppController extends Controller {
	var $uses = array('LargeArea','Girl','Shop','Review','Access','SummaryData','MShopsBusinessCategory','MNumericValue','Reviewer','BookmarkShop','BookmarkGirl','BigBanner');
	var $helpers = array('Html','Form','Text','ImgCommon','LinkCommon','TextCommon','BreadcrumbList','Js' => array('jquery'),'Session');
	var $components = array('Cookie','MediaInfo','Email','Auth','Session');


	var $device_file_name = '';
	var $device_path = '';
	var $parent_area = '';
	var $pcount = array();
	var $start_time;
	var $parent_reviewer = array();
	var $title_tag_common = '';
	var $meta_description_common = '';
	var $meta_keywords_common = '';
	var $h1_tag_common = '';

	// パンくず情報を保持
	public $breadcrumb = array();


	function beforefilter(){
		$this->start_time = microtime(TRUE);

		// サイトの初期化
		$this->init_site();

		// modelの初期化
		$this->init_model();

		// アクセス加算
		$this->saveAccessData();

		// ----- 画面生成用データ取得
		// FIXME 不要な画面は実行されないように修正
		// 新着情報
		$this->getNews();

		// ビッグバナー情報
		$this->getBigbanner();

		// カテゴリ情報を取得
		$this->get_category_data();

		// 店舗抽出（ランダム）
		$this->get_recommend_shop();

		// イチオシのお店
		$this->get_topic_shop();

		// 新着レビュアーを取得
		$this->get_new_reviewer();

		// ランキングデータ(店舗総合)
		$this->get_ranking_data_shop($this->params['controller'] == 'ranking' ? 10 : 3);

		// ランキングデータ(女の子アクセス)
		$this->get_ranking_data_girl($this->params['controller'] == 'ranking' ? 10 : 3);

		// ランキングデータ(レビュアー)
		$this->get_ranking_data_reviewer($this->params['controller'] == 'ranking' ? 10 : 3);
		// ----- /画面生成用データ取得


		// ログイン者データ取得
		// FIXME ログインしている場合のみに限定する
		$this->get_login_reviewer_data();

		// Pagenateの設定
		$this->get_pagenate_settings();

	}

	//文字コード変換←ココ
	function encode_mobile(&$item, $key){
		//echo "$key holds $item\n";
		$item = mb_convert_encoding($item, 'UTF-8', 'SJIS');
		//echo "$key =&gt; $item\n";
	}
	function beforeRender(){

		if($this->layout !== FALSE) {
			if(isset($this->params['prefix'])){
				if($this->params['prefix'] == 'i'){
					$this->layout = 'i_'.$this->layout;
				}elseif($this->params['prefix'] == 's'){
					$this->layout = 's_'.$this->layout;
				}
			}
		}

		// パンくず
		$this->set('breadcrumb', $this->breadcrumb);
	}

	function afterFilter(){
		parent::afterFilter();
		if(isset($this->params['prefix'])){
			//携帯の場合
			if($this->params['prefix'] == 'i'){
				// 全角文字の変換
				//$this->output = mb_convert_kana($this->output, "rak", 'UTF-8');
				// 出力文字コードの変換
				$this->output = mb_convert_encoding($this->output, "SJIS", 'UTF-8');
			}elseif($this->params['prefix'] == 's'){
			}
		}
		echo (Configure::read('debug'))?(microtime(TRUE) - $this->start_time)*1000:'';
	}

	function __construct(){
		// /i/がURLの中にあった場合、MOBILE変数を設定。このフラグを各クラスで判別
		if(ereg("^/i/",$_SERVER['REQUEST_URI'])){ // もし、URLが/<em>servicename</em>/m/だったら、"^/<em>servicename</em>/m/"
			define('i',1);
		}
		parent::__construct();
	}

	// redirect メソッド書き換え
	function redirect($url,$status = null){
		if(defined('i')){
			$url = $url."?".session_name()."=".session_id(); //転送先URLにセッションIDを自動付加
		}
		parent::redirect($url,$status);
	}

	/** サイト情報初期化 */
	function init_site() {

		//サブドメインのサイトの場合はリダイレクト先URLはそのまま、フォルダ形式の場合はリダイレクト先URLからフォルダ部分を除外
		$base_url = '';
		if($this->webroot == '/'){
			$base_url = Router::url(null, false);
		}else{
			$base_url = substr(Router::url(null, false),mb_strlen($this->webroot)-1);
		}
		$this->set('base_url', $base_url);

		//端末振り分け
		$mediaCategory = $this->MediaInfo->getMediaCategory();
		$this->set('mediaCategory', $mediaCategory);
		//携帯もしくは携帯クローラーからのアクセス
		//ユーザーエージェントが取れない場合のエラー対応
		if($mediaCategory == 'i' || (!empty($_SERVER['HTTP_USER_AGENT']) && preg_match("/(Googlebot-Mobile|Y\!J-SRD\/1.0|Y\!J-MBS\/1.0|mobile goo)/i",$_SERVER['HTTP_USER_AGENT']))){
			if(isset($this->params['prefix'])){
				//スマートフォンサイトにアクセスしている場合
				if($this->params['prefix'] == 's'){
					$this->redirect('/i' . substr($base_url,2));
				}
			}else{
				//PCサイトにアクセスしている場合
				$this->redirect('/i' . $base_url);
			}
		}elseif($mediaCategory == 's' && ($this->name != 'Reviewers' && $this->name != 'Accounts')){
			//スマートフォンからのアクセス
			if(isset($this->params['prefix'])){
				//携帯サイトにアクセスしている場合
				if($this->params['prefix'] == 'i'){
					$this->redirect('/s' . substr($base_url,2));
				}elseif($this->params['prefix'] == 's'){
					//スマートフォンサイトにアクセスしている場合
					//PC→スマートフォンサイト移動時
					if(preg_match("/view:s/", Router::url(null, false))){
						//クッキー削除
						$this->Cookie->delete('view');
						//start---2013/3/11 障害No.3-0009修正
						//$this->redirect(str_replace('/view:s','',$base_url));
						$redirect_to = str_replace('/view:s','',$base_url);
						if($redirect_to != '/s'){
							$this->redirect($redirect_to);
						}else{
							$this->redirect('/s/');
						}
						//end---2013/3/11 障害No.3-0009修正
					}elseif($this->Cookie->read('view') == 'pc'){
						//PCサイト状態時
						$this->redirect(substr($base_url,2));
					}
				}
			}else{
				//PCサイトへアクセスしている場合
				//スマートフォン→PCサイト移動時
				if(preg_match("/view:pc/", Router::url(null, false))){
					//クッキー作成
					$this->Cookie->write('view', 'pc', false, 30 * 24 * 60 * 60);
					//start---2013/3/11 障害No.3-0009修正
					//$this->redirect(str_replace('/view:pc','',$base_url));
					$redirect_to = str_replace('/view:pc','',$base_url);
					if($redirect_to != ''){
						$this->redirect($redirect_to);
					}else{
						$this->redirect('/');
					}
					//end---2013/3/11 障害No.3-0009修正
				}elseif($this->Cookie->read('view') != 'pc'){
					//PCサイト状態時でない
					$this->redirect('/s' . $base_url);
				}
			}
		}
		//全て認証なしでアクセス可能
		$this->Auth->allow('*');
		//start---2013/3/13 障害No.1-4-0010修正
		//レスポンスヘッダにキャッシュさせない情報を付加
		$this->disableCache();
		//end---2013/3/13 障害No.1-4-0010修正

		if(isset($this->params['prefix'])){
			if($this->params['prefix'] == 'i'){
				$this->device_file_name = 'i_';
				$this->device_path = '/i';
			}elseif($this->params['prefix'] == 's'){
				$this->device_file_name = 's_';
				$this->device_path = '/s';
			}
		}

		// 入力データの文字コード変換
		if(isset($this->params['prefix'])){
			if($this->params['prefix'] == 'i'){
				if(!empty($this->data)){
					array_walk_recursive($this->data, 'AppController::encode_mobile');
				}
			}
		}

		// 地域を検索
		$this->parent_area = $this->getLargeArea();
		$this->set('parent_area', $this->parent_area);

		// サイトパラメータ　初期化
		$this->init_settings();

		//QRコードを読み込んだときに表示される内容
		$qr_code_data = $this->parent_area['LargeArea']['url'];
		//QRコードのサイズ
		$qr_code_size = "2";
		//QRコードの画像URL
		$qr_code_url  = $this->parent_area['LargeArea']['url'] . "webroot/qr_code/php/qr_img.php?t=J&d=" . $qr_code_data ."&s=".$qr_code_size;
		$this->set('qr_code_url',$qr_code_url);

		//タイトルタグ・meta・H1設定
		$this->title_tag_common = 'ランキングと口コミで探せるデリヘル情報サイト[' . $this->parent_area['LargeArea']['name'] . 'デリヘル人気口コミランキング] ';
		$this->meta_description_common = 'ランキングと口コミで探せる' . $this->parent_area['LargeArea']['name'] . 'のデリヘル情報サイト[' . $this->parent_area['LargeArea']['name'] . 'デリヘル人気口コミランキング] ';
		$this->meta_keywords_common = $this->parent_area['LargeArea']['name'] . ',ランキング,人気,口コミ,デリヘル';
		$this->h1_tag_common = 'ランキングと口コミで探せるデリヘル情報サイト[' . $this->parent_area['LargeArea']['name'] . 'デリヘル人気口コミランキング] ';
	}

	/** 設定情報の初期化 */
	function init_settings() {

		//表示件数セット
		if(!isset($this->params['prefix'])){
			//PC版
			$this->pcount['news_list'] = APP_PCOUNT_NEWS_LIST;
			$this->pcount['top_reviews'] = APP_PCOUNT_TOP_REVIEWS;
			$this->pcount['top_rankings'] = APP_PCOUNT_TOP_RANKINGS;
			$this->pcount['rankings'] = APP_PCOUNT_RANKINGS;
			$this->pcount['users_top_reviews'] = APP_PCOUNT_USERS_TOP_REVIEWS;
			$this->pcount['users_girls'] = APP_PCOUNT_USERS_GIRLS;
			$this->pcount['reviews_list_post'] = APP_PCOUNT_REVIEWS_LIST_POST;
			$this->pcount['boards_list'] = APP_PCOUNT_BOARDS_LIST;
			$this->pcount['boards_detail'] = APP_PCOUNT_BOARDS_DETAIL;
			$this->pcount['boards_detail_res'] = APP_PCOUNT_BOARDS_DETAIL_RES;

			$this->pcount['categorylist'] = 20;

		}else{
			if($this->params['prefix'] == 'i'){
				//携帯版
				$this->pcount['news_list'] = APP_PCOUNT_NEWS_LIST_I;
				$this->pcount['top_reviews'] = APP_PCOUNT_TOP_REVIEWS_I;
				$this->pcount['rankings'] = APP_PCOUNT_RANKINGS_I;
				$this->pcount['users_top_reviews'] = APP_PCOUNT_USERS_TOP_REVIEWS_I;
				$this->pcount['users_girls'] = APP_PCOUNT_USERS_GIRLS_I;
				$this->pcount['reviews_list_post'] = APP_PCOUNT_REVIEWS_LIST_POST_I;
				$this->pcount['boards_detail'] = APP_PCOUNT_BOARDS_DETAIL_I;
				$this->pcount['boards_view_res'] = APP_PCOUNT_BOARDS_VIEW_RES_I;

				$this->pcount['categorylist'] = 20;

			}elseif($this->params['prefix'] == 's'){
				//スマートフォン
				$this->pcount['news_list'] = APP_PCOUNT_NEWS_LIST_S;
				$this->pcount['top_reviews'] = APP_PCOUNT_TOP_REVIEWS_S;
				$this->pcount['rankings'] = APP_PCOUNT_RANKINGS_S;
				$this->pcount['users_top_reviews'] = APP_PCOUNT_USERS_TOP_REVIEWS_S;
				$this->pcount['users_girls'] = APP_PCOUNT_USERS_GIRLS_S;
				$this->pcount['reviews_list_post'] = APP_PCOUNT_REVIEWS_LIST_POST_S;
				$this->pcount['boards_detail'] = APP_PCOUNT_BOARDS_DETAIL_S;
				$this->pcount['boards_view_res'] = APP_PCOUNT_BOARDS_VIEW_RES_S;

				$this->pcount['categorylist'] = 20;
			}
		}
		$this->set('pcount', $this->pcount);

		// パンくずに追加
		$this->addBreadCrumbs("全国" , APP_ALLAREA_URL);
		$this->addBreadCrumbs($this->parent_area['LargeArea']['name'], $this->parent_area['LargeArea']['url'] . 'top');
	}

	function init_model() {

		$this->Shop->hasOne['UsersAuthority']['conditions']['UsersAuthority.large_area_id'] = $this->parent_area['LargeArea']['id'];
		$this->Girl->hasOne['UsersAuthority']['conditions']['UsersAuthority.large_area_id'] = $this->parent_area['LargeArea']['id'];
		$this->User->hasOne['UsersAuthority']['conditions']['UsersAuthority.large_area_id'] = $this->parent_area['LargeArea']['id'];

	}

	function remove_model_conditions() {

		unset($this->Shop->hasOne['UsersAuthority']['conditions']['UsersAuthority.large_area_id']);
		unset($this->Girl->hasOne['UsersAuthority']['conditions']['UsersAuthority.large_area_id']);
		unset($this->User->hasOne['UsersAuthority']['conditions']['UsersAuthority.large_area_id']);

	}

	/** ログイン者データ */
	function get_login_reviewer_data() {

		//AUTH関連設定
		$this->Auth->authError = "ログインをお願いします。";
		$this->Auth->loginError = "ログインに失敗しました。";
		$this->Auth->userModel = 'Reviewer';
		$this->Auth->autoRedirect = false;
		$this->Auth->loginAction = array('controller' => 'accounts','action' => 'login',);
//		$this->Auth->userScope = array('Reviewer.large_area_id' => $this->parent_area['LargeArea']['id']);

		//レビュアーデータ
		$this->parent_reviewer = $this->Reviewer->findbyId($this->Auth->user('id'));
		$this->set('parent_reviewer',$this->parent_reviewer);

	}

	/** pagenatorの初期設定 */
	function get_pagenate_settings() {

		//ページングURL設定
		$user_id = '';
		if(isset($this->params['user_id'])){
			$user_id = $this->params['user_id'];
		}
		$girl_id = '';
		if(isset($this->params['girl_id'])){
			$girl_id = $this->params['girl_id'];
		}
		$board_id = '';
		if(isset($this->params['board_id'])){
			$board_id = $this->params['board_id'];
		}
		//start---2013/3/11 障害No.1-4-0002修正
		$business_category_id = '';
		if(isset($this->params['business_category_id'])){
			$business_category_id = $this->params['business_category_id'];
		}
		$reviewer_id = '';
		if(isset($this->params['reviewer_id'])){
			$reviewer_id = $this->params['reviewer_id'];
		}
		//end---2013/3/11 障害No.1-4-0002修正
		$phpsessid = '';
		if(isset($this->params['url']['PHPSESSID'])){
			$phpsessid = $this->params['url']['PHPSESSID'];
		}
		$this->set('paginate_url_param',array(
				'user_id' => $user_id,
				'board_id' => $board_id,
				'girl_id' => $girl_id,
				//start---2013/3/11 障害No.1-4-0002修正
				'business_category_id' => $business_category_id,
				'reviewer_id' => $reviewer_id,
				//end---2013/3/11 障害No.1-4-0002修正
				'?' => array('PHPSESSID' => $phpsessid),
		));

	}

	// ビッグバナーの画像を取得
	function getBigbanner() {

		$bigbanners = $this->BigBanner->find('all', array(
			'conditions' => array(
				'BigBanner.large_area_id' => $this->parent_area['LargeArea']['id'],
			),
			'order' => 'rand()',
		));
		$this->set('bigbanners', $bigbanners);

	}

	//新着情報取得
	function getNews(){
		$minus_14_day = date('Y-m-d', strtotime('-14 day'));

		//新規店舗
		$new_shops = $this->Shop->find('all', array(
			'fields' => array('User.id','Shop.name', 'Shop.created', '"Shop" as id', 'LargeArea.url'),
			'conditions' => array(
				'Shop.is_created' => 1,
				'DATE_FORMAT(Shop.created,"%Y-%m-%d") >= ' => $minus_14_day
			),
			'group' => 'Shop.id',
			'order' => 'Shop.created desc',
			'limit' => 5
		));
		foreach($new_shops as $key => $value){
			$new_shops[$key]['0']['news_modified'] = $value['Shop']['created'];
		}

		//新人の女の子
		$new_girls = $this->Girl->find('all', array(
				'fields' => array('User.id','Shop.name','Girl.id', 'Girl.name', 'Girl.created', '"Girl" as id', 'LargeArea.url'),
				'conditions' => array(
					'Shop.is_created' => 1,
					'Girl.is_deleted' => 0,
					'DATE_FORMAT(Girl.created,"%Y-%m-%d") >= ' => $minus_14_day
				),
				'group' => 'Girl.id',
				'order' => 'Girl.created desc',
				'limit' => 5
			));
		foreach($new_girls as $key => $value){
			$new_girls[$key]['0']['news_modified'] = $value['Girl']['created'];
		}

		//最新口コミ
		$new_reviews = $this->Review->find('all', array(
				'fields' => array('User.id','Shop.name','Girl.id', 'Girl.name', 'Review.created', '"Review" as id', 'LargeArea.url', 'Review.id'),
				'conditions' => array(
					$this->getCurrentAreaConditionForReview(),
					'DATE_FORMAT(Review.created,"%Y-%m-%d") >= ' => $minus_14_day,
				),
				'group' => 'Review.id',
				'order' => 'Review.created desc',
				'limit' => 5
			));
		foreach($new_reviews as $key => $value){
			$new_reviews[$key]['0']['news_modified'] = $value['Review']['created'];
		}


		//全てのデータをマージ
		$news_list = array_merge($new_shops, $new_girls, $new_reviews);
		//日付降順でソート
		$news_list = Set::sort($news_list, '{n}.0.news_modified', 'desc');
		array_splice($news_list, $this->pcount['news_list']);
		$this->set('news_list', $news_list);
	}

	//conditions当該地域
	function getCurrentAreaConditionForReview(){
		return array(
			'User.id IN (SELECT user_id FROM users_authorities WHERE large_area_id = ' . $this->parent_area['LargeArea']['id'] . ')',
			'Shop.is_created' => 1,
		);
	}

	//conditions当該地域(レビュアー用)
	// function getCurrentAreaConditionForReviewer(){
	// 	return array('Reviewer.large_area_id' => $this->parent_area['LargeArea']['id']);
	// }

// -------------------------------------------------------------------
// 追加機能分
// -------------------------------------------------------------------
	function addBreadCrumbs($name, $link) {
		$this->breadcrumb[] = array("name" => $name , "link" => $link);
	}

	function clearBreadCrumbs() {
		$this->breadcrumb = array();
	}

	function getLargeArea() {

//		$url = explode(':', env('HTTP_HOST'));
		$url = explode('.', env('HTTP_HOST'));	//19800210.com 対応

		$url = substr($url[0], 0, strlen($url[0])-1);
		// 地域を検索
		$large_area = $this->LargeArea->find('first', array(
			'conditions' => array(
				'LargeArea.url LIKE' => '%' . $url . '%',
			)
		));
		if(empty($large_area)) {
			// エリアが取れないと強制終了
			exit();
		}

		return $large_area;
	}

	/** アクセスカウント カウントアップ */



	function saveAccessData() {
		// アクセス加算
		// 全体
		$this->_saveAccess(Access::ACCESS_TYPE_ALL);

		// カテゴリ毎アクセスカウント
		if($this->params['controller'] == 'category')
		{
			$this->_saveAccess(Access::ACCESS_TYPE_CATEGORY, $this->params['business_category_id']);
		}

		// お店ごとアクセスカウント
		if($this->params['controller'] == 'users')
		{
			$this->_saveAccess(Access::ACCESS_TYPE_SHOP, $this->params['user_id']);
		}

		// 女の子毎アクセスカウント
		if($this->params['controller'] == 'users' && $this->params['action'] == 'girl')
		{
			$this->_saveAccess(Access::ACCESS_TYPE_GIRL, $this->params['girl_id']);
		}

	}

	/**
	 * アクセステーブルの更新処理
	 */
	function _saveAccess($access_type, $relation_id = Access::RELATION_ID_NONE) {

		$access = $this->Access->find('first', array('conditions' => array(
			'Access.large_area_id' => $this->parent_area['LargeArea']['id'],
			'Access.access_type' => $access_type,
			'Access.relation_id' => $relation_id,
			'Access.date' => date('Y-m-d'),
		)));
		if(!empty($access)){
			$access['Access']['count'] += 1;
			$this->Access->save($access);
		}else{
			$access['Access']['large_area_id'] = $this->parent_area['LargeArea']['id'];
			$access['Access']['access_type'] = $access_type;
			$access['Access']['date'] = date('Y-m-d');
			$access['Access']['relation_id'] = $relation_id;
			$access['Access']['count'] = 1;
			$access['Access']['rand'] = rand(35,96);
			$this->Access->save($access);
		}
	}

	function get_device_path($non_s_plefix = null) {
		if(!empty($this->params['prefix'])){
			if($this->params['prefix'] == 's' && $non_s_plefix == true){
				return '';
			}else{
				return '/' . $this->params['prefix'];
			}
		}
	}

	/**
	 * メール送信
	 */
	function send_mail($options = array()) {

		if(!is_array($options['smtpOptionsKey'])) {
			$options['smtpOptionsKey'] = Configure::read($options['smtpOptionsKey']);
		}

		//メール送信
		$this->Email->to = $options['to'];
		$this->Email->from = $options['from'];
		$this->Email->subject = $options['subject'];
		$this->Email->smtpOptions = $options['smtpOptionsKey'];
		$this->Email->delivery = 'smtp';
		$this->Email->lineLength = 1024;

		if(isset($options['bcc']) && !empty($options['bcc'])) {
			$this->Email->bcc = $options['bcc'];
		}

		// 一時退避
		$autoRender = $this->autoRender;
		$viewVar = $this->viewVars;
		$layout = $this->layout;

		// メッセージ抜き出し
		$this->viewVars = $options['params'];
		$this->autoRender = FALSE;
		$this->layout = FALSE;
		$message = $this->render('/email/' . $options['template']);
		$this->output = NULL;

		// データ復帰
		$this->autoRender = $autoRender;
		$this->viewVars = $viewVar;
		$this->layout = $layout;

		// FIXME デバッグ
		$this->log('メール送信します', 'debug');
		$this->log($message, 'debug');

		// メール送信
		$this->Email->send($message);
	}

	// ----- パーツデータ取得
	/**
	 *	新着口コミ(/top, /newest)
	 */
	function get_newest_review() {
		//口コミ情報取得
		$conditions = array(
			$this->getCurrentAreaConditionForReview(),
		);
		$new_review = $this->_getReviews($conditions, 'Review.created DESC', 20);
		$this->set('new_review', $new_review);
	}

	// カテゴリ情報を取得
	function get_category_data() {
		// カテゴリ
		$this->MShopsBusinessCategory->bindModel(array('belongsTo' => array('Shop' => array(
			'className'=>'Shop',
			'type'=>'inner',
			'fields' => array('ifnull(count(Shop.id),0) as cnt'),
			'conditions' => array(
				'Shop.shops_business_category_id = MShopsBusinessCategory.id',
				'Shop.is_created' => 1,
			),
			'foreignKey' => false,))),false);
		$this->MShopsBusinessCategory->bindModel(array('belongsTo' => array('UsersAuthority' => array(
			'className'=>'UsersAuthority',
			'type'=>'inner',
			'conditions' => array(
				'Shop.user_id = UsersAuthority.user_id',
				'UsersAuthority.large_area_id' => $this->parent_area['LargeArea']['id'],
			),
			'foreignKey' => false,))),false);
		$m_shop_business_category = $this->MShopsBusinessCategory->find('all', array(
			'order' => 'MShopsBusinessCategory.id',
			'group' => array('MShopsBusinessCategory.id HAVING cnt > 0'),
		));
		$this->set('categories', $m_shop_business_category);

		// カテゴリの値-名前リスト
		$arrCategoryValueList = array();
		foreach($m_shop_business_category AS $key => $category)
		{
			$arrCategoryValueList[$category['MShopsBusinessCategory']['id']] = $category['MShopsBusinessCategory']['name'];
		}
		$this->set('categories_value_list', $arrCategoryValueList);

		$this->MShopsBusinessCategory->unbindModel(array('belongsTo' => array('Shop', 'User', 'UsersAuthority')), false);
	}

	// ピックアップ口コミ情報取得
	function get_pickup_review() {

		$conditions = array(
			$this->getCurrentAreaConditionForReview(),
			'Review.pickup_admin' => 1
		);
		$pic_review = $this->_getReviews($conditions, 'rand()', 2);
		$this->set('pic_review', $pic_review);
	}

	// レコメンドショップ情報を取得
	function get_recommend_shop() {

//		$this->Shop->unbindModel(array('hasOne' => array('Review')));
		$recommend_shop = $this->Shop->find('all', array(
			'conditions' => array(
				'Shop.is_created' => 1,
				'UsersAuthority.is_show_recommend_shop' => 1
			),
			'order' =>'rand()',
		));
		$this->set('recommend_shop', $recommend_shop);

	}

	// イチオシのお店情報を取得
	function get_topic_shop() {
		// イチオシのお店を取得
//		$this->Shop->unbindModel(array('hasOne' => array('Review')));
		$topic_shop = $this->Shop->find('all', array(
			'conditions' => array(
				'Shop.is_created' => 1,
				'UsersAuthority.is_show_pickup_shop' => 1
			),
			'order' =>'rand()',
			'limit' => 15
		));
		$this->set('topic_shop', $topic_shop);
	}

	// 新着のレビュアー情報を取得 ※タカモト
	function get_new_reviewer() {

		$this->Reviewer->bindModel(array('belongsTo' => array('MReviewersAge' => array(
			'className'=>'MReviewersAge',
			'conditions' => array(
				'Reviewer.age_id = MReviewersAge.id',
			),
			'foreignKey' => false,))),false);
		$this->Reviewer->bindModel(array('belongsTo' => array('MReviewersJob' => array(
			'className'=>'MReviewersJob',
			'conditions' => array(
				'Reviewer.job_id = MReviewersJob.id',
			),
			'foreignKey' => false,))),false);

		$new_reviewer = $this->Reviewer->find('all', array(
			'conditions' => array(
				'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id'],
			),
			'order' =>'Reviewer.created DESC',
			'limit' => 5
		));
		$this->set('new_reviewer', $new_reviewer);
		$this->Shop->unbindModel(array('belongsTo' => array('MReviewersAge', 'MReviewersJob')));
	}

	// ランキング情報取得（店舗総合）
	function get_ranking_data_shop($limit = 3) {

		// 対象データは1週間
		$limit_day = date('Y-m-d', strtotime('-' . APP_SUMMARY_REVIEW_RANGE));

		// お店総合ランキング
		$this->SummaryData->bindModel(array('belongsTo' => array('Shop' => array(
			'className'=>'Shop',
			'type'=>'inner',
			'conditions' => array(
				'Shop.user_id = SummaryData.relation_id',
			),
			'foreignKey' => false,))),false);

		$this->SummaryData->bindModel(array('belongsTo' => array('User' => array(
			'className'=>'User',
			'type'=>'inner',
			'conditions' => array(
				'User.id = SummaryData.relation_id',
			),
			'foreignKey' => false,))),false);

		$this->SummaryData->bindModel(array('belongsTo' => array('UsersAuthority' => array(
			'className'=>'UsersAuthority',
			'type'=>'inner',
			'conditions' => array(
				'UsersAuthority.user_id = SummaryData.relation_id',
			),
			'foreignKey' => false,))),false);

		$this->SummaryData->bindModel(array('belongsTo' => array('ReviewTotal' => array(
			'className'=>'Review',
			'type'=>'inner',
			'fields' => array(
				'ReviewTotal.user_id',
				'count(ReviewTotal.id) AS total_cnt',
				'(AVG(ReviewTotal.score_girl_character) + AVG(ReviewTotal.score_girl_service) + AVG(ReviewTotal.score_girl_looks)) / 3 AS total_average',
			),
			'group' => 'ReviewTotal.user_id',
			'conditions' => array(
				'ReviewTotal.user_id = User.id',
				'DATE_FORMAT(ReviewTotal.created,"%Y-%m-%d") >= ' => $limit_day,
			),
			'foreignKey' => false,))),false);

		$shop_ranking = $this->SummaryData->find('all', array(
			'conditions' => array(
				'SummaryData.summary_type' => SummaryData::SUMMARY_TYPE_SHOP,
				'SummaryData.large_area_id' => $this->parent_area['LargeArea']['id'],
				'DATE_FORMAT(SummaryData.date,"%Y-%m-%d") >= ' => $limit_day,
			),
			'order' => 'SummaryData.rank ASC',
			'group' => 'SummaryData.relation_id',
			'limit' => $limit,
		));
		$this->set('shop_ranking', $shop_ranking);
		$this->SummaryData->unbindModel(array('belongsTo' => array('Shop', 'User', 'UsersAuthority', 'ReviewTotal')));
	}

	// ランキング情報取得（女の子アクセス数）
	function get_ranking_data_girl($limit = 3) {

		// 対象データは1週間
		$limit_day = date('Y-m-d', strtotime('-' . APP_SUMMARY_REVIEW_RANGE));

		// 女の子総合ランキング
		$this->SummaryData->bindModel(array('belongsTo' => array('Girl' => array(
			'className'=>'Girl',
			'type'=>'inner',
			'conditions' => array(
				'Girl.id = SummaryData.relation_id',
			),
			'foreignKey' => false,))),false);

		$this->SummaryData->bindModel(array('belongsTo' => array('Shop' => array(
			'className'=>'Shop',
			'type'=>'inner',
			'conditions' => array(
				'Shop.is_created' => 1,
				'Shop.user_id = Girl.user_id',
			),
			'foreignKey' => false,))),false);

		$this->SummaryData->bindModel(array('belongsTo' => array('User' => array(
			'className'=>'User',
			'type'=>'inner',
			'conditions' => array(
				'User.id = Girl.user_id',
			),
			'foreignKey' => false,))),false);

		$this->SummaryData->bindModel(array('belongsTo' => array('UsersAuthority' => array(
			'className'=>'UsersAuthority',
			'type'=>'inner',
			'conditions' => array(
				'UsersAuthority.large_area_id' => $this->parent_area['LargeArea']['id'],
				'UsersAuthority.user_id = Girl.user_id',
			),
			'foreignKey' => false,))),false);

		$this->SummaryData->bindModel(array('belongsTo' => array('ReviewTotal' => array(
			'className'=>'Review',
			'type'=>'inner',
			'fields' => array(
				'ReviewTotal.user_id',
				'count(ReviewTotal.id) AS total_cnt',
				'(AVG(ReviewTotal.score_girl_character) + AVG(ReviewTotal.score_girl_service) + AVG(ReviewTotal.score_girl_looks)) / 3 AS total_average',
			),
			'group' => 'ReviewTotal.girl_id',
			'conditions' => array(
				'ReviewTotal.girl_id = Girl.id',
				'DATE_FORMAT(ReviewTotal.created,"%Y-%m-%d") >= ' => $limit_day,
			),
			'foreignKey' => false,))),false);

		$girl_ranking = $this->SummaryData->find('all', array(
			'conditions' => array(
				'SummaryData.summary_type' => SummaryData::SUMMARY_TYPE_GIRL,
				'SummaryData.large_area_id' => $this->parent_area['LargeArea']['id'],
				'DATE_FORMAT(SummaryData.date,"%Y-%m-%d") >= ' => $limit_day,
			),
			'order' => 'SummaryData.rank ASC',
			'group' => 'SummaryData.relation_id',
			'limit' => $limit,
		));
		$this->set('girl_ranking', $girl_ranking);
		$this->SummaryData->unbindModel(array('belongsTo' => array('Shop', 'Girl', 'User', 'UsersAuthority', 'ReviewTotal')));
	}

	// ランキング情報取得（口コミ投稿者）
	function get_ranking_data_reviewer($limit = 3) {
/*
		// お店総合ランキング
		$this->SummaryData->bindModel(array('belongsTo' => array('Shop' => array(
			'className'=>'Shop',
			'type'=>'inner',
			'conditions' => array(
				'Shop.user_id = SummaryData.relation_id',
			),
			'foreignKey' => false,))),false);

		$this->SummaryData->bindModel(array('belongsTo' => array('User' => array(
			'className'=>'User',
			'type'=>'inner',
			'conditions' => array(
				'User.id = SummaryData.relation_id',
			),
			'foreignKey' => false,))),false);
*/
		// FIXME 統計情報と連結する必要あり
		$this->Reviewer->bindModel(array('belongsTo' => array('ReviewTotal' => array(
			'className'=>'Review',
			'type'=>'inner',
			'fields' => array(
				'ReviewTotal.reviewer_id',
				'count(ReviewTotal.id) AS total_cnt',
			),
			'group' => 'ReviewTotal.reviewer_id',
			'conditions' => array(
				'ReviewTotal.reviewer_id = Reviewer.id',
			),
			'foreignKey' => false,))),false);

		$reviewer_ranking = $this->Reviewer->find('all', array(
			'conditions' => array(
				'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id'],
			),
			'order' => '(SELECT count(id) FROM reviews r WHERE r.reviewer_id = Reviewer.id GROUP BY r.reviewer_id) DESC',
			'group' => 'Reviewer.id',
			'limit' => $limit,
		));
		$this->Reviewer->unbindModel(array('belongsTo' => array('ReviewTotal')), false);

		foreach($reviewer_ranking AS $key => $record) {

			$data = $this->Review->find('first', array(
				'conditions' => array(
					'Review.reviewer_id' => $record['Reviewer']['id'],
				),
				'order' => 'Review.created DESC',
			));

			if(!empty($data))
			{
				$reviewer_ranking[$key]['Review'] = $data['Review'];
			}
		}
		$this->set('reviewer_ranking', $reviewer_ranking);
	}


	/**
	 * レビュー情報取得共通関数
	*/
	function _getReviews($conditions, $order, $limit = -1, $add_fields = array(), $only_publish = true, $add_deleted = false) {

		if($only_publish) {
			if(!array_key_exists('Review.is_publish', $conditions))
			{
				$conditions['Review.is_publish'] = 1;
			}
		}
		if(!$add_deleted) {
			if(!array_key_exists('Review.comment_deleted', $conditions))
			{
				// コメント削除, 削除なしの場合のみ
				$conditions['Review.delete_flag <='] = Review::DELETE_FLAG_COMMENT_ONLY;
			}
		}

		$fields = array(
			'LargeArea.url','User.id','MReviewersAge.name','MReviewsPlace.name','MReviewersJob.name',
			'Shop.*','Girl.*','Review.*','Reviewer.*',
			'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg',
			'ifnull((SELECT count(reviews.id) FROM reviews WHERE reviews.reviewer_id > 0 AND reviews.reviewer_id = Reviewer.id GROUP BY reviews.reviewer_id HAVING count(*) > 0), 0) AS review_cnt',
		);

		if(!empty($add_fields)) {
			$fields = array_merge($fields, $add_fields);
		}

		$params = array(
			'fields' => $fields,
			'conditions' => $conditions,
			'group' => array('Review.id'),
			'order' => $order,
		);

		if($limit > 0) {
			$params['limit'] = $limit;
		}

		$this->Review->bindModel(array('hasOne' => array('MReviewersJob' => array(
			'className'=>'MReviewersJob',
			'conditions' => array(
				'MReviewersJob.id = Reviewer.job_id',
			),
			'foreignKey' => false,))),false);


		$reviews = $this->Review->find('all', $params);

		$this->Reviewer->unbindModel(array('hasOne' => array('MReviewersJob')), false);

		foreach($reviews AS $key => $record) {
			$reviews[$key]['Review']['appointed_type_name'] = $record['Review']['appointed_type'] == 0 ? '' : Review::$appointed_type[$record['Review']['appointed_type']];
			$reviews[$key]['Review']['appointed_sub_name'] = $record['Review']['appointed_sub'] == 0 ? '' : Review::$appointed_sub[$record['Review']['appointed_sub']];
		}

		return $reviews;
	}

	/**
	 * レビュー情報取得共通関数(ページネーション版)
	*/
	function _getReviewsPagenate($conditions, $order, $limit = 10, $add_fields = array(), $only_publish = true, $add_deleted = false) {

		if($only_publish) {
			if(!array_key_exists('Review.is_publish', $conditions))
			{
				$conditions['Review.is_publish'] = 1;
			}
		}
		if(!$add_deleted) {
			if(!array_key_exists('Review.comment_deleted', $conditions))
			{
				// コメント削除, 削除なしの場合のみ
				$conditions['Review.delete_flag <='] = Review::DELETE_FLAG_COMMENT_ONLY;
			}
		}

		$fields = array(
			'LargeArea.url','User.id','MReviewersAge.name','MReviewsPlace.name','MReviewersJob.name',
			'Shop.*','Girl.*','Review.*','Reviewer.*',
			'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg',
			'ifnull((SELECT count(reviews.id) FROM reviews WHERE reviews.reviewer_id > 0 AND reviews.reviewer_id = Reviewer.id GROUP BY reviews.reviewer_id HAVING count(*) > 0), 0) AS review_cnt',
		);

		if(!empty($add_fields)) {
			$fields = array_merge($fields, $add_fields);
		}

		$params = array(
			'fields' => $fields,
			'conditions' => $conditions,
			'group' => array('Review.id'),
			'order' => $order,
			'limit' => $limit,
		);

		$this->Review->bindModel(array('hasOne' => array('MReviewersJob' => array(
			'className'=>'MReviewersJob',
			'conditions' => array(
				'MReviewersJob.id = Reviewer.job_id',
			),
			'foreignKey' => false,))),false);


		$this->paginate = array('Review' => $params);
		$this->Reviewer->unbindModel(array('hasOne' => array('MReviewersJob')), false);

		$reviews = $this->paginate($this->Review);

		foreach($reviews AS $key => $record) {
			$reviews[$key]['Review']['appointed_type_name'] = $record['Review']['appointed_type'] == 0 ? '' : Review::$appointed_type[$record['Review']['appointed_type']];
			$reviews[$key]['Review']['appointed_sub_name'] = $record['Review']['appointed_sub'] == 0 ? '' : Review::$appointed_sub[$record['Review']['appointed_sub']];
		}

		return $reviews;
	}
}

// 店舗ページ用コントローラ
class UserController extends AppController {
	var $parent_shop = null;

	function beforefilter(){
		parent::beforefilter();

		//存在しない、店舗登録を行なっていないショップの場合はエラー画面へ
		$shop_present = $this->Shop->find('count',array('conditions' => array('Shop.user_id' => $this->params['user_id'], 'Shop.is_created' => 1)));

		if($shop_present == 0){
			$this->cakeError('error404');
		}

		//ショップ情報
		$this->parent_shop = $this->Shop->find('first', array(
			'conditions' => array(
				'Shop.user_id' => $this->params['user_id']
			)
		));
		$this->set('parent_shop',$this->parent_shop);

		// ショップに在籍する女の子の数をカウント
		$cntGirl = $this->Girl->find('count', array(
			'conditions' => array(
				'Girl.user_id' => $this->params['user_id'],
				'Girl.is_deleted' => 0,
			),
		));
		$this->set('count_girls', $cntGirl);

		//口コミデータ
		$review_data = $this->Review->find('first', array(
			'fields' => array(
				'ifnull(SUM(Review.score_girl_character),0) / count(Review.id) as score_girl_character',
				'ifnull(SUM(Review.score_girl_service),0) / count(Review.id) as score_girl_service',
				'ifnull(SUM(Review.score_girl_looks),0) / count(Review.id) as score_girl_looks',
				'count(Review.id) as count'
			),
			'conditions' => array(
				'Shop.is_created' => 1,
				'User.id' => $this->params['user_id'],
			),
			'group' => array('User.id'),
		));
		if(empty($review_data[0]['count'])){
			$review_data[0]['count'] = 0;
			$review_data[0]['total'] = 0;
			$review_data[0]['score_girl_character'] = 0;
			$review_data[0]['score_girl_service'] = 0;
			$review_data[0]['score_girl_looks'] = 0;
		}else{
			$review_data[0]['total'] = ($review_data[0]['score_girl_character']+$review_data[0]['score_girl_service']+
					$review_data[0]['score_girl_looks']) / 3;
		}
		$this->set('review_data', $review_data);
	}
	function beforeRender(){
		parent::beforeRender();

	}

}

//レビュアー部分の共通コントローラー
class ReviewersController extends AppController {

	function beforeFilter(){
		parent::beforeFilter();

		// レビュアー情報が無いとアクセス出来ない
		if(empty($this->parent_reviewer)) {
			$this->redirect($this->device_path . '/accounts/login');
			return;
		}
	}


	function getFollower($reviewer_id) {

		// フォローしている人
		$this->Reviewer->bindModel(array('belongsTo' => array('FollowReviewer' => array(
			'className'=>'FollowReviewer',
			'type' => 'inner',
			'conditions' => array(
				'FollowReviewer.from_reviewer_id' => $reviewer_id,
				'Reviewer.id = FollowReviewer.to_reviewer_id',
			),
			'foreignKey' => false,))),false);

		$follow_reviewer = $this->Reviewer->find('all', array(
			'fields' => array(
				'Reviewer.*', 'FollowReviewer.created','MShopsBusinessCategory.name',
				'ifnull((SELECT count(follow_reviewers.id) FROM follow_reviewers WHERE follow_reviewers.to_reviewer_id = Reviewer.id), 0) AS follower_cnt',
				'ifnull((SELECT count(reviews.id) FROM reviews WHERE reviews.reviewer_id = Reviewer.id), 0) AS review_cnt',
			),
			'order' => 'Reviewer.created DESC',
		));
		$this->set('follow_reviewer', $follow_reviewer);
		$this->Reviewer->unbindModel(array('belongsTo' => array('FollowReviewer')), false);

		// フォローされている人
		$this->Reviewer->bindModel(array('belongsTo' => array('FollowReviewer' => array(
			'className'=>'FollowReviewer',
			'type' => 'inner',
			'conditions' => array(
				'FollowReviewer.to_reviewer_id' => $reviewer_id,
				'Reviewer.id = FollowReviewer.from_reviewer_id',
			),
			'foreignKey' => false,))),false);

		$followed_reviewer = $this->Reviewer->find('all', array(
			'fields' => array(
				'Reviewer.*', 'FollowReviewer.created','MShopsBusinessCategory.name',
				'ifnull((SELECT count(follow_reviewers.id) FROM follow_reviewers WHERE follow_reviewers.to_reviewer_id = Reviewer.id), 0) AS follower_cnt',
				'ifnull((SELECT count(reviews.id) FROM reviews WHERE reviews.reviewer_id = Reviewer.id), 0) AS review_cnt',
			),
			'order' => 'Reviewer.created DESC',
		));
		$this->set('followed_reviewer', $followed_reviewer);
		$this->Reviewer->unbindModel(array('belongsTo' => array('FollowReviewer')), false);

		// おすすめのユーザー
		$recommend_reviewer = $this->Reviewer->find('all', array(
			'fields' => array(
				'Reviewer.*',
				'ifnull((SELECT count(follow_reviewers.id) FROM follow_reviewers WHERE follow_reviewers.to_reviewer_id = Reviewer.id), 0) AS follower_cnt',
			),
			'conditions' => array(
				'Reviewer.id <>' => $reviewer_id,
			),
			'order' => 'rand()',
			'limit' => 5,
		));
		$this->set('recommend_reviewer', $recommend_reviewer);

	}
}
