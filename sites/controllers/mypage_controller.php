<?php
class MypageController extends ReviewersController {
	var $name = 'Mypage';
	var $uses = array('Review','Message','MReviewersAge', 'MReviewersJob','ReviewerChangeemail','MReviewsAge','MReviewsPlace','ReviewsGoodCount','BookmarkShop','BookmarkGirl','Message');

	function beforeFilter(){
		parent::beforeFilter();
		$this->layout = 'mypage';

		// フォロワー情報取得
		$this->getFollower($this->Auth->user('id'));

		// パンくずに追加
		$this->clearBreadCrumbs();
		$this->addBreadCrumbs("全国" , APP_ALLAREA_URL);
		$this->addBreadCrumbs($this->parent_area['LargeArea']['name'], $this->parent_area['LargeArea']['url'] . 'top');
		$this->addBreadCrumbs("Myページ" , "/mypage");

		switch($this->params['action'])
		{
			case 'favorite_shop':
				$this->addBreadCrumbs("お気に入りのお店" , "/mypage/favorite_shop");
				$this->set('bar_select', 'shop');
				break;
			case 's_favorite_shop':
				$this->addBreadCrumbs("お気に入りのお店" , "/s/mypage/favorite_shop");
				$this->set('bar_select', 'shop');
				break;
			case 'favorite_girl':
				$this->addBreadCrumbs("お気に入りの女の子" , "/mypage/favorite_girl");
				$this->set('bar_select', 'girl');
				break;
			case 's_favorite_girl':
				$this->addBreadCrumbs("お気に入りの女の子" , "/s/mypage/favorite_girl");
				$this->set('bar_select', 'girl');
				break;
			case 'favorite_review':
				$this->addBreadCrumbs("お気に入りのレビュー" , "/mypage/favorite_review");
				$this->set('bar_select', 'review');
				break;
			case 's_favorite_review':
				$this->addBreadCrumbs("お気に入りのレビュー" , "/s/mypage/favorite_review");
				$this->set('bar_select', 'review');
				break;
			case 'follow':
				$this->addBreadCrumbs("フォロー一覧" , "#");
				$this->set('bar_select', 'top');
				break;
			case 's_follow':
				$this->addBreadCrumbs("フォロー一覧" , "/s/mypage/follow");
				$this->set('bar_select', 'top');
				break;
			case 'follower':
				$this->addBreadCrumbs("フォロワー一覧" , "#");
				$this->set('bar_select', 'top');
				break;
			case 's_follower':
				$this->addBreadCrumbs("フォロワー一覧" , "/s/mypage/follower");
				$this->set('bar_select', 'top');
				break;
			case 'profile':
				$this->addBreadCrumbs("プロフィール" , "/mypage/profile");
				$this->set('bar_select', 'top');
				$this->set('side_select', 'profile');
				break;
			case 'profile_edit':
			case 'profile_edit_end':
				$this->addBreadCrumbs("プロフィール" , "/mypage/profile");
				$this->addBreadCrumbs("プロフィール編集" , "/mypage/profile_edit");
				$this->set('bar_select', 'top');
				$this->set('side_select', 'profile');
				break;
			case 's_profile_edit':
			case 's_profile_edit_end':
				$this->addBreadCrumbs("プロフィール" , "/s/mypage/profile");
				$this->addBreadCrumbs("プロフィール編集" , "/s/mypage/profile_edit");
				$this->set('bar_select', 'top');
				$this->set('side_select', 'profile');
				break;
			case 'myreview':
				$this->addBreadCrumbs("Myレビュー" , "/mypage/myreview");
				$this->set('bar_select', 'top');
				$this->set('side_select', 'myreview');
				break;
			case 's_myreview':
				$this->addBreadCrumbs("Myレビュー" , "/s/mypage/myreview");
				$this->set('bar_select', 'top');
				$this->set('side_select', 'myreview');
				break;
			case 'myreview_edit':
			case 'myreview_edit_end':
				$this->addBreadCrumbs("Myレビュー編集" , "/mypage/myreview_edit");
				$this->set('bar_select', 'top');
				$this->set('side_select', 'myreview');
				break;
			case 'reviews':
				$this->addBreadCrumbs("レビュー" , "#");
				$this->set('bar_select', 'top');
				break;
			case 'index':
			default:
				$this->set('bar_select', 'top');
				$this->set('side_select', 'top');
				break;
		}
	}
	
	//レビュアーTOP
	function index(){

		//head
		$this->set('title_for_layout', $this->title_tag_common . ' Myページトップ');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',Myページ,トップ,デリヘルランキング');
		$this->set('meta_description', $this->meta_description_common . 'Myページトップ');
		$this->set('header_one', $this->h1_tag_common . 'Myページトップ');

		$shop_datas = $this->_get_favorite_shop_data();
		$girl_datas = $this->_get_favorite_girl_data();
		$review_datas = $this->_get_favorite_review_data();

		$list_data = array();

		foreach($shop_datas AS $ii => $record) {
			$list_data[$record['BookmarkShop']['created']] = array(
				'record' => $record,
				'type' => 'shop',
			);
		}
		foreach($girl_datas AS $ii => $record) {
			$list_data[$record['BookmarkGirl']['created']] = array(
				'record' => $record,
				'type' => 'girl',
			);
		}
		foreach($review_datas AS $ii => $record) {
			$list_data[$record['BookmarkReview']['created']] = array(
				'record' => $record,
				'type' => 'review',
			);
		}

		krsort($list_data);
		$this->set('list', $list_data);
	}
	function i_index() {
		$this->index();
	}
	function s_index() {
		$this->index();
	}
	
	function follow(){

		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 フォロー一覧');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルランキング,フォロー一覧');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 フォロー一覧');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 フォロー一覧');

		// データは共通で取得	
	}
	function s_follow() {
		$this->follow();
	}
	
	function follower(){

		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 フォロワー一覧');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルランキング,フォロワー一覧');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 フォロワー一覧');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 フォロワー一覧');

		// データは共通で取得	
	}
	function s_follower() {
		$this->follower();
	}
	
	function myreview(){

		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 Myレビュー');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルランキング,Myレビュー');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 Myレビュー');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 Myレビュー');

		// レビュー情報を取得する
		$conditions = array(
			'Shop.is_created' => 1,
			'Review.reviewer_id' => $this->Auth->user('id'),
		);
		$self_review = $this->_getReviews($conditions, 'Review.created DESC');

		$this->set('self_review', $self_review);

	}
	function s_myreview() {

		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 Myレビュー');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルランキング,Myレビュー');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 Myレビュー');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 Myレビュー');

		// レビュー情報を取得する
		$conditions = array(
			'Shop.is_created' => 1,
			'Review.reviewer_id' => $this->Auth->user('id'),
		);
		$self_review = $this->_getReviewsPagenate($conditions, 'Review.created DESC');

		$this->set('self_review', $self_review);
	}
	
	//↓レビュー編集画面・編集完了画面・削除完了画面等20150210
	function myreview_edit() {

		$objReviewData = $this->Review->find('first', array(
			'conditions' => array(
				'Review.id' => $this->params['review_id'],
				'Review.reviewer_id' => $this->parent_reviewer['Reviewer']['id'],
			),
		));

		if(empty($objReviewData)) {
			$this->cakeError('error404');
		}

		// 登録の場合
		if (!empty($this->data)){

			$this->Review->set($this->data);
			if($this->Review->validates()) {

				// 自動公開機能
				if(APP_REVIEW_AUTO_PUBLISH) {
					$this->data['Review']['is_publish'] = 1;
				} else {
					$this->data['Review']['is_publish'] = 0;
				}

				//口コミ投稿
				//ipアドレス等登録
				$this->data['Review']['ip_address'] = $this->MediaInfo->getClientIP();
				$this->data['Review']['uid'] = $this->MediaInfo->getUid();
				$this->data['Review']['media'] = $this->MediaInfo->getMedia();
				$this->Review->save($this->data['Review']);

				// 自動公開ではない場合のみメール送信
				if(!APP_REVIEW_AUTO_PUBLISH) {

					$subject = '【' . $this->parent_area['LargeArea']['name'] . 'デリヘル口コミランキング】　口コミ変更が行われました';
					$template = 'mypage_myreview_edit';

					// メール送信()
					$mailOptions = array(
						'to' => VALID_POST_REVIEW,
						'from' => VALID_POST_REVIEW,
						'subject' => $subject,
						'smtpOptionsKey' => 'smtpOptionPostReview',
						'params' => array(
							'area_data' => $this->parent_area,
							'review' => $this->data['Review'],
						),
						'template' => $template,
					);
					$this->send_mail($mailOptions);
				}

				// 更新完了画面に遷移
				$this->redirect($this->device_path . '/mypage/myreview_edit_end');
			}

		} else {
			$this->data = $objReviewData;
		}

		// 場所リスト
		$m_reviews_place = Set::Combine($this->MReviewsPlace->find('all'),'{n}.MReviewsPlace.id', '{n}.MReviewsPlace.name');
		$this->set('m_reviews_place', $m_reviews_place);

		// 指名リスト
		$this->set('appointed_type', Review::$appointed_type);
		$this->set('appointed_sub', Review::$appointed_sub);

		// 得点リスト
		$this->set('score_list', Review::$score);

		// Head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 Myレビュー編集');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルランキング,Myレビュー編集');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 Myレビュー編集');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 Myレビュー編集');
	}
	function s_myreview_edit() {
		$this->myreview_edit();
	}
	
	function myreview_edit_end() {

		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 Myレビュー編集(完了)');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルランキング,Myレビュー編集(完了)');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 Myレビュー編集(完了)');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 Myレビュー編集(完了)');

	}
	function s_myreview_edit_end() {
		$this->myreview_edit_end();
	}
	
	function shop_delete_end() {
		$this->render('shop_delete_end');
	}
	function s_shop_delete_end() {
		$this->shop_delete_end();
	}
	
	function girl_delete_end() {
		$this->render('girl_delete_end');
	}
	function s_girl_delete_end() {
		$this->girl_delete_end();
	}
	
	//↑レビュー編集画面・編集完了画面・削除完了画面等20150210
	function favorite_review(){

		//head
		$this->set('title_for_layout', $this->title_tag_common . ' お気に入りのレビュー');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルランキング,お気に入りのレビュー');
		$this->set('meta_description', $this->meta_description_common . 'お気に入りのレビュー');
		$this->set('header_one', $this->h1_tag_common . 'お気に入りのレビュー');

		$this->set('favorite_reviews', $this->_get_favorite_review_data());
	}
	function s_favorite_review() {
		$this->favorite_review();
	}
	
	function favorite_shop(){

		//head
		$this->set('title_for_layout', $this->title_tag_common . ' お気に入りのお店');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルランキング,お気に入りのお店');
		$this->set('meta_description', $this->meta_description_common . 'お気に入りのお店');
		$this->set('header_one', $this->h1_tag_common . 'お気に入りのお店');

		$this->set('favorite_shops', $this->_get_favorite_shop_data());
	}
	function s_favorite_shop() {
		$this->favorite_shop();
	}
	
	function favorite_girl(){

		//head
		$this->set('title_for_layout', $this->title_tag_common . ' お気に入りの女の子');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルランキング,お気に入りの女の子');
		$this->set('meta_description', $this->meta_description_common . 'お気に入りの女の子');
		$this->set('header_one', $this->h1_tag_common . 'お気に入りの女の子');

		$this->set('favorite_girls', $this->_get_favorite_girl_data());
	}
	function s_favorite_girl() {
		$this->favorite_girl();
	}
	
	function profile(){

		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 プロフィール表示');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルガイド,プロフィール,表示');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 プロフィール表示');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 プロフィール表示');
	}
	
	function profile_image(){
		$this->render('profile_image');
	}
	function s_profile_image() {
		$this->profile_image();
	}
	
	//プロフィール編集画面
	function profile_edit(){

		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 プロフィール編集');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルガイド,プロフィール,編集');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 プロフィール編集');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 プロフィール編集');

		if (!empty($this->data)) {
			$this->Reviewer->set($this->data);

			// 一時的に外す
			unset($this->Reviewer->validate['username']);
			unset($this->Reviewer->validate['email']);

			//他アカウントで同一メールアドレス登録済みの場合はエラー
			// if(0 < $this->Reviewer->find('count', array('conditions' => array(
			// 		'Reviewer.email' => $this->data['Reviewer']['email'],
			// 		'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id'],
			// 		'Reviewer.id <>' => $this->data['Reviewer']['id'])))){
			// 		$this->Reviewer->invalidate('email', 'このメールアドレスは既に登録済みです。');
			// }
			if($this->Reviewer->validates()){
				//メールアドレス変更かどうか
				$send_changeemail = 0;
/*
				//旧メールアドレスと一致しない場合はメールアドレス変更URL送信
				if($this->data['Reviewer']['before_email'] != $this->data['Reviewer']['email']){
					//メールアドレス変更
					$send_changeemail = 1;

					//uid生成
					$uid = '';
					$sCharList = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
					mt_srand();
					for($i = 0; $i < 16; $i++){
						$uid .= $sCharList{mt_rand(0, strlen($sCharList) - 1)};
					}
					//レビュアーデータ取得
					$reviewer = $this->Reviewer->find('first', array('conditions' => array(
							'Reviewer.email' => $this->data['Reviewer']['before_email'],
							//start---2013/3/11 障害No.1-7-0005修正
							'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id'],
							//end---2013/3/11 障害No.1-7-0005修正
					)));

					//これまでのパスワード変更データを無効にする
					$emaillist = $this->ReviewerChangeemail->find('all',  array('conditions' => array(
							'ReviewerChangeemail.reviewer_id' => $reviewer['Reviewer']['id'],
							'ReviewerChangeemail.is_changed' => 0)));
					foreach($emaillist as $key => $record){
						$emaillist[$key]['ReviewerChangeemail']['is_changed'] = 1;
					}
					//空データが登録されるため空チェックを行う
					if(!empty($emaillist)){
						$this->ReviewerChangeemail->saveAll($emaillist);
					}

					//パスワード変更データ保存
					$this->ReviewerChangeemail->create();
					$this->ReviewerChangeemail->reviewer_id = $reviewer['Reviewer']['id'];
					$this->ReviewerChangeemail->email = $this->data['Reviewer']['email'];
					$this->ReviewerChangeemail->uid = $uid;
					$this->ReviewerChangeemail->is_changed = 0;
					$this->ReviewerChangeemail->save($this->ReviewerChangeemail);

					//保存データ取得
					$id = $this->ReviewerChangeemail->id;
					$this->ReviewerChangeemail->create();
					$pass_data = $this->ReviewerChangeemail->findById($id);

					//メール送信
					$this->Email->to = $this->data['Reviewer']['email'];
					$this->Email->from = VALID_CHANGEEMAIL_URLSEND_EMAIL;
					$this->Email->subject = 'メールアドレス変更依頼';
					$this->Email->smtpOptions = Configure::read('smtpOptionChangeemailUrlsend');
					$this->Email->delivery = 'smtp';
					$this->Email->lineLength = 1024;

					$changeemail_input_url = '';
					$terminate_url = 'accounts/changeemail_end/';
					if(isset($this->params['prefix'])){
						if($this->params['prefix'] == 'i'){
							$terminate_url = 'i/accounts/changeemail_end/';
						}
					}
					$changeemail_input_url = $this->parent_area['LargeArea']['url'] .
					$terminate_url .
					$reviewer['Reviewer']['id'] . '/' .
					date('YmdHis', strtotime($pass_data['ReviewerChangeemail']['created'])) .
					'/' .
					$uid;

					//本文作成
					$message = array(
							'メールアドレスの変更を受け付けました。',
							'変更を完了するには以下のURLへアクセスしてください。',
							'',
							$changeemail_input_url,
							'なお、このURLは送信されて2時間有効です。',
							'----------------------',
							'このメールアドレスは送信専用です。',
					);
					$this->Email->send($message);
				}
				//新しいメールアドレスは保存しない
				$this->data['Reviewer']['email'] = $this->data['Reviewer']['before_email'];
*/
				$this->Reviewer->save($this->data);

				$this->redirect($this->device_path . '/mypage/profile_edit_end');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Reviewer->findbyId($this->Auth->user('id'));
		}

	
		// 各種リスト
		$this->MShopsBusinessCategory->unbindModel(array('belongsTo' => array('Shop', 'User')));
		$this->set('m_shops_business_categories', Set::Combine($this->MShopsBusinessCategory->find('all'),'{n}.MShopsBusinessCategory.id', '{n}.MShopsBusinessCategory.name'));
		$this->set('large_areas', Set::Combine($this->LargeArea->find('all'),'{n}.LargeArea.id', '{n}.LargeArea.name'));
		$this->set('m_reviewers_ages', Set::Combine($this->MReviewersAge->find('all'),'{n}.MReviewersAge.id', '{n}.MReviewersAge.name'));
		$this->set('m_reviewers_jobs', Set::Combine($this->MReviewersJob->find('all'),'{n}.MReviewersJob.id', '{n}.MReviewersJob.name'));

		$this->set('data',$this->data);

	}
	function s_profile_edit() {
		$this->profile_edit();
	}
	
	function i_profile() {
		$this->profile();
	}
	function s_profile() {
		$this->profile();
	}
	
	function profile_edit_end(){
		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 プロフィール編集完了');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルランキング,プロフィール,編集完了');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 プロフィール編集完了');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 プロフィール編集完了');

		// $this->set('send_changeemail', $send_changeemail);
	}
	function i_profile_end($send_changeemail) {
		$this->profile_end($send_changeemail);
	}
	function s_profile_edit_end() {
		$this->profile_edit_end();
	}
	
	// 削除
	// FIXME 未実装
	function myreview_delete($param){

/*
		//存在しない口コミの場合はエラー画面へ
		$present = $this->Review->find('count',array('conditions' => array('Review.reviewer_id' => $this->Auth->user('id'), 'Review.id' => $param)));
		if($present == 0){
			$this->cakeError('error404');
		}

		if (!empty($this->data)){
			//テーブル削除
			$this->ReviewsGoodCount->deleteAll(array('ReviewsGoodCount.review_id' => $this->data['Review']['id']));
			$this->Review->delete($this->data['Review']['id']);
			$this->redirect($this->device_path . '/mypage/myreview_delete_end');
		} else {
			$this->data = $this->Review->find('first', array(
					'fields' => array('LargeArea.url','User.id','Girl.id','Girl.name','Girl.age','Review.id','Review.comment','Shop.name','Review.created','Girl.image_1_s','Girl.image_1_m','Girl.image_1_l',
							'Review.score_girl_character','Review.score_girl_service','Review.score_girl_looks',
							'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg',
							'Girl.image_1_m','Review.comment_deleted','MReviewsAge.name'),
					'conditions' => array(
							'Review.id' => $param,
					),
					'group' => array('Review.id'),));
			$this->set('data',$this->data);
		}
*/
		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 '. ' 口コミ削除確認画面');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',削除,デリヘルガイド');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 口コミ削除確認画面');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 口コミ削除確認画面');
	}
	function i_myreview_delete($param) {
		$this->myreview_delete($param);
	}
	function s_myreview_delete($param) {
		$this->myreview_delete($param);
	}
	
	//削除完了画面
	function myreview_delete_end(){
		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 '. ' 口コミ削除完了画面');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',削除,完了,デリヘルガイド');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 口コミ削除完了画面');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 口コミ削除完了画面');
	}
	function i_myreview_delete_end() {
		$this->myreview_delete_end();
	}
	function s_myreview_delete_end() {
		$this->myreview_delete_end();
	}
	
	// ----------------------------------------------------------------------------------
	// Private関数
	// ----------------------------------------------------------------------------------
	// お気に入りのお店 情報取得
	function _get_favorite_shop_data() {

		// 地域絞りを外す
		$this->remove_model_conditions();

		$this->Shop->unbindModel(array('hasOne' => array('Review')));
		$this->Shop->bindModel(array('belongsTo' => array('BookmarkShop' => array(
			'className'=>'BookmarkShop',
			'type' => 'inner',
			'conditions' => array(
				'BookmarkShop.reviewer_id' => $this->Auth->user('id'),
				'Shop.user_id = BookmarkShop.user_id',
			),
			'foreignKey' => false,))),false);

		$favorite_shops = $this->Shop->find('all', array(
			'fields' => array(
				'User.*','UsersAuthority.*','Shop.*', 'System.*', 'LargeArea.url','BookmarkShop.created',
				'(SELECT count(*) FROM girls WHERE User.id = girls.user_id) AS girl_cnt',
				'(SELECT count(*) FROM reviews WHERE User.id = reviews.user_id) AS review_cnt',
			),
			'conditions' => array(
				'Shop.is_created' => 1,
			),
			'order' =>'UsersAuthority.order_rank ASC',
		));
		$this->Shop->unbindModel(array('belongsTo' => array('BookmarkShop')), false);

		// 地域絞りを付加する
		$this->init_model();

		return $favorite_shops;
	}

	// お気に入りの女の子 情報取得
	function _get_favorite_girl_data() {

		// 地域絞りを外す
		$this->remove_model_conditions();

//		$this->Girl->unbindModel(array('hasOne' => array('Review')));
		$this->Girl->bindModel(array('belongsTo' => array('BookmarkGirl' => array(
			'className'=>'BookmarkGirl',
			'type' => 'inner',
			'conditions' => array(
				'BookmarkGirl.reviewer_id' => $this->Auth->user('id'),
				'Girl.id = BookmarkGirl.girl_id',
			),
			'foreignKey' => false,))),false);

		$favorite_girls = $this->Girl->find('all', array(
			'fields' => array(
				'Girl.*','User.*','UsersAuthority.*','Shop.*','LargeArea.*','BookmarkGirl.created',
			),
			'conditions' => array(
				'Shop.is_created' => 1,
				'Girl.is_deleted' => 0,
			),
			'order' =>'UsersAuthority.order_rank ASC, Girl.show_order ASC',
		));
		$this->Girl->unbindModel(array('belongsTo' => array('BookmarkGirl')), false);

		// 地域絞りを付加する
		$this->init_model();

		return $favorite_girls;
	}

	// お気に入りのレビュー 情報取得
	function _get_favorite_review_data() {

		// 地域絞りを外す
		$this->remove_model_conditions();

		$this->Review->bindModel(array('belongsTo' => array('BookmarkReview' => array(
			'className'=>'BookmarkReview',
			'type' => 'inner',
			'conditions' => array(
				'BookmarkReview.reviewer_id' => $this->Auth->user('id'),
				'Review.id = BookmarkReview.review_id',
			),
			'foreignKey' => false,))),false);

		$conditions = array(
			'Shop.is_created' => 1,
		);
		$favorite_reviews = $this->_getReviews($conditions, 'Review.created DESC', -1, array('BookmarkReview.created'));
		$this->Review->unbindModel(array('belongsTo' => array('BookmarkReview')), false);

		// 地域絞りを付加する
		$this->init_model();

		return $favorite_reviews;
	}
}
?>
