<?php
class TopicController extends AppController {
	var $name = 'Topic';
	var $uses = array('Shop', 'Girl', 'Review');

	function beforeFilter() {
		parent::beforeFilter();

		switch($this->params['action'])
		{
			case 's_index':
				$this->addBreadCrumbs("イチオシのお店" , "/topic");
				break;
		}
	}


	// イチオシのお店情報を取得
	function s_index() {
		// イチオシのお店を取得
		$this->Shop->unbindModel(array('hasOne' => array('Review')));

		$topic_shop = $this->Shop->find('all', array(
			'conditions' => array(
				'Shop.is_created' => 1,
				'UsersAuthority.is_show_pickup_shop' => 1
			),
			'order' =>'rand()',
			'limit' => 15
		));

		foreach($topic_shop AS $ii => $record) {
			$girl_cnt = $this->Girl->find('count', array(
				'conditions' => array(
					'Girl.user_id' => $record['User']['id'],
				),
			));
			$topic_shop[$ii]['Girl']['cnt'] = $girl_cnt;

			$review_cnt = $this->Review->find('count', array(
				'conditions' => array(
					'Review.user_id' => $record['User']['id'],
				),
			));
			$topic_shop[$ii]['Review']['cnt'] = $review_cnt;
		}

		$this->set('topic_shop', $topic_shop);

		//head
		$this->set('title_for_layout', $this->title_tag_common . '新着お店激押し売れっ子');
		$this->set('meta_keywords', $this->meta_keywords_common . ',新着お店激押し売れっ子');
		$this->set('meta_description', $this->meta_description_common . '新着お店激押し売れっ子');
		$this->set('header_one', $this->h1_tag_common . '新着お店激押し売れっ子');
	}
}
?>
