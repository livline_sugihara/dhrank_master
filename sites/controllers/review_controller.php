<?php

class ReviewController extends AppController {
	var $name = 'Review';
	var $uses = array('Review','Reviewer','Shop', 'Girl','ReviewsReport','ReviewsGoodCount','MShopsBusinessCategory','MReviewsPlace','MReviewsReportsType');

	const SESSION_ENTRY_REGISTER_DATA_KEY = 'ReviewController.entry.register';
	const SESSION_ENTRY_ACCESS_CONDITION_KEY = 'ReviewController.entry.condition';

	const SESSION_REPORT_REGISTER_DATA_KEY = 'ReviewController.report.register';
	const SESSION_REPORT_ACCESS_CONDITION_KEY = 'ReviewController.report.condition';

	// 女の子、店舗選択両方あり
	const MODE_GIRL_SHOP = 'girl';
	// 女の子選択なし、店舗選択有り
	const MODE_SHOP = 'shop';
	// 女の子、店舗選択両方なし
	const MODE_FREE = 'free';

	function beforeFilter() {
		parent::beforeFilter();

		// レビュアー情報が無いとアクセス出来ない
		if(empty($this->parent_reviewer)) {
			$this->redirect($this->device_path . '/accounts/login');
			return;
		}

		switch($this->params['action'])
		{
			case 'search_shop':
				$this->addBreadCrumbs("口コミ投稿(店舗検索)" , "/review/search_shop");
				break;
			case 's_search_shop':
				$this->addBreadCrumbs("口コミ投稿(店舗検索)" , "/s/review/search_shop");
				break;
			case 'entry':
				$this->addBreadCrumbs("口コミ投稿" , "/review/entry");
				break;
			case 's_entry':
				$this->addBreadCrumbs("口コミ投稿" , "/s/review/entry");
				break;
			case 'report':
				$this->addBreadCrumbs("口コミ通報" , "/review/report");
				break;
			case 's_report':
				$this->addBreadCrumbs("口コミ通報" , "/s/review/report");
				break;
			case 'report_end':
				$this->addBreadCrumbs("口コミ通報(完了)" , "#");
				break;
			case 's_report_end':
				$this->addBreadCrumbs("口コミ通報(完了)" , "#");
				break;
		}
	}

	/**
	 * 店舗選択画面
	 */
	function search_shop(){

		$arrCondition = array(
			'UsersAuthority.large_area_id' => $this->parent_area['LargeArea']['id'],
		);

		// 検索されている場合
		if(!empty($this->data)) {
		
			if(!empty($this->data['Search']['category_id'])) {
				$arrCondition['Shop.shops_business_category_id'] = $this->data['Search']['category_id'];
			}
			if(!empty($this->data['Search']['shop_name'])) {
				$arrCondition['Shop.name LIKE'] = '%'. $this->data['Search']['shop_name'] . '%';
			}
		}

		$shop_list = $this->Shop->find('all', array(
			'conditions' => $arrCondition,
		));

		// 検索結果
		$this->set('shop_list', $shop_list);

		// カテゴリリスト
//		$this->MShopsBusinessCategory->unbindModel(array('belongsTo' => array('Shop', 'User', 'UsersAuthority')));
		$this->set('m_shops_business_categories',Set::Combine($this->MShopsBusinessCategory->find('all'),'{n}.MShopsBusinessCategory.id', '{n}.MShopsBusinessCategory.name'));

		//head
		$this->set('title_for_layout', $this->title_tag_common . '口コミ投稿選択');
		$this->set('meta_keywords', $this->meta_keywords_common . '口コミ投稿選択');
		$this->set('meta_description', $this->meta_description_common . '口コミ投稿選択');
		$this->set('header_one', $this->h1_tag_common . '口コミ投稿選択');
	}
		
	function s_search_shop() {
		$this->search_shop();
	}

	/**
	 * 口コミ投稿
	 */
	function entry(){

		$mode = isset($this->params['url']['mode']) ? $this->params['url']['mode'] : '';

		// 2回目以降の場合
		if(empty($mode)) {

			// セッションにもデータがない場合
			if(!$this->Session->check(self::SESSION_ENTRY_ACCESS_CONDITION_KEY)) {
				// アクセス不正でトップに戻す
				$this->redirect($this->device_path . '/top');
				return;
			}

			$session_data = $this->Session->read(self::SESSION_ENTRY_ACCESS_CONDITION_KEY);
			$mode = $session_data['mode'];
			$base_data = $session_data['base_data'];
			$shop_id = $session_data['shop_id'];
			$girl_id = $session_data['girl_id'];
		}
		// 初回アクセスの場合
		else
		{
			$shop_id = isset($this->params['url']['sid']) ? $this->params['url']['sid'] : '';
			$girl_id = isset($this->params['url']['gid']) ? $this->params['url']['gid'] : '';

			$base_data = array();

			// 女の子指定がある場合
			if(!empty($girl_id)) {
				$base_data = $this->Girl->find('first', array(
					'conditions' => array(
						'Girl.id' => $girl_id,
						'Girl.is_deleted' => 0,
					),
				));

				// 指定された女の子データがない場合
				if(empty($base_data)) {
					$base_data = array();
					$mode = self::MODE_SHOP;
				}
			}
			// 店舗指定がある場合
			if(empty($base_data) && !empty($shop_id)) {
				$base_data = $this->Shop->find('first', array(
					'conditions' => array(
						'Shop.id' => $shop_id,
					),
				));

				// 指定された女の子データがない場合
				if(empty($base_data)) {
					$base_data = array();
					$mode = self::MODE_FREE;
				}
			}

			// セッションに保存
			$session_data = array(
				'mode' => $mode,
				'base_data' => $base_data,
				'shop_id' => $shop_id,
				'girl_id' => $girl_id,
			);
			$this->Session->write(self::SESSION_ENTRY_ACCESS_CONDITION_KEY, $session_data);
		}

		// FORMに渡す
		$this->set('base_data', $base_data);
		$this->set('mode', $mode);

		// 場所リスト
		$m_reviews_place = Set::Combine($this->MReviewsPlace->find('all'),'{n}.MReviewsPlace.id', '{n}.MReviewsPlace.name');
		$this->set('m_reviews_place', $m_reviews_place);

		// 指名リスト
		$this->set('appointed_type', Review::$appointed_type);
		$this->set('appointed_sub', Review::$appointed_sub);

		// 得点リスト
		$this->set('score_list', Review::$score);

		//head
		$this->set('title_for_layout', $this->title_tag_common . '口コミ投稿');
		$this->set('meta_keywords', $this->meta_keywords_common . '口コミ投稿');
		$this->set('meta_description', $this->meta_description_common . '口コミ投稿');
		$this->set('header_one', $this->h1_tag_common . '口コミ投稿');

		// 登録の場合
		if (!empty($this->data)){

			// 投稿者情報
			$this->data['Review']['reviewer_id'] = $this->parent_reviewer['Reviewer']['id'];
			$this->data['Review']['reviewer_age_id'] = $this->parent_reviewer['Reviewer']['age_id'];
			$this->data['Review']['post_name'] = $this->parent_reviewer['Reviewer']['handle'];

			switch($mode) {
				case 'girl':
					$this->data['Review']['girl_id'] = $girl_id;
					$this->data['Review']['girl_name'] = $base_data['Girl']['name'];
					// breakなし
				case 'shop':
					$this->data['Review']['shop_id'] = $shop_id;
					$this->data['Review']['shop_name'] = $base_data['Shop']['name'];
					// breakなし
			}

			// 確認画面へ
			if(isset($this->params["form"]["confirm"])){

				$this->Review->set($this->data);
				if($this->Review->validates()){

					$this->set('data', $this->data);

					// 一旦Sessionに持つ
					$this->Session->write(self::SESSION_ENTRY_REGISTER_DATA_KEY, $this->data);

					// 確認画面をレンダリング
					$this->render($this->device_file_name . 'entry_confirm');
				}

			}
			// 入力画面へ
			else if(isset($this->params["form"]["return"]))
			{
				if($this->Session->check(self::SESSION_ENTRY_REGISTER_DATA_KEY)) {
					$this->data = $this->Session->read(self::SESSION_ENTRY_REGISTER_DATA_KEY);
				}

				//投稿画面へ戻る
				$this->set('data', $this->data);
			}
			// 登録処理へ
			else if(isset($this->params["form"]["register"]))
			{
				if($this->Session->check(self::SESSION_ENTRY_REGISTER_DATA_KEY)) {
					$this->data = $this->Session->read(self::SESSION_ENTRY_REGISTER_DATA_KEY);

					// 自動公開機能
					if(APP_REVIEW_AUTO_PUBLISH) {
						// 女の子・店舗共に登録されている場合
						if($mode == 'girl') {
							$this->data['Review']['is_publish'] = 1;
						}
					}

					//口コミ投稿
					//ipアドレス等登録
					$this->data['Review']['ip_address'] = $this->MediaInfo->getClientIP();
					$this->data['Review']['uid'] = $this->MediaInfo->getUid();
					$this->data['Review']['media'] = $this->MediaInfo->getMedia();
					$this->Review->save($this->data['Review']);

					$this->Session->delete(self::SESSION_ENTRY_REGISTER_DATA_KEY);
					$this->Session->delete(self::SESSION_ENTRY_ACCESS_CONDITION_KEY);

					// 自動公開ではない場合のみメール送信
					if(!APP_REVIEW_AUTO_PUBLISH) {

						switch($mode) {
							case self::MODE_GIRL_SHOP:
								$subject = '【' . $this->parent_area['LargeArea']['name'] . 'デリヘル口コミランキング】　口コミ投稿が行われました';
								$template = 'review_entry_admin_normal';
								break;
							case self::MODE_SHOP:
								$subject = '【' . $this->parent_area['LargeArea']['name'] . 'デリヘル口コミランキング】　口コミ投稿が行われました（女性無し）';
								$template = 'review_entry_admin_no_girl';
								break;
							case self::MODE_FREE:
								$subject = '【' . $this->parent_area['LargeArea']['name'] . 'デリヘル口コミランキング】　口コミ投稿が行われました（店・女性無し）';
								$template = 'review_entry_admin_free';
								break;
						}

						// メール送信()
						$mailOptions = array(
							'to' => VALID_POST_REVIEW,
							'from' => VALID_POST_REVIEW,
							'subject' => $subject,
							'smtpOptionsKey' => 'smtpOptionPostReview',
							'params' => array(
								'area_data' => $this->parent_area,
								'review' => $this->data['Review'],
								'base_data' => $base_data,
								'm_reviews_place' => $m_reviews_place,
							),
							'template' => $template,
						);
						$this->send_mail($mailOptions);
					}

					$this->redirect($this->device_path . '/review/entry_end');

				}
			}
		}
	}

	function s_entry() {
		$this->entry();
	}
	/**
	 * 投稿ページ完了
	 */
	function entry_end() {

		//head
		$this->set('title_for_layout', $this->title_tag_common . '口コミ投稿(完了)');
		$this->set('meta_keywords', $this->meta_keywords_common . '口コミ投稿(完了)');
		$this->set('meta_description', $this->meta_description_common . '口コミ投稿(完了)');
		$this->set('header_one', $this->h1_tag_common . '口コミ投稿(完了)');

	}
	function s_entry_end() {
		$this->entry_end();
	}

	/**
	 * 通報ページ
	 */
	function report() {

		if(!isset($this->params['review_id'])) {

			// セッションにもデータがない場合
			if(!$this->Session->check(self::SESSION_REPORT_ACCESS_CONDITION_KEY)) {
				// アクセス不正でトップに戻す
				$this->redirect($this->device_path . '/top');
				return;
			}

			$session_data = $this->Session->read(self::SESSION_REPORT_ACCESS_CONDITION_KEY);
			$objReviewData = $session_data['review_data'];

		} else {

			$objReviewData = $this->Review->find('first', array(
				'conditions' => array(
					'Review.id' => $this->params['review_id'],
				),
			));

			if(empty($objReviewData)) {
				$this->cakeError('error404');
			}

			$session_data = array(
				'review_data' => $objReviewData,
			);
			$this->Session->write(self::SESSION_REPORT_ACCESS_CONDITION_KEY, $session_data);
		}
		$this->set('review_data', $objReviewData);

		// 通報理由種別
		$m_reviews_reports_types = Set::Combine($this->MReviewsReportsType->find('all'),'{n}.MReviewsReportsType.id', '{n}.MReviewsReportsType.name');
		$this->set('m_reviews_reports_types', $m_reviews_reports_types);

		//head
		$this->set('title_for_layout', $this->title_tag_common . '口コミ通報');
		$this->set('meta_keywords', $this->meta_keywords_common . '口コミ通報');
		$this->set('meta_description', $this->meta_description_common . '口コミ通報');
		$this->set('header_one', $this->h1_tag_common . '口コミ通報');

		// 登録の場合
		if (!empty($this->data)){

			// 確認画面へ
			if(isset($this->params["form"]["confirm"])){

				$this->ReviewsReport->set($this->data);
				if($this->ReviewsReport->validates()) {

					// 一旦Sessionに持つ
					$this->Session->write(self::SESSION_REPORT_REGISTER_DATA_KEY, $this->data);

					// 確認画面をレンダリング
					$this->render($this->device_file_name . 'report_confirm');
				}

			} else if(isset($this->params["form"]["register"])) {

				if($this->Session->check(self::SESSION_REPORT_REGISTER_DATA_KEY)) {

					$save_data = $this->Session->read(self::SESSION_REPORT_REGISTER_DATA_KEY);

					//口コミ通報
					$save_data['ReviewsReport']['report_reviewer_id'] = $this->parent_reviewer['Reviewer']['id'];
					$save_data['ReviewsReport']['ip_address'] = $this->MediaInfo->getClientIP();
					$save_data['ReviewsReport']['uid'] = $this->MediaInfo->getUid();
					$save_data['ReviewsReport']['media'] = $this->MediaInfo->getMedia();
					$this->ReviewsReport->save($save_data['ReviewsReport']);

					$subject = '【' . $this->parent_area['LargeArea']['name'] . 'デリヘル口コミランキング】　口コミ通報が行われました';
					$template = 'review_report_admin';

					// メール送信()
					$mailOptions = array(
						'to' => VALID_REVIEW_REPORT,
						'from' => VALID_REVIEW_REPORT,
						'subject' => $subject,
						'smtpOptionsKey' => 'smtpOptionReviewReport',
						'params' => array(
							'area_data' => $this->parent_area,
							'm_reviews_reports_types' => $m_reviews_reports_types,
							'review_report' => $save_data['ReviewsReport'],
							'review' => $objReviewData['Review'],
						),
						'template' => $template,
					);
					$this->send_mail($mailOptions);

					// 更新完了画面に遷移
					$this->redirect($this->device_path . '/review/report_end');
					return;
				}
				
			} else if(isset($this->params["form"]["return"])) {

				if($this->Session->check(self::SESSION_REPORT_REGISTER_DATA_KEY)) {
					$this->data = $this->Session->read(self::SESSION_REPORT_REGISTER_DATA_KEY);
				}

			}
		}
	}
	function s_report() {
		$this->report();
	}
	
	
	/**
	 * 通報ページ完了
	 */
	function report_end() {

		//head
		$this->set('title_for_layout', $this->title_tag_common . '口コミ通報(完了)');
		$this->set('meta_keywords', $this->meta_keywords_common . '口コミ通報(完了)');
		$this->set('meta_description', $this->meta_description_common . '口コミ通報(完了)');
		$this->set('header_one', $this->h1_tag_common . '口コミ通報(完了)');

	}
	function s_report_end() {
		$this->report_end();
	}
	
	/**
	 * 店舗毎の女の子選択ダイアログ用Ajax応答関数
	 */
	function ajax_get_girls() {
		$this->autoRender = true;
		$this->layout = false;
		Configure::write('debug',0);

		if(!empty($this->data)) {
			$shop_id = $this->data['Shop']['shop_id'];

			$girl_list = $this->Girl->find('all', array(
				'conditions' => array(
					'Shop.id' => $shop_id,
					'Girl.is_deleted' => 0,
				),
				'order' => 'Girl.show_order'
			));
			$this->set('girl_list', $girl_list);
			$this->set('shop_id', $shop_id);
		}
	}
	function s_ajax_get_girls() {
		$this->ajax_get_girls();
	}

	function s_select_girls($shop_id) {

		$girl_list = $this->Girl->find('all', array(
			'conditions' => array(
				'Shop.id' => $shop_id,
				'Girl.is_deleted' => 0,
			),
			'order' => 'Girl.show_order'
		));
		$this->set('girl_list', $girl_list);
		$this->set('shop_id', $shop_id);
	}
}
?>
