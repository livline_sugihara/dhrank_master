<?php
class FrontController extends AppController {
	var $name = 'Front';
	var $uses = array('Shop');

	function index(){

/*
		//かな読み込み、セット
		$kana_list = Configure::read('kana');
		$this->set('kana_list',$kana_list);

		//ショップデータに口コミデータをバインド
		$bind = array('hasOne' => array('Review' => array(
				'className'=>'Review',
				'fields' => array('COUNT(Review.id) AS reviews_count'),
				'conditions' => 'Review.user_id = User.id',
				'foreignKey' => false)));
		//あかさたな別にショプデータ取得
		foreach($kana_list as $key => $value){
			$this->Shop->bindModel($bind,false);
			$shops = $this->Shop->find('all',array(
					'fields' => array('Shop.name','MShopsBusinessCategory.name', 'User.id', 'LargeArea.url','COUNT(Review.id) AS reviews_count'),
					'conditions' => array(
							$this->getCurrentAreaCondition(),
							'SUBSTRING(User.kana,1,1)' => $value,),
					'group' => array('Shop.user_id'),
					'order' => 'User.kana ASC'));
			$list[$key] = $shops;
		}
		$this->set('list',$list);
*/
		$this->get_shops_index_common();
	}

	//start---2013/3/13 障害No.1-6-0003修正
	function s_index(){
		$this->redirect('/s/top');
	}
	//end---2013/3/13 障害No.1-6-0003修正
	
	function about(){
		$this->render('about');
	}
	
	function s_about(){
		$this->render('s_about');
	}
	
	function entrance(){
		$this->render('entrance');
	}
	
	function term(){
		$this->render('term');
	}
	
	function s_term(){
		$this->render('s_term');
	}
	
	function tokyo(){
		$this->render('tokyo');
	}

	//meta取得
	private function get_shops_index_common(){
		//head
		$this->set('title_for_layout', $this->title_tag_common . 'お店一覧');
		$this->set('meta_keywords', $this->meta_keywords_common .  ',お店一覧');
		$this->set('meta_description', $this->meta_description_common . 'お店一覧');
		$this->set('header_one', $this->h1_tag_common . 'お店一覧');
	}
}
?>
