<?php
class RecommendController extends AppController {
	var $name = 'Recommend';
	var $uses = array('Shop', 'User');

	function beforeFilter() {
		parent::beforeFilter();

		switch($this->params['action'])
		{
			case 'shop':
				$this->addBreadCrumbs("おすすめ店舗" , "/recommend/shop");
				break;
			case 'girl':
				$this->addBreadCrumbs("新着お店激押し売れっ子" , "/recommend/girl");
				break;
			case 's_shop':
				$this->addBreadCrumbs("おすすめ店舗" , "/recommend/shop");
				break;
			case 's_girl':
				$this->addBreadCrumbs("新着お店激押し売れっ子" , "/recommend/girl");
				break;
		}
	}

	function index(){
/*
		//かな読み込み、セット
		$kana_list = Configure::read('kana');
		$this->set('kana_list',$kana_list);

		//ショップデータに口コミデータをバインド
		$bind = array('hasOne' => array('Review' => array(
				'className'=>'Review',
				'fields' => array('COUNT(Review.id) AS reviews_count'),
				'conditions' => 'Review.user_id = User.id',
				'foreignKey' => false)));
		//あかさたな別にショプデータ取得
		foreach($kana_list as $key => $value){
			$this->Shop->bindModel($bind,false);
			$shops = $this->Shop->find('all',array(
					'fields' => array('Shop.name','MShopsBusinessCategory.name', 'User.id', 'LargeArea.url','COUNT(Review.id) AS reviews_count'),
					'conditions' => array(
							$this->getCurrentAreaCondition(),
							'SUBSTRING(User.kana,1,1)' => $value,),
					'group' => array('Shop.user_id'),
					'order' => 'User.kana ASC'));
			$list[$key] = $shops;
		}
		$this->set('list',$list);
*/
		$this->get_shops_index_common();
	}
	
	function s_index(){
		$this->redirect('/s/top');
	}

	function shop(){

		// オススメ店舗情報は共通で取ってあるので、再取得しない

		//head
		$this->set('title_for_layout', $this->title_tag_common . 'おすすめ店舗');
		$this->set('meta_keywords', $this->meta_keywords_common . ',おすすめ店舗');
		$this->set('meta_description', $this->meta_description_common . 'おすすめ店舗');
		$this->set('header_one', $this->h1_tag_common . 'おすすめ店舗');
	}
		
	function s_shop() {
		$this->shop();
	}
	
	function girl(){
		
		// 新着お店激押し売れっ子(ランダムピック)
		$result = $this->Girl->find('all', array(
			'conditions' => array(
				'UsersAuthority.large_area_id' => $this->parent_area['LargeArea']['id'],
				'UsersAuthority.is_recommend_girl' => 1,
				'Girl.recommend_type_id >' => 0,
				'Girl.is_deleted' => 0,
			),
			'order' => 'UsersAuthority.order_rank ASC, User.id ASC, Girl.show_order'
		));

		$cnt = count($result);
		$recommend_girls = array();
		$arrShop = null;
		$prevShopId = null;
		for($ii = 0; $ii < $cnt; $ii++) {

			$record = $result[$ii];

			if($prevShopId != $record['Shop']['id']) {

				if(!empty($arrShop)) {
					$recommend_girls[] = $arrShop;
				}

				// 初期化する
				$arrShop = array(
					'User' => $record['User'],
					'Shop' => $record['Shop'],
					'Girls' => array(),
					'Review' => array('cnt' => $this->Review->find('count', array(
						'conditions' => array(
							'Review.user_id' => $record['User']['id'],
						),
					))),
				);
				$prevShopId = $record['Shop']['id'];
			}
			if(count($arrShop['Girls'] < 6)) {
				$arrShop['Girls'][] = $record['Girl'];
			}
			
			// 最終要素の場合	
			if($ii == $cnt - 1) {
				if(!empty($arrShop)) {
					$recommend_girls[] = $arrShop;
				}
			}
		}

		$this->set('recommend_girls', $recommend_girls); 

		//head
		$this->set('title_for_layout', $this->title_tag_common . '新着お店激押し売れっ子');
		$this->set('meta_keywords', $this->meta_keywords_common . ',新着お店激押し売れっ子');
		$this->set('meta_description', $this->meta_description_common . '新着お店激押し売れっ子');
		$this->set('header_one', $this->h1_tag_common . '新着お店激押し売れっ子');
	}
		
	function s_girl() {
//		$this->girl();

		// 新着お店激押し売れっ子(ランダムピック)
		$this->User->bindModel(array(
			'hasOne' => array('Shop' => array(
				'className'=>'Shop',
				'type' => 'INNER',
				'conditions' => array(
					'Shop.is_created' => 1,
				),
				'foreignKey' => 'user_id',
			)),

			'hasMany' => array('Girl' => array(
				'className'=>'Girl',
				'fields' => array('Girl.*'),
				'conditions' => array(
					'Girl.recommend_type_id >' => 0,
					'Girl.is_deleted' => 0,
				),
				'order' => 'Girl.show_order',
				'foreignKey' => 'user_id',
				'limit' => 6,
			),
		)),false);
/*
		$result = $this->User->find('all', array(
			'fields' => array(
				'Shop.*', 'User.*', 'UsersAuthority.*', // 'Girl.*'
			),
			'conditions' => array(
				// 'Shop.is_created' => 1,
				'UsersAuthority.large_area_id' => $this->parent_area['LargeArea']['id'],
				'UsersAuthority.is_recommend_girl' => 1,
			),
			'order' => 'UsersAuthority.order_rank ASC, User.id ASC',
		));
*/
		$this->paginate = array('User' => array(
			'fields' => array(
				'Shop.*', 'User.*', 'UsersAuthority.*', // 'Girl.*'
			),
			'conditions' => array(
				'UsersAuthority.large_area_id' => $this->parent_area['LargeArea']['id'],
				'UsersAuthority.is_recommend_girl' => 1,
			),
			'order' => 'UsersAuthority.order_rank ASC, User.id ASC',
			'limit' => 5,
		));
		$result = $this->paginate($this->User);

		foreach($result AS $ii => $record) {
			$result[$ii]['Review'] = array(
				'cnt' => $this->Review->find('count', array('conditions' => array(
					'Review.user_id' => $record['User']['id'],
				))),
			);
		}

		$this->set('recommend_girls', $result); 

		//head
		$this->set('title_for_layout', $this->title_tag_common . '新着お店激押し売れっ子');
		$this->set('meta_keywords', $this->meta_keywords_common . ',新着お店激押し売れっ子');
		$this->set('meta_description', $this->meta_description_common . '新着お店激押し売れっ子');
		$this->set('header_one', $this->h1_tag_common . '新着お店激押し売れっ子');

	}
}
?>
