<?php
class UsersController extends UserController {
	var $name = 'Users';
	var $uses = array('User','Shop','Girl', 'Coupon', 'Review','MReviewsAge','MNumericValue','Review','ReviewsGoodCount','VetoIpAddressReview','VetoUidReview','System','BookmarkShop','BookmarkGirl');

	var $device_render_path = '';

	function beforeFilter() {
		parent::beforeFilter();

		// 店舗画面はエリアで絞らない
		$this->remove_model_conditions();

		$urlBase = $this->get_device_path() . '/' . $this->parent_shop['User']['id'];

		// パンくずに基本情報を設定
		$this->addBreadCrumbs($this->parent_shop['Shop']['name'], $urlBase);

		switch($this->params['action'])
		{
			case 'index':
				break;
			case 'girls':
				$this->addBreadCrumbs("女の子一覧", $urlBase . '/girls');
				break;
			case 'girl':
				$this->addBreadCrumbs("女の子プロフィール", $urlBase . '/girls/' . $this->params['girl_id']);
				break;
			case 'coupons':
				$this->addBreadCrumbs("割引クーポン", $urlBase . '/coupons');
				break;
			case 'systems':
				$this->addBreadCrumbs("システム", $urlBase . '/systems');
				break;
			case 'reviews':
				$this->addBreadCrumbs("口コミ一覧" , $urlBase . "/reviews");
				break;
			case 'review':
				$this->addBreadCrumbs("口コミ" , "#");
				break;
			case 's_index':
				break;
			case 's_girls':
				$this->addBreadCrumbs("女の子一覧", $urlBase . '/girls');
				break;
			case 's_girl':
				$this->addBreadCrumbs("女の子プロフィール", $urlBase . '/girls/' . $this->params['girl_id']);
				break;
			case 's_coupons':
				$this->addBreadCrumbs("割引クーポン", $urlBase . '/coupons');
				break;
			case 's_systems':
				$this->addBreadCrumbs("システム", $urlBase . '/systems');
				break;
			case 's_reviews':
				$this->addBreadCrumbs("口コミ一覧" , $urlBase . "/reviews");
				break;
			case 's_review':
				$this->addBreadCrumbs("口コミ" , "#");
				break;
		}
	}

	//店舗情報詳細画面
	function index(){

		//口コミ情報取得
		$conditions = array(
			'Shop.is_created' => 1,
			'Review.user_id' => $this->params['user_id'],
		);
		$new_shop_review = $this->_getReviews($conditions, 'Review.created DESC', 100);

		$this->set('new_shop_review',$new_shop_review);
/*
		$this->paginate = array('Review' => array(
				'fields' => array(
					'LargeArea.url',
					'User.id',
					'Girl.id','Girl.name','Girl.age','Girl.image_1_s','Girl.image_1_m','Girl.image_1_l',
					'Review.id','Review.comment','Review.created','Review.post_name','Review.score_girl_character','Review.score_girl_service','Review.score_girl_looks',
						'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg',
					'Shop.name',
					'Reviewer.handle', 'Reviewer.age', ),
				'conditions' => array(
						$this->getCurrentAreaCondition(),
						'Review.user_id' => $this->params['user_id'],
				),
				'group' => array('Review.id'),
				'order' => array('Review.created DESC'),
				'limit' => $this->pcount['users_top_reviews']));
		$this->set('new_review',$this->paginate($this->Review));
*/

		// 激押し売れっ子(最大6件)取得
		// TODO 権限によって取得しなくても良い場合あり
		$recommendGirls = $this->Girl->find('all', array(
			'conditions' => array(
				'Girl.user_id' => $this->parent_shop['Shop']['user_id'],
				'Girl.is_deleted' => 0,
				'Girl.recommend_type_id > 0'
			),
			'order' => array('Girl.created DESC'),
			'limit' => 6
		));
		$this->set('recommend_girls', $recommendGirls);

		// クーポン情報
		// TODO 権限によって取得しなくても良い場合あり
		$coupons = $this->Coupon->find('all', array(
			'conditions' => array(
				'Coupon.user_id' => $this->parent_shop['User']['id'],
				'OR' => array(
					'Coupon.expire_unlimited' => 1,
					'Coupon.expire_date >=' => date('Y-m-d H:i:s')
				)
			),
			'order' => array(
				'Coupon.expire_unlimited DESC',
				'Coupon.expire_date ASC',
			),
			'limit' => 2
		));
		$this->set('coupons', $coupons);
		$this->set('m_target', Coupon::$targetList);
		$this->set('m_target_caption_list', Coupon::$targetCaptionList);

		//head
		$this->set('title_for_layout', $this->title_tag_common . $this->parent_shop['Shop']['name']);
		$this->set('meta_keywords', $this->meta_keywords_common . ',' . $this->parent_shop['Shop']['name']);
		$this->set('meta_description', $this->meta_description_common . $this->parent_shop['Shop']['name']);
		$this->set('header_one', $this->h1_tag_common . $this->parent_shop['Shop']['name']);

	}
	function i_index() {
		$this->index();
	}
	function s_index() {
		$this->index();
	}

	//店舗情報口コミ一覧画面
	function reviews(){

		// ピックアップ口コミ情報取得
		$pic_review = array();
		if($this->parent_shop['UsersAuthority']['is_pickup_review'] == 1) {
			$conditions = array(
				$this->getCurrentAreaConditionForReview(),
				'Review.pickup_shop' => 1,
				'User.id' => $this->parent_shop['User']['id'],
			);
			$pic_review = $this->_getReviews($conditions, 'rand()', 2);
		}
		$this->set('pic_review', $pic_review);

		//口コミ情報取得（全件取得）
		$conditions = array(
			$this->getCurrentAreaConditionForReview(),
			'Review.user_id' => $this->params['user_id'],
		);
		$shop_review = $this->_getReviews($conditions, 'Review.created DESC');
		$this->set('shop_review', $shop_review);

		//head
		$this->set('title_for_layout', $this->title_tag_common . $this->parent_shop['Shop']['name'].' ' .'口コミ一覧');
		$this->set('meta_keywords', $this->meta_keywords_common . ',' . $this->parent_shop['Shop']['name']. ',' .'口コミ一覧');
		$this->set('meta_description', $this->meta_description_common . $this->parent_shop['Shop']['name'].' ' .'口コミ一覧');
		$this->set('header_one', $this->h1_tag_common . $this->parent_shop['Shop']['name'].' ' .'口コミ一覧');
	}
	function s_reviews() {

		// ピックアップ口コミ情報取得
		$pic_review = array();
		if($this->parent_shop['UsersAuthority']['is_pickup_review'] == 1) {
			$conditions = array(
				$this->getCurrentAreaConditionForReview(),
				'Review.pickup_shop' => 1,
				'User.id' => $this->parent_shop['User']['id'],
			);
			$pic_review = $this->_getReviews($conditions, 'rand()', 2);
		}
		$this->set('pic_review', $pic_review);

		//口コミ情報取得（全件取得）
		$conditions = array(
			$this->getCurrentAreaConditionForReview(),
			'Review.user_id' => $this->params['user_id'],
		);
		$shop_review = $this->_getReviewsPagenate($conditions, 'Review.created DESC');
		$this->set('shop_review', $shop_review);

		//head
		$this->set('title_for_layout', $this->title_tag_common . $this->parent_shop['Shop']['name'].' ' .'口コミ一覧');
		$this->set('meta_keywords', $this->meta_keywords_common . ',' . $this->parent_shop['Shop']['name']. ',' .'口コミ一覧');
		$this->set('meta_description', $this->meta_description_common . $this->parent_shop['Shop']['name'].' ' .'口コミ一覧');
		$this->set('header_one', $this->h1_tag_common . $this->parent_shop['Shop']['name'].' ' .'口コミ一覧');
	}

	//口コミ
	function review(){

		//存在しない口コミの場合はエラー画面へ
		$review_present = $this->Review->find('first',array(
			'conditions' => array(
				'User.id' => $this->params['user_id'],
				'Review.id' => $this->params['review_id'],
				'Review.is_publish' => 1,
				'Review.delete_flag <=' => Review::DELETE_FLAG_COMMENT_ONLY,
			)));
		if(empty($review_present)){
			$this->cakeError('error404');
		}

		// レビュー情報を取得
		$conditions = array(
			'Shop.is_created' => 1,
			'Review.is_publish' => 1,
			'Review.delete_flag <=' => Review::DELETE_FLAG_COMMENT_ONLY,
			'Review.girl_id' => $review_present['Review']['girl_id'],
		);
		$reviews = $this->_getReviews($conditions, 'Review.created DESC');

		$pre_review = null;
		$current_review = null;
		$next_review = null;
		$order_num = 0;
		foreach($reviews AS $ii => $record) {

			$current_review = $record;

			if($current_review['Review']['id'] == $this->params['review_id']) {
				if(count($reviews) > $ii + 1) {
					$next_review = $reviews[$ii + 1];
				}
				$order_num = $ii + 1;
				break;
			}

			if(count($reviews) > $ii + 2) {
				$pre_review = $record;
			}
		}

		// $conditions = array(
		// 	'Shop.is_created' => 1,
		// 	'Review.id' => $this->params['review_id'],
		// 	'Review.user_id' => $this->params['user_id'],
		// );
		// $review = $this->_getReviews($conditions, 'Review.created DESC', 1);

		$this->set('all_reviews', $reviews);
		$this->set('review', $current_review);
		$this->set('back_review', $pre_review);
		$this->set('forward_review', $next_review);
		$this->set('order_num', $order_num);

/*
		//start---2013/4/30 障害No.4-0002修正
		$reviews_good_count = $this->ReviewsGoodCount->find('first',array(
			'fields' => array('ifnull(count(ReviewsGoodCount.id),0) as reviews_good_count_sum'),
			'conditions' => array(
				'Review.id' => $this->params['review_id'],
				'Review.user_id' => $this->params['user_id'],
			),
			'gruop' => 'Review.id',
		));
		$this->set('reviews_good_count',$reviews_good_count);
		//end---2013/4/30 障害No.4-0002修正

		$girl = $this->Girl->find('first',array(
			'conditions' => array(
				$this->getCurrentAreaCondition(),
				'Girl.id' => $review['Girl']['id'],
				'Girl.is_deleted' => 0,
				'Girl.user_id' => $this->params['user_id'],
			),));
		$this->set('girl',$girl);

		//ブックマーク
		$this->set('bookmark_girl', $this->BookmarkGirl->find('count', array('conditions' => array('reviewer_id' => $this->Auth->user('id'), 'girl_id' => $review['Girl']['id']))));
*/

		//head
		$this->set('title_for_layout', $this->title_tag_common . $this->parent_shop['Shop']['name'].' ' . $current_review['Girl']['name'].'さんの口コミ');
		$this->set('meta_keywords', $this->meta_keywords_common . ',' . $this->parent_shop['Shop']['name']. ',' . $current_review['Girl']['name'] );
		$this->set('meta_description', $this->meta_description_common . $this->parent_shop['Shop']['name'].' ' . $current_review['Girl']['name'].'さんの口コミ');
		$this->set('header_one', $this->h1_tag_common . $this->parent_shop['Shop']['name'].' ' . $current_review['Girl']['name'].'さんの口コミ');
	}
	function s_review() {
		$this->review();

		// その他の女の子
		$girls = $this->Girl->find('all', array(
			'conditions' => array(
				'Girl.user_id' => $this->params['user_id'],
				'Girl.is_deleted' => 0,
				'Girl.id != (SELECT girl_id FROM reviews WHERE id = ' . $this->params['review_id'] . ' LIMIT 1)',
			),
			'order' => 'Girl.show_order ASC, Girl.created ASC',
			'group' => 'Girl.id'
		));
		$this->set('other_girls', $girls);
	}

	//店舗別女の子
	function girls(){
		//レビュー情報BIND
		$this->Girl->bindModel(array('hasOne' => array('Review' => array(
				'className'=>'Review',
				'fields' => array('ifnull(count(Review.id),0) as count'),
				'conditions' => array('Review.girl_id = Girl.id'),
				'foreignKey' => false,))),false);
		//ブックマーク情報BIND
		$this->Girl->bindModel(array('hasOne' => array('BookmarkGirl' => array(
				'className'=>'BookmarkGirl',
				'conditions' => array(
						'BookmarkGirl.girl_id = Girl.id',
						'BookmarkGirl.reviewer_id' => $this->Auth->user('id')),
				'foreignKey' => false,))),false);

		$girls = $this->Girl->find('all', array(
			'conditions' => array(
				'Girl.user_id' => $this->params['user_id'],
				'Girl.is_deleted' => 0,
			),
			'order' => 'Girl.show_order ASC, Girl.created ASC',
			'group' => 'Girl.id'
		));
		$this->set('girls', $girls);
		//激押し売れっ子追加
		$recommend_girls = $this->Girl->find('all', array(
			'conditions' => array(
				'Girl.user_id' => $this->params['user_id']
				// 'Girl.is_deleted' => 0
				// 'NOT' => array('Girl.recommend_type_id' => 0)
			),
			'group' => 'Girl.id'
		));
		$this->set('recommend_girls', $recommend_girls);

/*
		//女の子情報取得（ページング）
		$this->paginate = array('Girl' => array(
				'conditions' => array('Girl.user_id' => $this->params['user_id']),
				'order' => 'Girl.show_order ASC, Girl.created ASC',
				'group' => 'Girl.id',
				'limit' => $this->pcount['users_girls']));
		$this->set('girls',$this->paginate('Girl'));
*/

		//head
		$this->set('title_for_layout', $this->title_tag_common . $this->parent_shop['Shop']['name'].' ' .'女の子一覧');
		$this->set('meta_keywords', $this->meta_keywords_common . ',' . $this->parent_shop['Shop']['name']. ',' .'女の子一覧');
		$this->set('meta_description', $this->meta_description_common . $this->parent_shop['Shop']['name'].' ' .'女の子一覧');
		$this->set('header_one', $this->h1_tag_common . $this->parent_shop['Shop']['name'].' ' .'女の子一覧');

	}
	function i_girls(){
		$this->girls();
	}
	function s_girls(){
		$this->girls();
	}

	//女の子情報詳細画面
	function girl() {

		//存在しない女の子の場合はエラー画面へ
		$girl_present = $this->Girl->find('count',array(
			'conditions' => array(
				'Shop.is_created' => 1,
				'Girl.is_deleted' => 0,
				'User.id' => $this->params['user_id'],
				'Girl.id' => $this->params['girl_id']
			)
		));

		if($girl_present == 0){
			$this->cakeError('error404');
		}

		// 女の子情報を取得
		$girl = $this->Girl->find('first',array(
			'conditions' => array(
				'Shop.is_created' => 1,
				'Girl.id' => $this->params['girl_id'],
				'Girl.is_deleted' => 0,
				'Girl.user_id' => $this->params['user_id'],
			),
		));
		$this->set('girl', $girl);

		// レビュー情報を取得
		$conditions = array(
			'Shop.is_created' => 1,
			'Review.girl_id' => $this->params['girl_id'],
			'Review.user_id' => $this->params['user_id'],
		);
		$reviews = $this->_getReviews($conditions, 'Review.created DESC');
		$this->set('reviews',$reviews);

		// その他の女の子
		$girls = $this->Girl->find('all', array(
			'conditions' => array(
				'Girl.user_id' => $this->params['user_id'],
				'Girl.is_deleted' => 0,
				'Girl.id !=' => $this->params['girl_id']
			),
			'order' => 'Girl.show_order ASC, Girl.created ASC',
			'group' => 'Girl.id'
		));
		$this->set('other_girls', $girls);


		//head
		$this->set('title_for_layout', $this->title_tag_common . $this->parent_shop['Shop']['name'].' ' .$girl['Girl']['name'].'さんプロフィール');
		$this->set('meta_keywords', $this->meta_keywords_common . ',' . $this->parent_shop['Shop']['name']. ',' .$girl['Girl']['name']. ','. 'プロフィール' );
		$this->set('meta_description', $this->meta_description_common . $this->parent_shop['Shop']['name'].' ' .$girl['Girl']['name'].'さんプロフィール');
		$this->set('header_one', $this->h1_tag_common . $this->parent_shop['Shop']['name'].' ' .$girl['Girl']['name'].'さんプロフィール');
	}

	function s_girl() {
		//存在しない女の子の場合はエラー画面へ
		$girl_present = $this->Girl->find('count',array(
			'conditions' => array(
				'Shop.is_created' => 1,
				'Girl.is_deleted' => 0,
				'User.id' => $this->params['user_id'],
				'Girl.id' => $this->params['girl_id']
			)
		));

		if($girl_present == 0){
			$this->cakeError('error404');
		}

		// 女の子情報を取得
		$girl = $this->Girl->find('first',array(
			'conditions' => array(
				'Shop.is_created' => 1,
				'Girl.id' => $this->params['girl_id'],
				'Girl.is_deleted' => 0,
				'Girl.user_id' => $this->params['user_id'],
			),
		));
		$this->set('girl', $girl);

		// レビュー情報を取得
		$conditions = array(
			'Shop.is_created' => 1,
			'Review.girl_id' => $this->params['girl_id'],
			'Review.user_id' => $this->params['user_id'],
		);
		$reviews = $this->_getReviewsPagenate($conditions, 'Review.created DESC', 5);
		$this->set('reviews',$reviews);

		// その他の女の子
		$girls = $this->Girl->find('all', array(
			'conditions' => array(
				'Girl.user_id' => $this->params['user_id'],
				'Girl.is_deleted' => 0,
				'Girl.id !=' => $this->params['girl_id']
			),
			'order' => 'Girl.show_order ASC, Girl.created ASC',
			'group' => 'Girl.id'
		));
		$this->set('other_girls', $girls);


		//head
		$this->set('title_for_layout', $this->title_tag_common . $this->parent_shop['Shop']['name'].' ' .$girl['Girl']['name'].'さんプロフィール');
		$this->set('meta_keywords', $this->meta_keywords_common . ',' . $this->parent_shop['Shop']['name']. ',' .$girl['Girl']['name']. ','. 'プロフィール' );
		$this->set('meta_description', $this->meta_description_common . $this->parent_shop['Shop']['name'].' ' .$girl['Girl']['name'].'さんプロフィール');
		$this->set('header_one', $this->h1_tag_common . $this->parent_shop['Shop']['name'].' ' .$girl['Girl']['name'].'さんプロフィール');
	}

	//店舗別女の子
	function coupons(){

		// クーポン情報
		$coupons = $this->Coupon->find('all', array(
			'conditions' => array(
				'Coupon.user_id' => $this->parent_shop['User']['id'],
				'OR' => array(
					'Coupon.expire_unlimited' => 1,
					'Coupon.expire_date >=' => date('Y-m-d H:i:s')
				)
			),
			'order' => array(
				'Coupon.expire_unlimited DESC',
				'Coupon.expire_date ASC',
			),
			'limit' => 6
		));
		$this->set('coupons', $coupons);
		$this->set('m_target', Coupon::$targetList);
		$this->set('m_target_caption_list', Coupon::$targetCaptionList);

		//head
		$this->set('title_for_layout', $this->title_tag_common . $this->parent_shop['Shop']['name'].' ' .'クーポン一覧');
		$this->set('meta_keywords', $this->meta_keywords_common . ',' . $this->parent_shop['Shop']['name']. ',' .'クーポン一覧');
		$this->set('meta_description', $this->meta_description_common . $this->parent_shop['Shop']['name'].' ' .'クーポン一覧');
		$this->set('header_one', $this->h1_tag_common . $this->parent_shop['Shop']['name'].' ' .'クーポン一覧');

	}
	function s_coupons(){
		$this->coupons();
	}

	//システム
	function systems(){

		$this->set('system', $this->System->find('first', array(
			'conditions' => array(
				'System.user_id' => $this->params['user_id']
			)
		)));

		//表示用
//		$this->set('change', Configure::read('change'));
//		$this->set('cancel', Configure::read('cancel'));
//		$this->set('receipt', Configure::read('receipt'));

		//head
		$this->set('title_for_layout', $this->title_tag_common . $this->parent_shop['Shop']['name'].' ' .'料金・システムページ');
		$this->set('meta_keywords', $this->meta_keywords_common . ',' . $this->parent_shop['Shop']['name'] .','.'料金,システム');
		$this->set('meta_description', $this->meta_description_common . $this->parent_shop['Shop']['name'].' ' .'料金・システムページ');
		$this->set('header_one', $this->h1_tag_common . $this->parent_shop['Shop']['name'].' ' .'料金・システム');
	}
	function i_systems() {
		$this->systems();
	}
	function s_systems() {
		$this->systems();
	}
}
?>