<?php
class NewestController extends AppController {
	var $name = 'Newest';
	var $uses = array('Shop');

	function beforeFilter() {
		parent::beforeFilter();

		switch($this->params['action'])
		{
			case 'index':
				$this->addBreadCrumbs("新着口コミ" , "/newest");
				break;
			case 's_index':
				$this->addBreadCrumbs("新着口コミ" , "/s/newest");
				break;
		}
	}

	function index(){
		//口コミ情報取得
		$this->get_newest_review();

		//head
		$this->set('title_for_layout', $this->title_tag_common . 'お店一覧');
		$this->set('meta_keywords', $this->meta_keywords_common .  ',お店一覧');
		$this->set('meta_description', $this->meta_description_common . 'お店一覧');
		$this->set('header_one', $this->h1_tag_common . 'お店一覧');
	}

	function s_index() {
		$this->index();
	}

}
?>
