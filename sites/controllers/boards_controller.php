<?php

class BoardsController extends AppController {
	var $name = 'Boards';
	var $uses = array('Board','BoardRes','VetoIpAddressBoard','VetoUidBoard');
	var $components = array('MediaInfo');

	//掲示板TOP
	function index(){

		$error_bid = 0;

		//掲示板モデルが空でレスモデルが空じゃない
		if(empty($this->data['Board']) && !empty($this->data['BoardRes'])){

			//投稿禁止IPアドレス判定
			if($this->VetoIpAddressBoard->find('count', array('conditions' => array('VetoIpAddressBoard.name' => $this->MediaInfo->getClientIP()))) != 0){
				$this->redirect($this->device_path . '/boards/error');
			}
			//投稿禁止個体識別番号判定
			if($this->VetoUidBoard->find('count', array('conditions' => array('VetoUidBoard.name' => $this->MediaInfo->getUid()))) != 0){
				$this->redirect($this->device_path . '/boards/error');
			}

			//レス投稿
			$this->BoardRes->set($this->data['BoardRes']);
			if($this->BoardRes->validates()){

				//レス保存
				$maxRes = $this->BoardRes->find('first', array(
						'fields' => 'MAX(sub_id) as sub_id',
						'conditions' => array('BoardRes.board_id' => $this->data['BoardRes']['board_id']),
						'group' => 'BoardRes.board_id',
				));
				$this->BoardRes->create();
				$this->BoardRes->set($this->data['BoardRes']);
				$this->BoardRes->sub_id = $maxRes[0]['sub_id']+1;
				$this->BoardRes->ip_address = $this->MediaInfo->getClientIP();
				$this->BoardRes->uid = $this->'BoardRes']['name'] = '名無しさん';
				}
				$this->BoardRes->name = $this->data['BoardRes']['name'];
				$this->BoardRes->save($this->BoardRes);

				//掲示板更新日更新
				$this->Board->id = $this->data['BoardRes']['board_id'];
				$this->Board->saveField('modified', date('Y-m-d H:i:s'));
				$this->redirect($this->device_path . '/boards/add_res_end/' . $this->data['BoardRes']['board_id']);
			}else{
				$error_bid = $this->data['BoardRes']['board_id'];
			}
		}

		//掲示板取得
		$board = $this->Board->find('first', array(
				'conditions' => array(
						'LargeArea.id' => $this->parent_area['LargeArea']['id'],
						'Board.id' => $this->params['board_id'],
				),
				'group' => 'Board.id',
		));

		//start---2013/3/12 障害No.3-0013修正
		//存在しない掲示板の場合はエラー画面へ
		if(empty($board)){
			$this->cakeError('error404');
		}
		//end---2013/3/12 障害No.3-0013修正

		//レス範囲解析
		if(!($res_range == null)){
			$boardres_first = array();
			$boardres_range = array();

			$boardres_first = array_slice($board['BoardRes'] , 0, 1);
			if($res_range == 'l50'){
				//最新50件
				$boardres_range = array_slice($board['BoardRes'] , -50);
			}elseif($res_range == '1-'){
				//1-100件
				$boardres_range = array_slice($board['BoardRes'] , 0, 100);
			}else{
				$start_to_end = explode('-', $res_range);
				if(count($start_to_end) == 2 && is_numeric($start_to_end[0]) && is_numeric($start_to_end[1])){
					$boardres_range = array_slice($board['BoardRes'] , $start_to_end[0] - 1, $start_to_end[1] - $start_to_end[0] + 1);
				}
			}
			//echo var_dump($boardres_first);
			//echo var_dump($boardres_range);

			//前100
			$boardres_before = '';
			if(!empty($boardres_range[0])){
				//表示内容の最初の項目のレスIDよりも低いレスIDのレス数を取得
				$boardres_before_count = $this->BoardRes->find('count', array('conditions' => array('BoardRes.board_id' => $this->params['board_id'], 'BoardRes.id < ' => $boardres_range[0]['id'])));
				//100件前がマイナスの場合
				if($boardres_before_count - 100 + 1 < 1){
					$boardres_before = (string)(1) . '-';
				}else{
					$boardres_before = (string)($boardres_before_count - 100 + 1) . '-';
				}
				//レス数が0の場合
				if($boardres_before_count == 0){
					$boardres_before .= (string)(1);
				}else{
					$boardres_before .= (string)($boardres_before_count);
				}
			}
			$this->set('boardres_before', $boardres_before);

			//後100
			$boardres_after = '';
			if(!empty($boardres_range[0])){
				//表示内容の最後の項目のレスIDよりも高いレスIDのレス数を取得
				$boardres_after_count = $this->BoardRes->find('count', array('conditions' => array('BoardRes.board_id' => $this->params['board_id'], 'BoardRes.id > ' => $boardres_range[count($boardres_range)-1]['id'])));
				//レス数が0の場合
				if($boardres_after_count == 0){
					$boardres_after = (string)(count($board['BoardRes'])) . '-' . (string)(count($board['BoardRes']));
				}else{
					//レス数が0でない場合
					$boardres_after = (string)(count($board['BoardRes']) - $boardres_after_count + 1) . '-';
					//レス数が100件存在しない場合
					if($boardres_after_count < 100){
						$boardres_after .= (string)(count($board['BoardRes']));
					}else{
						$boardres_after .= (string)(count($board['BoardRes']) - $boardres_after_count + 100);
					}
				}
			}
			$this->set('boardres_after', $boardres_after);


			if(!empty($boardres_first[0]) && !empty($boardres_range[0])){
				if($boardres_first[0]['id'] == $boardres_range[0]['id']){
					$board['BoardRes'] = $boardres_range;
				}else{
					$board['BoardRes'] = array_merge_recursive($boardres_first, $boardres_range);
				}
			}
		}else{
			//全部表示
			$this->set('boardres_before', '1-1');
			$this->set('boardres_after', count($board['BoardRes']) . '-' . count($board['BoardRes']));
		}
		$this->set('board',$board);



		//head
		$this->set('title_for_layout', $this->title_tag_common . '炎上掲示板'.' '.$board['Board']['title']);
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',炎上,掲示板,BBS');
		$this->set('meta_description', $this->meta_description_common . '炎上掲示板'.' '.$board['Board']['title']);
		$this->set('header_one', $this->h1_tag_common . '炎上掲示板'.' '.$board['Board']['title']);
	}
	function i_read() {

		//掲示板取得
		$board = $this->Board->find('first', array(
				'conditions' => array(
						'LargeArea.id' => $this->parent_area['LargeArea']['id'],
						'Board.id' => $this->params['board_id'],
				),
				'group' => 'Board.id',
		));
		$this->set('board',$board);


		//掲示板取得
		$this->paginate = array('BoardRes' => array(
				'conditions' => array(
						'BoardRes.board_id' => $this->params['board_id'],
				),
				'order' => 'BoardRes.sub_id ASC',
				'limit' => $this->pcount['boards_view_res']
		));
		$this->set('boardres',$this->paginate('BoardRes'));

		//head
		$this->set('title_for_layout', $this->title_tag_common . '炎上掲示板'.' '.$board['Board']['title']);
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',炎上,掲示板,BBS');
		$this->set('meta_description', $this->meta_description_common . '炎上掲示板'.' '.$board['Board']['title']);
		$this->set('header_one', $this->h1_tag_common . '炎上掲示板'.' '.$board['Board']['title']);
	}
	function s_read() {
		$this->i_read();
	}

	//エラー画面
	function error(){

		//head
		$this->set('title_for_layout', $this->title_tag_common . '炎上掲示板'.' 投稿エラー');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',炎上,掲示板,BBS');
		$this->set('meta_description', $this->meta_description_common . '炎上掲示板'.' 投稿エラー');
		$this->set('header_one', $this->h1_tag_common . '炎上掲示板'.' 投稿エラー');

	}
	function i_error() {
		$this->error();
	}
	function s_error() {
		$this->error();
	}

	//新規スレッド作成（携帯）
	function i_add_board(){

		if(!empty($this->data)){

			//投稿禁止IPアドレス判定
			if($this->VetoIpAddressBoard->find('count', array('conditions' => array('VetoIpAddressBoard.name' => $this->MediaInfo->getClientIP()))) != 0){
				$this->redirect($this->device_path . '/boards/error');
			}
			//投稿禁止個体識別番号判定
			if($this->VetoUidBoard->find('count', array('conditions' => array('VetoUidBoard.name' => $this->MediaInfo->getUid()))) != 0){
				$this->redirect($this->device_path . '/boards/error');
			}

			//掲示板モデル・レスモデルが空じゃない
			//新規スレッド作成
			$this->Board->set($this->data['Board']);
			$this->BoardRes->set($this->data['BoardRes']);
			$board_validate = $this->Board->validates();
			$boardres_validate = $this->BoardRes->validates();

			if($board_validate && $boardres_validate){

				//掲示板作成
				$this->Board->large_area_id = $this->parent_area['LargeArea']['id'];
				$this->Board->show_order = 10000;
				$this->Board->save($this->Board);

				//レス投稿
				$this->BoardRes->sub_id = 1;
				$this->BoardRes->ip_address = $this->MediaInfo->getClientIP();
				$this->BoardRes->uid = $this->MediaInfo->getUid();
				$this->BoardRes->media = $this->MediaInfo->getMedia();
				$this->BoardRes->board_id = $this->Board->id;
				if(empty($this->data['BoardRes']['name'])){
					$this->data['BoardRes']['name'] = '名無しさん';
				}
				$this->BoardRes->name = $this->data['BoardRes']['name'];
				$this->BoardRes->save($this->BoardRes);
				$this->redirect($this->device_path . '/boards/addend/' . $this->Board->id);
			}

		}
		$this->set('data', $this->data);


		//head
		$this->set('title_for_layout', $this->title_tag_common . '炎上掲示板 新規スレッド作成');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',炎上,掲示板,BBS');
		$this->set('meta_description', $this->meta_description_common . '炎上掲示板 新規スレッド作成');
		$this->set('header_one', $this->h1_tag_common . '炎上掲示板 新規スレッド作成');
	}
	function s_add_board(){
		$this->i_add_board();
	}

	//レス投稿（携帯）
	function i_add_res($board_id){

		//掲示板取得
		$board = $this->Board->find('first', array(
				'conditions' => array(
						'LargeArea.id' => $this->parent_area['LargeArea']['id'],
						'Board.id' => $board_id,
				),
				'group' => 'Board.id',
		));
		$this->set('board',$board);

		if(!empty($this->data)){
			//レス投稿
			$this->BoardRes->set($this->data['BoardRes']);
			if($this->BoardRes->validates()){

				//投稿禁止IPアドレス判定
				if($this->VetoIpAddressBoard->find('count', array('conditions' => array('VetoIpAddressBoard.name' => $this->MediaInfo->getClientIP()))) != 0){
					$this->redirect($this->device_path . '/boards/error');
				}
				//投稿禁止個体識別番号判定
				if($this->VetoUidBoard->find('count', array('conditions' => array('VetoUidBoard.name' => $this->MediaInfo->getUid()))) != 0){
					$this->redirect($this->device_path . '/boards/error');
				}

				//レス保存
				$maxRes = $this->BoardRes->find('first', array(
						'fields' => 'MAX(sub_id) as sub_id',
						'conditions' => array('BoardRes.board_id' => $this->data['BoardRes']['board_id']),
						'group' => 'BoardRes.board_id',
				));
				$this->BoardRes->create();
				$this->BoardRes->set($this->data['BoardRes']);
				$this->BoardRes->sub_id = $maxRes[0]['sub_id']+1;
				$this->BoardRes->ip_address = $this->MediaInfo->getClientIP();
				$this->BoardRes->uid = $this->MediaInfo->getUid();
				$this->BoardRes->media = $this->MediaInfo->getMedia();
				if(empty($this->data['BoardRes']['name'])){
					$this->data['BoardRes']['name'] = '名無しさん';
				}
				$this->BoardRes->name = $this->data['BoardRes']['name'];
				$this->BoardRes->save($this->BoardRes);

				//掲示板更新日更新
				$this->Board->id = $this->data['BoardRes']['board_id'];
				$this->Board->saveField('modified', date('Y-m-d H:i:s'));
				$this->redirect($this->device_path . '/boards/add_res_end/' . $this->data['BoardRes']['board_id']);
			}
		}


		//head
		$this->set('title_for_layout', $this->title_tag_common . '炎上掲示板'.' '.$board['Board']['title'] . ' レス投稿');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',炎上,掲示板,BBS');
		$this->set('meta_description', $this->meta_description_common . '炎上掲示板'.' '.$board['Board']['title'] . ' レス投稿');
		$this->set('header_one', $this->h1_tag_common . '炎上掲示板'.' '.$board['Board']['title'] . ' レス投稿');
	}
	function s_add_res($board_id){
		$this->i_add_res($board_id);
	}
}
?>
