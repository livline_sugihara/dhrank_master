<?php
class AjaxController extends AppController {
	var $name = 'Ajax';
	var $uses = array('User','Shop','Girl','Review','ReviewsGoodCount','BookmarkShop','BookmarkGirl','BookmarkReview','FollowReviewer');

	function beforeFilter() {
		parent::beforeFilter();
		$this->autoRender = false;
	}

	// 「ありがとう」へ投票
	function update_good() {

		// FIXME デバッグ
		$this->log('update_good() : ' . $this->params['review_id'], 'debug');

		$this->uses = null;
		Configure::write('debug',0);

		// ログインしていない場合
		if(empty($this->parent_reviewer)) {
			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 投稿にはログインが必要です。',
			));
			return;
		}

		$ip_address = $this->MediaInfo->getClientIP();

		//指定時間以内に投票があるかどうか取得
		$count = $this->ReviewsGoodCount->find('count', array(
			'conditions' => array(
				'ReviewsGoodCount.review_id' => $this->params['review_id'],
				'ReviewsGoodCount.ip_address' => $ip_address,
				'ReviewsGoodCount.created >= ' => date('Y-m-d H:i:s', strtotime('-' . APP_HOUR_GOODCOUNT_ENTRY . ' hour')),
		)));

		//投稿済みでない場合
		if($count == 0){
			//投票
			$data['ReviewsGoodCount']['review_id'] = $this->params['review_id'];
			$data['ReviewsGoodCount']['ip_address'] = $ip_address;
			$this->ReviewsGoodCount->save($data);

			echo json_encode(array(
				'success' => true,
			));
			return;
		}
		// echo $this->ReviewsGoodCount->find('count', array('conditions' => array('ReviewsGoodCount.review_id' => $this->params['review_id'],)));

		echo json_encode(array(
			'success' => false,
			'message' => '【エラー】 既に投稿されています。',
		));
	}

	function s_update_good(){
		$this->update_good();
	}

	// ブックマーク店舗更新
	function update_bookmark_shop(){

		// FIXME デバッグ
		$this->log('bookmark_shop('. $this->params['mode'] .') : ' . $this->params['user_id'], 'debug');

		$this->uses = null;
		Configure::write('debug',0);

		// ログインしていない場合
		if(empty($this->parent_reviewer)) {
			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 お気に入り機能はログインが必要です。',
			));
			return;
		}

		//ブックマーク取得
		$bookmark_shop = $this->BookmarkShop->find('first', array(
			'conditions' => array(
				'BookmarkShop.reviewer_id' => $this->Auth->user('id'),
				'BookmarkShop.user_id' => $this->params['user_id'])
			)
		);

		if($this->params['mode'] == 'add') {
			if(empty($bookmark_shop)){
				//登録する
				$this->BookmarkShop->create();
				$data['BookmarkShop']['reviewer_id'] = $this->Auth->user('id');
				$data['BookmarkShop']['user_id'] = $this->params['user_id'];
				$this->BookmarkShop->save($data);

	//			echo '<img src="/images' . $this->device_path . '/bookmark_off.png" />';

				echo json_encode(array(
					'success' => true,
					'message' => 'お気に入りに追加しました。',
				));
				return;
			}

			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 既にお気に入りに追加されています',
			));
			return;
		} else if($this->params['mode'] == 'delete') {

			if(!empty($bookmark_shop)) {
				//削除する
				$this->BookmarkShop->delete($bookmark_shop['BookmarkShop']['id']);
				// echo '<img src="/images' . $this->device_path . '/bookmark.png" />';

				echo json_encode(array(
					'success' => true,
					'message' => 'お気に入りから削除しました。',
				));
				return;
			}

			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 お気に入りに入っていないので削除出来ません。',
			));
			return;			
		}
	}

	function s_update_bookmark_shop(){
		$this->update_bookmark_shop();
	}

	// ブックマーク女の子更新
	function update_bookmark_girl(){

		// FIXME デバッグ
		$this->log('bookmark_girl('. $this->params['mode'] .') : ' . $this->params['girl_id'], 'debug');

		$this->uses = null;
		Configure::write('debug',0);

		// ログインしていない場合
		if(empty($this->parent_reviewer)) {
			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 お気に入り機能はログインが必要です。',
			));
			return;
		}

		//ブックマーク取得
		$bookmark_girl = $this->BookmarkGirl->find('first', array(
			'conditions' => array(
				'BookmarkGirl.reviewer_id' => $this->Auth->user('id'),
				'BookmarkGirl.girl_id' => $this->params['girl_id']
			)
		));

		if($this->params['mode'] == 'add') {

			if(empty($bookmark_girl)) {
				//登録する
				$this->BookmarkGirl->create();
				$data['BookmarkGirl']['reviewer_id'] = $this->Auth->user('id');
				$data['BookmarkGirl']['girl_id'] = $this->params['girl_id'];
				$this->BookmarkGirl->save($data);
				// echo '<img src="/images' . $this->device_path . '/bookmark_off.png" />';

				echo json_encode(array(
					'success' => true,
					'message' => 'お気に入りに追加しました。',
				));
				return;
			}

			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 既にお気に入りに追加されています',
			));
			return;

		} else if($this->params['mode'] == 'delete') {

			if(!empty($bookmark_girl)) {
				//削除する
				$this->BookmarkGirl->delete($bookmark_girl['BookmarkGirl']['id']);
				// echo '<img src="/images' . $this->device_path . '/bookmark.png" />';

				echo json_encode(array(
					'success' => true,
					'message' => 'お気に入りから削除しました。',
				));
				return;
			}

			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 お気に入りに入っていないので削除出来ません。',
			));
			return;			
		}
	}

	function s_update_bookmark_girl(){
		$this->update_bookmark_girl();
	}

	// ブックマークレビュー更新
	function update_bookmark_review(){

		// FIXME デバッグ
		$this->log('bookmark_review('. $this->params['mode'] .') : ' . $this->params['review_id'], 'debug');

		$this->uses = null;
		Configure::write('debug',0);

		// ログインしていない場合
		if(empty($this->parent_reviewer)) {
			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 お気に入り機能はログインが必要です。',
			));
			return;
		}

		//ブックマーク取得
		$bookmark_review = $this->BookmarkReview->find('first', array(
			'conditions' => array(
				'BookmarkReview.reviewer_id' => $this->Auth->user('id'),
				'BookmarkReview.review_id' => $this->params['review_id'],
			)
		));

		if($this->params['mode'] == 'add') {
			if(empty($bookmark_review)){
				//登録する
				$this->BookmarkReview->create();
				$data['BookmarkReview']['reviewer_id'] = $this->Auth->user('id');
				$data['BookmarkReview']['review_id'] = $this->params['review_id'];
				$this->BookmarkReview->save($data);
				// echo '<img src="/images' . $this->device_path . '/bookmark_off.png" />';

				echo json_encode(array(
					'success' => true,
					'message' => 'お気に入りに追加しました。',
				));
				return;
			}

			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 既にお気に入りに追加されています',
			));
			return;

		} else if($this->params['mode'] == 'delete') {
			if(!empty($bookmark_review)){
				//削除する
				$this->BookmarkReview->delete($bookmark_review['BookmarkReview']['id']);
				// echo '<img src="/images' . $this->device_path . '/bookmark.png" />';

				echo json_encode(array(
					'success' => true,
					'message' => 'お気に入りから削除しました。',
				));
				return;
			}

			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 お気に入りに入っていないので削除出来ません。',
			));
			return;			
		}
	}

	function s_update_bookmark_review(){
		$this->update_bookmark_review();
	}


	// ブックマークレビュー更新
	function update_follow(){

		// FIXME デバッグ
		$this->log('update_follow('. $this->params['mode'] .') : ' . $this->params['reviewer_id'], 'debug');

		$this->uses = null;
		Configure::write('debug',0);

		// ログインしていない場合
		if(empty($this->parent_reviewer)) {
			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 フォロワー機能はログインが必要です。',
			));
			return;
		}

		//ブックマーク取得
		$follow = $this->FollowReviewer->find('first', array(
			'conditions' => array(
				'FollowReviewer.from_reviewer_id' => $this->Auth->user('id'),
				'FollowReviewer.to_reviewer_id' => $this->params['reviewer_id'],
			)
		));

		if($this->params['mode'] == 'add') {
			if(empty($follow)){
				//登録する
				$this->FollowReviewer->create();
				$data['FollowReviewer']['from_reviewer_id'] = $this->Auth->user('id');
				$data['FollowReviewer']['to_reviewer_id'] = $this->params['reviewer_id'];
				$this->FollowReviewer->save($data);

				echo json_encode(array(
					'success' => true,
					'message' => 'フォローリストに追加しました。',
				));
				return;
			}

			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 既にフォローされています',
			));
			return;

		} else if($this->params['mode'] == 'delete') {
			if(!empty($follow)){
				//削除する
				$this->FollowReviewer->delete($follow['FollowReviewer']['id']);

				echo json_encode(array(
					'success' => true,
					'message' => 'フォロワーリストから削除しました。',
				));
				return;
			}

			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 フォロワーリストに無いので削除出来ません。',
			));
			return;			
		}
	}

	function s_update_follow(){
		$this->update_follow();
	}
}
?>