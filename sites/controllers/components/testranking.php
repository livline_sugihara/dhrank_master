<?php

class TestrankingComponent extends Object {
	var $_controller;
	//var $uses = array('TestRanking','Shop','Review','LargeArea','Reviewer');


    public function load(& $controller)
    {
        $this->_controller = $controller;
    }



	// エリア内店舗、カテゴリ別ランキング取得
	function index($category_id = '' ,$limit = 5)
	{
		$looks_datas = $this->_controller->TestRanking->looks('', $category_id, $this->parent_area['LargeArea']['id'], $limit);
		$service_datas = $this->_controller->TestRanking->service('', $category_id, $this->parent_area['LargeArea']['id'], $limit);
		$character_datas = $this->_controller->TestRanking->character('', $category_id, $this->parent_area['LargeArea']['id'], $limit);
		$data = array($looks_datas, $service_datas, $character_datas);

		return $data;


		// $this->set('looks_datas', $looks_datas);		//
		// $this->set('service_datas', $service_datas);		//
		// $this->set('character_datas', $character_datas);
	}

	function reviewer()
	{
		//$this->layout = '';
		//$this->autoRender = false;

		// 口コミ数集計
		$this->_controller->TestRanking->reviewer();
	}

	function access_count()
	{
		// アクセスカウント集計
		$this->layout = '';
		$this->autoRender = false;

		$this->_controller->TestRanking->access_count();

	}

	// 口コミランキングデータ取得
	function get_reviewer_ranking()
	{
		//$this->layout = '';
		//$this->autoRender = false;

		$datas = $this->_controller->TestRanking->getReviewerRankingData();
		return $datas;
	}


	// アクセスカウントランキングデータ取得
	function get_access_ranking()
	{
		$this->layout = '';
		$this->autoRender = false;

		$datas = $this->_controller->TestRanking->getAccessCountData();
		var_dump($datas);

	}

}
?>
