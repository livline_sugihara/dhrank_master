<?php

App::import('Vendor', 'pear_ini');
App::import('Vendor', 'Mobile', array('file' => 'Net'.DS.'UserAgent'.DS.'Mobile.php'));

class MediaInfoComponent extends Object{
	var $components = array('RequestHandler');

	function getUid(){
		$ua = &Net_UserAgent_Mobile::singleton();
		if(!$ua->isNonMobile()){
			if($ua->isDoCoMo()){
				//Docomo用の処理
				if(!empty($_SERVER['HTTP_X_DCMGUID'])){
					return $_SERVER['HTTP_X_DCMGUID'];
				}
			}elseif($ua->isSoftBank()){
				//Softbank用の処理
				if(!empty($_SERVER['HTTP_X_JPHONE_UID'])){
					return $_SERVER['HTTP_X_JPHONE_UID'];
				}
			}elseif($ua->isEZweb()){
				//Au用の処理
				if(!empty($_SERVER['HTTP_X_UP_SUBNO'])){
					return $_SERVER['HTTP_X_UP_SUBNO'];
				}
			}
		} else {
			return '';
		}
	}

	function getMedia(){
		$ua = &Net_UserAgent_Mobile::singleton();
		if(!$ua->isNonMobile()){
			return '携帯';
		} else {
			if (preg_match('!iPhone|iPod!', $ua->getUserAgent())) {
				return 'スマートフォン';
			} elseif (preg_match('!Android!i', $ua->getUserAgent())) {
				return 'スマートフォン';
			} else {
				return 'PC';
			}
		}
	}
	function getMediaCategory(){
		$ua = &Net_UserAgent_Mobile::singleton();
		if(!$ua->isNonMobile()){
			return 'i';
		} else {
			if (preg_match('!iPhone|iPod!', $ua->getUserAgent())) {
				return 's';
			} elseif (preg_match('!Android!i', $ua->getUserAgent())) {
				return 's';
			} else {
				return '';
			}
		}
	}
	function getClientIP(){
		return $this->RequestHandler->getClientIP();
	}
}