<?php

class RankingController extends AppController {
	var $name = 'Ranking';
	var $uses = array('Shop','Coupon','MShopsBusinessCategory');

	function beforeFilter() {
		parent::beforeFilter();

		switch($this->params['action'])
		{
			case 'category':
				$this->addBreadCrumbs("カテゴリ別ランキング" , "/ranking/category/" . $this->params['business_category_id']);
				break;
			case 'girl':
				$this->addBreadCrumbs("アクセスの多い女の子ランキング" , "/ranking/girl");
				break;
			case 'shop':
				$this->addBreadCrumbs("お店総合ランキング" , "/ranking/shop");
				break;
			case 'review':
				$this->addBreadCrumbs("口コミランキング" , "/ranking/review");
				break;
			case 's_girl':
				$this->addBreadCrumbs("アクセスの多い女の子ランキング" , "/ranking/girl");
				break;
			case 's_shop':
				$this->addBreadCrumbs("お店総合ランキング" , "/ranking/shop");
				break;
			case 's_review':
				$this->addBreadCrumbs("口コミランキング" , "/ranking/review");
				break;
			case 's_reviewer':
				$this->addBreadCrumbs("口コミ投稿者ランキング" , "/ranking/reviewer");
				break;
		}
	}

	function girl(){

		// データはapp_controllerにて取得

		//head
		$this->set('title_for_layout', $this->title_tag_common . 'アクセスの多い女の子ランキング');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',' . 'アクセスの多い女の子ランキング');
		$this->set('meta_description', $this->meta_description_common  . 'アクセスの多い女の子ランキング');
		$this->set('header_one', $this->h1_tag_common  . 'アクセスの多い女の子ランキング');

	}
	function s_girl() {
		$this->girl();
	}
	
	function shop(){

		// データはapp_controllerにて取得

		//head
		$this->set('title_for_layout', $this->title_tag_common . 'お店総合ランキング');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',' . 'お店総合ランキング');
		$this->set('meta_description', $this->meta_description_common  . 'お店総合ランキング');
		$this->set('header_one', $this->h1_tag_common  . 'お店総合ランキング');
	}
	function s_shop() {
		$this->shop();

		// ランキング情報取得（店舗総合）を加工
		$shop_ranking = $this->viewVars['shop_ranking'];
		$limit_day = date('Y-m-d');

		foreach($shop_ranking AS $ii => $record) {
			$cnt = $this->Coupon->find('count', array(
				'conditions' => array(
					'Coupon.user_id' => $record['User']['id'],
					'OR' => array(
						'Coupon.expire_unlimited' => 1,
						'DATE_FORMAT(Coupon.expire_date,"%Y-%m-%d") >= ' => $limit_day,
					),
				)
			));

			$shop_ranking[$ii]['Coupon']['cnt'] = $cnt;
		}
		$this->set('shop_ranking', $shop_ranking);
	}
	
	function reviewer(){

		// データはapp_controllerにて取得

		//head
		$this->set('title_for_layout', $this->title_tag_common . 'レビュアーランキング');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',' . 'レビュアーランキング');
		$this->set('meta_description', $this->meta_description_common  . 'レビュアーランキング');
		$this->set('header_one', $this->h1_tag_common  . 'レビュアーランキング');
	}
	function s_reviewer() {
		$this->reviewer();
	}
	
	function category(){

		// 対象データは1週間
		$limit_day = date('Y-m-d', strtotime('-' . APP_SUMMARY_REVIEW_RANGE));

		// お店総合ランキング
		$this->SummaryData->bindModel(array('belongsTo' => array('Shop' => array(
			'className'=>'Shop',
			'type'=>'inner',
			'conditions' => array(
				'Shop.user_id = SummaryData.relation_id',
				'Shop.shops_business_category_id' => $this->params['business_category_id'],
			),
			'foreignKey' => false,))),false);

		$this->SummaryData->bindModel(array('belongsTo' => array('User' => array(
			'className'=>'User',
			'type'=>'inner',
			'conditions' => array(
				'User.id = SummaryData.relation_id',
			),
			'foreignKey' => false,))),false);

		$this->SummaryData->bindModel(array('belongsTo' => array('ReviewTotal' => array(
			'className'=>'Review',
			'type'=>'inner',
			'fields' => array(
				'ReviewTotal.user_id',
				'count(ReviewTotal.id) AS total_cnt',
				'(AVG(ReviewTotal.score_girl_character) + AVG(ReviewTotal.score_girl_service) + AVG(ReviewTotal.score_girl_looks)) / 3 AS total_average',
			),
			'group' => 'ReviewTotal.user_id',
			'conditions' => array(
				'ReviewTotal.user_id = User.id',
				'DATE_FORMAT(ReviewTotal.created,"%Y-%m-%d") >= ' => $limit_day,
			),
			'foreignKey' => false,))),false);

		$shop_ranking = $this->SummaryData->find('all', array(
			'conditions' => array(
				'SummaryData.summary_type' => SummaryData::SUMMARY_TYPE_SHOP,
				'SummaryData.large_area_id' => $this->parent_area['LargeArea']['id'],
				'DATE_FORMAT(SummaryData.date,"%Y-%m-%d") >= ' => $limit_day,
			),
			'order' => 'SummaryData.rank ASC',
			'group' => 'SummaryData.relation_id',
			'limit' => 5,
		));
		$this->set('category_shop_ranking', $shop_ranking);
		$this->set('category_id', $this->params['business_category_id']);
		$this->SummaryData->unbindModel(array('belongsTo' => array('Shop', 'User', 'ReviewTotal')));


		//head
		$this->set('title_for_layout', $this->title_tag_common . 'お店総合ランキング');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',' . 'お店総合ランキング');
		$this->set('meta_description', $this->meta_description_common  . 'お店総合ランキング');
		$this->set('header_one', $this->h1_tag_common  . 'お店総合ランキング');

	}
	
	function review(){

		// 参考になった口コミランキング
		$fields = array('ifnull((SELECT count(reviews.id) FROM reviews WHERE reviews.reviewer_id > 0 AND reviews.reviewer_id = Reviewer.id GROUP BY reviews.reviewer_id HAVING count(*) > 0), 0) AS total_cnt');
		$conditions = array(
			'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id'],
		);
		$order = '(SELECT count(id) FROM reviews_good_counts good WHERE good.review_id = Review.id GROUP BY review_id) DESC';
		$helpful_review = $this->_getReviews($conditions, $order, 10, $fields);
		$this->set('helpful_review', $helpful_review);

		//head
		$this->set('title_for_layout', $this->title_tag_common . '参考になった口コミ');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',' . '参考になった口コミ');
		$this->set('meta_description', $this->meta_description_common  . '参考になった口コミ');
		$this->set('header_one', $this->h1_tag_common  . '参考になった口コミ');
	}

	function s_review() {
		$this->review();
	}
}
?>