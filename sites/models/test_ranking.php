<?php
class TestRanking extends AppModel {
	var $name = 'TestRanking';
	var $useTable = false;

	// 指定された店舗のルックスのランキングを取得
	function looks($user_id = '', $category_id = '', $large_area_id = '', $limit = 5)
	{
		$sql =  ' SELECT  ranking.avg, girls.name, girls.user_id, girls.id, shops.name FROM (';
		$sql .= '   SELECT TRUNCATE(AVG(score_girl_looks), 1) as avg, girl_id';
		$sql .= '   FROM reviews';
		$sql .= '   WHERE delete_flag = 0';
		if ($user_id != '')
			$sql .= ' AND user_id = ' . $user_id;
		$sql .= '   GROUP BY girl_id';
		$sql .= ' ) AS ranking';
		$sql .= ' LEFT JOIN girls';
		$sql .= ' ON ranking.girl_id = girls.id';
		$sql .= ' LEFT JOIN shops';
		$sql .= ' ON girls.user_id = shops.user_id';
		$sql .= ' LEFT JOIN users';
		$sql .= ' ON girls.user_id = users.id';
		$sql .= ' WHERE girls.is_deleted = 0';
		$sql .= ' AND users.large_area_id = "' . $large_area_id . '"';
		if ($category_id != '')
			$sql .= ' AND shops.shops_business_category_id = "' . $category_id . '"';
		$sql .= ' ORDER BY avg DESC';
		$sql .= ' LIMIT ' . $limit;

		$looks_datas = $this->query($sql);
        return $looks_datas;
	}

	// 店舗のサービスランキング($user_idがなければ、エリア店舗全体での検索)
	function service($user_id = '', $category_id = '', $large_area_id = '', $limit = 5)
	{
		// 指定された店舗の女性のサービスのランキングを取得
		$sql =  ' SELECT ranking.avg, girls.name, girls.user_id, girls.id , shops.name FROM (';
		$sql .= '   SELECT TRUNCATE(AVG(score_girl_service), 1) as avg, girl_id, shop_name';
		$sql .= '   FROM reviews';
		$sql .= '   WHERE delete_flag = 0';
		if ($user_id != '')
			$sql .= ' AND user_id = ' . $user_id;
		$sql .= '   GROUP BY girl_id';
		$sql .= ' ) AS ranking';
		$sql .= ' LEFT JOIN girls';
		$sql .= ' ON ranking.girl_id = girls.id';
		$sql .= ' LEFT JOIN shops';
		$sql .= ' ON girls.user_id = shops.user_id';
		$sql .= ' LEFT JOIN users';
		$sql .= ' ON girls.user_id = users.id';
		$sql .= ' WHERE girls.is_deleted = 0';
		$sql .= ' AND users.large_area_id = "' . $large_area_id . '"';
		if ($category_id != '')
			$sql .= ' AND shops.shops_business_category_id = "' . $category_id . '"';
		$sql .= ' ORDER BY avg DESC';
		$sql .= ' LIMIT ' . $limit;

		$service_datas = $this->query($sql);
		return $service_datas;
	}

	// キャラクターランキング($user_idがなければ、エリア店舗全体での検索)
	function character($user_id = '', $category_id = '', $large_area_id = '', $limit = 5)
	{
		// 指定された店舗の女性の性格のランキングを取得
		$sql =  ' SELECT ranking.avg, girls.name, girls.user_id, girls.id , shops.name  FROM (';
		$sql .= '   SELECT TRUNCATE(AVG(score_girl_character), 1) as avg, girl_id, shop_name';
		$sql .= '   FROM reviews';
		$sql .= '   WHERE delete_flag = 0';
		if ($user_id != '')
			$sql .= ' AND user_id = ' . $user_id;
		$sql .= '   GROUP BY girl_id';
		$sql .= ' ) AS ranking';
		$sql .= ' LEFT JOIN girls';
		$sql .= ' ON ranking.girl_id = girls.id';
		$sql .= ' LEFT JOIN shops';
		$sql .= ' ON girls.user_id = shops.user_id';
		$sql .= ' LEFT JOIN users';
		$sql .= ' ON girls.user_id = users.id';
		$sql .= ' WHERE girls.is_deleted = 0';
		$sql .= ' AND users.large_area_id = "' . $large_area_id . '"';
		if ($category_id != '')
			$sql .= ' AND shops.shops_business_category_id = "' . $category_id . '"';
		$sql .= ' ORDER BY avg DESC';
		$sql .= ' LIMIT ' . $limit;

		$character_datas = $this->query($sql);
		return $character_datas;
	}

	// レビュワーの口コミ数ランキング(「ありがとう」で集計)
	// 1ヶ月間で集計
	function reviewer($limit = 5)
	{
		$start_date = date('Y-m-01 00:00:00');
		$end_date = date('Y-m-t 23:59:59');

// TEST用
$start_date = '2014-01-01 00:00:00';
$end_date = '2014-01-31 00:00:00';
// TEST用

		// レビュワーの名前、レビュワーのID,口コミ数、アバター,コメント
		$sql =  ' SELECT reviewers.handle, reviews_good_counts.review_id, reviewers.avatar_id, reviewers.comment, COUNT(*) AS cnt';
		$sql .= ' FROM reviews_good_counts';
		$sql .= ' LEFT JOIN reviewers';
		$sql .= ' ON reviews_good_counts.review_id = reviewers.id';
		$sql .= ' LEFT JOIN reviewer_registers';
		$sql .= ' ON reviewer_registers.id = reviewers.id';
		$sql .= ' AND is_created = 1';
		$sql .= ' WHERE reviewers.handle IS NOT NULL';
		$sql .= ' AND reviews_good_counts.created >= "' . $start_date . '"';
		$sql .= ' AND reviews_good_counts.created <= "' . $end_date . '"';
		$sql .= ' GROUP BY reviews_good_counts.review_id';
		$sql .= ' ORDER BY cnt DESC';
		$sql .= ' LIMIT ' . $limit;

		$reviewer_datas = $this->query($sql);

		// 2週間前に集計したものを、DELETE
		$sql = ' DELETE FROM rankings';
		$sql .= ' WHERE category = "1"'; // 口コミランキングのカテゴリは１
		$this->query($sql);

		// INSERT
		$insert_sql_arr = array();
		$sql = ' INSERT INTO rankings ';
		$sql .= ' (ranking_id, ranking_name, ranking_comment, count, avatar_id, category, created, modified)';
		$sql .= ' VALUES ';

		foreach ($reviewer_datas as $key => $val)
		{
			var_dump($val);
			$insert_sql = '';
			$insert_sql .= ' (';
			$insert_sql .= ' "' . $val['reviews_good_counts']['review_id']. '",';
			$insert_sql .= ' "' . $val['reviewers']['handle']. '",';
			$insert_sql .= ' "' . $val['reviewers']['comment']. '",';
			$insert_sql .= ' "' . $val[0]['cnt'] . '",';
			$insert_sql .= ' "' . $val['reviewers']['avater_id']. '",';
			$insert_sql .= ' "1",'; // アクセスカウントのカテゴリは1
			$insert_sql .= ' now(),';
			$insert_sql .= ' now()';
			$insert_sql .= ' )';
			$insert_sql_arr[] = $insert_sql;
		}
		$sql .= implode(',', $insert_sql_arr);
		$this->query($sql);
	}

	// アクセスカウント集計
	function access_count($limit = 5)
	{
		// レビュワーのアクセスをカウントする
		$sql = ' SELECT reviewers.id, reviewers.access_count, reviewers.avatar_id, reviewers.handle, reviewers.comment';
		$sql .= ' FROM reviewers';
		$sql .= ' LEFT JOIN reviewer_registers';
		$sql .= ' ON reviewer_registers.id = reviewers.id ';
		$sql .= ' AND reviewer_registers.is_created = 1';
		$sql .= ' ORDER BY access_count DESC';
		$sql .= ' LIMIT ' . $limit;

		$access_datas = $this->query($sql);

		// 前の集計したものを、DELETE
		$sql = ' DELETE FROM rankings';
		$sql .= ' WHERE category = "2"'; // アクセスカウントのカテゴリは２
		$this->query($sql);

		// INSERT
		$insert_sql_arr = array();
		$sql = ' INSERT INTO rankings ';
		$sql .= ' (ranking_id, ranking_name, ranking_comment, count, avatar_id, category, created, modified)';
		$sql .= ' VALUES ';

		foreach ($access_datas as $key => $val)
		{
			$insert_sql = '';
			$insert_sql .= ' (';
			$insert_sql .= ' "' . $val['reviewers']['id']. '",';
			$insert_sql .= ' "' . $val['reviewers']['handle']. '",';
			$insert_sql .= ' "' . $val['reviewers']['comment']. '",';
			$insert_sql .= ' "' . $val['reviewers']['access_count']. '",';
			$insert_sql .= ' "' . $val['reviewers']['avatar_id']. '",';
			$insert_sql .= ' "2",'; // アクセスカウントのカテゴリは２
			$insert_sql .= ' now(),';
			$insert_sql .= ' now()';
			$insert_sql .= ' )';
			$insert_sql_arr[] = $insert_sql;
		}
		$sql .= implode(',', $insert_sql_arr);
		$this->query($sql);
	}

	// 口コミランキング取得
	function getReviewerRankingData($limit = 5)
	{
		$sql = ' SELECT ranking_id, ranking_name, avatar_id, ranking_comment, count';
		$sql .= ' FROM rankings';
		$sql .= ' WHERE category = "1"'; // 口コミランキングのカテゴリは１
		$sql .= ' ORDER BY count DESC'; // count降順
		$sql .= ' LIMIT ' . $limit;
		$ranking_datas = $this->query($sql);

		return $ranking_datas;

	}

	// アクセスカウント取得
	function getAccessCountData($limit = 5)
	{
		$sql = ' SELECT ranking_id, ranking_name, avatar_id, ranking_comment, count';
		$sql .= ' FROM rankings';
		$sql .= ' WHERE category = "2"'; // アクセスランキングのカテゴリは２
		$sql .= ' ORDER BY count DESC'; // count降順
		$sql .= ' LIMIT ' . $limit;
		$ranking_datas = $this->query($sql);

		return $ranking_datas;
	}

}


?>
