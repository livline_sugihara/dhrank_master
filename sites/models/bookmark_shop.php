<?php
class BookmarkShop extends AppModel {
	var $name = 'BookmarkShop';

	var $hasOne = array(
			'User' => array(
					'className' => 'User',
					'foreignKey' => '',
					'conditions' => array('User.id = BookmarkShop.user_id'),
					'fields' => '',
					'order' => ''
			),
			'LargeArea' => array(
					'className' => 'LargeArea',
					'foreignKey' => '',
					'conditions' => array('User.large_area_id = LargeArea.id'),
					'fields' => '',
					'order' => ''
			),
			'Shop' => array(
					'className' => 'Shop',
					'foreignKey' => '',
					'conditions' => array('Shop.user_id = User.id'),
					'fields' => '',
					'order' => ''
			),
			'Reviewer' => array(
					'className' => 'Reviewer',
					'foreignKey' => '',
					'conditions' => array('BookmarkShop.reviewer_id = Reviewer.id'),
					'fields' => '',
					'order' => ''
			),
			'MShopsBusinessCategory' => array(
					'className' => 'MShopsBusinessCategory',
					'foreignKey' => '',
					'conditions' => array('Shop.shops_business_category_id = MShopsBusinessCategory.id'),
					'fields' => '',
					'order' => ''
			),
	);

}
?>
