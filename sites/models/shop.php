<?php
class Shop extends AppModel {
	var $name = 'Shop';

	var $hasOne = array(
		'User' => array(
			'className' => 'User',
			'type' => 'inner',
			'foreignKey' => '',
			'conditions' => array('User.id = Shop.user_id'),
			'fields' => '',
			'order' => ''
		),
		'UsersAuthority' => array(
			'className' => 'UsersAuthority',
			'type' => 'inner',
			'foreignKey' => '',
			'conditions' => array('UsersAuthority.user_id = Shop.user_id'),
			'fields' => '',
			'order' => ''
		),
		'System' => array(
			'className' => 'System',
			'foreignKey' => '',
			'conditions' => array('System.user_id = Shop.user_id'),
			'fields' => '',
			'order' => ''
		),
		'LargeArea' => array(
			'className' => 'LargeArea',
			'type' => 'inner',
			'foreignKey' => '',
			'conditions' => array('UsersAuthority.large_area_id = LargeArea.id'),
			'fields' => '',
			'order' => ''
		),
		'MShopsBusinessCategory' => array(
			'className' => 'MShopsBusinessCategory',
			'foreignKey' => '',
			'conditions' => array('Shop.shops_business_category_id = MShopsBusinessCategory.id'),
			'fields' => '',
			'order' => ''
		),
	);
}
?>