<?php
class Girl extends AppModel {
	var $name = 'Girl';

	var $hasOne = array(
		'User' => array(
			'className' => 'User',
			'type' => 'inner',
			'foreignKey' => '',
			'conditions' => array('User.id = Girl.user_id'),
			'fields' => '',
			'order' => ''
		),
		'UsersAuthority' => array(
			'className' => 'UsersAuthority',
			'type' => 'inner',
			'foreignKey' => '',
			'conditions' => array('UsersAuthority.user_id = Girl.user_id'),
			'fields' => '',
			'order' => ''
		),
		'LargeArea' => array(
			'className' => 'LargeArea',
			'type' => 'inner',
			'foreignKey' => '',
			'conditions' => array('UsersAuthority.large_area_id = LargeArea.id'),
			'fields' => '',
			'order' => ''
		),
		'Shop' => array(
			'className' => 'Shop',
			'type' => 'inner',
			'foreignKey' => '',
			'conditions' => array('Shop.user_id = Girl.user_id'),
			'fields' => '',
			'order' => ''
		),
		'MGirlsCup' => array(
			'className' => 'MGirlsCup',
			'foreignKey' => '',
			'conditions' => array('MGirlsCup.id = Girl.girls_cup_id'),
			'fields' => '',
			'order' => ''
		),
		'MShopsBusinessCategory' => array(
			'className' => 'MShopsBusinessCategory',
			'foreignKey' => '',
			'conditions' => array('Shop.shops_business_category_id = MShopsBusinessCategory.id'),
			'fields' => '',
			'order' => ''
		),
		'MGirlsRecommendType' => array(
			'className' => 'MGirlsRecommendType',
			'foreignKey' => '',
			'conditions' => array('Girl.recommend_type_id = MGirlsRecommendType.id'),
			'fields' => '',
			'order' => ''
		),
	);

	//findメソッド実行後のオーバーライド
	public function afterFind($results){
	    foreach ($results as $key => $record){
	    	//count取得ではない
	    	if (isset($results[$key]['Girl']['id'])){
		    	//年齢
		        if (!isset($results[$key]['Girl']['age'])){
		            $results[$key]['Girl']['age'] = '－';
		        }
		    	//身長
		        if (!isset($results[$key]['Girl']['body_height'])){
		            $results[$key]['Girl']['body_height'] = '－';
		        }
		    	//カップ数
		        if (!isset($results[$key]['MGirlsCup']['name'])){
		            $results[$key]['MGirlsCup']['name'] = '－';
		        }
		        if (!isset($results[$key]['MGirlsCup']['name2'])){
		            $results[$key]['MGirlsCup']['name2'] = '－';
		        }
		    	//バスト
		        if (!isset($results[$key]['Girl']['bust'])){
		            $results[$key]['Girl']['bust'] = '－';
		        }
		    	//ウエスト
		        if (!isset($results[$key]['Girl']['waist'])){
		            $results[$key]['Girl']['waist'] = '－';
		        }
		    	//ヒップ
		        if (!isset($results[$key]['Girl']['hip'])){
		            $results[$key]['Girl']['hip'] = '－';
		        }
	    	}
	    }
	    return $results;
	}
}
?>