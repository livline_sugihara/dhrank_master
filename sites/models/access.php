<?php
class Access extends AppModel {
	var $name = 'Access';

	const ACCESS_TYPE_ALL = 1;			// アクセスカウント(全体)(relation_id = 0)
	const ACCESS_TYPE_CATEGORY = 2;		// カテゴリごとのアクセスカウント(relation_id = カテゴリID)
	const ACCESS_TYPE_SHOP = 3;			// お店ごとのアクセスカウント(relation_id = user_id)
	const ACCESS_TYPE_GIRL = 4;			// 女の子毎のアクセスカウント(relation_id = 女の子ID)
	const ACCESS_TYPE_REVIEW = 5;		// 口コミ毎のアクセスカウント(relation_id = 0)
	const ACCESS_TYPE_GOOD = 6;			// レビュー毎の「いいね」カウント(relation_id = レビューID)

	const RELATION_ID_NONE = 0;
}
