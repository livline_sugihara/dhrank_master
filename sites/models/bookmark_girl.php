<?php
class BookmarkGirl extends AppModel {
	var $name = 'BookmarkGirl';

	var $hasOne = array(
			'Girl' => array(
					'className' => 'Girl',
					'foreignKey' => '',
					'conditions' => array('Girl.id = BookmarkGirl.girl_id'),
					'fields' => '',
					'order' => ''
			),
			'User' => array(
					'className' => 'User',
					'foreignKey' => '',
					'conditions' => array('User.id = Girl.user_id'),
					'fields' => '',
					'order' => ''
			),
			'LargeArea' => array(
					'className' => 'LargeArea',
					'foreignKey' => '',
					'conditions' => array('User.large_area_id = LargeArea.id'),
					'fields' => '',
					'order' => ''
			),
			'Shop' => array(
					'className' => 'Shop',
					'foreignKey' => '',
					'conditions' => array('Shop.user_id = User.id'),
					'fields' => '',
					'order' => ''
			),
			'MGirlsCup' => array(
					'className' => 'MGirlsCup',
					'foreignKey' => '',
					'conditions' => array('MGirlsCup.id = Girl.girls_cup_id'),
					'fields' => '',
					'order' => ''
			),
			'Reviewer' => array(
					'className' => 'Reviewer',
					'foreignKey' => '',
					'conditions' => array('BookmarkGirl.reviewer_id = Reviewer.id'),
					'fields' => '',
					'order' => ''
			),
	);

}
?>
