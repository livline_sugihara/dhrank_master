<?php
class Reviewer extends AppModel {
	var $name = 'Reviewer';

	var $belongsTo = array(
		'LargeArea' => array(
			'className' => 'LargeArea',
			'foreignKey' => '',
			'conditions' => array('Reviewer.large_area_id = LargeArea.id'),
			'fields' => '',
			'order' => ''
		),
		'MReviewersAge' => array(
			'className' => 'MReviewersAge',
			'foreignKey' => '',
			'conditions' => array('Reviewer.age_id = MReviewersAge.id'),
			'fields' => '',
			'order' => ''
		),
		'MReviewersJob' => array(
			'className' => 'MReviewersJob',
			'foreignKey' => '',
			'conditions' => array('Reviewer.job_id = MReviewersJob.id'),
			'fields' => '',
			'order' => ''
		),
		'MShopsBusinessCategory' => array(
			'className' => 'MShopsBusinessCategory',
			'foreignKey' => '',
			'conditions' => array('Reviewer.favorite_category_id = MShopsBusinessCategory.id'),
			'fields' => '',
			'order' => ''
		),


	);

	public $validate = array(
			'username'=>array(
					array('rule' => array('isNoTag','username'),'message'=>'タグは入力できません。'),
					array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
					array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
					array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
					array('rule' => 'notEmpty','message'=>'ログインIDを記入してください。'),
			),
			'handle'=>array(
					array('rule' => array('isNoTag','handle'),'message'=>'タグは入力できません。'),
					array('rule' => 'notEmpty','message'=>'ニックネームを記入してください。'),
					array('rule' => array('minLength',1),'message'=>'1文字以上で入力してください。'),
					array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。')
			),
			'email'=>array(
					array('rule' => 'notEmpty','message'=>'メールアドレスを記入してください。'),
					array('rule' => array('email',false, VALID_EMAIL),'message'=>'メールアドレスを入力してください。'),
					array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			),
			'large_area_id'=>array(
					array('rule' => 'notEmpty','message'=>'選択してください。'),
			),
			'age_id'=>array(
					array('rule' => 'notEmpty','message'=>'選択してください。'),
			),
			'avatar_id'=>array(
					array('rule' => 'notEmpty','message'=>'選択してください。'),
			),
			'favorite_category_id'=>array(
					array('rule' => 'notEmpty','message'=>'選択してください。'),
			),
			'job_id'=>array(
					array('rule' => 'notEmpty','message'=>'選択してください。'),
			),
			// 'title'=>array(
			// 		array('rule' => array('isNoTag','title'),'message'=>'タグは入力できません。'),
			// 		array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。')
			// ),
			'comment'=>array(
					array('rule' => array('isNoTag','comment'),'message'=>'タグは入力できません。'),
					array('rule' => array('maxLength',1024),'message'=>'1024文字以下で入力してください。')
			),
			'password_confirm'=>array(
					array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
					array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
					array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
					array('rule' => 'notEmpty','message'=>'パスワードを記入してください。'),
			),
			// 'old_password'=>array(
			// 		array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			// 		array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			// 		array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			// 		array('rule' => 'equalToPasswordOld','message'=>'旧パスワードが正しくありません。'),
			// 		array('rule' => 'notEmpty','message'=>'旧パスワードを記入してください。'),
			// ),
			// 'new_password'=>array(
			// 		array('rule' => 'equalToPasswordNew','message'=>'新しいパスワードが一致しません。'),
			// 		array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			// 		array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			// 		array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			// 		array('rule' => 'notEmpty','message'=>'新しいパスワードを記入してください。'),
			// ),
			// 'new_password_confirm'=>array(
			// 		array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			// 		array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			// 		array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			// 		array('rule' => 'notEmpty','message'=>'新しいパスワード(確認)を記入してください。')
			// ),
	);

	function equalToPasswordOld($data){
		return ($this->data['Reviewer']['password'] == $this->data['Reviewer']['old_password']);
	}
	function equalToPasswordNew($data){
		return ($this->data['Reviewer']['new_password'] == $this->data['Reviewer']['new_password_confirm']);
	}
}
?>
