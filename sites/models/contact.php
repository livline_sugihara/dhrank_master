<?php

class Contact extends AppModel {

	var $name = 'Contact';
	var $useTable = false;

	// 有料掲載希望のバリデート
	public $validate_payment_insert = array(
		'insert_area'=>array(
			array('rule' => array('isNoTag','insert_area'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'category_name'=>array(
			array('rule' => array('isNoTag','category_name'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'shop_name'=>array(
			array('rule' => array('isNoTag','shop_name'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'charge_name'=>array(
			array('rule' => array('isNoTag','charge_name'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'charge_contact'=>array(
			array('rule' => array('isNoTag','charge_contact'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'emailto'=>array(
			array('rule' => 'notEmpty','message'=>'メールアドレスを記入してください。'),
			array('rule' => array('email',false, VALID_EMAIL),'message'=>'メールアドレスを入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'contact_time'=>array(
			array('rule' => array('isNoTag','contact_time'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'shop_url'=>array(
			array('rule' => 'url','message'=>'URLを入力してください。'),
			array('rule' => array('isNoTag','shop_url'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'sendfile'=>array(
			array('rule' => array('isNotOverFileSize','sendfile',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
		),
		'comment'=>array(
			array('rule' => array('isNoTag','comment'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
	);

	// 無料掲載希望のバリデート
	public $validate_free_insert = array(
		'insert_area'=>array(
			array('rule' => array('isNoTag','insert_area'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'category_name'=>array(
			array('rule' => array('isNoTag','category_name'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'shop_name'=>array(
			array('rule' => array('isNoTag','shop_name'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'charge_name'=>array(
			array('rule' => array('isNoTag','charge_name'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'charge_contact'=>array(
			array('rule' => array('isNoTag','charge_contact'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'emailto'=>array(
			array('rule' => 'notEmpty','message'=>'メールアドレスを記入してください。'),
			array('rule' => array('email',false, VALID_EMAIL),'message'=>'メールアドレスを入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'shop_url'=>array(
			array('rule' => 'url','message'=>'URLを入力してください。'),
			array('rule' => array('isNoTag','shop_url'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'shop_tel'=>array(
			array('rule' => array('custom',VALID_PHONE),'message'=>'電話番号を正確に入力してください。'),
			array('rule' => array('isNoTag','shop_tel'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'shop_address'=>array(
			array('rule' => array('isNoTag','shop_address'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),

		'shop_business_hours'=>array(
			array('rule' => array('isNoTag','shop_business_hours'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'lowest_cost'=>array(
			array('rule' => array('isNoTag','lowest_cost'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'reserve'=>array(
			array('rule' => array('isNoTag','reserve'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'numeric','message'=>'選択して下さい。'),
		),
		'receipt'=>array(
			array('rule' => array('isNoTag','receipt'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'numeric','message'=>'選択して下さい。'),
		),
		'layover'=>array(
			array('rule' => array('isNoTag','layover'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'numeric','message'=>'選択して下さい。'),
		),
		'cosplay'=>array(
			array('rule' => array('isNoTag','cosplay'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'numeric','message'=>'選択して下さい。'),
		),
		'credit'=>array(
			array('rule' => array('isNoTag','credit'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'numeric','message'=>'選択して下さい。'),
		),
		'sendfile'=>array(
			array('rule' => array('isNotOverFileSize','sendfile',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
		),
		'comment'=>array(
			array('rule' => array('isNoTag','comment'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
	);

	// 代理店契約のバリデート
	public $validate_agency_agreement = array(
		'corp_name'=>array(
			array('rule' => array('isNoTag','corp_name'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'charge_name'=>array(
			array('rule' => array('isNoTag','charge_name'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'charge_contact'=>array(
			array('rule' => array('isNoTag','charge_contact'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'emailto'=>array(
			array('rule' => 'notEmpty','message'=>'メールアドレスを記入してください。'),
			array('rule' => array('email',false, VALID_EMAIL),'message'=>'メールアドレスを入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'corp_url'=>array(
			array('rule' => 'url','message'=>'URLを入力してください。'),
			array('rule' => array('isNoTag','corp_url'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'comment'=>array(
			array('rule' => array('isNoTag','comment'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
	);

	// 代理店契約のバリデート
	public $validate_other = array(
		'emailto'=>array(
			array('rule' => 'notEmpty','message'=>'メールアドレスを記入してください。'),
			array('rule' => array('email',false, VALID_EMAIL),'message'=>'メールアドレスを入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'charge_name'=>array(
			array('rule' => array('isNoTag','charge_name'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'comment'=>array(
			array('rule' => array('isNoTag','comment'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
	);

}