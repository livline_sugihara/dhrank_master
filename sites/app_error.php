<?php
class AppError extends ErrorHandler {
	public function dispatchMethod($method, $messages) {
		//地域取得
		$this->controller->set('parent_area', $this->controller->getLargeArea());
		
		parent::dispatchMethod($method, $messages);
	}
}	
