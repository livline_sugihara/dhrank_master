<section id="middle" class="two">
    <div class="title_bar"><?php echo count($followed_reviewer); ?>ユーザーからフォローされています。</div> 
    <div id="followArea" class="inner">

<?php
foreach($followed_reviewer AS $ii => $record) {
?>
        <div class="box clearfix">
            <div class="img">
                <a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>"><?php echo $imgCommon->get_reviewer_avatar($record, array(80,80),'img_frame',null,$record['Reviewer']['handle']); ?></a>
            </div>
            <div class="summary">
                <p class="name"><?php echo $record['Reviewer']['handle']; ?></p>
                <p>フォロワー<span class="num"><?php echo $record[0]['follower_cnt']; ?></span></p>
                <p><img src="/img/ispot_review_ico.png" alt="レビュー" height="16" width="22">レビュー投稿<?php echo $record[0]['review_cnt']; ?>件</p>
                <p><span class="category">よく利用するカテゴリ</span><span><?php echo $record['MShopsBusinessCategory']['name']; ?></span></p> 
            </div>
        </div>
<?php } ?>
    </div>
</section>