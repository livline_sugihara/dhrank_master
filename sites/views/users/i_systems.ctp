
<div style="text-align:left;padding:5px;font-size:medium;background-color:#EFEBE3">
	<?php echo $parent_shop['Shop']['name']?>の店舗情報
</div>

<?php if(!empty($system)){?>
<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	営業時間
</div>
<div style="text-align:left;padding:3px;">
	<?php if(!empty($system['MHourOpen']['id']) && !empty($system['MHourClose']['id'])){?>
	<?php //start---2013/3/11 障害No.1-4-0005修正 ?>
	<?php //if(($system['MHourOpen']['id']+1) != $system['MHourClose']['id']){?>
	<?php if($system['MHourOpen']['id'] != ($system['MHourClose']['id']+1)){?>
	<?php //end---2013/3/11 障害No.1-4-0005修正 ?>
	<?php echo $system['MHourOpen']['name'] ?>～<?php echo $system['MHourClose']['name'] ?>
	<?php }else{?>
	24時間営業
	<?php }?>
	<?php }?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	最安値料金
</div>
<div style="text-align:left;padding:3px;">
	<?php if(!empty($system['System']['price_minutes']) && !empty($system['System']['price_cost'])){?>
	<?php echo $system['System']['price_minutes'] ?>分<?php echo $system['System']['price_cost'] ?>円～
	<?php }?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	チェンジ・キャンセル
</div>
<div style="text-align:left;padding:3px;">
	<?php if(!empty($system['System']['change_id'])){?>
	チェンジ：<?php echo $change[$system['System']['change_id']]?>
	<?php if($system['System']['change_id'] == 3){?>
	<?php echo $system['System']['change_price']?>円
	<?php }?>
	<?php //start---2013/3/12 障害No.1-4-0006修正 ?>
	<?php if($system['System']['change_id'] != 1 && $system['System']['change_transportation_expenses'] == 1){?>
	別途交通費要
	<?php }?>
	<?php //end---2013/3/12 障害No.1-4-0006修正 ?>
	<?php }?>
	<?php if(!empty($system['System']['cancel_id'])){?>
	キャンセル：<?php echo $change[$system['System']['cancel_id']]?>
	<?php if($system['System']['cancel_id'] == 3){?>
	<?php echo $system['System']['cancel_price']?>円
	<?php }?>
	<?php //start---2013/3/12 障害No.1-4-0006修正 ?>
	<?php if($system['System']['cancel_id'] != 1 && $system['System']['cancel_transportation_expenses'] == 1){?>
	別途交通費要
	<?php }?>
	<?php //end---2013/3/12 障害No.1-4-0006修正 ?>
	<?php }?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	カード利用
</div>
<div style="text-align:left;padding:3px;">
	<?php if($system['System']['credit_unavailable']) echo '利用不可';?>
	<?php if($system['System']['credit_visa']) echo 'VISA';?>
	<?php if($system['System']['credit_master']) echo 'MASTER';?>
	<?php if($system['System']['credit_jcb']) echo 'JCB';?>
	<?php if($system['System']['credit_americanexpress']) echo 'American Express';?>
	<?php if($system['System']['credit_diners']) echo 'Diners';?>
	<?php if($system['System']['credit_other']) echo '<br />' . $system['System']['credit_other_comment'];?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	出張可能場所
</div>
<div style="text-align:left;padding:3px;">
	<?php echo ($system['System']['place_love_hotel'])?'○':'✕';?>ラブホテル　<?php echo ($system['System']['place_business_hotel'])?'○':'✕';?>ビジネスホテル　<?php echo ($system['System']['place_home'])?'○':'✕';?>自宅　<?php echo ($system['System']['place_meeting'])?'○':'✕';?>待ち合わせ
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	定休日
</div>
<div style="text-align:left;padding:3px;">
	<?php if($system['System']['closed_365open']) echo '年中無休';?>
	<?php if($system['System']['closed_monday']) echo '月';?>
	<?php if($system['System']['closed_tuesday']) echo '火';?>
	<?php if($system['System']['closed_wednesday']) echo '水';?>
	<?php if($system['System']['closed_thursday']) echo '木';?>
	<?php if($system['System']['closed_friday']) echo '金';?>
	<?php if($system['System']['closed_saturday']) echo '土';?>
	<?php if($system['System']['closed_sunday']) echo '日';?>
	<?php if($system['System']['closed_public_holiday']) echo '祝日';?>
	<?php if($system['System']['closed_other']) echo '<br />' . $system['System']['closed_other_comment'];?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	領収書
</div>
<div style="text-align:left;padding:3px;">
	<?php if(!empty($system['System']['receipt_id'])){?>
	<?php echo $receipt[$system['System']['receipt_id']]?>
	<?php }?>
</div>

<?php if(!empty($system['System']['comment1_title']) || !empty($system['System']['comment1_content'])){?>
<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	<?php if(!empty($system['System']['comment1_title'])){?>
	<?php echo $system['System']['comment1_title']?>
	<?php }?>
</div>
<div style="text-align:left;padding:3px;">
	<?php if(!empty($system['System']['comment1_content'])){?>
	<?php //start---2013/3/12 障害No.1-4-0007修正 ?>
	<?php //echo $system['System']['comment1_content']?>
	<?php echo nl2br($system['System']['comment1_content'])?>
	<?php //end---2013/3/12 障害No.1-4-0007修正 ?>
	<?php }?>
</div>
<?php }?>

<?php if(!empty($system['System']['comment2_title']) || !empty($system['System']['comment2_content'])){?>
<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	<?php if(!empty($system['System']['comment2_title'])){?>
	<?php echo $system['System']['comment2_title']?>
	<?php }?>
</div>
<div style="text-align:left;padding:3px;">
	<?php if(!empty($system['System']['comment2_content'])){?>
	<?php //start---2013/3/12 障害No.1-4-0007修正 ?>
	<?php //echo $system['System']['comment2_content']?>
	<?php echo nl2br($system['System']['comment2_content'])?>
	<?php //end---2013/3/12 障害No.1-4-0007修正 ?>
	<?php }?>
</div>
<?php }?>

<?php if(!empty($system['System']['comment3_title']) || !empty($system['System']['comment3_content'])){?>
<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	<?php if(!empty($system['System']['comment3_title'])){?>
	<?php echo $system['System']['comment3_title']?>
	<?php }?>
</div>
<div style="text-align:left;padding:3px;">
	<?php if(!empty($system['System']['comment3_content'])){?>
	<?php //start---2013/3/12 障害No.1-4-0007修正 ?>
	<?php //echo $system['System']['comment3_content']?>
	<?php echo nl2br($system['System']['comment3_content'])?>
	<?php //end---2013/3/12 障害No.1-4-0007修正 ?>
	<?php }?>
</div>
<?php }?>

<?php }else{?>
<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	営業時間
</div>
<div style="text-align:left;padding:3px;">
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	最安値料金
</div>
<div style="text-align:left;padding:3px;">
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	チェンジ・キャンセル
</div>
<div style="text-align:left;padding:3px;">
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	カード利用
</div>
<div style="text-align:left;padding:3px;">
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	出張可能場所
</div>
<div style="text-align:left;padding:3px;">
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	定休日
</div>
<div style="text-align:left;padding:3px;">
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	領収書
</div>
<div style="text-align:left;padding:3px;">
</div>

<?php }?>

<hr color="#DED7C8" />

<?php //echo var_dump($course)?>
<?php //echo var_dump($system)?>
