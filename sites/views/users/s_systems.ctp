
    <!-- 店舗トップ共通部分 -->
	<?php echo $this->element('s_common/shop_top'); ?>
	<!-- /店舗トップ共通部分 -->

	<section id="shop_form">

		<div class="label2_2"><h3 class="label2_ttl">お店のシステム</h3></div>

		<table>
			<tr>
				<th>店名</th>
				<td><?php echo $parent_shop['Shop']['name']; ?></td>
			</tr>
			<tr>
				<th>業種</th>
				<td><?php echo $parent_shop['MShopsBusinessCategory']['name']; ?></td>
			</tr>
			<tr>
				<th>電話番号</th>
				<td><?php echo $parent_shop['Shop']['phone_number']; ?></td>
			</tr>
			<tr>
				<th>住所</th>
				<td><?php echo $parent_shop['System']['address']; ?></td>
			</tr>
			<tr>
				<th>アクセス</th>
				<td><?php echo $parent_shop['System']['access']; ?></td>
			</tr>
<?php
$opentime = date('H:i', strtotime($parent_shop['System']['open_time']));
$closetime = date('H:i', strtotime($parent_shop['System']['close_time']));
?>
			<tr>
				<th>営業時間</th>
				<td><?php echo $opentime; ?>〜<?php echo $closetime; ?></td>
			</tr>
			<tr>
				<th>最低料金</th>
				<td><?php echo $parent_shop['System']['price_cost']; ?>円～</td>
			</tr>
            
			<tr>
				<th>在籍数</th>
				<td><?php echo $count_girls; ?>人</td>
			</tr>
			<tr>
				<th>掲載口コミ数</th>
				<td><?php echo $review_data[0]['count']; ?>件</td>
			</tr>
            
			<tr>
				<th>クレジットカード</th>
				<td>
<?php
if($parent_shop['System']['credit_unavailable'] == 1) {
?>
						ご利用出来ません。
<?php
} else {
	$arrCredit = array(
		'credit_visa' => 'VISA',
		'credit_master' => 'MasterCard',
		'credit_jcb' => 'JCB',
		'credit_americanexpress' => 'AmericanExpress',
		'credit_diners' => 'Diners'
	);
	$cnt = 0;
	foreach($arrCredit AS $key => $cardName) {
		if($parent_shop['System'][$key] == 1) {
			if($cnt > 0) {
				echo ' / ';
			}

			echo $cardName;
			$cnt++;
		}
	}
}
?>
				</td>
			</tr>
<?php if($parent_shop['UsersAuthority']['is_official_link_display'] == 1) { ?>
			<tr>
				<th>お店のホームページ</th>
				<td><a href="<?php echo $parent_shop['Shop']['url_pc']; ?>" target="_blank"><?php echo $parent_shop['Shop']['url_pc']; ?></a></td>
			</tr>
<?php } ?>
<?php
for($ii = 1; $ii <= 3; $ii++) {
	if(!empty($parent_shop['System']['comment' . $ii . '_title']) && !empty($parent_shop['System']['comment' . $ii . '_content'])) {
?>
			<tr>
				<th><?php echo $parent_shop['System']['comment' . $ii . '_title']; ?></th>
				<td><?php echo $parent_shop['System']['comment' . $ii . '_content']; ?></td>
			</tr>
<?php
	}
}
?>
			</table>

	</section>


	<!-- ライン・メール・電話 -->
	<?php echo $this->element('s_common/line_mail'); ?>
	<!-- /ライン・メール・電話 -->

	<!-- 電話をする -->
	<?php echo $this->element('s_common/shop_tel'); ?>
	<!-- /電話をする -->

	<!-- メニュー -->
	<?php echo $this->element('s_common/shop_menu'); ?>
	<!-- /メニュー -->

<?php //echo $this->element('pagenate/s_numbers'); ?>


<?php //echo var_dump($course)?>
<?php //echo var_dump($system)?>


<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../<?php echo $parent_shop['User']['id']; ?>" itemprop="url"><span itemprop="title"><?php echo $parent_shop['Shop']['name']; ?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./systems" itemprop="url"><span itemprop="title">システム</a></li>
	</ul>
</nav>
