<section id="middle" class="wide">

    <!-- 店舗トップ共通部分 -->
    <?php echo $this->element('common/shop_top'); ?>
    <!-- /店舗トップ共通部分 -->

    <!-- tabInner -->
    <div class="tabInner">
            
        <h2 class="heading">口コミレビュー</h2>
        <div class="new_reviewArea clearfix">

        <!-- レビュー -->
        <?php echo $this->element('common/_review', array('record' => $review)); ?>
        <!-- /レビュー -->

        </div>
    </div>
    <!-- /tabInner -->
    
</section>

<section id="side" class="narrow">
    <?php echo $this->element('common/side_narrow'); ?>
</section>

<?php /*

<h3 class="rank_h2" style="margin-top:5px;">女の子プロフィール</h3>

<?php echo $this->element('girl_profile'); ?>

<p class="arrow_box">
<?php if(!empty($review['Review']['reviewer_id'])){?>
<a href="<?php echo $linkCommon->get_reviews_post($review)?>"><?php echo $review['Reviewer']['handle']?></a><span style="font-size:80%"><?php if(!empty($review['Reviewer']['age'])) echo '(' . $review['Reviewer']['age'] . ')';?>さんの口コミ</span>
<?php }else{?>
<span style="font-size:80%"><?php echo $review['Review']['post_name'] ?>さんの口コミ</span>
<?php }?>
<span class="profile_preview_right"><?php echo date('Y年n月', strtotime($review['Review']['created'])) ?></span>
</p>

<br class="clear" />

<div class="profile_kuchikomi_preview">
	<div class="profile_kuchikomi_preview_wrap">
        <div class="profile_kuchikomi_preview_wrap_right">
        	<h3 class="profile_kuchikomi_preview_wrap_right_top">
            <?php echo $review['Shop']['name'] ?>
            </h3>
        	<div class="profile_kuchikomi_preview_wrap_right_middle">
            	<span class="spot2"><?php echo $review['Girl']['name'] ?></span>
            	<?php echo $review['Girl']['age'] ?>歳 &nbsp; 見た目年齢:<?php echo $review['MReviewsAge']['name'] ?>

            </div>
        	<div class="profile_kuchikomi_preview_wrap_right_middle">
                評価
               	<?php for($i = 0; $i < 5; $i++){?><img src="/images/star_<?php echo ($i+1 <= round($review[0]['girl_avg']))?'on':'off';?>.png" class="kuchikomi_heart" /><?php }?>
        	<span class="spot4 score"><?php echo round($review[0]['girl_avg'],2)?></span>
               <span style="margin-left:10px;">[第一印象:<?php echo round($review['Review']['score_girl_first_impression'],1)?>　言葉遣い:<?php echo round($review['Review']['score_girl_word'],1)?>　サービス:<?php echo round($review['Review']['score_girl_service'],1)?>　感度:<?php echo round($review['Review']['score_girl_sensitivity'],1)?>　スタイル:<?php echo round($review['Review']['score_girl_style'],1)?>　あえぎ声:<?php echo round($review['Review']['score_girl_voice'],1)?>]</span>
            </div>
            <div class="profile_kuchikomi_preview_wrap_right_bottom">
                <?php echo nl2br($review['Review']['comment']);?>
            </div>
            <div class="vote">
				<?php
				$this->Html->script('jquery');

				$this->Js->get('#good_btn');
				$request = $this->Js->request('/' . $review['User']['id'] . '/getAjax_update_good/' . $review['Review']['id'],
		        array(
		            'method' => 'get',
		            'sync' => true,
		            'update' => '#good_count',
		        )
			    );
			    $this->Js->event('click',$request);
			    echo $this->Js->writeBuffer();
			    ?>
				この口コミは参考になりましたか？：
				<span id="good_count" class="spot7">
				<?php //start---2013/4/30 障害No.4-0002修正 ?>
				<?php //echo $review[0]['reviews_good_count']?>
				<?php echo $reviews_good_count[0]['reviews_good_count_sum']?>
				<?php //end---2013/4/30 障害No.4-0002修正 ?>
				</span><span style="font-size:16px;">票</span><input id="good_btn" type="button" value="投票する"  class="btn_vote"/>
            </div>

               <br class="clear" />
        </div>
    </div>
</div>

<div align="center" style="margin-top:40px;">
<?php //start---2013/3/11 障害No.1-4-0004修正 ?>
<!-- <a href="<?php echo $linkCommon->get_user_shop($review)?>" class="btn_back">店舗トップに戻る</a> -->
<a href="<?php echo $linkCommon->get_top($review)?>" class="btn_back">トップページに戻る</a>
<?php //end---2013/3/11 障害No.1-4-0004修正 ?>
</div>


<div align="center" style="margin-top:40px;">
<a href="<?php echo $linkCommon->get_user_girl($review)?>" class="btn_back">プロフィールに戻る</a>
</div>

*/ ?>

