
<h3 class="rank_h2" style="margin-top:5px;">女の子プロフィール</h3>

<?php echo $this->element('girl_profile'); ?>


<h3 class="rank_h2" style="margin-top:5px;"><?php echo $girl['Girl']['name']?>さんの口コミ</h3>

<?php echo $form->create(null, array('type'=>'post', 'url' => '/' . $parent_shop['User']['id'] . '/add_review_girl/' . $girl['Girl']['id']));?>
<?php echo $form->hidden('Review.user_id', array('value' => $parent_shop['User']['id']));?>
<?php echo $form->hidden('Review.girl_id', array('value' => $girl['Girl']['id']));?>
<table class="review_table">
	<tr>
		<td class="review_td1"><img src="/images/icon_table.jpg" />お店</td>
		<td class="review_td2"><?php echo $parent_shop['Shop']['name']; ?></td>
	</tr>
	<tr>
		<td class="review_td1"><img src="/images/icon_table.jpg" />投稿者名</td>
		<td class="review_td2">
			<?php if($parent_reviewer){?>
			<?php echo $form->hidden('Review.reviewer_id', array('value' => $parent_reviewer['Reviewer']['id']));?>
			<?php echo $parent_reviewer['Reviewer']['handle']?>
			<?php }else{?>
			<?php echo $form->text('Review.post_name'); ?>
			<?php echo $form->error('Review.post_name'); ?>
			<span class="small spot8">※最大12文字まで入力できます。</span>
			<?php }?>
         </td>
	</tr>
	<tr>
		<td class="review_td1"><img src="/images/icon_table.jpg" />お店採点</td>
		<td class="review_td2">
       <div style="float:left;margin-right:5px;">
     <span class="review_star">電話対応</span>
     <?php echo $form->radio('Review.score_shop_tel', $m_score,array('legend' => false, 'label' => false, 'class' => 'star required', 'value' => (empty($data))?3:$data['Review']['score_shop_tel'])); ?>
	</div>
	<div style="float:left;margin-right:5px;">
	<span class="review_star">到着時間</span>
	<?php echo $form->radio('Review.score_shop_time', $m_score,array('legend' => false, 'label' => false, 'class' => 'star required', 'value' => (empty($data))?3:$data['Review']['score_shop_time'])); ?>
	</div>
	<div style="float:left;margin-right:5px;">
    <span class="review_star">コスパ</span>
    <?php echo $form->radio('Review.score_shop_cost', $m_score,array('legend' => false, 'label' => false, 'class' => 'star required', 'value' => (empty($data))?3:$data['Review']['score_shop_cost'])); ?>
	</div>
        </td>
	</tr>
	<tr>
		<td class="review_td1"><img src="/images/icon_table.jpg" />女の子</td>
		<td class="review_td2"><span class="spot6"><?php echo $girl['Girl']['name']?></span></td>
		</tr>
	<tr>
		<td class="review_td1"><img src="/images/icon_table.jpg" />見た目年齢</td>
		<td class="review_td2">
			<?php echo $form->select('Review.reviews_age_id', $m_reviews_ages,null,array('empty'=>'選択してください')); ?>
			<?php echo $form->error('Review.reviews_age_id'); ?>
			<span class="small spot8">※あくまで見た目です</span>
		</td>
	</tr>
	<tr>
		<td class="review_td1"><img src="/images/icon_table.jpg" />女の子採点</td>
		<td class="review_td2">
       <div style="float:left;margin-bottom:10px;margin-right:5px;">
     <span class="review_star">第一印象</span>
     <?php echo $form->radio('Review.score_girl_first_impression', $m_score,array('legend' => false, 'label' => false, 'class' => 'star required', 'value' => (empty($data))?3:$data['Review']['score_girl_first_impression'])); ?>
	</div>
	<div style="float:left;margin-bottom:10px;margin-right:5px;">
	<span class="review_star">言葉遣い</span>
	<?php echo $form->radio('Review.score_girl_word', $m_score,array('legend' => false, 'label' => false, 'class' => 'star required', 'value' => (empty($data))?3:$data['Review']['score_girl_word'])); ?>
	</div>
	<div style="float:left;margin-bottom:10px;margin-right:5px;">
    <span class="review_star">サービス</span>
    <?php echo $form->radio('Review.score_girl_service', $m_score,array('legend' => false, 'label' => false, 'class' => 'star required', 'value' => (empty($data))?3:$data['Review']['score_girl_service'])); ?>
	</div>
           <div style="float:left;margin-right:5px;">
     <span class="review_star">感度</span>
     <?php echo $form->radio('Review.score_girl_sensitivity', $m_score,array('legend' => false, 'label' => false, 'class' => 'star required', 'value' => (empty($data))?3:$data['Review']['score_girl_sensitivity'])); ?>
	</div>
	<div style="float:left;margin-right:5px;">
	<span class="review_star">スタイル</span>
	<?php echo $form->radio('Review.score_girl_style', $m_score,array('legend' => false, 'label' => false, 'class' => 'star required', 'value' => (empty($data))?3:$data['Review']['score_girl_style'])); ?>
	</div>
	<div style="float:left;margin-right:5px;">
    <span class="review_star">あえぎ声</span>
    <?php echo $form->radio('Review.score_girl_voice', $m_score,array('legend' => false, 'label' => false, 'class' => 'star required', 'value' => (empty($data))?3:$data['Review']['score_girl_voice'])); ?>
	</div>

        </td>
	</tr>
	<tr>
		<td class="review_td1"><img src="/images/icon_table.jpg" />コメント</td>
		<td class="review_td2">
			<?php echo $form->textarea('Review.comment',array('cols' => 40, 'rows' => 10)); ?>
			<?php echo $form->error('Review.comment'); ?>
		</td>
	</tr>
</table>

<div align="center" style="margin-top:40px;">
<?php echo $form->submit('確認する', array('name' => 'confirm', 'div' => false, 'class' => 'btn_navi')); ?>
</div>
<?php echo $form->end();?>

