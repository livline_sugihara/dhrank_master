<?php // 店舗トップ共通部分 ?>
<?php echo $this->element('s_common/shop_top'); ?>
<?php // 店舗トップ共通部分 ?>
		<div class="review_navi">
			<p class="btns"><a href="<?php echo $linkCommon->get_user_girl($review)?>" class=".button.bordered.back">口コミ一覧</a></p>
			<p>口コミ</p>
			<p></p>
		</div>
		<div class="label2_2"><h3 class="label2_ttl"><?php echo $review['Girl']['name'] ?>さんの口コミ</h3></div><br />
<?php /*
<div id="mainImages" class="mainImageInit">
	<ul>
		<?php for($i=0;$i<4;$i++){?><li id="mainImage"><?php echo $imgCommon->get_girl($girl, 'l', $i+1)?></li><?php }?>
	</ul>
</div>
*/ ?>

		<section id="review_detail_box">
           <?php echo $this->element('s_common/_review_indi', array('record' => $review)); ?>
		</section>

<?php /*
<p style="margin-top:20px; text-align:center">
	<a href="<?php echo $linkCommon->get_user_add_review_girl($girl)?>" class="btn_post"><?php echo $girl['Girl']['name']?>さんの口コミを投稿する</a>
</p>
*/ ?>

   <br style="cloar:both;" />

        <div class="page_navi2">
        	<span style="float:left;font-size:70%;margin-left:8px;"><?php echo count($all_reviews); ?>件中 <?php echo $order_num; ?>件目を表示</span><br />
			<p>
				<a href="<?php echo $linkCommon->get_user_review($back_review); ?>">
					<img src="/img/s/green_arrow2.png" style="float:left;width:8px;margin-top:3px;">前へ
				</a>
			</p>
			<p>
				<a href="<?php echo $linkCommon->get_user_girl($review); ?>">
					一覧へ
				</a>
			</p>
			<p>
				<a href="<?php echo $linkCommon->get_user_review($forward_review); ?>">
					次へ<img src="/img/s/green_arrow.png" style="float:right;width:8px;margin-top:3px;">
				</a>
			</p>
		</div>

	<p class="btn_gr">
		<!-- a href="post.html" style="color:#d26a85;font-weight:bold" -->
		<a href="<?php echo $linkCommon->s_get_post_review($review['Shop']['id'], $review['Girl']['id']); ?>">この女の子の口コミを投稿する</a></p>
           <br style="cloar:both;" />
            

<?php // 他の口コミのある女の子 ?>
<?php echo $this->element('s_common/pick_review_girl'); ?>
        
		<p class="btn_left" style="margin:2% 0;"><a href="../girls">このお店の在籍者すべてを見る</a></p>

<?php // ライン・メール・電話 ?>
<?php echo $this->element('s_common/line_mail'); ?>

<?php // 電話する ?>
<?php echo $this->element('s_common/shop_tel'); ?>

<?php // メニュー表示 ?>
<?php echo $this->element('s_common/shop_menu'); ?>

<?php // 電話する ?>
<?php echo $this->element('pagenate/s_numbers'); ?>

<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../../top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../" itemprop="url"><span itemprop="title"><?php echo $parent_shop['Shop']['name']; ?></span></a></li>
		
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">口コミ</a></li>
	</ul>
</nav>
