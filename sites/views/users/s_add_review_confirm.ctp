



<?php echo $form->create(null, array('type'=>'post', 'url' => '/s/' . $parent_shop['User']['id'] . '/add_review'));?>
<?php echo $form->hidden('Review.user_id', array('value' => $parent_shop['User']['id']));?>

<h2 class="midashih2">
	口コミ内容確認
</h2>

<h3 class="midashih3">
	お店
</h3>

<p><?php echo $parent_shop['Shop']['name']; ?></p>

<h3 class="midashih3">
	投稿者名
</h3>

<p>
	<?php if($parent_reviewer){?>
	<?php echo $form->hidden('Review.reviewer_id', array('value' => $parent_reviewer['Reviewer']['id']));?>
	<?php echo $parent_reviewer['Reviewer']['handle']?>
	<?php }else{?>
	<?php echo $data['Review']['post_name']?>
	<?php echo $form->hidden('Review.post_name');?>
	<?php }?>
</p>

<h3 class="midashih3">
	お店採点
</h3>

<p>
	電話対応：
    <?php for($i = 0; $i < 5; $i++){?><img src="/images/s/star_<?php echo ($i+1 <= $data['Review']['score_shop_tel'])?'on':'off';?>.png" width="16px" /><?php }?>
	<?php echo $form->hidden('Review.score_shop_tel');?><br />
	到着時間：
    <?php for($i = 0; $i < 5; $i++){?><img src="/images/s/star_<?php echo ($i+1 <= $data['Review']['score_shop_time'])?'on':'off';?>.png" width="16px" /><?php }?>
	<?php echo $form->hidden('Review.score_shop_time');?><br />
	コスパ　：
    <?php for($i = 0; $i < 5; $i++){?><img src="/images/s/star_<?php echo ($i+1 <= $data['Review']['score_shop_cost'])?'on':'off';?>.png" width="16px" /><?php }?>
	<?php echo $form->hidden('Review.score_shop_cost');?>
</p>

<h3 class="midashih3">
	女の子
</h3>

<p>
	<?php echo $girl['Girl']['name']?>
	<?php echo $form->hidden('Review.girl_id');?>
</p>

<h3 class="midashih3">
	見た目年齢
</h3>

<p>
	<?php echo $m_reviews_ages[$data['Review']['reviews_age_id']]?>
	<?php echo $form->hidden('Review.reviews_age_id');?>
</p>

<h3 class="midashih3">
	女の子採点
</h3>

<p>
	第一印象：
    <?php for($i = 0; $i < 5; $i++){?><img src="/images/s/star_<?php echo ($i+1 <= $data['Review']['score_girl_first_impression'])?'on':'off';?>.png" width="16px" /><?php }?>
	<?php echo $form->hidden('Review.score_girl_first_impression');?><br />
	言葉遣い：
    <?php for($i = 0; $i < 5; $i++){?><img src="/images/s/star_<?php echo ($i+1 <= $data['Review']['score_girl_word'])?'on':'off';?>.png" width="16px" /><?php }?>
	<?php echo $form->hidden('Review.score_girl_word');?><br />
	サービス：
    <?php for($i = 0; $i < 5; $i++){?><img src="/images/s/star_<?php echo ($i+1 <= $data['Review']['score_girl_service'])?'on':'off';?>.png" width="16px" /><?php }?>
	<?php echo $form->hidden('Review.score_girl_service');?><br />
	感度　　：
    <?php for($i = 0; $i < 5; $i++){?><img src="/images/s/star_<?php echo ($i+1 <= $data['Review']['score_girl_sensitivity'])?'on':'off';?>.png" width="16px" /><?php }?>
	<?php echo $form->hidden('Review.score_girl_sensitivity');?><br />
	スタイル：
    <?php for($i = 0; $i < 5; $i++){?><img src="/images/s/star_<?php echo ($i+1 <= $data['Review']['score_girl_style'])?'on':'off';?>.png" width="16px" /><?php }?>
	<?php echo $form->hidden('Review.score_girl_style');?><br />
	あえぎ声：
    <?php for($i = 0; $i < 5; $i++){?><img src="/images/s/star_<?php echo ($i+1 <= $data['Review']['score_girl_voice'])?'on':'off';?>.png" width="16px" /><?php }?>
	<?php echo $form->hidden('Review.score_girl_voice');?><br />
</p>

<h3 class="midashih3">
	コメント
</h3>

<p>
	<?php echo nl2br($data['Review']['comment'])?>
	<?php echo $form->hidden('Review.comment');?>
</p>

<div style="text-align:center; margin-right:auto; margin-left:auto;">
<p>
<?php echo $form->submit('戻る', array('name' => 'return', 'div' => false, 'class' => 'btn_post')); ?>
<?php echo $form->submit('投稿', array('name' => 'add', 'div' => false, 'class' => 'btn_post')); ?>
</p>
</div>

<?php echo $form->end();?>



