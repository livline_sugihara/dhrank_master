<section id="middle" class="wide">

    <!-- 店舗トップ共通部分 -->
    <?php echo $this->element('common/shop_top'); ?>
    <!-- /店舗トップ共通部分 -->

    <!-- tabInner -->
    <div class="tabInner">

        <!-- クーポン情報 -->
        <?php echo $this->element('common/shop_coupon'); ?>
        <!-- /クーポン情報 -->

	</div>
    <!-- /tabInner -->

</section>

<section id="side" class="narrow">
    <?php echo $this->element('common/side_narrow'); ?>
</section>
