<?php /*
	レビュアー登録時メール送信テンプレート
*/ ?>
【<?php echo $area_name; ?> デリヘル口コミランキング】 レビュアーの登録を行いました。

メールアドレス： <?php echo $email; ?> 
ログインID： <?php echo $username; ?> 
ニックネーム： <?php echo $handle; ?> 
パスワード： <?php echo $password; ?> 
----------------------
このメールアドレスは送信専用です。
