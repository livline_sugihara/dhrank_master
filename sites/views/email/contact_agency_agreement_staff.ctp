お問い合わせフォームから送信されました。
確認を行い、対応して下さい。

■お問い合せ内容
---------------------------------------
会社名：　<?php echo $data['Contact']['corp_name']; ?> 
ご担当者名：　<?php echo $data['Contact']['charge_name']; ?> 
ご担当者様のご連絡先：　<?php echo $data['Contact']['charge_contact']; ?> 
ご連絡先メールアドレス：　<?php echo $data['Contact']['emailto']; ?> 
ホームページアドレス：　<?php echo $data['Contact']['corp_url']; ?> 
ご質問・ご要望等：
<?php echo $data['Contact']['comment']; ?> 
---------------------------------------
