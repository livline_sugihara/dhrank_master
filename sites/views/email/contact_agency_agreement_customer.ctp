お問い合わせありがとうございました。
担当者からご連絡をさせていただきます。

■お問い合せ内容
---------------------------------------
会社名：　<?php echo $data['Contact']['corp_name']; ?> 
ご担当者名：　<?php echo $data['Contact']['charge_name']; ?> 
ご担当者様のご連絡先：　<?php echo $data['Contact']['charge_contact']; ?> 
ご連絡先メールアドレス：　<?php echo $data['Contact']['emailto']; ?> 
ホームページアドレス：　<?php echo $data['Contact']['corp_url']; ?> 
ご質問・ご要望等：
<?php echo $data['Contact']['comment']; ?> 
---------------------------------------
