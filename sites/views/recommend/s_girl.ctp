    <!-- recommendGirlsArea -->
    <selection id="recommendGirlsArea2">
		<div class="link_o"><h2>お店激押し売れっ子！</h2></div>

<?php
if(empty($recommend_girls)) {
?>
        <span>このエリアで登録されている激押し売れっ子はいません。</span>
<?php
} else {
?>

<?php echo $this->element('s_pagenate/recommend_girl_pagenate'); ?>

<?php
    foreach($recommend_girls AS $ii => $record) {
?>

		<div class="label_small">
			<div class="clearfix">
				<p><span class="name"><?php echo $record['Shop']['name']; ?></span><span class="number">口コミ<?php echo $record['Review']['cnt']; ?>件</span></p>
			</div>
		</div>
		
		<div class="topic_list">
			<div class="clearfix list">
			
<?php
        foreach($record['Girl'] AS $jj => $girl_record) {
            $insRecord = array('Girl' => $girl_record, 'User' => $record['User']);
?>
                <div class="col">
                	<p>
                		<a href="<?php echo $linkCommon->get_user_girl($insRecord); ?>">
                			<?php echo $imgCommon->get_girl_with_time($insRecord, array(143, 177), 1, null, 'img_frame'); ?>
                		</a>
                	</p>
               </div>
<?php
        }
?>
			</div>
<?php
    }
?>

		</div>
		
    <!-- /recommendGirlsArea -->
    </div>
<?php
}
?>
</section>

<?php echo $this->element('s_pagenate/recommend_girl_pagenate'); ?>

<?php echo $this->element('s_common/footer_banner'); ?>

<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./girl/" itemprop="url"><span itemprop="title">激押し売れっ子</span></a></li>
	</ul>
</nav>