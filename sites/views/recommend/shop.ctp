<section id="middle" class="narrow">

    <!-- recommendShopArea -->
    <div id="recommendShopArea">
<?php
if(empty($recommend_shop)) {
        echo $this->element('takamoto_recommend/' . $parent_area['LargeArea']['id']);
} else {
?>
        <div class="title title05">
            <h2>RECOMMEND SHOP<span class="small">おすすめ店舗</span></h2>
        </div>
<?php
foreach($recommend_shop AS $key => $record) {
?>
        <div class="wrapper clearfix">
            <div class="box">
                <a href="<?php echo $linkCommon->get_user_shop($record); ?>">
                    <?php echo $imgCommon->get_shop_with_time($record, 'recommend', array(291)); ?>
                </a>
                <p class="tt"><?php echo $record['Shop']['name']; ?></p>
                <p class="caption"><?php echo $categories_value_list[$record['Shop']['shops_business_category_id']]; ?></p>
                <p><?php echo $record['Shop']['about_contents']; ?></p>
            </div>
<?php
    }
}
?>
    </div>
    <!-- /recommendShopArea -->
    
</section>

<section id="side" class="wide">
	<?php echo $this->element('common/side_wide'); ?>
</section>