<section id="middle" class="full">
    
    <div id="entryArea">
 
        <!--  body -->
        <div class="body">
            <div class="listHeading">口コミ通報について</div>
            <p>この口コミを不審に感じた理由をお書き下さい。<br />
            対応につきましては「利用規約」に違反しているか、慎重に調査、検討を進めた上判断させて頂きます。<br />
            この機能の悪用も「利用規約」の違反となりますのでご注意下さい。<br />
            また通報へのご返答は出来かねますのでご了承ください。
            </p>
            
            <div class="listHeading">（入力）口コミ通報フォーム</div>
            <div class="reviewForm">
                <form action="/report/confirm" method="post">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th>レビュアー名</th>
                            <td>
                                ダミーテキスト
                            </td>
                            
                        </tr>
                        <tr>
                            <th>店名</th>
                            <td>
                                ダミーテキスト
                            </td>
                            
                        </tr>
                        <tr>
                            <th>女の子</th>
                            <td>
                                ダミーテキスト
                            </td>
                            
                        </tr>
                        <tr>
                            <th>レビュー内容</th>
                            <td>
                                ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト
                                ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト
                                ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト
                            </td>
                            
                        </tr>
                        <tr>
                            <th>通報の理由</th>
                            <td>
                            <p>
                                <input type="radio" name="q1" value="営利目的の疑い"> 営利目的の疑い
                                <input type="radio" name="q2" value="誹謗中傷"> 誹謗中傷
                                <input type="radio" name="q3" value="公序良俗に反する"> 公序良俗に反する
                                <input type="radio" name="q4" value="その他利用規約違反"> その他利用規約違反
                            </p>
                                <textarea></textarea>
                            </td>
                        </tr>
                    </table>
                	<p class="align_c"><input type="submit" class="submitBtn redBtn" value="入力内容を確認する"></p>
                </form>
            </div>

        </div>
        <!--  /body -->
        
    </div>
</section>