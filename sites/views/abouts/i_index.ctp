
<div style="text-align:left;padding:5px;font-size:medium;background-color:#EFEBE3">
	当サイトについて
</div>
<div style="text-align:left;padding:3px;">
[デリヘル人気口コミランキング]はお店や女の子へ対するファンを大切にする事をコンセプトとしており、以下の運営ポリシーを遵守しております。
</div>

<div style="text-align:left;padding:5px;font-size:medium;background-color:#EFEBE3">
	口コミ掲載方針
</div>
<div style="text-align:left;padding:3px;">
当サイトに掲載しているランキングと口コミは一般ユーザー様の口コミ・評価で成り立っております。中立・公正な運営を厳守しておりますので、運営側で特定の対象への利益になるような操作等を行うことは一切ございません。
</div>

<div style="text-align:left;padding:5px;font-size:medium;background-color:#EFEBE3">
	レビュアー会員登録するとこんな機能がご利用いただけます
</div>
<div style="text-align:left;padding:3px;">
「デリヘル人気口コミランキング」では、自分好みの風俗店探しに役立つサービスが無料でご利用できます。<br />
他のユーザーのリアルな口コミでお気に入りの風俗店をサーチ！そしてリアルな口コミを書き込み！<br />
本気で風俗を楽しみたい人のための楽しいホームページです。<br />
実際に利用した風俗店の感想を口コミし、他のユーザーと共有できます。<br />
女の子のサービス内容・ルックス・スタッフ・お店の対応などの項目ごとに5段階評価が可能。あなたが評価した結果が、お店のランキングに反映されます。<br />
<!--
口コミを投稿すると、その口コミに対してお店からあなたへ当ててメッセージが届く事があります。<br />
お店の方との交流で、さらに楽しいご利用・プレイができるかも！？<br /> -->
また、レビュアー会員のページにお気に入りのお店・女の子をブックマークし、いつでも簡単に見る事ができます。
</div>

<div style="text-align:left;padding:5px;font-size:medium;background-color:#EFEBE3">
	店舗会員登録するとこんな機能がご利用いただけます
</div>
<div style="text-align:left;padding:3px;">
「デリヘル人気口コミランキング」では、あなたのお店を直接お客様にアピールすることができます。<br />
お店の情報や女の子のプロフィールを管理画面から簡単に24時間編集可能。<br />
あなたのお店をユーザーに直接発信して、売り上げアップにお役立て下さい。
<!-- <br />
また、口コミを書き込んだレビュアー会員様に対して、管理画面から直接メッセージを送る事ができます。<br />
メッセージを送る事でお役様へ直接お店のアピールを行う事ができ、注目度がアップします！<br />
新人情報・キャンペーンの発信や、特に良い口コミを投稿されたレビュアー会員様へのお礼の送信など、店舗様が自由にアレンジされてご利用いただけます。 -->
</div>

<div style="text-align:left;padding:5px;font-size:medium;background-color:#EFEBE3">
	レビュアー会員の退会を希望される場合
</div>
<div style="text-align:left;padding:3px;">
	退会される場合はお問い合わせフォームより、退会の申請を行って下さい。
</div>
<div style="text-align:right;padding:3px;">
	<a href="<?php echo $linkCommon->get_contacts($parent_area)?>">お問い合わせ</a>へ
</div>

<div style="text-align:left;padding:5px;font-size:medium;background-color:#EFEBE3">
	お問い合わせについて
</div>
<div style="text-align:left;padding:3px;">
	当サイトへのお問い合わせはこちらより承ります。
</div>
<div style="text-align:right;padding:3px;">
	<a href="<?php echo $linkCommon->get_contacts($parent_area)?>">お問い合わせ</a>へ
</div>

<hr color="#DED7C8" />

<div style="text-align:center;padding:3px;">
	[<a href="<?php echo $linkCommon->get_top($parent_area)?>">戻る</a>]
</div>