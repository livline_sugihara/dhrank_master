<div class="mapArea">
	<ul class="list">
		<li><a href="http://shinjyuku.dhrank.com/top"><img src="/img/area/kanto/shinjyuku.png" alt="新宿エリア" height="42" width="250"></a></li>
		<li><a href="http://ikebukuro.dhrank.com/top"><img src="/img/area/kanto/ikebukuro.png" alt="池袋エリア" height="42" width="250"></a></li>
		<li><a href="http://shibuya.dhrank.com/top"><img src="/img/area/kanto/shibuya.png" alt="渋谷エリア" height="42" width="250"></a></li>
		<li><a href="http://shinagawagotanda.dhrank.com/top"><img src="/img/area/kanto/shinagawa.png" alt="品川・五反田エリア" height="42" width="250"></a></li>
		<li><a href="http://nishitokyo.dhrank.com/top"><img src="/img/area/kanto/nishitokyo.png" alt="西東京エリア" height="42" width="250"></a></li>
		<li><a href="http://kinshicho.dhrank.com/top"><img src="/img/area/kanto/kinshicho.png" alt="錦糸町エリア" height="42" width="250"></a></li>
		<li><a href="http://uenouguisudani.dhrank.com/top"><img src="/img/area/kanto/ueno.png" alt="上野・鴬谷エリア" height="42" width="250"></a></li>
		<li><a href="http://kanagawa.dhrank.com/top"><img src="/img/area/kanto/kanagawa.png" alt="神奈川エリア" height="42" width="250"></a></li>
		<li><a href="http://saitama.dhrank.com/top"><img src="/img/area/kanto/saitama.png" alt="埼玉エリア" height="42" width="250"></a></li>
		<li><a href="http://chiba.dhrank.com/top"><img src="/img/area/kanto/chiba.png" alt="千葉エリア" height="42" width="250"></a></li>
	</ul>
</div>
