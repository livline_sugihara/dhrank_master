<!-- info_bar -->
<div id="info_bar" class="in">
	<div class="container">
    	<div class="box clearfix">
			<div class="list">
				<ul class="clearfix">
                	<li<?php if($bar_select == 'top') { ?> class="selected"<?php } ?>>
                		<a href="<?php echo $linkCommon->get_mypage_top($parent_area); ?>">Myページ</a>
                	</li>
					<li<?php if($bar_select == 'shop') { ?> class="selected"<?php } ?>>
						<a href="<?php echo $linkCommon->get_mypage_favorite_shop($parent_area); ?>">お気に入りのお店</a>
					</li>
        			<li<?php if($bar_select == 'girl') { ?> class="selected"<?php } ?>>
        				<a href="<?php echo $linkCommon->get_mypage_favorite_girl($parent_area); ?>">お気に入りの女の子</a>
        			</li>
                    <li<?php if($bar_select == 'review') { ?> class="selected"<?php } ?>>
                    	<a href="<?php echo $linkCommon->get_mypage_favorite_review($parent_area); ?>">お気に入りのレビュー</a>
                    </li>
					<li><a href="<?php echo $linkCommon->get_accounts_logout($parent_area)?>">ログアウト</a></li>
                </ul>
			</div>
		</div>
	</div>
</div>
<!-- / info_bar -->