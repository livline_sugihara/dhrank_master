<form enctype="multipart/form-data" id="ShopIndexForm" method="post" action="/manager/coupon" accept-charset="utf-8">

<div class="box clearfix">
    <div class="reviewer clearfix">
        <div class="icon">
            <a href="#">
                <img src="/img/tab_topictoukou_img2.png" alt="" height="30" width="30" class="img_reviewer">
            </a>
        </div>
        <div>
            <p><a href="#"><?php echo $record['Review']['post_name']; ?></a>さん</p>
            <p><?php echo empty($record['Reviewer']['age']) ? '不明' : $record['Reviewer']['age'] . '歳'; ?>／<a href="#">口コミ投稿： ---件</a></p>
        </div>
        
    </div>
    <div class="reviewText clearfix">
        <div class="clearfix">
            <p class="img_girl">
                <?php echo $imgCommon->get_girl_with_time($record, 's', 1); ?>
            </p>
            <div class="summary">
                <p class="shop"><?php echo $record['Shop']['name']; ?></p>
                <p class="name"><?php echo $record['Girl']['name']; ?></p>
                <p class="scoreInfo">
<?php
for($ii = 1; $ii <= 5; $ii++) {
    $suffix = ($ii <= $record[0]['girl_avg']) ? 'on' : 'off';
?>
                    <img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>
                    <span class="score"><?php echo round($record[0]['girl_avg'], 2); ?></span>
                </p>
                <p class="scoreType">[<span class="point">L</span>ルックス -- /<span class="point">S</span>サービス -- /<span class="point">C</span>性格 -- ]</p>
                <div class="course">
                    <span>コース料金／--分 --円</span>
                    <span>ご利用場所／--</span>
                </div>
                <div class="course_mark">
                    <span class="target">写真指名</span>
                    <span class="new">新規</span>
                </div>
            </div>
        </div>
        <div>
            <p class="tt">【レビュー】<?php echo $record['Girl']['name']; ?>ちゃん 口コミ報告</p>
            <p class="txt"><?php echo $record['Review']['comment']; ?></p>
        </div>
    </div>

		<div style="text-align:center;margin:5px;">
			<div class="submit">
				<p><a href="#" class="member_girls_button">編集する</a> <a href="#" class="member_girls_button" onClick="disp()">削除する</a></p>
			</div>
		</div>
</div>



					
</form>
<br style="clear:both;" />