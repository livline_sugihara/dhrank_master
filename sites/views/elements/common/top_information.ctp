<div class="infomation clearfix">

    <div class="box float_l">
        <div>
            <div class="clearfix">
                <p class="img">
                    <img src="/img/ispot_information_img.png" alt="クチコミ" height="50" width="82" style="margin-right:5px;">
                </p>
                <p class="tt">
                    <a href="/shop">サンプル</a> <br />
                    <a href="shop/detail">すふれ ちゃん</a>
                </p>
                <p class="scoreInfo">
                	<span>
                    <img src="/img/star_on.png">
                    <img src="/img/star_on.png">
                    <img src="/img/star_on.png">
                    <img src="/img/star_off.png">
                    <img src="/img/star_off.png">
                    </span>
                    <span class="score">2.5</span>
                </p>
            </div>
            <p class="txt">初のお店だったから電話するまでは少し緊張。でも実際はふつうの対応だった。 この値段なら普通のサラリーマンでも…</p>
            <div class="clearfix">
                <p class="link"><a href="/shop/review">・・・続きを読む</a></p>
            </div>
        </div>
    </div>

    <div class="box float_r">
        <div>
            <div class="clearfix">
                <p class="img">
                    <img src="/img/ispot_information_img.png" alt="クチコミ" height="50" width="82" style="margin-right:5px;">
                </p>
                <p class="tt">
                    <a href="/shop">サンプル</a> <br />
                    <a href="/shop/detail">みくる ちゃん</a>
                </p>
                <p class="scoreInfo">
                	<span>
                    <img src="/img/star_on.png">
                    <img src="/img/star_on.png">
                    <img src="/img/star_on.png">
                    <img src="/img/star_off.png">
                    <img src="/img/star_off.png">
                    </span>
                    <span class="score">2.5</span>
                </p>
            </div>
            <p class="txt">翌日到着し、確認の電話を入れてからホテルにチェックイン。チェックイン後、お店に電話。ホテルと部屋の番…</p>
            <div class="clearfix">
                <p class="link"><a href="/shop/review">・・・続きを読む</a></p>
            </div>
        </div>
    </div>

</div>