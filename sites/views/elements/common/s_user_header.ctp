
<h2 class="midashih2">
<img src="/images/s/crown.png" width="14px" /><?php echo $parent_shop['Shop']['name']?><span class="small">(<?php echo $parent_shop['MShopsBusinessCategory']['name']?>)</span>
</h2>

    <div class="shop_top">
	<div class="shop_top_1">
		<span class="small">総合評価</span><?php for($i = 0; $i < 5; $i++){?><img src="/images/s/star_<?php echo ($i+1 <= round($review_data[0]['total']))?'on':'off';?>.png" width="16px" style="vertical-align:middle;" /><?php }?><span class="spot5"><?php echo round($review_data[0]['total'],2)?></span>
		<span class="small" style="float:right">[口コミ総数 <?php echo $review_data[0]['count']?>件]</span>
	</div>
	<div class="shop_top_2">
		<img src="/images/s/icon_girl.png" style="vertical-align:middle;" width="13px" />
        <span class="shop_score4">第一印象  <?php echo round($review_data[0]['score_girl_first_impression'],1)?> | 言葉遣い <?php echo round($review_data[0]['score_girl_word'],1)?> | サービス  <?php echo round($review_data[0]['score_girl_service'],1)?> | 感度 <?php echo round($review_data[0]['score_girl_sensitivity'],1)?> | スタイル  <?php echo round($review_data[0]['score_girl_style'],1)?> | あえぎ声  <?php echo round($review_data[0]['score_girl_voice'],1)?></span>
	</div>
	<div class="shop_top_2" style="margin-bottom:2px;">
		<img src="/images/s/icon_shop.png" style="vertical-align:middle;" width="13px" />
        <span class="shop_score3">電話対応 <?php echo round($review_data[0]['score_shop_tel'],1)?> | 到着時間  <?php echo round($review_data[0]['score_shop_time'],1)?> | コスパ  <?php echo round($review_data[0]['score_shop_cost'],1)?></span>
        <?php if($parent_reviewer){?>
		<?php
		$this->Html->script('jquery');

		$this->Js->get('#bookmark_shop');
		//start---2013/3/13 障害No.1-6-0002修正
		//$request = $this->Js->request('/' . $parent_shop['User']['id'] . '/getAjax_update_bookmark_shop/',
		$request = $this->Js->request('/s/' . $parent_shop['User']['id'] . '/getAjax_update_bookmark_shop/',
		//end---2013/3/13 障害No.1-6-0002修正
				array(
						'method' => 'get',
						'sync' => true,
						'update' => '#bookmark_shop',
				)
		);
		$this->Js->event('click',$request);
		echo $this->Js->writeBuffer();
		?>
		<a href="#">
		<span id="bookmark_shop">
			<?php if($bookmark_shop == 0){?>
			<img src="/images/s/bookmark.png" width="63px" />
			<?php }else{?>
			<img src="/images/s/bookmark_off.png" width="63px" />
			<?php }?>
		</span>
		</a>
		<?php }else{?>
		<a href="<?php echo $linkCommon->get_accounts_login($parent_area);?>"><img src="/images/s/bookmark.png" style="vertical-align:middle;" width="63px" /></a>
		<?php }?>
		<?php if($parent_shop['User']['is_official_link_display'] && !empty($parent_shop['Shop']['url_smartphone'])){?>
		<a href="<?php echo $parent_shop['Shop']['url_smartphone']?>" target="_blank"><img src="/images/s/official.png" style="vertical-align:middle;" width="63px" /></a>
		<?php }?>
	</div>
</div>

<div data-role="navbar">
	<ul>
		<li><a href="<?php echo $linkCommon->get_user_shop($parent_shop)?>"><img src="/images/s/top.png" width="14px" /><br />トップ</a></li>
		<li><a href="<?php echo $linkCommon->get_user_girls($parent_shop)?>"><img src="/images/s/girls.png" width="14px" /><br />女の子</a></li>
		<li><a href="<?php echo $linkCommon->get_user_systems($parent_shop)?>"><img src="/images/s/system.png" width="14px" /><br />システム</a></li>
		<li><a href="<?php echo $linkCommon->get_user_add_review($parent_shop)?>"><img src="/images/s/write.png" width="14px" /><br />投稿する</a></li>
	</ul>
</div>

<ul data-role="listview" data-divider-theme="a">
	<li data-icon="false">
    	<a href="tel:<?php echo str_replace('-','',$parent_shop['Shop']['phone_number'])?>">
            <span style="float:left;font-size:18px;margin-top:2px;">TEL <?php echo $parent_shop['Shop']['phone_number']?></span>
			<span style="float:right"><img src="/images/s/tel.png" width="24px" /></span>
		</a>
    </li>
</ul>



