<div class="reviewerIconArea">
    <?php echo $imgCommon->get_reviewer_avatar($parent_reviewer, array(100,100)); ?>
</div>
<?php if(!empty($side_select)) { ?>
<ul>
    <li<?php if($side_select == 'top') { ?> class="select"<?php } ?>><a href="<?php echo $linkCommon->get_mypage_top($parent_area); ?>">自分の更新</a></li>
    <li<?php if($side_select == 'profile') { ?> class="select"<?php } ?>><a href="<?php echo $linkCommon->get_mypage_profile($parent_area); ?>">プロフィール</a></li>
    <li<?php if($side_select == 'myreview') { ?> class="select"<?php } ?>><a href="<?php echo $linkCommon->get_mypage_myreview(); ?>">Myレビュー</a></li>
</ul>
<?php } ?>
<div class="followBox">
    <p>
        <a href="<?php echo $linkCommon->get_mypage_follow(); ?>">
            フォローしている(<?php echo count($follow_reviewer);?>)
        </a>
    </p>
    <div class="img clearfix">
<?php
foreach($follow_reviewer AS $ii => $record) {
    if($ii >= 8) break;
?>
        <p>
            <a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>">
                <?php echo $imgCommon->get_reviewer_avatar($record, array(50,50),'img_frame',null,$record['Reviewer']['handle']); ?>
            </a>
        </p>
<?php } ?>
    </div>
</div>
<div class="followBox">
    <p>
        <a href="<?php echo $linkCommon->get_mypage_follower(); ?>">
            フォローされている(<?php echo count($followed_reviewer);?>)
        </a>
    </p>
    <div class="img clearfix">
<?php
foreach($followed_reviewer AS $ii => $record) {
    if($ii >= 8) break;
?>
        <p>
            <a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>">
                <?php echo $imgCommon->get_reviewer_avatar($record, array(50,50),'img_frame',null,$record['Reviewer']['handle']); ?>
            </a>
        </p>
<?php } ?>
    </div>
</div>

<!-- recommendUserArea --> 
<div class="recommendUserArea">
    <p>おすすめのユーザー</p>

<?php foreach($recommend_reviewer AS $ii => $record) { ?>
    <div class="box clearfix">
        <div class="img">
            <a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>">
                <?php echo $imgCommon->get_reviewer_avatar($record, array(50,50),'img_frame',null,$record['Reviewer']['handle']); ?>
            </a>
        </div>
        <div class="summary">
            <p class="name"><span><?php echo $record['Reviewer']['handle']; ?></span>さん</p>
            <p class="num"><?php echo $record[0]['follower_cnt']; ?>フォロワー</p>
            <p class="followBtn followReviewer" data="<?php echo $record['Reviewer']['id']; ?>" mode="add"><a href="#">フォローする</a></p>
        </div>
    </div>
<?php } ?>
</div>
