<!-- box -->
<div class="box clearfix">
	<p class="img">
<?php if($ii + 1 <= 3) { ?>
			<img src="/img/crown_<?php echo ($ii + 1); ?>.png" alt="" width="78"><br />
<?php } else { ?>
			<p class="num"><?php echo ($ii + 1) ?></p>
<?php } ?>
		<img src="/img/<?php echo $rank_image; ?>" alt="" width="78">
	</p>
	<div class="summary">
		<p class="info">
			<span class="name">
<?php if(!empty($parent_reviewer)) { ?>
				<a href="<?php echo $linkCommon->get_reviewer_reviews($record); ?>"><?php echo $record['Reviewer']['handle']; ?></a>さん
<?php } else { ?>
			<a href="<?php echo $linkCommon->get_accounts_register(null); ?>"><?php echo $record['Reviewer']['handle']; ?></a>さん
<?php } ?>
			</span>
			<span class="num"><img src="/img/ispot_review_ico.png" alt="レビュー" height="16" width="22">レビュー投稿　<?php echo $record[0]['total_cnt']; ?>件</span>
		</p>

<?php if($record['Review']['delete_flag'] == Review::DELETE_FLAG_COMMENT_ONLY) { ?>

<?php } else if($record['Review']['delete_flag'] == Review::DELETE_FLAG_SAVE_POINTS) { ?>
						<p class="txt">このコメントは削除依頼により削除されました。</p>

<?php } else if($record['Review']['member_only'] == 1) { ?>
						<p class="tt mar_t_05"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>" style="text-decoration:none;">会員限定の記事です。</a></p>

<?php } else { ?>

		<p class="tt"><a href="#"><?php echo $record['Review']['post_title']; ?></a></p>
		<p class="txt2"><?php echo mb_substr($record['Review']['comment'], 0, 100); ?> <span>
<?php if(!empty($parent_reviewer)) { ?>
				<a href="<?php echo $linkCommon->get_reviewer_reviews($record); ?>">…続きを読む</a>
<?php } else { ?>
			<a href="<?php echo $linkCommon->get_accounts_register(null); ?>">…続きを読む</a>
<?php } ?>
<?php } ?>



		</span></p>
	</div>
</div>
