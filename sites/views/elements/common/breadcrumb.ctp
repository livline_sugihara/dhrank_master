<div id="topic_path">
<?php
foreach($breadcrumb as $val) {
	$option = (isset($val['option'])) ? $val['option'] : array();
	$link = (isset($val['link'])) ? $val['link'] : null;
	$html->addCrumb($val['name'], $link, $option);
}

if(!isset($separator)) $separator = "";
if(!isset($top)) $top = '';
echo $html->getCrumbs($separator, $top);
?>
</div>