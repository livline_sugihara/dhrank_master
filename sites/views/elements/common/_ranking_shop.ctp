	<!-- box -->
	<div class="box clearfix">
		<div class="rankNum">
<?php
if($ii + 1 <= 3) {
?>
			<img src="/img/crown_<?php echo ($ii + 1); ?>.png" alt="" width="78"><br />
<?php } else { ?>
			<p class="num"><?php echo ($ii + 1) ?></p>
<?php } ?>
			<img src="/img/<?php echo $rank_image; ?>" alt="" width="78">
		</div>
		<p class="img">
			<?php echo $imgCommon->get_shop_with_time($record, 'icon', array(80, 80)); ?>
		</p>
		<div class="summary">
			<p class="tt"><a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo $record['Shop']['name']; ?></a></p>
			<div class="scoreInfo">
				<p>
<?php
for($ii = 1; $ii <= 5; $ii++) {
    $suffix = ($ii <= $record['SummaryData']['value']) ? 'on' : 'off';
?>
                    <img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>
                    <span class="score"><?php echo round($record['SummaryData']['value'], 2); ?></span>
            	</p>
			</div>
			<p class="cat">カテゴリ：<?php echo $categories_value_list[$record['Shop']['shops_business_category_id']];?></p> 
			<p class="link"><a href="<?php echo $linkCommon->get_user_shop($record); ?>">レビュー <?php echo $record[0]['total_cnt']; ?>件</a></p>
		</div>
		<div class="comment">
			<p><?php echo nl2br($textCommon->get_substr_tagstrip_text($record['Shop']['about_contents'], 75)); ?></p>
		</div>
	</div>
