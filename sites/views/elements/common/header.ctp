<!-- header -->
<header>
	<div class="container">
		<h1><span><?php echo $parent_area['LargeArea']['name']?></span>・ランキングと口コミで探せるデリヘル情報サイト</h1>

<?php
if(isset($searchbar) && $searchbar == true) {
?>
	<?php echo $form->create(null, array('url' => '/search', 'id' => 'SearchForm')); ?>
		<!-- 検索フォーム -->
		<div class="header_form">
        	<div class="radioBox">
                <input type="radio" name="data[Search][type]" id="shop" value="shop" checked="">
                <label for="shop" class="switch-shop">店名</label>
                <input type="radio" name="data[Search][type]" id="girl" value="girl">
                <label for="girl" class="switch-girl">女の子</label>
            </div>
				<input type="text" name="data[Search][text]">
				<p class="redBtn barBtn"><a href="#" onclick="do_search();">検索</a></p>
		</div>
		<!-- / 検索フォーム -->
	<?php echo $form->end(); ?>

<?php
}
?>
	</div>
</header>
<script>
function do_search() {
	$('#SearchForm').submit();
}
</script>