<!-- 会員登録 -->
<div>
	<a href="<?php echo $linkCommon->get_accounts_register($parent_area)?>"><img src="/img/reviewer.png" alt="レビュアー会員登録はこちら" width="250"></a>
</div>
<!-- / 会員登録 -->

<?php if($parent_area['LargeArea']['display_grade'] > LargeArea::DISPLAY_GRADE_FREE) { ?>
	<!-- category list -->
	<?php echo $this->element('common/side_wide_recommend_shop'); ?>
	<!-- /category list -->

	<!-- category list -->
	<?php echo $this->element('common/side_wide_category'); ?>
	<!-- /category list -->
<?php } ?>

<!-- mapArea -->
<?php echo $this->element('side_neighbor_area/' . $parent_area['LargeArea']['id']); ?>
<!-- /mapArea -->

<?php if($parent_area['LargeArea']['display_grade'] > LargeArea::DISPLAY_GRADE_FREE) { ?>
	<!-- topicArea -->
	<?php echo $this->element('common/side_wide_topic_shop'); ?>
	<!-- /topicArea -->

	<!-- voiceArea --> 
	<?php echo $this->element('common/side_wide_reviewer'); ?>
	<!-- /voiceArea --> 
<?php } ?>