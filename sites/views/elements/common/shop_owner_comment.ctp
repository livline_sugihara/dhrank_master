<h2 class="heading">店長からのお知らせ</h2>              
<div class="noticeArea clearfix">
    <div class="img">
        <?php echo $imgCommon->get_shop_with_time($parent_shop, 'icon', array(75, 75)); ?>
        <p class="name">店舗名</p>
        <p><?php echo $parent_shop['Shop']['name']; ?></p>
    </div>
    <div class="text">
        <p><?php echo $parent_shop['Shop']['infomation']; ?></p>
    </div>
</div>
