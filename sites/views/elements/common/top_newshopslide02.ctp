<div class="title title01">
    <h2>NEW SHOPS <span class="small">新着新規掲載店</span></h2>
</div>

<?php
if(!empty($new_shops)) {
?>
<div class="list">
    <ul id="newSlideShow2" class="clearfix">
<?php
	foreach($new_shops AS $key => $record) {
?>
        <li><a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo $imgCommon->get_shop_with_time($record, 'icon'); ?></a></li>
<?php
	}
?>
    </ul>
</div>
<?php
} else {
?>
	<!-- 仮設定 -->
	<?php echo $this->element('newshopslide/' . $parent_area['LargeArea']['id']); ?>

<?php
}
?>