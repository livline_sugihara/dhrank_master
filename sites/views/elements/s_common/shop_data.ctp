		<h3 class="label">お店のデーター</h3>
		<section id="shop_form">
			<table>
				<tr>
					<th>店名</th>
					<td><?php echo $parent_shop['Shop']['name']; ?></td>
				</tr>
				<tr>
					<th>業種</th>
					<td><?php echo $parent_shop['MShopsBusinessCategory']['name']; ?></td>
				</tr>
				<tr>
					<th>電話番号</th>
					<td><?php echo $parent_shop['Shop']['phone_number']; ?></td>
				</tr>
				<tr>
					<th>住所</th>
					<td><?php echo $parent_shop['System']['address']; ?></td>
				</tr>
<?php
$opentime = date('H:i', strtotime($parent_shop['System']['open_time']));
$closetime = date('H:i', strtotime($parent_shop['System']['close_time']));
?>
				<tr>
					<th>営業時間</th>
					<td><?php echo $opentime; ?>〜<?php echo $closetime; ?></td>
				</tr>
				<tr>
					<th>最低料金</th>
					<td><?php echo $parent_shop['System']['price_cost']; ?>円～</td>
				</tr>
				</table>
			</section><!-- /#shop_form-->
