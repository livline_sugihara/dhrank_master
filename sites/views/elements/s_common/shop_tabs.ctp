	<div id="tab">
		<ul class="clearfix">
			<li<?php if($this->params['action'] == 's_index') { ?> class="select"<?php } ?>><a href="<?php echo $linkCommon->get_user_shop($parent_shop); ?>">お店情報</a></li>
			<li<?php if($this->params['action'] == 's_girls' || $this->params['action'] == 's_girl' || $this->params['action'] == 's_review') { ?> class="select"<?php } ?>><a href="<?php echo $linkCommon->get_user_girls($parent_shop); ?>">女の子一覧</a></li>
<?php
	// 有料の場合のみ
	if($parent_shop['UsersAuthority']['is_show_all_contents_by_shop'] == 1) {
?>
			<li<?php if($this->params['action'] == 's_coupons') { ?> class="select"<?php } ?>><a href="<?php echo $linkCommon->get_user_coupons($parent_shop); ?>">クーポン</a></li>
<?php } ?>
			<li<?php if($this->params['action'] == 's_systems') { ?> class="select"<?php } ?>><a href="<?php echo $linkCommon->get_user_systems($parent_shop); ?>">システム</a></li>
<?php
	// 有料の場合のみ
	if($parent_shop['UsersAuthority']['is_show_all_contents_by_shop'] == 1) {
?>
			<li class="review<?php if($this->params['action'] == 's_reviews') { ?> select<?php } ?>"><a href="<?php echo $linkCommon->get_user_reviews($parent_shop); ?>">口コミ</a></li>
<?php } ?>
		</ul>
	</div>
