	<div class="box_inner">
		<div class="pickup_box_summary">
			<div class="clearfix">
<?php if($record['Reviewer']['handle'] == null) { ?>
						<p class="profile_img"><?php echo $imgCommon->get_reviewer_avatar($record, array(30,30), 'img_reviewer'); ?></p>
						<p class="name"><?php echo $record['Review']['post_name']; ?>さん</p>
<?php } else { ?>
						<p class="profile_img"><a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>"><?php echo $imgCommon->get_reviewer_avatar($record, array(30,30), 'img_reviewer'); ?></a></p>
						<p class="name"><?php echo $record['Reviewer']['handle']; ?>さん</p>
<?php } ?>
					<p class="font08"><?php echo $textCommon->escape_empty($record, 'MReviewersAge.name', '年齢不明'); ?>
						 | <?php if(empty($record['MReviewersJob']['name'])) { echo '職業不明'; } else { echo $record['MReviewersJob']['name']; } ?>
						 | 口コミ:<?php if($record['Review']['reviewer_id'] == 0) { echo '非会員'; } else { echo $record[0]['review_cnt'] . '件'; } ?>
					</p>
		</div>
	</div>
	<div class="clearfix links link_arrow">
		<p class="img"><?php echo $imgCommon->get_girl_with_time($record, array(90, 120), 1); ?></p>
		<div class="summary">
			<p class="caption"><a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo $record['Girl']['name']; ?></a><span class="font10">-  <?php echo $record['Girl']['age']; ?>歳</span></p>
			<p class="review">
<?php
for($ii = 1; $ii <= 5; $ii++) {
    $suffix = ($ii <= $record[0]['girl_avg']) ? 'on' : 'off';
?>
					<img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>
				<span class="cow"><?php echo round($record[0]['girl_avg'], 2); ?></span>
				<span class="course_mark" style="margin-top:4px;">
					<span class="target"><?php echo $textCommon->escape_empty($record, 'Review.appointed_sub_name', '不明'); ?></span>
					<span class="new"><?php echo $textCommon->escape_empty($record, 'Review.appointed_type_name', '不明'); ?></span>
				</span>
			</p>

<?php if($record['Review']['delete_flag'] == Review::DELETE_FLAG_COMMENT_ONLY) { ?>

<?php } else if($record['Review']['delete_flag'] == Review::DELETE_FLAG_SAVE_POINTS) { ?>
						<p class="txt">このコメントは削除依頼により削除されました。</p>

<?php } else if($record['Review']['member_only'] == 1) { ?>
						<p class="tt mar_t_05"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>" style="text-decoration:none;">会員限定の記事です。</a></p>

<?php } else { ?>
			<p class="tt mar_t_05"><?php echo $record['Review']['post_title']; ?></p>
			<p class="txt"><?php echo mb_strimwidth($record['Review']['comment'], 0, 100, '...', 'utf-8'); ?></p>
			<p class="font10 day"><?php echo date('Y年n月d日', strtotime($record['Review']['created'])); ?></p>
<?php } ?>

		</div>
	</div>
</div>
