<?php
    $opentime = date('H:i', strtotime($record['System']['open_time']));
    $closetime = date('H:i', strtotime($record['System']['close_time']));
?>
        <!-- shop -->
		<div class="rank_box underline">
			<div class="clearfix">
                <p class="shop_tt f_left">
                	<a href="<?php echo $linkCommon->get_user_shop($record); ?>" class="shopName">
                		<?php echo $record['Shop']['name']; ?>
                	</a>
                </p>

                <p class="whiteBtn delete2Btn bookmarkShop" data="<?php echo $record['Shop']['id']; ?>" mode="delete" style="margin-top:10px;">
                    <a href="#">お気に入り削除</a>
                </p>
            </div>
        </div>
        <!-- /shop -->
