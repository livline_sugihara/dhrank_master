	<header>
		<div class="clearfix">
			<div id="head_left">
				<h1><a href="/s/top/"><img src="/img/s/deri_logo.png" height="42"></a></h1>
				<p>国内最大級の<br>デリヘル口コミサイト</p>
			</div>
			<div id="head_account_right">
				<div class="button-toggle">&#9776;</div>
			</div>
		</div>
	</header>
	<div class="menu">
		<ul>
			<li><a href="<?php echo $linkCommon->get_mypage_top($record = null)?>">My Top</a></li>
			<li><a href="<?php echo $linkCommon->get_reviewer_profile($reviewer_data); ?>">プロフィール</a></li>
			<li><a href="<?php echo $linkCommon->get_reviewer_reviews($reviewer_data)?>">レビュー</a></li>
			<li><a href="<?php echo $linkCommon->get_reviewer_follow($reviewer_data )?>">フォローしている</a></li>
			<li><a href="<?php echo $linkCommon->get_reviewer_follower($reviewer_data)?>">フォローされている</a></li>
			<li class="menu_last"><a href="<?php echo $linkCommon->get_accounts_logout($record = null)?>">ログアウト</a></li>
		</ul>
	</div>
