<!-- middle -->
<section id="middle" class="two">
    <div class="title_bar">Myレビュー</div> 
    <div id="reviewListArea" class="inner">
    
        <div class="new_reviewArea clearfix">
            
<?php
$cnt = count($self_review);
for ($ii = 0; $ii < $cnt; $ii++) {
            echo $this->element('common/_review', array("record" => $self_review[$ii], 'myreview' => true));
}     
?>
            
        </div>
                        
    </div>
</section>