<section id="middle" class="two">
    <div class="title_bar">プロフィール<span><a href="<?php echo $linkCommon->get_mypage_profile_edit(); ?>">編集する</a></span></div> 
    <div class="inner profileTable">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <th>年代</th>
                <td><?php echo $parent_reviewer['MReviewersAge']['name']; ?></td>
                <th>職業</th>
                <td><?php echo $parent_reviewer['MReviewersJob']['name']; ?></td>
            </tr>
            <tr>
                <th>ニックネーム</th>
                <td><?php echo $parent_reviewer['Reviewer']['handle']; ?></td>
                <th>地域</th>
                <td><?php echo $parent_reviewer['LargeArea']['name']; ?></td>
            </tr>
            <tr>
                <th>よく利用する<br>カテゴリ</th>
                <td><?php echo $parent_reviewer['MShopsBusinessCategory']['name']; ?></td>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th>自己紹介</th>
                <td colspan="3">
                    <?php echo $parent_reviewer['Reviewer']['comment']; ?>
                </td>
            </tr>
        </table>        
    </div>
</section>