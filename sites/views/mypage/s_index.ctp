	<div class="label"><h2>自分の更新</h2></div>

	<section id="account_box">

<?php
foreach($list AS $datetime => $data) {
    $type = $data['type'];
    $record = $data['record'];
?>

		<div class="list">
		<div class="account_box">
			<div class="clearfix">
				<div class="head">
                <p class="user_img">
                    <?php echo $imgCommon->get_reviewer_avatar($parent_reviewer, array(40,40), 'img_reviewer'); ?>
                </p>            
<?php
    if($type == 'review') {
?>
					<div class="head_summary">
						<p class="date"><?php echo date('Y/m/d H:i', strtotime($datetime)); ?></p>
						<p><?php echo $record['Review']['post_name']; ?>さんのレビューをお気に入りに追加しました。</p>
                </div>
            </div>
			<?php echo $this->element('s_common/_review', array('record' => $record)); ?>
                </div>
            </div>
			
<?php
    } else if($type == 'girl') {
?>
                <div class="head_summary">
                    <p class="date"><?php echo date('Y/m/d H:i', strtotime($datetime)); ?></p>
                    <p class="tt">「<?php echo $record['Shop']['name']; ?>」の<?php echo $record['Girl']['name']; ?>ちゃんをお気に入りに追加しました。</p>
		            <p id="shop_img" class="center">
        		    	<a href="<?php echo $linkCommon->get_user_girl($record); ?>">
	            			<?php echo $imgCommon->get_girl_with_time($record, 'l', 1, null, 'img_frame'); ?>
	            		</a>
            		</p>
                </div>
            </div>
                        
<?php
    } else if($type == 'shop') {
?>        
                <div class="head_summary">
                    <p class="date"><?php echo date('Y/m/d H:i', strtotime($datetime)); ?></p>
                    <p class="tt">「<?php echo $record['Shop']['name']; ?>」をお気に入りに追加しました。</p>
<?php
        if($record['UsersAuthority']['is_show_all_contents_by_categorylist'] == 1) {
?>
            <?php echo $this->element('s_common/_shop_payment', array('record' => $record, 'reviewer' => true)); ?>
<?php
        } else {
?>
            <?php echo $this->element('s_common/_shop_free', array('record' => $record)); ?>
<?php
        }
    }
?>

                </div>
            </div>
             </div> </div>
            
<?php /*
            <!-- pay_shopList -->
            <div id="pay_shopList">
                            
                <!--shop-->
                <div class="shopList_frame">
                    <div>
                        <a href="/shop" class="shopName">サンプルショップ</a>　<span class="kuchikomi">UP</span><a href="/shop" class="css_btn_class">詳細を見る</a>
                    </div>
                    <div class="pay_mark clearfix">
                        <p class="pay_yoyaku">先行予約OK</p>
                        <p class="pay_credit">クレジットOK</p>
                        <p class="pay_ryoshu">領収書OK</p>
                        <p class="pay_machi">待ち合わせOK</p>
                        <p class="pay_cosplay">コスプレあり</p>
                        <p class="pay_coupon">クーポンあり</p>
                    </div>
                </div>
                
                <div class="shopList_main narrow clearfix">
                    <div>
                        <p class="catchCopy"><b><strong>キャッチコピーが入りますキャッチコピーが入りますキャッチコピーが入りますキャッチコピーが入ります</strong></b></p>
                        <p>説明文が入ります説明文が入ります説明文が入ります説明文が入ります説明文が入ります説明文が入ります</p>
                        
                        <div class="shopListLeft">
                            <dl class="clearfix">
                                <dt>営業時間</dt>
                                <dd>12:00〜0:00</dd>
                            
                                <dt>最低料金</dt>
                                <dd>16,000円〜</dd>

                                <dt>住所</dt>
                                <dd>駅前発</dd>
                            </dl>   
                            <p class="shopNum">
                                <span class="zaiseki">在籍数:</span><span class="num">25人</span>
                                <span class="kuchikomi2">口コミ:</span><span class="num">90件</span>
                            </p>
                        </div>
                        <div class="shopListRight">
                            <!-- p><img src="/img/shop_banner.jpg" /></p -->
                        </div>
                        <div class="shopListBottom clearfix">
                            <ul>
                                <li><span class="type1">新規</span><a href="/shop/coupon">ダミーテキストダミーテキストダミーテキストダミーテキスト</a></li>
                                <li><span class="type2">会員</span><a href="/shop/coupon">ダミーテキストダミーテキスト</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /shop -->
            </div>
<?php */ ?>
<?php
}
?>
        </div>
    </div>
</section>

	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile($record = null)?>">My Top</a></p>
<!--	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_top($record = null)?>">自分の更新</a></p> -->
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_myreview($record = null)?>">Myレビュー</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_follow($record = null)?>">フォローしている</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_follower($record = null)?>">フォローされている</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_shop($record = null)?>">お気に入りのお店</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_girl($record = null)?>">お気に入りの女の子</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_review($record = null)?>">お気に入りのレビュー</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile_edit($record = null)?>">会員情報の確認・変更</a></p>