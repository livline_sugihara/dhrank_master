<?php
class BreadcrumbListHelper extends Helper {
	var $helpers = array('Html','LinkCommon');

	//店舗情報ヘッダ(PC)
	function get_breadcrumb_list(){

		//viewに渡されたデータを取得
		$view =& ClassRegistry::getObject('view');

		//全国TOPのパンくずリスト追加
		$this->Html->addCrumb('年齢認証', APP_ALLAREA_URL);
		$this->Html->addCrumb('全国デリヘル情報', APP_ALLAREA_URL . 'top');
		$this->Html->addCrumb($view->viewVars['parent_area']['LargeArea']['name'] . 'デリヘル口コミランキング', $this->LinkCommon->get_top($view->viewVars['parent_area']));

		switch($this->params["controller"]){
			//店舗関連ページ
			case 'users':
				//店舗TOPをパンくずリスト追加
				$this->Html->addCrumb($view->viewVars['parent_shop']['Shop']['name'], $this->LinkCommon->get_user_shop($view->viewVars['parent_shop']));
				switch($this->params["action"]){
					case 'index':
						break;
					case 'girls':
						$this->Html->addCrumb('女の子', $this->LinkCommon->get_user_girls($view->viewVars['parent_shop']));
						break;
					case 'girl':
						$this->Html->addCrumb('女の子', $this->LinkCommon->get_user_girls($view->viewVars['parent_shop']));
						$this->Html->addCrumb($view->viewVars['girl']['Girl']['name'] . 'さん', $this->LinkCommon->get_user_girl($view->viewVars['girl']));
						break;
					case 'review':
						$this->Html->addCrumb('女の子', $this->LinkCommon->get_user_girls($view->viewVars['parent_shop']));
						$this->Html->addCrumb($view->viewVars['girl']['Girl']['name'] . 'さん', $this->LinkCommon->get_user_girl($view->viewVars['girl']));
						break;
					case 'systems':
						$this->Html->addCrumb('システム', $this->LinkCommon->get_user_systems($view->viewVars['parent_shop']));
						break;
					case 'add_review':
						$this->Html->addCrumb('体験談を投稿する', $this->LinkCommon->get_user_add_review($view->viewVars['parent_shop']));
						break;
					case 'add_review_confirm':
						$this->Html->addCrumb('体験談を投稿する', $this->LinkCommon->get_user_add_review($view->viewVars['parent_shop']));
						break;
					case 'add_review_end':
						$this->Html->addCrumb('体験談を投稿する', $this->LinkCommon->get_user_add_review($view->viewVars['parent_shop']));
						break;
					case 'add_review_girl':
						$this->Html->addCrumb('女の子', $this->LinkCommon->get_user_girls($view->viewVars['parent_shop']));
						$this->Html->addCrumb($view->viewVars['girl']['Girl']['name'] . 'さん', $this->LinkCommon->get_user_girl($view->viewVars['girl']));
						$this->Html->addCrumb('体験談を投稿する', $this->LinkCommon->get_user_add_review_girl($view->viewVars['girl']));
						break;
					case 'add_review_girl_confirm':
						$this->Html->addCrumb('女の子', $this->LinkCommon->get_user_girls($view->viewVars['parent_shop']));
						$this->Html->addCrumb($view->viewVars['girl']['Girl']['name'] . 'さん', $this->LinkCommon->get_user_girl($view->viewVars['girl']));
						$this->Html->addCrumb('体験談を投稿する', $this->LinkCommon->get_user_add_review_girl($view->viewVars['girl']));
						break;
					case 'add_review_girl_end':
						$this->Html->addCrumb('女の子', $this->LinkCommon->get_user_girls($view->viewVars['parent_shop']));
						$this->Html->addCrumb($view->viewVars['girl']['Girl']['name'] . 'さん', $this->LinkCommon->get_user_girl($view->viewVars['girl']));
						$this->Html->addCrumb('体験談を投稿する', $this->LinkCommon->get_user_add_review_girl($view->viewVars['girl']));
						break;
					case 'add_review_error':
						break;
				}
				break;
			case 'ranking':
				//ランキング
				switch($this->params["action"]){
					case 'index':
						$this->Html->addCrumb($view->viewVars['m_rank_business_category']['MShopsBusinessCategory']['name'] . 'ランキング', $this->LinkCommon->get_ranking($view->viewVars['parent_area'], $this->params['business_category_id']));
						break;
				}
				break;
			case 'reviews':
				//口コミ関連
				switch($this->params["action"]){
					case 'list_post':
						$this->Html->addCrumb($view->viewVars['reviewer']['Reviewer']['handle'] . 'さんのデリヘルガイド', $this->LinkCommon->get_reviews_post($view->viewVars['reviewer']));
						break;
				}
				break;
			case 'shops':
				//お店を探す
				switch($this->params["action"]){
					case 'index':
						$this->Html->addCrumb('お店一覧', $this->LinkCommon->get_shops($view->viewVars['parent_area']));
						break;
				}
				break;
			case 'boards':
				//掲示板
				$this->Html->addCrumb('炎上掲示板', $this->LinkCommon->get_boards_top($view->viewVars['parent_area']));
				switch($this->params["action"]){
					case 'index':
						break;
					case 'read':
						$pass = '';
						//start---2013/3/10 障害No.3-0008修正
						//if(!empty($this->params['pass'][1])){
						//	$pass = '/' . $this->params['pass'][1];
						//}
						if(!empty($this->params['pass'][0])){
							$pass = '/' . $this->params['pass'][0];
						}
						//end---2013/3/10 障害No.3-0008修正
						$this->Html->addCrumb($view->viewVars['board']['Board']['title'], $this->LinkCommon->get_boards_read($view->viewVars['board']) . $pass);
						break;
					case 'addend':
						break;
					case 'add_res_end':
						$this->Html->addCrumb($view->viewVars['board']['Board']['title'], $this->LinkCommon->get_boards_read($view->viewVars['board']));
						break;
					case 'all_list':
						$this->Html->addCrumb('スレッド一覧', $this->LinkCommon->get_boards_all_list($view->viewVars['parent_area']));
						break;
					case 'search_result':
						break;
					case 'error':
						break;
				}
				break;
			case 'abouts':
				//当サイトについて
				switch($this->params["action"]){
					case 'index':
						$this->Html->addCrumb('当サイトについて', $this->LinkCommon->get_abouts($view->viewVars['parent_area']));
						break;
				}
				break;
			case 'terms':
				//ご利用規約
				switch($this->params["action"]){
					case 'index':
						$this->Html->addCrumb('ご利用規約', $this->LinkCommon->get_terms($view->viewVars['parent_area']));
						break;
				}
				break;
			case 'contacts':
				//お問い合わせ
				switch($this->params["action"]){
					case 'index':
						$this->Html->addCrumb('お問い合わせ', $this->LinkCommon->get_contacts($view->viewVars['parent_area']));
						break;
					case 'end':
						$this->Html->addCrumb('お問い合わせ', $this->LinkCommon->get_contacts($view->viewVars['parent_area']));
						$this->Html->addCrumb('お問い合わせ完了', $this->LinkCommon->get_contacts_end($view->viewVars['parent_area']));
						break;
				}
				break;
			case 'accounts':
				//レビュアーアカウント関連
				switch($this->params["action"]){
					case 'login':
						$this->Html->addCrumb('レビュアー会員ログイン', $this->LinkCommon->get_accounts_login($view->viewVars['parent_area']));
						break;
					case 'register':
						$this->Html->addCrumb('レビュアー会員新規登録', $this->LinkCommon->get_accounts_register($view->viewVars['parent_area']));
						break;
					case 'register_end':
						$this->Html->addCrumb('レビュアー会員新規登録', $this->LinkCommon->get_accounts_register($view->viewVars['parent_area']));
						break;
					case 'register_input':
						$this->Html->addCrumb('レビュアー会員新規登録', $this->LinkCommon->get_accounts_register($view->viewVars['parent_area']));
						break;
					case 'register_input_end':
						$this->Html->addCrumb('レビュアー会員新規登録', $this->LinkCommon->get_accounts_register($view->viewVars['parent_area']));
						break;
					case 'send_loginid':
						$this->Html->addCrumb('レビュアー会員ログイン', $this->LinkCommon->get_accounts_login($view->viewVars['parent_area']));
						$this->Html->addCrumb('ログインIDの再送信', $this->LinkCommon->get_accounts_send_loginid($view->viewVars['parent_area']));
						break;
					case 'send_loginid_end':
						$this->Html->addCrumb('レビュアー会員ログイン', $this->LinkCommon->get_accounts_login($view->viewVars['parent_area']));
						$this->Html->addCrumb('ログインIDの再送信', $this->LinkCommon->get_accounts_send_loginid($view->viewVars['parent_area']));
						break;
					case 'changepassword_send':
						$this->Html->addCrumb('レビュアー会員ログイン', $this->LinkCommon->get_accounts_login($view->viewVars['parent_area']));
						$this->Html->addCrumb('パスワード変更手続き', $this->LinkCommon->get_accounts_changepassword_send($view->viewVars['parent_area']));
						break;
					case 'changepassword_send_end':
						$this->Html->addCrumb('レビュアー会員ログイン', $this->LinkCommon->get_accounts_login($view->viewVars['parent_area']));
						$this->Html->addCrumb('パスワード変更手続き', $this->LinkCommon->get_accounts_changepassword_send($view->viewVars['parent_area']));
						break;
					case 'changepassword_input':
						$this->Html->addCrumb('レビュアー会員ログイン', $this->LinkCommon->get_accounts_login($view->viewVars['parent_area']));
						$this->Html->addCrumb('パスワード変更手続き', $this->LinkCommon->get_accounts_changepassword_send($view->viewVars['parent_area']));
						break;
					case 'changepassword_input_end':
						$this->Html->addCrumb('レビュアー会員ログイン', $this->LinkCommon->get_accounts_login($view->viewVars['parent_area']));
						$this->Html->addCrumb('パスワード変更手続き', $this->LinkCommon->get_accounts_changepassword_send($view->viewVars['parent_area']));
						break;
					case 'changeemail_end':
						$this->Html->addCrumb('レビュアー会員ログイン', $this->LinkCommon->get_accounts_login($view->viewVars['parent_area']));
						break;
					case 'error':
						$this->Html->addCrumb('レビュアー会員ログイン', $this->LinkCommon->get_accounts_login($view->viewVars['parent_area']));
						break;
				}
				break;
		}
		return $this->Html->getCrumbs(' &gt; ','');

	}
}
