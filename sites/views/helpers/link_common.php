<?php
class LinkCommonHelper extends Helper {
	var $helpers = array('Html');

	private function get_device_path($non_s_plefix = null){
		if(!empty($this->params['prefix'])){
			if($this->params['prefix'] == 's' && $non_s_plefix == true){
				return '';
			}else{
				return '/' . $this->params['prefix'];
			}
		}
	}

	//トップURL取得
	function get_top($record = null){
		return $this->output($this->get_device_path() . '/top');
	}

	//お店一覧URL取得
	function get_shops($record = null){
		return $this->output($this->get_device_path() . '/shops');
	}

	//かな別お店一覧URL取得
	function get_shops_kana($record, $kana_key){
		return $this->output($this->get_device_path() . '/shops/kana/' . $kana_key);
	}

	//ランキングURL取得
	function get_ranking_category($record){
		return $this->output($this->get_device_path() . '/ranking/category/' . $record['MShopsBusinessCategory']['id']);
	}

	//投稿者別口コミURL取得
	// function get_reviews_post($record){
	// 	return $this->output($this->get_device_path() . '/reviews/list_post/' . $record['Reviewer']['id']);
	// }

	//掲示板TOP
	// function get_boards_top($record){
	// 	return $this->output($this->get_device_path() . '/boards');
	// }

	//新規スレッド作成
	// function get_boards_add_board($record){
	// 	return $this->output($this->get_device_path() . '/boards/add_board');
	// }

	//レス投稿
	// function get_boards_add_res($record){
	// 	return $this->output($this->get_device_path() . '/boards/add_res/' . $record['Board']['id']);
	// }

	//掲示板詳細表示画面
	// function get_boards_read($record){
	// 	return $this->output($this->get_device_path() . '/boards/read/' . $record['Board']['id']);
	// }

	//掲示板一覧画面
	// function get_boards_all_list($record){
	// 	return $this->output($this->get_device_path() . '/boards/all_list');
	// }

	//当サイトについて
	function get_abouts($record = null){
		return $this->output($this->get_device_path() . '/front/about');
	}

	//ご利用規約
	function get_terms($record = null){
		return $this->output($this->get_device_path() . '/front/term');
	}

	//お問い合わせ
	function get_contacts($record = null){
		return $this->output($this->get_device_path() . '/contact');
	}

	//お問い合わせ完了
	function get_contacts_end($record = null){
		return $this->output($this->get_device_path() . '/contact/end');
	}

	//カテゴリー詳細URL取得
	function get_category_detail($business_category_id){
		return $this->output($this->get_device_path() . '/category/' . $business_category_id);
	}

	/*
	 * ここから店舗ページ
	*/
	//ショップURL取得
	function get_user_shop($record){
		return $this->output($this->get_device_path() . '/' . $record['User']['id']);
	}

	//女の子一覧URL取得
	function get_user_girls($record){
		return $this->output($this->get_device_path() . '/' . $record['User']['id'] . '/girls');
	}

	//システムURL取得
	function get_user_systems($record){
		return $this->output($this->get_device_path() . '/' . $record['User']['id'] . '/systems');
	}

	//クーポンURL(単発)URL取得 ※タカモト
	function get_user_coupon($record){
		return $this->output($this->get_device_path() . '/' . $record['User']['id'] . '/coupon/' . $record['Coupon']['id']);
	}

	//クーポンURL取得
	function get_user_coupons($record){
		return $this->output($this->get_device_path() . '/' . $record['User']['id'] . '/coupons');
	}

	//口コミ(単発)URL取得
	function get_user_review($record){

		if(empty($record)) {
			return 'javascript:void(0);';
		}
		return $this->output($this->get_device_path() . '/' . $record['User']['id'] . '/review/' . $record['Review']['id']);
	}

	//口コミ(一覧)URL取得
	function get_user_reviews($record){
		return $this->output($this->get_device_path() . '/' . $record['User']['id'] . '/reviews');
	}

	//女の子URL取得
	function get_user_girl($record){
		return $this->output($this->get_device_path() . '/' . $record['User']['id'] . '/girls/' . $record['Girl']['id']);
	}

	//店舗ブックマーク追加・解除完了画面（携帯）
	function get_user_add_shops_bookmark_end($record){
		return $this->output($this->get_device_path() . '/' . $record['User']['id'] . '/add_shops_bookmark_end');
	}

	//女の子ブックマーク追加・解除完了画面（携帯）
	function get_user_add_girls_bookmark_end($record){
		return $this->output($this->get_device_path() . '/' . $record['User']['id'] . '/add_girls_bookmark_end/' . $record['Girl']['id']);
	}

	//口コミ投票完了画面（携帯）
	function get_user_add_good_count_end($record){
		return $this->output($this->get_device_path() . '/' . $record['User']['id'] . '/add_good_count_end/' . $record['Review']['id']);
	}

	//女の子画像URL取得
	function get_girls_image($record,$size,$number){
		if(!empty($record['Girl']['image_' . $number . '_' . $size])){
			return $this->output(
					//start---2013/4/25 障害No.4-0001修正
					//APP_IMG_URL . 'girl/' . $record['Girl']['id'] . '/' . $record['Girl']['image_' . $number . '_' . $size]);
					APP_IMG_URL . 'girl/' . $record['Girl']['user_id'] . '/' . $record['Girl']['id'] . '/' . $record['Girl']['image_' . $number . '_' . $size]);
					//end---2013/4/25 障害No.4-0001修正
		}else{
			return $this->output(
					APP_IMG_URL . 'common/girl_' . $size . '.jpg');
		}
		return $this->output($this->get_device_path() . '/girls/image/' . $record['Girl']['id'] . '/' . $filename);
	}
	/*
	 * ここからマイページ
	*/

	//レビュアーTOP ※02230223麻生編集
	function get_mypage_top($record = null){
		return $this->output($this->get_device_path() . '/mypage');
	}
	
	//プロフィール確認 ※02230223麻生編集
	function get_mypage_profile($record = null){
		return $this->output($this->get_device_path() . '/mypage/profile');
	}

	//プロフィール編集 ※02230223麻生編集
	function get_mypage_profile_edit($record = null){
		return $this->output($this->get_device_path() . '/mypage/profile_edit');
	}

	// Myレビュー ※02230223麻生編集
	function get_mypage_myreview($record = null){
		return $this->output($this->get_device_path() . '/mypage/myreview');
	}

	//レビュアー口コミ編集 ※02230223麻生編集
	function get_mypage_edit_review($record){
		return $this->output($this->get_device_path() . '/mypage/myreview_edit/' . $record['Review']['id']);
	}

	// レビュアー口コミ削除 ※02230223麻生編集
	function get_mypage_delete_review($record){
		return $this->output($this->get_device_path() . '/mypage/myreview_delete/' . $record['Review']['id']);
	}

	// レビュアーお気に入りのお店 ※02230223麻生編集
	function get_mypage_favorite_shop($record = null){
		return $this->output($this->get_device_path() . '/mypage/favorite_shop');
	}

	// レビュアーお気に入りの女の子 ※02230223麻生編集
	function get_mypage_favorite_girl($record = null){
		return $this->output($this->get_device_path() . '/mypage/favorite_girl');
	}

	// レビュアーお気に入りのレビュー ※02230223麻生編集
	function get_mypage_favorite_review($record = null){
		return $this->output($this->get_device_path() . '/mypage/favorite_review');
	}

	// レビュアーがフォローしているレビュアー一覧 ※02230223麻生編集
	function get_mypage_follow($record = null){
		return $this->output($this->get_device_path() . '/mypage/follow');
	}

	// レビュアーがフォローされているレビュアー一覧 ※02230223麻生編集
	function get_mypage_follower($record = null){
		return $this->output($this->get_device_path() . '/mypage/follower');
	}

	/*
	 * ここから他レビュアーページ
	*/
	//プロフィール確認
	function get_reviewer_profile($record = null){
		return $this->output($this->get_device_path() . '/reviewer/profile/' . $record['Reviewer']['id']);
	}

	// 口コミ一覧
	function get_reviewer_reviews($record = null){
		return $this->output($this->get_device_path() . '/reviewer/reviews/' . $record['Reviewer']['id']);
	}

	// レビュアーがフォローしているレビュアー一覧
	function get_reviewer_follow($record = null){
		return $this->output($this->get_device_path() . '/reviewer/follow/' . $record['Reviewer']['id']);
	}

	// レビュアーがフォローされているレビュアー一覧
	function get_reviewer_follower($record = null){
		return $this->output($this->get_device_path() . '/reviewer/follower/' . $record['Reviewer']['id']);
	}


	/**
	 * アカウント管理
	 */
	//ログイン画面 ※2015/02/23 01:16 true削除しました。タカモト
	function get_accounts_login($record = null){
		return $this->output($this->get_device_path() . '/accounts/login');
	}

	//ログイン画面
	function get_accounts_login_s($record){
		return $this->output($this->get_device_path(true) . '/s/accounts/login');
	}

	//ログアウト  ※2015/02/23 01:16 true削除しました。タカモト
	function get_accounts_logout($record = null){
		return $this->output($this->get_device_path() . '/accounts/logout');
	}
	//ログアウト
	function get_accounts_logout_s($record){
		return $this->output($this->get_device_path(true) . '/s/accounts/logout');
	}

	//新規登録  ※2015/02/23 01:16 true削除しました。タカモト
	function get_accounts_register($record = null){
		return $this->output($this->get_device_path() . '/accounts/register');
	}
	//新規登録
	function get_accounts_register_s($record){
		return $this->output($this->get_device_path(true) . '/s/accounts/register');
	}

	//ログインID送信画面
	function get_accounts_send_loginid($record){
		return $this->output($this->get_device_path(true) . '/accounts/send_loginid');
	}

	//パスワード再発行URL送信画面
	function get_accounts_changepassword_send($record){
		return $this->output($this->get_device_path(true) . '/accounts/changepassword_send');
	}

	// 投稿画面
	// 投稿画面（店舗選択)
	function get_review_select_shop() {
		return $this->output($this->get_device_path(true) . '/review/search_shop');
	}

	// 投稿画面（店舗選択)※制作：タカモト
	function s_get_review_select_shop() {
		return $this->output($this->get_device_path(true) . '/review/search_shop');
	}

	// 投稿画面（女の子選択)
	function get_review_select_girls($record) {
		return $this->output($this->get_device_path() . '/review/select_girls/' . $record['Shop']['id']);
	}

	// 投稿画面（店舗選択)
	function get_post_review($shop_id = -1, $girl_id = -1) {
		$param = '';

		if($shop_id != -1 && $girl_id != -1) {
			$param = '?mode=girl';
		} else if($shop_id != -1) {
			$param = '?mode=shop';
		} else {
			$param = '?mode=free';
		}

		if($shop_id != -1) {
			$param .= '&sid=' . $shop_id;
		}
		if($girl_id != -1) {
			$param .= '&gid=' . $girl_id;
		}

		return $this->output($this->get_device_path() . '/review/entry' . $param);
	}

	// 口コミ通報ページ
	function get_review_report($record) {
		return $this->output($this->get_device_path() . '/review/report/' . $record['Review']['id']);
	}

	// 口コミ通報ページ ※制作：タカモト
	function s_get_review_report($record) {
		return $this->output($this->s_get_device_path(null) . '/s/review/report/' . $record['Review']['id']);
	}
	
	// 投稿画面（店舗選択) タカモト
	function s_get_post_review($shop_id = -1, $girl_id = -1) {
		$param = '';

		if($shop_id != -1 && $girl_id != -1) {
			$param = '?mode=girl';
		} else if($shop_id != -1) {
			$param = '?mode=shop';
		} else {
			$param = '?mode=free';
		}

		if($shop_id != -1) {
			$param .= '&sid=' . $shop_id;
		}
		if($girl_id != -1) {
			$param .= '&gid=' . $girl_id;
		}

		return $this->output($this->get_device_path(true) . '/s/review/entry' . $param);
	}

}
