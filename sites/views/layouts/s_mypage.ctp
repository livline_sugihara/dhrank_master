<!DOCTYPE html>
<html lang="ja">
<head>
<?php echo $this->Html->charset(); ?>
<title><?php echo $title_for_layout; ?></title>
<?php
	if(!empty($meta_keywords)){
		echo $this->Html->meta('keywords', null, array('content' => $meta_keywords), true);
	}
	if(!empty($meta_description)){
	echo $this->Html->meta('description', null, array('content' => $meta_description), true);
	}
?>
<?php
echo $this->Html->css(array(
	's/style.css',
	's/base.css',
	's/top.css'
	)
);
	//echo $scripts_for_layout;
?>
<?php /* 非表示 タカモト
	<link href="/css/s/style.css" rel="stylesheet" type="text/css" media="all">
	<link href="/css/s/base.css" rel="stylesheet" type="text/css" media="all">
	<link href="/css/s/top.css" rel="stylesheet" type="text/css" media="all">
	<link href="/css/s/jquery.bxslider.css" rel="stylesheet" type="text/css">
*/ ?>

<?php
echo $this->Html->script(array(
	'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
	's/jquery.bxslider.min.js',
	's/jquery.biggerlink.min.js',
	's/jquery.flexslider.js',
	's/jquery.equalheight.js',
	's/menu.js'
	)
);
?>
<?php /* 非表示 タカモト
	<script type="text/javascript" src="/js/s/jquery.js"></script>
	<script type="text/javascript" src="/js/s/jquery.bxslider.js"></script>
	<script type="text/javascript" src="/js/s/jquery.biggerlink.min.js"></script>
*/ ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<script type="text/javascript">
	//slideshow
$(document).ready(function(){
  $('#newSlideShow').bxSlider({
	  ticker: 'true',
	  	displaySlideQty: 4, 
	  	minSlides: 4,	
	  	maxSlides: 4,
	  	speed: 70000
  });
});

	//slideshow
$(document).ready(function(){
  $('#newSlideShow2').bxSlider({
	  ticker: 'true',
	  	displaySlideQty: 4, 
	  	minSlides: 4,	
	  	maxSlides: 4,
	  	speed: 70000
  });
});

	//slideshow
$(document).ready(function(){
  $('#girlsSlideShow').bxSlider({
	  ticker: 'true',
		speed: 70000
  });
});	


	//tabs
	$(function() {
		
		$(".content_wrap").hide();
		$("#tabs li:first").addClass("").show();
		$(".content_wrap:first").show();
		 
		$("#tabs li").click(function() {
			$("#tabs li").removeClass("select");
			$(this).addClass("select");
			  
			var num = $("#tabs li").index(this);
			$(".content_wrap").not(num).fadeOut(1000).eq(num).fadeIn(1000);
			return false;
		});
	});

	/* slider */
	$('#shop_ichioshi').bxSlider({
		auto:true,
		speed:1000,
		infiniteLoop: true,
		hideControlOnEnd: false,
 		pager: false,
		controls: false,
	});
	
$(window).load(function() {
	$('.slider').flexslider({
		animation:'slide',
		animationSpeed:100,
		slideshow: true,
		slideshowSpeed: 5000,
		controlNav:true,
		directionNav:true,
		prevText:'戻る',
		nextText:'進む',
		itemWidth:320,
		move:0
	});
});
	


</script>
	
</head>

<body>


	<?php echo $this->element('s_common/mypage_header'); ?>

	<?php echo $this->element('s_common/name_display'); ?>

	<?php echo $this->element('s_common/breadcrumb'); ?>  

	<?php echo $content_for_layout; ?>

	<?php echo $this->element('s_common/mypage_footer'); ?>


<script type="text/javascript">

	$(document).ready(function() {
	$('.col').equalHeight();
	$('.col').biggerlink();
    $('.links').biggerlink();
	});
	
	$('#Search1').keyup(function(){
		if (!$(this).val()) {
			// 検索文字列無し
			$('#MyBookmark1 div').fadeIn();
		} else {
			// 検索文字列有り
			$('#MyBookmark1 div').fadeOut();
			$('#MyBookmark1 div:contains(' + this.value + ')').fadeIn();
		}
	});

	$(function() {
    $("#tabs li").click(function() {
        var num = $("#tab li").index(this);
        $(".content_wrap").addClass('disnon');
        $(".content_wrap").eq(num).removeClass('disnon');
        $("#tabs li").removeClass('select');
        $(this).addClass('select')
    });
});

	    // ありがとうボタン
		$('.thanksBtn').click(function(e) {
	        var review_id = $(this).attr('data');

	        if(review_id == undefined) {
	        	return;
	        }

	        // ありがとうリクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_good/"　+ review_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					if(data.success) {
						alert('投稿されました');
					} else {
						alert(data.message);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});

	    // お気に入り店舗ボタン
		$('.bookmarkShop').click(function(e) {
			var mode = $(this).attr('mode');
	        var user_id = $(this).attr('data');

	        if(user_id == undefined) {
	        	return;
	        }

	        if(mode == 'delete') {
	        	if(!confirm('お気に入りを削除します。宜しいですか？')) {
	        		return;
	        	}
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_bookmark_shop/"　+ mode + '/' + user_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					alert(data.message);
					location.reload();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});

	    // お気に入り女の子ボタン
		$('.bookmarkGirl').click(function(e) {
			var mode = $(this).attr('mode');
	        var girl_id = $(this).attr('data');

	        if(girl_id == undefined) {
	        	return;
	        }

	        if(mode == 'delete') {
	        	if(!confirm('お気に入りを削除します。宜しいですか？')) {
	        		return;
	        	}
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_bookmark_girl/"　+ mode + '/' + girl_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					alert(data.message);
					location.reload();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});

	    // お気に入りレビューボタン
		$('.bookmarkReview').click(function(e) {
			var mode = $(this).attr('mode');
	        var review_id = $(this).attr('data');

	        if(review_id == undefined) {
	        	return;
	        }

	        if(mode == 'delete') {
	        	if(!confirm('お気に入りを削除します。宜しいですか？')) {
	        		return;
	        	}
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_bookmark_review/"　+ mode + '/' + review_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					alert(data.message);
					location.reload();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
				}
			});
	        e.preventDefault();
		});

	    // フォローボタン
		$('.followReviewer').click(function(e) {
			var mode = $(this).attr('mode');
	        var reviewer_id = $(this).attr('data');

	        if(reviewer_id == undefined) {
	        	return;
	        }

	        if(mode == 'delete') {
	        	if(!confirm('レビュワーを解除します。宜しいですか？')) {
	        		return;
	        	}
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_follow/"　+ mode + '/' + reviewer_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					alert(data.message);
					location.reload();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});
		

</script>

</body>
</html>


