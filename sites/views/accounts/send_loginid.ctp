<section id="middle" class="full">
    <div class="title title2">
        <h2>INQUIRY LOGIN ID <span class="small">レビュアー会員様ログイン</span></h2>
    </div>
    <p class="text">レビュアー会員・ログインIDを再送信します。 </p>

    <div class="formArea" style="height: 300px;">
        <?php echo $form->create(null, array('action' => '')); ?>

<?php
if ($session->check('Message.auth')) {
?>
            <?php echo $session->flash('auth'); ?>
<?php }?>

            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <th>メールアドレス</th>
                    <td>
                        <?php echo $form->text('SendLoginid.email', array('size' => 30)); ?>
                        <?php echo $form->error('SendLoginid.email'); ?>
                    </td>
                </tr>
                <tr>
                    <th>ニックネーム</th>
                    <td>
                        <?php echo $form->text('SendLoginid.handle', array('size' => 30)); ?>
                        <?php echo $form->error('SendLoginid.handle'); ?>
                    </td>
                </tr>
            </table>

            <p class="align_c">
                <?php echo $form->submit('送信', array('class' => 'redBtn','value'=>'送信', 'div' => false)); ?>
            </p>
    
        <?php echo $form->end(); ?>        

    </div>
</section>


<?php /*
<h2 class="rank_h2">レビュアー会員・ログインIDを再送信します</h2>

<?php echo $form->create(null,array('type'=>'post','action'=>''));?>
<div class="login_reviewer">

<p style="text-align:left;font-size:14px;">
ご登録されているメールアドレスとニックネームを入力して下さい。<br  />
ご登録のメールアドレスにログインIDをお送りいたします。
</p>
    <table class="login_table">
        <tr>
            <td class="login_td1"><img src="/images/icon_table.jpg" />メールアドレス</td>
            <td class="login_td2"><?php echo $form->text('SendLoginid.email'); ?><?php echo $form->error('SendLoginid.email'); ?></td>
        </tr>
        <tr>
            <td class="login_td1"><img src="/images/icon_table.jpg" />ニックネーム</td>
            <td class="login_td2"><?php echo $form->text('SendLoginid.handle'); ?><?php echo $form->error('SendLoginid.handle'); ?></td>
        </tr>
    </table>

</div>


<div align="center" style="margin-top:40px;">
<?php echo $form->submit('送信', array('class' => 'btn_back','value'=>'送信', 'div' => false)); ?>
</div>
<?php echo $form->end(); ?>

<br />

<div align="center" style="margin-top:40px;">
<a href="<?php echo $linkCommon->get_accounts_login($parent_area);?>" class="btn_back">戻る</a>
</div>
*/?>