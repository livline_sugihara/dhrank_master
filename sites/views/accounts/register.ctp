<div class="body">

    <div class="title title2">
        <h2>REVIEWER REGISTER <span class="small">レビュアー会員新規登録</span></h2>
    </div>

    <p class="text">
    メールアドレスをご入力ください。<br />
    会員登録のご案内メールをお送りします。 
    </p>

    <div class="formArea">
        <?php echo $form->create(null, array('action' => '')); ?>
<?php
if ($session->check('Message.auth')) {
?>
            <?php echo $session->flash('auth'); ?>
<?php }?>

            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <th>メールアドレス</th>
                    <td>
                        <?php echo $form->text('email', array('size' => 30)); ?>
                        <?php echo $form->error('email'); ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        <a href="<?php echo $linkCommon->get_terms($parent_area)?>">ご利用規約</a>に同意</th>
                    <td>
                        <?php echo $form->checkbox('agreement'); ?><?php echo $form->error('agreement'); ?>
                    </td>
                </tr>
            </table>
       
            <p class="align_c">
                <?php echo $form->submit('登録する', array('class' => 'redBtn','value'=>'登録する', 'div' => false)); ?>
            </p>
        
        <?php echo $form->end(); ?>

    </div>
    <!-- /rankingArea-->
</div>
<?php /*
<h2 class="rank_h2">レビュアー会員・新規登録</h2>

<?php echo $form->create(null, array('action' => '')); ?>
<?php if ($session->check('Message.auth')){ ?>
<?php echo $session->flash('auth'); ?>
<?php }?>
<div class="login_reviewer">
<p style="text-align:left;font-size:16px;">
メールアドレスをご入力ください。<br />
会員登録のご案内メールをお送りします。
</p>
	<table class="submit_table">
    	<tr>
        	<td class="submit_td1"><img src="/images/icon_table.jpg" />メールアドレス</td>
            <td class="submit_td2"><?php echo $form->text('email'); ?><?php echo $form->error('email'); ?></td>
        </tr>
    	<tr>
        	<td class="submit_td1"><img src="/images/icon_table.jpg" /><a href="<?php echo $linkCommon->get_terms($parent_area)?>">ご利用規約</a>に同意</td>
            <td class="submit_td2"><?php echo $form->checkbox('agreement'); ?><?php echo $form->error('agreement'); ?></td>
        </tr>
    </table>
</div>


<div align="center" style="margin-top:40px;">
<?php echo $form->submit('登録する', array('class' => 'btn_navi','value'=>'登録する')); ?>
 </div>
<?php echo $form->end(); ?>

*/ ?>