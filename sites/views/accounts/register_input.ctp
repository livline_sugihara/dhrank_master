<div class="body">

    <div class="title title2">
        <h2>REVIEWER REGISTER <span class="small">レビュアー会員新規登録</span></h2>
    </div>

    <p class="text">以下の情報をご入力下さい。</p>

    <div class="formArea">
        <?php echo $form->create(null, array('action' => '')); ?>
            <?php echo $form->hidden('ReviewerRegister.id', array('value' => $data['ReviewerRegister']['id']));?>
<?php
if ($session->check('Message.auth')) {
?>
            <?php echo $session->flash('auth'); ?>
<?php }?>

            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <th>ログインID</th>
                    <td>
                        <?php echo $form->text('Reviewer.username', array('size' => 30)); ?>
                        <?php echo $form->error('Reviewer.username'); ?>
                    </td>
                </tr>
                <tr>
                    <th>ニックネーム</th>
                    <td>
                        <?php echo $form->text('Reviewer.handle', array('size' => 30)); ?>
                        <?php echo $form->error('Reviewer.handle'); ?>
                    </td>
                </tr>
                <tr>
                    <th>パスワード</th>
                    <td>
                        <?php echo $form->text('Reviewer.password_confirm', array('size' => 30)); ?>
                        <?php echo $form->error('Reviewer.password_confirm'); ?>
                    </td>
                </tr>
            </table>
       
            <p class="align_c">
                <?php echo $form->submit('登録する', array('class' => 'redBtn','value'=>'登録する', 'div' => false)); ?>
            </p>
        
        <?php echo $form->end(); ?>

    </div>
    <!-- /rankingArea-->
</div>

<?php /*
<h2 class="rank_h2">レビュアー会員・新規登録</h2>

<?php echo $form->create(null, array('action' => '')); ?>
<?php echo $form->hidden('ReviewerRegister.id', array('value' => $data['ReviewerRegister']['id']));?>
<?php if ($session->check('Message.auth')){ ?>
<?php echo $session->flash('auth'); ?>
<?php }?>
<div class="login_reviewer">
	<table class="submit_table">
    	<tr>
        	<td class="submit_td1"><img src="/images/icon_table.jpg" />ログインID</td>
            <td class="submit_td2"><?php echo $form->text('Reviewer.username'); ?><?php echo $form->error('Reviewer.username'); ?></td>
        </tr>
    	<tr>
        	<td class="submit_td1"><img src="/images/icon_table.jpg" />ニックネーム</td>
            <td class="submit_td2"><?php echo $form->text('Reviewer.handle'); ?><?php echo $form->error('Reviewer.handle'); ?></td>
        </tr>
    	<tr>
        	<td class="submit_td1"><img src="/images/icon_table.jpg" />パスワード</td>
            <td class="submit_td2"><?php echo $form->password('Reviewer.password_confirm'); ?><?php echo $form->error('Reviewer.password_confirm'); ?></td>
        </tr>
    </table>
</div>


<div align="center" style="margin-top:40px;">
<?php echo $form->submit('登録する', array('class' => 'btn_navi','value'=>'登録する')); ?>
 </div>
<?php echo $form->end(); ?>
*/ ?>