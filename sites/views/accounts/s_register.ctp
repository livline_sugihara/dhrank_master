<div class="body">
<div style="border-bottom:2px solid #c03c61;padding-bottom:2px;"><h2 class="link_o2">新規会員登録</h2></div>
<section id="account_form">
	<div class="inner">
		<table>
			<tr>
				<th>メールアドレス ※半角英数</th>
			</tr>
		</table>
		<p class="mar_b_10 font11">ご入力いただいたメールアドレスに確認メールを送信します。</p>
		<div class="mar_b_20 post_bg">
<?php echo $form->create(null, array('action' => '')); ?>
<?php
	if ($session->check('Message.auth')) {
?>
<?php echo $session->flash('auth'); ?>
<?php }?>
			<?php echo $form->text('email'); ?><?php echo $form->error('email'); ?><br />
			<?php echo $form->checkbox('agreement'); ?><?php echo $form->error('agreement'); ?>
			<a href="/s/front/term">ご利用規約</a>に同意
			<p class="btn_pi">
<?php echo $form->submit('規約に同意の上 次へ', array('class' => 'redBtn','value'=>'登録する', 'div' => false)); ?>
			</p>
<?php echo $form->end(); ?>
		</div>
	</div>

</section>


<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
	</ul>
</nav>


<?php /*
<h2 class="rank_h2">レビュアー会員・新規登録</h2>

<?php echo $form->create(null, array('action' => '')); ?>
<?php if ($session->check('Message.auth')){ ?>
<?php echo $session->flash('auth'); ?>
<?php }?>
<div class="login_reviewer">
<p style="text-align:left;font-size:16px;">
メールアドレスをご入力ください。<br />
会員登録のご案内メールをお送りします。
</p>
	<table class="submit_table">
    	<tr>
        	<td class="submit_td1"><img src="/images/icon_table.jpg" />メールアドレス</td>
            <td class="submit_td2"><?php echo $form->text('email'); ?><?php echo $form->error('email'); ?></td>
        </tr>
    	<tr>
        	<td class="submit_td1"><img src="/images/icon_table.jpg" /><a href="<?php echo $linkCommon->get_terms($parent_area)?>">ご利用規約</a>に同意</td>
            <td class="submit_td2"><?php echo $form->checkbox('agreement'); ?><?php echo $form->error('agreement'); ?></td>
        </tr>
    </table>
</div>


<div align="center" style="margin-top:40px;">
<?php echo $form->submit('登録する', array('class' => 'btn_navi','value'=>'登録する')); ?>
 </div>
<?php echo $form->end(); ?>

*/ ?>