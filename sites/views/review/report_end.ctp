<section id="middle" class="full">
    
    <div id="entryArea">
        <!--  body -->
        <div class="body">
        	<div class="listHeading">口コミ通報の完了</div>
        	<p>口コミの不適切使用を管理者に送信しました。ご協力ありがとうございました。</p>
        </div>

        <div align="center" style="margin-top: 40px;">
			<a href="<?php echo $linkCommon->get_top(); ?>" class="css_btn_class2">トップへ戻る</a>
		</div>
    </div>
</section>
