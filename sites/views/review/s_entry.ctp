	<div class="link_o"><h2>口コミ投稿</h2></div>
	<section id="postArea">
                <?php echo $form->create(null,array('type'=>'post','action'=> '')); ?>

			<table>
				<tr>
					<th><span class="required">必須</span>店名</th>
					<td>
<?php if($mode == 'free') { ?>
	<?php echo $form->text('Review.shop_name',array('size' => '30')); ?>
<?php } else { ?>
	<?php echo $base_data['Shop']['name']; ?>
	<?php echo $form->hidden('Review.shop_name', array('value' => $base_data['Shop']['name'])); ?>
	<?php echo $form->hidden('Review.user_id', array('value' => $base_data['User']['id'])); ?>
<?php } ?>
					</td>
				</tr>
				<tr>
					
<?php if($mode != 'girl') { ?>
	<th>
		<span class="required">必須</span>女の子
	</th>
<?php } else { ?>
	<th style="background:#ffffff;text-align:center;">
		<?php echo $imgCommon->get_girl_with_time($base_data, 'm'); ?>
		<p class="btn_pi2" style="font-size:80%;"><a href="<?php echo $linkCommon->get_user_girls($base_data); ?>">絞り直し</a></p>	
	</th>
<?php } ?>
					
					<td>					
<?php if($mode != 'girl') { ?>
	<?php echo $form->text('Review.girl_name',array('size' => '30')); ?>
<?php } else { ?>
	<?php echo $base_data['Girl']['name']; ?>
	<?php echo $form->hidden('Review.girl_name', array('value' => $base_data['Girl']['name'])); ?>
	<?php echo $form->hidden('Review.girl_id', array('value' => $base_data['Girl']['id'])); ?>
<?php } ?>
					</td>
				</tr>
				<tr>
					<th><span class="required">必須</span>コース／料金</th>
					<td>
<?php
$minites_list = array();
for($ii = 10; $ii <= 300; $ii += 5) { $minites_list[$ii] = $ii; }
$costs_list = array();
for($ii = 2000; $ii <= 100000; $ii += 1000) { $costs_list[$ii] = $ii; }
?>
						<?php echo $form->select('Review.course_minute', $minites_list,null,array('empty'=>'---')); ?> 分
						<?php echo $form->select('Review.course_cost', $costs_list,null,array('empty'=>'------')); ?> 円

						<?php echo $form->error('Review.course_minute'); ?>
						<?php echo $form->error('Review.course_cost'); ?>
					</td>
				</tr>
				<tr>
					<th><span class="required">必須</span>ご利用場所</th>
					<td>
						<?php echo $form->select('Review.using_place_id', $m_reviews_place,null,array('empty'=>false)); ?>
						<?php echo $form->error('Review.using_place_id'); ?>
					</td>
				</tr>
				<tr>
					<th><span class="required">必須</span>指名</th>
					<td>
<?php echo $form->select('Review.appointed_type', $appointed_type,null,array('empty'=>false)); ?>
・
<?php echo $form->select('Review.appointed_sub', $appointed_sub,null,array('empty'=>false)); ?>

<?php echo $form->error('Review.appointed_type'); ?>
<?php echo $form->error('Review.appointed_sub'); ?>
					</td>
				</tr>
				<tr>
					<th style="background:#FFE3E6;"><span class="required">必須</span>L(ルックス)</th>
					<td>
						<?php echo $form->select('Review.score_girl_looks', $score_list,null,array('empty'=>'採点して下さい')); ?>
						<?php echo $form->error('Review.score_girl_looks'); ?>
					</td>
				</tr>
				<tr>
					<th style="background:#FFE3E6;"><span class="required">必須</span>S(サービス)</th>
					<td>
						<?php echo $form->select('Review.score_girl_service', $score_list,null,array('empty'=>'採点して下さい')); ?>
						<?php echo $form->error('Review.score_girl_service'); ?>
					</td>
				</tr>
				<tr>
					<th style="background:#FFE3E6;"><span class="required">必須</span>C(性格)</th>
					<td>
						<?php echo $form->select('Review.score_girl_character', $score_list,null,array('empty'=>'採点して下さい')); ?>
						<?php echo $form->error('Review.score_girl_character'); ?>
					</td>
				</tr>
			</table>

			<p><br /></p>

			<h3 class="label_4"><span class="required">必須</span>口コミ</h3>
			<?php echo $form->textarea('Review.comment',array('rows' => '20')); ?>
			<?php echo $form->error('Review.comment'); ?>

			<p><br /></p>

			<h3 class="label_4"><span class="required">必須</span>この子の女の子の良さを一言で表すと？<br />（タイトルとして表示されます）</h3>
            <?php echo $form->text('Review.post_title',array('size' => '30')); ?>
            <?php echo $form->error('Review.post_title'); ?>

			<p class="submitBtn btn_green2"><?php echo $form->submit('確認する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'confirm'));?></p>
			<?php echo $form->end(); ?>


<?php /*
<section id="middle" class="full">
    
    <div id="entryArea">
        <div class="title title07">
            <h2>REVIEW SUBMIT <span class="small">口コミ投稿</span></h2>
        </div>

        <!--  body -->
        <div class="body">
            <div class="listHeading">クチコミ投稿のご注意</div>
            <p>女の子のルックスや性格、サービスなどの良かったところをみんなでシェアするサイトです。女の子が傷つくような誹謗中傷などはお控えください。<br />
            該当する書き込みがあった場合、編集部にて削除させて頂くことがあります。
            </p>
                <br />
            <p>ルックスとは、例えば（顔、胸、お尻、肌質、指（爪）、体型、髪型センス、化粧センス、香り、清潔度）など<br />
            性格とは、例えば（気配り、謙虚さ、優しさ、協調性、献身的、癒し）など<br />
            サービスとは、例えば（敏感度、あえぎ声、淫乱度、濡れ具合、丁寧な接客、時間的配慮）など<br />
            実際に遊んで満足した女の子の良かったところをシェアしましょう。
            </p>
            
            <div class="listHeading">クチコミ投稿フォーム <span>全ての項目にご記入下さい。</span></div>

                <?php echo $form->create(null,array('type'=>'post','action'=> '')); ?>
                
                <div class="reviewForm">
                <table cellspacing="0" cellpadding="0" border="0" class="noLCS">
                    <tr>
                        <th>店名</th>
                        <td>
<?php if($mode == 'free') { ?>
                            <?php echo $form->text('Review.shop_name',array('size' => '30')); ?>
<?php } else { ?>
                            <?php echo $base_data['Shop']['name']; ?>
                            <?php echo $form->hidden('Review.shop_name', array('value' => $base_data['Shop']['name'])); ?>
                            <?php echo $form->hidden('Review.user_id', array('value' => $base_data['User']['id'])); ?>
<?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <th>女の子</th>
                        <td>
<?php if($mode != 'girl') { ?>
                            <?php echo $form->text('Review.girl_name',array('size' => '30')); ?>
<?php } else { ?>
                            <?php echo $base_data['Girl']['name']; ?>
                            <?php echo $form->hidden('Review.girl_name', array('value' => $base_data['Girl']['name'])); ?>
                            <?php echo $form->hidden('Review.girl_id', array('value' => $base_data['Girl']['id'])); ?>
<?php } ?>
                        </td>
                        
                    </tr>
                    <tr>
                        <th>コース／料金</th>
                        <td>
<?php
$minites_list = array();
for($ii = 10; $ii <= 120; $ii += 5) { $minites_list[$ii] = $ii; }
$costs_list = array();
for($ii = 2000; $ii <= 100000; $ii += 1000) { $costs_list[$ii] = $ii; }
?>
                            <?php echo $form->select('Review.course_minute', $minites_list,null,array('empty'=>'---')); ?> 分
                            <?php echo $form->select('Review.course_cost', $costs_list,null,array('empty'=>'------')); ?> 円

                            <?php echo $form->error('Review.course_minute'); ?>
                            <?php echo $form->error('Review.course_cost'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご利用場所</th>
                        <td>
                            <?php echo $form->select('Review.using_place_id', $m_reviews_place,null,array('empty'=>false)); ?>
                            <?php echo $form->error('Review.using_place_id'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>指名</th>
                        <td>
                            <?php echo $form->select('Review.appointed_type', $appointed_type,null,array('empty'=>false)); ?>
                            ・
                            <?php echo $form->select('Review.appointed_sub', $appointed_sub,null,array('empty'=>false)); ?>

                            <?php echo $form->error('Review.appointed_type'); ?>
                            <?php echo $form->error('Review.appointed_sub'); ?>
                        </td>
                    </tr>
                </table>
                </div>
                
               <div class="circle">女の子を3つのカテゴリーから評価して下さい。</div>
                
                <div class="reviewFormRed">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <th>L(ルックス)</th>
                        <td>
                            <?php echo $form->select('Review.score_girl_looks', $score_list,null,array('empty'=>'採点して下さい')); ?>
                            <?php echo $form->error('Review.score_girl_looks'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>S(サービス)</th>
                        <td>
                            <?php echo $form->select('Review.score_girl_service', $score_list,null,array('empty'=>'採点して下さい')); ?>
                            <?php echo $form->error('Review.score_girl_service'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>C(性格)</th>
                        <td>
                            <?php echo $form->select('Review.score_girl_character', $score_list,null,array('empty'=>'採点して下さい')); ?>
                            <?php echo $form->error('Review.score_girl_character'); ?>
                        </td>
                    </tr>
                </table>
                </div>
                
                <div class="circle">ルックス、性格、サービス等で特に何が良かったのか（上記「クチコミ投稿のご注意」参照）を、みなさんが参考にしやすいように書いて下さい。</div>
                
                <div class="reviewForm">
                <table cellspacing="0" cellpadding="0" border="0" class="noLCS">
                    <tr>
                        <th>クチコミ</th>
                        <td>
                            <?php echo $form->textarea('Review.comment'); ?>
                            <?php echo $form->error('Review.comment'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>この女の子の良さを一言でいうと<br>(※タイトルとして表示されます）</th>
                        <td>
                            <?php echo $form->text('Review.post_title',array('size' => '30')); ?>
                            <?php echo $form->error('Review.post_title'); ?>
                        </td>
                    </tr>
                </table>
                </div>
                <p class="align_c">
                    <?php echo $form->submit('入力内容を確認する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'confirm'));?>
                </p>
			<?php echo $form->end(); ?>

        </div>
        <!--  /body -->
        
    </div>
*/ ?>
</section>
<script>
$(document).ready(function(){
    $('#ReviewAppointedType').change(function(e) {
        refreshAppointed();
        e.preventDefault();
    });

    // ロード時に一度実行
    refreshAppointed();
});

function refreshAppointed() {
    var appointed_type_id = $('#ReviewAppointedType').children(':selected').val();

    // 新規の場合
    if(appointed_type_id == 1) {
        $('#ReviewAppointedSub').children('[value=1]').removeAttr('selected').hide();
        $('#ReviewAppointedSub').children('[value=2]').attr('selected', 'selected');
    } else {
        $('#ReviewAppointedSub').children('[value=2]').removeAttr('selected');
        $('#ReviewAppointedSub').children('[value=1]').attr('selected', 'selected').show();
    }
}
</script>



<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../../s/top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">口コミ投稿</span></li>
	</ul>
</nav>