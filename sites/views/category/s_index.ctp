	<div class="link_o"><h2>検索結果一覧</h2></div>
	<div class="label2_2"><h3 class="label2_ttl"><?php echo $category['MShopsBusinessCategory']['name']; ?></h3></div>
	<div class="kensuu"><p>全<span class="font18"><strong><?php echo $paginator->counter(array('format' => '%count%</strong></span>件中 <span>%start%</span>～<span>%end%</span>件表示')); ?></p></div>




	<section id="rankingArea">
<?php
foreach($list as $key => $record) {
    $opentime = date('H:i', strtotime($record['System']['open_time']));
    $closetime = date('H:i', strtotime($record['System']['close_time']));

    if($record['UsersAuthority']['is_show_all_contents_by_categorylist'] == 1) {
//        print_r($record);
?>
		<div class="rank_box bigger underline">
			<p class="shop_tt"><?php echo $record['Shop']['name']; ?><span>UP</span></p>
			<ul class="shop_icon clearfix">
			<?php if($record['Shop']['service01'] == 1) { ?><li class="pay_yoyaku">先行予約OK</li><?php } ?>
			<?php if($record['Shop']['service02'] == 1) { ?><li class="pay_credit">クレジットOK</li><?php } ?>
			<?php if($record['Shop']['service03'] == 1) { ?><li class="pay_ryoshu">領収書OK</li><?php } ?>
			<?php if($record['Shop']['service04'] == 1) { ?><li class="pay_machi">待ち合わせOK</li><?php } ?>
			<?php if($record['Shop']['service05'] == 1) { ?><li class="pay_cosplay">コスプレあり</li><?php } ?>
			<?php if($record['Shop']['service06'] == 1) { ?><li class="pay_coupon">クーポンあり</li><?php } ?>
			</ul>
				<div class="clearfix">
					<div class="box_left">
						<a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo $imgCommon->get_shop_with_time($record, 'list_sp', array(160,210)); ?></a>
						<p class="btn_red" style="margin-top:5px;"><a href="<?php echo $linkCommon->get_user_shop($record); ?>">店舗詳細を見る</a></p>
					</div>
					<div class="box_right">
						<p class="rank_txt"><a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo nl2br($record['Shop']['about_title']); ?></a></p>
						<ul class="clearfix">
							<li class="icon_access"><?php echo $categories_value_list[$record['Shop']['shops_business_category_id']];?>/<?php echo $record['System']['address']; ?></li>
							<li class="icon_people">在籍数<br><?php echo $record[0]['girl_cnt']; ?>人</li>
							<li class="icon_price">最安値料金<br><?php echo $record['System']['price_cost']; ?>円～</li>
							<li class="icon_kuchikomi_up">口コミ<br><?php echo $record[0]['review_cnt']; ?>件</li>
							<li class="icon_time_up">営業時間<br><?php echo $opentime; ?>〜<?php echo $closetime; ?></li>
						</ul>
					</div>
				</div>
		</div><!-- /.rank_box-->
<?php /*
		<div>
                <a href="<?php echo $linkCommon->get_user_shop($record); ?>" class="shopName">
                    <?php echo $record['Shop']['name']; ?>
                </a>　
                <span class="kuchikomi">UP</span>
                <a href="<?php echo $linkCommon->get_user_shop($record); ?>" class="css_btn_class">
                    詳細を見る
                </a>
            </div>

        </div>
        
        <div class="shopList_main clearfix">
            <div>
                <a href="<?php echo $linkCommon->get_user_shop($record); ?>">
                    <?php echo $imgCommon->get_shop_with_time($record, 'list', array(160,210)); ?>
                </a>
            </div>
            <div>
                <p class="catchCopy"><b><strong><?php echo $record['Shop']['about_title']; ?></strong></b></p>
                <p class="categoryComment"><?php echo nl2br($record['Shop']['about_contents']); ?></p>
                
                <div class="shopListLeft">
                    <dl class="clearfix">
                        <dt>営業時間</dt>
                        <dd><?php echo $opentime; ?>〜<?php echo $closetime; ?></dd>
                    
                        <dt>最低料金</dt>
                        <dd><?php echo $record['System']['price_cost']; ?>円〜</dd>

                        <dt>住所</dt>
                        <dd><?php echo $record['System']['address']; ?></dd>
                    </dl>   
                    <p class="shopNum">
                        <span class="zaiseki">在籍数: </span><span class="num"><?php echo $record[0]['girl_cnt']; ?>人</span>
                        <span class="kuchikomi2">口コミ: </span><span class="num"><?php echo $record[0]['review_cnt']; ?>件</span>
                    </p>
                </div>
                <div class="shopListRight">
                    <a href="<?php echo $linkCommon->get_user_shop($record); ?>" class="css_btn_more">詳細を見る</a><a href="#" class="css_btn_bookmark">お気に入りに保存</a>
                </div>

                <div class="shopListBottom clearfix">
                    <ul>

<?php
if(!empty($record['Coupon'])) {
?>
<?php
    foreach($record['Coupon'] AS $jj => $coupon) {
?>
                        <li>
                            <span class="type<?php echo $coupon['target'];?>">
                                <?php echo $m_target[$coupon['target']]; ?>
                            </span>
                            <a href="<?php echo $linkCommon->get_user_coupons($record); ?>"><?php echo $coupon['condition']; ?>
                            </a>
                        </li>
 <?php
    }
} else {
?>
                        <li style="line-height: 2.8em">割引クーポンは発行されていません。</li>
<?php
}
?>                    </ul>
                </div>
            </div>
        </div>
        <!-- /shop -->
*/ ?>
<?php
    } else {
?>

		<div class="rank_box bigger underline">
			<div class="clearfix">
				<p class="shop_tt f_left"><a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo $record['Shop']['name']; ?></a></p>
				<div class="box_right2 f_right">
					<ul>
						<li class="icon_kuchikomi_up">口コミ<br><?php echo $record[0]['review_cnt']; ?>件</li>
					</ul>
				</div>
			</div>
		</div><!-- /.rank_box-->
<?php /*

        <!-- shop -->
        <div class="shopList_frame">
            <div>
                <a href="<?php echo $linkCommon->get_user_shop($record); ?>" class="shopName"><?php echo $record['Shop']['name']; ?></a>
                <span class="kuchikomi">口コミ</span><span class="c_red"><?php echo $record[0]['review_cnt']; ?>件</span>
                <a href="<?php echo $linkCommon->get_user_shop($record); ?>" class="css_btn_class">詳細を見る</a>
            </div>
            <div>
            営業時間・<?php echo $opentime; ?>〜<?php echo $closetime; ?>　最低料金・<?php echo $record['System']['price_cost']; ?>円〜　住所・<?php echo $record['System']['address']; ?>
            </div>
        </div>
        <!-- /shop -->
*/ ?>
<?php
    }
}
?>

    </div>
    <!-- /pay_shopList -->

<?php /*
    <!-- pageNav03 -->
    <ul class="pageNav03 clear">
        <li><a href="#">&laquo; 前</a></li>
        <li><a href="#">1</a></li>
        <li><span>2</span></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">6</a></li>
        <li><a href="#">次 &raquo;</a></li>
    </ul>
*/ ?>
<?php echo $this->element('s_pagenate/footer_pagenate'); ?>
	</section>
<?php echo $this->element('s_common/footer_banner'); ?>
