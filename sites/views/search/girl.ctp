<section id="middle" class="narrow">

    <!-- pay_shopList -->
    <div id="pay_shopList">
    
        <div class="title title04">
            <h2>女の子 検索結果</h2>
        </div>

        <div class="pageCounter">
            <span class="num"><?php echo $paginator->counter(array('format' => '%count%')); ?></span><span>件ありました</span>
            <?php echo $this->element('pagenate/search_header_pagenate'); ?>
        </div>

        <div class="girlsArea_result clearfix">
<?php
$cnt = 0;
foreach($list as $key => $record) {
?>
            <div>
                <p class="img">
                    <a href="<?php echo $linkCommon->get_user_girl($record); ?>">
	                    <?php echo $imgCommon->get_girl_with_time($record, array(75, 75)); ?>
                    </a>
                </p>
                <p class="name"><a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo $record['Girl']['name']; ?></a></p>
               <!-- <p><?php echo $record['LargeArea']['name']; ?></p> -->
                <p><?php echo $record['Shop']['name']; ?></p>
                <p class="btn bookmarkGirl" data="<?php echo $record['Girl']['id']; ?>" mode="add"><a href="#" class="css_btn_class">お気に入りに保存</a></p>
            </div>
<?php
    if($cnt % 4 == 3) {
?>
            <p class="clearLine"></p>
<?php
    }
    $cnt++;
}
?>
        </div>
    </div>
    <!-- /pay_shopList -->

    <?php echo $this->element('pagenate/search_footer_pagenate'); ?>

    <!-- ispotArea -->
    <div id="ispotArea">
        <?php echo $this->element('common/top_pic_review'); ?>
    </div>
    <!-- /ispotArea -->

    <?php echo $this->element('common/top_review_bunner'); ?>

    <!-- categoryArea -->
    <div id="categoryArea">
        <?php echo $this->element('common/top_category'); ?>
    </div>

</section>

<section id="side" class="wide">
    <?php echo $this->element('common/side_wide'); ?>
</section>