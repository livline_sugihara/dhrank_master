
<div class="body">
    
<div class="title title2">
	<h2>CONTACT <span class="small">お問い合わせ</span></h2>
</div>

<p class="text">
	デリヘルクチコミランキングをご覧いただき、ありがとうございます。<br />
	まずはWEBでのお問い合わせ、またはお客様受付窓口までお電話ください。<br />
	各エリアを専門に担当する営業をご手配させていただきます。
</p>

<div id="ContentsTabArea">

    <ul id="tabs" class="clearfix">
        <li id="tab_payment_insert" style="width:198px;"<?php if(!isset($this->data['Contact']['mode']) || $this->data['Contact']['mode'] == 'payment_insert') { ?>　class="select"<?php } ?>>有料掲載希望</li>
        <li id="tab_free_insert" style="width:198px;"<?php if($this->data['Contact']['mode'] == 'free_insert') { ?>　class="select"<?php } ?>>無料掲載希望</li>
        <li id="tab_agency_agreement" style="width:198px;"<?php if($this->data['Contact']['mode'] == 'agency_agreement') { ?>　class="select"<?php } ?>>代理店契約</li>
        <li id="tab_other" style="width:198px;"<?php if($this->data['Contact']['mode'] == 'other') { ?>　class="select"<?php } ?>>その他</li>
    </ul>
    
    <!-- content_wrap -->
    <div class="content_wrap">           
        <!-- rankingArea -->
        <div class="formArea">
            
           	<?php echo $form->create(null,array('type'=>'post','action'=> '','enctype' => 'multipart/form-data')); ?>
                <?php echo $form->hidden('Contact.mode', array('value' => 'payment_insert')); ?>
            
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <th>掲載希望エリア <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.insert_area',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.insert_area'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>業種 <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.category_name',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.category_name'); ?>
                        </td>
                        
                    </tr>
                    <tr>
                        <th>店舗名 <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.shop_name',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.shop_name'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご担当者名 <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.charge_name',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.charge_name'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご担当者様のご連絡先 <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.charge_contact',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.charge_contact'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご連絡先メールアドレス <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.emailto',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.emailto'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご連絡可能な時間帯 <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.contact_time',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.contact_time'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>店舗URL</th>
                        <td>
                            <?php echo $form->text('Contact.shop_url',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.shop_url'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>届出確認書or許可書</th>
                        <td>
                            <?php echo $form->file('Contact.sendfile'); ?>
                            <?php echo $form->error('Contact.sendfile'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご質問・ご要望など</th>
                        <td>
                            <?php echo $form->textarea('Contact.comment'); ?>
                            <?php echo $form->error('Contact.comment'); ?>
                        </td>
                    </tr>
                </table>
            
                <p class="align_c">
                    <?php echo $form->submit('入力内容を確認する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'confirm'));?>
                </p>
			<?php echo $form->end(); ?>
		</div>
    </div>
    <!-- /content_wrap -->
    
    <!-- content_wrap -->
    <div class="content_wrap">
        <div class="formArea">
            
        	<?php echo $form->create(null,array('type'=>'post','action'=> '','enctype' => 'multipart/form-data')); ?>
                <?php echo $form->hidden('Contact.mode', array('value' => 'free_insert')); ?>
            
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <th>掲載希望エリア</th>
                        <td>
                            <?php echo $form->text('Contact.insert_area',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.insert_area'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>業種</th>
                        <td>
                            <?php echo $form->text('Contact.category_name',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.category_name'); ?>
                        </td>
                        
                    </tr>
                    <tr>
                        <th>店舗名</th>
                        <td>
                            <?php echo $form->text('Contact.shop_name',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.shop_name'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご担当者名</th>
                        <td>
                            <?php echo $form->text('Contact.charge_name',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.charge_name'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご担当者様のご連絡先</th>
                        <td>
                            <?php echo $form->text('Contact.charge_contact',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.charge_contact'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご連絡先メールアドレス</th>
                        <td>
                            <?php echo $form->text('Contact.emailto',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.emailto'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>店舗URL</th>
                        <td>
                            <?php echo $form->text('Contact.shop_url',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.shop_url'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>店舗電話番号</th>
                        <td>
                            <?php echo $form->text('Contact.shop_tel',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.shop_tel'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>住所or（…発）</th>
                        <td>
                            <?php echo $form->text('Contact.shop_address',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.shop_address'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>営業時間</th>
                        <td>
                            <?php echo $form->text('Contact.shop_business_hours',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.shop_business_hours'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>最低料金</th>
                        <td>
                            <?php echo $form->text('Contact.lowest_cost',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.lowest_cost'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>先行予約</th>
                        <td>
                            <?php echo $form->radio('Contact.reserve', $umu_select, array('legend' => false, 'div' => false, 'separator' => '　')); ?>
                            <?php echo $form->error('Contact.reserve'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>領収書</th>
                        <td>
                            <?php echo $form->radio('Contact.receipt', $umu_select, array('legend' => false, 'div' => false, 'separator' => '　')); ?>
                            <?php echo $form->error('Contact.receipt'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>待ち合わせ</th>
                        <td>
                            <?php echo $form->radio('Contact.layover', $umu_select, array('legend' => false, 'div' => false, 'separator' => '　')); ?>
                            <?php echo $form->error('Contact.layover'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>コスプレ</th>
                        <td>
                            <?php echo $form->radio('Contact.cosplay', $umu_select, array('legend' => false, 'div' => false, 'separator' => '　')); ?>
                            <?php echo $form->error('Contact.cosplay'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>クレジットカード</th>
                        <td>
                            <?php echo $form->radio('Contact.credit', $umu_select, array('legend' => false, 'div' => false, 'separator' => '　')); ?>
                            <?php echo $form->error('Contact.credit'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>届出確認書or許可書</th>
                        <td>
                            <?php echo $form->file('Contact.sendfile'); ?>
                            <?php echo $form->error('Contact.sendfile'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご質問・ご要望など</th>
                        <td>
                            <?php echo $form->textarea('Contact.comment'); ?>
                            <?php echo $form->error('Contact.comment'); ?>
                        </td>
                    </tr>
                </table>
            
                <p class="align_c">
                    <?php echo $form->submit('入力内容を確認する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'confirm'));?>
                </p>
            <?php echo $form->end(); ?>
        </div>
    </div>
    <!-- /content_wrap -->
    
    <!-- content_wrap -->
    <div class="content_wrap">
        <!-- rankingArea -->
        <div class="formArea">
            
            <?php echo $form->create(null,array('type'=>'post','action'=> '','enctype' => 'multipart/form-data')); ?>
                <?php echo $form->hidden('Contact.mode', array('value' => 'agency_agreement')); ?>
           
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <th>会社名 <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.corp_name',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.corp_name'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご担当者名 <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.charge_name',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.charge_name'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご担当者様のご連絡先 <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.charge_contact',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.charge_contact'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご連絡先メールアドレス <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.emailto',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.emailto'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ホームページアドレス <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.corp_url',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.corp_url'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご質問・ご要望など</th>
                        <td>
                            <?php echo $form->textarea('Contact.comment'); ?>
                            <?php echo $form->error('Contact.comment'); ?>
                        </td>
                    </tr>
                </table>
            
                <p class="align_c">
                    <?php echo $form->submit('入力内容を確認する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'confirm'));?>
                </p>
            <?php echo $form->end(); ?>
            
           <p></p>
   
        </div>
    </div>
    <!-- /content_wrap -->

    <!-- content_wrap -->
    <div class="content_wrap">
        <!-- rankingArea -->
        <div class="formArea">
        
            <?php echo $form->create(null,array('type'=>'post','action'=> '','enctype' => 'multipart/form-data')); ?>
                <?php echo $form->hidden('Contact.mode', array('value' => 'other')); ?>
           
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <th>メールアドレス <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.emailto',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.emailto'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>お名前 <span>必須</span></th>
                        <td>
                            <?php echo $form->text('Contact.charge_name',array('size' => '30')); ?>
                            <?php echo $form->error('Contact.charge_name'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご質問・ご要望など</th>
                        <td>
                            <?php echo $form->textarea('Contact.comment'); ?>
                            <?php echo $form->error('Contact.comment'); ?>
                        </td>
                    </tr>
                </table>
            
                <p class="align_c">
                    <?php echo $form->submit('入力内容を確認する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'confirm'));?>
                </p>
            <?php echo $form->end(); ?>
            
           	<p></p>

        </div>
    </div>
    <!-- /content_wrap -->

</div>
<!-- /tabArea -->

<script>
	//tabs
	$(function() {
		
		$(".content_wrap").hide();
		$("#tabs li:first").addClass("").show();
		$(".content_wrap:first").show();
		 
		$("#tabs li").click(function() {
			$("#tabs li").removeClass("select");
			$(this).addClass("select");
			  
			var num = $("#tabs li").index(this);
			$(".content_wrap").not(num).fadeOut(1000).eq(num).fadeIn(1000);
			return false;
		});

        // 初期表示
        var mode = '<?php echo $this->data['Contact']['mode']; ?>';
        $('#tab_' + mode).click();
	});
</script>
