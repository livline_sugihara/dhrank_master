<section id="middle" class="narrow">

    <!-- shop list -->
    <?php echo $this->element('common/middle_shop_list'); ?>
    <!-- /shop list -->

	<?php echo $this->element('common/top_review_bunner'); ?>

	<!-- categoryArea -->
	<div id="categoryArea">
		<?php echo $this->element('common/top_category'); ?>
	</div>
	<!-- /categoryArea -->

</section>

<section id="side" class="wide">
	<?php echo $this->element('common/side_wide'); ?>
</section>
