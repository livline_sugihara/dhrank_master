<section id="middle" class="narrow">

	<!-- categoryArea -->
	<div id="categoryArea">
		<?php echo $this->element('common/top_category'); ?>
	</div>
	<!-- /categoryArea -->

</section>

<section id="side" class="wide">
	<?php echo $this->element('common/side_wide'); ?>
</section>
