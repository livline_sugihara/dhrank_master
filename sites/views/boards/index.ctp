
<h2 class="rank_h2">炎上掲示板</h2>

<?php echo $form->create(null, array('type'=>'post', 'action' => 'search_result'));?>
<div class="bbs_search">
	タイトル検索：<?php echo $form->text('Boards.search_word'); ?><?php echo $form->submit('検索', array('div' => false)); ?>
</div>
<?php echo $form->end();?>

<a name="top"></a>

<div class="bbs_list">
	<?php foreach($board_list as $key => $record){ ?>
	<a href="<?php echo $linkCommon->get_boards_read($record)?>/l50"><?php echo $key+1?>:<?php echo $record['Board']['title']?>(<?php echo $record[0]['count']?>)</a>&nbsp;
	<?php } ?>
    <div style="text-align:right"><a href="<?php echo $linkCommon->get_boards_all_list($record)?>">スレッド一覧はこちら</a></div>
</div>



<?php foreach($board_detail_list as $key => $record){ ?>
<a name="<?php echo ($key+1)?>"></a>
<div class="bbs_preview">
	<div class="bbs_title">
    	<?php echo '【' . (string)(($key+1)+($paginator->current()-1)*$pcount['boards_detail']) . ':' . $record[0]['count'] . '】'?><span class="spot4"><?php echo $record['Board']['title']?></span>
    	<span style="float:right"><a href="#top">■</a> <a href="#<?php echo ($key)?>">▲</a> <a href="#<?php echo ($key+2)?>">▼</a></span>
    </div>

	<dl>
		<?php if(!empty($record['BoardRes'][0])){?>
		<dt><?php echo $record['BoardRes'][0]['sub_id']?> <?php echo $record['BoardRes'][0]['name']?> 投稿日：<?php echo $textCommon->get_datetimewithweek($record['BoardRes'][0]['created'])?></dt>
			<dd><?php echo nl2br($record['BoardRes'][0]['comment'])?></dd>
		<?php }?>
		<?php $i = count($record['BoardRes']) - ($pcount['boards_detail_res']-1);?>
		<?php if($i <= 0) $i=1;?>
		<?php for($i; $i < count($record['BoardRes']); $i++){?>
		<dt><?php echo $record['BoardRes'][$i]['sub_id']?> <?php echo $record['BoardRes'][$i]['name']?> 投稿日：<?php echo $textCommon->get_datetimewithweek($record['BoardRes'][$i]['created'])?></dt>
			<dd><?php echo nl2br($record['BoardRes'][$i]['comment'])?></dd>
		<?php }?>
	</dl>

	<div class="bbs_write">
		<?php //start---2013/3/12 障害No.1-4-0008修正 ?>
		<?php //if($record['BoardRes'][count($record['BoardRes'])-1]['sub_id'] != 1000 || $record['Board']['show_order'] <= 2){?>
		<?php //start---2013/3/13 障害No.1-4-0011修正 ?>
		<?php //if(count($record['BoardRes']) == 0 || (count($record['BoardRes']) != 0 && $record['BoardRes'][count($record['BoardRes'])-1]['sub_id'] != 1000) || $record['Board']['show_order'] <= 2){?>
		<?php if(count($record['BoardRes']) == 0 || (count($record['BoardRes']) != 0 && $record['BoardRes'][count($record['BoardRes'])-1]['sub_id'] < 1000) || $record['Board']['show_order'] <= 2){?>
		<?php //end---2013/3/13 障害No.1-4-0011修正 ?>
		<?php //end---2013/3/12 障害No.1-4-0008修正 ?>
			<?php echo $form->create(null, array('type'=>'post', 'action' => ''));?>
	    	<?php echo $form->hidden('BoardRes.board_id', array('value' => $record['Board']['id'])); ?>
	        <?php echo $form->submit('書き込む', array('div' => false)); ?>&nbsp; 名前：<?php echo $form->text('BoardRes.name', array('value' => ($error_bid == $record['Board']['id'])?$data['BoardRes']['name'] : '')); ?>
			<?php if($error_bid == $record['Board']['id']) echo $form->error('BoardRes.name'); ?><br />
	        <?php echo $form->textarea('BoardRes.comment', array('cols' => 60, 'rows' => 6, 'value' => ($error_bid == $record['Board']['id'])?$data['BoardRes']['comment'] : '')); ?>
			<?php if($error_bid == $record['Board']['id']) echo $form->error('BoardRes.comment'); ?>
			<?php echo $form->end();?>
		<?php }?>
        <a href="<?php echo $linkCommon->get_boards_read($record)?>">全部読む</a> <a href="<?php echo $linkCommon->get_boards_read($record)?>/l50">最新50</a> <a href="<?php echo $linkCommon->get_boards_read($record)?>/1-">1-100</a> <a href="#top">板のトップ</a> <a href="<?php echo $linkCommon->get_boards_top($record)?>">リロード</a>
    </div>
</div>
<?php } ?>

<?php echo $this->element('pagenate/numbers'); ?>

<div class="bbs_post">
<?php echo $form->create(null, array('type'=>'post', 'action' => ''));?>
<table>
	<tr>
		<td align="right" valign="top">タイトル：</td>
		<td>
			<?php echo $form->text('Board.title'); ?>
			<?php echo $form->error('Board.title'); ?>
		</td>
		<td align="right"><?php echo $form->submit('新規スレッド作成', array('div' => false)); ?></td>
	</tr>
	<tr>
		<td align="right" valign="top">名前：</td>
		<td colspan="2">
			<?php echo $form->text('BoardRes.name', array('value' => ($error_bid == 0)?$data['BoardRes']['name'] : '')); ?>
			<?php if($error_bid == 0) echo $form->error('BoardRes.name'); ?>
		</td>
	</tr>
	<tr>
		<td align="right" valign="top">内容：</td>
		<td colspan="2">
			<?php echo $form->textarea('BoardRes.comment', array('cols' => 80, 'rows' => 10, 'value' => ($error_bid == 0)?$data['BoardRes']['comment'] : '')); ?>
			<?php if($error_bid == 0) echo $form->error('BoardRes.comment'); ?>
		</td>
	</tr>
</table>
<?php echo $form->end();?>
</div>

<div align="right" style="margin-right:5px">
	<img src="/images/point.png" style="margin-right:3px" /><a href="<?php echo $linkCommon->get_boards_read($board_delete)?>">削除依頼はこちら</a>
</div>



<?php //echo var_dump($list)?>
