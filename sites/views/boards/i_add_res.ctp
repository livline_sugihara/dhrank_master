
<div style="text-align:left;padding:5px;font-size:medium;background-color:#EFEBE3">
	「<?php echo $board['Board']['title']?>」に書き込む
</div>

<?php echo $form->create(null, array('type'=>'post', 'action' => 'add_res/' . $board['Board']['id'] . '?guid=ON' ,'encoding' => null));?>
<?php echo $form->hidden('BoardRes.board_id', array('value' => $board['Board']['id'])); ?>
<div style="text-align:left;padding:3px;">
    名前：<br />
	<?php echo $form->text('BoardRes.name'); ?><?php echo $form->error('BoardRes.name'); ?><br />
    本文：<br />
    <?php echo $form->textarea('BoardRes.comment', array('cols' => 30, 'rows' => 4)); ?>
	<?php echo $form->error('BoardRes.comment'); ?><br />
    <?php echo $form->submit('書き込む', array('div' => false)); ?>　
</div>
<?php echo $form->end();?>

<hr color="#DED7C8" />


<div style="text-align:center;padding:3px;">
	[<a href="<?php echo $linkCommon->get_boards_top($board)?>">戻る</a>]
</div>
