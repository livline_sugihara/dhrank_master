<section id="middle" class="wide">

    <div class="categoryRankingArea">
        
        <div class="title title03">
            <h2>REVIEWER ranking <span class="small">クチコミ投稿者ランキング</span></h2>
        </div>
        
        <!-- box -->
        <div class="box rv clearfix">
            <div class="rankNum">
                <img src="/img/crown_1.png" alt="" width="78"><br />
                <img src="/img/arrow_up2.png" alt="" width="78">
            </div>
            <p class="img"><img src="/img/tab_topictoukou_img1.png" alt="" height="80" width="80" class="img_frame"></p>
            <div class="summary">
                <div>
                    <span class="name"><a href="/review">デリヘル貴族 さん</a></span>
                    <span class="reviewNum"><a href="#">レビュー投稿　4件</a></span>
                </div>
                <p class="lbl one">最新のクチコミ</p>
                <p><a href="#">これがリアルな素人なのか！</a></p>
                <p class="info">こちらの投稿件数が多いので、内容を参考にして初めての利用。</p>
            </div>
        </div>
        
        <!-- box -->
        <div class="box rv clearfix">
            <div class="rankNum">
                <img src="/img/crown_2.png" alt="" width="78"><br />
                <img src="/img/arrow_up.png" alt="" width="78">
            </div>
            <p class="img"><img src="/img/tab_topictoukou_img2.png" alt="" height="80" width="80" class="img_frame"></p>
            <div class="summary">
                <div>
                    <span class="name"><a href="/review">キャベツさん太郎 さん</a></span>
                    <span class="reviewNum"><a href="#">レビュー投稿　4件</a></span>
                </div>
                <p class="lbl two">最新のクチコミ</p>
                <p><a href="#">仕事の疲れがとれた～そして綺麗</a></p>
                <p class="info">初のお店だったから電話するまでは少し緊張。でも実際はふ</p>
            </div>
        </div>
        
        <!-- box -->
        <div class="box rv clearfix">
            <div class="rankNum">
                <img src="/img/crown_3.png" alt="" width="78"><br />
                <img src="/img/arrow_down.png" alt="" width="78">
            </div>
            <p class="img"><img src="/img/tab_topictoukou_img3.png" alt="" height="80" width="80" class="img_frame"></p>
            <div class="summary">
                <div>
                    <span class="name"><a href="/review">ハチミツボーイ さん</a></span>
                    <span class="reviewNum"><a href="#">レビュー投稿　4件</a></span>
                </div>
                <p class="lbl three">最新のクチコミ</p>
                <p><a href="#">美人若妻とエグエロいAFプレイ！</a></p>
                <p class="info">翌日、博多に到着し、確認の電話を入れてからホテルにチェックイン。チ</p>
            </div>
        </div>

        <!-- box -->
        <div class="box rv clearfix">
            <div class="rankNum">
                <p class="num">4</p>
                <img src="/img/arrow_down.png" alt="" width="78">
            </div>
            <p class="img"><img src="/img/tab_topictoukou_img1.png" alt="" height="80" width="80" class="img_frame"></p>
            <div class="summary">
                <div>
                    <span class="name"><a href="/review">ハチミツボーイ さん</a></span>
                    <span class="reviewNum"><a href="#">レビュー投稿　4件</a></span>
                </div>
                <p class="lbl">最新のクチコミ</p>
                <p><a href="#">仕事の疲れがとれた～そして綺麗</a></p>
                <p class="info">初のお店だったから電話するまでは少し緊張。でも実際はふつうの対応だった。</p>
            </div>
        </div>
        
        <!-- box -->
        <div class="box rv clearfix">
            <div class="rankNum">
                <p class="num">5</p>
                <img src="/img/arrow_down.png" alt="" width="78">
            </div>
            <p class="img"><img src="/img/tab_topictoukou_img2.png" alt="" height="80" width="80" class="img_frame"></p>
            <div class="summary">
                <div>
                    <span class="name"><a href="/review">ハチミツボーイ さん</a></span>
                    <span class="reviewNum"><a href="#">レビュー投稿　4件</a></span>
                </div>
                <p class="lbl">最新のクチコミ</p>
                <p><a href="#">これがリアルな素人なのか！</a></p>
                <p class="info">こちらの投稿件数が多いので、内容を参考にして初めての利用。 投稿通り、受</p>
            </div>
        </div>
        
        
                            
    </div>
    <!-- /rankingArea -->
    
    <p id="top_banner">
        <a href="/review/list"><img src="/img/top_center_banner3.png" alt="" height="184" width="606"></a>
    </p>

</section>

<section id="side" class="narrow">
	<?php echo $this->element('common/side_narrow'); ?>
</section>