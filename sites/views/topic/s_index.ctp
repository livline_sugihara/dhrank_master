	<div class="link_o"><h2>このエリアのイチオシのお店</h2></div>
	<section id="rankingArea">


		<div id="topicSlider" class="inner">
<?php
	$cnt = min(count($topic_shop), 15);
	for($ii = 0; $ii < $cnt; $ii++) {
		$record = $topic_shop[$ii];
	    $opentime = date('H:i', strtotime($record['System']['open_time']));
	    $closetime = date('H:i', strtotime($record['System']['close_time']));
		$link = $linkCommon->get_user_shop($record);
?>

			<div class="rank_box links underline">
				<p class="shop_tt"><?php echo $record['Shop']['name']; ?><span>UP</span></p>
				<ul class="shop_icon clearfix">
			<?php if($record['Shop']['service01'] == 1) { ?><li class="pay_yoyaku">先行予約OK</li><?php } ?>
			<?php if($record['Shop']['service02'] == 1) { ?><li class="pay_credit">クレジットOK</li><?php } ?>
			<?php if($record['Shop']['service03'] == 1) { ?><li class="pay_ryoshu">領収書OK</li><?php } ?>
			<?php if($record['Shop']['service04'] == 1) { ?><li class="pay_machi">待ち合わせOK</li><?php } ?>
			<?php if($record['Shop']['service05'] == 1) { ?><li class="pay_cosplay">コスプレあり</li><?php } ?>
			<?php if($record['Shop']['service06'] == 1) { ?><li class="pay_coupon">クーポンあり</li><?php } ?>
				</ul>
				<div class="clearfix">
					<div class="box_left">
						<?php echo $imgCommon->get_shop_with_time($record, 'list_sp', array(528, 390)); ?>
						<p class="btn_red"><a href="<?php echo $html->url('/', true) ?>s/<?php echo $record['User']['id']; ?>/coupons">クーポンを見る</a></p>
					</div>
					<div class="box_right">
						<p class="rank_txt"><a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php // echo nl2br($record['Shop']['about_contents']); ?><?php echo mb_strimwidth($record['Shop']['about_title'], 0, 140,'...','UTF-8');?></a></p>
						<ul class="clearfix">
							<li class="icon_access"><?php echo $categories_value_list[$record['Shop']['shops_business_category_id']];?>/<?php echo $record['System']['address']; ?></li>
							<li class="icon_people">在籍数<br><?php echo $record['Girl']['cnt']; ?>人</li>
							<li class="icon_price">最安値料金<br><?php echo $record['System']['price_cost']; ?>円～</li>
							<li class="icon_kuchikomi_up">口コミ<br><?php echo $record['Review']['cnt']; ?>件</li>
							<li class="icon_time_up">営業時間<br><?php echo $opentime; ?>〜<?php echo $closetime; ?></li>
						</ul>
					</div>
				</div>
			</div>
<?php
}
?>
		</div>
		<!-- /topicSlider -->

<?php // echo $this->element('s_common/line_mail'); ?>


	</section>
<?php echo $this->element('s_common/footer_banner'); ?>

<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./topic/" itemprop="url"><span itemprop="title">イチオシのお店</span></a></li>
	</ul>
</nav>
