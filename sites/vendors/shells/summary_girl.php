<?php
class SummaryGirlShell extends Shell {
//	var $tasks = array('Summary');
	var $uses = array('Shop', 'Access', 'SummaryData');

	function main() {

		// 対象データは1週間
		$limit_day = date('Y-m-d', strtotime('-' . APP_SUMMARY_REVIEW_RANGE));

		$result = $this->Access->find('all', array(
			'fields' => array(
				'Access.large_area_id',
				'Access.relation_id AS girl_id',
				'sum(Access.count) AS access_count'
			),
			'conditions' => array(
				'Access.access_type' => Access::ACCESS_TYPE_GIRL,
				'DATE_FORMAT(Access.date,"%Y-%m-%d") >= ' => $limit_day,
			),
			'group' => 'Access.large_area_id, Access.relation_id'
		));

//		$this->out(var_export($result, true));

//		$this->Summary->execute();

		foreach($result AS $key => $record) {

			// 条件部分を作成
			$conditions = array(
				'summary_type' => SummaryData::SUMMARY_TYPE_GIRL,
				'large_area_id' => $record['Access']['large_area_id'],
				'date' => date('Y-m-d'),
				'relation_id' => $record['Access']['girl_id'],
			);

			// データが既にあるかどうかの確認
			$insData = $this->SummaryData->find('first', array(
				'conditions' => $conditions,
			));

			// すでにある場合は更新
			if(empty($insData))
			{
				// 挿入値を作成
				$insData = array('SummaryData' => $conditions);
			}
			$insData['SummaryData']['value'] = $record[0]['access_count'];
			$insData['SummaryData']['rank'] = SummaryData::RANK_MAX;

			$this->SummaryData->save($insData['SummaryData']);
			$this->SummaryData->create();

//			$this->out(var_export($insData, true));
		}

		// 並び順を更新
		// FIXME ここは他のロジックとまとめたい
		$this->SummaryData->bindModel(array('belongsTo' => array('Girl' => array(
			'className'=>'Girl',
			'type'=>'inner',
			'conditions' => array(
				'Girl.id = SummaryData.relation_id',
			),
			'foreignKey' => false,))),false);

		$this->SummaryData->bindModel(array('belongsTo' => array('User' => array(
			'className'=>'User',
			'type'=>'inner',
			'conditions' => array(
				'User.id = Girl.user_id',
			),
			'foreignKey' => false,))),false);

		$this->SummaryData->bindModel(array('belongsTo' => array('UsersAuthority' => array(
			'className'=>'UsersAuthority',
			'type'=>'inner',
			'conditions' => array(
				'UsersAuthority.user_id = Girl.user_id',
				'UsersAuthority.large_area_id = SummaryData.large_area_id',
			),
			'foreignKey' => false,))),false);

		$result = $this->SummaryData->find('all', array(
			'conditions' => array(
				'summary_type' => SummaryData::SUMMARY_TYPE_GIRL,
				'date' => date('Y-m-d'),
				'rank' => SummaryData::RANK_MAX,
			),
			'order' => 'SummaryData.large_area_id ASC, SummaryData.value DESC, UsersAuthority.order_rank ASC',
		));
		$this->SummaryData->unbindModel(array('belongsTo' => array('User', 'Girl')));

		$rank = 0;
		$oldValue = 0;
		$large_area_id = 0;
		$area_cnt = 0;
		foreach($result AS $ii => $record) {

			// 前回のランクを取得
			$prev_data = $this->SummaryData->find('first', array(
				'conditions' => array(
					'SummaryData.summary_type' => SummaryData::SUMMARY_TYPE_GIRL,
					'SummaryData.large_area_id' => $record['SummaryData']['large_area_id'],
					'DATE_FORMAT(SummaryData.date,"%Y-%m-%d") < ' => date('Y-m-d'),
					'SummaryData.relation_id' => $record['SummaryData']['relation_id'],
				),
				'order' => 'SummaryData.date DESC'
			));

			if(empty($prev_data)) {
				$prev_rank = SummaryData::RANK_MAX;
			} else {
				$prev_rank = $prev_data['SummaryData']['rank'];
			}

			// エリアIDが違う場合は次のエリアに移行
			if($record['SummaryData']['large_area_id'] != $large_area_id) {
				$rank = 0;
				$oldValue = 0;
				$large_area_id = $record['SummaryData']['large_area_id'];
				$area_cnt = 0;
			}

			// 同立以外はカウントアップ
			if($oldValue != $record['SummaryData']['value']) {
				$rank = $area_cnt + 1;
				$oldValue = $record['SummaryData']['value'];
			}

			$record['SummaryData']['rank'] = $rank;
			$record['SummaryData']['prev_rank'] = $prev_rank;
			$this->SummaryData->save($record['SummaryData']);
			$this->SummaryData->create();

			$area_cnt++;
		}
	}
}
?>