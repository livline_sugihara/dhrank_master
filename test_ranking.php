<?php
class TestRanking extends AppModel {
	var $name = 'TestRanking';
	var $useTable = false;

	// 指定された店舗のルックスのランキングを取得
	function looks($user_id = '', $category_id = '', $large_area_id = '', $limit = 5)
	{
		$sql =  ' SELECT  ranking.avg, girls.name, girls.user_id, girls.id, shops.name FROM (';
		$sql .= '   SELECT TRUNCATE(AVG(score_girl_looks), 2) as avg, girl_id';
		$sql .= '   FROM reviews';
		$sql .= '   WHERE delete_flag = 0';
		if ($user_id != '')
			$sql .= ' AND user_id = ' . $user_id;
		$sql .= '   GROUP BY girl_id';
		$sql .= ' ) AS ranking';
		$sql .= ' LEFT JOIN girls';
		$sql .= ' ON ranking.girl_id = girls.id';
		$sql .= ' LEFT JOIN shops';
		$sql .= ' ON girls.user_id = shops.user_id';
		$sql .= ' LEFT JOIN users';
		$sql .= ' ON girls.user_id = users.id';
		$sql .= ' WHERE girls.is_deleted = 0';
		$sql .= ' AND users.large_area_id = "' . $large_area_id . '"';
		if ($category_id != '')
			$sql .= ' AND shops.shops_business_category_id = "' . $category_id . '"';
		$sql .= ' ORDER BY avg DESC';
		$sql .= ' LIMIT ' . $limit;

		$looks_datas = $this->query($sql);
        return $looks_datas;
	}

	// 店舗のサービスランキング($user_idがなければ、エリア店舗全体での検索)
	function service($user_id = '', $category_id = '', $large_area_id = '', $limit = 5)
	{
		// 指定された店舗の女性のサービスのランキングを取得
		$sql =  ' SELECT ranking.avg, girls.name, girls.user_id, girls.id , shops.name FROM (';
		$sql .= '   SELECT TRUNCATE(AVG(score_girl_service), 2) as avg, girl_id, shop_name';
		$sql .= '   FROM reviews';
		$sql .= '   WHERE delete_flag = 0';
		if ($user_id != '')
			$sql .= ' AND user_id = ' . $user_id;
		$sql .= '   GROUP BY girl_id';
		$sql .= ' ) AS ranking';
		$sql .= ' LEFT JOIN girls';
		$sql .= ' ON ranking.girl_id = girls.id';
		$sql .= ' LEFT JOIN shops';
		$sql .= ' ON girls.user_id = shops.user_id';
		$sql .= ' LEFT JOIN users';
		$sql .= ' ON girls.user_id = users.id';
		$sql .= ' WHERE girls.is_deleted = 0';
		$sql .= ' AND users.large_area_id = "' . $large_area_id . '"';
		if ($category_id != '')
			$sql .= ' AND shops.shops_business_category_id = "' . $category_id . '"';
		$sql .= ' ORDER BY avg DESC';
		$sql .= ' LIMIT ' . $limit;

		$service_datas = $this->query($sql);
		return $service_datas;
	}

	// キャラクターランキング($user_idがなければ、エリア店舗全体での検索)
	function character($user_id = '', $category_id = '', $large_area_id = '', $limit = 5)
	{
		// 指定された店舗の女性の性格のランキングを取得
		$sql =  ' SELECT ranking.avg, girls.name, girls.user_id, girls.id , shops.name  FROM (';
		$sql .= '   SELECT TRUNCATE(AVG(score_girl_character), 2) as avg, girl_id, shop_name';
		$sql .= '   FROM reviews';
		$sql .= '   WHERE delete_flag = 0';
		if ($user_id != '')
			$sql .= ' AND user_id = ' . $user_id;
		$sql .= '   GROUP BY girl_id';
		$sql .= ' ) AS ranking';
		$sql .= ' LEFT JOIN girls';
		$sql .= ' ON ranking.girl_id = girls.id';
		$sql .= ' LEFT JOIN shops';
		$sql .= ' ON girls.user_id = shops.user_id';
		$sql .= ' LEFT JOIN users';
		$sql .= ' ON girls.user_id = users.id';
		$sql .= ' WHERE girls.is_deleted = 0';
		$sql .= ' AND users.large_area_id = "' . $large_area_id . '"';
		if ($category_id != '')
			$sql .= ' AND shops.shops_business_category_id = "' . $category_id . '"';
		$sql .= ' ORDER BY avg DESC';
		$sql .= ' LIMIT ' . $limit;

		$character_datas = $this->query($sql);
		return $character_datas;
	}

	// レビュワーの口コミ数ランキング(「ありがとう」で集計)
	// $dayは集計期間
	function reviewer_count($large_areas_id = 13, $limit = 5, $day = 7)
	{
		$timestamp = strtotime('-' . $day . ' day');
		$start_date = date('Y-m-d 00:00:00', $timestamp);
		$end_date = date('Y-m-d 23:59:59', $timestamp + (60 * 60 * 24 * 6)); // 1日前

// TEST用
$start_date = '2014-04-01 00:00:00';
$end_date = '2014-04-31 00:00:00';
// TEST用

		// レビュワーの名前、レビュワーのID,口コミ数、アバター,コメント
		$sql =  ' SELECT reviewers.handle, reviews_good_counts.review_id, reviewers.avatar_id, reviewers.comment, COUNT(*) AS cnt, reviewers.large_area_id';
		$sql .= ' FROM reviews_good_counts';
		$sql .= ' LEFT JOIN reviewers';
		$sql .= ' ON reviews_good_counts.review_id = reviewers.id';
		$sql .= ' LEFT JOIN reviewer_registers';
		$sql .= ' ON reviewer_registers.id = reviewers.id';
		$sql .= ' AND is_created = 1';
		$sql .= ' WHERE reviewers.handle IS NOT NULL';
		$sql .= ' AND reviewers.large_area_id = "' . $large_areas_id . '"';
		$sql .= ' AND reviews_good_counts.created >= "' . $start_date . '"';
		$sql .= ' AND reviews_good_counts.created <= "' . $end_date . '"';
		$sql .= ' GROUP BY reviews_good_counts.review_id';
		$sql .= ' ORDER BY cnt DESC';
		$sql .= ' LIMIT ' . $limit;

		$reviewer_datas = $this->query($sql);

		// 2週間前に集計したものを、DELETE
		$this->deleteData($large_areas_id, 3, $day = 14);

		// INSERT
		$insert_sql_arr = array();
		$sql = ' INSERT INTO rankings ';
		$sql .= ' (ranking_id, ranking_name, ranking_comment, count, standing, before_standing, avatar_id, large_areas_id, category, created, modified)';
		$sql .= ' VALUES ';
		$standing = 1; // 順位
		$tie_standing = 1; // タイ順位
		$count = 0; // カウント
		foreach ($reviewer_datas as $key => $val)
		{
			$insert_sql = '';
			$insert_sql .= ' (';
			$insert_sql .= ' "' . $val['reviews_good_counts']['review_id']. '",';
			$insert_sql .= ' "' . $val['reviewers']['handle']. '",';
			$insert_sql .= ' "' . $val['reviewers']['comment']. '",';
			$insert_sql .= ' "' . $val[0]['cnt'] . '",';
			if ($count == $val[0]['cnt'])
			{
				// 同位の場合
				$insert_sql .= ' "' . $tie_standing . '",';
			}
			else{
				// 違う場合
				$tie_standing = $standing;
				$insert_sql .= ' "' . $standing . '",';
			}
			// 前回の順位を取得
			$select_sql = ' SELECT standing';
			$select_sql .= ' FROM rankings';
			$select_sql .= ' WHERE ranking_id =' . $val['reviews_good_counts']['review_id'];
			$select_sql .= ' LIMIT 1';
			$before_standing_data = $this->query($select_sql);
			if (count($before_standing_data) == 0)
				$before_standing = 0;
			else
				$before_standing = $before_standing_data[0]['rankings']['standing'];

			$insert_sql .= ' "' . $before_standing. '",';
			$insert_sql .= ' "' . $val['reviewers']['avatar_id']. '",';
			$insert_sql .= ' "' . $val['reviewers']['large_area_id'] . '",';
			$insert_sql .= ' "3",'; // 口コミのカテゴリは3
			$insert_sql .= ' now(),';
			$insert_sql .= ' now()';
			$insert_sql .= ' )';
			$insert_sql_arr[] = $insert_sql;
			$count = $val[0]['cnt'];
			$standing++;

		}
		$sql .= implode(',', $insert_sql_arr);
		$this->query($sql);
	}

/*
	// 口コミ投稿者のカウント集計
	function access_count($large_areas_id = 13, $limit = 5, $day = 7)
	{
		$timestamp = strtotime('-' . $day . ' day');
		$start_date = date('Y-m-d 00:00:00', $timestamp);
		$end_date = date('Y-m-d 23:59:59', $timestamp + (60 * 60 * 24 * 6)); // 1日前

		// レビュワーのアクセスをカウントする
		$sql = ' SELECT reviewers.id, reviewers.access_count, reviewers.avatar_id, reviewers.handle, reviewers.comment';
		$sql .= ' FROM reviewers';
		$sql .= ' LEFT JOIN reviewer_registers';
		$sql .= ' ON reviewer_registers.id = reviewers.id ';
		$sql .= ' AND reviewer_registers.is_created = 1';
		$sql .= ' AND reviewer.large_area_id = ';
		$sql .= ' ORDER BY access_count DESC';
		$sql .= ' LIMIT ' . $limit;

		$access_datas = $this->query($sql);

		// 2週間前に集計したものを、DELETE
		$this->deleteData($large_areas_id, 2, 14);

		// INSERT
		$insert_sql_arr = array();
		$sql = ' INSERT INTO rankings ';
		$sql .= ' (ranking_id, ranking_name, ranking_comment, count, standing, before_standing, avatar_id, category, created, modified)';
		$sql .= ' VALUES ';
		$standing = 1; // 順位
		$tie_standing = 1; // タイ順位
		$count = 0; // カウント
		foreach ($access_datas as $key => $val)
		{
			$insert_sql = '';
			$insert_sql .= ' (';
			$insert_sql .= ' "' . $val['reviewers']['id']. '",';
			$insert_sql .= ' "' . $val['reviewers']['handle']. '",';
			$insert_sql .= ' "' . $val['reviewers']['comment']. '",';
			$insert_sql .= ' "' . $val['reviewers']['access_count']. '",';
			if ($count == $val['reviewers']['access_count'])
			{
				// 同位の場合
				$insert_sql .= ' "' . $tie_standing . '",';
			}
			else{
				// 違う場合
				$tie_standing = $standing;
				$insert_sql .= ' "' . $standing . '",';
			}

			// 前回の順位を取得
			$select_sql = ' SELECT standing';
			$select_sql .= ' FROM rankings';
			$select_sql .= ' WHERE ranking_id =' . $val['reviewers']['id'];
			$select_sql .= ' LIMIT 1';
			$before_standing_data = $this->query($select_sql);
			if (count($before_standing_data) == 0)
				$before_standing = 0;
			else
				$before_standing = $before_standing_data[0]['rankings']['standing'];

			$insert_sql .= ' "' . $before_standing. '",';
			$insert_sql .= ' "' . $val['reviewers']['avatar_id']. '",';
			$insert_sql .= ' "2",'; // アクセスカウントのカテゴリは２
			$insert_sql .= ' now(),';
			$insert_sql .= ' now()';
			$insert_sql .= ' )';
			$insert_sql_arr[] = $insert_sql;
			$count = $val['reviewers']['access_count'];
			$standing++;
		}
		$sql .= implode(',', $insert_sql_arr);
		$this->query($sql);
	}
*/
	// アクセスの多い女の子集計
	function girls_count($large_areas_id = 13, $limit = 5, $day = 7)
	{
		$timestamp = strtotime('-' . $day . ' day');
		$start_date = date('Y-m-d 00:00:00', $timestamp);
		$end_date = date('Y-m-d 23:59:59', $timestamp + (60 * 60 * 24 * ($day - 1))); // 1日前

		// TEST用
		$start_date = '2014-01-01 00:00:00';
		$end_date = '2014-04-30 23:59:59';
		// TEST用

		$sql = '  SELECT users.large_area_id, girls.id, girls.self_comment, girls.access_count, girls.name';
		$sql .= ' FROM users';
		$sql .= ' INNER JOIN shops';
		$sql .= ' ON shops.user_id = users.id';
		$sql .= ' LEFT JOIN girls';
		$sql .= ' ON girls.user_id = shops.user_id';
		$sql .= ' WHERE girls.is_deleted = 0';
		$sql .= ' AND users.large_area_id = "' . $large_areas_id . '"';
		$sql .= ' ORDER BY girls.access_count DESC';
		$sql .= ' LIMIT ' . $limit;
		$girls_datas = $this->query($sql);

		// 女の子の口コミの平均点取得
		foreach($girls_datas as $key => $val)
		{
			$review_sql =  ' SELECT ROUND(((reviews.score_girl_character + reviews.score_girl_service + reviews.score_girl_service) / 3),2) AS girl_avg ';
			$review_sql .= ' FROM reviews';
			$review_sql .= ' WHERE reviews.girl_id = "' . $val['girls']['id'] . '"';
			$review_sql .= ' AND reviews.created >= "' . $start_date . '"';
			$review_sql .= ' AND reviews.created <= "' . $end_date . '"';
			$review_sql .= ' AND reviews.is_publish = 1';
			$review_sql .= ' AND reviews.delete_flag = 0';
			$review_sql .= ' GROUP BY reviews.girl_id';
			$review_data = $this->query($review_sql);
			// 平均点が取得できなければ０を入れる
			if (isset($review_data[0][0]['girl_avg']))
				$girls_datas[$key]['girl_avg'] = $review_data[0][0]['girl_avg'];
			else
				$girls_datas[$key]['girl_avg'] = 0;
		}


		// 2週間前に集計したものを、DELETE
		$this->deleteData($large_areas_id, 2, 14);

		// INSERT
		$insert_sql_arr = array();
		$sql = ' INSERT INTO rankings ';
		$sql .= ' (ranking_id, ranking_name, ranking_comment, count, standing, before_standing, avatar_id, large_areas_id, category, created, modified)';
		$sql .= ' VALUES ';
		$standing = 1; // 順位
		$tie_standing = 1; // タイ順位
		$count = 0; // カウント

		foreach ($girls_datas as $key => $val)
		{
			$insert_sql = '';
			$insert_sql .= ' (';
			$insert_sql .= ' "' . $val['girls']['id']. '",';
			$insert_sql .= ' "' . $val['girls']['name']. '",';
			$insert_sql .= ' "' . $val['girls']['self_comment']. '",';
			// 女性の口コミの平均点を取得
			$insert_sql .= ' "' . $val['girl_avg']. '",';
			if ($count == $val['girls']['access_count'])
			{
				// 同位の場合
				$insert_sql .= ' "' . $tie_standing . '",';
			}
			else{
				// 違う場合
				$tie_standing = $standing;
				$insert_sql .= ' "' . $standing . '",';
			}

			// 前回の順位を取得
			$select_sql = ' SELECT standing';
			$select_sql .= ' FROM rankings';
			$select_sql .= ' WHERE ranking_id =' . $val['girls']['id'];
			$select_sql .= ' LIMIT 1';
			$before_standing_data = $this->query($select_sql);
			if (count($before_standing_data) == 0)
				$before_standing = 0;
			else
				$before_standing = $before_standing_data[0]['rankings']['standing'];

			$insert_sql .= ' "' . $before_standing. '",';
			$insert_sql .= ' "",';
			$insert_sql .= ' "' . $val['users']['large_area_id'] . '",';
			$insert_sql .= ' "2",'; // 女の子のカテゴリは2
			$insert_sql .= ' now(),';
			$insert_sql .= ' now()';
			$insert_sql .= ' )';
			$insert_sql_arr[] = $insert_sql;
			$count = $val['girls']['access_count'];
			$standing++;
		}
		$sql .= implode(',', $insert_sql_arr);
		$this->query($sql);

	}

	// お店総合集計
	function shops_count($large_areas_id = 13, $limit = 5, $day = 7)
	{
		// 2週間前に集計したものを、DELETE
		$this->deleteData($large_areas_id, 1, 14);

		$timestamp = strtotime('-' . $day . ' day');
		$start_date = date('Y-m-d 00:00:00', $timestamp);
		$end_date = date('Y-m-d 23:59:59', $timestamp + (60 * 60 * 24 * ($day - 1))); // 1日前

		// TEST用
		$start_date = '2014-01-24 00:00:00';
		$end_date = '2014-01-30 23:59:59';
		// TEST用

		// 1週間分の口コミを集計
		$sql = ' SELECT *, ROUND(((shop_value.cha + shop_value.ser + shop_value.loo) / 3),2) AS shop_avg FROM ';
		$sql .= ' (SELECT users.id, users.large_area_id,';
		$sql .= ' TRUNCATE(AVG(reviews.score_girl_character),1) AS cha,';
		$sql .= ' TRUNCATE(AVG(reviews.score_girl_service),1) as ser, ';
		$sql .= ' TRUNCATE(AVG(reviews.score_girl_looks),1) as loo,';
		$sql .= ' shops.name,';
		$sql .= ' shops.about_title,';
		$sql .= ' shops.about_contents';
		$sql .= ' FROM users';
		$sql .= ' INNER JOIN shops';
		$sql .= ' ON shops.user_id = users.id';
		$sql .= ' LEFT JOIN reviews';
		$sql .= ' ON reviews.user_id = users.id';
		$sql .= ' AND reviews.created >= "' . $start_date . '"';
		$sql .= ' AND reviews.created <= "' . $end_date . '"';
		$sql .= ' AND reviews.is_publish = 1';
		$sql .= ' AND reviews.delete_flag = 0';
		$sql .= ' WHERE users.large_area_id = "' . $large_areas_id . '"';
		$sql .= ' GROUP BY reviews.user_id';
		$sql .= ' ) AS shop_value';
		$sql .= ' ORDER BY shop_avg DESC';

		$shop_datas = $this->query($sql);

		// INSERT
		$insert_sql_arr = array();
		$sql = ' INSERT INTO rankings ';
		$sql .= ' (ranking_id, ranking_name, ranking_comment, count, standing, before_standing, avatar_id, large_areas_id, category, created, modified)';
		$sql .= ' VALUES ';
		$standing = 1; // 順位
		$tie_standing = 1; // タイ順位
		$count = 0; // カウント

		// ランキング順に。
		foreach($shop_datas as $key => $val)
		{
			$insert_sql = '';
			$insert_sql .= ' (';
			$insert_sql .= ' "' . $val['shop_value']['id']. '",';
			$insert_sql .= ' "' . mysql_real_escape_string($val['shop_value']['name']). '",';
			$insert_sql .= ' "' . $val['shop_value']['about_title']. '",';
			$insert_sql .= ' "' . $val[0]['shop_avg']. '",';
			if ($count == $val[0]['shop_avg'])
			{
				// 同位の場合
				$insert_sql .= ' "' . $tie_standing . '",';
			}
			else{
				// 違う場合
				$tie_standing = $standing;
				$insert_sql .= ' "' . $standing . '",';
			}

			// 前回の順位を取得
			$select_sql = ' SELECT standing';
			$select_sql .= ' FROM rankings';
			$select_sql .= ' WHERE ranking_id = "' . $val['shop_value']['id'] . '"';
			$select_sql .= ' LIMIT 1';
			$before_standing_data = $this->query($select_sql);
			if (count($before_standing_data) == 0)
				$before_standing = 0;
			else
				$before_standing = $before_standing_data[0]['rankings']['standing'];

			$insert_sql .= ' "' . $before_standing. '",';
			$insert_sql .= ' "0",';
			$insert_sql .= ' "' . $val['shop_value']['large_area_id'] . '",';
			$insert_sql .= ' "1",'; // お店の総合評価のカテゴリは1
			$insert_sql .= ' now(),';
			$insert_sql .= ' now()';
			$insert_sql .= ' )';
			$insert_sql_arr[] = $insert_sql;
			$count = $val[0]['shop_avg'];
			$standing++;
		}

		$sql .= implode(',', $insert_sql_arr);
		$this->query($sql);

	}



/* 使わない
	// 口コミ投稿者のアクセスカウント取得
	function getAccessCountData($limit = 5, $day = 7)
	{
		$timestamp = strtotime('-' . $day . ' day');
		$start_date = date('Y-m-d 00:00:00', $timestamp);
		$end_date = date('Y-m-d 23:59:59', $timestamp + (60 * 60 * 24 * ($day - 1))); // 1日前

		$sql = ' SELECT rankings.ranking_id, rankings.ranking_name, rankings.avatar_id, rankings.ranking_comment, rankings.count, rankings.standing, rankings.before_standing';
		$sql .= ' FROM rankings';
		$sql .= ' WHERE category = "2"'; // アクセスランキングのカテゴリは２
		$sql .= ' AND rankings.created >= "' . $start_date . '"';
		$sql .= ' AND rankings.created <= "' . $end_date . '"';
		$sql .= ' ORDER BY rankings.standing ASC'; // count降順
		$sql .= ' LIMIT ' . $limit;

		$ranking_datas = $this->query($sql);

		return $ranking_datas;
	}
*/
	// お店の総合評価を取得
	function getShopRankingData($large_area_id = 13, $limit = 5, $day = 7)
	{
		$timestamp = strtotime('-' . $day . ' day');
		$start_date = date('Y-m-d 00:00:00', $timestamp);
		$end_date = date('Y-m-d 23:59:59', $timestamp + (60 * 60 * 24 * ($day - 1))); // 1日前

		$sql = ' SELECT ranking_id, ranking_name, avatar_id, ranking_comment, count, standing, before_standing, large_areas_id';
		$sql .= ' FROM rankings';
		$sql .= ' WHERE category = "1"'; // お店の総合カテゴリは1
		$sql .= ' AND large_areas_id = "' . $large_area_id . '"';
//		$sql .= ' AND created >= "' . $start_date . '"';
//		$sql .= ' AND created <= "' . $end_date . '"';
		$sql .= ' ORDER BY standing ASC'; // count降順
		$sql .= ' LIMIT ' . $limit;

		$ranking_datas = $this->query($sql);

		// ランキングで取得した店舗の情報取得
		foreach($ranking_datas as $key => $val)
		{
			$sql = '';
			$sql = ' SELECT COUNT(*) AS review_cnt, shops.about_title, shops.about_contents, shops.shops_business_category_id';
			$sql .= ' FROM reviews';
			$sql .= ' LEFT JOIN shops';
			$sql .= ' ON reviews.user_id = shops.user_id';
			$sql .= ' WHERE reviews.user_id = "' . $val['rankings']['ranking_id'] . '"';
			$sql .= ' AND reviews.is_publish = 1';
			$sql .= ' AND reviews.delete_flag = 0';
			$sql .= ' GROUP BY reviews.user_id';
			$sql .= ' LIMIT 1';
			$reviews_cnt_data = $this->query($sql);
			$reviews_cnt = ($reviews_cnt_data[0][0]['review_cnt'] != NULL) ? $reviews_cnt_data[0][0]['review_cnt'] : 0;
			$ranking_datas[$key]['rankings']['review_cnt'] = $reviews_cnt;
			$ranking_datas[$key]['rankings']['about_title'] = $reviews_cnt_data[0]['shops']['about_title'];
			$ranking_datas[$key]['rankings']['about_contents'] = $reviews_cnt_data[0]['shops']['about_contents'];
			$ranking_datas[$key]['rankings']['shops_business_category_id'] = $reviews_cnt_data[0]['shops']['shops_business_category_id'];

			// about_titleが空の場合
			if (   $ranking_datas[$key]['rankings']['about_title'] == ''
				|| $ranking_datas[$key]['rankings']['about_title'] == NULL ) $ranking_datas[$key]['rankings']['about_title'] = '只今店舗情報準備中です。しばらくお待ちください。';
			// about_contentsが空の場合
			if (   $ranking_datas[$key]['rankings']['about_contents'] == ''
				|| $ranking_datas[$key]['rankings']['about_contents'] == NULL ) $ranking_datas[$key]['rankings']['about_contents'] = '只今店舗情報準備中です。しばらくお待ちください。';

		}
		return $ranking_datas;
	}

	// アクセスの多い女の子を取得
	function getGirlRankingData($large_areas_id = 13, $limit = 5, $day = 7)
	{
		$timestamp = strtotime('-' . $day . ' day');
		$start_date = date('Y-m-d 00:00:00', $timestamp);
		$end_date = date('Y-m-d 23:59:59', $timestamp + (60 * 60 * 24 * ($day - 1))); // 1日前

		$sql = ' SELECT ranking_id, ranking_name, avatar_id, ranking_comment, count, standing, before_standing, large_areas_id, girls.*, shops.*';
		$sql .= ' FROM rankings';
		$sql .= ' INNER JOIN girls';
		$sql .= ' ON girls.id = rankings.ranking_id';
		$sql .= ' INNER JOIN shops';
		$sql .= ' ON girls.user_id = shops.user_id';
		$sql .= ' WHERE category = "2"';
		$sql .= ' AND large_areas_id = "' . $large_areas_id . '"';
//		$sql .= ' AND created >= "' . $start_date . '"';
//		$sql .= ' AND created <= "' . $end_date . '"';
		$sql .= ' ORDER BY standing ASC';
		$sql .= ' LIMIT ' . $limit;

		$ranking_datas = $this->query($sql);

		// レビューが何件か取得する
		foreach($ranking_datas as $key => $val)
		{
			$review_sql =  ' SELECT count(*) AS girl_review_count ';
			$review_sql .= ' FROM reviews';
			$review_sql .= ' WHERE reviews.girl_id = "' . $val['girls']['id'] . '"';
			// $review_sql .= ' AND reviews.created >= "' . $start_date . '"';
			// $review_sql .= ' AND reviews.created <= "' . $end_date . '"';
			$review_sql .= ' AND reviews.is_publish = 1';
			$review_sql .= ' AND reviews.delete_flag = 0';
			$review_sql .= ' GROUP BY reviews.girl_id';
			$review_data = $this->query($review_sql);
			// 平均点が取得できなければ０を入れる
			if (isset($review_data[0][0]['girl_review_count']))
				$ranking_datas[$key]['girl_review_count'] = $review_data[0][0]['girl_review_count'];
			else
				$ranking_datas[$key]['girl_review_count'] = 0;
		}
		return $ranking_datas;
	}

	// 口コミランキング取得
	function getReviewerRankingData($large_areas_id = 13, $limit = 5, $day = 7)
	{
		$timestamp = strtotime('-' . $day . ' day');
		$start_date = date('Y-m-d 00:00:00', $timestamp);
		$end_date = date('Y-m-d 23:59:59', $timestamp + (60 * 60 * 24 * ($day - 1))); // 1日前

		$sql = ' SELECT ranking_id, ranking_name, avatar_id, ranking_comment, count, standing, before_standing, large_areas_id';
		$sql .= ' FROM rankings';
		$sql .= ' WHERE category = "3"'; // 口コミランキングのカテゴリは3
		$sql .= ' AND large_areas_id = "' . $large_areas_id . '"';
		// $sql .= ' AND created >= "' . $start_date . '"';
		// $sql .= ' AND created <= "' . $end_date . '"';
		$sql .= ' ORDER BY standing ASC';
		$sql .= ' LIMIT ' . $limit;
		$ranking_datas = $this->query($sql);

		return $ranking_datas;

	}

	// 使わないデータを削除
	function deleteData($large_areas_id, $category, $day = 14)
	{
		$timestamp = strtotime('-' . $day . ' day');
		$date = date('Y-m-d 23:59:59', $timestamp);

		$sql = ' DELETE FROM rankings';
		$sql .= ' WHERE category = "'. $category . '"';
		$sql .= ' AND large_areas_id = "'. $large_areas_id . '"';
		$sql .= ' AND created <= "'. $date . '"';
		$this->query($sql);
	}


}
?>
