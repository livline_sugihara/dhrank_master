<?php
class AppError extends ErrorHandler {
	public function dispatchMethod($method, $messages) {
		//ユーザー情報取得
		//総合管理者アカウントデータ
		$this->controller->set('parent_admin', $this->controller->Admin->findbyId($this->controller->Auth->user('id')));

		parent::dispatchMethod($method, $messages);
	}
}
