<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.app
 */


class AppController extends Controller {
	var $uses = array('Admin');
	var $components = array('Auth');
	var $helpers = array('Html','Form','Text','TextCommon','Session');
	var $parent_admin;
	var $parent_admin_large_area_list;
	var $start_time;

	function beforeFilter() {
		$this->start_time = microtime(TRUE);
		$this->Auth->authError = "ログインをお願いします。";
		$this->Auth->loginError = "ログインに失敗しました。";
		$this->layout = 'default';
		$this->setAdminAuth();

		if($this->Auth->user()){
			$this->parent_admin =$this->Admin->findById($this->Auth->user('id'));
			$this->set('parent_admin', $this->parent_admin);
			$this->parent_admin_large_area_list = Set::extract('/AdminsAuthLargeArea[selected=1]/large_area_id', $this->parent_admin);
		}elseif(!(($this->name == 'Admins' && $this->action == 'login') || ($this->name == 'Admins' && $this->action == 'logout'))){
			//AuthにAdminアカウントデータがない場合は、セッションタイムアウトのため、ログインとログアウト画面以外はログイン画面へ
			$this->redirect('/admins/login');
		}
	}

	# 管理画面領域用に、adminをアカウントテーブルに使用するよう設定
	function setAdminAuth() {
		$this->Auth->userModel = 'Admin';
		$this->Auth->loginRedirect = '/';
	}
	function afterFilter(){
		parent::afterFilter();
		// 時間表示していたので、コメントアウト
		//echo (Configure::read('debug'))?(microtime(TRUE) - $this->start_time)*1000:'';
	}
}
