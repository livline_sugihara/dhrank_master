<?php

class LargeArea extends AppModel {
	public $name = 'LargeArea';

	/** 表示グレード(無料パターン) */
	const DISPLAY_GRADE_FREE = 0;
	/** 表示グレード(半有料パターン) */
	const DISPLAY_GRADE_MIDDLE = 1;
	/** 表示グレード(有料パターン) */
	const DISPLAY_GRADE_PAY = 2;


	public $validate = array(
		'name'=>array(
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'isUnique','message'=>'既に登録されている大エリア名です。'),
			array('rule' => 'notEmpty','message'=>'大エリア名を記入してください。'),
		),
		'url'=>array(
			array('rule' => array('lastStr','/'),'message'=>'最後は"/"(スラッシュ)を入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'url','message'=>'URLを入力してください。'),
			array('rule' => 'isUnique','message'=>'既に登録されているURLです。'),
			array('rule' => 'notEmpty','message'=>'URLを入力してください。'),
		),
		// 'link_order_index'=>array('rule' => 'notEmpty','message'=>'選択してください。'),
		// 'is_display_footer'=>array('rule' => 'notEmpty','message'=>'選択してください。'),
		'display_grade'=>array('rule' => 'notEmpty','message'=>'選択してください。'),
		'area_banner_url'=>array(
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'url','message'=>'URLを入力してください。'),
		),
	);

	//最後の文字のチェック
	function lastStr($data, $value){
		if(mb_substr($data['url'],strlen($data['url'])-1,1) == $value){
			return true;
		}else{
			return false;
		}
	}

	public static function getDisplayGradeOptions() {
		return array(
			self::DISPLAY_GRADE_FREE => '無料パターン',
			self::DISPLAY_GRADE_MIDDLE => '中間パターン',
			self::DISPLAY_GRADE_PAY => '有料パターン',
		);
	}
}
?>