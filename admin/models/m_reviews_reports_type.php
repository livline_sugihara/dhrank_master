<?php

class MReviewsReportsType extends AppModel {
	public $name = 'MReviewsReportsType';
	public $useTable = 'm_reviews_reports_types';

	public $validate = array(
		'name'=>array(
			array('rule' => 'isUnique','message'=>'既に登録されているログインIDです。'), // ユニークチェック
			array('rule2' => 'notEmpty','message'=>'ログインIDを記入してください。'), // 空チェック
		),
		'reviews_count'=>array(
			array('rule' => array('naturalNumber', true), 'message'=>'口コミ最低必要数は0以上を入力してください。'),  // trueで0以上の整数
			array('rule2' => 'notEmpty','message'=>'。'), // 空チェック
		),
		'reviews_good_count'=>array(
			array('rule' => array('naturalNumber', true), 'message'=>'口コミ最低必要数は0以上を入力してください。'),  // trueで0以上の整数
			array('rule2' => 'notEmpty','message'=>'。'), // 空チェック
		),
	);

}

?>
