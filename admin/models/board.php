<?php
class Board extends AppModel {
	var $name = 'Board';

	var $hasOne = array(
		'LargeArea' => array(
			'className' => 'LargeArea',
			'foreignKey' => '',
			'conditions' => array('Board.large_area_id = LargeArea.id'),
			'fields' => '',
			'order' => ''
		),
	);

	var $hasMany = array(
			'BoardRes' => array(
					'className' => 'BoardRes',
					'foreignKey' => '',
					'conditions' => '',
					'foreignKey' => 'board_id',
					'fields' => '',
					'order' => 'created asc'
			),
	);

}
?>