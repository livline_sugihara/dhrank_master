<?php
class Reviewer extends AppModel {
	var $name = 'Reviewer';

	var $belongsTo = array(
		'LargeArea' => array(
			'className' => 'LargeArea',
			'foreignKey' => '',
			'conditions' => array('Reviewer.large_area_id = LargeArea.id'),
			'fields' => '',
			'order' => ''
		),
		'MReviewersAge' => array(
			'className' => 'MReviewersAge',
			'foreignKey' => '',
			'conditions' => array('Reviewer.age_id = MReviewersAge.id'),
			'fields' => '',
			'order' => ''
		),
		'MReviewersJob' => array(
			'className' => 'MReviewersJob',
			'foreignKey' => '',
			'conditions' => array('Reviewer.job_id = MReviewersJob.id'),
			'fields' => '',
			'order' => ''
		),
		'MShopsBusinessCategory' => array(
			'className' => 'MShopsBusinessCategory',
			'foreignKey' => '',
			'conditions' => array('Reviewer.favorite_category_id = MShopsBusinessCategory.id'),
			'fields' => '',
			'order' => ''
		),
	);

}
?>