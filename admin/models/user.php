<?php

class User extends AppModel {
	public $name = 'User';

	public $belongsTo = array(
		"Shop" => array(
			'className' => 'Shop',
			'conditions' => 'User.id = Shop.user_id',
			'order' => '',
			'foreignKey' => ''
		),
		'UsersAuthority' => array(
			'className' => 'UsersAuthority',
			'conditions' => 'User.id = UsersAuthority.user_id',
			'order' => '',
			'foreignKey' => ''
		),
		"LargeArea" => array(
			'className' => 'LargeArea',
			'conditions' => 'UsersAuthority.large_area_id = LargeArea.id',
			'order' => '',
			'foreignKey' => ''
		),
	);

	public $validate = array(
		'username'=>array(
			array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			array('rule' => 'isUnique','message'=>'既に登録されているログインIDです。'),
			array('rule' => 'notEmpty','message'=>'ログインIDを記入してください。'),
		),
		'password'=>array(
			array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			array('rule' => 'notEmpty','message'=>'パスワードを記入してください。'),
		),
		'kana'=>array(
			array('rule' => array('custom',VALID_HIRAGANA),'message'=>'全角ひらがなで入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'かなを記入してください。'),
		),
		'large_area_id'=>array('rule' => 'notEmpty','message'=>'選択してください。'),
		'old_password'=>array(
			array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			array('rule' => 'equalToPasswordOld','message'=>'旧パスワードが正しくありません。'),
			array('rule' => 'notEmpty','message'=>'旧パスワードを記入してください。'),
		),
		'new_password'=>array(
			array('rule' => 'equalToPasswordNew','message'=>'新しいパスワードが一致しません。'),
			array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			array('rule' => 'notEmpty','message'=>'新しいパスワードを記入してください。'),
		),
		'new_password_confirm'=>array(
			array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			array('rule' => 'notEmpty','message'=>'新しいパスワード(確認)を記入してください。')
		)
	);

	function equalToPasswordOld($data){
		return ($this->data['User']['password'] == $this->data['User']['old_password']);
	}
	function equalToPasswordNew($data){
		return ($this->data['User']['new_password'] == $this->data['User']['new_password_confirm']);
	}
	function reviewDisplayAndAccept($data){
		if($this->data['User']['is_review_display'] == 0 && $this->data['User']['review_accept'] != 0){
			return false;
		}else{
			return true;
		}
	}
}

?>