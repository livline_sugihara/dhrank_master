<?php
class Review extends AppModel {
	var $name = 'Review';

	public static $appointed_type = array(
		1 => '新規',
		2 => 'リピート',
	);

	public static $appointed_sub = array(
		1 => '本指名',
		2 => '写真指名',
		3 => 'フリー',
		4 => 'おすすめ',
	);

	public static $score = array(
		1 => '★☆☆☆☆ 1点',
		2 => '★★☆☆☆ 2点',
		3 => '★★★☆☆ 3点',
		4 => '★★★★☆ 4点',
		5 => '★★★★★ 5点',
	);

	/** 削除フラグ(削除なし) */
	const DELETE_FLAG_NONE = 0;
	/** 削除フラグ(コメントのみ削除) */
	const DELETE_FLAG_COMMENT_ONLY = 1;
	/** 削除フラグ(依頼により削除されました) */
	const DELETE_FLAG_SAVE_POINTS = 2;
	/** 削除フラグ(全削除) */
	const DELETE_FLAG_ALL = 3;

	var $hasOne = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => '',
			'conditions' => array('User.id = Review.user_id'),
			'fields' => '',
			'order' => ''
		),
		'LargeArea' => array(
			'className' => 'LargeArea',
			'foreignKey' => '',
			'conditions' => array('User.large_area_id = LargeArea.id'),
			'fields' => '',
			'order' => ''
		),
		'Shop' => array(
			'className' => 'Shop',
			'foreignKey' => '',
			'conditions' => array('Shop.user_id = User.id'),
			'fields' => '',
			'order' => ''
		),
		'VetoIpAddressReview' => array(
			'className' => 'VetoIpAddressReview',
			'foreignKey' => '',
			'conditions' => array('VetoIpAddressReview.name = Review.ip_address'),
			'fields' => '',
			'order' => ''
		),
		'VetoUidReview' => array(
			'className' => 'VetoUidReview',
			'foreignKey' => '',
			'conditions' => array('VetoUidReview.name = Review.uid'),
			'fields' => '',
			'order' => ''
		),
		'Reviewer' => array(
			'className' => 'Reviewer',
			'foreignKey' => '',
			'conditions' => array('Reviewer.id = Review.reviewer_id'),
			'fields' => '',
			'order' => ''
		),
		'Girl' => array(
			'className' => 'Girl',
			'foreignKey' => '',
			'conditions' => array('Girl.id = Review.girl_id'),
			'fields' => '',
			'order' => ''
		),
		'ReviewsReport' => array(
			'className' => 'ReviewsReport',
			'foreignKey' => '',
			'conditions' => array('ReviewsReport.report_review_id = Review.id'),
			'fields' => '',
			'order' => ''
		),
		'MReviewsReportsType' => array(
			'className' => 'MReviewsReportsType',
			'foreignKey' => '',
			'conditions' => array('MReviewsReportsType.id = ReviewsReport.report_type_id'),
			'fields' => '',
			'order' => ''
		),

	);

	public $validate = array(
			'created'=>array(
					array('rule' => 'datetime','message'=>'正しい日付を入力してください。'),
			),
	);

	//datetimeチェック関数
	public function datetime($check) {
		list ($key, $datetime) = each($check);
		if (is_null($datetime)) {
			return true;
		}
		// format check (Y-m-d H:i:s)
		$regex = '/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}:(\d{2}))$/';
		if (!preg_match($regex, $datetime)) {
			return false;
		}
		$list = explode(' ', $datetime);
		$date = explode('-', $list[0]);
		// date check (Y-m-d)
		if (!checkdate($date[1], $date[2], $date[0])) {
			return false;
		}
		$time = explode(':', $list[1]);
		// time check (H:i:s)
		if ($time[0] < 0 || $time[0] >=24 || $time[1] < 0 || $time[1] >=60 || $time[2] < 0 || $time[2] >=60) {
			return false;
		}
		return true;
	}
}
?>
