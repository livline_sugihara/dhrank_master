<?php
class BoardRes extends AppModel {
	var $name = 'BoardRes';

	var $hasOne = array(
		'VetoIpAddressBoard' => array(
			'className' => 'VetoIpAddressBoard',
			'foreignKey' => '',
			'conditions' => array('VetoIpAddressBoard.name = BoardRes.ip_address'),
			'fields' => '',
			'order' => ''
		),
		'VetoUidBoard' => array(
			'className' => 'VetoUidBoard',
			'foreignKey' => '',
			'conditions' => array('VetoUidBoard.name = BoardRes.uid'),
			'fields' => '',
			'order' => ''
		),
	);

}
?>