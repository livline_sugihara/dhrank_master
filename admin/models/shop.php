<?php

class Shop extends AppModel {
	public $name = 'Shop';

	public $validate = array(
		'name'=>array(
			array('rule' => 'notEmpty','message'=>'店舗名称を記入してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。')
		),
		'phone_number'=>array(
			array('rule' => 'notEmpty','message'=>'電話番号を記入してください。'),
			array('rule' => array('custom',VALID_PHONE),'message'=>'電話番号を正確に入力してください。'),
			array('rule' => array('minLength',10),'message'=>'10文字以上で入力してください。'),
			array('rule' => array('maxLength',15),'message'=>'15文字以下で入力してください。')
		),
		'email'=>array(
			array('rule' => 'notEmpty','message'=>'メールアドレスを記入してください。'),
			array('rule' => array('email',false, VALID_EMAIL),'message'=>'メールアドレスを入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'isUnique','message'=>'既に登録されているメールアドレスです。'),
		),
	);

}

?>