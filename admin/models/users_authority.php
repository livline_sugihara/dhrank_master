<?php
class UsersAuthority extends AppModel {
	public $name = 'UsersAuthority';

	public $belongsTo = array(
		"LargeArea" => array(
			'className' => 'LargeArea',
			'conditions' => '',
			'order' => '',
			'foreignKey' => 'large_area_id'
		),
		"Shop" => array(
			'className' => 'Shop',
			'conditions' => 'UsersAuthority.user_id = Shop.user_id',
			'order' => '',
			'foreignKey' => ''
		)
	);

	public $validate = array(
		'is_official_link_display' => array('rule' => 'notEmpty','message'=>'選択してください。'),
		'is_recommend_girl' => array('rule' => 'notEmpty','message'=>'選択してください。'),
		'is_shop_header_image' => array('rule' => 'notEmpty','message'=>'選択してください。'),
		'is_show_new_shop' => array('rule' => 'notEmpty','message'=>'選択してください。'),
		'is_show_recommend_shop' => array('rule' => 'notEmpty','message'=>'選択してください。'),
		'is_show_pickup_shop' => array('rule' => 'notEmpty','message'=>'選択してください。'),
		'is_show_all_contents_by_categorylist' => array('rule' => 'notEmpty','message'=>'選択してください。'),
		'is_show_all_contents_by_shop' => array('rule' => 'notEmpty','message'=>'選択してください。'),
		'is_pickup_review' => array('rule' => 'notEmpty','message'=>'選択してください。'),
		'is_delete_review' => array('rule' => 'notEmpty','message'=>'選択してください。'),
		'order_rank' => array('rule' => 'notEmpty','message'=>'選択してください。'),
	);
}
?>