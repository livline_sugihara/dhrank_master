<?php

class ReviewsReport extends AppModel {
	public $name = 'ReviewReports';
	public $useTable = 'reviews_reports';

	public $hasOne = array(
        'Review' => array(
			'className' => 'Review',
			'foreignKey' => '',
			'conditions' => array('ReviewsReport.report_review_id = Review.id'),
			'fields' => '',
			'order' => ''
        ),
		'MReviewsReportsType' => array(
			'className' => 'MReviewsReportsType',
			'foreignKey' => '',
			'conditions' => array('ReviewsReport.report_type_id = MReviewsReportsType.id'),
			'fields' => '',
			'order' => ''
		)
    );

}
?>
