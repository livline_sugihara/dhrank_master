<?php

class AllareaLink extends AppModel {
	public $name = 'AllareaLink';

	public $validate = array(
			'name'=>array(
					array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
					array('rule' => 'notEmpty','message'=>'サイト名を記入してください。'),
			),
			'url'=>array(
					array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
					array('rule' => 'url','message'=>'URLを入力してください。'),
					array('rule' => 'notEmpty','message'=>'URLを入力してください。'),
			),
			'banner_add'=>array(
					array('rule' => array('isNotOverFileSize','banner_add',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
					array('rule' => array('isExtentionJpegOrGif','banner_add'),'message'=>'Jpeg画像またはGif画像を選択してください。'),
					array('rule' => array('imageNotEmpty', 'banner_add'),'message' => 'バナー画像を選択してください。'),
			),
			'banner_edit'=>array(
					array('rule' => array('isNotOverFileSize','banner_edit',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
					array('rule' => array('isExtentionJpegOrGif','banner_edit'),'message'=>'Jpeg画像またはGif画像を選択してください。'),
			),
	);

}

?>