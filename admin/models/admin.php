<?php

class Admin extends AppModel {
	public $name = 'Admin';

	var $hasMany = array(
			'AdminsAuthLargeArea' => array(
					'className' => 'AdminsAuthLargeArea',
					'foreignKey' => '',
					'conditions' => '',
					'foreignKey' => 'admin_id',
					'fields' => '',
					'order' => 'large_area_id asc'
			),
	);

	public $validate = array(
		'username'=>array(
			array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			array('rule' => 'isUnique','message'=>'既に登録されているログインIDです。'),
			array('rule' => 'notEmpty','message'=>'ログインIDを記入してください。'),
		),
		'password_confirm'=>array(
			array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			array('rule' => 'notEmpty','message'=>'パスワードを記入してください。'),
		),
		'old_password'=>array(
			array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			array('rule' => 'equalToPasswordOld','message'=>'旧パスワードが正しくありません。'),
			array('rule' => 'notEmpty','message'=>'旧パスワードを記入してください。'),
		),
		'new_password'=>array(
			array('rule' => 'equalToPasswordNew','message'=>'新しいパスワードが一致しません。'),
			array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			array('rule' => 'notEmpty','message'=>'新しいパスワードを記入してください。'),
		),
		'new_password_confirm'=>array(
			array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			array('rule' => 'notEmpty','message'=>'新しいパスワード(確認)を記入してください。')
		),
	);

	function equalToPasswordOld($data){
		return ($this->data['Admin']['password'] == $this->data['Admin']['old_password']);
	}
	function equalToPasswordNew($data){
		return ($this->data['Admin']['new_password'] == $this->data['Admin']['new_password_confirm']);
	}

}

?>