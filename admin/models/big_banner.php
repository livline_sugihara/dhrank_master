<?php
class BigBanner extends AppModel {
	public $name = 'BigBanner';

	public $belongsTo = array(
		"LargeArea" => array(
			'className' => 'LargeArea',
			'conditions' => '',
			'order' => '',
			'foreignKey' => 'large_area_id'
		),
	);

	public $validate = array(
		'large_area_id'=>array('rule' => 'notEmpty','message'=>'選択してください。'),
		'shop_filename'=>array('rule' => 'notEmpty','message'=>'ファイルを選択して下さい'),
		'girl_filename'=>array('rule' => 'notEmpty','message'=>'ファイルを選択して下さい'),
		'shop_link_url'=>array(
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'url','message'=>'URLを入力してください。'),
		),
		'girl_link_url'=>array(
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'url','message'=>'URLを入力してください。'),
		),
	);
}
?>