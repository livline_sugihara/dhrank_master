<?php
//画像論理パス
define('APP_IMG_URL','http://img.dhrank.com/');
//店舗管理システム
define('APP_MEMBER_URL','http://member.dhrank.com/');

Configure::write('presence', array(
null => '',
0 => '無',
1 => '有',
));

Configure::write('link_order', array(
null => '',
1 => '固定',
2 => 'ランダム',
));

Configure::write('default_enable', array(
0 => '不可',
1 => '可',
));

Configure::write('weekday', array( "日", "月", "火", "水", "木", "金", "土" ));

Configure::write('kana', array(
1 => array('あ','い','う','え','お','ぁ','ぃ','ぅ','ぇ','ぉ',),
2 => array('か','き','く','け','こ','が','ぎ','ぐ','げ','ご',),
3 => array('さ','し','す','せ','そ','ざ','じ','ず','ぜ','ぞ',),
4 => array('た','ち','つ','て','と','だ','ぢ','づ','で','ど','っ',),
5 => array('な','に','ぬ','ね','の',),
6 => array('は','ひ','ふ','へ','ほ','ば','び','ぶ','べ','ぼ','ぱ','ぴ','ぷ','ぺ','ぽ',),
7 => array('ま','み','む','め','も',),
8 => array('や','ゆ','よ','ゃ','ゅ','ょ',),
9 => array('ら','り','る','れ','ろ',),
10 => array('わ','を','ん','ゎ',),
));

// 正の整数
define('VALID_POSITIVE_NUMBER', '/^[0-9]+$/');
// 半角英数記号
define('VALID_HALF_ALPHANUMERICCODE', '/^[!-~]+$/');
// 全角ひらがな
define('VALID_HIRAGANA','/^(?:\xE3\x81[\x81-\xBF]|\xE3\x82[\x80-\x93])+$/');
// 電話番号
//start---2012/4/6障害No.3-0012修正
//define('VALID_PHONE','/0\d{1,4}-\d{1,4}-\d{4}/');
define('VALID_PHONE','/0\d{1,4}-\d{1,4}-\d{3,4}/');
//end---2012/4/6障害No.3-0012修正
// メールアドレス
define('VALID_EMAIL','/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/');



//画像定義
//店舗情報ヘッダ（PC）
define('APP_IMG_HEADER_PC_WIDTH',840);
define('APP_IMG_HEADER_PC_HEIGHT',160);
//店舗情報ヘッダ（携帯）
define('APP_IMG_HEADER_MOBILE_WIDTH',200);
define('APP_IMG_HEADER_MOBILE_HEIGHT',40);
//ランキング画像
define('APP_IMG_RANK_WIDTH',120);
define('APP_IMG_RANK_HEIGHT',160);
//求人画像（PC）
define('APP_IMG_JOB_PC_WIDTH',720);
define('APP_IMG_JOB_PC_HEIGHT',160);
//求人画像（携帯）
define('APP_IMG_JOB_MOBILE_WIDTH',240);
define('APP_IMG_JOB_MOBILE_HEIGHT',120);
//ミニバナー（PC）
define('APP_IMG_MINI_BANNER_PC_WIDTH',200);
define('APP_IMG_MINI_BANNER_PC_HEIGHT',40);
//ミニバナー（携帯）
define('APP_IMG_MINI_BANNER_MOBILE_WIDTH',200);
define('APP_IMG_MINI_BANNER_MOBILE_HEIGHT',40);
//ニュース画像L
define('APP_IMG_NEWS_L_WIDTH',240);
define('APP_IMG_NEWS_L_HEIGHT',320);
//ニュース画像M
define('APP_IMG_NEWS_M_WIDTH',120);
define('APP_IMG_NEWS_M_HEIGHT',160);
//ニュース画像S
define('APP_IMG_NEWS_S_WIDTH',60);
define('APP_IMG_NEWS_S_HEIGHT',80);
//求人速報画像L
define('APP_IMG_JOBSQUICKS_L_WIDTH',240);
define('APP_IMG_JOBSQUICKS_L_HEIGHT',320);
//求人速報画像M
define('APP_IMG_JOBSQUICKS_M_WIDTH',120);
define('APP_IMG_JOBSQUICKS_M_HEIGHT',160);
//求人速報画像S
define('APP_IMG_JOBSQUICKS_S_WIDTH',60);
define('APP_IMG_JOBSQUICKS_S_HEIGHT',80);
//女の子画像L
define('APP_IMG_GIRL_L_WIDTH',240);
define('APP_IMG_GIRL_L_HEIGHT',320);
//女の子画像M
define('APP_IMG_GIRL_M_WIDTH',120);
define('APP_IMG_GIRL_M_HEIGHT',160);
//女の子画像S
define('APP_IMG_GIRL_S_WIDTH',60);
define('APP_IMG_GIRL_S_HEIGHT',80);
//写メール画像L
define('APP_IMG_BBS_L_WIDTH',240);
define('APP_IMG_BBS_L_HEIGHT',320);
//写メール画像M
define('APP_IMG_BBS_M_WIDTH',120);
define('APP_IMG_BBS_M_HEIGHT',160);
//写メール画像S
define('APP_IMG_BBS_S_WIDTH',60);
define('APP_IMG_BBS_S_HEIGHT',80);


//ポイント
//ニュース更新
define('APP_POINT_NEWS',5);
//求人速報更新
define('APP_POINT_JOBSQUICKS',5);
//割引情報更新
define('APP_POINT_DISCOUNTS',5);
//写メールBBS投稿
define('APP_POINT_BBS',3);
//写メールBBS投稿（メールから）
define('APP_POINT_BBS_MAIL',3);
//店舗系画面
define('APP_POINT_SHOPS',1);
//Webサイトクリック時
define('APP_POINT_WEB_SITES',1);

//テキスト抜粋
//トップ：店舗ニュース速報
define('APP_CSIZE_TOP_NEWS',100);
define('APP_CSIZE_TOP_NEWS_I',100);
define('APP_CSIZE_TOP_NEWS_S',100);
//トップ：求人ニュース速報
define('APP_CSIZE_TOP_JOBSQUICKS',100);
define('APP_CSIZE_TOP_JOBSQUICKS_I',100);
define('APP_CSIZE_TOP_JOBSQUICKS_S',100);
//トップ：女の子レビュー
define('APP_CSIZE_TOP_REVIEWS',30);
define('APP_CSIZE_TOP_REVIEWS_I',30);
define('APP_CSIZE_TOP_REVIEWS_S',30);
//即ヒメ：女の子コメント
define('APP_CSIZE_QUICKS_COMMENT',54);
//店舗情報：店舗ニュース速報
define('APP_CSIZE_SHOP_NEWS',100);
define('APP_CSIZE_SHOP_NEWS_I',100);
define('APP_CSIZE_SHOP_NEWS_S',100);
//店舗情報：求人ニュース速報
define('APP_CSIZE_SHOP_JOBSQUICKS',100);
define('APP_CSIZE_SHOP_JOBSQUICKS_I',100);
define('APP_CSIZE_SHOP_JOBSQUICKS_S',100);
//start---ver1.1
//県内バナー（店名）
define('APP_CSIZE_TOP_BANNER',30);
//end---ver1.1


//店舗登録依頼送信先
define('VALID_SHOP_REQUESTS_EMAIL','request@delih-f.com');
//define('VALID_SHOP_REQUESTS_EMAIL','info@admin.web-pulse.info');

Configure::write('smtpOptionShopRequest', array(
'port'=>'587',
'host' => 'delih-f.com',
'username'=>'request@delih-f.com',
'password'=>'F55KXt93',
));
/*
 Configure::write('smtpOptionShopRequest', array(
 		'port'=>'587',
 		'host' => 'admin.web-pulse.info',
 		'username'=>'info@admin.web-pulse.info',
 		'password'=>'admin.web',
 ));
*/

//求人メッセージ送信
define('VALID_JOBSMESSAGES_EMAIL','recruit@delih-f.com');
//define('VALID_JOBSMESSAGES_EMAIL','info@admin.web-pulse.info');

Configure::write('smtpOptionJobsmessage', array(
'port'=>'587',
'host' => 'delih-f.com',
'username'=>'recruit@delih-f.com',
'password'=>'H5AvJdNq',
));
/*
 Configure::write('smtpOptionJobsmessage', array(
 		'port'=>'587',
 		'host' => 'admin.web-pulse.info',
 		'username'=>'info@admin.web-pulse.info',
 		'password'=>'admin.web',
 ));
*/

//求人メッセージ送信2(店舗)
define('VALID_JOBSMESSAGES2_EMAIL','recruit@delih-f.com');
//define('VALID_JOBSMESSAGES2_EMAIL','info@admin.web-pulse.info');

Configure::write('smtpOptionJobsmessage2', array(
'port'=>'587',
'host' => 'delih-f.com',
'username'=>'recruit@delih-f.com',
'password'=>'H5AvJdNq',
));
/*
 Configure::write('smtpOptionJobsmessage2', array(
 		'port'=>'587',
 		'host' => 'admin.web-pulse.info',
 		'username'=>'info@admin.web-pulse.info',
 		'password'=>'admin.web',
 ));
*/

//パスワード変更URL送信
define('VALID_CHANGEPASSWORD_URLSEND_EMAIL','reminder@delih-f.com');
//define('VALID_CHANGEPASSWORD_URLSEND_EMAIL','info@admin.web-pulse.info');

Configure::write('smtpOptionChangepasswordUrlsend', array(
'port'=>'587',
'host' => 'delih-f.com',
'username'=>'reminder@delih-f.com',
'password'=>'lCO4EiGy',
));
/*
 Configure::write('smtpOptionChangepasswordUrlsend', array(
 		'port'=>'587',
 		'host' => 'admin.web-pulse.info',
 		'username'=>'info@admin.web-pulse.info',
 		'password'=>'admin.web',
 ));
*/

//パスワード変更完了送信
define('VALID_CHANGEPASSWORD_END_EMAIL','reminder@delih-f.com');
//define('VALID_CHANGEPASSWORD_END_EMAIL','info@admin.web-pulse.info');

Configure::write('smtpOptionChangepasswordEnd', array(
'port'=>'587',
'host' => 'delih-f.com',
'username'=>'reminder@delih-f.com',
'password'=>'lCO4EiGy',
));
/*
 Configure::write('smtpOptionChangepasswordEnd', array(
 		'port'=>'587',
 		'host' => 'admin.web-pulse.info',
 		'username'=>'info@admin.web-pulse.info',
 		'password'=>'admin.web',
 ));
*/
//start---2012/3/27障害No.1-3-0002修正
define('VALID_GIRLS_IMAGE_UPLOAD_EMAIL','picture@delih-f.com');
//define('VALID_GIRLS_IMAGE_UPLOAD_EMAIL','imageupload@admin.web-pulse.info');
//end---2012/3/27障害No.1-3-0002修正


?>