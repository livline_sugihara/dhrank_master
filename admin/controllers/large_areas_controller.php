<?php
class LargeAreasController extends AppController {
	var $name = 'LargeAreas';
	var $uses = array('LargeArea','User','Board','BoardRes','AdminsAuthLargeArea','Reviewer','ReviewerChangeemail','ReviewerChangepassword','ReviewerPassport','ReviewerRegister','Access','AreaIndexLink');
	var $components = array('CompFile');

	function beforeFilter() {
		parent::beforeFilter();
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_large_areas']){
			$this->cakeError('error404');
		}
	}


	function index(){
		$this->set('list', $this->LargeArea->find('all',array('order' => 'LargeArea.show_order ASC')));
		//登録済みのユーザがあるかどうか
		$count_user = $this->User->find('all', array('fields' => array('count(User.id) as count', 'large_area_id'),'group' => 'large_area_id'));
		$count_user = Set::combine($count_user, '{n}.User.large_area_id', '{n}.0.count');
		$this->set('count_user', $count_user);
		// $this->set('link_order', Configure::read('link_order'));
		// $this->set('presence', Configure::read('presence'));
	}

	// 登録画面
	function add() {
		//登録ボタン押下時
		if (!empty($this->data)){
			$this->LargeArea->set($this->data);
			if($this->LargeArea->validates()){
				//大エリア作成
				$this->data['LargeArea']['show_order'] = 10000;
				$this->LargeArea->save($this->data['LargeArea']);

				//画像処理
				if(!empty($this->data['LargeArea']['area_banner_name_add']['tmp_name'])){

					// FIXME デバッグ
					$this->log('ファイルアップします。 [' . $this->data['LargeArea']['area_banner_name_add']['tmp_name'] . ']', 'debug');

					//画像アップロード(1.ファイル名(extなし),2.tmp_name,3.path)
					$upload_name = $this->CompFile->uploadImage(
							$this->data['LargeArea']['id'],
							$this->data['LargeArea']['area_banner_name_add']['tmp_name'],
							'banner' . DS . 'area_banner'
					);
					if(!empty($upload_name)){

						// FIXME デバッグ
						$this->log('ファイルアップ正常に完了しました [' . $upload_name . ']', 'debug');

						$this->data_update['LargeArea']['area_banner_name'] = $upload_name;
						$this->data_update['LargeArea']['id'] = $this->data['LargeArea']['id'];
						$this->LargeArea->save($this->data_update);
					}
				}

				//掲示板作成（新人）
				// $this->Board->create();
				// $this->Board->large_area_id = $this->LargeArea->id;
				// $this->Board->title = '新人さん入店情報スレッド';
				// $this->Board->show_order = 1;
				// $this->Board->save($this->Board);
				// $this->BoardRes->create();
				// $this->BoardRes->board_id = $this->Board->id;
				// $this->BoardRes->sub_id = 1;
				// $this->BoardRes->name = '名無しさん＠お腹いっぱい';
				// $this->BoardRes->comment = '掲載されていない新人の女の子を見つけたらこちらに書き込んでください。';
				// $this->BoardRes->save($this->BoardRes);

				//掲示板作成（新店）
				// $this->Board->create();
				// $this->Board->large_area_id = $this->LargeArea->id;
				// $this->Board->title = '新店情報スレッド';
				// $this->Board->show_order = 2;
				// $this->Board->save($this->Board);
				// $this->BoardRes->create();
				// $this->BoardRes->board_id = $this->Board->id;
				// $this->BoardRes->sub_id = 1;
				// $this->BoardRes->name = '名無しさん＠お腹いっぱい';
				// $this->BoardRes->comment = '掲載されていない新店を見つけたらこちらに書き込んでください。';
				// $this->BoardRes->save($this->BoardRes);

				//掲示板作成（削除依頼）
				// $this->Board->create();
				// $this->Board->large_area_id = $this->LargeArea->id;
				// $this->Board->title = '削除依頼';
				// $this->Board->show_order = -1;
				// $this->Board->save($this->Board);
				// $this->BoardRes->create();
				// $this->BoardRes->board_id = $this->Board->id;
				// $this->BoardRes->sub_id = 1;
				// $this->BoardRes->name = '管理人★';
				// $this->BoardRes->comment = '炎上掲示板の削除依頼はこちらにお願いします。
				// 		必ず・スレッド名・記事番号をお書きください。
				// 		●削除依頼スレに書いて良い事
				// 		　・依頼者のレス
				// 		　・削除人による削除判断前の、削除依頼に対する反論
				// 		　・依頼者にとって有用なアドバイス・誘導
				// 		　・削除人による削除判断
				// 		●削除依頼スレにおける禁止事項
				// 		　・削除人でない者による削除判断、または依頼者を混乱させる発言
				// 		　・削除されたものに対する議論
				// 		　・その他、依頼者・削除人に対する根拠のない誹謗中傷など
				// 		　・削除処理を催促する発言
				// 		※禁止事項に該当する書き込みは削除妨害と判断され 規制が行われる可能性があります。
				// 		※書込みの削除は、管理人の判断によります。';
				// $this->BoardRes->save($this->BoardRes);

				$this->redirect('/large_areas/addend');
			}
		}
		// $this->set('link_order', Configure::read('link_order'));
		// $this->set('presence', Configure::read('presence'));
		$this->set('display_grade', LargeArea::getDisplayGradeOptions());
	}

	// 登録完了画面
	function addend(){
	}

	// 編集画面
	function edit($param){
		if (empty($this->data)){
			$this->LargeArea->id = $param;
			$this->data = $this->LargeArea->read();
		}else{
			$this->LargeArea->set($this->data);
			if($this->LargeArea->validates()){
				$this->LargeArea->save($this->data);

				//画像処理
				if(!empty($this->data['LargeArea']['area_banner_name_edit']['tmp_name'])){

					// FIXME デバッグ
					$this->log('ファイルアップします。 [' . $this->data['LargeArea']['area_banner_name_edit']['tmp_name'] . ']', 'debug');

					//現在の画像を削除
					$this->CompFile->deleteFile('banner' . DS . 'area_banner' . DS . $this->data['LargeArea']['area_banner_name']);

					//画像アップロード(1.ファイル名(extなし),2.tmp_name,3.path)
					$upload_name = $this->CompFile->uploadImage(
							$this->data['LargeArea']['id'],
							$this->data['LargeArea']['area_banner_name_edit']['tmp_name'],
							'banner' . DS . 'area_banner'
					);
					if(!empty($upload_name)){

						// FIXME デバッグ
						$this->log('ファイルアップ正常に完了しました [' . $upload_name . ']', 'debug');

						$this->data_update['LargeArea']['area_banner_name'] = $upload_name;
						$this->data_update['LargeArea']['id'] = $this->data['LargeArea']['id'];
						$this->LargeArea->save($this->data_update);
					}
				}
				$this->redirect('/large_areas/editend');
			}
		}
		// $this->set('link_order', Configure::read('link_order'));
		// $this->set('presence', Configure::read('presence'));
		$this->set('display_grade', LargeArea::getDisplayGradeOptions());
		$this->set('data',$this->data);
	}

	// 編集完了画面
	function editend(){
	}

	// 削除
	function delete($param){
		if (!empty($this->data)){
			//大エリア削除
			$this->LargeArea->delete($this->data['LargeArea']['id']);

			// //掲示板データ取得
			// $boards = $this->Board->find('all',array('fields' => 'id','conditions' => array('Board.large_area_id' => $this->data['LargeArea']['id'])));
			// //掲示板レス削除
			// foreach($boards as $board){
			// 	$this->BoardRes->deleteAll(array('BoardRes.board_id' => $board['Board']['id']));
			// }
			// //掲示板削除
			// $this->Board->deleteAll(array('Board.large_area_id' => $this->data['LargeArea']['id']));

			//権限削除
			$this->AdminsAuthLargeArea->deleteAll(array('AdminsAuthLargeArea.large_area_id' => $this->data['LargeArea']['id']));

			// //レビュアーデータ取得
			// $reviewers = $this->Reviewer->find('all',array('fields' => 'id','conditions' => array('Reviewer.large_area_id' => $this->data['LargeArea']['id'])));
			// foreach($reviewers as $reviewer){
			// 	//メールアドレス変更ログ
			// 	$this->ReviewerChangeemail->deleteAll(array('ReviewerChangeemail.reviewer_id' => $reviewer['Reviewer']['id']));
			// 	//パスワード変更ログ
			// 	$this->ReviewerChangepassword->deleteAll(array('ReviewerChangepassword.reviewer_id' => $reviewer['Reviewer']['id']));
			// 	//パスポート
			// 	$this->ReviewerPassport->deleteAll(array('ReviewerPassport.reviewer_id' => $reviewer['Reviewer']['id']));
			// }
			// //レビュアー
			// $this->Reviewer->deleteAll(array('Reviewer.large_area_id' => $this->data['LargeArea']['id']));
			// //レビュアー会員登録ログ
			// $this->ReviewerRegister->deleteAll(array('ReviewerRegister.large_area_id' => $this->data['LargeArea']['id']));

			//アクセスデータ
			$this->Access->deleteAll(array('Access.large_area_id' => $this->data['LargeArea']['id']));

			//リンク集データ取得
			//start---2013/3/11 障害No.3-0011修正
			//$area_index_links = $this->AreaIndexLink->find('all',array('fields' => 'id','conditions' => array('AreaIndexLink.large_area_id' => $this->data['LargeArea']['id'])));
			// $area_index_links = $this->AreaIndexLink->find('all',array('conditions' => array('AreaIndexLink.large_area_id' => $this->data['LargeArea']['id'])));
			// //end---2013/3/11 障害No.3-0011修正
			// foreach($area_index_links as $area_index_link){
			// 	//リンク集画像削除
			// 	$this->CompFile->deleteFile('link' . DS . 'area_index' . DS . $area_index_link['AreaIndexLink']['banner']);
			// }
			// //リンク集
			// $this->AreaIndexLink->deleteAll(array('AreaIndexLink.large_area_id' => $this->data['LargeArea']['id']));

			$this->redirect('/large_areas/deleteend');
		} else {
			$this->LargeArea->id = $param;
			$this->data = $this->LargeArea->read();
			// $this->set('link_order', Configure::read('link_order'));
			// $this->set('presence', Configure::read('presence'));
			$this->set('data',$this->data);
		}
	}
	// 削除完了画面
	function deleteend(){
	}

	// //表示順位編集
	// function change_link_order() {

	// 	if (!empty($this->data)){
	// 		foreach ($this->data['LargeArea'] as $data) {
	// 			if(empty($data['show_order'])){
	// 				$data['show_order'] = 10000;
	// 			}
	// 			$this->LargeArea->save($data);
	// 		}
	// 		//start---2013/2/22 障害No.2-0001修正
	// 		//$this->redirect($this->device_path . '/large_areas/change_link_order_end');
	// 		$this->redirect('/large_areas/change_link_order_end');
	// 		//end---2013/2/22 障害No.2-0001修正

	// 	}else{
	// 		$this->set('list', $this->LargeArea->find('all', array(
	// 				'conditions' => array('LargeArea.is_display_footer' => 1),
	// 				'order' => 'LargeArea.show_order ASC, LargeArea.created ASC')));
	// 		$show_order = array();
	// 		for($i=0;$i<$this->LargeArea->getNumRows();$i++){
	// 			$show_order[$i+1] = $i+1;
	// 		}
	// 		$this->set('show_order', $show_order);
	// 	}
	// }
	// function change_link_order_end(){
	// }
}
?>