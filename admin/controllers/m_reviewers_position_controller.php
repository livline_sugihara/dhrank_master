<?php
class MReviewersPositionController extends AppController {
//	var $name = 'MReviewersPosition';

	// 役職管理一覧画面
	function index()
	{
		// 役職テーブルマスタからデータを取得
		$data = $this->MReviewersPosition->find('all',
			array(
				'fields' => '*',
			)
		);
		$this->set('data', $data);
	}

	// 役職管理変更画面
	function confirm($id)
	{
		if ($id == null || $id == '')
			$this->redirect('/m_reviewers_position');

			// 役職テーブルマスタからデータを取得
			$data = $this->MReviewersPosition->find('first',
				array(
					'fields' => '*',
					'conditions' => array('MReviewersPosition.id' => $id),
				)
			);
			$this->set('id', $id);
			$this->set('data', $data);
	}

	// 役職管理変更完了画面
	function finish()
	{

		// ＩＤがなければ、indexに遷移
		if (!isset($this->params['form']['id']) || $this->params['form']['id'] == null || $this->params['form']['id'] == '')
			$this->redirect('/m_reviewers_position');

		$errmsg = array();
		// エラーチェック
		// 役職名チェック
		if (!isset($this->params['form']['name']) || $this->params['form']['name'] == '')
			$errmsg['name'] = '役職名を入力してください。';
		elseif (strlen($this->params['form']['name']) > 255)
			$errmsg['name'] = '役職名は255文字以内で入力してください';

		// 口コミ最低必要数チェック
		if (!isset($this->params['form']['reviews_count']) || $this->params['form']['reviews_count'] == '')
			$errmsg['reviews_count'] = '口コミ最低必要数を入力してください。';
		elseif ($this->params['form']['reviews_count'] < 0)
			$errmsg['reviews_count'] = '口コミ最低必要数は０以上の値を入力してください。';
		elseif (!is_numeric($this->params['form']['reviews_count']))
			$errmsg['reviews_count'] = '口コミ最低必要数は数値を入力してください。';

		// エラーがあるか判定を行う。
		if (count($errmsg) > 0)
		{
			$this->set('form', $this->params['form']);
			$this->set('errmsg',$errmsg);
			$this->render('confirm');
		}
		else
		{
			$data = array('MReviewersPosition' => array(
				'id' => $this->params['form']['id'],
				'name' => $this->params['form']['name'],
				'reviews_count' => $this->params['form']['reviews_count'],
				)
			);
			$fields = array(
						'id',
						'name',
						'reviews_count',
			);

			// 正常の場合
			$this->MReviewersPosition->save($data, false, $fields);
		}
	}
}
?>
