<?php
class AreaIndexLinksController extends AppController {
	var $name = 'AreaIndexLinks';
	var $uses = array('AreaIndexLink','LargeArea');
	var $components = array('CompFile');

	function beforeFilter() {
		parent::beforeFilter();
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_area_links_edit'] && !$this->parent_admin['Admin']['is_auth_area_links_delete']){
			$this->cakeError('error404');
		}
	}

	function index(){
		//検索条件設定
		$conditions = array();
		if (!empty($this->data['LargeArea']['id'])){
			if ($this->data['LargeArea']['id'] == -1){
				array_push($conditions, array('AreaIndexLink.large_area_id' => $this->parent_admin_large_area_list));
			}else{
				array_push($conditions, array('AreaIndexLink.large_area_id' => $this->data['LargeArea']['id']));
			}
		}
		if(!empty($conditions)){
			$this->set('list', $this->AreaIndexLink->find('all', array(
					'conditions' => array($conditions),
					'order' => 'AreaIndexLink.show_order ASC, AreaIndexLink.created ASC',
			)));
		}else{
			$this->set('list', array());
		}
		//コンボボックスデータ取得
		$large_area = Set::Combine($this->LargeArea->find('all', array('conditions' => array('LargeArea.id' => $this->parent_admin_large_area_list))), '{n}.LargeArea.id', '{n}.LargeArea.name');
		$large_area[-1] = '全て';
		$this->set('large_area', $large_area);
	}

	// 登録画面
	function add() {
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_area_links_edit']){
			$this->cakeError('error404');
		}

		//start---2013/3/7 障害No.1-1-0012修正
		//大エリア登録がなければエラー
		if($this->LargeArea->find('count') == 0){
			$this->redirect('/area_index_links/error');
		}
		//end---2013/3/7 障害No.1-1-0012修正

		//登録ボタン押下時
		if (!empty($this->data)){
			$this->AreaIndexLink->set($this->data);
			if($this->AreaIndexLink->validates()){
				$this->data['AreaIndexLink']['show_order'] = 10000;
				$this->AreaIndexLink->save($this->data['AreaIndexLink']);
				//画像処理
				if(!empty($this->data['AreaIndexLink']['banner_add']['tmp_name'])){
					//画像アップロード(1.ファイル名(extなし),2.tmp_name,3.path)
					$upload_name = $this->CompFile->uploadImage(
							$this->AreaIndexLink->id,
							$this->data['AreaIndexLink']['banner_add']['tmp_name'],
							'link' . DS . 'area_index'
					);
					if(!empty($upload_name)){
						$this->data_update['AreaIndexLink']['banner'] = $upload_name;
					}
				}
				$this->data_update['AreaIndexLink']['id'] = $this->AreaIndexLink->id;
				$this->AreaIndexLink->save($this->data_update);
				$this->redirect('/area_index_links/addend');
			}
		}
		//大エリアセレクトボックス
		$this->set('large_area', Set::Combine($this->LargeArea->find('all', array('conditions' => array('LargeArea.id' => $this->parent_admin_large_area_list))), '{n}.LargeArea.id', '{n}.LargeArea.name'));
	}

	// 登録完了画面
	function addend(){
	}

	// 編集画面
	function edit($param){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_area_links_edit']){
			$this->cakeError('error404');
		}

		if (empty($this->data)){
			//地域リンク集データ読み込み
			$this->data = $this->AreaIndexLink->find('first', array('conditions' => array('AreaIndexLink.id' => $param, 'LargeArea.id' => $this->parent_admin_large_area_list)));
			if(!$this->data){
				$this->cakeError('error404');
			}
		}else{
			$this->AreaIndexLink->set($this->data);
			if($this->AreaIndexLink->validates()){
				//画像処理
				if(!empty($this->data['AreaIndexLink']['banner_edit']['tmp_name'])){
					//現在の画像を削除
					$this->CompFile->deleteFile('link' . DS . 'area_index' . DS . $this->data['AreaIndexLink']['banner']);
					//画像アップロード(1.ファイル名(extなし),2.tmp_name,3.path)
					$upload_name = $this->CompFile->uploadImage(
							$this->AreaIndexLink->id,
							$this->data['AreaIndexLink']['banner_edit']['tmp_name'],
							'link' . DS . 'area_index'
					);
					if(!empty($upload_name)){
						$this->data['AreaIndexLink']['banner'] = $upload_name;
					}
				}
				$this->AreaIndexLink->save($this->data);
				$this->redirect('/area_index_links/editend');
			}
		}
		//大エリアセレクトボックス
		$this->set('large_area', Set::Combine($this->LargeArea->find('all', array('conditions' => array('LargeArea.id' => $this->parent_admin_large_area_list))), '{n}.LargeArea.id', '{n}.LargeArea.name'));
		$this->set('data',$this->data);
	}

	// 編集完了画面
	function editend(){
	}

	// 削除
	function delete($param){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_area_links_delete']){
			$this->cakeError('error404');
		}

		if (!empty($this->data)){
			//画像削除
			$this->CompFile->deleteFile('link' . DS . 'area_index' . DS . $this->data['AreaIndexLink']['banner']);
			$this->AreaIndexLink->delete($this->data['AreaIndexLink']['id']);
			$this->redirect('/area_index_links/deleteend');
		} else {
			//地域リンク集データ読み込み
			$this->data = $this->AreaIndexLink->find('first', array('conditions' => array('AreaIndexLink.id' => $param, 'LargeArea.id' => $this->parent_admin_large_area_list)));
			if(!$this->data){
				$this->cakeError('error404');
			}
			$this->set('data',$this->data);
		}
		//大エリアセレクトボックス
		$this->set('large_area', Set::Combine($this->LargeArea->find('all'), '{n}.LargeArea.id', '{n}.LargeArea.name'));
	}
	// 削除完了画面
	function deleteend(){
	}

	//表示順位編集
	function change_link_order() {
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_area_links_edit']){
			$this->cakeError('error404');
		}

		//大エリアリスト
		$this->LargeArea->bindModel(array('hasOne' => array('AreaIndexLink' => array(
				'className'=>'AreaIndexLink',
				'fields' => array('COUNT(AreaIndexLink.id) AS count'),
				'conditions' => array('AreaIndexLink.large_area_id = LargeArea.id','LargeArea.id' => $this->parent_admin_large_area_list,),
				'foreignKey' => false))));
		$this->set('large_area', $this->LargeArea->find('all',array(
				'group' => 'LargeArea.id HAVING COUNT(AreaIndexLink.id) >= 1')));
		$this->set('link_order', Configure::read('link_order'));
	}
	function change_link_order2($large_area_id) {
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_area_links_edit']){
			$this->cakeError('error404');
		}

		//地域管理権限なしはエラー
		if(!in_array($large_area_id, $this->parent_admin_large_area_list)){
			$this->cakeError('error404');
		}

		if (!empty($this->data)){
			foreach ($this->data['AreaIndexLink'] as $data) {
				if(empty($data['show_order'])){
					$data['show_order'] = 10000;
				}
				$this->AreaIndexLink->save($data);
			}
			//start---2013/2/22 障害No.2-0001修正
			//$this->redirect($this->device_path . '/area_index_links/change_link_order_end');
			$this->redirect('/area_index_links/change_link_order_end');
			//end---2013/2/22 障害No.2-0001修正
		}else{
			$this->set('list', $this->AreaIndexLink->find('all', array(
					'conditions' => array('AreaIndexLink.large_area_id' => $large_area_id),
					'order' => 'AreaIndexLink.show_order ASC, AreaIndexLink.created ASC')));
			$show_order = array();
			for($i=0;$i<$this->AreaIndexLink->getNumRows();$i++){
				$show_order[$i+1] = $i+1;
			}
			$this->set('show_order', $show_order);
		}
		$this->set('large_area', $this->LargeArea->find('first', array('conditions' => array('LargeArea.id' => $large_area_id))));
	}
	function change_link_order_end(){
	}


	//start---2013/3/7 障害No.1-1-0012修正
	function error(){}
	//end---2013/3/7 障害No.1-1-0012修正

}
?>