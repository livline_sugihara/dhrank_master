<?php
class ReviewsReportController extends AppController {

	// 口コミ非掲載理由
	var $name = 'ReviewsReport';
	var $uses = array('Review','LargeArea', 'ReviewsReport','MReviewsReportsType');

	// 通報データ一覧
	function index()
	{
		// 通報データの取得
		$review_reports_data = $this->ReviewsReport->find('all',
			array(
				'fields' => '*',
			)
		);
		// 通報種別の種類
		$this->set('review_reports_data', $review_reports_data);
	}

	// 非掲載確認
	function confirm($review_id)
	{
		// $review_idを持っていなければリダイレクト
		if ($review_id == null || $review_id == '')
			$this->redirect('/reviews_report');

		// レビューデータ取得
		$review_data = $this->Review->find('first',array(
			'fields' => '*',
			'conditions' => array('Review.id' => $review_id),
		));

		$this->set('data', $review_data);

	}

	// 非掲載完了
	function finish($review_id)
	{
		// $review_idを持っていなければリダイレクト
		if ($review_id == null || $review_id == '')
			$this->redirect('/reviews_report');

		// 登録する内容設定
		$flg = 0;
		if ($this->params['form']['not_publish'] == '非掲載にする')
			$flg = 1;

		$data = array(
			'id' => $review_id,
			'not_publish' => $flg,
		);

		// 登録する項目
		$fields = array('not_publish');

		// データ更新
		$this->Review->save($data, false, $fields); // validateチェックを行う必要がないので、false

		// レビューデータ取得
		$review_data = $this->Review->find('first', array(
			'fields' => '*',
			'conditions' => array('Review.id' => $review_id),
		));

		$this->set('data', $review_data);
		$this->set('flg', $flg);

	}
}
?>
