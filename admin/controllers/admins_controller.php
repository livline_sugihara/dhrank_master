<?php
class AdminsController extends AppController {
	var $name = 'Admins';
	var $uses = array('Admin','LargeArea','AdminsAuthLargeArea');

	function beforeFilter() {
		parent::beforeFilter();
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_admins'] && $this->action != 'login' && $this->action != 'logout'){
			$this->cakeError('error404');
		}
	}

	//総合管理者アカウント一覧
	function index(){
		//地域管理権限有効大エリア数を取得
		$this->Admin->bindModel(array('hasOne' => array('AdminsAuthLargeArea' => array(
				'className'=>'AdminsAuthLargeArea',
				'fields'=>'COUNT(AdminsAuthLargeArea.id) as count',
				'conditions' => array('AdminsAuthLargeArea.selected' => 1),
				'foreignKey' => 'admin_id',))),false);
		//アカウント一覧取得
		$this->set('list', $this->Admin->find('all',array(
				'conditions' => array(),
				'group' => 'Admin.id',
				'order' => 'Admin.id ASC')));
		//表示用
		$this->set('presence', Configure::read('presence'));
		//大エリア
		$this->set('large_areas', $this->LargeArea->find('all',array(
				'order' => 'LargeArea.id ASC')));
		//大エリアカウント数
		$this->set('large_area_count', $this->LargeArea->find('count'));
	}

	//ログイン画面
	function login() {
		$this->layout = 'login';
		$user = $this->Auth->user();
		if ($user) {
			$this->redirect($this->Auth->redirect());
		}
	}
	//ログアウト
	function logout() {
		$this->Auth->logout();
		$this->redirect('/');
	}

	//総合管理者アカウント登録
	function add(){
		if(!empty($this->data)){
			$this->Admin->set($this->data);
			if($this->Admin->validates()){
				//validateが通ったのでパスワードを格納
				$this->data['Admin']['password'] = $this->Auth->password($this->data['Admin']['password_confirm']);
				//総合管理者アカウント
				$this->Admin->save($this->data);
				//大エリア登録
				foreach($this->data['AdminsAuthLargeArea'] as $admins_auth_large_area){
					$admins_auth_large_area['admin_id'] = $this->Admin->id;
					$this->AdminsAuthLargeArea->id = null;
					$this->AdminsAuthLargeArea->set($admins_auth_large_area);
					$this->AdminsAuthLargeArea->save($admins_auth_large_area);
				}
				$this->redirect('/admins/addend');
			}else{
				$this->data['Admin']['password'] = '';
			}
		}
		$this->set('data',$this->data);
		//大エリア
		$this->set('large_areas', $this->LargeArea->find('all',array('order' => 'LargeArea.id ASC')));
	}
	//登録完了画面
	function addend(){
	}

	//総合管理者アカウント編集
	function edit($param){
		if(!empty($this->data)){
			$this->Admin->set($this->data);
			if($this->Admin->validates()){
				//総合管理者アカウント
				$this->Admin->save($this->data);
				//大エリア登録
				//start---2013/3/8 障害No.1-1-0013修正
				if(!empty($this->data['AdminsAuthLargeArea'])){
					$this->AdminsAuthLargeArea->set($this->data);
					$this->AdminsAuthLargeArea->saveAll($this->data['AdminsAuthLargeArea']);
				}
				//end---2013/3/8 障害No.1-1-0013修正
				$this->redirect('/admins/editend');
			}
		}else{
			//総合管理者アカウントデータ読み込み
			$this->Admin->id = $param;
			$this->data = $this->Admin->read();
		}
		$this->set('data',$this->data);
		//大エリア
		$this->set('large_areas', $this->LargeArea->find('all',array('order' => 'LargeArea.id ASC')));
	}
	//編集完了画面
	function editend(){
	}


	//総合管理者アカウント削除
	function delete($param){
		if (!empty($this->data)){
			$this->AdminsAuthLargeArea->deleteAll(array('AdminsAuthLargeArea.admin_id' => $this->data['Admin']['id']));
			$this->Admin->delete($this->data['Admin']['id']);
			$this->redirect('/admins/deleteend');
		} else {
			//地域管理権限有効大エリア数を取得
			$this->Admin->bindModel(array('hasOne' => array('AdminsAuthLargeArea' => array(
					'className'=>'AdminsAuthLargeArea',
					'fields'=>'COUNT(AdminsAuthLargeArea.id) as count',
					'conditions' => array('AdminsAuthLargeArea.selected' => 1),
					'foreignKey' => 'admin_id',))),false);
			$this->Admin->id = $param;
			$this->data = $this->Admin->read();
			$this->set('data',$this->data);
		}

		//start---2013/3/1 障害No.1-1-0001修正
		//削除対象がadminの場合はエラー
		//自分自身のIDの場合はエラー
		//if($this->data['Admin']['username'] == 'admin'){
		if($this->data['Admin']['username'] == 'admin' || $this->data['Admin']['username'] == $this->parent_admin['Admin']['username']){
			//end---2013/3/1 障害No.1-1-0001修正
			$this->cakeError('error404');
		}
		//表示用
		$this->set('presence', Configure::read('presence'));
		//大エリア
		$this->set('large_areas', $this->LargeArea->find('all',array(
				'order' => 'LargeArea.id ASC')));
		//大エリアカウント数
		$this->set('large_area_count', $this->LargeArea->find('count'));
	}

	//削除完了画面
	function deleteend(){
	}

	// パスワード変更
	function changepassword($param){
		$oTmpOld = $this->data['Admin']['old_password'];
		$this->Admin->id = $param;
		$oTmp = $this->Admin->read();
		if (!empty($this->data)){
			$this->data['Admin']['old_password'] = $this->Auth->password($this->data['Admin']['old_password']);
			$this->Admin->set($this->data);
			unset($this->Admin->validate['password']);
			unset($this->Admin->validate['plan_id']);
			if($this->Admin->validates()){
				$this->data['Admin']['password'] = $this->data['Admin']['new_password'];
				$this->Admin->saveField('password',$this->Auth->password($this->data['Admin']['new_password']));
				$this->redirect('/admins/changepasswordend');
			}
		} else {
		}
		$this->data['Admin']['id'] = $oTmp['Admin']['id'];
		$this->data['Admin']['password'] = $oTmp['Admin']['password'];
		$this->data['Admin']['old_password'] = $oTmpOld;
		$this->set('data',$this->data);
	}
	// パスワード変更終了画面
	function changepasswordend(){
	}
}
?>
