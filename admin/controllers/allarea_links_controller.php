<?php
class AllareaLinksController extends AppController {
	var $name = 'AllareaLinks';
	var $uses = array('AllareaLink','Meta');
	var $components = array('CompFile');

	function beforeFilter() {
		parent::beforeFilter();
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_allarea_links']){
			$this->cakeError('error404');
		}
	}


	function index(){
		$this->set('list', $this->AllareaLink->find('all', array('order' => 'AllareaLink.show_order ASC, AllareaLink.created ASC')));
		$this->set('link_order', Configure::read('link_order'));
		$this->set('allarea_link_order',$this->Meta->find('first', array('conditions' => array('name' => 'allarea_link_order'))));
	}

	// 登録画面
	function add() {
		//登録ボタン押下時
		if (!empty($this->data)){
			$this->AllareaLink->set($this->data);
			if($this->AllareaLink->validates()){
				$this->data['AllareaLink']['show_order'] = 10000;
				$this->AllareaLink->save($this->data['AllareaLink']);
				//画像処理
				if(!empty($this->data['AllareaLink']['banner_add']['tmp_name'])){
					//画像アップロード(1.ファイル名(extなし),2.tmp_name,3.path)
					$upload_name = $this->CompFile->uploadImage(
							$this->AllareaLink->id,
							$this->data['AllareaLink']['banner_add']['tmp_name'],
							'link' . DS . '_allarea'
					);
					if(!empty($upload_name)){
						$this->data_update['AllareaLink']['banner'] = $upload_name;
					}
				}
				$this->data_update['AllareaLink']['id'] = $this->AllareaLink->id;
				$this->AllareaLink->save($this->data_update);
				$this->redirect('/allarea_links/addend');
			}
		}
	}

	// 登録完了画面
	function addend(){
	}

	// 編集画面
	function edit($param){
		if (empty($this->data)){
			$this->AllareaLink->id = $param;
			$this->data = $this->AllareaLink->read();
		}else{
			$this->AllareaLink->set($this->data);
			if($this->AllareaLink->validates()){
				//画像処理
				if(!empty($this->data['AllareaLink']['banner_edit']['tmp_name'])){
					//現在の画像を削除
					$this->CompFile->deleteFile('link' . DS . 'parent' . DS . $this->data['AllareaLink']['banner']);
					//画像アップロード(1.ファイル名(extなし),2.tmp_name,3.path)
					$upload_name = $this->CompFile->uploadImage(
							$this->AllareaLink->id,
							$this->data['AllareaLink']['banner_edit']['tmp_name'],
							'link' . DS . '_allarea'
					);
					if(!empty($upload_name)){
						$this->data['AllareaLink']['banner'] = $upload_name;
					}
				}
				$this->AllareaLink->save($this->data);
				$this->redirect('/allarea_links/editend');
			}
		}
		$this->set('data',$this->data);
	}

	// 編集完了画面
	function editend(){
	}

	// 削除
	function delete($param){
		if (!empty($this->data)){
			//画像削除
			//start---2013/3/10 障害No.1-1-0014修正
			//$this->CompFile->deleteFile('link' . DS . 'parent' . DS . $this->data['AllareaLink']['banner']);
			$this->CompFile->deleteFile('link' . DS . '_allarea' . DS . $this->data['AllareaLink']['banner']);
			//end---2013/3/10 障害No.1-1-0014修正
			$this->AllareaLink->delete($this->data['AllareaLink']['id']);
			$this->redirect('/allarea_links/deleteend');
		} else {
			$this->AllareaLink->id = $param;
			$this->data = $this->AllareaLink->read();
			$this->set('data',$this->data);
		}
	}
	// 削除完了画面
	function deleteend(){
	}

	//表示順位編集
	function change_link_order() {

		if (!empty($this->data)){
			foreach ($this->data['AllareaLink'] as $data) {
				if(empty($data['show_order'])){
					$data['show_order'] = 10000;
				}
				$this->AllareaLink->save($data);
			}
			//start---2013/2/22 障害No.2-0001修正
			//$this->redirect($this->device_path . '/allarea_links/change_link_order_end');
			$this->redirect('/allarea_links/change_link_order_end');
			//end---2013/2/22 障害No.2-0001修正

		}else{
			$this->set('list', $this->AllareaLink->find('all', array(
					'order' => 'AllareaLink.show_order ASC, AllareaLink.created ASC')));
			$show_order = array();
			for($i=0;$i<$this->AllareaLink->getNumRows();$i++){
				$show_order[$i+1] = $i+1;
			}
			$this->set('show_order', $show_order);
		}
	}
	function change_link_order_end(){
	}
}
?>