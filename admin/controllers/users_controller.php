<?php
class UsersController extends AppController {
	var $name = 'Users';
	var $uses = array('User','UsersAuthority','LargeArea','Shop','Girl','Review','Passport','Changepassword','System','BookmarkShop','BookmarkGirl','Message','ReviewsGoodCount');

	var $components = array('CompFile');

	function beforeFilter() {
		parent::beforeFilter();
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_users_edit'] && !$this->parent_admin['Admin']['is_auth_users_delete'] && !$this->parent_admin['Admin']['is_auth_users_changepassword']){
			$this->cakeError('error404');
		}
	}

	function index(){
		//検索条件設定
		$conditions = array();
		if (!empty($this->data['LargeArea']['id'])){
			if ($this->data['LargeArea']['id'] == -1) {
				array_push($conditions, array(
					'User.id IN (SELECT user_id FROM users_authorities WHERE large_area_id IN (' . implode(',', $this->parent_admin_large_area_list) . '))',
				));
			}else{
				array_push($conditions, array(
					'User.id IN (SELECT user_id FROM users_authorities WHERE large_area_id = ' . $this->data['LargeArea']['id'] . ')'
				));
			}
		}

		//並び替え
		$order = '';
		if(!empty($this->params['named']['order'])){
			if($this->params['named']['order'] == 'id_asc'){
				$order = 'User.id ASC';
			}elseif($this->params['named']['order'] == 'id_desc'){
				$order = 'User.id DESC';
			}elseif($this->params['named']['order'] == 'kana_asc'){
				$order = 'User.kana ASC';
			}elseif($this->params['named']['order'] == 'kana_desc'){
				$order = 'User.kana DESC';
			}
			$this->set('named_order','order:' . $this->params['named']['order']);
		}else{
			//初期表示時はポイント総計の多い順
			$this->set('named_order','id_asc');
			$order = 'UsersAuthority.id ASC, User.id ASC';
		}

		//データ取得
		if(!empty($conditions)){
			$this->set('list', $this->User->find('all',array(
					'conditions' => array($conditions),
					'order' => $order)));
		}else{
			$this->set('list', array());
		}
		//表示用
		$this->set('presence', Configure::read('presence'));

		//コンボボックスデータ取得
		$large_area = Set::Combine($this->LargeArea->find('all', array('conditions' => array('LargeArea.id' => $this->parent_admin_large_area_list))), '{n}.LargeArea.id', '{n}.LargeArea.name');
		$large_area[-1] = '全て';
		$this->set('large_area', $large_area);
	}

	// 登録画面
	function add() {
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_users_edit']){
			$this->cakeError('error404');
		}

		//大エリア登録がなければエラー
		if($this->LargeArea->find('count') == 0){
			$this->redirect('/users/error');
		}

		//大エリアセレクトボックス
		$this->set('large_areas', Set::Combine($this->LargeArea->find('all', array(
			'conditions' => array(
				'LargeArea.id' => $this->parent_admin_large_area_list,
			)
		)), '{n}.LargeArea.id', '{n}.LargeArea.name'));

		if (empty($this->data)){
			//初期表示
			// $this->data['User']['is_review_delete_enabled'] = 1;
			// $this->data['User']['is_official_link_display'] = 1;
			// $this->data['User']['is_sidebar'] = 1;
		}else{
			//更新ボタン押下時
			//バリデーションチェック
			$this->User->set($this->data);
			$this->Shop->set($this->data);
			$user_validate = $this->User->validates();
			$shop_validate = $this->Shop->validates();
			if($user_validate && $shop_validate){
				//店舗アカウント情報を保存する前にパスワードを退避
				$passTmp = $this->data['User']['password'];
				//暗号化
				$this->data['User']['password'] = $this->Auth->password($this->data['User']['password']);
				//店舗アカウントデータ保存
				$this->User->create();
				$this->User->save($this->data['User']);
				//店舗データ保存
				$this->Shop->user_id = $this->User->id;
				$this->Shop->name = $this->data['Shop']['name'];
				$this->Shop->save($this->Shop);
				//保存後オブジェクトにパスワードを戻す
				$this->data['User']['password'] = $passTmp;
				$this->set('data',$this->data);

				//大エリア取り出し
				$large_area = $this->LargeArea->findAllById($this->data['User']['large_area_id']);
				$this->set('large_area',$large_area[0]);

				$large_area = $this->LargeArea->find('all', array(
					'conditions' => array(
						'LargeArea.id' => $this->data['UsersAuthority']['large_area_id'],
					)
				));
				$this->set('register_large_area', $large_area);

/*
				//config取得
				$presence = Configure::read('presence');
				//レビュー削除権限取り出し
				$this->set('is_review_delete_enabled',$presence[$this->data['User']['is_review_delete_enabled']]);
				//オフィシャルリンク表示取り出し
				$this->set('is_official_link_display',$presence[$this->data['User']['is_official_link_display']]);
				//サイドバー表示取り出し
				$this->set('is_sidebar',$presence[$this->data['User']['is_sidebar']]);
*/
				// エリアの保存
				$this->_make_authority($this->User->id);

				$this->render($action = "addend");
			}
		}
	}

	// 更新完了画面
	function addend(){
	}

	// 編集画面
	function edit($param){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_users_edit']){
			$this->cakeError('error404');
		}

		//大エリアセレクトボックス
		$this->set('large_areas', Set::Combine($this->LargeArea->find('all', array(
			'conditions' => array(
				'LargeArea.id' => $this->parent_admin_large_area_list,
			)
		)), '{n}.LargeArea.id', '{n}.LargeArea.name'));


		if (empty($this->data)){
			//店舗アカウントデータ読み込み
			$this->User->unbindModel(array('belongsTo' => array('LargeArea', 'UsersAuthority')));
			$this->data = $this->User->find('first', array(
				'conditions' => array(
					'User.id' => $param,
					// 'LargeArea.id' => $this->parent_admin_large_area_list
				)
			));

			$users_authority = $this->UsersAuthority->find('all', array(
				'conditions' => array(
					'UsersAuthority.user_id' => $param,
				),
			));

			foreach($users_authority AS $ii => $record) {
				$this->data['UsersAuthority']['large_area_id'][] = $record['UsersAuthority']['large_area_id'];
			}

			if(!$this->data){
				$this->cakeError('error404');
			}
		}else{

			$this->User->set($this->data);
			$this->Shop->set($this->data);
			$user_validate = $this->User->validates();
			$shop_validate = $this->Shop->validates();
			if($user_validate && $shop_validate){
				$this->User->id = $this->data['User']['id'];
				$this->User->save($this->data);
				$this->Shop->name = $this->data['Shop']['name'];
				$this->Shop->save($this->Shop);

				// エリアの保存
				$this->_make_authority($param);

				$this->redirect('/users/editend');
			}
		}
		$this->set('data',$this->data);
	}

	function _make_authority($user_id) {

		$register_area = Set::Combine($this->UsersAuthority->find('all', array(
			'conditions' => array(
				'UsersAuthority.user_id' => $user_id,
			),
		)), '{n}.UsersAuthority.id', '{n}.UsersAuthority.large_area_id');

		foreach($this->data['UsersAuthority']['large_area_id'] AS $ii => $large_area_id) {

			$ind = array_search($large_area_id, $register_area);
			// 未登録の場合
			if($ind === FALSE) {
				$ins = array(
					'user_id' => $user_id,
					'large_area_id' => $large_area_id,
					'order_rank' => 1000,
				);
				$this->UsersAuthority->create();
				$this->UsersAuthority->save($ins);

				// FIXME デバッグ
				$this->log('UserId: ' . $user_id . 'のエリアID[' . $large_area_id . ']を追加しました', 'debug');
			} else {
				// FIXME デバッグ
				$this->log('UserId: ' . $user_id . 'のエリアID[' . $large_area_id . ']は存在しますのでキープします', 'debug');
			}
			unset($register_area[$ind]);
		}

		// 登録済みで、選択されなかったもの
		foreach($register_area AS $id => $large_area_id) {

			$this->UsersAuthority->delete($id);

			// FIXME デバッグ
			$this->log('UserId: ' . $user_id . 'のエリアID[' . $large_area_id . ']を削除しました', 'debug');
		}

	}

	// 編集画面
	function edit_authority($user_id, $large_area_id){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_users_edit']){
			$this->cakeError('error404');
		}

		if (empty($this->data)){
			//店舗アカウントデータ読み込み
			$this->data = $this->UsersAuthority->find('first', array(
				'conditions' => array(
					'UsersAuthority.user_id' => $user_id,
					'UsersAuthority.large_area_id' => $large_area_id,
				)
			));

			// FIXME デバッグ
			$this->log($this->data, 'debug');

			if(!$this->data){
				$this->cakeError('error404');
			}
		}else{

			// FIXME デバッグ
			$this->log($this->data, 'debug');

			$this->UsersAuthority->set($this->data);
			if($this->UsersAuthority->validates()) {
				$this->UsersAuthority->id = $this->data['UsersAuthority']['id'];
				$this->UsersAuthority->save($this->data);

				$this->redirect('/users/editend');
			}
		}
		$this->set('data',$this->data);
	}


	// 編集完了画面
	function editend(){
	}

	// 削除
	function delete($param){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_users_delete']){
			$this->cakeError('error404');
		}

		if (!empty($this->data)){
			//店舗フォルダ削除
			$this->CompFile->deleteFolder('shop' . DS . $this->data['User']['id']);

			//女の子データ取得
			$girls = $this->Girl->find('all',array('fields' => 'id','conditions' => array('Girl.user_id' => $this->data['User']['id'])));
			//女の子画像用店舗フォルダ削除
			//start---2013/4/25 障害No.4-0001修正
			$this->CompFile->deleteFolder('girl' . DS . $this->data['User']['id']);
			//end---2013/4/25 障害No.4-0001修正

			foreach($girls as $girl){
				//start---2013/4/25 障害No.4-0001修正
				//$this->CompFile->deleteFolder('girl' . DS . $girl['Girl']['id']);
				//end---2013/4/25 障害No.4-0001修正
				//start---2013/3/1 障害No.1-1-0004修正
				$this->BookmarkGirl->deleteAll(array('BookmarkGirl.girl_id' => $girl['Girl']['id']));
				//end---2013/3/1 障害No.1-1-0004修正
			}
			//女の子データ削除
			$this->Girl->deleteAll(array('Girl.user_id' => $this->data['User']['id']));

			//口コミデータ取得
			$reviews = $this->Review->find('all',array('fields' => 'id','conditions' => array('Review.user_id' => $this->data['User']['id'])));
			//投票削除
			foreach($reviews as $review){
				$this->ReviewsGoodCount->deleteAll(array('ReviewsGoodCount.review_id' => $review['Review']['id']));
				//start---2013/3/1 障害No.1-1-0004修正
				//メッセージ削除
				$this->Message->deleteAll(array('Message.review_id' => $review['Review']['id']));
				//end---2013/3/1 障害No.1-1-0004修正
			}
			//口コミ削除
			$this->Review->deleteAll(array('Review.user_id' => $this->data['User']['id']));

			//店舗データ削除
			$this->Shop->deleteAll(array('Shop.user_id' => $this->data['User']['id']));
			$this->System->deleteAll(array('System.user_id' => $this->data['User']['id']));

			//ブックマーク削除
			$this->BookmarkShop->deleteAll(array('BookmarkShop.user_id' => $this->data['User']['id']));
			//start---2013/3/1 障害No.1-1-0004修正
			//$this->BookmarkGirl->deleteAll(array('BookmarkGirl.user_id' => $this->data['User']['id']));
			//end---2013/3/1 障害No.1-1-0004修正

			//start---2013/3/1 障害No.1-1-0004修正
			//メッセージ削除
			//$this->Message->deleteAll(array('Message.user_id' => $this->data['User']['id']));
			//end---2013/3/1 障害No.1-1-0004修正

			//パスポート・パスワード変更ログ削除
			$this->Passport->deleteAll(array('Passport.user_id' => $this->data['User']['id']));
			$this->Changepassword->deleteAll(array('Changepassword.user_id' => $this->data['User']['id']));
			//Userデータ削除
			$this->User->delete($this->data['User']['id']);
			$this->redirect('/users/deleteend');
		} else {
			//店舗アカウントデータ読み込み
			$this->data = $this->User->find('first', array('conditions' => array('User.id' => $param, 'LargeArea.id' => $this->parent_admin_large_area_list)));
			if(!$this->data){
				$this->cakeError('error404');
			}
			$this->set('data',$this->data);
		}
		//表示用
		$this->set('presence', Configure::read('presence'));
	}

	//削除完了画面
	function deleteend(){
	}

	// パスワード変更
	function changepassword($param){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_users_changepassword']){
			$this->cakeError('error404');
		}

		$oTmpOld = $this->data['User']['old_password'];
		$this->User->id = $param;
		$oTmp = $this->User->read();
		if (!empty($this->data)){
			$this->data['User']['old_password'] = $this->Auth->password($this->data['User']['old_password']);
			$this->User->set($this->data);
			unset($this->User->validate['password']);
			unset($this->User->validate['plan_id']);
			if($this->User->validates()){
				$this->data['User']['password'] = $this->data['User']['new_password'];
				$this->User->saveField('password',$this->Auth->password($this->data['User']['new_password']));
				$this->redirect('/users/changepasswordend');
			}
		} else {
			//start---2013/3/12 障害No.3-0012修正
			//店舗アカウントデータ読み込み
			$this->data = $this->User->find('first', array('conditions' => array('User.id' => $param, 'LargeArea.id' => $this->parent_admin_large_area_list)));
			if(!$this->data){
				$this->cakeError('error404');
			}
			//end---2013/3/12 障害No.3-0012修正
		}
		$this->data['User']['id'] = $oTmp['User']['id'];
		$this->data['User']['password'] = $oTmp['User']['password'];
		$this->data['User']['old_password'] = $oTmpOld;
		$this->set('data',$this->data);
	}
	// パスワード変更終了画面
	function changepasswordend(){
	}

	//エラー
	function error(){
	}
}
?>
