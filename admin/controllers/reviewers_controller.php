<?php
class ReviewersController extends AppController {
	var $name = 'Reviewers';
	var $uses = array('Reviewer','Review','LargeArea');

	function beforeFilter() {
		parent::beforeFilter();
		//権限がない場合はエラー
		// if(!$this->parent_admin['Admin']['is_auth_reviews_edit'] && !$this->parent_admin['Admin']['is_auth_reviews_delete']){
		// 	$this->cakeError('error404');
		// }
	}

	function index(){
		//検索条件設定
		$conditions = array();
		if (!empty($this->data['LargeArea']['id'])){
			if ($this->data['LargeArea']['id'] == -1){
				array_push($conditions, array('Reviewer.large_area_id' => $this->parent_admin_large_area_list));
			}else{
				array_push($conditions, array('Reviewer.large_area_id' => $this->data['LargeArea']['id']));
			}
		}
		if(!empty($conditions)){
			//データ取得
			$this->set('list', $this->Reviewer->find('all',array(
				'fields' => array(
					'LargeArea.name','Reviewer.*','MReviewersAge.name','MReviewersJob.name',
				),
				'conditions' => array($conditions), 'order' => 'Reviewer.created DESC')));
		}else{
			$this->set('list', array());
		}

		//コンボボックスデータ取得
		$large_area = Set::Combine($this->LargeArea->find('all', array('conditions' => array('LargeArea.id' => $this->parent_admin_large_area_list))), '{n}.LargeArea.id', '{n}.LargeArea.name');
		$large_area[-1] = '全て';
		$this->set('large_area', $large_area);
	}

	// 削除
	function delete($param){

		// 権限がない場合はエラー
		// if(!$this->parent_admin['Admin']['is_auth_reviews_delete']){
		// 	$this->cakeError('error404');
		// }

		if (!empty($this->data)){
			//テーブル削除
			$this->Reviewer->delete($this->data['Reviewer']['id']);

			$reviews = $this->Review->find('all', array(
				'conditions' => array('Review.reviewer_id' => $this->data['Reviewer']['id']),
			));
			foreach($reviews AS $ii => $review) {

				$this->ReviewsGoodCount->deleteAll(array('ReviewsGoodCount.review_id' => $review['Review']['id']));
				$this->ReviewsReport->deleteAll(array('ReviewsReport.report_review_id' => $review['Review']['id']));
			}
			$this->Review->deleteAll(array('Review.reviewer_id' => $this->data['Reviewer']['id']));

			$this->redirect('/reviewers/deleteend');
		} else {
			$this->data = $this->Reviewer->find('first', array(
				'fields' => array(
					'LargeArea.name','Reviewer.*','MReviewersAge.name','MReviewersJob.name','MShopsBusinessCategory.name',
				),
				'conditions' => array('Reviewer.id' => $param)
			));
			if(!$this->data){
				$this->cakeError('error404');
			}
			$this->set('data',$this->data);
		}
	}

	// 削除完了
	function deleteend(){
	}
}
?>
