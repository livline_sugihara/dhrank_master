<?php
class BigbannerController extends AppController {
	var $name = 'Bigbanner';
	var $uses = array('LargeArea', 'BigBanner');
	var $components = array('CompFile');

	function beforeFilter() {
		parent::beforeFilter();
		// //権限がない場合はエラー
		// if(!$this->parent_admin['Admin']['is_auth_area_links_edit'] && !$this->parent_admin['Admin']['is_auth_area_links_delete']){
		// 	$this->cakeError('error404');
		// }
	}

	function index(){
		//検索条件設定
		$conditions = array();
		if (!empty($this->data['BigBanner']['large_area_id'])){
			if ($this->data['BigBanner']['large_area_id'] == -1){
				array_push($conditions, array('BigBanner.large_area_id' => $this->parent_admin_large_area_list));
			}else{
				array_push($conditions, array('BigBanner.large_area_id' => $this->data['BigBanner']['large_area_id']));
			}
		}
		if(!empty($conditions)){
			$this->set('list', $this->BigBanner->find('all', array(
					'conditions' => array($conditions),
					'order' => 'BigBanner.created DESC',
			)));
		}else{
			$this->set('list', array());
		}

		//コンボボックスデータ取得
		$large_area = Set::Combine($this->LargeArea->find('all', array('conditions' => array('LargeArea.id' => $this->parent_admin_large_area_list))), '{n}.LargeArea.id', '{n}.LargeArea.name');
		$large_area[-1] = '全て';
		$this->set('large_area', $large_area);
	}

	// 登録画面
	function add() {
		//権限がない場合はエラー
		// if(!$this->parent_admin['Admin']['is_auth_area_links_edit']){
		// 	$this->cakeError('error404');
		// }

		//start---2013/3/7 障害No.1-1-0012修正
		// 大エリア登録がなければエラー
		// if($this->LargeArea->find('count', array('conditions' => array('id' => $large_area_id))) == 0){
		// 	$this->redirect('/area_index_links/error');
		// }
		//end---2013/3/7 障害No.1-1-0012修正

		//登録ボタン押下時
		if (!empty($this->data)){
			$this->BigBanner->set($this->data);
			if($this->BigBanner->validates()){
				$this->BigBanner->save($this->data['BigBanner']);

				//画像処理(店舗)
				if(!empty($this->data['BigBanner']['shop_filename_add']['tmp_name'])){

					//画像アップロード(1.ファイル名(extなし),2.tmp_name,3.path)
					$upload_name = $this->CompFile->uploadImage(
							'shop_' . $this->BigBanner->id,
							$this->data['BigBanner']['shop_filename_add']['tmp_name'],
							'banner' . DS . 'big_banner' . DS . $this->data['BigBanner']['large_area_id']
					);
					if(!empty($upload_name)){
						$this->data_update['BigBanner']['shop_filename'] = $upload_name;
					}
				}
				//画像処理(女の子)
				if(!empty($this->data['BigBanner']['girl_filename_add']['tmp_name'])){

					//画像アップロード(1.ファイル名(extなし),2.tmp_name,3.path)
					$upload_name = $this->CompFile->uploadImage(
							'girl_' . $this->BigBanner->id,
							$this->data['BigBanner']['girl_filename_add']['tmp_name'],
							'banner' . DS . 'big_banner' . DS . $this->data['BigBanner']['large_area_id']
					);
					if(!empty($upload_name)){
						$this->data_update['BigBanner']['girl_filename'] = $upload_name;
					}
				}
				$this->data_update['BigBanner']['id'] = $this->BigBanner->id;
				$this->BigBanner->save($this->data_update);
				$this->redirect('/bigbanner/addend');
			}
		}
		//大エリアセレクトボックス
		$this->set('large_area', Set::Combine($this->LargeArea->find('all', array('conditions' => array('LargeArea.id' => $this->parent_admin_large_area_list))), '{n}.LargeArea.id', '{n}.LargeArea.name'));
	}

	// 登録完了画面
	function addend(){
	}

	// 編集画面
	function edit($param){
		//権限がない場合はエラー
		// if(!$this->parent_admin['Admin']['is_auth_area_links_edit']){
		// 	$this->cakeError('error404');
		// }

		if (empty($this->data)){
			//地域リンク集データ読み込み
			$this->data = $this->BigBanner->find('first', array('conditions' => array('BigBanner.id' => $param, 'BigBanner.large_area_id' => $this->parent_admin_large_area_list)));
			if(!$this->data){
				$this->cakeError('error404');
			}
		}else{
			$this->BigBanner->set($this->data);
			if($this->BigBanner->validates()){
				//画像処理(店舗)
				if(!empty($this->data['BigBanner']['shop_filename_edit']['tmp_name'])){

					//画像アップロード(1.ファイル名(extなし),2.tmp_name,3.path)
					$upload_name = $this->CompFile->uploadImage(
							'shop_' . $this->BigBanner->id,
							$this->data['BigBanner']['shop_filename_edit']['tmp_name'],
							'banner' . DS . 'big_banner' . DS . $this->data['BigBanner']['large_area_id']
					);
					if(!empty($upload_name)){
						$this->data['BigBanner']['shop_filename'] = $upload_name;
					}
				}
				//画像処理(女の子)
				if(!empty($this->data['BigBanner']['girl_filename_edit']['tmp_name'])){

					//画像アップロード(1.ファイル名(extなし),2.tmp_name,3.path)
					$upload_name = $this->CompFile->uploadImage(
							'girl_' . $this->BigBanner->id,
							$this->data['BigBanner']['girl_filename_edit']['tmp_name'],
							'banner' . DS . 'big_banner' . DS . $this->data['BigBanner']['large_area_id']
					);
					if(!empty($upload_name)){
						$this->data['BigBanner']['girl_filename'] = $upload_name;
					}
				}
				$this->BigBanner->save($this->data);
				$this->redirect('/bigbanner/editend');
			}
		}
		//大エリアセレクトボックス
		$this->set('large_area', Set::Combine($this->LargeArea->find('all', array('conditions' => array('LargeArea.id' => $this->parent_admin_large_area_list))), '{n}.LargeArea.id', '{n}.LargeArea.name'));
		$this->set('data',$this->data);
	}

	// 編集完了画面
	function editend(){
	}

	// 削除
	function delete($param){
		//権限がない場合はエラー
		// if(!$this->parent_admin['Admin']['is_auth_area_links_delete']){
		// 	$this->cakeError('error404');
		// }

		if (!empty($this->data)){
			//画像削除
			$this->CompFile->deleteFile('banner' . DS . 'big_banner' . DS . $this->data['BigBanner']['large_area_id'] . DS . $this->data['BigBanner']['shop_filename']);
			$this->CompFile->deleteFile('banner' . DS . 'big_banner' . DS . $this->data['BigBanner']['large_area_id'] . DS . $this->data['BigBanner']['girl_filename']);
			$this->BigBanner->delete($this->data['BigBanner']['id']);
			$this->redirect('/bigbanner/deleteend');
		} else {
			//地域リンク集データ読み込み
			$this->data = $this->BigBanner->find('first', array(
				'conditions' => array(
					'BigBanner.id' => $param,
					'BigBanner.large_area_id' => $this->parent_admin_large_area_list
				)
			));
			if(!$this->data){
				$this->cakeError('error404');
			}
			$this->set('data',$this->data);
		}
		//大エリアセレクトボックス
		$this->set('large_area', Set::Combine($this->LargeArea->find('all'), '{n}.LargeArea.id', '{n}.LargeArea.name'));
	}
	// 削除完了画面
	function deleteend(){
	}
}
?>