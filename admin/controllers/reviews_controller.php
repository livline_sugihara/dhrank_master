<?php
class ReviewsController extends AppController {
	var $name = 'Reviews';
	var $uses = array('Review','LargeArea','VetoIpAddressReview','VetoUidReview','ReviewsGoodCount');

	function beforeFilter() {
		parent::beforeFilter();
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_reviews_edit'] && !$this->parent_admin['Admin']['is_auth_reviews_delete']){
			$this->cakeError('error404');
		}
	}

	function index(){
		//検索条件設定
		$conditions = array();
		if (!empty($this->data['LargeArea']['id'])){
			if ($this->data['LargeArea']['id'] == -1){
//				array_push($conditions, array('User.large_area_id' => $this->parent_admin_large_area_list));
				array_push($conditions, array(
					'User.id IN (SELECT user_id FROM users_authorities WHERE large_area_id IN (' . implode(',', $this->parent_admin_large_area_list) . '))',
					'Review.is_publish=0'
				));
			}else{
//				array_push($conditions, array('User.large_area_id' => $this->data['LargeArea']['id']));
				array_push($conditions, array(
					'User.id IN (SELECT user_id FROM users_authorities WHERE large_area_id = ' . $this->data['LargeArea']['id'] . ')'
				));
			}
		}
		if(!empty($conditions)){
			//データ取得
			$review_data = 	$this->Review->find('all',
								array(
									'fields' => array(
										'LargeArea.name','Girl.name','Shop.name','Review.*','Reviewer.*','ReviewsReport.*',
										'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg',
										'VetoIpAddressReview.id','VetoUidReview.id',
									),
									'conditions' => array($conditions),
									'order' => 'Review.created DESC',
									'group' => 'Review.id',
								)
							);
			$this->set('list', $review_data);

		}else{
			$this->set('list', array());
		}

		//コンボボックスデータ取得
		$large_area = Set::Combine($this->LargeArea->find('all',
								array('conditions' =>
									array(
										'LargeArea.id' => $this->parent_admin_large_area_list,
									)
								)
							),
							'{n}.LargeArea.id', '{n}.LargeArea.name');
		// テスト大エリア
		$large_area[-1] = '未承認';
		$this->set('large_area', $large_area);
	}

	//投稿日時編集
	function edit_created($param){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_reviews_edit']){
			$this->cakeError('error404');
		}

		$created = '';
		if (!empty($this->data)){
			$this->Review->set($this->data);
			$created = $this->data['Review']['created']['year'] . '-' .
					$this->data['Review']['created']['month'] . '-' .
					$this->data['Review']['created']['day'] . ' ' .
					$this->data['Review']['created']['hour'] . ':' .
					$this->data['Review']['created']['min'] . ':' . $this->data['Review']['created_second'];
			if($this->Review->validates()){
				$this->Review->id = $this->data['Review']['id'];
				$this->data['Review']['created'] = $created;
				$this->data['Review']['modified'] = $created;
				$this->Review->save($this->data);
				$this->redirect('/reviews/edit_created_end');
			}
		}
		$this->data = $this->Review->find('first', array(
			'fields' => array(
				'LargeArea.name','Girl.name','Shop.name','Review.*','Reviewer.*',
				'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg',
			),
			'conditions' => array(
				'Review.id' => $param,
				'User.id IN (SELECT user_id FROM users_authorities WHERE large_area_id IN (' . implode(',', $this->parent_admin_large_area_list) . '))',
			)
		));
		if(!$this->data){
			$this->cakeError('error404');
		}
		if($created){
			$this->data['Review']['created'] = $created;
		}
		$this->set('data',$this->data);
	}

	//投稿日時編集完了画面
	function edit_created_end(){
	}

	// 削除ステータス変更
	function delete($param) {
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_reviews_delete']) {
			$this->cakeError('error404');
		}

		$this->set('delete_method', array(
			Review::DELETE_FLAG_NONE => '通常',
			Review::DELETE_FLAG_COMMENT_ONLY => 'コメント削除',
			Review::DELETE_FLAG_SAVE_POINTS => '依頼削除',
			Review::DELETE_FLAG_ALL => '全削除',
		));

		if (!empty($this->data)) {

			$this->Review->create();
			$this->Review->id = $this->data['Review']['id'];
			// $this->Review->comment = null;
			$this->Review->delete_flag = $this->data['Review']['delete_flag'];

			$sql = '';
			$sql .= ' UPDATE reviews';
			$sql .= ' SET delete_flag = "' . $this->Review->delete_flag . '"';
			$sql .= ' WHERE id = "'.$this->Review->id.'"';

			$this->Review->query($sql);
//			$this->Review->save($this->Review, false, array('delete_flag'));
			$this->redirect('/reviews/deleteend');
		} else {
			$this->data = $this->Review->find('first', array(
				'fields' => array(
					'LargeArea.name','Girl.name','Shop.name','Review.*','Reviewer.*',
					'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg',
				),
				'conditions' => array(
					'Review.id' => $param,
					'User.id IN (SELECT user_id FROM users_authorities WHERE large_area_id IN (' . implode(',', $this->parent_admin_large_area_list) . '))',
				)
			));
			if(!$this->data){
				$this->cakeError('error404');
			}
			$this->set('data',$this->data);
		}
	}

	// 削除ステータス変更完了画面
	function deleteend(){
	}

	// ピックアップステータス変更
	function pickup($param) {
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_reviews_delete']) {
			$this->cakeError('error404');
		}

		if (!empty($this->data)) {
			$this->Review->create();
			$this->Review->id = $this->data['Review']['id'];
			$this->Review->pickup_admin = 1;

			// PICKUPフラグを立てる
			$sql = '';
			$sql .= ' UPDATE reviews';
			$sql .= ' SET pickup_admin = 1';
			$sql .= ' WHERE id = "' . $param . '"';
			$this->Review->query($sql);

//			$this->Review->save($this->Review, false, array('pickup_admin'));
			$this->redirect('/reviews/pickupend');
		} else {
			$this->data = $this->Review->find('first', array(
				'fields' => array(
					'LargeArea.name','Girl.name','Shop.name','Review.*','Reviewer.*',
					'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg',
				),
				'conditions' => array(
					'Review.id' => $param,
					'User.id IN (SELECT user_id FROM users_authorities WHERE large_area_id IN (' . implode(',', $this->parent_admin_large_area_list) . '))',
				)
			));
			if(!$this->data){
				$this->cakeError('error404');
			}
			$this->set('data',$this->data);
		}
	}

	// ピックアップステータス変更完了画面
	function pickupend(){
	}

	// ピックアップステータス解除
	function pickup_remove($param) {
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_reviews_delete']) {
			$this->cakeError('error404');
		}

		if (!empty($this->data)) {
			$this->Review->create();
			$this->Review->id = $this->data['Review']['id'];
			$this->Review->pickup_admin = 0;
			// PICKUPフラグを解除する
			$sql = '';
			$sql .= ' UPDATE reviews';
			$sql .= ' SET pickup_admin = 0';
			$sql .= ' WHERE id = "' . $param . '"';
			$this->Review->query($sql);

//			$this->Review->save($this->Review, false, array('pickup_admin'));
			$this->redirect('/reviews/pickup_remove_end');
		} else {
			$this->data = $this->Review->find('first', array(
				'fields' => array(
					'LargeArea.name','Girl.name','Shop.name','Review.*','Reviewer.*',
					'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg',
				),
				'conditions' => array(
					'Review.id' => $param,
					'User.id IN (SELECT user_id FROM users_authorities WHERE large_area_id IN (' . implode(',', $this->parent_admin_large_area_list) . '))',
				)
			));
			if(!$this->data){
				$this->cakeError('error404');
			}
			$this->set('data',$this->data);
		}
	}

	// ピックアップステータス変更完了画面
	function pickup_remove_end(){
	}

	// ピックアップステータス変更
	function member_only($param) {
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_reviews_delete']) {
			$this->cakeError('error404');
		}

		if (!empty($this->data)) {

			$cnt = $this->Review->find('count', array(
				'conditions' => array(
					'Review.id' => $param,
					'member_only' => 1,
					'User.id IN (SELECT user_id FROM users_authorities WHERE large_area_id IN (' . implode(',', $this->parent_admin_large_area_list) . '))',
				)
			));

			$this->Review->create();
			$this->Review->id = $this->data['Review']['id'];
			if($cnt == 0) {
				$this->Review->member_only = 1;
			} else {
				$this->Review->member_only = 0;
			}

			// 会員限定フラグを変更する
			$sql = '';
			$sql .= ' UPDATE reviews';
			$sql .= ' SET member_only = "' . $this->Review->member_only . '"';
			$sql .= ' WHERE id = "' . $param . '"';

			$this->Review->query($sql);
			//$this->Review->save($this->Review, false, array('member_only'));
			$this->redirect('/reviews/member_onlyend');
		} else {
			$this->data = $this->Review->find('first', array(
				'fields' => array(
					'LargeArea.name','Girl.name','Shop.name','Review.*','Reviewer.*',
					'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg',
				),
				'conditions' => array(
					'Review.id' => $param,
					'User.id IN (SELECT user_id FROM users_authorities WHERE large_area_id IN (' . implode(',', $this->parent_admin_large_area_list) . '))',
				)
			));
			if(!$this->data){
				$this->cakeError('error404');
			}
			$this->set('data',$this->data);
		}
	}

	// ピックアップステータス変更完了画面
	function member_onlyend(){
	}

	// 公開ステータス変更
	function publish($param) {
		//権限がない場合はエラー
		// if(!$this->parent_admin['Admin']['is_auth_reviews_delete']) {
		// 	$this->cakeError('error404');
		// }

		if (!empty($this->data)) {
			$this->Review->create();
			$this->Review->id = $this->data['Review']['id'];
			$this->Review->is_publish = 1;
			$this->Review->save($this->Review, false, array('is_publish'));
			$this->redirect('/reviews/publishend');
		} else {
			$this->data = $this->Review->find('first', array(
				'fields' => array(
					'LargeArea.name','Girl.name','Shop.name','Review.*','Reviewer.*',
					'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg',
				),
				'conditions' => array(
					'Review.id' => $param,
					'User.id IN (SELECT user_id FROM users_authorities WHERE large_area_id IN (' . implode(',', $this->parent_admin_large_area_list) . '))',
				)
			));
			if(!$this->data){
				$this->cakeError('error404');
			}
			$this->set('data',$this->data);
		}
	}

	// ピックアップステータス変更完了画面
	function publishend(){
	}

/*
	//コメント削除完了画面
	function delete_comment_end(){
	}

	// 削除
	function delete($param){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_reviews_delete']){
			$this->cakeError('error404');
		}

		if (!empty($this->data)){
			//テーブル削除
			$this->Review->delete($this->data['Review']['id']);
			$this->ReviewsGoodCount->deleteAll(array('ReviewsGoodCount.review_id' => $this->data['Review']['id']));
			$this->redirect('/reviews/deleteend');
		} else {
			$this->data = $this->Review->find('first', array(
					'fields' => array('LargeArea.name','Girl.name','Review.id','Review.comment','Shop.name','Review.created','Review.post_name','Reviewer.id','Reviewer.handle',
							'Review.media', 'Review.ip_address', 'Review.uid', 'Review.comment_deleted',
							'(Review.score_girl_first_impression + Review.score_girl_word + Review.score_girl_service + Review.score_girl_sensitivity + Review.score_girl_style + Review.score_girl_voice) / 6  as girl_avg',),
					'conditions' => array('Review.id' => $param, 'LargeArea.id' => $this->parent_admin_large_area_list)));
			if(!$this->data){
				$this->cakeError('error404');
			}
			$this->set('data',$this->data);
		}
	}
*/
	//IPアドレス投稿禁止設定
	function veto_ip_address($param){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_reviews_edit']){
			$this->cakeError('error404');
		}

		if (!empty($this->data)){
			//start---2013/3/5 障害No.1-1-0007修正
			if(0 == $this->VetoIpAddressReview->find('count', array('conditions' => array('VetoIpAddressReview.name' => $this->data['Review']['ip_address'])))){
				//end---2013/3/5 障害No.1-1-0007修正
				$this->VetoIpAddressReview->name = $this->data['Review']['ip_address'];
				$this->VetoIpAddressReview->save($this->VetoIpAddressReview);
				//start---2013/3/5 障害No.1-1-0007修正
			}
			//end---2013/3/5 障害No.1-1-0007修正

			$this->redirect('/reviews/veto_ip_address_end');
		} else {
			$this->Review->id = $param;
			$this->data = $this->Review->read();
			$this->set('data',$this->data);
		}
	}

	//IPアドレス投稿禁止設定完了画面
	function veto_ip_address_end(){
	}

	//個体識別番号投稿禁止設定
	function veto_uid($param){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_reviews_edit']){
			$this->cakeError('error404');
		}

		if (!empty($this->data)){
			//start---2013/3/5 障害No.1-1-0007修正
			if(0 == $this->VetoUidReview->find('count', array('conditions' => array('VetoUidReview.name' => $this->data['Review']['uid'])))){
				//end---2013/3/5 障害No.1-1-0007修正
				$this->VetoUidReview->name = $this->data['Review']['uid'];
				$this->VetoUidReview->save($this->VetoUidReview);
				//start---2013/3/5 障害No.1-1-0007修正
			}
			//end---2013/3/5 障害No.1-1-0007修正

			$this->redirect('/reviews/veto_uid_end');
		} else {
			$this->Review->id = $param;
			$this->data = $this->Review->read();
			$this->set('data',$this->data);
		}
	}

	//個体識別番号投稿禁止設定完了画面
	function veto_uid_end(){
	}

	//禁止IPアドレス一覧画面
	function view_veto_ip_address(){
		$list = $this->VetoIpAddressReview->find('all',array('order' => 'VetoIpAddressReview.created ASC'));
		$this->set('list',$list);
	}
	//禁止IPアドレス解除確認画面
	function cancel_veto_ip_address($param){
		if (!empty($this->data)){
			//禁止IPアドレス解除
			$this->VetoIpAddressReview->delete($this->data['VetoIpAddressReview']['id']);
			$this->redirect('/reviews/cancel_veto_ip_address_end');
		} else {
			$this->VetoIpAddressReview->id = $param;
			$this->data = $this->VetoIpAddressReview->read();
			$this->set('data',$this->data);
		}
	}
	//禁止IPアドレス解除完了画面
	function cancel_veto_ip_address_end(){

	}

	//禁止個体識別番号一覧画面
	function view_veto_uid(){
		$list = $this->VetoUidReview->find('all',array('order' => 'VetoUidReview.created ASC'));
		$this->set('list',$list);
	}

	//禁止個体識別番号解除確認画面
	function cancel_veto_uid($param){
		if (!empty($this->data)){
			//禁止個体識別番号解除
			$this->VetoUidReview->delete($this->data['VetoUidReview']['id']);
			$this->redirect('/reviews/cancel_veto_uid_end');
		} else {
			$this->VetoUidReview->id = $param;
			$this->data = $this->VetoUidReview->read();
			$this->set('data',$this->data);
		}
	}
	//禁止個体識別番号解除完了画面
	function cancel_veto_uid_end(){

	}
}
?>
