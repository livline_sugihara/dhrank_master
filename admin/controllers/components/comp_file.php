<?php
class CompFileComponent extends Object
{
	var $uses = array('Folder','File');

	//画像アップロード(圧縮無し)
	function uploadImage($name, $tmp_name, $path) {
		// 画像アップロード
		if (!empty($tmp_name)) {
			$filename = "";
			$imginfo = getimagesize($tmp_name);
			clearstatcache();
			$upload_path = APP_IMG_PATH . $path . DS;

			// ディレクトリがなければ作成
			if(!file_exists($upload_path)) {
				$this->makeFolder($path);
			}

			switch ($imginfo[2]) {
				case 2: // jpeg
					$filename = sprintf("%s.jpg", $name);
					move_uploaded_file($tmp_name, $upload_path . $filename);
					chmod($upload_path . $filename, 0777);
					break;
				case 1: // gif
					$filename = sprintf("%s.gif", $name);
					move_uploaded_file($tmp_name, $upload_path . $filename);
					chmod($upload_path . $filename, 0777);
					break;
				Default:
					break;
			}
			return $filename;
		} else {
			return  false;
		}
	}

	function makeFolder($path){
		$folder = APP_IMG_PATH . $path ;
		if( !file_exists($folder) ){
			mkdir($folder, 0777);
			chmod($folder, 0777);
		}
	}
	function deleteFolder($path){
		$folder = APP_IMG_PATH . $path ;
		if( file_exists($folder) ){
			$f = new Folder();
			$f->delete($folder);
		}
	}
	function deleteFile($path){
		$file = APP_IMG_PATH . $path ;
		if( file_exists($file) ){
			$f = new File($file);
			$f->delete();
		}
	}


}