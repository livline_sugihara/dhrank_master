<?php
class BoardsController extends AppController {
	var $name = 'Boards';
	var $uses = array('Board','LargeArea','VetoIpAddressBoard','VetoUidBoard','BoardRes');

	function beforeFilter() {
		parent::beforeFilter();
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_boards_edit'] && !$this->parent_admin['Admin']['is_auth_boards_delete']){
			$this->cakeError('error404');
		}
	}

	function index(){
		//検索条件設定
		$conditions = array();
		if (!empty($this->data['LargeArea']['id'])){
			if ($this->data['LargeArea']['id'] == -1){
				array_push($conditions, array('Board.large_area_id' => $this->parent_admin_large_area_list));
			}else{
				array_push($conditions, array('Board.large_area_id' => $this->data['LargeArea']['id']));
			}
		}
		//レス数をBIND
		$this->Board->bindModel(array('hasOne' => array('BoardRes' => array(
				'className'=>'BoardRes',
				'fields'=>'COUNT(BoardRes.id) as count',
				'conditions' => array(),
				'foreignKey' => 'board_id',))),false);
		if(!empty($conditions)){
			//データ取得
			$this->set('list', $this->Board->find('all',array(
					'conditions' => array($conditions),
					'group' => 'Board.id',
					'order' => 'Board.show_order ASC, Board.modified DESC',
			)));
		}else{
			$this->set('list', array());
		}

		//コンボボックスデータ取得
		$large_area = Set::Combine($this->LargeArea->find('all', array('conditions' => array('LargeArea.id' => $this->parent_admin_large_area_list))), '{n}.LargeArea.id', '{n}.LargeArea.name');
		$large_area[-1] = '全て';
		$this->set('large_area', $large_area);
	}

	//掲示板内容表示画面
	function view($param){
		$this->data = $this->Board->find('first', array('conditions' => array('Board.id' => $param, 'LargeArea.id' => $this->parent_admin_large_area_list)));
		if(!$this->data){
			$this->cakeError('error404');
		}
		$this->set('data',$this->data);
		$this->set('list',$this->BoardRes->find('all', array('conditions' => array('BoardRes.board_id' => $param))));
	}

	//掲示板レス削除確認画面
	function delete_res($param){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_boards_delete']){
			$this->cakeError('error404');
		}

		//start---2013/3/12 障害No.3-0012修正
		$ret = $this->Board->find('first', array('conditions' => array('Board.id' => $param, 'LargeArea.id' => $this->parent_admin_large_area_list)));
		if(!$ret){
			$this->cakeError('error404');
		}
		//end---2013/3/12 障害No.3-0012修正

		if (!empty($this->data)){
			$this->set('list',$this->BoardRes->find('all', array('conditions' => array('BoardRes.id' => Set::extract('/BoardRes[id>0]/id', $this->data)))));
			$this->Board->id = $this->data['Board']['id'];
			$this->data = $this->Board->read();
			$this->set('data',$this->data);
		}
	}

	//掲示板レス削除完了画面
	function delete_res_end(){
		if (!empty($this->data)){
			$this->BoardRes->deleteAll(array('BoardRes.id' => Set::extract('/BoardRes[id>0]/id', $this->data)));
		} else {
		}
	}

	//掲示板削除確認画面
	function delete($param){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_boards_delete']){
			$this->cakeError('error404');
		}

		if (!empty($this->data)){
			//テーブル削除
			$this->Board->delete($this->data['Board']['id']);
			$this->BoardRes->deleteAll(array('BoardRes.board_id' => $this->data['Board']['id']));
			$this->redirect('/boards/deleteend');
		} else {
			//レス数をBIND
			$this->Board->bindModel(array('hasOne' => array('BoardRes' => array(
					'className'=>'BoardRes',
					'fields'=>'COUNT(BoardRes.id) as count',
					'conditions' => array(),
					'foreignKey' => 'board_id',))),false);
			$this->data = $this->Board->find('first', array('conditions' => array('Board.id' => $param, 'LargeArea.id' => $this->parent_admin_large_area_list)));
			if(empty($this->data['Board']['id'])){
				$this->cakeError('error404');
			}
			$this->set('data',$this->data);
		}
	}

	//削除完了画面
	function deleteend(){
	}

	//IPアドレス投稿禁止設定
	function veto_ip_address($param){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_boards_edit']){
			$this->cakeError('error404');
		}

		if (!empty($this->data)){
			//start---2013/3/5 障害No.1-1-0007修正
			if(0 == $this->VetoIpAddressBoard->find('count', array('conditions' => array('VetoIpAddressBoard.name' => $this->data['BoardRes']['ip_address'])))){
				//end---2013/3/5 障害No.1-1-0007修正
				$this->VetoIpAddressBoard->name = $this->data['BoardRes']['ip_address'];
				$this->VetoIpAddressBoard->save($this->VetoIpAddressBoard);
				//start---2013/3/5 障害No.1-1-0007修正
			}
			//end---2013/3/5 障害No.1-1-0007修正

			$this->redirect('/boards/veto_ip_address_end');
		} else {
			$this->BoardRes->id = $param;
			$this->data = $this->BoardRes->read();
			$this->set('data',$this->data);
		}
	}

	//IPアドレス投稿禁止設定完了画面
	function veto_ip_address_end(){
	}

	//個体識別番号投稿禁止設定
	function veto_uid($param){
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_boards_edit']){
			$this->cakeError('error404');
		}

		if (!empty($this->data)){
			//start---2013/3/5 障害No.1-1-0007修正
			if(0 == $this->VetoUidBoard->find('count', array('conditions' => array('VetoUidBoard.name' => $this->data['BoardRes']['uid'])))){
				//end---2013/3/5 障害No.1-1-0007修正
				$this->VetoUidBoard->name = $this->data['BoardRes']['uid'];
				$this->VetoUidBoard->save($this->VetoUidBoard);
				//start---2013/3/5 障害No.1-1-0007修正
			}
			//end---2013/3/5 障害No.1-1-0007修正

			$this->redirect('/boards/veto_uid_end');
		} else {
			$this->BoardRes->id = $param;
			$this->data = $this->BoardRes->read();
			$this->set('data',$this->data);
		}
	}

	//個体識別番号投稿禁止設定完了画面
	function veto_uid_end(){
	}

	//禁止IPアドレス一覧画面
	function view_veto_ip_address(){
		$list = $this->VetoIpAddressBoard->find('all',array('order' => 'VetoIpAddressBoard.created ASC'));
		$this->set('list',$list);
	}
	//禁止IPアドレス解除確認画面
	function cancel_veto_ip_address($param){
		if (!empty($this->data)){
			//禁止IPアドレス解除
			$this->VetoIpAddressBoard->delete($this->data['VetoIpAddressBoard']['id']);
			$this->redirect('/boards/cancel_veto_ip_address_end');
		} else {
			$this->VetoIpAddressBoard->id = $param;
			$this->data = $this->VetoIpAddressBoard->read();
			$this->set('data',$this->data);
		}
	}
	//禁止IPアドレス解除完了画面
	function cancel_veto_ip_address_end(){

	}
	//禁止個体識別番号一覧画面
	function view_veto_uid(){
		$list = $this->VetoUidBoard->find('all',array('order' => 'VetoUidBoard.created ASC'));
		$this->set('list',$list);
	}
	//禁止個体識別番号解除確認画面
	function cancel_veto_uid($param){
		if (!empty($this->data)){
			//禁止個体識別番号解除
			$this->VetoUidBoard->delete($this->data['VetoUidBoard']['id']);
			$this->redirect('/boards/cancel_veto_uid_end');
		} else {
			$this->VetoUidBoard->id = $param;
			$this->data = $this->VetoUidBoard->read();
			$this->set('data',$this->data);
		}
	}
	//禁止個体識別番号解除完了画面
	function cancel_veto_uid_end(){

	}
}
?>