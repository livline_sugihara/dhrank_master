<?php
class AllareaLinkOrdersController extends AppController {
	var $name = 'AllareaLinkOrders';
	var $uses = array('Meta');

	function beforeFilter() {
		parent::beforeFilter();
		//権限がない場合はエラー
		if(!$this->parent_admin['Admin']['is_auth_allarea_links']){
			$this->cakeError('error404');
		}
	}


	function index(){
		if (empty($this->data)){
			$this->data = $this->Meta->find('first', array('conditions' => array('name' => 'allarea_link_order')));
		}else{
			$this->Meta->set($this->data);
			//start---2013/2/22 障害No.2-0003修正
			if(empty($this->data['Meta']['value'])){
				$this->Meta->invalidate('value', '選択してください。');
			}
			//end---2013/2/22 障害No.2-0003修正
			if($this->Meta->validates()){
				$this->Meta->save($this->data);
				$this->redirect('/allarea_link_orders/editend');
			}
		}
		$this->set('link_order', Configure::read('link_order'));
		$this->set('data',$this->data);
	}

	// 編集完了画面
	function editend(){
	}

}
?>