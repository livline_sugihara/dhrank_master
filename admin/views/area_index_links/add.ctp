
<h3>地域リンク集登録（インデックス）</h3>

<?php echo $form->create(null, array('type'=>'post','enctype' => 'multipart/form-data', 'action' => "")); ?>
<table>
	<tr>
		<th>大エリア</th>
		<td><?php echo $form->select('AreaIndexLink.large_area_id', $large_area,null,array('empty'=>'選んでください')); ?>
		<?php echo $form->error('AreaIndexLink.large_area_id'); ?></td>
	</tr>
	<tr>
		<th width="300">サイト名</th>
		<td>
			<?php echo $form->text('AreaIndexLink.name'); ?>
			<?php echo $form->error('AreaIndexLink.name'); ?>
		</td>
	</tr>
	<tr>
		<th>URL</th>
		<td>
			<?php echo $form->text('AreaIndexLink.url'); ?>
			<?php echo $form->error('AreaIndexLink.url'); ?>
		</td>
	</tr>
	<tr>
		<th>バナー画像</th>
		<td>
			<?php echo $form->file('AreaIndexLink.banner_add');?>
			<?php echo $form->error('AreaIndexLink.banner_add'); ?>
		</td>
	</tr>
</table>
<?php echo $form->end('地域リンク集登録'); ?>
