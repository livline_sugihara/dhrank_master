
<h3>地域リンク集（インデックス）削除</h3>

<?php echo $form->create(null,array('type'=>'post','action'=>"")); ?>
<?php echo $form->hidden('AreaIndexLink.id'); ?>
<?php echo $form->hidden('AreaIndexLink.banner'); ?>
<table>
	<tr>
		<th width="300">ID</th>
		<td>
			<?php echo $data['AreaIndexLink']['id']; ?>
		</td>
	</tr>
	<tr>
		<th>大エリア</th>
		<td>
			<?php echo $data['LargeArea']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>サイト名</th>
		<td>
			<?php echo $data['AreaIndexLink']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>URL</th>
		<td>
			<?php //start---2013/3/7 障害No.1-1-0011修正 ?>
			<?php //echo $data['AreaIndexLink']['url']; ?>
			<a href="<?php echo $data['AreaIndexLink']['url']; ?>" target="_blank"><?php echo $data['AreaIndexLink']['url']; ?></a>
			<?php //end---2013/3/7 障害No.1-1-0011修正 ?>
		</td>
	</tr>
	<tr>
		<th>バナー</th>
		<td>
			<img src="<?php echo APP_IMG_URL . 'link/area_index/' . $data['AreaIndexLink']['banner'] ?>" />
		</td>
	</tr>
</table>
<?php
$msg = __($data['AreaIndexLink']['name'] . 'を削除します。よろしいですか？', true);
echo $form->submit(__('削除', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')"));
echo $form->end();
?>
