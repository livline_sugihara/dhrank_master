<?php if($parent_admin['Admin']['is_auth_area_links_edit']){?>
<?php echo $html->link('登録','/area_index_links/add'); ?>
&nbsp;
<?php echo $html->link('表示順位編集','/area_index_links/change_link_order'); ?>
&nbsp;
<?php }?>

<h3>地域リンク集一覧（インデックス）</h3>

<?php echo $form->create(null, array('type'=>'post', 'action' => '')); ?>
<table>
	<tr style='color: #000066; background-color: #AAAAFF'>
		<th>ID</th>
		<th>
			大エリア<br />
			<?php echo $form->select('LargeArea.id', $large_area,null,array('empty'=>'選択','onchange'=>'return this.form.submit();')); ?>
		</th>
		<th>サイト名</th>
		<th>URL</th>
		<th>バナー</th>
		<?php if($parent_admin['Admin']['is_auth_area_links_edit']){?>
		<th>編集</th>
		<?php }?>
		<?php if($parent_admin['Admin']['is_auth_area_links_delete']){?>
		<th>削除</th>
		<?php }?>
	</tr>
	<?php foreach($list as $record){ ?>
	<tr style='color: #000099; background-color: #DDDDFF'>
		<td>
			<?php echo $record['AreaIndexLink']['id'] ?>
		</td>
		<td>
			<?php echo $record['LargeArea']['name'] ?>
		</td>
		<td>
			<?php echo $record['AreaIndexLink']['name'] ?>
		</td>
		<td>
			<a href="<?php echo $record['AreaIndexLink']['url'] ?>" target="_blank"><?php echo $record['AreaIndexLink']['url'] ?> </a>
		</td>
		<td>
			<img src="<?php echo APP_IMG_URL . 'link/area_index/' . $record['AreaIndexLink']['banner'] ?>" />
		</td>
		<?php if($parent_admin['Admin']['is_auth_area_links_edit']){?>
		<td>
			<a href="/area_index_links/edit/<?php echo $record['AreaIndexLink']['id'] ?>">編集</a>
		</td>
		<?php }?>
		<?php if($parent_admin['Admin']['is_auth_area_links_delete']){?>
		<td>
			<a href="/area_index_links/delete/<?php echo $record['AreaIndexLink']['id'] ?>">削除</a>
		</td>
		<?php }?>
	</tr>
	<?php } ?>
</table>
<?php echo $form->end(); ?>
