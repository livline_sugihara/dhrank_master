<h3>地域リンク集（インデックス）表示順位編集</h3>
<h3>編集対象大エリア：<?php echo $large_area['LargeArea']['name']?></h3>
<?php echo $form->create(null, array('type'=>'post', 'action' => '/change_link_order2/' . $large_area['LargeArea']['id'])); ?>
<table class="normal">
	<tr>
		<th>サイト名</th>
		<th>URL</th>
		<th>表示順</th>
	</tr>
	<?php foreach($list as $key => $record){?>
	<tr>
		<td>
			<?php echo $record['AreaIndexLink']['name'] ?>
		</td>
		<td>
			<?php //start---2013/3/7 障害No.1-1-0011修正 ?>
			<?php //echo $record['AreaIndexLink']['url']; ?>
			<a href="<?php echo $record['AreaIndexLink']['url']; ?>" target="_blank"><?php echo $record['AreaIndexLink']['url']; ?></a>
			<?php //end---2013/3/7 障害No.1-1-0011修正 ?>
		</td>
		<td>
			<?php echo $form->select('AreaIndexLink.' . $key . '.show_order', $show_order,$record['AreaIndexLink']['show_order'],array('empty'=>'選択してください')); ?>
			<?php echo $form->hidden('AreaIndexLink.' . $key . '.id', array('value' => $record['AreaIndexLink']['id']));?> 番目に表示
		</td>
	</tr>
	<?php } ?>
</table>
<?php echo $form->end('表示順位変更'); ?>

