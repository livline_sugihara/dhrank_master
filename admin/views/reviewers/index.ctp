<h3>レビュアー情報一覧</h3>
<style>
table tr th {
	text-align: center;
}

td.align-center {
	text-align: center;
}
td.align-right {
	text-align: right;
}
</style>

<?php echo $form->create(null, array('type'=>'post', 'action' => '')); ?>
<table>
	<tr style='color: #000066; background-color: #AAAAFF'>
		<th>
			メインエリア<br />
			<?php echo $form->select('LargeArea.id', $large_area,null,array('empty'=>'選択','onchange'=>'return this.form.submit();')); ?>
		</th>
		<th>ユーザーID</th>
		<th>ニックネーム</th>
		<th>メールアドレス</th>
		<th>職業</th>
		<th>年代</th>
		<th>コメント</th>
		<th>削除</th>
	</tr>

<?php foreach($list as $record){ ?>
	<tr style='color: #000099; background-color: #DDDDFF'>
		<td class="align-center">
			<?php echo $record['LargeArea']['name']; ?>
		</td>
		<td>
			<?php echo $record['Reviewer']['username']; ?>
		</td>
		<td>
			<?php echo $record['Reviewer']['handle']; ?>
		</td>
		<td>
			<?php echo $record['Reviewer']['email']; ?>
		</td>

		<td class="align-center">
			<?php echo $record['MReviewersJob']['name']; ?>
		</td>
		<td class="align-center">
			<?php echo $record['MReviewersAge']['name']; ?>
		</td>
		<td>
			<?php echo mb_substr($record['Reviewer']['comment'], 0, 20); ?>
		</td>
		<td class="align-center">
			<a href="/reviewers/delete/<?php echo $record['Reviewer']['id'] ?>">削除</a>
		</td>
	</tr>
<?php } ?>
</table>
<?php echo $form->end(); ?>