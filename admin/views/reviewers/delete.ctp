<h3>レビュアー削除</h3>

<?php echo $form->create(null,array('type'=>'post','action'=>"")); ?>
<?php echo $form->hidden('Reviewer.id'); ?>
<table>
	<tr>
		<th>メインエリア</th>
		<td>
			<?php echo $data['LargeArea']['name'] ?>
		</td>
	</tr>
	<tr>
		<th>ユーザーID</th>
		<td>
			<?php echo $data['Reviewer']['username']; ?>
		</td>
	</tr>
	<tr>
		<th>ニックネーム</th>
		<td>
			<?php echo $data['Reviewer']['handle']?>
		</td>
	</tr>
	<tr>
		<th>メールアドレス</th>
		<td>
			<?php echo $data['Reviewer']['email']; ?>
		</td>
	</tr>
	<tr>
		<th>年代</th>
		<td>
			<?php echo $data['MReviewersAge']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>よく利用するカテゴリ</th>
		<td>
			<?php echo $data['MShopsBusinessCategory']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>職業</th>
		<td>
			<?php echo $data['MReviewersJob']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>コメント</th>
		<td>
			<?php echo $data['Reviewer']['comment']; ?>
		</td>
	</tr>
	<tr>
		<th>登録日時</th>
		<td>
			<?php echo date('Y年m月d日 H時i分s秒', strtotime($data['Reviewer']['created'])); ?>
		</td>
	</tr>
</table>
<?php
$msg = __($data['Reviewer']['username'] . ':' . $data['Reviewer']['handle'] . 'を削除します。削除したデータは元に戻りません！よろしいですか？', true);
echo $form->submit(__('削除', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')"));
echo $form->end();
?>

<?php //echo var_dump($data)?>