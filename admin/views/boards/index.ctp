

<h3>掲示板情報一覧</h3>
<a href="/boards/view_veto_ip_address">禁止IPアドレス一覧画面へ</a>
<br />
<a href="/boards/view_veto_uid">禁止個体識別番号一覧画面へ</a>
<?php echo $form->create(null, array('type'=>'post', 'action' => '')); ?>
<table>
	<tr style='color: #000066; background-color: #AAAAFF'>
		<th>
			大エリア<br />
			<?php echo $form->select('LargeArea.id', $large_area,null,array('empty'=>'選択','onchange'=>'return this.form.submit();')); ?>
		</th>
		<th>更新日</th>
		<th>掲示板名</th>
		<th>内容表示</th>
		<?php if($parent_admin['Admin']['is_auth_boards_delete']){?>
		<th>削除</th>
		<?php }?>
	</tr>

	<?php foreach($list as $record){ ?>
	<tr style='color: #000099; background-color: #DDDDFF'>
		<td>
			<?php echo $record['LargeArea']['name'] ?>
		</td>
		<td>
			<?php echo $record['Board']['modified'] ?>
		</td>
		<td>
			<?php echo $record['Board']['title'] ?>(<?php echo $record[0]['count']?>)
		</td>
		<td>
			<a href="/boards/view/<?php echo $record['Board']['id'] ?>">表示</a>
		</td>
		<?php if($parent_admin['Admin']['is_auth_boards_delete']){?>
		<td>
			<?php if($record['Board']['show_order'] > 2){?>
			<a href="/boards/delete/<?php echo $record['Board']['id'] ?>">削除</a>
			<?php }?>
		</td>
		<?php }?>
	</tr>
	<?php } ?>

</table>
<?php echo $form->end(); ?>