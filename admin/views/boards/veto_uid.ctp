<h3>個体識別番号投稿禁止設定</h3>

<?php //start---2013/3/5 障害No.1-1-0006修正 ?>
<?php //echo $form->create(null,array('type'=>'post','action'=>"")); ?>
<?php echo $form->create(null,array('type'=>'post','action'=>"veto_uid/" . $data['BoardRes']['id'])); ?>
<?php //end---2013/3/5 障害No.1-1-0006修正 ?>
<?php echo $form->hidden('BoardRes.uid'); ?>
<table>
	<tr>
		<th>個体識別番号</th>
		<td><?php echo $data['BoardRes']['uid']; ?></td>
	</tr>
</table>
<?php
$msg = __('[' . $data['BoardRes']['uid'] . ']を投稿禁止にします。よろしいですか？', true);
echo $form->submit(__('投稿禁止', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')"));
echo $form->end();
?>

<?php //echo var_dump($data)?>