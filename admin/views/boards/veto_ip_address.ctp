<h3>IPアドレス投稿禁止設定</h3>

<?php //start---2013/3/5 障害No.1-1-0006修正 ?>
<?php //echo $form->create(null,array('type'=>'post','action'=>"")); ?>
<?php echo $form->create(null,array('type'=>'post','action'=>"veto_ip_address/" . $data['BoardRes']['id'])); ?>
<?php //end---2013/3/5 障害No.1-1-0006修正 ?>
<?php echo $form->hidden('BoardRes.ip_address'); ?>
<table>
	<tr>
		<th>IPアドレス</th>
		<td><?php echo $data['BoardRes']['ip_address']; ?></td>
	</tr>
</table>
<?php
$msg = __('[' . $data['BoardRes']['ip_address'] . ']を投稿禁止にします。よろしいですか？', true);
echo $form->submit(__('投稿禁止', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')"));
echo $form->end();
?>

<?php //echo var_dump($data)?>