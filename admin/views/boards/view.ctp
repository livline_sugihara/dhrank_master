

<h3>掲示板閲覧</h3>
<?php echo $form->create(null, array('type'=>'post', 'action' => 'delete_res')); ?>
<?php echo $form->hidden('Board.id'); ?>
<div>
	<?php echo $data['Board']['title']?>
</div>
<?php foreach($list as $key => $record){ ?>
<div>
	<?php if($parent_admin['Admin']['is_auth_boards_delete']){?>
	<?php //start---2013/3/12 障害No.1-4-0009修正 ?>
	<?php if($data['Board']['show_order'] > 2 || $record['BoardRes']['sub_id'] != 1){?>
	<?php //end---2013/3/12 障害No.1-4-0009修正 ?>
	<?php //start---2013/3/5 障害No.1-1-0005修正 ?>
	<?php //echo $form->checkbox('BoardRes.' . $key . '.id', array('checked' => false))?>
	<?php echo $form->checkbox('BoardRes.' . $key . '.id', array('checked' => false,'value' => $record['BoardRes']['id']))?>
	<?php //end---2013/3/5 障害No.1-1-0005修正 ?>
	<?php //start---2013/3/12 障害No.1-4-0009修正 ?>
	<?php }?>
	<?php //end---2013/3/12 障害No.1-4-0009修正 ?>
	<?php }?>
	<?php echo $record['BoardRes']['sub_id']?>
	名前：
	<?php echo $record['BoardRes']['name']?>
	投稿日：
	<?php echo $textCommon->get_datetimewithweek($record['BoardRes']['created'])?>
	<br /> 媒体：
	<?php echo $record['BoardRes']['media'] ?>
	&nbsp;&nbsp;&nbsp; IPアドレス：
	<?php if(!empty($record['VetoIpAddressBoard']['id']) || !$parent_admin['Admin']['is_auth_boards_edit']){?>
	<?php echo $record['BoardRes']['ip_address'] ?>
	<?php }else{?>
	<a href="/boards/veto_ip_address/<?php echo $record['BoardRes']['id'] ?>"><?php echo $record['BoardRes']['ip_address'] ?> </a>
	<?php }?>
	&nbsp;&nbsp;&nbsp; 個体識別番号：
	<?php if(!empty($record['VetoUidBoard']['id']) || !$parent_admin['Admin']['is_auth_boards_edit']){?>
	<?php echo $record['BoardRes']['uid'] ?>
	<?php }else{?>
	<a href="/boards/veto_uid/<?php echo $record['BoardRes']['id'] ?>"><?php echo $record['BoardRes']['uid'] ?> </a>
	<?php }?>
	<div style="margin: 5px;">
		<?php echo nl2br($record['BoardRes']['comment'])?>
	</div>
</div>
<?php }?>

<?php if($parent_admin['Admin']['is_auth_boards_delete']){?>
<?php echo $form->submit('削除', array('div' => 'false')); ?>
<?php }?>
<?php echo $form->end(); ?>
