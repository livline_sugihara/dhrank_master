

<h3>掲示板レス削除確認画面</h3>
<?php echo $form->create(null, array('type'=>'post', 'action' => 'delete_res_end')); ?>
<?php echo $form->hidden('Board.id'); ?>
<div>
	<?php echo $data['Board']['title']?>
</div>
<?php if(!empty($list)){?>
<?php foreach($list as $key => $record){ ?>
<div>
	<?php echo $form->hidden('BoardRes.' . $key . '.id', array('value' => $record['BoardRes']['id'])); ?>
	<?php echo $record['BoardRes']['sub_id']?>
	名前：
	<?php echo $record['BoardRes']['name']?>
	投稿日：
	<?php echo $textCommon->get_datetimewithweek($record['BoardRes']['created'])?>
	<br /> 媒体：
	<?php echo $record['BoardRes']['media'] ?>
	&nbsp;&nbsp;&nbsp; IPアドレス：
	<?php echo $record['BoardRes']['ip_address'] ?>
	&nbsp;&nbsp;&nbsp; 個体識別番号：
	<?php echo $record['BoardRes']['uid'] ?>
	<div style="margin: 5px;">
		<?php echo nl2br($record['BoardRes']['comment'])?>
	</div>
</div>
<?php }?>

<?php
$msg = __('レスを削除します。削除したデータは元に戻りません！よろしいですか？', true);
echo $form->submit(__('削除', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')"));
echo $form->end();
?>
<?php }else{?>
レスが選択されていません。<br />
<?php }?>
<a href="/boards/view/<?php echo $data['Board']['id']?>">戻る</a>
