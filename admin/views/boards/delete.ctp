<h3>掲示板削除</h3>

<?php echo $form->create(null,array('type'=>'post','action'=>"")); ?>
<?php echo $form->hidden('Board.id'); ?>
<table>
	<tr>
		<th>大エリア</th>
		<td>
			<?php echo $data['LargeArea']['name'] ?>
		</td>
	</tr>
	<tr>
		<th>更新日</th>
		<td>
			<?php echo $data['Board']['modified']; ?>
		</td>
	</tr>
	<tr>
		<th>掲示板名</th>
		<td>
			<?php echo $data['Board']['title']; ?>
			<?php echo '(' . $data[0]['count'] . ')'?>
		</td>
	</tr>
	<tr>
		<th>1番目のレス</th>
		<td>
			<?php if(!empty($data['BoardRes'][0]['id'])){?>
			<?php echo $data['BoardRes'][0]['sub_id']?>
			名前：
			<?php echo $data['BoardRes'][0]['name']?>
			投稿日：
			<?php echo $textCommon->get_datetimewithweek($data['BoardRes'][0]['created'])?>
			<br /> 媒体：
			<?php echo $data['BoardRes'][0]['media'] ?>
			&nbsp;&nbsp;&nbsp; IPアドレス：
			<?php echo $data['BoardRes'][0]['ip_address'] ?>
			&nbsp;&nbsp;&nbsp; 個体識別番号：
			<?php echo $data['BoardRes'][0]['uid'] ?>
			<div style="margin: 5px;">
				<?php echo nl2br($data['BoardRes'][0]['comment'])?>
			</div>

			<?php }?>
		</td>
	</tr>
</table>
<?php
$msg = __('掲示板を削除します。削除したデータは元に戻りません！よろしいですか？', true);
echo $form->submit(__('削除', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')"));
echo $form->end();
?>

<?php //echo var_dump($data)?>