﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php echo $this->Html->charset(); ?>
<title><?php __('CakePHP: the rapid development php framework:'); ?> <?php echo $title_for_layout; ?>
</title>
<?php
echo $this->Html->meta('icon');
echo $this->Html->css('cake.generic');
echo $scripts_for_layout;
?>
</head>
<body>

	<div id="container">

<?php //echo var_dump($parent_admin); ?>

		<div id="header">
			<h1><?php echo $this->Html->link('管理画面TOP', '/'); ?></h1>
		</div>

		<div id="content">
<?php if($parent_admin['Admin']['is_auth_admins']){?>
			<?php echo $html->link('総合管理者アカウント管理','/admins'); ?>&nbsp;
<?php }?>

<?php if($parent_admin['Admin']['is_auth_users_edit'] || $parent_admin['Admin']['is_auth_users_delete'] || $parent_admin['Admin']['is_auth_users_changepassword']){?>
			<?php echo $html->link('店舗アカウント管理','/users'); ?>&nbsp;
<?php }?>

<?php if($parent_admin['Admin']['is_auth_large_areas']){?>
			<?php echo $html->link('大エリア管理','/large_areas'); ?>&nbsp;
<?php }?>

<?php if($parent_admin['Admin']['is_auth_reviews_edit'] || $parent_admin['Admin']['is_auth_reviews_delete']){?>
			<?php echo $html->link("口コミ管理",'/reviews'); ?>&nbsp;
			<?php echo $html->link("口コミ通報管理",'/reviews_report'); ?>&nbsp;
<?php }?>

<?php /* if($parent_admin['Admin']['is_auth_boards_edit'] || $parent_admin['Admin']['is_auth_boards_delete']){?>
			<?php echo $html->link("掲示板管理",'/boards'); ?>&nbsp;
<?php } */ ?>

<?php /* if($parent_admin['Admin']['is_auth_allarea_links']){?>
			<?php echo $html->link('全国リンク集管理','/allarea_links'); ?>&nbsp;
<?php } */ ?>

<?php /* if($parent_admin['Admin']['is_auth_area_links_edit'] || $parent_admin['Admin']['is_auth_area_links_delete']){?>
			<?php echo $html->link('地域リンク集管理','/area_index_links'); ?>&nbsp;
<?php } */ ?>

			<?php echo $html->link("レビュアー管理",'/reviewers'); ?>&nbsp;
			<?php echo $html->link("役職管理",'/m_reviewers_position')?>&nbsp;
			<?php echo $html->link("ビッグバナー管理",'/bigbanner'); ?>&nbsp;
			<?php echo $html->link('ログアウト','/admins/logout'); ?>
			&nbsp; <br /> <br />
			<?php echo $this->Session->flash(); ?>
			<?php echo $content_for_layout; ?>

		</div>

		<div id="footer">
			<?php echo $this->Html->link(
					$this->Html->image('cake.power.gif', array('alt'=> __('CakePHP: the rapid development php framework', true), 'border' => '0')),
					'http://www.cakephp.org/',
					array('target' => '_blank', 'escape' => false)
			);
			?>
		</div>

	</div>

	<?php echo $this->element('sql_dump'); ?>
	<?php //echo $cakeDebug; ?>
</body>
</html>
