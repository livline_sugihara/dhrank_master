<h3>店舗アカウント削除</h3>

<?php echo $form->create(null,array('type'=>'post','action'=>"")); ?>
<?php echo $form->hidden('User.id'); ?>
<table>
	<tr>
		<th>ID</th>
		<td><?php echo $data['User']['id']; ?></td>
	</tr>
	<tr>
		<th>ログインID</th>
		<td><?php echo $data['User']['username']; ?></td>
	</tr>
	<tr>
		<th>店舗名称</th>
		<td><a href="<?php echo $data['LargeArea']['url'] . $data['User']['id'] ?>" target="_blank"><?php echo $data['Shop']['name']; ?></a></td>
	</tr>
	<tr>
		<th>かな</th>
		<td><?php echo $data['User']['kana']; ?></td>
	</tr>
	<tr>
		<th>電話番号</th>
		<td><?php echo $data['Shop']['phone_number']; ?></td>
	</tr>
	<tr>
		<th>メールアドレス</th>
		<td><?php echo $data['Shop']['email']; ?></td>
	</tr>
	<tr>
		<th>地域</th>
		<td><?php echo $data['LargeArea']['name']; ?></td>
	</tr>
	<tr>
		<th>レビュー削除権限</th>
		<td><?php echo $presence[$data['User']['is_review_delete_enabled']] ?></td>
	</tr>
	<tr>
		<th>オフィシャルリンク表示</th>
		<td><?php echo $presence[$data['User']['is_official_link_display']] ?></td>
	</tr>
	<tr>
		<th>サイドバー表示</th>
		<td><?php echo $presence[$data['User']['is_sidebar']] ?></td>
	</tr>
</table>
<?php
$msg = __($data['User']['username'] . ':' . $data['Shop']['name'] . 'を削除します。削除したデータは元に戻りません！よろしいですか？', true);
echo $form->submit(__('削除', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')"));
echo $form->end();
?>