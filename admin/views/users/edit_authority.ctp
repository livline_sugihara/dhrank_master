<h3>店舗アカウント権限編集</h3>

<?php echo $form->create(null,array('type'=>'post','url'=>"/users/edit_authority/" . $data['UsersAuthority']['user_id'] . "/" . $data['UsersAuthority']['large_area_id'])); ?>
<script language=javascript>
function openClose(value){
	if(value == 0)	{
		document.getElementById('isrunningpanel').style.display="none";
	}else{
		document.getElementById('isrunningpanel').style.display="block";
	}
}

window.onload = function() {
	var flagDs = document.getElementById("UserIsRunning").checked;
	openClose(flagDs);
}
</script>

<table>
	<tr>
		<th width="300">ID</th>
		<td>
			<?php echo $data['UsersAuthority']['id']; ?>
			<?php echo $form->hidden('UsersAuthority.id'); ?>
		</td>
	</tr>
	<tr>
		<th>店名</th>
		<td><?php echo $data['Shop']['name']; ?></td>
	</tr>
	<tr>
		<th>エリア名</th>
		<td>
			<?php echo $data['LargeArea']['name']; ?>
			<?php echo $form->hidden('UsersAuthority.large_area_id'); ?>
		</td>
	</tr>
	<tr>
		<th>オフィシャルリンク表示</th>
		<td><?php echo $form->checkbox('UsersAuthority.is_official_link_display'); ?>
		<?php echo $form->error('UsersAuthority.is_official_link_display'); ?></td>
	</tr>

	<tr>
		<th>激押し売れっ子</th>
		<td><?php echo $form->checkbox('UsersAuthority.is_recommend_girl'); ?>
		<?php echo $form->error('UsersAuthority.is_recommend_girl'); ?></td>
	</tr>
	<tr>
		<th>店舗ヘッダー画像表示</th>
		<td><?php echo $form->checkbox('UsersAuthority.is_shop_header_image'); ?>
		<?php echo $form->error('UsersAuthority.is_shop_header_image'); ?></td>
	</tr>
	<tr>
		<th>新着新規掲載店への表示</th>
		<td><?php echo $form->checkbox('UsersAuthority.is_show_new_shop'); ?>
		<?php echo $form->error('UsersAuthority.is_show_new_shop'); ?></td>
	</tr>
	<tr>
		<th>おすすめ店舗への表示</th>
		<td><?php echo $form->checkbox('UsersAuthority.is_show_recommend_shop'); ?>
		<?php echo $form->error('UsersAuthority.is_show_recommend_shop'); ?></td>
	</tr>
	<tr>
		<th>イチオシのお店への表示</th>
		<td><?php echo $form->checkbox('UsersAuthority.is_show_pickup_shop'); ?>
		<?php echo $form->error('UsersAuthority.is_show_pickup_shop'); ?></td>
	</tr>
	<tr>
		<th>カテゴリ画面のバナー表示ON/OFF</th>
		<td><?php echo $form->checkbox('UsersAuthority.is_show_category_banner'); ?>
		<?php echo $form->error('UsersAuthority.is_show_category_banner'); ?></td>
	</tr>
	<tr>
		<th>カテゴリ画面にて全ての項目を表示</th>
		<td><?php echo $form->checkbox('UsersAuthority.is_show_all_contents_by_categorylist'); ?>
		<?php echo $form->error('UsersAuthority.is_show_all_contents_by_categorylist'); ?></td>
	</tr>
	<tr>
		<th>店舗画面にて全ての項目を表示</th>
		<td><?php echo $form->checkbox('UsersAuthority.is_show_all_contents_by_shop'); ?>
		<?php echo $form->error('UsersAuthority.is_show_all_contents_by_shop'); ?></td>
	</tr>
	<tr>
		<th>Pickup口コミの設定</th>
		<td><?php echo $form->checkbox('UsersAuthority.is_pickup_review'); ?>
		<?php echo $form->error('UsersAuthority.is_pickup_review'); ?></td>
	</tr>
	<tr>
		<th>口コミ削除</th>
		<td><?php echo $form->checkbox('UsersAuthority.is_delete_review'); ?>
		<?php echo $form->error('UsersAuthority.is_delete_review'); ?></td>
	</tr>
	<tr>
		<th>掲示板の非表示</th>
		<td><?php echo $form->checkbox('UsersAuthority.is_board_shop'); ?>
		<?php echo $form->error('UsersAuthority.is_board_shop'); ?></td>
	</tr>
	<tr>
		<th>並び順</th>
		<td><?php echo $form->text('UsersAuthority.order_rank'); ?> 番に設定
		<?php echo $form->error('UsersAuthority.order_rank'); ?></td>
	</tr>

</table>
<?php echo $form->end('送信'); ?>

<?php // print_r($data); ?>
