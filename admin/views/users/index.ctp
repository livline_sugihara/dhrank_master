﻿<?php if($parent_admin['Admin']['is_auth_users_edit']){?>
<?php echo $html->link('店舗アカウント登録','/users/add'); ?>
&nbsp;
<?php }?>

<h3>店舗アカウント一覧</h3>

<?php echo $form->create(null, array('type'=>'post', 'action' => '/index/' . $named_order, 'name' => 'UserIndexForm')); ?>

<table>
	<tr style='color: #000066; background-color: #AAAAFF; white-space: nowrap;'>
		<th>
			ID<br /> <a href='#' onclick='UserIndexForm.action="/users/index/order:id_asc";UserIndexForm.submit();return false;'>▲</a><a href='#'
				onclick='UserIndexForm.action="/users/index/order:id_desc";UserIndexForm.submit();return false;'>▼</a>
		</th>
<?php if($parent_admin['Admin']['is_auth_users_edit']){?>
		<th colspan="2">編集</th>
<?php }?>
		<th>
			<?php echo $form->select('LargeArea.id', $large_area,null,array('empty'=>'選択','onchange'=>'return this.form.submit();')); ?>
		</th>
		<th>ログインID</th>
		<th>店舗名称</th>
		<th>
			かな<br /> <a href='#' onclick='UserIndexForm.action="/users/index/order:kana_asc";UserIndexForm.submit();return false;'>▲</a><a href='#'
				onclick='UserIndexForm.action="/users/index/order:kana_desc";UserIndexForm.submit();return false;'>▼</a>
		</th>
		<th>電話番号</th>
		<th>メールアドレス</th>
<?php /*
		<th>
			レビュー<br />削除権限
		</th>
		<th>
			オフィシャル<br />リンク表示
		</th>
		<th>
			サイド<br />バー表示
		</th>
*/ ?>
		<?php if($parent_admin['Admin']['is_auth_users_changepassword']){?>
		<th>パスワード変更</th>
		<?php }?>
		<?php if($parent_admin['Admin']['is_auth_users_delete']){?>
		<th>削除</th>
		<?php }?>
	</tr>

	<?php foreach($list as $record){ ?>
	<tr style='color: #000099; background-color: #DDDDFF'>
		<td nowrap>
			<?php echo $record['User']['id'] ?>
		</td>
<?php if($parent_admin['Admin']['is_auth_users_edit']){?>
		<td nowrap>
			<a href="/users/edit/<?php echo $record['User']['id'] ?>">基本</a>
		</td>
		<td nowrap>
			<a href="/users/edit_authority/<?php echo $record['User']['id'] ?>/<?php echo  $record['UsersAuthority']['large_area_id'] ?>">権限</a>
		</td>
<?php }?>
		<td nowrap>
			<?php echo $record['LargeArea']['name'] ?>
		</td>
		<td nowrap>
			<a href="<?php echo $record['LargeArea']['url'] . $record['User']['id'] ?>" target="_blank"><?php echo $record['User']['username'] ?> </a>
		</td>
		<td nowrap>
			<?php echo $record['Shop']['name'] ?>
		</td>
		<td nowrap>
			<?php echo $record['User']['kana'] ?>
		</td>
		<td nowrap>
			<?php echo $record['Shop']['phone_number'] ?>
		</td>
		<td nowrap>
			<?php echo $record['Shop']['email'] ?>
		</td>
<?php /*
		<td nowrap>
			<?php echo $presence[$record['User']['is_review_delete_enabled']] ?>
		</td>
		<td nowrap>
			<?php echo $presence[$record['User']['is_official_link_display']] ?>
		</td>
		<td nowrap>
			<?php echo $presence[$record['User']['is_sidebar']] ?>
		</td>
*/ ?>
		<?php if($parent_admin['Admin']['is_auth_users_changepassword']){?>
		<td nowrap>
			<a href="/users/changepassword/<?php echo $record['User']['id'] ?>">パスワード変更</a>
		</td>
		<?php }?>
		<?php if($parent_admin['Admin']['is_auth_users_delete']){?>
		<td nowrap>
			<a href="/users/delete/<?php echo $record['User']['id'] ?>">削除</a>
		</td>
		<?php }?>
	</tr>
	<?php } ?>

</table>
<?php echo $form->end(); ?>