<h3>店舗アカウント登録が完了しました</h3>

<table>
	<tr>
		<th>ログインID</th>
		<td><?php echo $data['User']['username']; ?></td>
	</tr>
	<tr>
		<th>パスワード</th>
		<td><?php echo $data['User']['password']; ?></td>
	</tr>
	<tr>
		<th>店名</th>
		<td><?php echo $data['Shop']['name']; ?></td>
	</tr>
	<tr>
		<th>かな</th>
		<td><?php echo $data['User']['kana']; ?></td>
	</tr>
	<tr>
		<th>電話番号</th>
		<td><?php echo $data['Shop']['phone_number']; ?></td>
	</tr>
	<tr>
		<th>メールアドレス</th>
		<td><?php echo $data['Shop']['email']; ?></td>
	</tr>
	<tr>
		<th>メインで活動するエリア</th>
		<td><?php echo $large_area['LargeArea']['name']; ?></td>
	</tr>
	<tr>
		<th>登録サイト</th>
		<td>
			<ul>
<?php foreach($register_large_area AS $ii => $record) { ?>
				<li><?php echo $record['LargeArea']['name']; ?></li>
<?php } ?>
			</ul>
			<?php //echo $html->link($large_area['LargeArea']['url'],$large_area['LargeArea']['url'],array('target'=>'_blank'),'',''); ?>
		</td>
	</tr>
<?php /*
	<tr>
		<th>レビュー削除権限</th>
		<td><?php echo $is_review_delete_enabled; ?></td>
	</tr>
	<tr>
		<th>オフィシャルリンク表示</th>
		<td><?php echo $is_official_link_display; ?></td>
	</tr>
	<tr>
		<th>サイドバー表示</th>
		<td><?php echo $is_sidebar; ?></td>
	</tr>
*/ ?>
</table>

<a href="/users">※一覧に戻る</a>
