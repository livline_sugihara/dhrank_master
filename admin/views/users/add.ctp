
<h3>店舗アカウント登録</h3>

<style>
p#area_list {
	margin:5px 0 0 10px;
	width: 100%;
}
p#area_list label {
	padding-right:0.3em;
	margin-right: 1em;
	display: block;
	float: left;
	width: 10em;
}

</style>

<?php echo $form->create(null, array('type'=>'post', 'action' => "add")); ?>

<table>
	<tr>
		<th width="300">ログインID</th>
		<td><?php echo $form->text('User.username'); ?>
		<?php echo $form->error('User.username'); ?></td>
	</tr>
	<tr>
		<th>パスワード</th>
		<td><?php echo $form->password('User.password'); ?>
		<?php echo $form->error('User.password'); ?></td>
	</tr>
	<tr>
		<th>店名</th>
		<td><?php echo $form->text('Shop.name'); ?>
		<?php echo $form->error('Shop.name'); ?></td>
	</tr>
	<tr>
		<th>かな</th>
		<td><?php echo $form->text('User.kana'); ?>
		<?php echo $form->error('User.kana'); ?></td>
	</tr>
	<tr>
		<th>電話番号</th>
		<td><?php echo $form->text('Shop.phone_number'); ?>
		<?php echo $form->error('Shop.phone_number'); ?></td>
	</tr>
	<tr>
		<th>メールアドレス</th>
		<td><?php echo $form->text('Shop.email'); ?>
		<?php echo $form->error('Shop.email'); ?></td>
	</tr>
	<tr>
		<th>メインで活動するエリア</th>
		<td><?php echo $form->select('User.large_area_id', $large_areas,null,array('empty'=>'選んでください')); ?>
		<?php echo $form->error('User.large_area_id'); ?></td>
	</tr>
	<tr>
		<th>大エリア</th>
		<td>
			<p id="area_list">
<?php
$separator = "\n";
$tmp_out = explode("\n", $form->input('UsersAuthority.large_area_id', array('type'=>'select', 'multiple' => 'checkbox', 'options' => $large_areas)));
$out = '';
//調整
foreach ($tmp_out as $key => $line) {
	$line = trim($line);
	if (!$line || $key == 0) {
		continue;
	}
	if ($key > 0) {
		$line = str_replace(array('<div class="checkbox">', '</div>'), '', $line);
		$line = preg_replace(array('@<label.*?>@', '@</label>@'), '', $line);
		$line = '<label>' . $line . '</label>';
	}
	$out[] = $line;
}
echo implode($separator, $out);
?>
			</p>
		</td>
	</tr>
<?php /*
	<tr>
		<th>レビュー削除権限</th>
		<td><?php echo $form->checkbox('User.is_review_delete_enabled'); ?></td>
	</tr>
	<tr>
		<th>オフィシャルリンク表示</th>
		<td><?php echo $form->checkbox('User.is_official_link_display'); ?></td>
	</tr>
	<tr>
		<th>サイドバー表示</th>
		<td><?php echo $form->checkbox('User.is_sidebar'); ?></td>
	</tr>
*/ ?>
</table>

<?php echo $form->end('店舗アカウント登録'); ?>
