<h3>店舗アカウントパスワード変更</h3>

<?php echo $form->create(null,array('type'=>'post','action'=>"changepassword/" . $data['User']['id'])); ?>
<?php echo $form->hidden('User.id'); ?>
<?php echo $form->hidden('User.password'); ?>
<table>
	<tr>
		<th>旧パスワード</th>
		<td><?php echo $form->password('User.old_password'); ?><br />
		<?php echo $form->error('User.old_password'); ?></td>
	</tr>
	<tr>
		<th>新パスワード</th>
		<td><?php echo $form->password('User.new_password'); ?><br />
		<?php echo $form->error('User.new_password'); ?></td>
	</tr>
	<tr>
		<th>新パスワード(確認)</th>
		<td><?php echo $form->password('User.new_password_confirm'); ?><br />
		<?php echo $form->error('User.new_password_confirm'); ?></td>
	</tr>
</table>
<?php echo $form->end('変更'); ?>