<h3>口コミ非掲載確認画面</h3>
<table>
	<tr>
		<th>大エリア</th>
		<td>
			<?php echo $data['LargeArea']['name'] ?>
		</td>
	</tr>
	<tr>
		<th>投稿日</th>
		<td>
			<?php echo $data['Review']['created']; ?>
		</td>
	</tr>
	<tr>
		<th>投稿者</th>
		<td>
			<?php if(!empty($data['Reviewer']['id'])){?>
			<?php echo $data['Reviewer']['handle']?>
			<?php }else{?>
			<?php echo $data['Review']['post_name'] ?>
			<?php }?>
		</td>
	</tr>
	<tr>
		<th>店名</th>
		<td>
			<?php echo $data['Shop']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>女の子名</th>
		<td>
			<?php echo $data['Girl']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>通報種別</th>
		<td>
			<?php echo $data['MReviewsReportsType']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>通報理由</th>
		<td>
			<?php echo $data['ReviewsReport']['comment']; ?>
		</td>
	</tr>
	<tr>
		<th>レビュー</th>
		<td>
			<?php echo $data['Review']['comment']; ?>
		</td>
	</tr>
	<tr>
		<th>媒体</th>
		<td>
			<?php echo $data['Review']['media']; ?>
		</td>
	</tr>
	<tr>
		<th>IPアドレス</th>
		<td>
			<?php echo $data['Review']['ip_address']; ?>
		</td>
	</tr>
	<tr>
		<th>個体識別番号</th>
		<td>
			<?php echo $data['Review']['uid']; ?>
		</td>
	</tr>
</table>
<?php
echo $form->create(null,array('type'=>'post','action'=>'./finish/' . $data['Review']['id']));
if ($data['Review']['not_publish'] == "0") {
	echo $form->submit(__('非掲載にする', true), array('name'=>'not_publish'));
}else{
	echo $form->submit(__('非掲載にしない', true), array('name'=>'not_publish'));
}
echo $form->end();
?>
