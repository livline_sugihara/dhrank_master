<h3>レビュアー情報一覧</h3>
<style>
table tr th {
	text-align: center;
}
td.align-center {
	text-align: center;
}
td.align-right {
	text-align: right;
}
</style>

<table>
	<tr style='color: #000066; background-color: #AAAAFF'>
		<th>ID</th>
		<th>レビュー内容a</th>
		<th>通報コメント</th>
		<th>通知理由</th>
		<th>非掲載</th>
	</tr>
	<?php foreach($review_reports_data as $key => $val) { ?>
	<tr style='color: #000099; background-color: #DDDDFF'>
		<td class="align-center">
			<?php echo $val['ReviewsReport']['id']; ?>
		</td>
		<td>
			<?php echo nl2br($val['Review']['comment']); ?>
		</td>
		<td>
			<?php echo nl2br($val['ReviewsReport']['comment']); ?>
		</td>
		<td>
			<?php echo $val['MReviewsReportsType']['name']?><br />
		</td>
		<td>
			<?php
				switch($val['Review']['not_publish'])
				{
					case "0": // 非掲載にする
						echo '<a href="/reviews_report/confirm/'.$val['Review']['id'].'">する</a>';
						break;
					case "1": // 非掲載にしない
						echo '<a href="/reviews_report/confirm/'.$val['Review']['id'].'">しない</a>';
						break;
				}
			?>
			<br />
		</td>
	</tr>
	<?php } ?>
</table>
