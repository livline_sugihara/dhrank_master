
<h3>大エリア削除</h3>

<?php echo $form->create(null,array('type'=>'post','action'=>"")); ?>
<?php echo $form->hidden('LargeArea.id'); ?>
<table>
	<tr>
		<th width="300">ID</th>
		<td><?php echo $data['LargeArea']['id']; ?></td>
	</tr>
	<tr>
		<th>大エリア名</th>
		<td><?php echo $data['LargeArea']['name']; ?></td>
	</tr>
	<tr>
		<th>URL</th>
		<td><?php echo $data['LargeArea']['url']; ?></td>
	</tr>
	<tr>
		<th>リンク集表示順</th>
		<td><?php echo $link_order[$data['LargeArea']['link_order_index']] ?></td>
	</tr>
	<tr>
		<th>フッター地域リンク表示</th>
		<td><?php echo $presence[$data['LargeArea']['is_display_footer']] ?></td>
	</tr>
</table>
<?php
$msg = __($data['LargeArea']['name'] . 'を削除します。よろしいですか？', true);
echo $form->submit(__('削除', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')"));
echo $form->end();
?>
