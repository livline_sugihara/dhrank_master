<h3>大エリアフッターリンク表示順位編集</h3>

<?php echo $form->create(null, array('type'=>'post', 'action' => '')); ?>
<table class="normal">
	<tr>
		<th>大エリア</th>
		<th>URL</th>
		<th>表示順</th>
	</tr>
	<?php foreach($list as $key => $record){?>
	<tr>
		<td>
			<?php echo $record['LargeArea']['name'] ?>
		</td>
		<td>
			<?php //start---2013/3/7 障害No.1-1-0010修正 ?>
			<?php //echo $record['LargeArea']['url']; ?>
			<a href="<?php echo $record['LargeArea']['url']; ?>" target="_blank"><?php echo $record['LargeArea']['url']; ?></a>
			<?php //end---2013/3/7 障害No.1-1-0010修正 ?>
		</td>
		<td>
			<?php echo $form->select('LargeArea.' . $key . '.show_order', $show_order,$record['LargeArea']['show_order'],array('empty'=>'選択してください')); ?>
			<?php echo $form->hidden('LargeArea.' . $key . '.id', array('value' => $record['LargeArea']['id']));?> 番目に表示
		</td>
	</tr>
	<?php } ?>
</table>
<?php echo $form->end('表示順位変更'); ?>

