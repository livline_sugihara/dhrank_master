
<h3>大エリア登録</h3>

<?php echo $form->create(null, array('type'=>'post', 'action' => "", 'enctype' => 'multipart/form-data')); ?>
<table>
	<tr>
		<th width="300">大エリア名</th>
		<td><?php echo $form->text('LargeArea.name'); ?>
		<?php echo $form->error('LargeArea.name'); ?></td>
	</tr>
	<tr>
		<th>URL<br />※最後は"/"(スラッシュ)で終わること</th>
		<td><?php echo $form->text('LargeArea.url'); ?>
		<?php echo $form->error('LargeArea.url'); ?></td>
	</tr>
<?php /*
	<tr>
		<th>リンク集表示順</th>
		<td><?php echo $form->select('LargeArea.link_order_index',$link_order,null,array('empty'=>'選んでください')); ?>
		<?php echo $form->error('LargeArea.link_order_index'); ?></td>
	</tr>
	<tr>
		<th>フッター地域リンク表示</th>
		<td><?php echo $form->select('LargeArea.is_display_footer',$presence,null,array('empty'=>'選んでください')); ?>
		<?php echo $form->error('LargeArea.is_display_footer'); ?></td>
	</tr>
*/ ?>
	<tr>
		<th>表示グレード</th>
		<td><?php echo $form->select('LargeArea.display_grade', $display_grade, null, array('empty' => false)); ?>
		<?php echo $form->error('LargeArea.display_grade'); ?></td>
	</tr>
	<tr>
		<th width="300">地域バナー</th>
		<td>
<?php if(!empty($data['LargeArea']['area_banner_name'])) { ?>
			<img src="<?php echo APP_IMG_URL . 'banner/area_banner/' . $data['LargeArea']['area_banner_name'] ?>?time=<?php echo date('YmdHis'); ?>" /><br />
<?php } ?>
			<?php echo $form->file('LargeArea.area_banner_name_add'); ?>
			<?php echo $form->error('LargeArea.area_banner_name_add'); ?>
		</td>
	</tr>
	<tr>
		<th width="300">地域バナーリンクURL</th>
		<td><?php echo $form->text('LargeArea.area_banner_url'); ?>
		<?php echo $form->error('LargeArea.area_banner_url'); ?></td>
	</tr>

</table>
<?php echo $form->end('大エリア登録'); ?>
