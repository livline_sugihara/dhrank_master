<?php echo $html->link('大エリア登録','/large_areas/add'); ?>&nbsp;
<?php // echo $html->link('フッターリンク表示順位編集','/large_areas/change_link_order'); ?>
&nbsp;
<br /><br />
<h3>大エリア一覧</h3>

<style>
table tr th {
	text-align: center;
}

td.align-center {
	text-align: center;
}
td.align-right {
	text-align: right;
}
</style>

<table>
	<tr style='color: #000066; background-color: #AAAAFF'>
		<th>ID</th>
		<th>大エリア名</th>
		<th>基本URL</th>
		<th>表示グレード</th>
		<th>地域バナー</th>
		<th>地域バナーURL</th>
		<th>編集</th>
		<th>削除</th>
	</tr>

<?php foreach($list as $record){ ?>
	<tr style='color: #000099; background-color: #DDDDFF'>
		<td>
			<?php echo $record['LargeArea']['id'] ?>
		</td>
		<td>
			<?php echo $record['LargeArea']['name'] ?>
		</td>
		<td>
			<a href="<?php echo $record['LargeArea']['url'] ?>" target="_blank">
				<?php echo $record['LargeArea']['url'] ?>
			</a>
		</td>
		<td>
			<?php echo $record['LargeArea']['display_grade']; ?>
		</td>
		<td>
<?php
$filePath = APP_IMG_URL . 'banner/area_banner/' . $record['LargeArea']['area_banner_name'];
if(!empty($record['LargeArea']['area_banner_name'])) {
?>
			<img src="<?php echo $filePath; ?>?time=<?php echo date('YmdHis'); ?>" width="150" height="50" />
<?php } ?>
		</td>
		<td>
			<?php echo $record['LargeArea']['area_banner_url']; ?>
		</td>
		<td class="align-center">
			<a href="/large_areas/edit/<?php echo $record['LargeArea']['id'] ?>">編集</a>
		</td>
		<td class="align-center">
	<?php if(empty($count_user[$record['LargeArea']['id']]) && empty($count_small_area[$record['LargeArea']['id']])){ ?>
			<a href="/large_areas/delete/<?php echo $record['LargeArea']['id'] ?>">削除</a>
	<?php } else { ?>
			店舗登録有
	<?php } ?>
		</td>
	</tr>
<?php } ?>
</table>
