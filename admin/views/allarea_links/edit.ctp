<h3>全国リンク集編集</h3>

<?php echo $form->create(null, array('type'=>'post','enctype' => 'multipart/form-data', 'action' => 'edit/' . $data['AllareaLink']['id'])); ?>
<?php echo $form->hidden('AllareaLink.id'); ?>
<?php echo $form->hidden('AllareaLink.banner'); ?>
<table>
	<tr>
		<th width="300">サイト名</th>
		<td>
			<?php echo $form->text('AllareaLink.name'); ?>
			<?php echo $form->error('AllareaLink.name'); ?>
		</td>
	</tr>
	<tr>
		<th>URL</th>
		<td>
			<?php echo $form->text('AllareaLink.url'); ?>
			<?php echo $form->error('AllareaLink.url'); ?>
		</td>
	</tr>
	<tr>
		<th>バナー画像</th>
		<td>
			<img src="<?php echo APP_IMG_URL . 'link/_allarea/' . $data['AllareaLink']['banner'] ?>" /><br />
			<?php echo $form->file('AllareaLink.banner_edit');?>
			<?php echo $form->error('AllareaLink.banner_edit'); ?>
		</td>
	</tr>
</table>
<?php echo $form->end('送信'); ?>
