
<h3>全国リンク集登録</h3>

<?php echo $form->create(null, array('type'=>'post','enctype' => 'multipart/form-data', 'action' => "")); ?>
<table>
	<tr>
		<th width="300">サイト名</th>
		<td>
			<?php echo $form->text('AllareaLink.name'); ?>
			<?php echo $form->error('AllareaLink.name'); ?>
		</td>
	</tr>
	<tr>
		<th>URL</th>
		<td>
			<?php echo $form->text('AllareaLink.url'); ?>
			<?php echo $form->error('AllareaLink.url'); ?>
		</td>
	</tr>
	<tr>
		<th>バナー画像</th>
		<td>
			<?php echo $form->file('AllareaLink.banner_add');?>
			<?php echo $form->error('AllareaLink.banner_add'); ?>
		</td>
	</tr>
</table>
<?php echo $form->end('全国リンク集登録'); ?>
