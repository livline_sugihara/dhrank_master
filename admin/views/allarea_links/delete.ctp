
<h3>全国リンク集削除</h3>

<?php echo $form->create(null,array('type'=>'post','action'=>"")); ?>
<?php echo $form->hidden('AllareaLink.id'); ?>
<?php echo $form->hidden('AllareaLink.banner'); ?>
<table>
	<tr>
		<th width="300">ID</th>
		<td>
			<?php echo $data['AllareaLink']['id']; ?>
		</td>
	</tr>
	<tr>
		<th>サイト名</th>
		<td>
			<?php echo $data['AllareaLink']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>URL</th>
		<td>
			<?php //start---2013/3/7 障害No.1-1-0010修正 ?>
			<?php //echo $data['AllareaLink']['url']; ?>
			<a href="<?php echo $data['AllareaLink']['url']; ?>" target="_blank"><?php echo $data['AllareaLink']['url']; ?></a>
			<?php //end---2013/3/7 障害No.1-1-0010修正 ?>
		</td>
	</tr>
	<tr>
		<th>バナー</th>
		<td>
			<img src="<?php echo APP_IMG_URL . 'link/_allarea/' . $data['AllareaLink']['banner'] ?>" />
		</td>
	</tr>
</table>
<?php
$msg = __($data['AllareaLink']['name'] . 'を削除します。よろしいですか？', true);
echo $form->submit(__('削除', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')"));
echo $form->end();
?>
