<h3>全国リンク集表示順位編集</h3>

<?php echo $form->create(null, array('type'=>'post', 'action' => '')); ?>
<table class="normal">
	<tr>
		<th>サイト名</th>
		<th>URL</th>
		<th>表示順</th>
	</tr>
	<?php foreach($list as $key => $record){?>
	<tr>
		<td>
			<?php echo $record['AllareaLink']['name'] ?>
		</td>
		<td>
			<?php //start---2013/3/7 障害No.1-1-0010修正 ?>
			<?php //echo $record['AllareaLink']['url']; ?>
			<a href="<?php echo $record['AllareaLink']['url']; ?>" target="_blank"><?php echo $record['AllareaLink']['url']; ?></a>
			<?php //end---2013/3/7 障害No.1-1-0010修正 ?>
		</td>
		<td>
			<?php echo $form->select('AllareaLink.' . $key . '.show_order', $show_order,$record['AllareaLink']['show_order'],array('empty'=>'選択してください')); ?>
			<?php echo $form->hidden('AllareaLink.' . $key . '.id', array('value' => $record['AllareaLink']['id']));?> 番目に表示
		</td>
	</tr>
	<?php } ?>
</table>
<?php echo $form->end('表示順位変更'); ?>

