<?php echo $html->link('登録','/allarea_links/add'); ?>
&nbsp;
<?php echo $html->link('表示順位編集','/allarea_links/change_link_order'); ?>
&nbsp;
<?php echo $html->link('全体表示順編集','/allarea_link_orders'); ?><?php echo '(全体表示順：' . $link_order[$allarea_link_order['Meta']['value']] . ')'?>
&nbsp;

<h3>全国リンク集一覧</h3>

<table>
	<tr style='color: #000066; background-color: #AAAAFF'>
		<th>ID</th>
		<th>サイト名</th>
		<th>URL</th>
		<th>バナー</th>
		<th>編集</th>
		<th>削除</th>
	</tr>

	<?php foreach($list as $record){ ?>
	<tr style='color: #000099; background-color: #DDDDFF'>
		<td>
			<?php echo $record['AllareaLink']['id'] ?>
		</td>
		<td>
			<?php echo $record['AllareaLink']['name'] ?>
		</td>
		<td>
			<a href="<?php echo $record['AllareaLink']['url'] ?>" target="_blank"><?php echo $record['AllareaLink']['url'] ?> </a>
		</td>
		<td>
			<img src="<?php echo APP_IMG_URL . 'link/_allarea/' . $record['AllareaLink']['banner'] ?>" />
		</td>
		<td>
			<a href="/allarea_links/edit/<?php echo $record['AllareaLink']['id'] ?>">編集</a>
		</td>
		<td>
			<a href="/allarea_links/delete/<?php echo $record['AllareaLink']['id'] ?>">削除</a>
		</td>
	</tr>
	<?php } ?>

</table>
