<h3>役職管理一覧画面</h3>
<style>
table tr  {
	text-align: center;
}
td.align-center {
	text-align: center;
}
td.align-right {
	text-align: right;
}
</style>
<table>
	<tr style='color: #000066; background-color: #AAAAFF'>
		<th>
			ID
		</th>
		<th>
			役職名
		</th>
		<th>
			口コミ最低必要数
		</th>
		<th>
			変更
		</th>
	</tr>
<?php
	foreach($data as $key => $val){
		echo '<tr>';
		echo '<td>'.$val['MReviewersPosition']['id'].'</td>';
		echo '<td>'.$val['MReviewersPosition']['name'].'</td>';
		echo '<td>'.$val['MReviewersPosition']['reviews_count'].'</td>';
		echo '<td><a href="/m_reviewers_position/confirm/'.$val['MReviewersPosition']['id'].'">変更する</a></td>';
		echo '</tr>';
	}
?>
</table>
