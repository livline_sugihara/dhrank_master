
<h3>役職管理変更画面</h3>
<br />
<h4>役職の変更を行います</h4>
<style>
table tr  {
	text-align: center;
}
td.align-center {
	text-align: center;
}
td.align-right {
	text-align: right;
}
span.errmsg {
	color: #ff0000;
}


</style>
<form action="/m_reviewers_position/finish/" method="POST">
<table>
	<tr style='color: #000066; background-color: #AAAAFF'>
		<th>
			ID
		</th>
		<th>
			役職名
		</th>
		<th>
			口コミ最低必要数
		</th>
	</tr>
<?php
	if (!isset($errmsg))
	{
		foreach($data as $key => $val){
			echo '<tr>';
			echo '<td>'.$val['id'].'</td>';
			echo '<td><input type="text" name="name" value="'.$val['name'].'" /></td>';
			echo '<td><input type="text" name="reviews_count" value="'.$val['reviews_count'].'" /></td>';
			echo '</tr>';
			echo '<input type="hidden" name="id" value="'. $id . '" />';
		}
	}
	else
	{
		echo '<span class="errmsg">'.implode('<br />', $errmsg).'</span>';
		echo '<tr>';
		echo '<td>'.$form['id'].'</td>';
		echo '<td><input type="text" name="name" value="'.$form['name'].'" /></td>';
		echo '<td><input type="text" name="reviews_count" value="'.$form['reviews_count'].'" /></td>';
		echo '</tr>';
		echo '<input type="hidden" name="id" value="'. $form['id'] . '" />';
	}
?>
</table>
<input type="submit" name="submit" value="変更する" />
</form>
