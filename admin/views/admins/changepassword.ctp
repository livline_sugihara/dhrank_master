<h3>総合管理者アカウントパスワード変更</h3>

<?php echo $form->create(null,array('type'=>'post','action'=>'')); ?>
<?php echo $form->hidden('Admin.id'); ?>
<?php echo $form->hidden('Admin.password'); ?>
<table>
	<tr>
		<th>旧パスワード</th>
		<td><?php echo $form->password('Admin.old_password'); ?><br />
		<?php echo $form->error('Admin.old_password'); ?></td>
	</tr>
	<tr>
		<th>新パスワード</th>
		<td><?php echo $form->password('Admin.new_password'); ?><br />
		<?php echo $form->error('Admin.new_password'); ?></td>
	</tr>
	<tr>
		<th>新パスワード(確認)</th>
		<td><?php echo $form->password('Admin.new_password_confirm'); ?><br />
		<?php echo $form->error('Admin.new_password_confirm'); ?></td>
	</tr>
</table>
<?php echo $form->end('変更'); ?>