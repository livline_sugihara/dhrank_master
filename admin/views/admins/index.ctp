<?php echo $html->link('総合管理者アカウント登録','/admins/add'); ?>&nbsp;

<h3>総合管理者アカウント一覧</h3>

	<?php echo $form->create(null, array('type'=>'post', 'action' => '')); ?>

	<table>
		<tr style='color: #000066; background-color: #AAAAFF; white-space: nowrap;'>
			<th rowspan="2">ID</th>
			<th rowspan="2">ログインID</th>
			<th rowspan="2">総合管理者アカ<br />ウント管理権限</th>
			<th colspan="3">店舗アカウン<br />ト管理権限</th>
			<th rowspan="2">大エリア<br />管理権限</th>
			<th rowspan="2">全国リンク<br />集管理権限</th>
			<th colspan="2">地域リンク<br />集管理権限</th>
			<th colspan="2">口コミ<br />管理権限</th>
			<th colspan="2">掲示板<br />管理権限</th>
			<th rowspan="2">地域別<br />管理権限</th>
			<!-- <th rowspan="2">カテゴリバナー管理権限</th>			 -->
			<th rowspan="2">編集</th>
			<th rowspan="2">パスワード変更</th>
			<th rowspan="2">削除</th>
		</tr>
		<tr style='color: #000066; background-color: #AAAAFF; white-space: nowrap;'>
			<th>編集</th>
			<th>削除</th>
			<th>パスワード変更</th>
			<th>編集</th>
			<th>削除</th>
			<th>編集</th>
			<th>削除</th>
			<th>編集</th>
			<th>削除</th>
		</tr>

	<?php foreach($list as $record){ ?>
	<tr style='color: #000099; background-color: #DDDDFF'>
		<td nowrap><?php echo $record['Admin']['id'] ?></td>
		<td nowrap><?php echo $record['Admin']['username'] ?></td>
		<td nowrap><?php echo $presence[$record['Admin']['is_auth_admins']] ?></td>
		<td nowrap><?php echo $presence[$record['Admin']['is_auth_users_edit']] ?></td>
		<td nowrap><?php echo $presence[$record['Admin']['is_auth_users_delete']] ?></td>
		<td nowrap><?php echo $presence[$record['Admin']['is_auth_users_changepassword']] ?></td>
		<td nowrap><?php echo $presence[$record['Admin']['is_auth_large_areas']] ?></td>
		<td nowrap><?php echo $presence[$record['Admin']['is_auth_allarea_links']] ?></td>
		<td nowrap><?php echo $presence[$record['Admin']['is_auth_area_links_edit']] ?></td>
		<td nowrap><?php echo $presence[$record['Admin']['is_auth_area_links_delete']] ?></td>
		<td nowrap><?php echo $presence[$record['Admin']['is_auth_reviews_edit']] ?></td>
		<td nowrap><?php echo $presence[$record['Admin']['is_auth_reviews_delete']] ?></td>
		<td nowrap><?php echo $presence[$record['Admin']['is_auth_boards_edit']] ?></td>

		<td nowrap><?php echo $presence[$record['Admin']['is_auth_boards_delete']] ?></td>
		<td nowrap>
		<?php if($large_area_count == $record[0]['count']){?>
		全エリア
		<?php }else{?>
			<?php foreach($record['AdminsAuthLargeArea'] as $key => $admins_auth_large_area){
				if($admins_auth_large_area['selected'] == true){
					$ret =  Set::extract('/LargeArea[id=' . $admins_auth_large_area['large_area_id'] . ']/name', $large_areas);
					echo (!empty($ret[0]) ? $ret[0] . '&nbsp;' : '');
				}
			}?>
		<?php }?>
		</td>
		<!-- <td nowrap><?php echo $presence[$record['Admin']['is_auth_banner_toggle']] ?></td>		 -->
		<td nowrap><a href="/admins/edit/<?php echo $record['Admin']['id'] ?>">編集</a></td>
		<td nowrap><a href="/admins/changepassword/<?php echo $record['Admin']['id'] ?>">パスワード変更</a></td>
		<td nowrap>
			<?php //start---2013/3/1 障害No.1-1-0001修正 ?>
			<?php //if($record['Admin']['username'] != 'admin'){?>
			<?php if($record['Admin']['username'] != 'admin' && $record['Admin']['username'] != $parent_admin['Admin']['username']){?>
			<?php //end---2013/3/1 障害No.1-1-0001修正 ?>
				<a href="/admins/delete/<?php echo $record['Admin']['id'] ?>">削除</a>
			<?php }?>
		</td>
	</tr>
	<?php } ?>

</table>
<?php echo $form->end(); ?>
