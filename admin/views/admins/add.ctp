
<h3>総合管理者アカウント登録</h3>

<?php echo $form->create(null, array('type'=>'post', 'action' => '', 'name' => 'adminform')); ?>

<table>
	<tr>
		<th colspan="2">ログインID</th>
		<td><?php echo $form->text('Admin.username'); ?>
		<?php echo $form->error('Admin.username'); ?></td>
	</tr>
	<tr>
		<th colspan="2">パスワード</th>
		<td><?php echo $form->password('Admin.password_confirm'); ?>
		<?php echo $form->error('Admin.password_confirm'); ?></td>
	</tr>
	<tr>
		<th colspan="2">総合管理者アカウント管理権限</th>
		<td><?php echo $form->checkbox('Admin.is_auth_admins'); ?></td>
	</tr>
	<tr>
		<th rowspan="3" width="150">店舗アカウント管理権限</th>
		<th width="150">編集</th>
		<td><?php echo $form->checkbox('Admin.is_auth_users_edit'); ?></td>
	</tr>
	<tr>
		<th>削除</th>
		<td><?php echo $form->checkbox('Admin.is_auth_users_delete'); ?></td>
	</tr>
	<tr>
		<th>パスワード変更</th>
		<td><?php echo $form->checkbox('Admin.is_auth_users_changepassword'); ?></td>
	</tr>
	<tr>
		<th colspan="2">大エリア管理権限</th>
		<td><?php echo $form->checkbox('Admin.is_auth_large_areas'); ?></td>
	</tr>
	<tr>
		<th colspan="2">全国リンク集管理権限</th>
		<td><?php echo $form->checkbox('Admin.is_auth_allarea_links'); ?></td>
	</tr>
	<tr>
		<th rowspan="2">地域リンク集管理権限</th>
		<th>編集</th>
		<td><?php echo $form->checkbox('Admin.is_auth_area_links_edit'); ?></td>
	</tr>
	<tr>
		<th>削除</th>
		<td><?php echo $form->checkbox('Admin.is_auth_area_links_delete'); ?></td>
	</tr>
	<tr>
		<th rowspan="2">口コミ管理権限</th>
		<th>編集</th>
		<td><?php echo $form->checkbox('Admin.is_auth_reviews_edit'); ?></td>
	</tr>
	<tr>
		<th>削除</th>
		<td><?php echo $form->checkbox('Admin.is_auth_reviews_delete'); ?></td>
	</tr>
	<tr>
		<th rowspan="2">掲示板管理権限</th>
		<th>編集</th>
		<td><?php echo $form->checkbox('Admin.is_auth_boards_edit'); ?></td>
	</tr>
	<tr>
		<th>削除</th>
		<td><?php echo $form->checkbox('Admin.is_auth_boards_delete'); ?></td>
	</tr>
	<tr>
		<th colspan="2">地域管理権限</th>
		<td>
			<input type="checkbox" name="all_checked" onClick="AllChecked();" /><label>全選択</label>

<script language="JavaScript">
function AllChecked() {
	var check =  document.adminform.all_checked.checked;
    var inputs = document.getElementsByTagName("input");
    for (var i = 0, l = inputs.length; i < l; i++) {
        var input = inputs[i];
        if (input.type == "checkbox" && input.name != "all_checked" && input.name.match(/AdminsAuthLargeArea/i))
            input.checked = check;
    }
}
</script>
			<hr />
			<?php foreach($large_areas as $key => $record){
				$ret =  Set::extract('/AdminsAuthLargeArea[large_area_id=' . $record['LargeArea']['id'] . ']', $data);
				echo $form->hidden('AdminsAuthLargeArea.' . $key . '.id', array('value' => (!empty($ret[0]) ? $ret[0]['AdminsAuthLargeArea']['id'] : null)));
				echo $form->hidden('AdminsAuthLargeArea.' . $key . '.large_area_id', array('value' => $record['LargeArea']['id']));?>
				<?php echo $form->checkbox('AdminsAuthLargeArea.' . $key . '.selected', array('checked' => (!empty($ret[0]) ? $ret[0]['AdminsAuthLargeArea']['selected'] : false))); ?>
				<label><?php echo $record['LargeArea']['name'];?></label>
			<?php }?>
		</td>
	</tr>
</table>

<?php echo $form->end('総合管理者アカウント登録'); ?>
