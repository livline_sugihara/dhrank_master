<h3>店舗アカウント削除</h3>

<?php echo $form->create(null,array('type'=>'post','action'=>"")); ?>
<?php echo $form->hidden('Admin.id'); ?>
<table>
	<tr>
		<th colspan="2">ID</th>
		<td><?php echo $data['Admin']['id']; ?></td>
	</tr>
	<tr>
		<th colspan="2">ログインID</th>
		<td><?php echo $data['Admin']['username']; ?></td>
	</tr>
	<tr>
		<th colspan="2">総合管理者アカウント管理権限</th>
		<td><?php echo $presence[$data['Admin']['is_auth_admins']] ?></td>
	</tr>
	<tr>
		<th rowspan="3" width="150">店舗アカウント管理権限</th>
		<th width="150">編集</th>
		<td><?php echo $presence[$data['Admin']['is_auth_users_edit']] ?></td>
	</tr>
	<tr>
		<th>削除</th>
		<td><?php echo $presence[$data['Admin']['is_auth_users_delete']] ?></td>
	</tr>
	<tr>
		<th>パスワード変更</th>
		<td><?php echo $presence[$data['Admin']['is_auth_users_changepassword']] ?></td>
	</tr>
	<tr>
		<th colspan="2">大エリア管理権限</th>
		<td><?php echo $presence[$data['Admin']['is_auth_large_areas']] ?></td>
	</tr>
	<tr>
		<th colspan="2">全国リンク集管理権限</th>
		<td><?php echo $presence[$data['Admin']['is_auth_allarea_links']] ?></td>
	</tr>
	<tr>
		<th rowspan="2">地域リンク集管理権限</th>
		<th>編集</th>
		<td><?php echo $presence[$data['Admin']['is_auth_area_links_edit']] ?></td>
	</tr>
	<tr>
		<th>削除</th>
		<td><?php echo $presence[$data['Admin']['is_auth_area_links_delete']] ?></td>
	</tr>
	<tr>
		<th rowspan="2">口コミ管理権限</th>
		<th>編集</th>
		<td><?php echo $presence[$data['Admin']['is_auth_reviews_edit']] ?></td>
	</tr>
	<tr>
		<th>削除</th>
		<td><?php echo $presence[$data['Admin']['is_auth_reviews_delete']] ?></td>
	</tr>
	<tr>
		<th rowspan="2">掲示板管理権限</th>
		<th>編集</th>
		<td><?php echo $presence[$data['Admin']['is_auth_boards_edit']] ?></td>
	</tr>
	<tr>
		<th>削除</th>
		<td><?php echo $presence[$data['Admin']['is_auth_boards_delete']] ?></td>
	</tr>
	<tr>
		<th colspan="2">地域管理権限</th>
		<td>
		<?php if($large_area_count == $data[0]['count']){?>
		全エリア
		<?php }else{?>
			<?php foreach($data['AdminsAuthLargeArea'] as $key => $admins_auth_large_area){
				if($admins_auth_large_area['selected'] == true){
					$ret =  Set::extract('/LargeArea[id=' . $admins_auth_large_area['large_area_id'] . ']/name', $large_areas);
					echo (!empty($ret[0]) ? $ret[0] . '&nbsp;' : '');
				}
			}?>
		<?php }?>
		</td>
	</tr>
</table>
<?php
$msg = __($data['Admin']['username'] . 'を削除します。削除したデータは元に戻りません！よろしいですか？', true);
echo $form->submit(__('削除', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')"));
echo $form->end();
?>
