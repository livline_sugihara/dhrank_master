<h3>口コミ情報一覧</h3><style>
table tr th {
	text-align: center;
}
td.align-center {
	text-align: center;
}
td.align-right {
	text-align: right;
}
</style>
<a href="/reviews/view_veto_ip_address">禁止IPアドレス一覧画面へ</a>
<br />
<a href="/reviews/view_veto_uid">禁止個体識別番号一覧画面へ</a>
<?php echo $form->create(null, array('type'=>'post', 'action' => '')); ?>
<table>
	<tr style='color: #000066; background-color: #AAAAFF'>
		<th>
			大エリア<br />

			<?php
				echo $form->select(
						'LargeArea.id',
						$large_area,
						null, // selected指定
						array(
							'empty'=>'選択',
							'onchange'=>'return this.form.submit();'
						)
					);
			?>
		</th>
		<th>投稿日</th>
		<th>投稿者</th>
		<th>店名</th>
		<th>女の子名</th>
		<th>女の子評価</th>
		<th>コメント</th>
		<th>媒体</th>
		<th>IPアドレス</th>
		<th>個体識別番号</th>
		<th>公開</th>
		<th>PickUP</th>
		<th>会員限定</th>
		<?php if($parent_admin['Admin']['is_auth_reviews_delete']){?>
		<th>削除</th>
		<?php }?>

	</tr>

<?php foreach($list as $record){ ?>	<tr style='color: #000099; background-color: #DDDDFF'>
		<td class="align-center">
			<?php echo $record['LargeArea']['name'] ?>
		</td>
		<td>
			<?php if($parent_admin['Admin']['is_auth_reviews_edit']){?>
			<a href="/reviews/edit_created/<?php echo $record['Review']['id'] ?>"><?php echo $record['Review']['created'] ?> </a>
			<?php }else{?>
			<?php echo $record['Review']['created'] ?>
			<?php }?>
		</td>
		<td>
			<?php if(!empty($record['Reviewer']['id'])){?>
			<?php echo $record['Reviewer']['handle']?>
			<?php }else{?>
			<?php echo $record['Review']['post_name'] ?>
			<?php }?>
		</td>
		<td>
			<?php echo $record['Shop']['name'] ?>
		</td>
		<td>
			<?php echo $record['Girl']['name'] ?>
		</td>

		<td class="align-right">			<?php echo round($record[0]['girl_avg'],2) ?>
		</td>

		<td>			<?php echo mb_substr($record['Review']['comment'],0,20) ?>
		</td>

		<td class="align-center">			<?php echo $record['Review']['media'] ?>
		</td>

		<td>			<?php if(!empty($record['VetoIpAddressReview']['id']) || !$parent_admin['Admin']['is_auth_reviews_edit']){?>
			<?php echo $record['Review']['ip_address'] ?>
			<?php }else{?>
			<a href="/reviews/veto_ip_address/<?php echo $record['Review']['id'] ?>"><?php echo $record['Review']['ip_address'] ?> </a>
			<?php }?>
		</td>

		<td>
			<?php if(!empty($record['VetoUidReview']['id']) || !$parent_admin['Admin']['is_auth_reviews_edit']){?>
			<?php echo $record['Review']['uid'] ?>
			<?php }else{?>
			<a href="/reviews/veto_uid/<?php echo $record['Review']['id'] ?>"><?php echo $record['Review']['uid'] ?> </a>
			<?php }?>
		</td>
		<td>
	<?php if($record['Review']['is_publish'] == 0) { ?>
			<a href="/reviews/publish/<?php echo $record['Review']['id'] ?>">未公開</a>
	<?php } else { ?>
			公開済み
	<?php } ?>
		</td>
		<td>
<?php /*

	<?php if($record['Review']['pickup_admin'] == 0) { ?>
			<a href="/reviews/pickup/<?php echo $record['Review']['id'] ?>">未選択</a>
	<?php } else { ?>
			選択済
	<?php } ?>
*/ ?>
	<?php if($record['Review']['pickup_admin'] == 0) { ?>			<a href="/reviews/pickup/<?php echo $record['Review']['id'] ?>">未選択</a>
	<?php } else { ?>
			<a href="/reviews/pickup_remove/<?php echo $record['Review']['id'] ?>" style="background-color: pink;">選択中</a>
	<?php } ?>
		</td>
		<td>
	<?php if($record['Review']['member_only'] == 0) { ?>
			<a href="/reviews/member_only/<?php echo $record['Review']['id'] ?>">通常</a>
	<?php } else { ?>
			<a href="/reviews/member_only/<?php echo $record['Review']['id'] ?>" style="background-color: pink;">会員限定</a>
	<?php } ?>
		</td>
	<?php if($parent_admin['Admin']['is_auth_reviews_delete']){?>
		<td>
		<?php if($record['Review']['delete_flag'] == 0) { ?>
			<a href="/reviews/delete/<?php echo $record['Review']['id'] ?>">通常</a>
		<?php } else if($record['Review']['delete_flag'] == 1) { ?>
			<a href="/reviews/delete/<?php echo $record['Review']['id'] ?>" style="background-color: pink;">コメント削除</a>
		<?php } else if($record['Review']['delete_flag'] == 2) { ?>
			<a href="/reviews/delete/<?php echo $record['Review']['id'] ?>" style="background-color: pink;">依頼削除</a>
		<?php } else { ?>
			<span style="background-color: pink;">全削除済み</span>
		<?php } ?>
		</td>
	<?php } ?>
	</tr>
<?php } ?>
</table>
<?php echo $form->end(); ?>
