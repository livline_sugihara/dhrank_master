<h3>IPアドレス投稿禁止設定</h3>

<?php echo $form->create(null,array('type'=>'post','action'=>"")); ?>
<?php echo $form->hidden('Review.ip_address'); ?>
<table>
	<tr>
		<th>IPアドレス</th>
		<td><?php echo $data['Review']['ip_address']; ?></td>
	</tr>
</table>
<?php
$msg = __('[' . $data['Review']['ip_address'] . ']を投稿禁止にします。よろしいですか？', true);
echo $form->submit(__('投稿禁止', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')"));
echo $form->end();
?>

<?php //echo var_dump($data)?>