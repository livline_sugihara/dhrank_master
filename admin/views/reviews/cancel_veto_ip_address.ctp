<h3>IPアドレス禁止解除確認画面</h3>

<?php //start---2012/3/15障害No.1-1-0011修正?>
<?php echo $form->create(null,array('type'=>'post','action'=>'cancel_veto_ip_address/' . $data['VetoIpAddressReview']['id'])); ?>
<?php //end---2012/3/15障害No.1-1-0011修正?>
<?php echo $form->hidden('VetoIpAddressReview.id'); ?>
<table>
	<tr>
		<th>IPアドレス</th>
		<td><?php echo $data['VetoIpAddressReview']['name']; ?></td>
	</tr>
</table>
<?php
$msg = __('[' . $data['VetoIpAddressReview']['name'] . ']の投稿禁止を解除します。よろしいですか？', true);
echo $form->submit(__('解除', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')"));
echo $form->end();
?>

<?php //echo var_dump($data)?>