<h3>口コミ投稿日時編集</h3>

<?php echo $form->create(null,array('type'=>'post','action'=>"")); ?>
<?php echo $form->hidden('Review.id'); ?>
<table>
	<tr>
		<th>大エリア</th>
		<td>
			<?php echo $data['LargeArea']['name'] ?>
		</td>
	</tr>
	<tr>
		<th>投稿日</th>
		<td>
			<?php echo $form->year('Review.created', date('Y', strtotime('-3 year')), date('Y', strtotime('1 year')), $data['Review']['created'], array('empty' => null));?>
			年
			<?php echo $form->month('Review.created', $data['Review']['created'], array('empty' => null, 'monthNames' => false));?>
			月
			<?php echo $form->day('Review.created', $data['Review']['created'], array('empty' => null));?>
			日
			<?php echo $form->hour('Review.created', true, $data['Review']['created'], array('empty' => null));?>
			時
			<?php echo $form->minute('Review.created', $data['Review']['created'], array('empty' => null));?>
			分
			<?php echo $form->input('Review.created_second', array('type' => 'select', 'options' => array_combine(range(0,59), range(0,59)), 'label' => false, 'div' => false, 'selected' => date('s',strtotime($data['Review']['created']))));?>
			秒
			<?php echo $form->error('Review.created'); ?>
		</td>
	</tr>
	<tr>
		<th>投稿者</th>
		<td>
			<?php if(!empty($data['Reviewer']['id'])){?>
			<?php echo $data['Reviewer']['handle']?>
			<?php }else{?>
			<?php echo $data['Review']['post_name'] ?>
			<?php }?>
		</td>
	</tr>
	<tr>
		<th>店名</th>
		<td>
			<?php echo $data['Shop']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>女の子名</th>
		<td>
			<?php echo $data['Girl']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>女の子評価</th>
		<td>
			<?php echo round($data[0]['girl_avg'],2) ?>
		</td>
	</tr>
	<tr>
		<th>コメント</th>
		<td>
			<?php echo $data['Review']['comment']; ?>
		</td>
	</tr>
	<tr>
		<th>媒体</th>
		<td>
			<?php echo $data['Review']['media']; ?>
		</td>
	</tr>
	<tr>
		<th>IPアドレス</th>
		<td>
			<?php echo $data['Review']['ip_address']; ?>
		</td>
	</tr>
	<tr>
		<th>個体識別番号</th>
		<td>
			<?php echo $data['Review']['uid']; ?>
		</td>
	</tr>
</table>
<?php echo $form->end('送信'); ?>

<?php //echo var_dump($data)?>