<?php
if($data['Review']['member_only'] == 0) {
?>
<h3>会員限定選択</h3>
<?php
} else {
?>
<h3>会員限定解除</h3>
<?php
}
?>


<?php echo $form->create(null,array('type'=>'post','action'=>"")); ?>
<?php echo $form->hidden('Review.id'); ?>
<table>
	<tr>
		<th>大エリア</th>
		<td>
			<?php echo $data['LargeArea']['name'] ?>
		</td>
	</tr>
	<tr>
		<th>投稿日</th>
		<td>
			<?php echo $data['Review']['created']; ?>
		</td>
	</tr>
	<tr>
		<th>投稿者</th>
		<td>
			<?php if(!empty($data['Reviewer']['id'])){?>
			<?php echo $data['Reviewer']['handle']?>
			<?php }else{?>
			<?php echo $data['Review']['post_name'] ?>
			<?php }?>
		</td>
	</tr>
	<tr>
		<th>店名</th>
		<td>
			<?php echo $data['Shop']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>女の子名</th>
		<td>
			<?php echo $data['Girl']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>女の子評価</th>
		<td>
			<?php echo round($data[0]['girl_avg'],2) ?>
		</td>
	</tr>
	<tr>
		<th>コメント</th>
		<td>
			<?php echo mb_substr($data['Review']['comment'],0,50); ?>
		</td>
	</tr>
	<tr>
		<th>媒体</th>
		<td>
			<?php echo $data['Review']['media']; ?>
		</td>
	</tr>
	<tr>
		<th>IPアドレス</th>
		<td>
			<?php echo $data['Review']['ip_address']; ?>
		</td>
	</tr>
	<tr>
		<th>個体識別番号</th>
		<td>
			<?php echo $data['Review']['uid']; ?>
		</td>
	</tr>
</table>
<?php
if($data['Review']['member_only'] == 0) {
	echo $form->submit(__('会員限定に選択', true), array('name'=>'member_only'));
} else {
	echo $form->submit(__('会員限定を解除', true), array('name'=>'member_only'));
}
echo $form->end();
?>

<?php //echo var_dump($data)?>