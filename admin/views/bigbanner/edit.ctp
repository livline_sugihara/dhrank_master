<h3>ビッグバナー編集</h3>

<?php echo $form->create(null, array('type'=>'post','enctype' => 'multipart/form-data', 'action' => 'edit/' . $data['BigBanner']['id'])); ?>
<?php echo $form->hidden('BigBanner.id'); ?>
<?php echo $form->hidden('BigBanner.shop_filename'); ?>
<?php echo $form->hidden('BigBanner.girl_filename'); ?>
<table>
	<tr>
		<th>大エリア</th>
		<td><?php echo $form->select('BigBanner.large_area_id', $large_area,null,array('empty'=>'選んでください')); ?>
		<?php echo $form->error('BigBanner.large_area_id'); ?></td>
	</tr>
	<tr>
		<th>バナー画像(店舗)</th>
		<td>
<?php
if(!empty($data['BigBanner']['shop_filename'])) {
?>
			<img src="<?php echo APP_IMG_URL . 'banner/big_banner/' . $data['BigBanner']['large_area_id'] . '/' . $data['BigBanner']['shop_filename']; ?>" width="320" height="210" /><br />
<?php } ?>
			<?php echo $form->file('BigBanner.shop_filename_edit');?>
			<?php echo $form->error('BigBanner.shop_filename_edit'); ?>
		</td>
	</tr>
	<tr>
		<th>リンクURL(店舗)</th>
		<td>
			<?php echo $form->text('BigBanner.shop_link_url');?>
			<?php echo $form->error('BigBanner.shop_link_url'); ?>
		</td>
	</tr>
	<tr>
		<th>バナー画像(女の子)</th>
		<td>
<?php
if(!empty($data['BigBanner']['girl_filename'])) {
?>
			<img src="<?php echo APP_IMG_URL . 'banner/big_banner/' . $data['BigBanner']['large_area_id'] . '/' . $data['BigBanner']['girl_filename']; ?>" width="500" height="300" /><br />
<?php } ?>
			<?php echo $form->file('BigBanner.girl_filename_edit');?>
			<?php echo $form->error('BigBanner.girl_filename_edit'); ?>
		</td>
	</tr>
	<tr>
		<th>リンクURL(女の子)</th>
		<td>
			<?php echo $form->text('BigBanner.girl_link_url');?>
			<?php echo $form->error('BigBanner.girl_link_url'); ?>
		</td>
	</tr>
</table>
<?php echo $form->end('送信'); ?>
