
<h3>ビッグバナー登録</h3>

<?php echo $form->create(null, array('type'=>'post','enctype' => 'multipart/form-data', 'action' => "")); ?>
<table>
	<tr>
		<th>大エリア</th>
		<td><?php echo $form->select('BigBanner.large_area_id', $large_area,null,array('empty'=>'選んでください')); ?>
		<?php echo $form->error('BigBanner.large_area_id'); ?></td>
	</tr>
	<tr>
		<th>バナー画像(店舗)</th>
		<td>
			<?php echo $form->file('BigBanner.shop_filename_add');?>
			<?php echo $form->error('BigBanner.shop_filename_add'); ?>
		</td>
	</tr>
	<tr>
		<th>リンクURL(店舗)</th>
		<td>
			<?php echo $form->text('BigBanner.shop_link_url');?>
			<?php echo $form->error('BigBanner.shop_link_url'); ?>
		</td>
	</tr>
	<tr>
		<th>バナー画像(女の子)</th>
		<td>
			<?php echo $form->file('BigBanner.girl_filename_add');?>
			<?php echo $form->error('BigBanner.girl_filename_add'); ?>
		</td>
	</tr>
	<tr>
		<th>リンクURL(女の子)</th>
		<td>
			<?php echo $form->text('BigBanner.girl_link_url');?>
			<?php echo $form->error('BigBanner.girl_link_url'); ?>
		</td>
	</tr>
</table>
<?php echo $form->end('登録'); ?>
