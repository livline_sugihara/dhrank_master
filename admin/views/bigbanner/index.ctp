<?php // if($parent_admin['Admin']['is_auth_area_links_edit']){?>

<?php echo $html->link('ビッグバナー登録','/bigbanner/add'); ?>
<?php // } ?>
<br /><br />

<h3>ビッグバナー一覧</h3>
<?php echo $form->create(null, array('type'=>'post', 'action' => '')); ?>
<table>
	<tr style='color: #000066; background-color: #AAAAFF'>
		<th>ID</th>
		<th>
			大エリア<br />
			<?php echo $form->select('BigBanner.large_area_id', $large_area,null,array('empty'=>'選択','onchange'=>'return this.form.submit();')); ?>
		</th>
		<th>店舗 バナー画像 / リンクURL</th>
		<th>女の子 バナー画像 / リンクURL</th>
		<th>編集</th>
		<th>削除</th>
	</tr>
<?php foreach($list as $record){ ?>
	<tr style='color: #000099; background-color: #DDDDFF'>
		<td>
			<?php echo $record['BigBanner']['id'] ?>
		</td>
		<td>
			<?php echo $record['LargeArea']['name'] ?>
		</td>
		<td height="210" style="position: relative;">
			<img src="<?php echo APP_IMG_URL . 'banner/big_banner/' . $record['LargeArea']['id'] . '/' . $record['BigBanner']['shop_filename'] . '?time=' . date('YmdHis'); ?>" width="160" height="105" />
			
			<div style="position: absolute; bottom: 0;"><?php echo $record['BigBanner']['shop_link_url'] ?></div>
		</td>
		<td height="210" style="position: relative;">
			<img src="<?php echo APP_IMG_URL . 'banner/big_banner/' . $record['LargeArea']['id'] . '/' . $record['BigBanner']['girl_filename'] . '?time=' . date('YmdHis'); ?>" width="250" height="150" />

			<div style="position: absolute; bottom: 0;"><?php echo $record['BigBanner']['girl_link_url'] ?></div>
		</td>
<?php //if($parent_admin['Admin']['is_auth_area_links_edit']){?>
		<td>
			<a href="/bigbanner/edit/<?php echo $record['BigBanner']['id'] ?>">編集</a>
		</td>
<?php //} ?>
<?php // if($parent_admin['Admin']['is_auth_area_links_delete']){?>
		<td>
			<a href="/bigbanner/delete/<?php echo $record['BigBanner']['id'] ?>">削除</a>
		</td>
<?php //} ?>
	</tr>
<?php } ?>
</table>
<?php echo $form->end(); ?>
