
<h3>ビッグバナー削除</h3>

<?php echo $form->create(null,array('type'=>'post','action'=>'delete/' . $data['BigBanner']['id'])); ?>
<?php echo $form->hidden('BigBanner.id'); ?>
<?php echo $form->hidden('BigBanner.large_area_id'); ?>
<?php echo $form->hidden('BigBanner.shop_filename'); ?>
<?php echo $form->hidden('BigBanner.girl_filename'); ?>
<table>
	<tr>
		<th width="300">ID</th>
		<td>
			<?php echo $data['BigBanner']['id']; ?>
		</td>
	</tr>
	<tr>
		<th>大エリア</th>
		<td>
			<?php echo $data['LargeArea']['name']; ?>
		</td>
	</tr>
	<tr>
		<th>バナー画像(店舗)</th>
		<td>
<?php
if(!empty($data['BigBanner']['shop_filename'])) {
?>
			<img src="<?php echo APP_IMG_URL . 'banner/big_banner/' . $data['BigBanner']['large_area_id'] . '/' . $data['BigBanner']['shop_filename']; ?>" width="320" height="210" /><br />
<?php } ?>
		</td>
	</tr>
	<tr>
		<th>リンクURL(店舗)</th>
		<td>
			<?php echo $data['BigBanner']['shop_link_url']; ?>
		</td>
	</tr>
	<tr>
		<th>バナー画像(女の子)</th>
		<td>
<?php
if(!empty($data['BigBanner']['girl_filename'])) {
?>
			<img src="<?php echo APP_IMG_URL . 'banner/big_banner/' . $data['BigBanner']['large_area_id'] . '/' . $data['BigBanner']['girl_filename']; ?>" width="500" height="300" /><br />
<?php } ?>
		</td>
	</tr>
	<tr>
		<th>リンクURL(女の子)</th>
		<td>
			<?php echo $data['BigBanner']['girl_link_url']; ?>
		</td>
	</tr>
</table>
<?php
$msg = __('バナーを削除します。よろしいですか？', true);
echo $form->submit(__('削除', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')"));
echo $form->end();
?>
