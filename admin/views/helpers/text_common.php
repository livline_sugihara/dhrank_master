<?php
class TextCommonHelper extends Helper {
	var $helpers = array('Html');

	//日付テキスト取得
	function get_datetimewithweek($date){
		$weekday = Configure::read('weekday');
		$week = $weekday[date('w',strtotime($date))];
		return date('Y/m/d',strtotime($date)) . '(' . $week . ')' . date('H:i:s',strtotime($date));
	}
}