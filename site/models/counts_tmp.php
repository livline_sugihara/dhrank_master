<?php
class CountsTmp extends AppModel {
	public $name = 'CountsTmp';

	function truncate()
	{
		return $this->getDataSource()->truncate($this->table);
	}

	function largeCounts($id)
	{
		$query = "SELECT * FROM users LEFT JOIN shops ON users.id = shops.user_id where shops.is_created = 1 AND users.large_area_id = ". $id;
		$counts = $this->query($query);
		return $counts;
	}

	function smallCounts($id)
	{
		$query = "SELECT * FROM users LEFT JOIN shops ON users.id = shops.user_id where shops.is_created = 1 AND users.small_area_id LIKE '%". $id ."%'";
		$counts = $this->query($query);
		return $counts;
	}

	function categoryLCounts($large, $small, $category)
	{
		$query = "SELECT * FROM users LEFT JOIN shops ON users.id = shops.user_id where shops.is_created = 1 AND users.large_area_id = ". $large .
		" AND shops.shops_business_category_id = ". $category;
		$counts = $this->query($query);
		return $counts;
	}

	function categorySCounts($large, $small, $category)
	{
		$query = "SELECT * FROM users LEFT JOIN shops ON users.id = shops.user_id where shops.is_created = 1 AND users.large_area_id = ". $large .
		" AND users.small_area_id LIKE '%". $small ."%' AND shops.shops_business_category_id = ". $category;
		$counts = $this->query($query);
		return $counts;
	}

}
?>
