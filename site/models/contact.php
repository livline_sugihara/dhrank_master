<?php

class Contact extends AppModel {

	var $name = 'Contact';
	var $useTable = false;

	// 有料掲載希望のバリデート
	public $validate_payment_insert = array(
		'insert_area'=>array(
			array('rule' => array('isNoTag','insert_area'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'category_name'=>array(
			array('rule' => array('isNoTag','category_name'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'shop_name'=>array(
			array('rule' => array('isNoTag','shop_name'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'charge_name'=>array(
			array('rule' => array('isNoTag','charge_name'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'charge_contact'=>array(
			array('rule' => array('custom',VALID_PHONE),'message'=>'電話番号はハイフン付きで入力してください。'),
			array('rule' => array('isNoTag','charge_contact'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'emailto'=>array(
			array('rule' => 'notEmpty','message'=>'メールアドレスを記入してください。'),
			array('rule' => array('email',false, VALID_EMAIL),'message'=>'メールアドレスを入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'contact_time'=>array(
			array('rule' => array('isNoTag','contact_time'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'shop_url'=>array(
			array('rule' => 'url','message'=>'URLを入力してください。'),
			array('rule' => array('isNoTag','shop_url'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'sendfile'=>array(
			array('rule' => array('isNotOverFileSize','sendfile',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
			array('rule' => array('isExtCheck'),'message'=>'ファイルの拡張子は、jpg,jpeg,pngのいずれかにしてください。'),

		),
		'comment'=>array(
			array('rule' => array('isNoTag','comment'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
	);

	// 無料掲載希望のバリデート
	public $validate_free_insert = array(
		'insert_area2'=>array(
			array('rule' => array('isNoTag','insert_area2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'category_name2'=>array(
			array('rule' => array('isNoTag','category_name2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'shop_name2'=>array(
			array('rule' => array('isNoTag','shop_name2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'charge_name2'=>array(
			array('rule' => array('isNoTag','charge_name2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'charge_contact2'=>array(
			array('rule' => array('isNoTag','charge_contact2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'emailto2'=>array(
			array('rule' => 'notEmpty','message'=>'メールアドレスを記入してください。'),
			array('rule' => array('email',false, VALID_EMAIL),'message'=>'メールアドレスを入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'shop_url2'=>array(
			array('rule' => 'url','message'=>'URLを入力してください。'),
			array('rule' => array('isNoTag','shop_url2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'shop_tel2'=>array(
			array('rule' => array('custom',VALID_PHONE),'message'=>'電話番号はハイフン付きで入力してください。'),
			array('rule' => array('isNoTag','shop_tel2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'shop_address2'=>array(
			array('rule' => array('isNoTag','shop_address2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),

		'shop_business_hours2'=>array(
			array('rule' => array('isNoTag','shop_business_hours2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'lowest_cost2'=>array(
			array('rule' => array('isNoTag','lowest_cost2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
		'reserve2'=>array(
			array('rule' => array('isNoTag','reserve2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'numeric','message'=>'選択して下さい。'),
		),
		'receipt2'=>array(
			array('rule' => array('isNoTag','receipt2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'numeric','message'=>'選択して下さい。'),
		),
		'layover2'=>array(
			array('rule' => array('isNoTag','layover2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'numeric','message'=>'選択して下さい。'),
		),
		'cosplay2'=>array(
			array('rule' => array('isNoTag','cosplay2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'numeric','message'=>'選択して下さい。'),
		),
		'credit2'=>array(
			array('rule' => array('isNoTag','credit2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => array('numeric'),'message'=>'選択して下さい。'),
		),
		'sendfile2'=>array(
			array('rule' => array('isNotOverFileSize','sendfile2',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
		),
		'comment2'=>array(
			array('rule' => array('isNoTag','comment2'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
	);

	// 代理店契約のバリデート
	public $validate_agency_agreement = array(
		'corp_name3'=>array(
			array('rule' => array('isNoTag','corp_name3'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'charge_name3'=>array(
			array('rule' => array('isNoTag','charge_name3'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'charge_contact3'=>array(
			array('rule' => array('isNoTag','charge_contact3'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'emailto3'=>array(
			array('rule' => 'notEmpty','message'=>'メールアドレスを記入してください。'),
			array('rule' => array('email',false, VALID_EMAIL),'message'=>'メールアドレスを入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'corp_url3'=>array(
			array('rule' => 'url','message'=>'URLを入力してください。'),
			array('rule' => array('isNoTag','corp_url3'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'comment3'=>array(
			array('rule' => array('isNoTag','comment3'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
	);

	// 代理店契約のバリデート
	public $validate_other = array(
		'emailto4'=>array(
			array('rule' => 'notEmpty','message'=>'メールアドレスを記入してください。'),
			array('rule' => array('email',false, VALID_EMAIL),'message'=>'メールアドレスを入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'charge_name4'=>array(
			array('rule' => array('isNoTag','charge_name4'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => 'notEmpty','message'=>'必須項目です。'),
		),
		'comment4'=>array(
			array('rule' => array('isNoTag','comment4'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
		),
	);

}
