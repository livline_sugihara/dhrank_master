<?php

class User extends AppModel {

	var $name = 'User';

	var $hasOne = array(
		'UsersAuthority' => array(
			'className' => 'UsersAuthority',
			'type' => 'inner',
			'foreignKey' => '',
			'conditions' => array('UsersAuthority.user_id = User.id'),
			'fields' => '',
			'order' => ''
		),
		'Shop' => array(
			'className' => 'Shop',
			'conditions' => 'Shop.user_id = User.id',
		)
	);

	public $belongsTo = array(
		'LargeArea' => array(
			'className' => 'LargeArea',
			'conditions' => array('LargeArea.id = User.large_area_id')
		)
	);

}
