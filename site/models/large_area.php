<?php
class LargeArea extends AppModel {
	public $name = 'LargeArea';
	public $hasMany = array(
		'SmallArea' => array(
			'className' => 'SmallArea',
			'foreignKey' => 'large_area_id'
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'large_area_id'
		),
		// 'Shop' => array(
		// 	'className' => 'Shop',
		// 	'foreignKey' => '',
		// 	'conditions' => 'User.id = Shop.user_id'
		// ),
		);

	/** 表示グレード(無料パターン) */
	const DISPLAY_GRADE_FREE = 0;
	/** 表示グレード(半有料パターン) */
	const DISPLAY_GRADE_MIDDLE = 1;
	/** 表示グレード(有料パターン) */
	const DISPLAY_GRADE_PAY = 2;

	public static function getDisplayGradeOptions() {
		return array(
			self::DISPLAY_GRADE_FREE => '無料パターン',
			self::DISPLAY_GRADE_MIDDLE => '中間パターン',
			self::DISPLAY_GRADE_PAY => '有料パターン',
		);
	}
}
?>
