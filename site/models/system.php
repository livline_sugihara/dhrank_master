<?php
class System extends AppModel {
	var $name = 'System';

	var $hasOne = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => '',
			'conditions' => array('User.id = System.user_id'),
			'fields' => '',
			'order' => ''
		),
		'Shop' => array(
			'className' => 'Shop',
			'foreignKey' => '',
			'conditions' => array('Shop.user_id = System.user_id'),
			'fields' => '',
			'order' => ''
		),
		'LargeArea' => array(
			'className' => 'LargeArea',
			'foreignKey' => '',
			'conditions' => array('User.large_area_id = LargeArea.id'),
			'fields' => '',
			'order' => ''
		),
		'MHourClose' => array(
			'className' => 'MHourClose',
			'foreignKey' => '',
			'conditions' => array('System.hour_close_id = MHourClose.id'),
			'fields' => '',
			'order' => ''
		),
		'MHourOpen' => array(
			'className' => 'MHourOpen',
			'foreignKey' => '',
			'conditions' => array('System.hour_open_id = MHourOpen.id'),
			'fields' => '',
			'order' => ''
		),
	);

}
?>