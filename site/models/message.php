<?php
class Message extends AppModel {
	var $name = 'Message';

	var $hasOne = array(
			'Review' => array(
					'className' => 'Review',
					'foreignKey' => '',
					'conditions' => array('Review.id = Message.review_id'),
					'fields' => '',
					'order' => ''
			),
			'User' => array(
					'className' => 'User',
					'foreignKey' => '',
					'conditions' => array('User.id = Review.user_id'),
					'fields' => '',
					'order' => ''
			),
			'LargeArea' => array(
					'className' => 'LargeArea',
					'foreignKey' => '',
					'conditions' => array('User.large_area_id = LargeArea.id'),
					'fields' => '',
					'order' => ''
			),
			'Shop' => array(
					'className' => 'Shop',
					'foreignKey' => '',
					'conditions' => array('Shop.user_id = User.id'),
					'fields' => '',
					'order' => ''
			),
	);

}
?>