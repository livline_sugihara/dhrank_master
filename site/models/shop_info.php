<?php
class ShopInfo extends AppModel {

	public $name = 'ShopInfo';
	public $hasOne = array(
		'User' => array(
			'className' => 'User',
			'type' => 'inner',
			'foreignKey' => '',
			'conditions' => array('User.id = ShopInfo.user_id'),
			'fields' => '',			'order' => ''
			),
		'UsersAuthority' => array(
			'className' => 'UsersAuthority',
			'type' => 'inner',
			'foreignKey' => '',
			'conditions' => array('UsersAuthority.user_id = ShopInfo.user_id'),
			'fields' => '',			'order' => ''
			),
		// 'LargeArea' => array(
		// 	'className' => 'LargeArea',
		// 	'type' => 'inner',
		// 	'foreignKey' => '',
		// 	'conditions' => array('UsersAuthority.large_area_id = LargeArea.id'),
		// 	'fields' => '',		// 	'order' => ''
		// ),
		'Shop' => array(
			'className' => 'Shop',
			'type' => 'inner',
			'foreignKey' => '',
			'conditions' => array('Shop.user_id = ShopInfo.user_id'),
			'fields' => '',
			'order' => ''
			),
			// 'MShopsBusinessCategory' => array(
			// 	'className' => 'MShopsBusinessCategory',
			// 	'foreignKey' => '',
			// 	'conditions' => array('Shop.shops_business_category_id = MShopsBusinessCategory.id'),
			// 	'fields' => '',
			// 	'order' => ''
			// )
		);
			public function getInfo($large_area_id, $user_id = null)
			{
				$cond = array('Shop.is_created' => 1, 'User.large_area_id' => $large_area_id);
				if (!is_null($user_id))
				{
					$cond['Shop.user_id'] = $user_id;
				}

				$info = $this->find('all', array(
					'conditions' => $cond,
					'desc' => 'modified',
					'limit' => 10,
				));
				return $info;
			}
	}
?>
