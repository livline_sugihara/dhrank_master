<?php
class SummaryData extends AppModel {
	var $name = 'SummaryData';

	const SUMMARY_TYPE_SHOP = 1;			// お店総合(relation_id = user_id)
	const SUMMARY_TYPE_GIRL = 2;			// アクセスの多い女の子(relation_id = girl_id)
	const SUMMARY_TYPE_REVIEWER = 3;		// 口コミ投稿者(relation_id = reviewer_id)

	const RELATION_ID_NONE = 0;

	const RANK_MAX = 100000;				// RANKの想定最大値
}
