<?php
class ReviewerRegister extends AppModel {
	var $name = 'ReviewerRegister';

	var $hasOne = array(
	);

	public $validate = array(
			'email'=>array(
					array('rule' => 'notEmpty','message'=>'メールアドレスを記入してください。'),
					array('rule' => array('email',false, VALID_EMAIL),'message'=>'メールアドレスを入力してください。'),
					array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			),
			'agreement'=>array('rule' => 'is_agreement','message'=>'選択してください。'),
	);

	function is_agreement($data){
		return ($this->data['ReviewerRegister']['agreement'] == 1);
	}
}
?>
