<?php
class Board extends AppModel {
	var $name = 'Board';

	var $hasOne = array(
			'LargeArea' => array(
					'className' => 'LargeArea',
					'foreignKey' => '',
					'conditions' => array('Board.large_area_id = LargeArea.id'),
					'fields' => '',
					'order' => ''
			),
	);

	var $hasMany = array(
			'BoardRes' => array(
					'className' => 'BoardRes',
					'foreignKey' => '',
					'conditions' => '',
					'foreignKey' => 'board_id',
					'fields' => '',
					'order' => 'sub_id asc'
			),
	);

	public $validate = array(
			'title'=>array(
					//start---2013/3/5 障害No.2-0019修正
					array('rule' => array('isNoTag','title'),'message'=>'タグは入力できません。'),
					//end---2013/3/5 障害No.2-0019修正
					array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
					array('rule' => 'notEmpty','message'=>'タイトルを記入してください。'),
			),
	);

}
?>