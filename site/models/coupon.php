<?php
class Coupon extends AppModel {
	public $name = 'Coupon';
	public static $targetList = array(
		1 => '新規',
		2 => '会員',
		9 => '全員',
	);
	public static $targetCaptionList = array(
		1 => '初めてご利用のお客様に限り',
		2 => '会員様に限り',
		9 => '全員OK',
	);

	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => '',
			'conditions' => 'Coupon.user_id = User.id',
			'fields' => '',
			'order' => ''
		),
	);

	//findメソッド実行後のオーバーライド
	public function afterFind($results)
	{
		foreach ($results as $key => $record){
			//count取得ではない
			if (isset($results[$key]['Coupon']['id'])){
				// 有効期限
				if(isset($results[$key]['Coupon']['expire_date']) && !empty($results[$key]['Coupon']['expire_date']))
				{
					list($year, $month, $day) = sscanf($results[$key]['Coupon']['expire_date'], "%d-%d-%d 23:59:59");

					$results[$key]['Coupon']['expire_date_year'] = $year;
					$results[$key]['Coupon']['expire_date_month'] = $month;
					$results[$key]['Coupon']['expire_date_day'] = $day;
				}
			}
		}

		return $results;
	}

}
