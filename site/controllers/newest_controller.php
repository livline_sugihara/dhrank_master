<?php
class NewestController extends AppController {
	var $name = 'Newest';
	var $uses = array('Shop', 'TestRanking', 'ShopInfo', 'CountsTmp');
	var $components = array('Testranking');

	function beforeFilter() {
		parent::beforeFilter();

		switch($this->params['action'])
		{
			case 'index':
				$this->addBreadCrumbs("新着口コミ" , "/newest");
				break;
			case 's_index':
				$this->addBreadCrumbs("新着口コミ" , "/s/newest");
				break;
		}
	}

	function index(){
		//口コミ情報取得
		$this->get_newest_review();

		//head
		$this->set('title_for_layout', $this->title_tag_common . 'お店一覧');
		$this->set('meta_keywords', $this->meta_keywords_common .  ',お店一覧');
		$this->set('meta_description', $this->meta_description_common . 'お店一覧');
		$this->set('header_one', $this->h1_tag_common . 'お店一覧');

		//レビュアーランキング取得
		$this->Testranking->load($this);
		$reviewer = $this->Testranking->get_reviewer_ranking($this->parent_area['LargeArea']['id']);
		$this->set('reviewer', $reviewer);


		//お店のお知らせ取得
		$info = $this->ShopInfo->getInfo($this->Session->read('Area.largeArea'));

		$this->set('info', $info);

		//エリア取得
		$area = $this->LargeArea->find('all', array(
			'conditions' => array('id' => $this->parent_area['LargeArea']['id']),
			'fields' => array('id', 'name', 'area_banner_url')
		));
		$this->set('area', $area);

		//ショップカウント数取得
		$area_id[] = $area[0]['LargeArea']['id'];
		foreach ($area[0]['SmallArea'] as $s)
		{
			$area_id[] = '_'. $s['id'];
		}
		$shop_count = $this->CountsTmp->find('all', array(
			'conditions' => array('area_id' => $area_id)
		));
		if($this->Session->read('Area.smallArea')==0)
		{
			$shop_count['Category'] = $this->CountsTmp->find('first', array(
				'conditions' => array('CountsTmp.area_id' => $this->Session->read('Area.largeArea'))
			));
		}
		else
		{
			$shop_count['Category'] = $this->CountsTmp->find('first', array(
				'conditions' => array('CountsTmp.area_id' => '_'. $this->Session->read('Area.smallArea'))
			));
		}
		$this->set('shop_count', $shop_count);

	}

	function s_index() {
		$this->index();
	}

}
?>
