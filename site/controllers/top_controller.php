<?php

class TopController extends AppController {
	var $name = 'Top';
	var $uses = array('TestRanking', 'AreaIndexLink','Review','Shop', 'User', 'Girl', 'MShopsBusinessCategory', 'Coupon', 'ShopInfo', 'LargeArea', 'SmallArea', 'CountsTmp');
	var $components = array('Search', 'RequestHandler', 'Testranking');

	function beforeFilter() {
		parent::beforeFilter();

		//セッションにエリア情報を格納
		$this->Session->write('Area.largeArea', $this->parent_area['LargeArea']['id']);
		$this->Session->write('Area.smallArea', 0);
		$this->Session->write('Area.Name', $this->parent_area['LargeArea']['name']);

		switch($this->params['action'])
		{
			case 'index':
			case 'top':
				break;
			case 'add':
			case 'addend':
//				$this->addBreadCrumbs("登録" , "/girls/add");
				break;
		}
	}

	function index() {
		$this->layout = 'index';
		/*
		//アクセス取得
		$access_yesterday = $this->Access->find('first', array('conditions' => array('Access.large_area_id' => $this->parent_area['LargeArea']['id'], 'Access.date' => date('Y-m-d', strtotime('-1 day')))));
		if(empty($access_yesterday)){
			$access_yesterday['Access']['count'] = 0;
		}
		if(!empty($access_yesterday['Access']['rand'])){
			$this->set('access_yesterday', $access_yesterday['Access']['count']*$access_yesterday['Access']['rand'] + 10000);
		}else{
			$this->set('access_yesterday', 0);
		}

		//地域リンク集
		$link_order = '';
		if($this->parent_area['LargeArea']['link_order_index'] == 1){
			$link_order = 'AreaIndexLink.show_order ASC, AreaIndexLink.created ASC';
		}elseif($this->parent_area['LargeArea']['link_order_index'] == 2){
			$link_order = 'rand()';
		}
		//リンク集取得
		$this->set('list', $this->AreaIndexLink->find('all', array(
				'conditions' => array('AreaIndexLink.large_area_id' => $this->parent_area['LargeArea']['id']),
				'order' => $link_order,
		)));
		*/

		//head
		$this->set('title_for_layout', $this->title_tag_common);
		$this->set('meta_keywords', $this->meta_keywords_common);
		$this->set('meta_description', $this->meta_description_common);
		$this->set('header_one', $this->h1_tag_common);
	}
	function i_index() {
		$this->index();
	}
	function s_index() {
		$this->index();
	}

	function top() {

		// 口コミ情報取得
		$this->get_newest_review();
		// ピックアップ口コミ情報取得
		$this->get_pickup_review();

		// 新着新規掲載店取得
		$bindShop = array('hasOne' => array('Shop' => array(
			'className'=>'Shop',
			'conditions' => 'Shop.user_id = User.id',
			'foreignKey' => false)));
		$this->User->bindModel($bindShop, false);
		$this->set('new_shops', $this->User->find('all', array(
			'conditions' => array(
				'UsersAuthority.large_area_id' => $this->parent_area['LargeArea']['id'],
				'UsersAuthority.is_show_new_shop' => 1
			),
			'order' => 'UsersAuthority.order_rank ASC, User.kana ASC'
		)));

		// 新着お店激押し売れっ子(ランダムピック)
		$recommend_girls = $this->Girl->find('all', array(
			'conditions' => array(
				'UsersAuthority.is_recommend_girl' => 1,
				'Shop.is_created' => 1,
				'Girl.is_deleted' => 0,
				'Girl.recommend_type_id >' => 0,
			),
			'order' => 'rand()',
			'limit' => 25
		));
		$this->set('recommend_girls', $recommend_girls);

		// 参考になった口コミランキング
		$fields = array('ifnull((SELECT count(reviews.id) FROM reviews WHERE reviews.reviewer_id > 0 AND reviews.reviewer_id = Reviewer.id GROUP BY reviews.reviewer_id HAVING count(*) > 0), 0) AS total_cnt');
		$conditions = array(
			'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id'],
		);
		$order = '(SELECT count(id) FROM reviews_good_counts good WHERE good.review_id = Review.id GROUP BY review_id) DESC';
		$helpful_review = $this->_getReviews($conditions, $order, 3, $fields);
		$this->set('helpful_review', $helpful_review);

		// Newランキング
		$this->Testranking->load($this);
		$shops_rank = $this->Testranking->get_shops_ranking($this->parent_area['LargeArea']['id']);
		$girls_rank = $this->Testranking->get_girl_ranking_data($this->parent_area['LargeArea']['id']);
		$this->set('shops_rank', $shops_rank);
		$this->set('girls_rank', $girls_rank);
		// スマフォの場合
		if(isset($this->params['prefix']) && $this->params['prefix'] == 's') {
			// ランキング情報取得（店舗総合）を加工
			$shop_ranking = $this->viewVars['shop_ranking'];
			$limit_day = date('Y-m-d');

			foreach($shop_ranking AS $ii => $record) {
				$cnt = $this->Coupon->find('count', array(
					'conditions' => array(
						'Coupon.user_id' => $record['User']['id'],
						'OR' => array(
							'Coupon.expire_unlimited' => 1,
							'DATE_FORMAT(Coupon.expire_date,"%Y-%m-%d") >= ' => $limit_day,
						),
					)
				));

				$shop_ranking[$ii]['Coupon']['cnt'] = $cnt;
			}
			$this->set('shop_ranking', $shop_ranking);
		}
		//head
		$this->set('title_for_layout', $this->title_tag_common . 'トップページ');
		$this->set('meta_keywords', $this->meta_keywords_common . ',トップ');
		$this->set('meta_description', $this->meta_description_common . 'トップページ');
		$this->set('header_one', $this->h1_tag_common . 'トップページ');

		//レビュアーランキング取得
		$this->Testranking->load($this);
		$reviewer = $this->Testranking->get_reviewer_ranking($this->parent_area['LargeArea']['id']);
		$this->set('reviewer', $reviewer);


		//お店のお知らせ取得
		$info = $this->ShopInfo->getInfo($this->Session->read('Area.largeArea'));
		$this->set('info', $info);

		//エリア取得
		$area = $this->LargeArea->find('all', array(
			'conditions' => array('id' => $this->parent_area['LargeArea']['id']),
			'fields' => array('id', 'name', 'area_banner_url')
		));
		$this->set('area', $area);

		//ショップカウント数取得
		$area_id[] = $area[0]['LargeArea']['id'];
		foreach ($area[0]['SmallArea'] as $s)
		{
			$area_id[] = '_'. $s['id'];
		}
		$shop_count = $this->CountsTmp->find('all', array(
			'conditions' => array('area_id' => $area_id)
		));
		if($this->Session->read('Area.smallArea')==0)
		{
			$shop_count['Category'] = $this->CountsTmp->find('first', array(
				'conditions' => array('CountsTmp.area_id' => $this->Session->read('Area.largeArea'))
			));
		}
		else
		{
			$shop_count['Category'] = $this->CountsTmp->find('first', array(
				'conditions' => array('CountsTmp.area_id' => '_'. $this->Session->read('Area.smallArea'))
			));
		}
		$this->set('shop_count', $shop_count);

		switch($this->parent_area['LargeArea']['display_grade']) {
			case LargeArea::DISPLAY_GRADE_FREE:
				$this->render($this->device_file_name . 'top_free');
				break;
			case LargeArea::DISPLAY_GRADE_MIDDLE:
				$this->getShopList();
				$this->render($this->device_file_name . 'top_middle');
				break;
			case LargeArea::DISPLAY_GRADE_PAY:
				$this->render($this->device_file_name . 'top_pay');
				break;
		}

	}
	function i_top(){
		$this->top();
	}
	function s_top(){

		//top_banner - order_rank順にでバナー(イメージリストSP)の取得
		$area_id = $this->parent_area['LargeArea']['id'];
		$top_banner = $this->Shop->find('all', array(
			'conditions' => array('User.large_area_id' => $area_id, 'UsersAuthority.is_show_recommend_shop' => 1),
			'desc' => 'UsersAuthority.order_rank',
			'limit' => 5
		));
		$this->set('top_banner', $top_banner);

		//bottom_banner - ランダム順にてバナー（イメージリストSP）の取得
		$bottom_banner = $this->Shop->find('all', array(
			'conditions' => array('User.large_area_id' => $area_id, 'UsersAuthority.is_show_recommend_shop' => 1),
			'desc' => 'rand()',
			'limit' => 5
		));
		$this->set('bottom_banner', $bottom_banner);


		if($this->RequestHandler->isPost()){

			$area_id = $this->parent_area['LargeArea']['id'];

			$posts = $_POST;

			$this->Search->load($this);
			//クーポン検索
			if(isset($posts['data']['AreaIndexLink']['targetlist']))
			{
				$targetlist = $posts['data']['AreaIndexLink']['targetlist'];
				$data = $this->Search->coupon($area_id, $targetlist);
				$this->set('coupons', $data);
			}
		}
		$this->top();
	}

	/**
	 * お店リストを取得
	 */
	function getShopList() {

		//ショップデータに口コミデータをバインド
		$this->Shop->bindModel(array('hasMany' => array('Coupon' => array(
			'className'=>'Coupon',
			'foreignKey' => 'user_id',
			'limit' => 2
		))), false);

		// FIXME 「*」をやめたほうが良い
		$this->paginate = array('Shop' => array(
			'fields' => array('User.*','UsersAuthority.*','Shop.*', 'System.*', 'LargeArea.url',
				'(SELECT count(*) FROM girls WHERE User.id = girls.user_id) AS girl_cnt',
				'(SELECT count(*) FROM reviews WHERE User.id = reviews.user_id) AS review_cnt',
			),
			'conditions' => array(
				'Shop.is_created' => 1,
			),
			'group' => 'User.id',
			'limit' => 10,
			'order' => 'UsersAuthority.order_rank ASC, rand()'
		));
		$this->set('list', $this->paginate($this->Shop));
	}
}
?>
