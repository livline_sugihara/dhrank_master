<?php

class TestrankingComponent extends Object {
    var $_controller;

	public function load(& $controller)
	{
			$this->_controller = $controller;
	}

	// 口コミ数集計
	public function reviewer()
	{
		$this->_controller->TestRanking->reviewer();
	}

	// アクセスカウント集計
	// public function access_count()
	// {
	// 	$this->_controller->TestRanking->access_count();
    //
	// }

	// お店の総合ランキングを取得
	public function shops_count()
	{
		$this->_controller->TestRanking->shops_count();

	}

	// アクセスの多い女の子集計
	public function girls_count()
	{
		$this->_controller->TestRanking->girls_count();
	}

	// 口コミランキングデータ取得
	public function get_reviewer_ranking($large_area_id)
	{
		$datas = $this->_controller->TestRanking->getReviewerRankingData($large_area_id);
        foreach ($datas as $i => $record)
        {
            $d = $this->_controller->Review->find('first', array(
                'conditions' => array('Review.reviewer_id' => $record['rankings']['ranking_id']),
                'fields' => array('post_title', 'comment', 'delete_flag', 'member_only', 'modified'),
                'order' => 'modified DESC',
            ));
            $datas[$i]['Review'] = $d['Review'];
        }
		return $datas;
	}

	// アクセスカウントランキングデータ取得
	public function get_girl_ranking_data($large_area_id)
	{

		$datas = $this->_controller->TestRanking->getGirlRankingData($large_area_id);
		return $datas;

	}

	// お店の総合評価データ取得
	public function get_shops_ranking($large_area_id)
	{
		$datas = $this->_controller->TestRanking->getShopRankingData($large_area_id);
		return $datas;

	}


}
?>
