<?php
class CategoryController extends AppController {
	var $name = 'Category';
	var $uses = array('Shop', 'Review', 'Girl', 'Coupon', 'MShopsBusinessCategory', 'ShopInfo', 'LargeArea', 'CountsTmp', 'TestRanking');
	var $components = array('Testranking');

	function beforeFilter() {
		parent::beforeFilter();

		// 自カテゴリ情報を取得
		$category = $this->MShopsBusinessCategory->find('first', array(
			'conditions' => array(
				'MShopsBusinessCategory.id' => $this->params['business_category_id'],
			)
		));
		$this->set('category', $category);

		// クーポンリスト
		$this->set('m_target', Coupon::$targetList);

		switch($this->params['action'])
		{
			case 'index':
			if ($this->Session->read('Area.smallArea')!==0)
			{
				$this->addBreadCrumbs($this->Session->read('Area.Name'), "/area/" . $this->Session->read('Area.smallArea'));
			}
				$this->addBreadCrumbs($category['MShopsBusinessCategory']['name'] , "/category/" . $this->params['business_category_id']);
				break;
			case 's_index':
			if ($this->Session->read('Area.smallArea')!==0)
			{
				$this->addBreadCrumbs($this->Session->read('Area.Name'), "/area/" . $this->Session->read('Area.smallArea'));
			}
				$this->addBreadCrumbs($category['MShopsBusinessCategory']['name'] , "/category/" . $this->params['business_category_id']);
				break;
		}
	}


	function index() {

		// ピックアップ口コミ情報取得
		$this->get_pickup_review();

		//ショップデータに口コミデータをバインド
		$this->Shop->bindModel(array('hasMany' => array('Coupon' => array(
			'className'=>'Coupon',
			'foreignKey' => 'user_id',
			'limit' => 2
		))), false);

		// FIXME 「*」をやめたほうが良い
		// $this->paginate = array('Shop' => array(
		// 	'fields' => array('User.*','UsersAuthority.*','Shop.*', 'System.*', 'LargeArea.url',
		// 		'(SELECT count(*) FROM girls WHERE User.id = girls.user_id) AS girl_cnt',
		// 		'(SELECT count(*) FROM reviews WHERE User.id = reviews.user_id) AS review_cnt',
		// 	),
		// 	'conditions' => array(
		// 		'Shop.is_created' => 1,
		// 		'Shop.shops_business_category_id' => $this->params['business_category_id'],
		// 	),
		// 	'group' => 'User.id',
		// 	'limit' => $this->pcount['categorylist'],
		// 	'order' => 'UsersAuthority.order_rank ASC, rand()'
		// ));
		if ($this->Session->read('Area.smallArea')!==0)
		{
			$this->paginate = array('Shop' => array(
				'fields' => array('User.*','UsersAuthority.*','Shop.*', 'System.*', 'LargeArea.url',
					'(SELECT count(*) FROM girls WHERE User.id = girls.user_id) AS girl_cnt',
					'(SELECT count(*) FROM reviews WHERE User.id = reviews.user_id) AS review_cnt',
				),
				'conditions' => array(
					'Shop.is_created' => 1,
					'User.large_area_id' => $this->Session->read('Area.largeArea'),
					'User.small_area_id LIKE' => '%'. $this->Session->read('Area.smallArea') .'%',
					'Shop.shops_business_category_id' => $this->params['business_category_id'],
				),
				'group' => 'User.id',
				'limit' => $this->pcount['categorylist'],
				'order' => 'UsersAuthority.order_rank ASC, rand()'
			));
		}
		else
		{
			$this->paginate = array('Shop' => array(
				'fields' => array('User.*','UsersAuthority.*','Shop.*', 'System.*', 'LargeArea.url',
					'(SELECT count(*) FROM girls WHERE User.id = girls.user_id) AS girl_cnt',
					'(SELECT count(*) FROM reviews WHERE User.id = reviews.user_id) AS review_cnt',
				),
				'conditions' => array(
					'Shop.is_created' => 1,
					'User.large_area_id' => $this->Session->read('Area.largeArea'),
					'Shop.shops_business_category_id' => $this->params['business_category_id'],
				),
				'group' => 'User.id',
				'limit' => $this->pcount['categorylist'],
				'order' => 'UsersAuthority.order_rank ASC, rand()'
			));
		}
		$this->set('list', $this->paginate($this->Shop));

		//お店のお知らせ取得
		$info = $this->ShopInfo->getInfo($this->Session->read('Area.largeArea'));

		$this->set('info', $info);

		//エリア取得
		$area = $this->LargeArea->find('all', array(
			'conditions' => array('id' => $this->parent_area['LargeArea']['id']),
			'fields' => array('id', 'name', 'area_banner_url')
		));
		$this->set('area', $area);

		//レビュアーランキング取得
		$this->Testranking->load($this);
		$reviewer = $this->Testranking->get_reviewer_ranking($this->Session->read('Area.largeArea'));
		$this->set('reviewer', $reviewer);

		//ショップカウント数取得
		$area_id[] = $area[0]['LargeArea']['id'];
		foreach ($area[0]['SmallArea'] as $s)
		{
			$area_id[] = '_'. $s['id'];
		}
		$shop_count = $this->CountsTmp->find('all', array(
			'conditions' => array('area_id' => $area_id)
		));
		if($this->Session->read('Area.smallArea')==0)
		{
			$shop_count['Category'] = $this->CountsTmp->find('first', array(
				'conditions' => array('CountsTmp.area_id' => $this->Session->read('Area.largeArea'))
			));
		}
		else
		{
			$shop_count['Category'] = $this->CountsTmp->find('first', array(
				'conditions' => array('CountsTmp.area_id' => '_'. $this->Session->read('Area.smallArea'))
			));
		}
		$this->set('shop_count', $shop_count);
		//head
		$this->set('title_for_layout', $this->title_tag_common . 'お店一覧');
		$this->set('meta_keywords', $this->meta_keywords_common .  ',お店一覧');
		$this->set('meta_description', $this->meta_description_common . 'お店一覧');
		$this->set('header_one', $this->h1_tag_common . 'お店一覧');
	}

	function s_index() {
		$this->index();
	}
}
?>
