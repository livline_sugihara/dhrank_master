<?php

class ReportController extends AppController {
	var $name = 'Report';
	//start---2013/4/30 障害No.4-0002修正
	//var $uses = array('Review','Reviewer');
	var $uses = array('Review','Reviewer','ReviewsGoodCount');
	//end---2013/4/30 障害No.4-0002修正

	//投稿者別口コミ一覧
	//start---2013/3/11 障害No.1-4-0002修正
	//function list_post($param){
	function list_post(){
		//end---2013/3/11 障害No.1-4-0002修正

		//レビュアー取得
		$reviewer = $this->Reviewer->find('first',array(
				'conditions' => array(
						$this->getCurrentAreaConditionForReviewer(),
						//start---2013/3/11 障害No.1-4-0002修正
						//'Reviewer.id' => $param,
						'Reviewer.id' => $this->params['reviewer_id'],
						//end---2013/3/11 障害No.1-4-0002修正
				),
		));
		$this->set('reviewer',$reviewer);

		//各件数取得
		$rcommon_review_count = $this->Review->find('first',array(
				//start---2013/4/30 障害No.4-0002修正
				//'fields' => array('count(Review.id) as review_count','ifnull(count(ReviewsGoodCount.id),0) as reviews_good_count_sum'),
				'fields' => array('count(Review.id) as review_count'),
				//end---2013/4/30 障害No.4-0002修正
				'conditions' => array('Review.reviewer_id' => $reviewer['Reviewer']['id']),
				'gruop' => 'Review.reviewer_id',
		));
		$this->set('rcommon_review_count',$rcommon_review_count);
		//start---2013/4/30 障害No.4-0002修正
		$rcommon_reviews_good_count = $this->ReviewsGoodCount->find('first',array(
				'fields' => array('ifnull(count(ReviewsGoodCount.id),0) as reviews_good_count_sum'),
				'conditions' => array('Review.reviewer_id' => $reviewer['Reviewer']['id']),
				'gruop' => 'Review.reviewer_id',
		));
		$this->set('rcommon_reviews_good_count',$rcommon_reviews_good_count);
		//end---2013/4/30 障害No.4-0002修正

		//表示用
		$this->set('sex', Configure::read('sex'));

		//口コミ取得
		$this->paginate = array('Review' => array(
				'fields' => array('LargeArea.url','User.id','Girl.id','Girl.name','Girl.age','Review.id','Review.comment','Shop.name','Review.created','Review.post_name','Review.reviewer_id','Girl.image_1_s','Girl.image_1_m','Girl.image_1_l',
						'Review.score_girl_character','Review.score_girl_service','Review.score_girl_looks',
						'Review.comment_deleted',
						'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg'),

				'conditions' => array(
						$this->getCurrentAreaCondition(),
						'Review.reviewer_id' => $this->params['reviewer_id'],
				),
				'group' => array('Review.id'),
				'order' => 'Review.created DESC',
				'limit' => $this->pcount['reviews_list_post']));
		$this->set('reviews',$this->paginate('Review'));

		//head
		$this->set('title_for_layout', $this->title_tag_common .$reviewer['Reviewer']['handle']. 'のデリヘルガイド');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',一覧');
		$this->set('meta_description', $this->meta_description_common .$reviewer['Reviewer']['handle']. 'のデリヘルガイド');
		$this->set('header_one', $this->h1_tag_common .$reviewer['Reviewer']['handle']. 'のデリヘルガイド');
	}
		
	function confirm(){
		$this->render('confirm');
	}
	
	
	//start---2013/3/11 障害No.1-4-0002修正
	//function i_list_post($param) {$this->list_post($param);}
	//function s_list_post($param) {$this->list_post($param);}
	function i_list_post() {
		$this->list_post();
	}
	function s_list_post() {
		$this->list_post();
	}
	//end---2013/3/11 障害No.1-4-0002修正

}
?>
