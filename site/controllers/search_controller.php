<?php
class SearchController extends AppController {
	var $name = 'Search';
	var $uses = array('TestRanking', 'AreaIndexLink','Review','Shop', 'User', 'Girl', 'MShopsBusinessCategory', 'Coupon', 'ShopInfo', 'LargeArea', 'SmallArea', 'CountsTmp', 'TestRanking');
	var $components = array('Testranking');

	const SESSION_ACCESS_CONDITION_KEY = 'SearchController.condition';

	function beforeFilter() {
		parent::beforeFilter();

		switch($this->params['action'])
		{
			case 'index':
				$this->addBreadCrumbs('検索結果', "#");
				break;
			case 's_index':
				$this->addBreadCrumbs('検索結果', "#");
				break;
		}

		// クーポンリスト
		$this->set('m_target', Coupon::$targetList);

		// ピックアップ口コミ情報取得
		$this->get_pickup_review();
	}

	function index() {

		// 検索条件がない場合は表示しない
		if(empty($this->data)) {
			if(!$this->Session->check(self::SESSION_ACCESS_CONDITION_KEY)) {
				$this->cakeError('error404');
				return;
			}

			$session_data = $this->Session->read(self::SESSION_ACCESS_CONDITION_KEY);
			$search_type = $session_data['search_type'];
			$search_text = $session_data['search_text'];

		} else {
			// FIXME デバッグ
			$this->log($this->data, 'debug');

			$search_type = $this->data['Search']['type'];
			$search_text = $this->data['Search']['text'];

			$session_data = array(
				'search_type' => $search_type,
				'search_text' => $search_text,
			);
			$this->Session->write(self::SESSION_ACCESS_CONDITION_KEY, $session_data);
		}

		if($search_type == 'shop') {
			$search_type_str = '店舗';

			//ショップデータに口コミデータをバインド
			$this->Shop->bindModel(array('hasMany' => array('Coupon' => array(
				'className'=>'Coupon',
				'foreignKey' => 'user_id',
				'limit' => 2
			))), false);

			$this->paginate = array('Shop' => array(
				'fields' => array('User.*','UsersAuthority.*','Shop.*', 'System.*', 'LargeArea.url',
					'(SELECT count(*) FROM girls WHERE User.id = girls.user_id) AS girl_cnt',
					'(SELECT count(*) FROM reviews WHERE User.id = reviews.user_id) AS review_cnt',
				),
				'conditions' => array(
					'Shop.is_created' => 1,
					'OR' => array(
						'Shop.name LIKE' => '%' . $search_text . '%',
						'User.kana LIKE' => '%' . $search_text . '%',
					),
				),
				'group' => 'User.id',
				'limit' => 10,
				'order' => 'UsersAuthority.order_rank ASC, rand()'
			));
			$this->set('list', $this->paginate($this->Shop));
		} else if($search_type == 'girl') {
			$search_type_str = '女の子';

			$this->paginate = array('Girl' => array(
				'fields' => array(
					'User.*','UsersAuthority.*','Shop.*', 'Girl.*', 'LargeArea.*',
				),
				'conditions' => array(
					'Shop.is_created' => 1,
					'Girl.is_deleted' => 0,
					'Girl.name LIKE' => '%' . $search_text . '%',
				),
				'group' => 'User.id',
				'limit' => 8,
				'order' => 'UsersAuthority.order_rank ASC, rand()'
			));
			$this->set('list', $this->paginate($this->Girl));
		}
		$this->set('search_type_str', $search_type_str);
		// Newランキング
		$this->Testranking->load($this);
		$shops_rank = $this->Testranking->get_shops_ranking($this->parent_area['LargeArea']['id']);
		$girls_rank = $this->Testranking->get_girl_ranking_data($this->parent_area['LargeArea']['id']);
		$this->set('shops_rank', $shops_rank);
		$this->set('girls_rank', $girls_rank);
		// スマフォの場合
		if(isset($this->params['prefix']) && $this->params['prefix'] == 's') {
			// ランキング情報取得（店舗総合）を加工
			$shop_ranking = $this->viewVars['shop_ranking'];
			$limit_day = date('Y-m-d');
			foreach($shop_ranking AS $ii => $record)
			{
				$cnt = $this->Coupon->find('count', array(
					'conditions' => array(
						'Coupon.user_id' => $record['User']['id'],
						'OR' => array(
							'Coupon.expire_unlimited' => 1,
							'DATE_FORMAT(Coupon.expire_date,"%Y-%m-%d") >= ' => $limit_day,
							),
						)
					));
				$shop_ranking[$ii]['Coupon']['cnt'] = $cnt;
			}
			$this->set('shop_ranking', $shop_ranking);
		}

		//レビュアーランキング取得
		$this->Testranking->load($this);
		$reviewer = $this->Testranking->get_reviewer_ranking($this->parent_area['LargeArea']['id']);
		$this->set('reviewer', $reviewer);

		//お店のお知らせ取得
		$info = $this->ShopInfo->getInfo($this->Session->read('Area.largeArea'));
		$this->set('info', $info);

		//エリア取得
		$area = $this->LargeArea->find('all', array(
			'conditions' => array('id' => $this->parent_area['LargeArea']['id']),
			'fields' => array('id', 'name', 'area_banner_url')
		));
		$this->set('area', $area);

		//ショップカウント数取得
		$area_id[] = $area[0]['LargeArea']['id'];
		foreach ($area[0]['SmallArea'] as $s)
		{
			$area_id[] = '_'. $s['id'];
		}
		$shop_count = $this->CountsTmp->find('all', array(
			'conditions' => array('area_id' => $area_id)
		));

		if($this->Session->read('Area.smallArea')==0)
		{
			$shop_count['Category'] = $this->CountsTmp->find('first', array(
				'conditions' => array('CountsTmp.area_id' => $this->Session->read('Area.largeArea'))
			));
		}
		else
		{
			$shop_count['Category'] = $this->CountsTmp->find('first', array(
				'conditions' => array('CountsTmp.area_id' => '_'. $this->Session->read('Area.smallArea'))
			));
		}
		$this->set('shop_count', $shop_count);



		//head
		$this->set('title_for_layout', $this->title_tag_common . '検索結果（' . $search_type_str . '）');
		$this->set('meta_keywords', $this->meta_keywords_common .  ',検索結果（' . $search_type_str . '）');
		$this->set('meta_description', $this->meta_description_common . '検索結果（' . $search_type_str . '）');
		$this->set('header_one', $this->h1_tag_common . '検索結果（' . $search_type_str . '）');

		$this->render($this->device_file_name . $search_type);
	}

	function s_index() {
		$this->index();
	}
}
?>
