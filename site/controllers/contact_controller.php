<?php
class ContactController extends AppController {
	var $name = 'Contact';
	var $uses = array('Contact');
	const SESSION_CONTACT_DATA_KEY = 'ContactController.index.contact';

	function beforeFilter(){
		parent::beforeFilter();
		$this->layout = 'login';
		switch($this->params['action'])
		{
			case 'index':
				$this->addBreadCrumbs("お問い合わせ" , "/contact");
				break;
		}
	}

	function index(){
		$umu_select = array(0 => '有', 1 => '無');
		$this->set('umu_select', $umu_select);
		//head
		$this->set('title_for_layout', $this->title_tag_common . ' お問い合わせ');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',お問い合わせ');
		$this->set('meta_description', $this->meta_description_common . 'お問い合わせ');
		$this->set('header_one', $this->h1_tag_common . 'お問い合わせ');
		if(!empty($this->data)) {
			$mode = $this->data['Contact']['mode'];
			switch($mode) {
				// 有料掲載希望
				case 'payment_insert':
					$form_name = '有料掲載希望';
					$this->Contact->validate = $this->Contact->validate_payment_insert;
					break;

				// 無料掲載希望
					case 'free_insert':
					$this->Contact->validate = $this->Contact->validate_free_insert;
					$form_name = '無料掲載希望';
					break;

				// 代理店契約
					case 'agency_agreement':
					$this->Contact->validate = $this->Contact->validate_agency_agreement;
					$form_name = '代理店契約';
					break;

				// その他
					case 'other':
					$this->Contact->validate = $this->Contact->validate_other;
					$form_name = 'その他';
					break;

				default:
					$this->cakeError('error404');
					break;

			}
			if(isset($this->params["form"]["confirm"])) {

				// 確認画面
				$this->Contact->set($this->data);

				if($this->Contact->validates()) {
					// 画像が存在するかチェック
					if (isset($this->data['Contact']['sendfile']['tmp_name'])){
						if(is_uploaded_file($this->data['Contact']['sendfile']['tmp_name']))
						{
							// 拡張子をチェック
							$ext = '.png';
							if ($this->data['Contact']['sendfile']['type'] == 'image/jpeg') $ext = '.jpg';

							// imgの下に格納
							$imgpath = dirname(__FILE__). '/../../img/contact/';
							$nowdate = date('Ymd',strtotime('now'));
							if (!file_exists($imgpath.$nowdate)) mkdir($imgpath.$nowdate);
							$filename = end(explode('/', $this->data['Contact']['sendfile']['tmp_name'])) . $ext;
							move_uploaded_file( $this->data['Contact']['sendfile']['tmp_name'], $imgpath . $nowdate. '/' . $filename); // アップロードしたファイル
							// img.dhrank.comを作成
							$domain_arr = explode('.',$_SERVER['SERVER_NAME']);
							$img_domain = 'http://img.' . $domain_arr[1] . '.' . $domain_arr[2] . '/contact/' . $nowdate . '/' . $filename;
							$this->data['imgfile'] = $img_domain;
						}
					}
					// 確認画面をレンダリング
					$this->render($this->device_file_name . 'confirm');
					// 一旦Sessionに持つ
					$this->Session->write(self::SESSION_CONTACT_DATA_KEY, $this->data);
				}

			} else if(isset($this->params["form"]["register"])) {

				// 完了画面
				if($this->Session->check(self::SESSION_CONTACT_DATA_KEY)) {
					// セッションの値を取得
					$this->data = $this->Session->read(self::SESSION_CONTACT_DATA_KEY);
				}
/*
				// テスト
				$to      = 'kazuya.misono@livline.jp';
				$subject = 'subject';
				$message = 'hello';
				$headers = 'From: contact@19800210.com' . "\r\n" .
				    	'Reply-To: contact@19800210.com' . "\r\n" .
				    	'X-Mailer: PHP/' . phpversion();

				mail($to, $subject, $message, $headers);
exit;
*/
				//投稿画面へ戻る
				$this->set('data', $this->data);

				// メールアドレス
				$email = '';
				if (isset($this->data['Contact']['emailto'])) $email = $this->data['Contact']['emailto'];
				if (isset($this->data['Contact']['emailto2'])) $email = $this->data['Contact']['emailto2'];
				if (isset($this->data['Contact']['emailto3'])) $email = $this->data['Contact']['emailto3'];
				if (isset($this->data['Contact']['emailto4'])) $email = $this->data['Contact']['emailto4'];

				// 投稿者へ確認メール
				$mailOptions = array(
//					'to' => $this->data['Contact']['emailto2'],
					'to' => $email,
					'from' => VALID_CONTACTS_EMAIL,
					'subject' => 'お問い合わせ（' . $form_name . '）を承りました。',
					'smtpOptionsKey' => 'smtpOptionRegisterUrlsend',
//					'smtpOptionsKey' => Configure::read('smtpOptionRegisterUrlsend'),
					'params' => array(
						'data' =>  $this->data,
						'umu_select' => $umu_select,
					),
					'template' => 'contact_' . $mode . '_customer',

				);

				$this->send_mail($mailOptions);

				// 管理者へメール
				$mailOptions = array(
					'to' => VALID_CONTACTS_EMAIL,
					'from' => VALID_CONTACTS_EMAIL,
					'subject' => 'お問い合わせ（' . $form_name . '）を受信しました',
					'smtpOptionsKey' => 'smtpOptionRegisterUrlsend',
					'params' => array(
						'data' =>  $this->data,
						'umu_select' => $umu_select,
					),
					'template' => 'contact_' . $mode . '_staff',
				);

				$this->send_mail($mailOptions);
				$this->redirect($this->device_path . '/contact/end');
			}
		}
	}

	function s_index() {
		$this->index();
	}

	function end() {		//head
		$this->set('title_for_layout', $this->title_tag_common . ' お問い合わせ送信完了画面');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',お問い合わせ,送信完了');
		$this->set('meta_description', $this->meta_description_common . 'お問い合わせ送信完了画面');
		$this->set('header_one', $this->h1_tag_common . 'お問い合わせ送信完了画面');
	}

	function s_end() {
		$this->end();
	}

}
?>
