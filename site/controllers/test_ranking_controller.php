<?php

class TestRankingController extends AppController {
	var $name = 'TestRanking';
	var $uses = array('TestRanking','Shop','Review','LargeArea','Reviewer');
	$this->autoRender = false;

	// エリア内店舗、カテゴリ別ランキング取得
	function index($category_id = '' ,$limit = 5)
	{
		$looks_datas = $this->TestRanking->looks('', $category_id, $this->parent_area['LargeArea']['id'], $limit);
		$service_datas = $this->TestRanking->service('', $category_id, $this->parent_area['LargeArea']['id'], $limit);
		$character_datas = $this->TestRanking->character('', $category_id, $this->parent_area['LargeArea']['id'], $limit);

		$this->set('looks_datas', $looks_datas);
		$this->set('service_datas', $service_datas);
		$this->set('character_datas', $character_datas);
	}

	function reviewer()
	{
		$this->layout = '';
		$this->autoRender = false;

		// 口コミ数集計
		$this->TestRanking->reviewer();
	}

	function access_count()
	{
		// アクセスカウント集計
		$this->layout = '';
		$this->autoRender = false;

		$this->TestRanking->access_count();

	}

	// 口コミランキングデータ取得
	function get_reviewer_ranking()
	{
		$this->layout = '';
		$this->autoRender = false;

		$datas = $this->TestRanking->getReviewerRankingData();
	}


	// アクセスカウントランキングデータ取得
	function get_access_ranking()
	{
		$this->layout = '';
		$this->autoRender = false;

		$datas = $this->TestRanking->getAccessCountData();

	}

}
?>
