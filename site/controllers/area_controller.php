<?php
class AreaController extends AppController {
	var $name = 'Area';
	var $uses = array('TestRanking', 'AreaIndexLink','Review','Shop', 'User', 'Girl', 'MShopsBusinessCategory', 'Coupon', 'ShopInfo', 'LargeArea', 'SmallArea', 'CountsTmp', 'TestRanking');
	var $components = array('Testranking');

	function index()
	{
		$id = (isset($this->params['small_area_id']))? $this->params['small_area_id'] : 0;
		if($id)
		{
			$this->Session->write('Area.largeArea', $this->parent_area['LargeArea']['id']);
			$this->Session->write('Area.smallArea', $id);
		}
		else
		{
			$this->Session->write('Area.largeArea', $this->parent_area['LargeArea']['id']);
			$this->Session->write('Area.smallArea', 0);
		}


		//地域名取得
		if(empty($id))
		{
			$small_area = $this->parent_area['LargeArea'];
			$this->Session->write('Area.Name', $small_area['name']);
		}
		else
		{
			$small_area = array_filter($this->parent_area['SmallArea'], function($arr) use($id){
				if($arr['id'] == $id)
				{
					return array_values($arr);
				}
			});
		$small_area = array_values($small_area);
		$this->Session->write('Area.Name', $small_area[0]['name']);
		}
		$this->set('small_area', $small_area);

		//パンクズ取得
		if ($this->Session->read('Area.smallArea')!==0) {
			$this->addBreadCrumbs($this->Session->read('Area.Name'), "/area/" . $this->Session->read('Area.smallArea'));
		}

		//店舗絞り込み
		if(!empty($id))
		{
			$s_area = $this->CountsTmp->smallCounts($id);
			foreach ($s_area as $s)
			{
				$shops = $s['shops'];
			}
		}
		else
		{
			$l_area = $this->CountsTmp->largeCounts($this->Session->read('Area.largeArea'));
			foreach ($l_area as $l )
			{
				$shops = $l['shops'];
			}
		}
		$this->set('shops', $shops);

		// ピックアップ口コミ情報取得
		$this->get_pickup_review();
		//ページネーション
		if (!empty($id))
		{
			$this->paginate = array('Shop' => array(
				'fields' => array('User.*','UsersAuthority.*','Shop.*', 'System.*', 'LargeArea.url',
					'(SELECT count(*) FROM girls WHERE User.id = girls.user_id) AS girl_cnt',
					'(SELECT count(*) FROM reviews WHERE User.id = reviews.user_id) AS review_cnt',
				),
				'conditions' => array(
					'Shop.is_created' => 1,
					'User.large_area_id' => $this->Session->read('Area.largeArea'),
					'User.small_area_id LIKE' => '%'. $this->Session->read('Area.smallArea') .'%',
				),
				'group' => 'User.id',
				'limit' => $this->pcount['categorylist'],
				'order' => 'UsersAuthority.order_rank ASC, rand()'
			));
		}
		else
		{
			$this->paginate = array('Shop' => array(
				'fields' => array('User.*','UsersAuthority.*','Shop.*', 'System.*', 'LargeArea.url',
					'(SELECT count(*) FROM girls WHERE User.id = girls.user_id) AS girl_cnt',
					'(SELECT count(*) FROM reviews WHERE User.id = reviews.user_id) AS review_cnt',
				),
				'conditions' => array(
					'Shop.is_created' => 1,
					'User.large_area_id' => $this->Session->read('Area.largeArea'),
				),
				'group' => 'User.id',
				'limit' => $this->pcount['categorylist'],
				'order' => 'UsersAuthority.order_rank ASC, rand()'
			));
		}
		$this->set('list', $this->paginate($this->Shop));

		//お店のお知らせ取得
		$info = $this->ShopInfo->getInfo($this->Session->read('Area.largeArea'));
		$this->set('info', $info);

		//エリア取得
		$area = $this->LargeArea->find('all', array(
			'conditions' => array('id' => $this->parent_area['LargeArea']['id']),
			'fields' => array('id', 'name', 'area_banner_url')
		));
		$this->set('area', $area);

		//レビュアーランキング取得
		$this->Testranking->load($this);
		$reviewer = $this->Testranking->get_reviewer_ranking($this->Session->read('Area.largeArea'));
		$this->set('reviewer', $reviewer);

		//ショップカウント数取得
		$area_id[] = $area[0]['LargeArea']['id'];
		foreach ($area[0]['SmallArea'] as $s)
		{
			$area_id[] = '_'. $s['id'];
		}
		$shop_count = $this->CountsTmp->find('all', array(
			'conditions' => array('area_id' => $area_id)
		));
		if($this->Session->read('Area.smallArea')==0)
		{
			$shop_count['Category'] = $this->CountsTmp->find('first', array(
				'conditions' => array('CountsTmp.area_id' => $this->Session->read('Area.largeArea'))
			));
		}
		else
		{
			$shop_count['Category'] = $this->CountsTmp->find('first', array(
				'conditions' => array('CountsTmp.area_id' => '_'. $this->Session->read('Area.smallArea'))
			));
		}
		$this->set('shop_count', $shop_count);
		//head
		$this->set('title_for_layout', $this->title_tag_common . 'お店一覧');
		$this->set('meta_keywords', $this->meta_keywords_common .  ',お店一覧');
		$this->set('meta_description', $this->meta_description_common . 'お店一覧');
		$this->set('header_one', $this->h1_tag_common . 'お店一覧');
	}

	function s_index() {
		$this->index();
	}

}
?>
