<?php
class LoginController extends AppController {
	var $name = 'Login';
	var $uses = array('Changepassword','Shop','Passport');
	var $components = array('Cookie','Email');
	public $expires = 2592000; //60 * 60 * 24 * 30

	function index(){
		//かな読み込み、セット
		$kana_list = Configure::read('kana');
		$this->set('kana_list',$kana_list);

		//ショップデータに口コミデータをバインド
		$bind = array('hasOne' => array('Review' => array(
				'className'=>'Review',
				'fields' => array('COUNT(Review.id) AS reviews_count'),
				'conditions' => 'Review.user_id = User.id',
				'foreignKey' => false)));
		//あかさたな別にショプデータ取得
		foreach($kana_list as $key => $value){
			$this->Shop->bindModel($bind,false);
			$shops = $this->Shop->find('all',array(
					'fields' => array('Shop.name','MShopsBusinessCategory.name', 'User.id', 'LargeArea.url','COUNT(Review.id) AS reviews_count'),
					'conditions' => array(
							$this->getCurrentAreaCondition(),
							'SUBSTRING(User.kana,1,1)' => $value,),
					'group' => array('Shop.user_id'),
					'order' => 'User.kana ASC'));
			$list[$key] = $shops;
		}
		$this->set('list',$list);
		$this->get_shops_index_common();
	}
		
	//start---2013/3/13 障害No.1-6-0003修正
	function s_index(){
		$this->redirect('/s/top');
	}
	//end---2013/3/13 障害No.1-6-0003修正
	
	function reviewer(){
		$this->render('reviewer');
	}
	
	function shop(){
		$this->render('shop');
	}
	
	function shop_login(){
		
		$this->redirect('/manager');
		/*
		$user = $this->Auth->user();

		if (!empty($this->data) && !empty($user)){
			//モバイルの場合はパスポート保存処理をしない
			if(!empty($this->data['User']['remember_me'])){
				//パスポートを保存するか
				if ($this->data['User']['remember_me']) {
					$this->passportWrite($user);
				} else {
					$this->passportDelete($user);
				}
				unset($this->data['User']['remember_me']);
			}
		}
		
		if ($user){
			$this->redirect($this->device_path . '/');
		}else{
			$cookie_user = $this->Cookie->Read('User');

			if ($cookie_user) {
				//パスポートでログイン
				$options = array(
						'conditions' => array(
								'Passport.passport'   => $cookie_user['passport'],
								'Passport.modified >' => date('Y-m-d H:i:s', $this->expires)
						));
				$passport = $this->Passport->find('first', $options);
				if ($passport){
					$user['username'] = $passport['User']['username'];
					$user['password'] = $passport['User']['password'];

					if ($this->Auth->login($user)) {
						$this->passportWrite($passport);
						$this->redirect($this->device_path . '/');
					}
				}
			}
		}
		*/
		
		
		$this->set('title_for_layout', $this->title_tag_common.'店舗会員・ログイン');
		$this->set('header_one', $this->h1_tag_common.'店舗会員・ログイン');
	}

	//meta取得
	private function get_shops_index_common(){
		//head
		$this->set('title_for_layout', $this->title_tag_common . 'お店一覧');
		$this->set('meta_keywords', $this->meta_keywords_common .  ',お店一覧');
		$this->set('meta_description', $this->meta_description_common . 'お店一覧');
		$this->set('header_one', $this->h1_tag_common . 'お店一覧');
	}

	//start---2013/3/13 障害No.1-6-0003修正
	function kana(){
		if(!empty($this->params['pass'][0])){
			$this->redirect('/shops#kana' . $this->params['pass'][0]);
		}
	}
	//end---2013/3/13 障害No.1-6-0003修正


	//かな別お店一覧表示画面（携帯）
	function i_kana($param){

		//ショップデータに口コミデータをバインド
		$bind = array('hasOne' => array('Review' => array(
				'className'=>'Review',
				'fields' => array('COUNT(Review.id) AS reviews_count'),
				'conditions' => 'Review.user_id = User.id',
				'foreignKey' => false)));
		$this->Shop->bindModel($bind,false);

		//かな読み込み
		$kana_list = Configure::read('kana');
		$list = $this->Shop->find('all',array(
				'fields' => array('Shop.name','MShopsBusinessCategory.name', 'User.id', 'LargeArea.url','COUNT(Review.id) AS reviews_count'),
				'conditions' => array(
						$this->getCurrentAreaCondition(),
						'LEFT(User.kana, 1)' => $kana_list[$param]),
				'group' => array('Shop.id'),
				'order' => 'User.kana ASC'));
		$this->set('list',$list);
		$this->set('kana',$kana_list[$param][0]);

		//head
		$this->set('title_for_layout', $this->title_tag_common . 'お店一覧 ' . $kana_list[$param][0] . '行');
		$this->set('meta_keywords', $this->meta_keywords_common .  ',お店一覧');
		$this->set('meta_description', $this->meta_description_common . 'お店一覧 ' . $kana_list[$param][0] . '行');
		$this->set('header_one', $this->h1_tag_common . 'お店一覧 ' . $kana_list[$param][0] . '行');
	}

	function s_kana($param){
		$this->i_kana($param);
	}
}
?>
