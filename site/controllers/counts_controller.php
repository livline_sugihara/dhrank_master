<?php
class CountsController extends AppController {

	var $uses = array('Shop', 'User', 'LargeArea', 'SmallArea', 'CountsTmp');

	function main(){

		$this->CountsTmp->truncate();

		$areas = $this->LargeArea->find('all');

		foreach ($areas as $area)
		{
			$cnt = $this->CountsTmp->largeCounts($area['LargeArea']['id']);

			$this->CountsTmp->create();
			$data['CountsTmp']['area_id'] = $area['LargeArea']['id'];
			$data['CountsTmp']['area_name'] = $area['LargeArea']['name'];
			$data['CountsTmp']['shops_count'] = count($cnt);
			$this->CountsTmp->save($data);

			if (!empty($area['SmallArea']))
			{
				foreach ($area['SmallArea'] as $s)
				{
					$cnt = $this->CountsTmp->smallCounts($s['id']);

					$this->CountsTmp->create();
					$data['CountsTmp']['area_id'] = $s['id'];
					$data['CountsTmp']['area_name'] = $s['name'];
					$data['CountsTmp']['shops_count'] = count($cnt);
					$this->CountsTmp->save($data);
				}
			}

		}
	}


}
?>
