<?php
class SummaryShopShell extends Shell {
//	var $tasks = array('Summary');
	var $uses = array('Shop', 'Review', 'SummaryData');

	var $avg_fields = array(
		'score_girl_character', 'score_girl_service', 'score_girl_looks',
	);

	function main() {

		// 対象データは1週間
		// FIXME レビュー機能が無いため、過去のデータを拾うため。通常は7日??
		$limit_day = date('Y-m-d', strtotime('-' . APP_SUMMARY_REVIEW_RANGE));

		$result = $this->Review->find('all', array(
			'fields' => array(
				'Review.user_id',
				'AVG(Review.score_girl_character) AS score_girl_character',
				'AVG(Review.score_girl_service) AS score_girl_service',
				'AVG(Review.score_girl_looks) AS score_girl_looks',
				'User.large_area_id',
			),
			'conditions' => array(
				'DATE_FORMAT(Review.created,"%Y-%m-%d") >= ' => $limit_day,
			),
			'group' => 'Review.user_id',
			'recursive' => 1,
		));

//		$this->out(var_export($result, true));

		foreach($result AS $key => $record) {

			// 平均値を算出
			$avg_total = 0;
			foreach($this->avg_fields AS $field_name) {
				$avg_total += $record[0][$field_name];
			}
			$avg_total_avg = $avg_total / count($this->avg_fields);

			// 条件部分を作成
			$conditions = array(
				'summary_type' => SummaryData::SUMMARY_TYPE_SHOP,
				'large_area_id' => $record['User']['large_area_id'],
				'date' => date('Y-m-d'),
				'relation_id' => $record['Review']['user_id'],
			);

			// データが既にあるかどうかの確認
			$insData = $this->SummaryData->find('first', array(
				'conditions' => $conditions,
			));

			// すでにある場合は更新
			if(empty($insData))
			{
				// 挿入値を作成
				$insData = array('SummaryData' => $conditions);
			}
			$insData['SummaryData']['value'] = $avg_total_avg;
			$insData['SummaryData']['rank'] = SummaryData::RANK_MAX;

			$this->SummaryData->save($insData['SummaryData']);
			$this->SummaryData->create();

//			$this->out(var_export($insData, true));
		}

		// 並び順を更新
		// FIXME ここは他のロジックとまとめたい
		$this->SummaryData->bindModel(array('belongsTo' => array('User' => array(
			'className'=>'User',
			'type'=>'inner',
			'conditions' => array(
				'User.id = SummaryData.relation_id',
			),
			'foreignKey' => false,))),false);

		$this->SummaryData->bindModel(array('belongsTo' => array('UsersAuthority' => array(
			'className'=>'UsersAuthority',
			'type'=>'inner',
			'conditions' => array(
				'UsersAuthority.user_id = SummaryData.relation_id',
				'UsersAuthority.large_area_id = SummaryData.large_area_id',
			),
			'foreignKey' => false,))),false);

		$result = $this->SummaryData->find('all', array(
			'conditions' => array(
				'summary_type' => SummaryData::SUMMARY_TYPE_SHOP,
				'date' => date('Y-m-d'),
				'rank' => SummaryData::RANK_MAX,
			),
			'order' => 'SummaryData.large_area_id ASC, SummaryData.value DESC, UsersAuthority.order_rank ASC',
		));
		$this->SummaryData->unbindModel(array('belongsTo' => array('User')));

		$rank = 0;
		$oldValue = 0;
		$large_area_id = 0;
		$area_cnt = 0;
		foreach($result AS $ii => $record) {

			// 前回のランクを取得
			$prev_data = $this->SummaryData->find('first', array(
				'conditions' => array(
					'SummaryData.summary_type' => SummaryData::SUMMARY_TYPE_SHOP,
					'SummaryData.large_area_id' => $record['SummaryData']['large_area_id'],
					'DATE_FORMAT(SummaryData.date,"%Y-%m-%d") < ' => date('Y-m-d'),
					'SummaryData.relation_id' => $record['SummaryData']['relation_id'],
				),
				'order' => 'SummaryData.date DESC'
			));

			if(empty($prev_data)) {
				$prev_rank = SummaryData::RANK_MAX;
			} else {
				$prev_rank = $prev_data['SummaryData']['rank'];
			}

			// エリアIDが違う場合は次のエリアに移行
			if($record['SummaryData']['large_area_id'] != $large_area_id) {
				$rank = 0;
				$oldValue = 0;
				$large_area_id = $record['SummaryData']['large_area_id'];
				$area_cnt = 0;
			}

			// 同立以外はカウントアップ
			if($oldValue != $record['SummaryData']['value']) {
				$rank = $area_cnt + 1;
				$oldValue = $record['SummaryData']['value'];
			}

			$record['SummaryData']['rank'] = $rank;
			$record['SummaryData']['prev_rank'] = $prev_rank;
			$this->SummaryData->save($record['SummaryData']);
			$this->SummaryData->create();

			$area_cnt++;
		}
	}
}
?>