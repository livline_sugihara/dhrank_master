<?php
class ReviewersCountsShell extends Shell {

	public $uses = array('TestRanking','LargeArea');

	// 実行関数
	function main()
	{
		// レビュワーランキング作成処理
		$areas = $this->LargeArea->find('all');

		foreach ($areas as $area)
		{
			$this->TestRanking->reviewer_count($area['LargeArea']['id'], 5, 7, true);
		}
	}
}
?>
