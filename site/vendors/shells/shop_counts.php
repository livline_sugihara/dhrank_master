<?php

App::import('Core', 'Controller');

class ShopCountsShell extends Shell {

	public $uses = array('Shop', 'User', 'LargeArea', 'SmallArea', 'CountsTmp');
	public $Controller;

	function main()
	{
		// $this->CountsTmp->create();
		// $data['CountsTmp']['name'] = 'test';
		// $this->CountsTmp->save($data);

		$this->CountsTmp->truncate();

		$areas = $this->LargeArea->find('all');

		foreach ($areas as $area)
		{
			//大エリア取得
			$cnt = $this->CountsTmp->largeCounts($area['LargeArea']['id']);
			$cate1 = $this->CountsTmp->categoryLCounts($area['LargeArea']['id'], 0, 1);
			$cate2 = $this->CountsTmp->categoryLCounts($area['LargeArea']['id'], 0, 2);
			$cate3 = $this->CountsTmp->categoryLCounts($area['LargeArea']['id'], 0, 3);
			$cate4 = $this->CountsTmp->categoryLCounts($area['LargeArea']['id'], 0, 4);
			$cate5 = $this->CountsTmp->categoryLCounts($area['LargeArea']['id'], 0, 5);
			$cate6 = $this->CountsTmp->categoryLCounts($area['LargeArea']['id'], 0, 6);
			$cate7 = $this->CountsTmp->categoryLCounts($area['LargeArea']['id'], 0, 7);
			$cate1000 = $this->CountsTmp->categoryLCounts($area['LargeArea']['id'], 0, 1000);

			$this->CountsTmp->create();
			$data['CountsTmp']['area_id'] = $area['LargeArea']['id'];
			$data['CountsTmp']['area_name'] = $area['LargeArea']['name'];
			$data['CountsTmp']['shops_count'] = count($cnt);
			$data['CountsTmp']['category_1'] = count($cate1);
			$data['CountsTmp']['category_2'] = count($cate2);
			$data['CountsTmp']['category_3'] = count($cate3);
			$data['CountsTmp']['category_4'] = count($cate4);
			$data['CountsTmp']['category_5'] = count($cate5);
			$data['CountsTmp']['category_6'] = count($cate6);
			$data['CountsTmp']['category_7'] = count($cate7);
			$data['CountsTmp']['category_1000'] = count($cate1000);
			$this->CountsTmp->save($data);

			if (!empty($area['SmallArea']))
			{
				foreach ($area['SmallArea'] as $s)
				{
					$cnt = $this->CountsTmp->smallCounts($s['id']);
					$cate1 = $this->CountsTmp->categorySCounts($area['LargeArea']['id'], $s['id'], 1);
					$cate2 = $this->CountsTmp->categorySCounts($area['LargeArea']['id'], $s['id'], 2);
					$cate3 = $this->CountsTmp->categorySCounts($area['LargeArea']['id'], $s['id'], 3);
					$cate4 = $this->CountsTmp->categorySCounts($area['LargeArea']['id'], $s['id'], 4);
					$cate5 = $this->CountsTmp->categorySCounts($area['LargeArea']['id'], $s['id'], 5);
					$cate6 = $this->CountsTmp->categorySCounts($area['LargeArea']['id'], $s['id'], 6);
					$cate7 = $this->CountsTmp->categorySCounts($area['LargeArea']['id'], $s['id'], 7);
					$cate1000 = $this->CountsTmp->categorySCounts($area['LargeArea']['id'], $s['id'], 1000);

					$this->CountsTmp->create();
					$data['CountsTmp']['area_id'] = '_'. $s['id'];
					$data['CountsTmp']['area_name'] = $s['name'];
					$data['CountsTmp']['shops_count'] = count($cnt);
					$data['CountsTmp']['category_1'] = count($cate1);
					$data['CountsTmp']['category_2'] = count($cate2);
					$data['CountsTmp']['category_3'] = count($cate3);
					$data['CountsTmp']['category_4'] = count($cate4);
					$data['CountsTmp']['category_5'] = count($cate5);
					$data['CountsTmp']['category_6'] = count($cate6);
					$data['CountsTmp']['category_7'] = count($cate7);
					$data['CountsTmp']['category_1000'] = count($cate1000);
					$this->CountsTmp->save($data);
				}
			}
		}

	}


}
?>
