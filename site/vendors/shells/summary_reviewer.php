<?php
class SummaryReviewerShell extends Shell {
//	var $tasks = array('Summary');
	var $uses = array('Reviewer', 'Review', 'ReviewsGoodCount', 'SummaryData');

	function main() {

		// メモリを256M使う
		ini_set('memory_limit' ,'256M');

		// 対象データは1週間
		$limit_day = date('Y-m-d', strtotime('-' . APP_SUMMARY_REVIEW_RANGE));
		$today = date('Y-m-d');

		$this->Review->bindModel(array('hasOne' => array('ReviewsGoodCount' => array(
			'className'=>'ReviewsGoodCount',
			'type'=>'inner',
			'fields' => array(
				'ReviewsGoodCount.review_id',
				'count(ReviewsGoodCount.id) AS good_count'
			),
			'group' => 'ReviewsGoodCount.review_id',
			'conditions' => array(
				'DATE_FORMAT(ReviewsGoodCount.created,"%Y-%m-%d") >= ' => $limit_day,
			),
			'foreignKey' => 'review_id',))),false);

		// $this->Review->bindModel(array('belongsTo' => array('Reviewer' => array(
		// 	'className'=>'Reviewer',
		// 	'type'=>'inner',
		// 	'conditions' => array(
		// 	),
		// 	'foreignKey' => 'reviewer_id',))),false);

		$result = $this->Review->find('all', array(
			'fields' => array(
				'Review.reviewer_id',
				'Reviewer.*',
				'count(Review.reviewer_id) AS reviewer_count'
			),
			'conditions' => array(
				'Review.reviewer_id !=' => 0,
				'Review.delete_flag <= ' => Review::DELETE_FLAG_SAVE_POINTS,
				'Review.is_publish' => 1,
				'DATE_FORMAT(Review.created,"%Y-%m-%d") >= ' => $limit_day,
			),
			'group' => 'Review.reviewer_id'
		));

		// $this->out(var_export($result, true));
		// $this->Summary->execute();

		foreach($result AS $key => $record) {

			// 条件部分を作成
			$conditions = array(
				'summary_type' => SummaryData::SUMMARY_TYPE_REVIEWER,
//				'large_area_id' => SummaryData::LARGE_AREA_ID_NONE,
				'date' => date('Y-m-d'),
				'relation_id' => $record['Review']['reviewer_id'],
			);

			// データが既にあるかどうかの確認
			$insData = $this->SummaryData->find('first', array(
				'conditions' => $conditions,
			));

			// すでにある場合は更新
			if(empty($insData))
			{
				// 挿入値を作成
				$insData = array('SummaryData' => $conditions);
			}
			$insData['SummaryData']['large_area_id'] = $record['Reviewer']['large_area_id'];
			$insData['SummaryData']['value'] = $record[0]['reviewer_count'];
			$insData['SummaryData']['rank'] = SummaryData::RANK_MAX;

			$this->SummaryData->save($insData['SummaryData']);
			$this->SummaryData->create();

			// $this->out(var_export($insData, true));
		}

		// 並び順を更新
		$result = $this->SummaryData->find('all', array(
			'conditions' => array(
				'summary_type' => SummaryData::SUMMARY_TYPE_REVIEWER,
				'date' => date('Y-m-d'),
				'rank' => SummaryData::RANK_MAX,
			),
			'order' => 'SummaryData.large_area_id ASC, SummaryData.value DESC, SummaryData.relation_id DESC',
		));

		$rank = 0;
		$oldValue = 0;
		$large_area_id = 0;
		$area_cnt = 0;
		foreach($result AS $ii => $record) {

			// 前回のランクを取得
			$prev_data = $this->SummaryData->find('first', array(
				'conditions' => array(
					'SummaryData.summary_type' => SummaryData::SUMMARY_TYPE_REVIEWER,
					'DATE_FORMAT(SummaryData.date,"%Y-%m-%d") < ' => $today,
					'SummaryData.relation_id' => $record['SummaryData']['relation_id'],
				),
				'order' => 'SummaryData.date DESC'
			));

			if(empty($prev_data)) {
				$prev_rank = SummaryData::RANK_MAX;
			} else {
				$prev_rank = $prev_data['SummaryData']['rank'];
			}

			// エリアIDが違う場合は次のエリアに移行
			if($record['SummaryData']['large_area_id'] != $large_area_id) {
				$rank = 0;
				$oldValue = 0;
				$large_area_id = $record['SummaryData']['large_area_id'];
				$area_cnt = 0;
			}

			// 同立以外はカウントアップ
			if($oldValue != $record['SummaryData']['value']) {
				$rank = $area_cnt + 1;
				$oldValue = $record['SummaryData']['value'];
			}

			$record['SummaryData']['rank'] = $rank;
			$record['SummaryData']['prev_rank'] = $prev_rank;
			$this->SummaryData->save($record['SummaryData']);
			$this->SummaryData->create();

			$area_cnt++;
		}
	}
}
?>