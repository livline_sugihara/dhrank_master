<?php
class ShopsCountsShell extends Shell {

	public $uses = array('TestRanking','LargeArea');

	// 実行関数
	function main()
	{
		// お店の口コミランキング作成処理
		$areas = $this->LargeArea->find('all');

		foreach ($areas as $area)
		{
			$this->TestRanking->shops_count($area['LargeArea']['id'], 5, 7, true);
		}
	}
}
?>
