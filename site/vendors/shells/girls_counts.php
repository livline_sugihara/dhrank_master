<?php
class GirlsCountsShell extends Shell {

	public $uses = array('TestRanking','LargeArea');

	// 実行関数
	function main()
	{
		// アクセスの多い女の子ランキング作成処理
		$areas = $this->LargeArea->find('all');

		foreach ($areas as $area)
		{
			$this->TestRanking->girls_count($area['LargeArea']['id'],5,7, true);
		}
	}
}
?>
