<?php

//INDEX
Router::connect('/', array('controller' => 'top', 'action' => 'index'));
Router::connect('/i/', array('controller' => 'top', 'action' => 'index', 'prefix' => 'i', 'i'=>true));
Router::connect('/s/', array('controller' => 'top', 'action' => 'index', 'prefix' => 's', 's'=>true));
Router::connect('/view:pc', array('controller' => 'top', 'action' => 'index'));
Router::connect('/s/view:s', array('controller' => 'top', 'action' => 'index', 'prefix' => 's', 's'=>true));

//TOP
Router::connect('/top/*', array('controller' => 'top', 'action' => 'top'));
Router::connect('/i/top/*', array('controller' => 'top', 'action' => 'top', 'prefix' => 'i', 'i'=>true));
Router::connect('/s/top/*', array('controller' => 'top', 'action' => 'top', 'prefix' => 's', 's'=>true));

//業種別ランキング
Router::connect('/ranking/category/:business_category_id/*', array('controller' => 'ranking', 'action' => 'category'), array('business_category_id' => '[0-9]+'));
Router::connect('/i/ranking/category/:business_category_id/*', array('controller' => 'ranking', 'action' => 'category', 'prefix' => 'i', 'i'=>true), array('business_category_id' => '[0-9]+'));
Router::connect('/s/ranking/category/:business_category_id/*', array('controller' => 'ranking', 'action' => 'category', 'prefix' => 's', 's'=>true), array('business_category_id' => '[0-9]+'));

//女の子詳細画面
Router::connect('/:user_id/girls/:girl_id/*', array('controller' => 'users', 'action' => 'girl'), array('user_id' => '[0-9]+','girl_id' => '[0-9]+'));
Router::connect('/i/:user_id/girls/:girl_id', array('controller' => 'users', 'action' => 'girl', 'prefix' => 'i', 'i'=>true), array('user_id' => '[0-9]+','girl_id' => '[0-9]+'));
Router::connect('/s/:user_id/girls/:girl_id/*', array('controller' => 'users', 'action' => 'girl', 'prefix' => 's', 's'=>true), array('user_id' => '[0-9]+','girl_id' => '[0-9]+'));

//女の子一覧
Router::connect('/:user_id/girls/*', array('controller' => 'users', 'action' => 'girls'), array('user_id' => '[0-9]+'));
Router::connect('/i/:user_id/girls/*', array('controller' => 'users', 'action' => 'girls', 'prefix' => 'i', 'i'=>true), array('user_id' => '[0-9]+'));
Router::connect('/s/:user_id/girls/*', array('controller' => 'users', 'action' => 'girls', 'prefix' => 's', 's'=>true), array('user_id' => '[0-9]+'));

//口コミ詳細表示画面
Router::connect('/:user_id/review/:review_id/*', array('controller' => 'users', 'action' => 'review'), array('user_id' => '[0-9]+','review_id' => '[0-9]+'));
Router::connect('/i/:user_id/review/:review_id/*', array('controller' => 'users', 'action' => 'review', 'prefix' => 'i', 'i'=>true), array('user_id' => '[0-9]+','review_id' => '[0-9]+'));
Router::connect('/s/:user_id/review/:review_id/*', array('controller' => 'users', 'action' => 'review', 'prefix' => 's', 's'=>true), array('user_id' => '[0-9]+','review_id' => '[0-9]+'));

//口コミ一覧表示画面
Router::connect('/:user_id/reviews/*', array('controller' => 'users', 'action' => 'reviews'), array('user_id' => '[0-9]+'));
Router::connect('/i/:user_id/reviews/*', array('controller' => 'users', 'action' => 'reviews', 'prefix' => 'i', 'i'=>true), array('user_id' => '[0-9]+'));
Router::connect('/s/:user_id/reviews/*', array('controller' => 'users', 'action' => 'reviews', 'prefix' => 's', 's'=>true), array('user_id' => '[0-9]+'));

//システム
Router::connect('/:user_id/systems/*', array('controller' => 'users', 'action' => 'systems'), array('user_id' => '[0-9]+'));
Router::connect('/i/:user_id/systems', array('controller' => 'users', 'action' => 'systems', 'prefix' => 'i', 'i'=>true), array('user_id' => '[0-9]+'));
Router::connect('/s/:user_id/systems/*', array('controller' => 'users', 'action' => 'systems', 'prefix' => 's', 's'=>true), array('user_id' => '[0-9]+'));

//クーポン一覧
Router::connect('/:user_id/coupons/*', array('controller' => 'users', 'action' => 'coupons'), array('user_id' => '[0-9]+'));
Router::connect('/i/:user_id/coupons/*', array('controller' => 'users', 'action' => 'coupons', 'prefix' => 'i', 'i'=>true), array('user_id' => '[0-9]+'));
Router::connect('/s/:user_id/coupons/*', array('controller' => 'users', 'action' => 'coupons', 'prefix' => 's', 's'=>true), array('user_id' => '[0-9]+'));

//店舗TOP
Router::connect('/:user_id/*', array('controller' => 'users', 'action' => 'index'), array('user_id' => '[0-9]+'));
Router::connect('/i/:user_id/*', array('controller' => 'users', 'action' => 'index', 'prefix' => 'i', 'i'=>true), array('user_id' => '[0-9]+'));
Router::connect('/s/:user_id/*', array('controller' => 'users', 'action' => 'index', 'prefix' => 's', 's'=>true), array('user_id' => '[0-9]+'));

// カテゴリーページ
Router::connect('/category/:business_category_id/*', array('controller' => 'category', 'action' => 'index'), array('business_category_id' => '[0-9]+'));
Router::connect('/i/category/:business_category_id/*', array('controller' => 'category', 'action' => 'index', 'prefix' => 'i', 'i'=>true), array('business_category_id' => '[0-9]+'));
Router::connect('/s/category/:business_category_id/*', array('controller' => 'category', 'action' => 'index', 'prefix' => 's', 's'=>true), array('business_category_id' => '[0-9]+'));

// Myレビュー 編集
Router::connect('/mypage/myreview_edit/:review_id/*', array('controller' => 'mypage', 'action' => 'myreview_edit'), array('review_id' => '[0-9]+'));
Router::connect('/i/mypage/myreview_edit/:review_id/*', array('controller' => 'mypage', 'action' => 'myreview_edit', 'prefix' => 'i', 'i'=>true), array('review_id' => '[0-9]+'));
Router::connect('/s/mypage/myreview_edit/:review_id/*', array('controller' => 'mypage', 'action' => 'myreview_edit', 'prefix' => 's', 's'=>true), array('review_id' => '[0-9]+'));

// 口コミ通報
Router::connect('/review/report/:review_id/*', array('controller' => 'review', 'action' => 'report'), array('review_id' => '[0-9]+'));
Router::connect('/i/review/report/:review_id/*', array('controller' => 'review', 'action' => 'report', 'prefix' => 'i', 'i'=>true), array('review_id' => '[0-9]+'));
Router::connect('/s/review/report/:review_id/*', array('controller' => 'review', 'action' => 'report', 'prefix' => 's', 's'=>true), array('review_id' => '[0-9]+'));

// レビュワー プロフィール
Router::connect('/reviewer/profile/:reviewer_id/*', array('controller' => 'reviewer', 'action' => 'profile'), array('reviewer_id' => '[0-9]+'));
Router::connect('/i/reviewer/profile/:reviewer_id/*', array('controller' => 'reviewer', 'action' => 'profile', 'prefix' => 'i', 'i'=>true), array('reviewer_id' => '[0-9]+'));
Router::connect('/s/reviewer/profile/:reviewer_id/*', array('controller' => 'reviewer', 'action' => 'profile', 'prefix' => 's', 's'=>true), array('reviewer_id' => '[0-9]+'));

// レビュワー レビュー一覧
Router::connect('/reviewer/reviews/:reviewer_id/*', array('controller' => 'reviewer', 'action' => 'reviews'), array('reviewer_id' => '[0-9]+'));
Router::connect('/i/reviewer/reviews/:reviewer_id/*', array('controller' => 'reviewer', 'action' => 'reviews', 'prefix' => 'i', 'i'=>true), array('reviewer_id' => '[0-9]+'));
Router::connect('/s/reviewer/reviews/:reviewer_id/*', array('controller' => 'reviewer', 'action' => 'reviews', 'prefix' => 's', 's'=>true), array('reviewer_id' => '[0-9]+'));

// レビュワー フォロー一覧
Router::connect('/reviewer/follow/:reviewer_id/*', array('controller' => 'reviewer', 'action' => 'follow'), array('reviewer_id' => '[0-9]+'));
Router::connect('/i/reviewer/follow/:reviewer_id/*', array('controller' => 'reviewer', 'action' => 'follow', 'prefix' => 'i', 'i'=>true), array('reviewer_id' => '[0-9]+'));
Router::connect('/s/reviewer/follow/:reviewer_id/*', array('controller' => 'reviewer', 'action' => 'follow', 'prefix' => 's', 's'=>true), array('reviewer_id' => '[0-9]+'));

// レビュワー フォロワー一覧
Router::connect('/reviewer/follower/:reviewer_id/*', array('controller' => 'reviewer', 'action' => 'follower'), array('reviewer_id' => '[0-9]+'));
Router::connect('/i/reviewer/follower/:reviewer_id/*', array('controller' => 'reviewer', 'action' => 'follower', 'prefix' => 'i', 'i'=>true), array('reviewer_id' => '[0-9]+'));
Router::connect('/s/reviewer/follower/:reviewer_id/*', array('controller' => 'reviewer', 'action' => 'follower', 'prefix' => 's', 's'=>true), array('reviewer_id' => '[0-9]+'));

// ----- Ajax
// ありがとうを投票（Ajax）
Router::connect('/update_good/:review_id/*', array('controller' => 'ajax', 'action' => 'update_good'), array('review_id' => '[0-9]+'));
Router::connect('/i/update_good/:review_id/*', array('controller' => 'ajax', 'action' => 'update_good', 'prefix' => 'i', 'i'=>true), array('review_id' => '[0-9]+'));
Router::connect('/s/update_good/:review_id/*', array('controller' => 'ajax', 'action' => 'update_good', 'prefix' => 's', 's'=>true), array('review_id' => '[0-9]+'));

// 店舗ブックマーク追加（Ajax）
Router::connect('/update_bookmark_shop/:mode/:user_id/*', array('controller' => 'ajax', 'action' => 'update_bookmark_shop'), array('user_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));
Router::connect('/i/update_bookmark_shop/:mode/:user_id/*', array('controller' => 'ajax', 'action' => 'update_bookmark_shop', 'prefix' => 'i', 'i'=>true), array('user_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));
Router::connect('/s/update_bookmark_shop/:mode/:user_id/*', array('controller' => 'ajax', 'action' => 'update_bookmark_shop', 'prefix' => 's', 's'=>true), array('user_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));

//女の子ブックマーク追加（Ajax）
Router::connect('/update_bookmark_girl/:mode/:girl_id/*', array('controller' => 'ajax', 'action' => 'update_bookmark_girl'), array('girl_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));
Router::connect('/i/update_bookmark_girl/:mode/:girl_id/*', array('controller' => 'ajax', 'action' => 'update_bookmark_girl', 'prefix' => 'i', 'i'=>true), array('girl_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));
Router::connect('/s/update_bookmark_girl/:mode/:girl_id/*', array('controller' => 'ajax', 'action' => 'update_bookmark_girl', 'prefix' => 's', 's'=>true), array('girl_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));

//レビューブックマーク追加（Ajax）
Router::connect('/update_bookmark_review/:mode/:review_id/*', array('controller' => 'ajax', 'action' => 'update_bookmark_review'), array('review_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));
Router::connect('/i/update_bookmark_review/:mode/:review_id/*', array('controller' => 'ajax', 'action' => 'update_bookmark_review', 'prefix' => 'i', 'i'=>true), array('review_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));
Router::connect('/s/update_bookmark_review/:mode/:review_id/*', array('controller' => 'ajax', 'action' => 'update_bookmark_review', 'prefix' => 's', 's'=>true), array('review_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));

// 口コミを削除(Ajax)
Router::connect('/delete_review/:mode/:review_id/*', array('controller' => 'ajax', 'action' => 'delete_review'), array('review_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));
Router::connect('/i/delete_review/:mode/:review_id/*', array('controller' => 'ajax', 'action' => 'delete_review', 'prefix' => 'i', 'i'=>true), array('review_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));
Router::connect('/s/delete_review/:mode/:review_id/*', array('controller' => 'ajax', 'action' => 'delete_review', 'prefix' => 's', 's'=>true), array('review_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));

// フォロワー追加
Router::connect('/update_follow/:mode/:reviewer_id/*', array('controller' => 'ajax', 'action' => 'update_follow'), array('reviewer_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));
Router::connect('/i/update_follow/:mode/:reviewer_id/*', array('controller' => 'ajax', 'action' => 'update_follow', 'prefix' => 'i', 'i'=>true), array('reviewer_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));
Router::connect('/s/update_follow/:mode/:reviewer_id/*', array('controller' => 'ajax', 'action' => 'update_follow', 'prefix' => 's', 's'=>true), array('reviewer_id' => '[0-9]+', 'mode' => '[a-zA-Z0-9]+'));

// スモールエリア
Router::connect('/area/:small_area_id/*', array('controller' => 'area', 'action' => 'index'), array('small_area_id' => '[0-9]+'));
Router::connect('/i/area/:small_area_id/*', array('controller' => 'area', 'action' => 'index', 'prefix' => 'i', 'i'=>true), array('small_area_id' => '[0-9]+'));
Router::connect('/s/area/:small_area_id/*', array('controller' => 'area', 'action' => 'index', 'prefix' => 's', 's'=>true), array('small_area_id' => '[0-9]+'));

//plefix設定
Router::connect('/i/', array('prefix' => 'i', 'i'=>true));
Router::connect('/s/', array('prefix' => 's', 's'=>true));

// テスト用
// 検索用コントローラー
Router::connect('/test/', array('controller' => 'test', 'action' => 'index'));
// (Ajax)データ取得
Router::connect('/test_update/', array('controller' => 'test', 'action' => 'update'));
// ランキング
Router::connect('/test_ranking/', array('controller' => 'test_ranking', 'action' => 'index'));
