<section id="middle" class="narrow">
    <!-- shop list -->
    <?php echo $this->element('common/area_shop_list'); ?>
    <!-- /shop list -->

    <!-- ispotArea -->
    <div id="ispotArea">
        <?php echo $this->element('common/top_pic_review'); ?>
    </div>
    <!-- /ispotArea -->

    <?php echo $this->element('common/top_review_bunner'); ?>

    <!-- categoryArea -->
    <div id="categoryArea">
        <?php echo $this->element('common/top_category'); ?>
    </div>

</section>

<section id="side" class="wide">
    <?php echo $this->element('common/side_wide'); ?>
</section>
