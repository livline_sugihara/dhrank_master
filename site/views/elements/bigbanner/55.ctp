<div class="container">
	<!-- eyecatchArea -->
    <div id="eyecatchArea">
    
		<div class="inner clearfix">
			<div class="left">
				<div id="slider1_wrapper">
					<div id="slider1">
					  <div><img src="/img/left1.jpg" height="210" width="320"></div>
					  <div><img src="/img/left2.jpg" height="210" width="320"></div>
					  <div><img src="/img/left3.jpg" height="210" width="320"></div>
					</div>
				</div>

				<div id="slider2_wrapper">
					<div id="slider2">
					  <?php echo $this->element('sidebar/news'); ?>
					</div>
				</div>
			</div>

			<div class="right">
            	<div id="slider3_wrapper">
                	<div id="slider3">
						<div><img src="/img/right.jpg" alt="" height="300" width="500"></div>
						<div><img src="/img/right.jpg" alt="" height="300" width="500"></div>
						<div><img src="/img/right.jpg" alt="" height="300" width="500"></div>
            		</div>
            	</div>
			</div>
		</div>
        
	</div>
	<!-- / mainArea -->
</div>