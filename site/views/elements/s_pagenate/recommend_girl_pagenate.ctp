<div class="page_navi">
<?php
if($paginator->hasPrev()) {
?>
<p><?php echo $paginator->prev('戻る'); ?></p>
<?php
}
?>

<p><?php echo $paginator->counter(array('format' => '%page%/%pages%')); ?> ページ</p>

<?php
if($paginator->hasNext()) {
?>
<p><?php echo $paginator->next('次へ'); ?></p>
<?php
}
?>
</div>

