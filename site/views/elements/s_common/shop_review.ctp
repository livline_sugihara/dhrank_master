<?php
	$cnt = min(count($pic_review), 1);
	for ($ii = 0; $ii < $cnt; $ii++) {
		echo $this->element('s_common/_pickup_shop', array("record" => $pic_review[$ii]));
	}
?>
<?php // print_r($pic_review); ?>
	<h3 class="label">新着口コミ</h3>
	<section id="pickup_box">
<?php
	$cnt = 0;
	if($parent_shop['UsersAuthority']['is_show_all_contents_by_shop'] == 1) {
	// 有料版はクチコミは2件まで
		$cnt = min(count($new_shop_review), 2);
	} else {
	// 無料版の場合はクチコミを全件表示
		$cnt = count($new_shop_review);
	}
	for ($ii = 0; $ii < $cnt; $ii++) {
		echo $this->element('s_common/_shop_review', array("record" => $new_shop_review[$ii]));
	}
?>
	</section><!-- /#pickup_box-->
