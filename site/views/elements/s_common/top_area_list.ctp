<section class="top_area_list">

<p>
    エリアから探す　//todo:No.4 要デザイン
</p>

<ul class="list-group">
<h2></h2>

<li class="list-group-item">関東</li><ul>

<li class="list-group-item"><a href="http://shinjyuku.dhrank.com" class="btn_on">新宿</a></li>

<li class="list-group-item"><a href="http://ikebukuro.dhrank.com" class="btn_on">池袋</a></li>

<li class="list-group-item"><a href="http://shibuya.dhrank.com" class="btn_on">渋谷</a></li>

<li class="list-group-item"><a href="http://shinagawagotanda.dhrank.com" class="btn_on">品川･五反田</a></li>

<li class="list-group-item"><a href="http://nishitokyo.dhrank.com/" class="btn_on">西東京</a></li>

<li class="list-group-item"><a href="http://kinshicho.dhrank.com" class="btn_on">錦糸町</a></li>

<li class="list-group-item"><a href="http://uenouguisudani.dhrank.com" class="btn_on">上野･鴬谷</a></li>

<li class="list-group-item"><a href="http://kanagawa.dhrank.com" class="btn_on">神奈川</a></li>

<li class="list-group-item"><a href="http://saitama.dhrank.com" class="btn_on">埼玉</a></li>

<li class="list-group-item"><a href="http://chiba.dhrank.com" class="btn_on">千葉</a></li>



<li class="list-group-item"><a href="http://gunma.dhrank.com" class="btn_on">群馬</a></li>

<li class="list-group-item"><a href="http://ibaraki.dhrank.com" class="btn_on">茨城</a></li>

<li class="list-group-item"><a href="http://tochigi.dhrank.com" class="btn_on">栃木</a></li>


</ul>

</ul>

<br class="clear" style="margin-bottom:10px;" />

<ul class="list-group">
<li class="list-group-item">北海道・東北</li>
<ul>

<li class="list-group-item"><a href="http://hokkaido.dhrank.com/" class="btn_on">北海道(札幌)</a></li>

<span class="btn_off">旭川</span>

<span class="btn_off">帯広</span>

<span class="btn_off">函館</span>

<span class="btn_off">胆振</span>

<li class="list-group-item"><a href="http://miyagi.dhrank.com/" class="btn_on">宮城</a></li>

<span class="btn_off">福島</span>

</ul>
</ul>

<br class="clear" />

<ul class="list-group">
<li class="list-group-item">北陸・甲信越</li>
<ul>

<li class="list-group-item"><a href="http://niigata.dhrank.com/" class="btn_on">新潟</a></li>

<li class="list-group-item"><a href="http://ishikawa.dhrank.com/" class="btn_on">石川</a></li>

<li class="list-group-item"><a href="http://toyama.dhrank.com/" class="btn_on">富山</a></li>

<li class="list-group-item"><a href="http://fukui.dhrank.com/" class="btn_on">福井</a></li>

</ul>

</ul>


<br class="clear" />
<ul class="list-group">
<li class="list-group-item">東海</li>
<ul>
    <li class="list-group-item"><a href="http://aichi.dhrank.com" class="btn_on">愛知(名古屋)</a></li>

    <span class="btn_off">三河</span>

    <span class="btn_off">尾張</span>

    <li class="list-group-item"><a href="http://shizuoka.dhrank.com" class="btn_on">静岡</a></li>

    <li class="list-group-item"><a href="http://gifu.dhrank.com" class="btn_on">岐阜</a></li>

    <li class="list-group-item"><a href="http://mie.dhrank.com" class="btn_on">三重</a></li>
</ul>
</ul>

<br class="clear" />
<ul class="list-group">
<li class="list-group-item">関西</li>
<ul>
    <li class="list-group-item"><a href="http://osaka.dhrank.com" class="btn_on">大阪</a></li>

    <li class="list-group-item"><a href="http://hyogo.dhrank.com" class="btn_on">兵庫</a></li>

    <li class="list-group-item"><a href="http://kyoto.dhrank.com" class="btn_on">京都</a></li>
</ul>
</ul>

<br class="clear" />
<ul class="list-group">
<li class="list-group-item">中国</li>
<ul>
    <li class="list-group-item"><a href="http://hiroshima.dhrank.com" class="btn_on">広島</a></li>

    <li class="list-group-item"><a href="http://okayama.dhrank.com" class="btn_on">岡山</a></li>
</ul>
</ul>

<br class="clear" />
<ul class="list-group">
<li class="list-group-item">九州・沖縄</li>
<ul>
<li class="list-group-item"><a href="http://fukuoka.dhrank.com" class="btn_on">福岡</a></li>

<span class="btn_off">北九州</span>

<span class="btn_off">久留米</span>

<li class="list-group-item"><a href="http://nagasaki.dhrank.com" class="btn_on">長崎</a></li>

<li class="list-group-item"><a href="http://kumamoto.dhrank.com" class="btn_on">熊本</a></li>

<li class="list-group-item"><a href="http://miyazaki.dhrank.com" class="btn_on">宮崎</a></li>

<li class="list-group-item"><a href="http://okinawa.dhrank.com" class="btn_on">沖縄</a></li>
</ul>
</ul></section>