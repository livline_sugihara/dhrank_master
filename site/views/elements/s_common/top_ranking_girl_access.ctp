<!-- rankingArea -->
<div class="categoryRankingArea">

	<section id="pickup_box_top">

<?php foreach (array_slice($girls_rank, 2) as $key => $record): ?>

	<?php
		$rank_image = "";
		if ($record['rankings']['before_standing'] > 5 && $record['rankings']['standing'] <= 3)
		{
			$rank_image = 'arrow_up2.png'; //急上昇
		}
		else if ($record['rankings']['before_standing'] == $record['rankings']['standing'])
		{
			$rank_image = 'arrow_stay.png'; //ステイ
		}
		else if ($record['rankings']['before_standing'] < $record['rankings']['standing'])
		{
			$rank_image = 'arrow_down.png'; //ダウン
		}
		else
		{
			$rank_image = 'arrow_up.png'; //アップ
		}

		//旧ヘルパー用に代入
		$record['User']['id'] = $record['girls']['user_id'];
		$record['Girl'] = $record['girls'];
		$record['Shop'] = $record['shops'];
		$record[0]['total_average'] = $record['rankings']['count'];
		$record['SummaryData']['value'] = $record['rankings']['count'];
		$record[0]['total_cnt'] = $record['girl_review_count'];
	  	echo $this->element('s_common/_ranking_girl', array(
			'ii' => $key,
			'record' => $record,
			'rank_image' => $rank_image
		));


	 ?>


<?php endforeach; ?>


</section>

<?php if(!empty($parent_reviewer)) { ?>
	<p class="btn_pi"><a href="/s/ranking/girl">もっと見る</a></p>
<?php } else { ?>
	<p class="btn_pi"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>">もっと見る</a></p>
<?php } ?>

</div>
<!-- /rankingArea-->
<?php // print_r($girl_ranking); ?>
