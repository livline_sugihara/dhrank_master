		<div class="footer_menu">
			<div class="cate">
		        <ul>
		            <li>
		                <a href="<?php echo $this->Html->url(array('controller' => 'area', 'action' => 'index', 'small_area_id' => 0)) ?>">
			                <p style="font-weight:bold;" id="menu_area">
			                <?php echo $this->Html->image('s/icon_search_area.jpg', array('width' => '60px')); ?>
			                    発エリアから探す
			                </p>
		            	</a>
						<ul>
							<a href="<?php echo $this->Html->url(array('controller' => 'area', 'action' => 'index')) ?>">
								<li class="clearfix">
									<?php echo $area[0]['LargeArea']['name'] ?>
									<p class="shop_count">
										<?php echo $shop_count[0]['CountsTmp']['shops_count'] ?>
									</p>
								</li>
							</a>
						<?php foreach ($area[0]['SmallArea'] as $key => $record): ?>
							<a href="<?php echo $this->Html->url(array('controller' => 'area', 'action' => 'index')) . '/' . $record['id'] ?>">
								<li class="clearfix">
									<?php echo $record['name']; ?>
									<p class="shop_count">
										<?php echo $shop_count[$key+1]['CountsTmp']['shops_count']; ?>
									</p>
								</li>
							</a>
						<?php endforeach; ?>
						</ul>
					</li>
		            <li>
						<a href="<?php echo $this->Html->url(array('controller' => 'category', 'action' => 'index', 'business_category_id' => '1')) ?>">
		                <p style="font-weight:bold;" id="menu_cate">
		                <?php echo $this->Html->image('s/icon_search_category.jpg', array('width' => '60px')); ?>
		                    業種別から探す
		                </p>
		            	</a>
					<ul>
						<?php foreach ($categories as $key => $record): ?>
							<a href="<?php echo $this->Html->url(array('controller'=> 'category', 'action' => 'index')) . '/' . $record['MShopsBusinessCategory']['id']; ?>">
								<li class="clearfix">
									<?php echo $record['MShopsBusinessCategory']['name']; ?>
									<p class="shop_count">
										<?php echo $shop_count['Category']['CountsTmp']['category_'. $record['MShopsBusinessCategory']['id']]; ?>
									</p>
								</li>
							</a>
						<?php endforeach; ?>
					</ul>
					</li>
		            <!-- <li><a href="<?php echo $this->Html->url(array('controller' => 'newest', 'action' => 'index')) ?>">
		                <?php echo $this->Html->image('s/icon_search_review.jpg'); ?>
		                <p>
		                    口コミから探す
		                </p>
		            	</a>
					</li>
		            <li><a href="#">
		                <?php echo $this->Html->image('s/icon_search_coupon.jpg'); ?>
		                <p>
		                    クーポンから探す
		                </p>
		            </a></li> -->
		        </ul>
		    </div>
		</div>
