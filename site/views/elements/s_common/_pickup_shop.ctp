
		<div class="box_inner">
			<div class="clearfix">
				<p class="img"><?php echo $imgCommon->get_girl_with_time($record, 'l', 1); ?></p>
				<div class="summary">

<?php if($record['Reviewer']['handle'] == null) { ?>
					<p class="caption"><?php echo $record['Review']['post_name']; ?><span class="font10">口コミ:<?php if($record['Review']['reviewer_id'] == 0) { echo '非会員'; } else { echo $record[0]['review_cnt'] . '件'; } ?></span></p>
<?php } else { ?>
					<p class="caption"><?php echo $record['Reviewer']['handle']; ?><span class="font10">口コミ:<?php if($record['Review']['reviewer_id'] == 0) { echo '非会員'; } else { echo $record[0]['review_cnt'] . '件'; } ?></span></p>
<?php } ?>



<?php if($record['Review']['delete_flag'] == Review::DELETE_FLAG_COMMENT_ONLY) { ?>

<?php } else if($record['Review']['delete_flag'] == Review::DELETE_FLAG_SAVE_POINTS) { ?>
						<p class="txt">このコメントは削除依頼により削除されました。</p>

<?php } else if($record['Review']['member_only'] == 1) { ?>
						<p class="tt mar_t_05"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>" style="text-decoration:none;">会員限定の記事です。</a></p>

<?php } else { ?>


					<p class="tt"><?php echo $record['Review']['title']; ?></p>
					<div class="stars"><span class="new_icon">New</span>

<?php
for($ii = 1; $ii <= 5; $ii++) {
    $suffix = ($ii <= $record[0]['girl_avg']) ? 'on' : 'off';
?>
                    <img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>
                    <span class="rate"><?php echo round($record[0]['girl_avg'], 2); ?></span></div>

					<p class="txt"><?php echo $record['Review']['comment']; ?></p>
<?php } ?>
				</div>
			</div>
		</div>
