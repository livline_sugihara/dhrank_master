					<div class="review_ranking_box_top">
						<div class="box_inner bigger">
							<div class="clearfix links link_arrow">

<?php if($record['Reviewer']['handle'] == null) { ?>
								<p class="img">									<?php echo $this->Html->image('face/face'. sprintf("%03d", $record['rankings']['avatar_id']) .'.png', array('width' => '100')); ?>								</p>
<?php } else { ?>
								<p class="img">									<a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>">										<?php echo $this->Html->image('face/face'. sprintf("%03d", $record['rankings']['avatar_id']) .'.png', array('width' => '80')); ?>									</a>								</p>
<?php } ?>


								<div class="summary" style="overflow:hidden;height:98px;">
									<div class="clearfix">
									<p class="mar_r_5 f_left rank_name">
<?php
	$eng_cnt = array('one', 'two', 'three');
	if($ii + 1 <= 3) {
?>
										<img src="/img/s/small_rank_<?php echo ($ii + 1); ?>_mark.png" alt="" width="25">
<?php } else { ?>
										<p class="num"><?php echo ($ii + 1) ?>位
<?php } ?>
										<img src="/img/s/<?php echo $rank_image; ?>" alt="" width="30"></p>

<?php if($record['Reviewer']['handle'] == null) { ?>
										<p class="caption f_left"><?php echo mb_strimwidth($record['Review']['post_name'],0,20); ?>さん</p>
<?php } else { ?>
										<p class="caption f_left"><?php echo nl2br($record['Reviewer']['handle']); ?>さん</p>
<?php } ?>




										<p class="review_number f_left">レビュー <?php echo $record['rankings']['review_cnt']; ?>件</p>
									</div>
									<p style="margin-top:8px;margin-bottom:5px;">
										<span class="lbl <?php if($ii + 1 <= 3) { echo $eng_cnt[$ii]; } ?>">最新の口コミ</span>
									</p>

<?php if($record['Review']['delete_flag'] == Review::DELETE_FLAG_COMMENT_ONLY) { ?>

<?php } else if($record['Review']['delete_flag'] == Review::DELETE_FLAG_SAVE_POINTS) { ?>
						<p class="txt">このコメントは削除依頼により削除されました。</p>

<?php } else if($record['Review']['member_only'] == 1) { ?>
						<p class="tt mar_t_05"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>" style="text-decoration:none;">会員限定の記事です。</a></p>

<?php } else { ?>
									<p class="tt mar_t_05"><?php echo $record['Review']['post_title']; ?></p>
									<p class="txt"><?php echo nl2br($textCommon->get_substr_tagstrip_text($record['Review']['comment'], 16)); ?></p>
<?php } ?>

								</div>
							</div>
						</div>
					</div>