<?php
// レビューページからの呼び出しかどうか
$review_flag = isset($reviewer) && $reviewer;
// Myレビューからの呼び出しかどうか
$myreview_flag = isset($myreview) && $myreview;

$opentime = date('H:i', strtotime($record['System']['open_time']));
$closetime = date('H:i', strtotime($record['System']['close_time']));
?>
        <!--shop-->
	
		<div class="rank_box underline">
                <p class="shop_tt"><a href="<?php echo $linkCommon->get_user_shop($record); ?>" class="shopName">
                    <?php echo $record['Shop']['name']; ?>
                </a><span>UP</span></p>
				<ul class="shop_icon clearfix">
                    <?php if($record['Shop']['service01'] == 1) { ?><li>先行予約OK</li><?php } ?>
                    <?php if($record['Shop']['service02'] == 1) { ?><li>クレジットOK</li><?php } ?>
                    <?php if($record['Shop']['service03'] == 1) { ?><li>領収書OK</li><?php } ?>
                    <?php if($record['Shop']['service04'] == 1) { ?><li>待ち合わせOK</li><?php } ?>
                    <?php if($record['Shop']['service05'] == 1) { ?><li class="coupon">コスプレあり</li><?php } ?>
                    <?php if($record['Shop']['service06'] == 1) { ?><li class="kodawari">クーポンあり</li><?php } ?>
				</ul>
				<div class="clearfix">
					<div class="box_left">
                        <a href="<?php echo $linkCommon->get_user_shop($record); ?>">
                    		<?php echo $imgCommon->get_shop_with_time($record, 'list_sp', array(160,210)); ?>
                		</a>


<?php if($review_flag) { ?>
        <p class="whiteBtn delete2Btn bookmarkShop" data="<?php echo $record['User']['id']; ?>" mode="delete" style="margin-top:10px;"><a href="#">お気に入り削除</a></p>
<?php } else if($myreview_flag) { ?>
        <p class="whiteBtn favoriteBtn"><a href="<?php echo $linkCommon->get_mypage_edit_review($record); ?>">このクチコミを編集</a></p>
<?php } else { ?>
<?php } ?>

					</div>
					<div class="box_right">
						<p class="rank_txt"><a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo $record['Shop']['about_title']; ?></a></p>
						<ul class="clearfix">
							<li class="icon_access"><?php echo $categories_value_list[$record['Shop']['shops_business_category_id']];?>/<?php echo $record['System']['address']; ?></li>
							<li class="icon_people">在籍数<br><?php echo $record[0]['girl_cnt']; ?>人</li>
							<li class="icon_price">最安値料金<br><?php echo $record['System']['price_cost']; ?>円～</li>
							<li class="icon_kuchikomi_up">口コミ<br><?php echo $record[0]['review_cnt']; ?>件</li>
							<li class="icon_time_up">営業時間<br><?php echo $opentime; ?>〜<?php echo $closetime; ?></li>
						</ul>
            		</div>
        		</div>
        	</div>
        <!-- /shop -->
