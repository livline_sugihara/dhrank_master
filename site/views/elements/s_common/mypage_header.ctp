	<header>
		<div class="clearfix">
			<div id="head_left">
				<h1><a href="/s/top/"><img src="/img/s/deri_logo.png" height="42"></a></h1>
				<p>国内最大級の<br>デリヘル口コミサイト</p>
			</div>
			<div id="head_account_right">
				<div class="button-toggle">&#9776;</div>
			</div>
		</div>
	</header>
	<div class="menu">
		<ul>
			<li><a href="<?php echo $linkCommon->get_mypage_profile($record = null)?>">My Top</a></li>
			<li><a href="<?php echo $linkCommon->get_mypage_top($record = null)?>">自分の更新</a></li>
			<li><a href="<?php echo $linkCommon->get_mypage_myreview($record = null)?>">Myレビュー</a></li>
			<li><a href="<?php echo $linkCommon->get_mypage_follow($record = null)?>">フォローしている</a></li>
			<li><a href="<?php echo $linkCommon->get_mypage_follower($record = null)?>">フォローされている</a></li>
			<li><a href="<?php echo $linkCommon->get_mypage_favorite_shop($record = null)?>">お気に入りのお店</a></li>
			<li><a href="<?php echo $linkCommon->get_mypage_favorite_girl($record = null)?>">お気に入りの女の子</a></li>
			<li><a href="<?php echo $linkCommon->get_mypage_favorite_review($record = null)?>">お気に入りのレビュー</a></li>
			<li><a href="<?php echo $linkCommon->get_mypage_profile_edit($record = null)?>">会員情報の確認・変更</a></li>
			<li class="menu_last"><a href="<?php echo $linkCommon->get_accounts_logout($record = null)?>">ログアウト</a></li>
		</ul>
	</div>
