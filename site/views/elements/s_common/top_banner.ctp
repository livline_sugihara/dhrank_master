    <!-- <div class="top_banner">
        <?php //foreach ($top_banner as $banner): ?>
        <img src="<?php //echo APP_IMG_URL. 'shop/' .$banner['Shop']['user_id']. '/' .$banner['Shop']['image_list_sp']; ?>" alt="イメージリストSP画像" />
        <?php //endforeach; ?>
    </div> -->

		<div class="slider" style="margin-bottom:35px;">

<?php
	$cnt = min(count($topic_shop), 15);
	for($ii = 0; $ii < $cnt; $ii++) {
		$record = $topic_shop[$ii];
		$link = $linkCommon->get_user_shop($record);
		if($ii % 5 == 0) {
?>
			<ul class="slides">
<?php
		}
?>
				<li><a href="<?php echo $link; ?>"><?php echo $imgCommon->get_shop_with_time($record, 'recommend', true, 'width:100%;'); ?></a></li>
<?php
		if($ii % 5 == 4 || $ii == $cnt - 1) {
?>
			</ul>
<?php
		}
	}
?>
		</div>
