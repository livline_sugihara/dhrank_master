		<div class="ranking_box">
<?php if($ii + 1 <= 3) { ?>
			<div class="head" id="ranking_0<?php echo ($ii + 1); ?>">
				<div class="clearfix">
					<p class="rank_img"><img src="/img/crown_<?php echo ($ii + 1); ?>.png" alt="" height="41" width="39"><br />
<?php } else { ?>
			<div class="head">
				<div class="clearfix">
					<p class="rank_img num" style="font-weight:bold;font-size:150%;"><span style="margin-left:10px;"><?php echo ($ii + 1) ?></span><br />
<?php } ?>
					<img src="/img/s/<?php echo $rank_image; ?>" alt="" height="27" width="34"></p>
					<div class="summary">
						<p class="name"><div class="img"><a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo $record['Shop']['name']; ?></a></div></p>
						<p><a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo $record['Girl']['name']; ?></a>への口コミ</p>

<?php if($record['Reviewer']['handle'] == null) { ?>
						<p><?php echo $record['Review']['post_name']; ?>さん／<?php echo $textCommon->escape_empty($record, 'MReviewersAge.name', '年齢不明'); ?></p>

<?php } else { ?>
						<p><a href="<?php echo $linkCommon->get_reviewer_reviews($record); ?>"><?php echo $record['Reviewer']['handle']; ?>さん／<?php echo $textCommon->escape_empty($record, 'MReviewersAge.name', '年齢不明'); ?></a></p>
<?php } ?>
					</div>
					<p class="right_img"><?php echo $imgCommon->get_girl_with_time($record, array(60, 80), 1); ?></p>
				</div>
			</div>
			<div class="content2">

<?php if($record['Review']['delete_flag'] == Review::DELETE_FLAG_COMMENT_ONLY) { ?>

<?php } else if($record['Review']['delete_flag'] == Review::DELETE_FLAG_SAVE_POINTS) { ?>
						<p class="txt">このコメントは削除依頼により削除されました。</p>

<?php } else if($record['Review']['member_only'] == 1) { ?>
						<p class="tt mar_t_05"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>" style="text-decoration:none;">会員限定の記事です。</a></p>

<?php } else { ?>


				<p class="tt">
					<a href="<?php echo $linkCommon->get_user_review($record); ?>" style="text-decoration:none;"><?php echo $record['Review']['post_title']; ?></a>
				</p>
<?php if(!empty($parent_reviewer)) { ?>
					<p class="txt2"><a href="<?php echo $linkCommon->get_user_review($record); ?>" style="text-decoration:none;"><?php echo mb_substr($record['Review']['comment'], 0, 1000); ?></a></p>
<?php } else { ?>
					<p class="txt2"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>" style="text-decoration:none;"><?php echo mb_substr($record['Review']['comment'], 0, 40); ?>…続きを読む</a></p>
<?php } ?><?php } ?>
			</div>
		</div>
