	<section id="new_shop_box">
		<p id="new_shop_box_tt" class="link_pi">新規掲載店</p>
<?php
	if(!empty($new_shops)) {
?>
		<div id="newSlideShow" class="list">
<?php
	foreach($new_shops AS $key => $record) {
?>
			<div><a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo $imgCommon->get_shop_with_time($record, 'icon'); ?></a></div>
<?php
	}
?>
		</div>
<?php
	} else {
?>
<!-- 仮設定 -->
<?php echo $this->element('newshopslide/' . $parent_area['LargeArea']['id']); ?>
<?php
	}
?>
	</section>