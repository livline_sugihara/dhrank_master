<!-- rankingArea -->
<div class="categoryRankingArea">
	<?php foreach (array_slice($shops_rank, 2) as $key => $record): ?>	<?php		$rank_image = "";		if ($record['rankings']['before_standing'] > 5 && $record['rankings']['standing'] <= 3)		{			$rank_image = 'arrow_up2.png';		}		else if ($record['rankings']['before_standing'] == $record['rankings']['standing'])		{			$rank_image = 'arrow_stay.png';		}		else if ($record['rankings']['before_standing'] < $record['rankings']['standing'])		{			$rank_image = 'arrow_down.png';		}		else		{			$rank_image = 'arrow_up.png';		}		$record['User']['id'] = $record['rankings']['ranking_id'];		$record['Shop']['name'] = $record['rankings']['ranking_name'];		$record['SummaryData']['value'] = $record['rankings']['count'];		$record['Shop']['shops_business_category_id'] = $record['rankings']['shops_business_category_id'];		$record['Shop']['about_contents'] = $record['rankings']['about_contents'];	  	echo $this->element('s_common/_ranking_shop', array(			'ii' => $key,			'record' => $record,			'rank_image' => $rank_image		));	 ?>	<?php endforeach; ?>
<?php if(!empty($parent_reviewer)) { ?>
	<p class="btn_pi"><a href="/s/ranking/shop">もっと見る</a></p>
<?php } else { ?>
	<p class="btn_pi"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>">もっと見る</a></p>
<?php } ?>

</div>

<!-- /rankingArea -->