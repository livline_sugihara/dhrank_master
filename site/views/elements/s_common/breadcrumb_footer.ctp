
	<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
 <span itemprop=title>
<?php
foreach($breadcrumb as $val) {
	$option = (isset($val['option'])) ? $val['option'] : array('itemprop'=>'url');
	$link = (isset($val['link'])) ? $val['link'] : null;
	$html->addCrumb($val['name'], $link, $option);
}
if(!isset($separator)) $separator = " </span></li><li itemscope itemtype=http://data-vocabulary.org/Breadcrumb> <span itemprop=title>";
if(!isset($top)) $top = '';
?>
				<?php echo $html->getCrumbs($separator, $top); ?>
			</span></li>



	</ul>
	</nav>
