	<section id="area_shop_box">
		<p id="area_shop_box_tt"><img id="area_shop_box_tt_img" src="/img/s/area_shop_tt.png" alt="このエリアのイチオシのお店" height="49" width="49"><span><a href="/s/topic"><img src="/img/s/area_next_btn.png"></a></span> </p>
		<div class="slider" style="margin-bottom:35px;">
<?php
	$cnt = min(count($topic_shop), 15);
	for($ii = 0; $ii < $cnt; $ii++) {
		$record = $topic_shop[$ii];
		$link = $linkCommon->get_user_shop($record);
		if($ii % 5 == 0) {
?>
			<ul class="slides">
<?php
		}
?>
				<li><a href="<?php echo $link; ?>"><?php echo $imgCommon->get_shop_with_time($record, 'recommend', true, 'width: 100%;'); ?></a></li>
<?php
		if($ii % 5 == 4 || $ii == $cnt - 1) {
?>
			</ul>
<?php
		}
	}
?>
		</div>
	</section>