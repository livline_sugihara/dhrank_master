	<footer>
		<p class="btn_or"><a href="<?php echo $linkCommon->get_accounts_login($record = null); ?>">ログイン・新規会員登録</a></p>
		<ul class="fot-link">
			<li><a href="<?php echo $linkCommon->get_abouts($record = null); ?>">当サイトについて</a></li>
			<li><a href="<?php echo $linkCommon->get_terms($record = null); ?>">ご利用規約</a></li>
			<li><a href="<?php echo $linkCommon->get_contacts($record = null); ?>">お問い合わせ</a></li>
		</ul>
	</footer>
	<div id="copyright">Copyright © <?php echo $parent_area['LargeArea']['name']?>口コミデリヘルランキング All Rights Reserved.</div>
