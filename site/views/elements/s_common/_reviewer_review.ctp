<?php
// レビューページからの呼び出しかどうか
$review_flag = isset($reviewer) && $reviewer;
// Myレビューからの呼び出しかどうか
$myreview_flag = isset($myreview) && $myreview;
?>
			<div class="box_inner">
				<div class="pickup_box_summary">
					<div class="clearfix">

<?php if($record['Reviewer']['handle'] == null) { ?>
						<p class="profile_img"><?php echo $imgCommon->get_reviewer_avatar($record, array(30,30), 'img_reviewer'); ?></p>
						<div class="f_left">
						<p class="name"><?php echo $record['Review']['post_name']; ?>さん</p>
<?php } else { ?>
						<p class="profile_img"><a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>"><?php echo $imgCommon->get_reviewer_avatar($record, array(30,30), 'img_reviewer'); ?></a></p>
						<div class="f_left">
						<p class="name"><?php echo $record['Reviewer']['handle']; ?>さん</p>
<?php } ?>
						<p class="font08"><?php echo $textCommon->escape_empty($record, 'MReviewersAge.name', '不明'); ?> | <?php echo $parent_reviewer['MReviewersJob']['name']; ?> | 口コミ <?php if($record['Review']['reviewer_id'] == 0) { echo '非会員'; } else { echo $record[0]['review_cnt'] . '件'; } ?></p>
						</div>
						<div class="f_right">


<?php if($review_flag) { ?>
							<p class="whiteBtn delete2Btn bookmarkReview" data="<?php echo $record['Review']['id']; ?>" mode="delete"><a href="#">お気に入りから削除</a></p>
<?php } ?>
						</div>
					</div>					
				</div>
				<div class="clearfix links link_arrow">
					<p class="img"><?php echo $imgCommon->get_girl_with_time($record, array(90, 120), 1); ?></p>
					<div class="summary">
						<p class="caption"><a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo $record['Girl']['name']; ?></a><span class="font10"> <?php echo $record['Girl']['age']; ?>歳</span></p>
						<p class="review">
<?php
for($ii = 1; $ii <= 5; $ii++) {
    $suffix = ($ii <= $record[0]['girl_avg']) ? 'on' : 'off';
?>
                    <img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>
                    <span class="cow"><?php echo round($record[0]['girl_avg'], 2); ?></span>
                    <span class="course_mark">
                    	<span class="target"><?php echo $textCommon->escape_empty($record, 'Review.appointed_sub_name', '不明'); ?></span>
                    	<span class="new"><?php echo $textCommon->escape_empty($record, 'Review.appointed_type_name', '不明'); ?></span>
					</span>
                        </p>
                    <p class="tt mar_t_05"><?php echo $record['Review']['post_title']; ?></p>
					<p class="txt"><?php echo $record['Review']['comment']; ?></p>
                    <p class="font10 day"><?php echo date('Y年m月d日', strtotime($record['Review']['created'])); ?></p>
				</div>
			</div>
		</div>
