	<header>
		<div class="clearfix">
			<div id="head_left">
				<h1><a href="<?php echo $linkCommon->get_top($record = null); ?>"><img src="/img/s/deri_logo.png" height="42"></a></h1>
				<p>国内最大級の<br>デリヘル口コミサイト</p>
			</div>
			<div id="head_right">
				<ul class="clearfix">
<?php
	// ログイン時
	if(!empty($parent_reviewer)) {
?>
					<li><a href="<?php echo $linkCommon->get_mypage_top($record = null)?>"><img src="/img/s/header_btn03.gif" alt="Myページ" width="46" height="44"></a></li>
					<li><a href="<?php echo $linkCommon->get_accounts_logout($record = null)?>"><img src="/img/s/header_btn04.gif" alt="ログアウト" width="46" height="44"></a></li>
<?php
	// 未ログイン時
	} else {
?>
					<li><a href="<?php echo $linkCommon->get_accounts_register($record = null)?>"><img src="/img/s/header_btn01.gif" alt="会員登録" width="46" height="44"></a></li>
					<li><a href="<?php echo $linkCommon->get_accounts_login($record = null)?>"><img src="/img/s/header_btn02.gif" alt="ログイン" width="46" height="44"></a></li>
<?php
	}
?>
				</ul>
			</div>
		</div>
	</header>
