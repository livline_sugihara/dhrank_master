<div id="top_new_search">
    <div class="forms clearfix">
    <?php echo $this->Form->create('Shop'); ?>
    <?php echo $this->Form->input('name', array('label' => false, 'class' => 'form', 'div' => false, 'placeholder' => 'ドエムカンパニー')); ?>
    <?php echo $this->Form->submit('s/top_search_btn.png', array('class' => 'submit', 'div' => false)); ?>
    <?php echo $this->Form->end(); ?>
    </div>
    <div class="cate">
        <ul>
            <li>
                <!-- <a href="<?php echo $this->Html->url(array('controller' => 'area', 'action' => 'index', 'small_area_id' => 0)) ?>"> -->
                <a href="<?php echo $this->Html->url('#menu_area') ?>">
                    <?php echo $this->Html->image('s/icon_search_area.jpg'); ?>
                <p>
                    発エリア<small>から</small>
                </p>
            </a></li>
            <li>
                <!-- <a href="<?php echo $this->Html->url(array('controller' => 'category', 'action' => 'index', 'business_category_id' => '1')) ?>"> -->
                <a href="<?php echo $this->Html->url('#menu_cate') ?>">
                <?php echo $this->Html->image('s/icon_search_category.jpg'); ?>
                <p>
                    業種別<small>から</small>
                </p>
            </a></li>
            <li><a href="<?php echo $this->Html->url(array('controller' => 'newest', 'action' => 'index')) ?>">
                <?php echo $this->Html->image('s/icon_search_review.jpg'); ?>
                <p>
                    口コミ<small>から</small>
                </p>
            </a></li>
            <li><a href="#">
                <?php echo $this->Html->image('s/icon_search_coupon.jpg'); ?>
                <p>
                    クーポン<small>から</small>
                </p>
            </a></li>
        </ul>
    </div>
</div>
