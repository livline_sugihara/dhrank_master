<script>
	$(document).ready(function() {
	$('.col').equalHeight();
	$('.col').biggerlink();
	});

	$('#Search1').keyup(function(){
		if (!$(this).val()) {
			// 検索文字列無し
			$('#MyBookmark1 div').fadeIn();
		} else {
			// 検索文字列有り
			$('#MyBookmark1 div').fadeOut();
			$('#MyBookmark1 div:contains(' + this.value + ')').fadeIn();
		}
	});

	$(function() {
    $("#tabs li").click(function() {
        var num = $("#tab li").index(this);
        $(".content_wrap").addClass('disnon');
        $(".content_wrap").eq(num).removeClass('disnon');
        $("#tabs li").removeClass('select');
        $(this).addClass('select')
    });
    $('.links').biggerlink();

});

	    // ありがとうボタン
		$('.thanksBtn').click(function(e) {
	        var review_id = $(this).attr('data');

	        if(review_id == undefined) {
	        	return;
	        }

	        // ありがとうリクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_good/"　+ review_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					if(data.success) {
						alert('投稿されました');
					} else {
						alert(data.message);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});

	    // お気に入り店舗ボタン
		$('.bookmarkShop').click(function(e) {
			var mode = $(this).attr('mode');
	        var user_id = $(this).attr('data');

	        if(user_id == undefined) {
	        	return;
	        }

	        if(mode == 'delete') {
	        	if(!confirm('お気に入りを削除します。宜しいですか？')) {
	        		return;
	        	}
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_bookmark_shop/"　+ mode + '/' + user_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					alert(data.message);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});

	    // お気に入り女の子ボタン
		$('.bookmarkGirl').click(function(e) {
			var mode = $(this).attr('mode');
	        var girl_id = $(this).attr('data');

	        if(girl_id == undefined) {
	        	return;
	        }

	        if(mode == 'delete') {
	        	if(!confirm('お気に入りを削除します。宜しいですか？')) {
	        		return;
	        	}
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_bookmark_girl/"　+ mode + '/' + girl_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					alert(data.message);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});

	    // お気に入りレビューボタン
		$('.bookmarkReview').click(function(e) {
			var mode = $(this).attr('mode');
	        var review_id = $(this).attr('data');

	        if(review_id == undefined) {
	        	return;
	        }

	        if(mode == 'delete') {
	        	if(!confirm('お気に入りを削除します。宜しいですか？')) {
	        		return;
	        	}
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_bookmark_review/"　+ mode + '/' + review_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					alert(data.message);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
				}
			});
	        e.preventDefault();
		});

	    // フォローボタン
		$('.followReviewer').click(function(e) {
			var mode = $(this).attr('mode');
	        var reviewer_id = $(this).attr('data');

	        if(reviewer_id == undefined) {
	        	return;
	        }

	        if(mode == 'delete') {
	        	if(!confirm('レビュワーを解除します。宜しいですか？')) {
	        		return;
	        	}
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_follow/"　+ mode + '/' + reviewer_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					alert(data.message);
					location.reload();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});

		// 自分の口コミを削除する
		$('.ReviewDelete').click(function(e){
			if (!confirm('この口コミを削除します。削除して宜しいですか？'))
			{
				return false;
			}
			var mode = $(this).attr('mode');
			var review_id = $(this).attr('data');
			jQuery.ajax({
				type: "get",
				url: "/delete_review/"　+ mode + '/' + review_id,
				data: "",
				dataType: "html",
				cache: false,
				success: function(data){
					alert('口コミを削除いたしました。');
					location.reload();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					console.log('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
				//e.preventDefault();
			});
		});
	});
});
</script>
