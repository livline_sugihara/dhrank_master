	<footer>
<?php // echo $this->element('common/breadcrumb', array("breadcrumb" => $breadcrumb)); ?>

		<?php echo $this->element('s_common/footer_menu'); ?>

		<p class="btn_or"><a href="<?php echo $linkCommon->get_accounts_login($record = null)?>">ログイン・新規会員登録</a></p>
		<ul class="fot-link">
			<li><a href="<?php echo $linkCommon->get_abouts($record = null)?>">当サイトについて</a></li>
			<li><a href="<?php echo $linkCommon->get_terms($record = null)?>">ご利用規約</a></li>
			<li><a href="<?php echo $linkCommon->get_contacts($record = null)?>">お問い合わせ</a></li>
		</ul>
<?php /* 非表示タカモト
	<section>
		<div style="text-align:center; margin:5px;">
<?php if(isset($mediaCategory) && $mediaCategory == 's'){?>
			表示：スマートフォン <a href="<?php echo substr($html->webroot,0,-1) . substr(rtrim($base_url, '/'),2) . '/view:pc';?>">PC</a>
<?php }?>
		</div>
		<div style="text-align:center; margin:5px;">
			<script type="text/javascript" src="http://media.line.naver.jp/js/line-button.js" ></script>
			<script type="text/javascript">
				new jp.naver.line.media.LineButton({"pc":false,"lang":"ja","type":"a"});
			</script>
		</div>
	</section>
*/ ?>
	</footer>
	<div id="copyright">Copyright © <?php echo $parent_area['LargeArea']['name']?>口コミデリヘルランキング All Rights Reserved.</div>