	<div class="link_o"><h2>お店情報</h2></div>
	<div class="shop_info clear">
		<span class="font09"><?php echo $parent_shop['MShopsBusinessCategory']['name']; ?></span>
		<h2 class="font22 font_normal"><?php echo $parent_shop['Shop']['name']; ?></h2>
<?php
$opentime = date('H:i', strtotime($parent_shop['System']['open_time']));
$closetime = date('H:i', strtotime($parent_shop['System']['close_time']));
?>
		<p class="font09">営業時間 <?php echo $opentime; ?>〜<?php echo $closetime; ?></p>
		<ul class="shop_icon clearfix">
			<?php if($parent_shop['Shop']['service01'] == 1) { ?><li>先行予約OK</li><?php } ?>
			<?php if($parent_shop['Shop']['service02'] == 1) { ?><li>クレジットOK</li><?php } ?>
			<?php if($parent_shop['Shop']['service03'] == 1) { ?><li>領収書OK</li><?php } ?>
			<?php if($parent_shop['Shop']['service04'] == 1) { ?><li>待ち合わせOK</li><?php } ?>
			<?php if($parent_shop['Shop']['service05'] == 1) { ?><li class="coupon">コスプレあり</li><?php } ?>
			<?php if($parent_shop['Shop']['service06'] == 1) { ?><li class="mensok">クーポンあり</li><?php } ?>
		</ul>
		<p class="btn_pi"><a href="tel:<?php echo $parent_shop['Shop']['phone_number']; ?>">お店に電話する</a></p>
		<div class="btns_box clearfix">
<?php if(isset($review_flag) && $review_flag) { ?>
			<p class="btns f_left bookmarkShop" data="<?php echo $parent_shop['User']['id']; ?>" mode="delete"><a href="#">お気に入りに解除</a></p>
<?php } else if(isset($myreview_flag) && $myreview_flag) { ?>
			<p class="btns f_left bookmarkShop"><a href="#">このクチコミを編集</a></p>
<?php } else { ?>
			<p class="btns f_left bookmarkShop" data="<?php echo $parent_shop['User']['id']; ?>" mode="add"><a href="#">Myデリヘルへ保存</a></p>
<?php } ?>
			<p class="btns f_right"><a href="<?php echo $linkCommon->get_review_select_girls($parent_shop); ?>">口コミ投稿する</a></p>
		</div>
	</div>
<!-- tab -->
<?php echo $this->element('s_common/shop_tabs'); ?>
<!-- /tab -->
