<div class="title title04">
    <h2>CATEGORY <span class="small">カテゴリーからお店を探す</span></h2>
</div>

<div class="list clearfix">
<?php
foreach($categories AS $key => $record) {
?>
    <div class="box">
        <p class="tt"><a href="<?php echo $linkCommon->get_category_detail($record['MShopsBusinessCategory']['id']); ?>"><?php echo $record['MShopsBusinessCategory']['name']; ?>を探す</a></p>
        <div class="clearfix">
            <p class="img">
                <a href="<?php echo $linkCommon->get_category_detail($record['MShopsBusinessCategory']['id']); ?>">
                    <?php echo $imgCommon->get_category($record, array(80, 80)); ?>
                </a>
            </p>
            <p class="txt"><?php echo $record['MShopsBusinessCategory']['content']; ?></p>
        </div>
    </div>
<?php
}
?>
</div>