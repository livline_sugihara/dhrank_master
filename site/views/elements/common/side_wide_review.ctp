<!-- rankingMenuArea -->
<div class="rankingMenuArea reviewArea">
	<div class="inner">
        <p class="side_tt"><img src="/img/side_left_label_review.jpg" alt="口コミから探す" height="45" width="240"></p>
        <ul class="list">
            <!-- <li><strong><a href="">口コミを投稿する</a></strong></li> -->
			<li><strong><?php echo $this->Html->link('口コミを投稿する', array('controller' => 'review', 'action' => 'search_shop')); ?></strong></li>
            <li><?php echo $this->Html->link('最新から順番に見る', array('controller' => 'newest')); ?></li>
            <!-- <li><a href="#">最新から順番に見る</a></li> -->
		<?php if ($this->Session->read('Area.smallArea')===0): ?>
			<li><?php echo $this->Html->link('お店別の口コミ', array('controller' => 'area', 'action' => 'index')); ?></li>
		<?php else: ?>
			<li><?php echo $this->Html->link('お店別の口コミ', array('controller' => 'area', 'action' => 'index', 'small_area_id' => $this->Session->read('Area.smallArea'))); ?></li>
		<?php endif; ?>
            <!-- <li><a href="#">エリア別の口コミ</a></li> -->
            <!-- <li><a href="#">投稿者ランキング</a></li> -->
			<li><?php echo $this->Html->link('投稿者ランキング', array('controller' => 'ranking', 'action' => 'reviewer')); ?></li>
            <!-- <li><a href="#">非掲載理由</a></li> -->
            </ul>
            </div>
            </div>
            <!-- /rankingArea -->
