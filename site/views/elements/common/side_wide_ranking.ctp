<div class="siderankArea">
	<div class="title title06">
		<h2>投稿者ランキング</h2>
		</div>
		<div class="list">
			<?php foreach ($reviewer as $r): ?>
				<div class="box clearfix">

					<div class="rankNum">

						<p>
							<?php if ($r['rankings']['standing']==1): ?>
								<?php echo $this->Html->image('no1_img.jpg', array('height' => '30')); ?>
							<?php elseif($r['rankings']['standing']==2): ?>
								<?php echo $this->Html->image('no2_img.jpg', array('height' => '30')); ?>
							<?php elseif($r['rankings']['standing']==3): ?>
								<?php echo $this->Html->image('no3_img.jpg', array('height' => '30')); ?>
							<?php elseif($r['rankings']['standing']==4): ?>
								<?php echo '4'; ?>
							<?php elseif($r['rankings']['standing']==5): ?>
								<?php echo '5'; ?>
							<?php endif; ?>
						</p>

						<p>
							<?php if ($r['rankings']['before_standing'] < $r['rankings']['standing']): ?>
								<?php echo $this->Html->image('side_arrow_up.png', array('height' => '20')); ?>
							<?php elseif($r['rankings']['before_standing'] > $r['rankings']['standing']): ?>
								<?php echo $this->Html->image('side_arrow_down.png', array('height' => '20')); ?>
							<?php elseif($r['rankings']['before_standing'] == $r['rankings']['standing']): ?>
								<?php echo $this->Html->image('side_arrow_stay.png', array('height' => '20')); ?>
							<?php elseif($r['rankings']['before_standing'] < $r['rankings']['standing'] && ($r['rankings']['before_standing'] - $r['rankings']['standing']) > 100): ?>
								<?php echo $this->Html->image('side_arrow_double.png', array('height' => '20')); ?>
							<?php endif; ?>
						</p>

					</div>

					<div class="rankImg">

				<!-- <a href="<?php //echo $linkCommon->get_reviewer_reviews($r['ranking_id']); ?>"> -->

					<?php echo $this->Html->image('face/face'. sprintf("%03d", $r['rankings']['avatar_id']) .'.png', array('width' => '50')); ?>

				<!-- </a> -->

					</div>

					<div class="rankCmt">

						<div>
							<p class="name"><a href="#"><?php echo $r['rankings']['ranking_name']; ?></a>さん</p>

							<p class="info">
								<?php echo $r['rankings']['ranking_comment']; ?>
							</p>

						</div>

					</div>

				</div>
		<?php endforeach; ?>
		<p class="rankLink"><?php echo $this->Html->link('投稿者ランキングへ', array('controller' => 'ranking', 'action' => 'reviewer')); ?><img src="/images/icon_arrow.png" width="13" height="14" alt=""></p>

	</div>
</div>


<?php /*
	<div class="list">
		<div class="box clearfix">
			<div class="img"><a href="#"><img src="/img/tab_topictoukou_img1.png" alt="" height="50" width="50" class="img_frame"></a></div>
			<div class="summary">
				<p class="tt"><span>デリヘル貴族</span>さん</p>
				<p class="txt">デリヘル専門のデリヘル貴族です。たまにソープも行きます。</p>
				<p class="option">30代後半 会社員</p>
			</div>
		</div>
		<div class="box clearfix">
			<div class="img"><a href="#"><img src="/img/tab_topictoukou_img2.png" alt="" height="50" width="50" class="img_frame"></a></div>
			<div class="summary">
				<p class="tt"><span>キャベツさん太郎</span>さん</p>
				<p class="txt">お笑いなどのバラエティー番組を見るのが大好きです♪</p>
				<p class="option">40代後半 自営業</p>
			</div>
		</div>
		<div class="box clearfix">
			<div class="img"><a href="#"><img src="/img/tab_topictoukou_img3.png" alt="" height="50" width="50" class="img_frame"></a></div>
			<div class="summary">
				<p class="tt"><span>ハチミツボーイ</span>さん</p>
				<p class="txt">仕事はサラリーマンで土日が休みです！スポーツが大好きです！</p>
				<p class="option">20代後半 アルバイト</p>
			</div>
		</div>
		<div class="box clearfix">
			<div class="img"><a href="#"><img src="/img/tab_topictoukou_img1.png" alt="" height="50" width="50" class="img_frame"></a></div>
			<div class="summary">
				<p class="tt"><span>デリヘル貴族</span>さん</p>
				<p class="txt">デリヘル専門のデリヘル貴族です。たまにソープも行きます。</p>
				<p class="option">30代後半 会社員</p>
			</div>
		</div>
		<div class="box clearfix">
			<div class="img"><a href="#"><img src="/img/tab_topictoukou_img2.png" alt="" height="50" width="50" class="img_frame"></a></div>
			<div class="summary">
				<p class="tt"><span>キャベツさん太郎</span>さん</p>
				<p class="txt">お笑いなどのバラエティー番組を見るのが大好きです♪</p>
				<p class="option">40代後半 自営業</p>
			</div>
		</div>
	</div>
</div>
*/ ?>
