	<!-- box -->
	<div class="box clearfix">
		<div class="summary">
                <p class="img">
                    <img src="/img/ispot_information_img.png" alt="クチコミ" style="margin-right:5px;" width="82" height="50">
                </p>
			<p class="tt">
			<a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo $record['Shop']['name']; ?></a><br />
			<a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo $record['Girl']['name']; ?>ちゃん</a></p>

			<p class="scoreInfo">
<?php
for($ii = 1; $ii <= 5; $ii++) {
    $suffix = ($ii <= $record[0]['girl_avg']) ? 'on' : 'off';
?>
                    <img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>
				<span class="score"><?php echo round($record[0]['girl_avg'], 2); ?></span>
			</p>


<?php if($record['Review']['delete_flag'] == Review::DELETE_FLAG_COMMENT_ONLY) { ?>

<?php } else if($record['Review']['delete_flag'] == Review::DELETE_FLAG_SAVE_POINTS) { ?>
						<p class="txt">このコメントは削除依頼により削除されました。</p>

<?php } else if($record['Review']['member_only'] == 1) { ?>
						<p class="tt mar_t_05"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>" style="text-decoration:none;">会員限定の記事です。</a></p>

<?php } else { ?>
			<p>
				<?php echo mb_strimwidth($record['Review']['comment'], 0, 120,'...','UTF-8');?>
			</p>
			<div class="clearfix">
				<p class="link"><a href="<?php echo $linkCommon->get_user_girl($record); ?>">・・・続きを読む</a></p>
			</div><?php } ?>



		</div>
	</div>
	<!-- /box -->
