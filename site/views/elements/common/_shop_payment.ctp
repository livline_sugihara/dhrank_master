<?php
// レビューページからの呼び出しかどうか
$review_flag = isset($reviewer) && $reviewer;
//pr($record);
$opentime = date('H:i', strtotime($record['System']['open_time']));
$closetime = date('H:i', strtotime($record['System']['close_time']));
?>
    <!--shop-->
    <?php if ($record['UsersAuthority']['order_rank']==1 || $record['UsersAuthority']['order_rank']==2): ?>
        <div class="shopList_frame shop_premium">
    <?php elseif($record['UsersAuthority']['order_rank']==3 || $record['UsersAuthority']['order_rank']==4 || $record['UsersAuthority']['order_rank']==5): ?>
        <div class="shopList_frame shop_middle">
    <?php else: ?>
        <div class="shopList_frame">
    <?php endif; ?>
            <div class="shop_pay">
                <a href="<?php echo $linkCommon->get_user_shop($record); ?>" class="shopName">
                                        <?php echo $record['Shop']['name']; ?>

                </a>　
                <span class="kuchikomi">UP</span>
                <a href="<?php echo $linkCommon->get_user_shop($record); ?>" class="css_btn_class">
                    詳細を見る
                </a>
            </div>
                <div class="shop_pay pay_mark clearfix">
                    <?php if($record['Shop']['service01'] == 1) { ?><p class="pay_yoyaku">先行予約OK</p><?php } ?>
                    <?php if($record['Shop']['service02'] == 1) { ?><p class="pay_credit">クレジットOK</p><?php } ?>
                    <?php if($record['Shop']['service03'] == 1) { ?><p class="pay_ryoshu">領収書OK</p><?php } ?>
                    <?php if($record['Shop']['service04'] == 1) { ?><p class="pay_machi">待ち合わせOK</p><?php } ?>
                    <?php if($record['Shop']['service05'] == 1) { ?><p class="pay_cosplay">コスプレあり</p><?php } ?>
                    <?php if($record['Shop']['service06'] == 1) { ?><p class="pay_coupon">クーポンあり</p><?php } ?>
                </div>
        </div>

        <div class="shopList_main clearfix">
            <div>
                <a href="<?php echo $linkCommon->get_user_shop($record); ?>">
                    <?php echo $imgCommon->get_shop_with_time($record, 'list', array(160,210)); ?>
                </a>
            </div>
            <div>
                <p class="catchCopy"><b><strong><?php echo $record['Shop']['about_title']; ?></strong></b></p>
                <p class="categoryComment"><?php echo nl2br($record['Shop']['about_contents']); ?></p>

                <div class="shopListLeft">
                    <dl class="clearfix">
                        <dt>営業時間</dt>
                        <dd><?php echo $opentime; ?>〜<?php echo $closetime; ?></dd>

                        <dt>最低料金</dt>
                        <dd><?php echo $record['System']['price_cost']; ?>円〜</dd>

                        <dt>住所</dt>
                        <dd><?php echo $record['System']['address']; ?></dd>
                    </dl>
                    <p class="shopNum">
                        <span class="zaiseki">在籍数: </span><span class="num"><?php echo $record[0]['girl_cnt']; ?>人</span>
                        <span class="kuchikomi2">口コミ: </span><span class="num"><?php echo $record[0]['review_cnt']; ?>件</span>
                    </p>
                </div>
                <div class="shopListRight">
<?php if($review_flag) { ?>
                    <a href="<?php echo $linkCommon->get_user_shop($record); ?>" class="css_btn_more">詳細を見る</a>

                   <!-- <p class="whiteBtn favoriteBtn bookmarkShop" data="<?php echo $record['User']['id']; ?>" mode="add"><a href="#">お気に入りに保存</a></p> -->

                     <a href="#" class="css_btn_bookmark bookmarkShop" data="<?php echo $record['User']['id']; ?>" mode="add">お気に入りに保存</a>
<?php } ?>

<?php if ($record['UsersAuthority']['is_show_category_banner']==1): ?>
    <div class="catebanner">
        <img src="<?php echo APP_IMG_URL. 'shop/' .$record['Shop']['user_id']. '/banner.gif'; ?>" alt="カテゴリバナー" />
    </div>
<?php endif; ?>
                </div>

                <div class="shopListBottom clearfix">
                    <ul>
<?php
if(!empty($record['Coupon'])) {
?>
<?php
    foreach($record['Coupon'] AS $jj => $coupon) {
?>
                    <p><?php echo $record['Coupon']['condition']; ?></p>
                        <li>


                            <span class="type<?php echo $coupon['target'];?>">
                                <?php echo $m_target[$coupon['target']]; ?>
                            </span>


                            <!-- span class="type<?php echo $coupon['target'];?>">
                                <?php echo $m_target[$coupon['target']]; ?><?php echo $textCommon->get_vertical_text($m_target[$record['Coupon']['target']]); ?>
                            </span -->
                            <a href="<?php echo $linkCommon->get_user_coupons($record); ?>"><?php echo $coupon['condition']; ?>
                            </a>
                        </li>
 <?php
    }
} else {
?>
                        <li style="line-height: 2.8em">割引クーポンは発行されていません。</li>
<?php
}
?>                    </ul>
                </div>
            </div>
        </div>
        <!-- /shop -->
