<h2 class="heading">口コミレビュー</h2>
<div class="new_reviewArea clearfix">

<?php
$cnt = 0;
if($parent_shop['UsersAuthority']['is_show_all_contents_by_shop'] == 1) {
	// 有料版はクチコミは5件まで
	$cnt = min(count($new_shop_review), 2);
} else {
	// 無料版の場合はクチコミを全件表示
	$cnt = count($new_shop_review);
}
for ($ii = 0; $ii < $cnt; $ii++) {
    echo $this->element('common/_review', array("record" => $new_shop_review[$ii]));
}
?>
</div>
