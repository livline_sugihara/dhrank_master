	<!-- box -->
	<div class="box clearfix">
		<div class="rankNum">
<?php
if($ii + 1 <= 3) {
?>
			<img src="/img/crown_<?php echo ($ii + 1); ?>.png" alt="" width="78"><br />
<?php } else { ?>
			<p class="num"><?php echo ($ii + 1) ?></p>
<?php } ?>
			<img src="/img/<?php echo $rank_image; ?>" alt="" width="78">
		</div>
		<p class="img">			<a href="<?php echo $linkCommon->get_user_girl($record); ?>">

				<?php echo $imgCommon->get_girl_with_time($record, array(60, 80), 1, null, 'img_frame'); ?>
			</a>
		</p>
		<div class="summary">
			<p class="shop"><?php echo $record['Shop']['name']; ?></p>
			<p class="tt2">
				<a href="<?php echo $linkCommon->get_user_girl($record); ?>">
					<?php echo $record['Girl']['name']; ?>
				</a>
			</p>
			<div class="scoreInfo">
				<p>
<?php
$score = round($record[0]['total_average'], 2);
for($ii = 1; $ii <= 5; $ii++) {
    $suffix = ($ii <= $score) ? 'on' : 'off';
?>
                    <img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>
                    <span class="score"><?php echo $score; ?></span>
            	</p>
			</div>
			<div>
			<p class="cat">カテゴリ：<?php echo $categories_value_list[$record['Shop']['shops_business_category_id']];?></p>
			<p class="link"><a href="<?php echo $linkCommon->get_user_girl($record); ?>">レビュー <?php echo $record[0]['total_cnt']; ?>件</a></p>
			</div>
		</div>
		<div class="comment">
			<p><?php echo nl2br($textCommon->get_substr_tagstrip_text($record['Girl']['owner_comment'], 75)); ?></p>
		</div>
	</div>