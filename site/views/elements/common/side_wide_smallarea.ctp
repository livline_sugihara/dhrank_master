<div class="rankingMenuArea">
	<div class="inner">
		<img src="/img/side_left_label_area.jpg" alt="デリヘルランキング" height="45" width="240"></p>
		<ul class="list">
			<?php $cnt = 0; ?>
			<li class="clearfix">
				<?php echo $this->Html->link($area[0]['LargeArea']['name'], array('controller' => 'area', 'action' => 'index')); ?>
				<p class="shop_count">
					<?php echo $shop_count[$cnt]['CountsTmp']['shops_count']; ?>
				</p>
			</li>
			<?php
				foreach ($area[0]['SmallArea'] as $a):
				$cnt++;
			?>
				<li class="clearfix">
					<?php echo $this->Html->link($a['name'], array('controller' => 'area', 'action' => 'index', 'small_area_id' => $a['id'])); ?>
					<p class="shop_count">
						<?php echo $shop_count[$cnt]['CountsTmp']['shops_count']; ?>
					</p>
				</li>
			<?php endforeach; ?>
		</ul>
		</div>
</div>
