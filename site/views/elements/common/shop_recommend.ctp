<div class="pickupArea clearfix">

<?php
$cnt = min(count($recommend_girls), 6);

if($cnt > 0) {
    for ($ii = 0; $ii < $cnt; $ii++) {
        $record = $recommend_girls[$ii];
?>
<dl>
    <dt><?php echo $record['MGirlsRecommendType']['name']; ?>がオススメ</dt>
    <dd>
        <div>
            <p class="name"><a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo $record['Girl']['name']; ?></a></p>
            <p><?php echo mb_substr(strip_tags($record['Girl']['owner_comment']),0,24).'…'; ?></p>
        </div>
        <div class="img">
            <?php echo $imgCommon->get_girl_with_time($record, 'l'); ?>
        </div>
    </dd>
</dl>
<?php
    }
}elseif($cnt==0){
    $cnt = min(count($other_girls), 6);
     for ($ii = 0; $ii < $cnt; $ii++) {
         $record = $other_girls[$ii];?>
         <dl>
             <dt><?php echo $record['MGirlsRecommendType']['name']; ?></dt>
             <dd>
                 <div>
                    <p class="name"><a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo $record['Girl']['name']; ?></a></p>
                    <p><?php echo mb_substr(strip_tags($record['Girl']['owner_comment']),0,24).'…'; ?></p>
                </div>
                 <div class="img">
                    <?php echo $imgCommon->get_girl_with_time($record, 'l'); ?>
                 </div>
            </dd>
        </dl>
                         <?php    }}?>
</div>
