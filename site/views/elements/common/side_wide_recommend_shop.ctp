<!-- recommendArea -->
<div class="recommendShopArea">
	<div class="title title07">
		<a href="/recommend/shop"><h2>RECOMMEND SHOP</h2><p class="moreBtn redBtn">MORE</p></a>
	</div>
<!-- list -->
	<div class="list clearfix">
<?php if(empty($recommend_shop)) {
	//echo $this->element('takamoto_recommend_top/' . $parent_area['LargeArea']['id']);
} else {
	$cnt = min(count($recommend_shop), 2);
	for($ii = 0; $ii < $cnt; $ii++) {
		$record = $recommend_shop[$ii];
?>
		<!-- box -->
		<div class="box">
			<a href="<?php echo $linkCommon->get_user_shop($record); ?>">
				<?php echo $imgCommon->get_shop_with_time($record, 'recommend', array(244)); ?>
			</a>
			<p class="tt"><?php echo $record['Shop']['name']; ?></p>
			<p class="caption"><?php echo $categories_value_list[$record['Shop']['shops_business_category_id']]; ?></p>
			<p class="txt"><?php echo $record['Shop']['about_contents']; ?></p>
		</div>
		<!-- /box -->
<?php
	}
}
?>

<?php /*
		<div class="box">
			<a href="/shop"><img src="/img/recommend_03_img.png" alt="" width="244"></a>
			<p class="tt">クラブバレンタイン</p>
			<p class="caption">デリヘル</p>
			<p class="txt">クラブバレンタイン大阪は『清純な女の子』を『安心の価格』、『納得のサービス』でお届けしております。関西最大グループである「シグマグループ」の旗艦店として、本当に多くのお客様によって支えられ今では在籍コンパニオンの数も130人を超えました。 </p>
		</div>
*/ ?>
	</div>
</div>
<!-- /recommendArea -->