<!-- rankingArea -->
<div class="categoryRankingArea">

<?php
	$count = 0;
	foreach ($shops_rank as $key => $record):
		if ($count >= 3) continue;
?>
<?php

	$rank_image = "";
	if ($record['rankings']['before_standing'] > 5 && $record['rankings']['standing'] <= 3)
	{
		$rank_image = 'arrow_up2.png'; //急上昇
	}
	else if ($record['rankings']['before_standing'] == $record['rankings']['standing'])
	{
		$rank_image = 'arrow_stay.png'; //ステイ
	}
	else if ($record['rankings']['before_standing'] < $record['rankings']['standing'])
	{
		$rank_image = 'arrow_down.png'; //ダウン
	}
	else
	{
		$rank_image = 'arrow_up.png'; //アップ
	}
	//旧ヘルパー用に代入
	$record['User']['id'] = $record['rankings']['ranking_id'];
	$record['Shop']['name'] = $record['rankings']['ranking_name'];
	$record['SummaryData']['value'] = $record['rankings']['count'];
	$record['Shop']['shops_business_category_id'] = $record['rankings']['shops_business_category_id'];
	$record['Shop']['about_contents'] = $record['rankings']['about_contents'];
  	echo $this->element('common/_ranking_shop', array(
		'ii' => $key,
		'record' => $record,
		'rank_image' => $rank_image
	));
	$count++;

 ?>



<?php endforeach; ?>


<?php
foreach($shop_ranking AS $ii => $record)
{	$rank_image = '';
		// 急上昇
			if($record['SummaryData']['prev_rank'] > 5 && $record['SummaryData']['rank'] <= 3)
			{
				$rank_image = 'arrow_up2.png';
				}
				else if($record['SummaryData']['prev_rank'] == $record['SummaryData']['rank'])
				{
					// ステイ
					$rank_image = 'arrow_stay.png';
					}
					else if($record['SummaryData']['prev_rank'] < $record['SummaryData']['rank'])
					{
						// ダウン
						$rank_image = 'arrow_down.png';
						}
						else
						{
							// アップ
									$rank_image = 'arrow_up.png';
									}
?>

<?php
/*
echo $this->element('common/_ranking_shop', array(
		'ii' => $ii,
		'record' => $record,
		'rank_image' => $rank_image,
	));
?>

<?php
*/}
?>



<?php if(!empty($parent_reviewer)) { ?>
	<p class="redBtn"><a href="/ranking/shop">もっと見る</a></p>
<?php } else { ?>
	<p class="redBtn"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>">もっと見る</a></p>
<?php } ?>

</div>
<!-- /rankingArea -->
