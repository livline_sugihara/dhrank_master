<div class="title title03">
	<h2>REVIEW RANKING <span class="small">参考になった口コミランキング</span></h2>
</div>

<!-- list -->
<div class="list">
<?php
$cnt = min(count($helpful_review), 3);
for ($ii = 0; $ii < $cnt; $ii++) {
	$record = $helpful_review[$ii];
	$rank_image = 'arrow_up2.png';
	// 急上昇
	// if($record['SummaryData']['prev_rank'] > 5 && $record['SummaryData']['rank'] <= 3) {
	// 	$rank_image = 'arrow_up2.png';
	// } else if($record['SummaryData']['prev_rank'] == $record['SummaryData']['rank']) {
	// 	// ステイ
	// 	$rank_image = 'arrow_stay.png';
	// } else if($record['SummaryData']['prev_rank'] < $record['SummaryData']['rank']) {
	// 	// ダウン
	// 	$rank_image = 'arrow_down.png';
	// } else {
	// 	// アップ
	// 	$rank_image = 'arrow_up.png';
	// }

	$param = array(
		'record' => $record,
		'ii' => $ii,
		'rank_image' => $rank_image,
	);
	echo $this->element('common/_review_ranking', $param);
}
?>
</div>
<!-- /list -->

<?php // print_r($new_review); ?>