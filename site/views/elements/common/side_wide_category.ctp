<!-- rankingMenuArea -->
<div class="rankingMenuArea">
	<div class="inner">
		<p class="side_tt"><img src="/img/side_left_label_rank.jpg" alt="デリヘルランキング" height="45" width="240"></p>
		<ul class="list">
<?php
$cnt = 0;
foreach($categories AS $key => $record) {
?>
			<li class="clearfix">
				<a href="<?php echo $linkCommon->get_category_detail($record['MShopsBusinessCategory']['id']); ?>"><?php echo $record['MShopsBusinessCategory']['name']; ?></a>
				<p class="shop_count">
				<?php echo $shop_count['Category']['CountsTmp']["category_". $record['MShopsBusinessCategory']['id']]; ?>
				</p>
			</li>
			<?php $cnt++; } ?>
		</ul>
	</div>
</div>
<!-- /rankingArea -->
