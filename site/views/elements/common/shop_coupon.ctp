<h2 class="heading">割引情報</h2>
<div class="couponArea">
<?php if(count($coupons) > 0) { ?>
<?php
    foreach($coupons AS $key => $record) {
?>
    <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <th class="type<?php echo $record['Coupon']['target']; ?>"><?php echo $textCommon->get_vertical_text($m_target[$record['Coupon']['target']]); ?></th>
            <td>
                <div>
                    <p><?php echo $record['Coupon']['condition']; ?></p>
                    <ul>
                        <li>提示条件：<span>予約時</span></li>
                        <li>利用条件：<span><?php echo $m_target_caption_list[$record['Coupon']['target']]; ?></span></li>
                        <li>有効期限：<span><?php if($record['Coupon']['expire_unlimited'] == 1) { ?>期限なし<?php } else { echo date('Y年m月d日まで', strtotime($record['Coupon']['expire_date'])); } ?></span></li>
                    </ul>
                </div>
            </td>
            <td class="price"><?php echo $record['Coupon']['content']; ?></td>
        </tr>
    </table>
<?php
    }
?>
<?php
} else {
?>
    <p>割引クーポンは発行されていません。</p>
<?php
}
?>
</div>
