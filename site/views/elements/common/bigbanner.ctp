<div class="container">
	<!-- eyecatchArea -->
    <div id="eyecatchArea">
		<div class="inner clearfix">
			<div class="left">
				<div id="slider1_wrapper">
					<div id="slider1">
<?php foreach($bigbanners AS $ii => $record) { ?>
						<div>
							<a href="<?php echo $record['BigBanner']['shop_link_url']; ?>">
								<?php echo $imgCommon->get_bigbanner($record, 'shop', array(320,210)); ?>
							</a>
						</div>
<?php } ?>
					</div>
				</div>

				<div id="slider2_wrapper">
					<div id="slider2">
						<?php echo $this->element('sidebar/news'); ?>
					</div>
				</div>
			</div>

			<div class="right">
            	<div id="slider3_wrapper">
                	<div id="slider3">
<?php foreach($bigbanners AS $ii => $record) { ?>
						<div>
							<a href="<?php echo $record['BigBanner']['girl_link_url']; ?>">
								<?php echo $imgCommon->get_bigbanner($record, 'girl', array(500,300)); ?>
							</a>
						</div>
<?php } ?>
            		</div>
            	</div>
			</div>
		</div>
	</div>
	<!-- / mainArea -->
</div>