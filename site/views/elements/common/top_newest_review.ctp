<div class="title title02">
	<a href="/newest">
		<h2>NEWEST REVIEW <span class="small">新着口コミ</span></h2>
		<p class="moreBtn redBtn">MORE</p>
	</a>
</div>

<div class="list">
<?php
$cnt = min(count($new_review), 5);
for ($ii = 0; $ii < $cnt; $ii++) {
    echo $this->element('common/_review_newest', array("record" => $new_review[$ii]));
}
?>
</div>

<?php // print_r($new_review); ?>