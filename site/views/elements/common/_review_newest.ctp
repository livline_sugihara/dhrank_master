	<!-- box -->
	<div class="box clearfix">
		<p class="img_girl">
			<a href="<?php echo $linkCommon->get_user_girl($record); ?>">
				<?php echo $imgCommon->get_girl_with_time($record, array(90, 120), 1); ?>
			</a>
		</p>
		<div class="summary">
			<p class="scoreInfo">
				<div><span class="date" style="font-size:110%;font-weight:bold;margin-right:5px;"><a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo $record['Girl']['name']; ?></a></span><?php echo $record['Girl']['age']; ?>歳		
				<span style="float:right;font-size:80%;"><?php echo date('Y.m.d', strtotime($record['Review']['created'])); ?></span>
				</div>
			<!--	<span class="mark">NEW</span> -->
<?php
for($ii = 1; $ii <= 5; $ii++) {
    $suffix = ($ii <= $record[0]['girl_avg']) ? 'on' : 'off';
?>
                    <img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>
				<span class="score"><?php echo round($record[0]['girl_avg'], 2); ?></span>
				<span class="course_mark">
					<span class="target"><?php echo $textCommon->escape_empty($record, 'Review.appointed_sub_name', '不明'); ?></span>
					<span class="new"><?php echo $textCommon->escape_empty($record, 'Review.appointed_type_name', '不明'); ?></span>
				</span>
			</p>
<?php if($record['Review']['delete_flag'] == Review::DELETE_FLAG_COMMENT_ONLY) { ?>

<?php } else if($record['Review']['delete_flag'] == Review::DELETE_FLAG_SAVE_POINTS) { ?>
			<p class="txt">このコメントは削除依頼により削除されました。</p>

<?php } else if($record['Review']['member_only'] == 1) { ?>
			<p class="tt mar_t_05"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>" style="text-decoration:none;">会員限定の記事です。</a></p>

<?php } else { ?>
			<p class="tt"><a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo mb_strimwidth($record['Review']['post_title'],0,80); ?></a></p>
			<p><?php echo mb_strimwidth($record['Review']['comment'],0,240,"…"); ?></p>
<?php } ?>
		</div>
	</div>
	<!-- /box -->
