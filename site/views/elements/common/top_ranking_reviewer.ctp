<!-- rankingArea -->
<div class="categoryRankingArea">

	<?php foreach ($reviewer as $key => $record): ?>

		<?php
			$rank_image = 'arrow_up2.png';
			$record['Reviewer']['id'] = $record['rankings']['ranking_id'];
			$record['Reviewer']['handle'] = $record['rankings']['ranking_name'];
			echo $this->element('common/_ranking_reviewer', array(		
				'ii' => $key,
				'record' => $record,
				'rank_image' => $rank_image,
			));
		?>

	<?php endforeach; ?>

<?php if(!empty($parent_reviewer)) { ?>
	<p class="redBtn"><a href="/ranking/reviewer">もっと見る</a></p>
<?php } else { ?>
	<p class="redBtn"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>">もっと見る</a></p>
<?php } ?>

</div>
<!-- /rankingArea-->
