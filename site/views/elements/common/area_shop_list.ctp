     <!-- pay_shopList -->
    <div id="pay_shopList">

<?php if(isset($small_area)) { ?>
        <div class="title title04">
            <h2><?php echo $this->Session->read('Area.Name'); ?>の店舗一覧</h2>
        </div>
        <div class="pageCounter">
            <span class="num"><?php echo $paginator->counter(array('format' => '%count%')); ?></span><span>件の店舗があります</span>
            <?php echo $this->element('pagenate/category_header_pagenate'); ?>
        </div>
<?php } else { ?>
        <div class="pageCounter">
            <span class="num"><?php echo $paginator->counter(array('format' => '%count%')); ?></span><span>件あります</span>
            <?php echo $this->element('pagenate/category_header_pagenate'); ?>
        </div>
<?php } ?>

<?php
foreach($list as $key => $record) {

    if($record['UsersAuthority']['is_show_all_contents_by_categorylist'] == 1) {
//pr($record);
?>
        <?php echo $this->element('common/_shop_payment', array('record' => $record)); ?>
<?php
    } else if($record['UsersAuthority']['is_show_all_contents_by_categorylist'] == 1 && $record['UsersAuthority']['is_show_category_banner'] == 0){?>
        <?php echo $this->element('common/_shop_middle', array('record' => $record)); ?>
<?php    } else {?>

        <?php echo $this->element('common/_shop_free', array('record' => $record)); ?>
<?php
    }
}
?>
    </div>
    <!-- /pay_shopList -->

    <?php echo $this->element('pagenate/category_footer_pagenate'); ?>
