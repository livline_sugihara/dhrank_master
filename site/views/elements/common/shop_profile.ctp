<div class="header clearfix">
	<script type="text/javascript">
	$(function(){
		$('.profile_slide ul').bxSlider({
			auto:true,
			pause: 7000,
			speed:1000,
			pager:true,
			controls:false
		});
	});
	</script>
	<div class="profile_slide">
		<ul>
			<li><?php echo $imgCommon->get_shop_with_time($parent_shop, 'profile'); ?></li>
			<li><?php echo $imgCommon->get_shop_with_time($parent_shop, 'profile'); ?></li>
			<li><?php echo $imgCommon->get_shop_with_time($parent_shop, 'profile'); ?></li>
		</ul><!-- 0804変更　変更前width160px -->
	</div>
    <div style="height:234px; overflow:hidden">
        <p class="catchCopy_shop"><?php echo $parent_shop['Shop']['about_title']; ?></p>
        <p><?php echo nl2br($parent_shop['Shop']['about_contents']); ?></p>
    </div>
</div>