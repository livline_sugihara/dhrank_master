	<!-- box -->
	<div class="box rv clearfix">
		<div class="rankNum">
<?php
$eng_cnt = array('one', 'two', 'three');
if($ii + 1 <= 3) {
?>
			<img src="/img/crown_<?php echo ($ii + 1); ?>.png" alt="" width="78"><br />
<?php } else { ?>
			<p class="num"><?php echo ($ii + 1) ?></p>
<?php } ?>
			<img src="/img/<?php echo $rank_image; ?>" alt="" width="78">
		</div>
		<p class="img">
			<?php echo $this->Html->image('face/face'. sprintf("%03d", $record['rankings']['avatar_id']) .'.png', array('width' => '80')); ?>
		<div class="summary">
			<div>
				<span class="name">
				<a href="<?php echo $linkCommon->get_reviewer_reviews($record); ?>"><?php echo $record['rankings']['ranking_name']; ?>さん</a>
				</span>
				<span class="reviewNum"><a href="<?php echo $linkCommon->get_reviewer_reviews($record); ?>">レビュー投稿　<?php echo $record['rankings']['count']; ?>件</a></span>
			</div>
			<p style="margin-top:8px;margin-bottom:5px;">
				<span class="lbl <?php if($ii + 1 <= 3) { echo $eng_cnt[$ii]; } ?>">最新の口コミ</span>
			</p>
			<p><a href="<?php echo $linkCommon->get_reviewer_reviews($record); ?>"><?php echo $record['Review']['post_title']; ?></a></p>
			<p class="info">
				<?php echo nl2br($textCommon->get_substr_tagstrip_text($record['Review']['comment'], 75)); ?>
			</p>
		</div>
	</div>
	<!-- /box -->
