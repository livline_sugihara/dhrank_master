<div class="title title05">
	<a href="/recommend/girl">
		<h2>RECOMMEND GIRLS<span class="small">新着お店激押し売れっ子</span></h2>
		<p class="moreBtn redBtn">MORE</p>
	</a>
</div>

<div id="girlsSlideShow" class="list">
<?php
if(!empty($recommend_girls)) {
	foreach($recommend_girls AS $key => $record) {
?>
	<div>
		<p>
			<a href="<?php echo $linkCommon->get_user_girl($record); ?>">
				<?php echo $imgCommon->get_girl_with_time($record, array(90, 120), 1, null, 'img_frame'); ?>
			</a>
		</p>
		<p class="name"><?php echo $record['Girl']['name']; ?></p>
		<!-- キャッチコピー追加 -->
		<p class="copy"><?php echo mb_strimwidth($record['Girl']['owner_comment'], 0, 33, "..."); ?></p>
		<p class="info"><?php echo $record['Shop']['name']; ?></p>
	</div>
<?php
	}
}
?>
</div>
