<div>
	<div class="center">
<?php
// ログイン時
if(!empty($parent_reviewer)) {
?>
        <p>ようこそ、<?php echo $parent_reviewer['Reviewer']['handle']; ?> さん。</p>
        <a href="<?php echo $linkCommon->get_mypage_top($parent_area); ?>">Myページ</a>
<?php
// 未ログイン時
} else {
?>
        <p>ようこそ、ゲストさん。</p>
        <a href="<?php echo $linkCommon->get_accounts_login($parent_area)?>" class="css_btn_login">ログインする</a>
        <a href="<?php echo $linkCommon->get_accounts_register($parent_area)?>" class="redLink">会員登録をする</a>
<?php } ?>
	</div>
</div>
