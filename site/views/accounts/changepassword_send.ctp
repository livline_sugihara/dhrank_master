<section id="middle" class="full">
    <div class="title title2">
        <h2>CHANGE PASSWORD <span class="small">レビュアー会員・パスワード変更手続</span></h2>
    </div>
    <p class="text">ログインIDとパ登録されている、メールアドレスとログインIDを入力して下さい。<br />
パスワード変更手続き用URLをメールにて送信いたします。<br /></p>
    <p class="text">※パスワードを変更すると、これまでのパスワードは無効になります。</p>

    <div class="formArea">
        <?php echo $form->create(null, array('action' => '')); ?>
<?php
if ($session->check('Message.auth')) {
?>
            <?php echo $session->flash('auth'); ?>
<?php }?>
    
            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <th>メールアドレス</th>
                    <td>
                        <?php echo $form->text('ReviewerChangepassword.email', array('size' => 30)); ?>
                        <?php echo $form->error('ReviewerChangepassword.email'); ?>
                    </td>
                </tr>
                <tr>
                    <th>ログインID</th>
                    <td>
                        <?php echo $form->text('ReviewerChangepassword.username', array('size' => 30)); ?>
                        <?php echo $form->error('ReviewerChangepassword.username'); ?>
                    </td>
                </tr>
            </table>

            <p class="align_c">
                <?php echo $form->submit('送信', array('class' => 'redBtn','value'=>'送信', 'div' => false)); ?>
            </p>

        <?php echo $form->end(); ?>        
    </div>
</section>

<?php /*`
<h2 class="rank_h2">レビュアー会員・パスワード変更手続</h2>

<?php echo $form->create(null,array('type'=>'post','action'=>''));?>
<div class="login_reviewer">

<p style="text-align:left;font-size:14px;">
ご登録されている、メールアドレスとログインIDを入力して下さい。<br />
パスワード変更手続き用URLをメールにて送信いたします。<br />
</p>
<p style="text-align:left;font-size:12px;">
※パスワードを変更すると、これまでのパスワードは無効になります。
</p>
    <table class="login_table">
        <tr>
            <td class="login_td1"><img src="/images/icon_table.jpg" />メールアドレス</td>
            <td class="login_td2"><?php echo $form->text('ReviewerChangepassword.email'); ?><?php echo $form->error('ReviewerChangepassword.email'); ?></td>
        </tr>
        <tr>
            <td class="login_td1"><img src="/images/icon_table.jpg" />ログインID</td>
            <td class="login_td2"><?php echo $form->text('ReviewerChangepassword.username'); ?><?php echo $form->error('ReviewerChangepassword.username'); ?></td>
        </tr>
    </table>

</div>


<div align="center" style="margin-top:40px;">
<?php echo $form->submit('送信', array('class' => 'btn_back','value'=>'送信')); ?>
</div>
<?php echo $form->end(); ?>

<br />

<div align="center" style="margin-top:40px;">
<a href="<?php echo $linkCommon->get_accounts_login($parent_area);?>" class="btn_back">戻る</a>
</div>
*/ ?>