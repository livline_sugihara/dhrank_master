	<div style="border-bottom:2px solid #c03c61;padding-bottom:2px;">
		<h2 class="link_o2">パスワード変更手続</h2>
	</div>
	
	<section id="rogin_area">

		<div style="background-color:#f0efed;padding:5px 0;">

    <p class="text">新パスワードを2回入力して下さい。</p>

<p><br /><p>

    <div class="formArea">
          <?php echo $form->create(null, array('action' => '')); ?>
            <?php echo $form->hidden('Reviewer.id'); ?>
            <?php echo $form->hidden('ReviewerChangepassword.id'); ?>
<?php
if ($session->check('Message.auth')) {
?>
            <?php echo $session->flash('auth'); ?>
<?php }?>

<div class="inner_box">
	<p><label for="id">新パスワード</label></p>
		<?php echo $form->password('Reviewer.new_password', array('size' => 30)); ?>
		<?php echo $form->error('Reviewer.new_password'); ?>
		
	<p><label for="id">新パスワード(確認)</label></p>
		<?php echo $form->password('Reviewer.new_password_confirm', array('size' => 30)); ?>
        <?php echo $form->error('Reviewer.new_password_confirm'); ?>
</div>

	<p class="btn_pi">
		<input type="submit" class="redBtn" value="変更">
	</p>
	
        <?php echo $form->end(); ?>

<p><br /><p>

  	  </div>
    
	</section>

<p><br /><p>

<?php /*
<h2 class="rank_h2">レビュアー会員・パスワード変更手続</h2>

<?php echo $form->create(null,array('type'=>'post','action'=>'')); ?>
<?php echo $form->hidden('Reviewer.id'); ?>
<?php echo $form->hidden('ReviewerChangepassword.id'); ?>
<div class="login_reviewer">

	<table class="login_table">
    	<tr>
        	<td class="login_td1"><img src="/images/icon_table.jpg" />新パスワード</td>
            <td class="login_td2"><?php echo $form->password('Reviewer.new_password'); ?><?php echo $form->error('Reviewer.new_password'); ?></td>
        </tr>
    	<tr>
        	<td class="login_td1"><img src="/images/icon_table.jpg" />新パスワード<br />(確認)</td>
            <td class="login_td2"><?php echo $form->password('Reviewer.new_password_confirm'); ?><?php echo $form->error('Reviewer.new_password_confirm'); ?></td>
        </tr>
    </table>

</div>


<div align="center" style="margin-top:40px;">
<?php echo $form->submit('変更', array('class' => 'btn_back','value'=>'変更')); ?>
</div>
<?php echo $form->end(); ?>
*/ ?>
