<section id="middle" class="full">
    <div class="title title2">
        <h2>CHANGE PASSWORD <span class="small">パスワード変更手続</span></h2>
    </div>
    <p class="text">新パスワードを2回入力して下さい。 </p>
    
    <div class="formArea">
        <?php echo $form->create(null, array('action' => '')); ?>
            <?php echo $form->hidden('Reviewer.id'); ?>
            <?php echo $form->hidden('ReviewerChangepassword.id'); ?>
<?php
if ($session->check('Message.auth')) {
?>
            <?php echo $session->flash('auth'); ?>
<?php }?>
    
            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <th>新パスワード</th>
                    <td>
                        <?php echo $form->password('Reviewer.new_password', array('size' => 30)); ?>
                        <?php echo $form->error('Reviewer.new_password'); ?>
                    </td>
                </tr>
                <tr>
                    <th>新パスワード(確認)</th>
                    <td>
                        <?php echo $form->password('Reviewer.new_password_confirm', array('size' => 30)); ?>
                        <?php echo $form->error('Reviewer.new_password_confirm'); ?>
                    </td>
                </tr>
            </table>

            <p class="align_c">
                <?php echo $form->submit('変更', array('class' => 'redBtn','value'=>'変更', 'div' => false)); ?>
            </p>

        <?php echo $form->end(); ?>

    </div>
</section>

<?php /*
<h2 class="rank_h2">レビュアー会員・パスワード変更手続</h2>

<?php echo $form->create(null,array('type'=>'post','action'=>'')); ?>
<?php echo $form->hidden('Reviewer.id'); ?>
<?php echo $form->hidden('ReviewerChangepassword.id'); ?>
<div class="login_reviewer">

	<table class="login_table">
    	<tr>
        	<td class="login_td1"><img src="/images/icon_table.jpg" />新パスワード</td>
            <td class="login_td2"><?php echo $form->password('Reviewer.new_password'); ?><?php echo $form->error('Reviewer.new_password'); ?></td>
        </tr>
    	<tr>
        	<td class="login_td1"><img src="/images/icon_table.jpg" />新パスワード<br />(確認)</td>
            <td class="login_td2"><?php echo $form->password('Reviewer.new_password_confirm'); ?><?php echo $form->error('Reviewer.new_password_confirm'); ?></td>
        </tr>
    </table>

</div>


<div align="center" style="margin-top:40px;">
<?php echo $form->submit('変更', array('class' => 'btn_back','value'=>'変更')); ?>
</div>
<?php echo $form->end(); ?>
*/ ?>
