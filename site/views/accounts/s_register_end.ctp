<div style="border-bottom:2px solid #c03c61;padding-bottom:2px;">
	<h2 class="link_o2">会員登録</h2>
</div>


<section id="account_done">

		<div class="inner">
		
			<div class=" mar_t_05 steps"></div>
			
			<div class="mar_b_20 post_bg">

				<p id="p1" class="mar_b_30">
                	ご入力いただいたメールアドレスへ<br />
					確認メールを送信しました。
                </p>
		
				<p style="width:80%;margin:0 auto;">
                	<span style="color:#a93353">現在は仮登録状況です。</span><br />
					メールに記載されているURLをクリックして、<br />
                    登録を完了させてください。<br />
					<br />
                    登録はごくごく簡単な入力になっております。
                </p>
				
				
			</div>
		</div>

	</section>
