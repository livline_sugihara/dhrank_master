<section id="middle" class="narrow">

    <!-- recommendGirlsArea -->
    <div id="recommendGirlsArea">
        <div class="title title05">
            <h2>RECOMMEND GIRLS<span class="small">新着お店激押し売れっ子</span></h2>
        </div>
<?php
if(empty($recommend_girls)) {
?>
        <span>このエリアで登録されている激押し売れっ子はいません。</span>
<?php
} else {
    foreach($recommend_girls AS $ii => $record) {
?>
        <article class="recommend_shop">
            <h3><?php echo $record['Shop']['name']; ?></h3>
            <ul>
<?php
        foreach($record['Girls'] AS $jj => $girl_record) {
            $insRecord = array('Girl' => $girl_record, 'User' => $record['User']);
?>
                <li>
                    <a href="<?php echo $linkCommon->get_user_girl($insRecord); ?>">
                        <?php echo $imgCommon->get_girl_with_time($insRecord, array(91, 121), 1, null, 'img_frame'); ?>
                    </a>
                    <p class="name"><?php echo $girl_record['name']; ?></p>
            		<!-- キャッチコピー追加 -->            		<p class="copy">
                    <?php echo mb_strimwidth($girl_record['catch_phrase'], 0, 15, "..."); ?></p>
                    <p class="data"><?php echo $girl_record['age']; ?>歳・<?php echo $girl_record['body_height']; ?>cm</p>
                </li>
<?php
        }
?>
            </ul>
        </article>
<?php
    }
?>
    </div>
    <!-- /recommendGirlsArea -->
<?php
}
?>
</section>
<?php // print_r($recommend_girls); ?>
<section id="side" class="wide">
    <?php echo $this->element('common/side_wide'); ?>
</section>
