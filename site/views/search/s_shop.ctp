	<div class="link_o"><h2>検索結果一覧</h2></div>
	<div class="kensuu"><p>全<span class="font18"><strong><?php echo $paginator->counter(array('format' => '%count%</strong></span>件中 <span>%start%</span>～<span>%end%</span>件表示')); ?></p></div>
<!-- pay_shopList -->
	<div id="pay_shopList">
<?php 
foreach($list as $key => $record) {

	if($record['UsersAuthority']['is_show_all_contents_by_categorylist'] == 1) {
?>
		<?php echo $this->element('s_common/_shop_search_payment', array('record' => $record)); ?>
<?php
	} else {
?>
		<?php echo $this->element('s_common/_shop_search_free', array('record' => $record)); ?>
<?php
	}
}
?>
	</div><!-- /pay_shopList -->
	<?php echo $this->element('s_pagenate/footer_pagenate'); ?>
<?php echo $this->element('s_common/footer_banner'); ?>
