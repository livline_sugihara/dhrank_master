<section id="middle" class="narrow">

    <!-- heavy_reviewArea -->
    <div id="heavy_reviewArea">
        
        <div class="title title02">
            <h2>NEWEST REVIEW <span class="small">新着口コミ</span></h2>
        </div>
                    
<div class="list">
<?php
$cnt = count($new_review);
for ($ii = 0; $ii < $cnt; $ii++) {
    echo $this->element('common/_review_newest', array("record" => $new_review[$ii]));
}
?>
</div>
        
    </div>
    <!-- /heavy_reviewArea -->

</section>

<section id="side" class="wide">
	<?php echo $this->element('/common/side_wide'); ?>
</section>