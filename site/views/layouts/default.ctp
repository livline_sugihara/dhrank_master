<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<?php echo $this->Html->charset(); ?>
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><?php echo $title_for_layout; ?></title>

<?php
if(!empty($meta_keywords)){
echo $this->Html->meta('keywords', null, array('content' => $meta_keywords), true);
}
if(!empty($meta_description)){
echo $this->Html->meta('description', null, array('content' => $meta_description), true);
}
?>

<?php /*
<script type="text/javascript" src="<?php echo $parent_area['LargeArea']['url'] ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $parent_area['LargeArea']['url'] ?>js/jquery.rating.js"></script>
*/ ?>

<?php
echo $this->Html->css(array(
	'add',
	'main',
	'normalize',
	'style_new',
	'jquery.bxslider'
	)
);

//echo $scripts_for_layout;
?>

<?php echo $this->Html->script(array(
	'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
	'main.js',
	'jquery.mousewheel.js',
	'jquery.easing.1.3.js',
	'jquery.bxslider.js',
	'jquery.lightbox_me.js',
	'jquery.scrollUp.js'
	)
);
?>

</head>
<!-- / head -->

<?php /*
<script type="text/javascript">
   function cancelSubmit(e){
       if (!e) var e = window.event;

       if(e.keyCode == 13)
           return false;
   }

   window.onload = function (){
   var list = document.getElementsByTagName("input");
       for(var i=0; i<list.length; i++){
           if(list[i].type == 'text' || list[i].type == 'checkbox' ||
list[i].type == 'radio' || list[i].type == 'select' || list[i].type ==
'file')
               {
               list[i].onkeypress = function (event){
               return cancelSubmit(event);
               };
           }
       }
   }
</script>

<?php //start---2013/3/10 障害No.1-7-0003修正 ?>
<script type="text/javascript">
$.ajaxSetup({ cache: false });
</script>
<?php //end---2013/3/10 障害No.1-7-0003修正 ?>
*/ ?>

<body>

<?php echo $this->element('common/header', array('searchbar' => true)); ?>
<?php echo $this->element('common/bigbanner'); ?>
<?php // echo $this->element('bigbanner/' . $parent_area['LargeArea']['id']); ?>
<?php echo $this->element('common/bar_out'); ?>
<?php echo $this->element('common/breadcrumb', array("breadcrumb" => $breadcrumb)); ?>

<!-- contents -->
<section id="contents" class="container clearfix">

	<?php echo $content_for_layout; ?>

    <!-- 女の子選択ダイアログ用タグ -->
    <div id="frame"></div>
    <!-- //女の子選択ダイアログ用タグ -->

</section>
<!-- /contents -->

<?php echo $this->element('common/footer'); ?>

</body>

<?php echo $this->element('common/common_ajax'); ?>

</html>
