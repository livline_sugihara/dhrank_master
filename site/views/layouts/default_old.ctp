<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<?php echo $this->Html->charset(); ?>
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title><?php echo $title_for_layout; ?></title>
<?php
		if(!empty($meta_keywords)){
		echo $this->Html->meta('keywords', null, array('content' => $meta_keywords), true);
		}
		if(!empty($meta_description)){
		echo $this->Html->meta('description', null, array('content' => $meta_description), true);
		}
?>
	<script type="text/javascript" src="<?php echo $parent_area['LargeArea']['url'] ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $parent_area['LargeArea']['url'] ?>js/jquery.rating.js"></script>
<?php
		echo $this->Html->css('style');
		echo $this->Html->css('jquery.rating');
		echo $scripts_for_layout;
?>
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->

<script type="text/javascript">
   function cancelSubmit(e){
       if (!e) var e = window.event;

       if(e.keyCode == 13)
           return false;
   }

   window.onload = function (){
   var list = document.getElementsByTagName("input");
       for(var i=0; i<list.length; i++){
           if(list[i].type == 'text' || list[i].type == 'checkbox' ||
list[i].type == 'radio' || list[i].type == 'select' || list[i].type ==
'file')
               {
               list[i].onkeypress = function (event){
               return cancelSubmit(event);
               };
           }
       }
   }
</script>
<?php //start---2013/3/10 障害No.1-7-0003修正 ?>
<script type="text/javascript">
$.ajaxSetup({ cache: false });
</script>
<?php //end---2013/3/10 障害No.1-7-0003修正 ?>
</head>

<body>

<?php echo $this->element('common/header'); ?>

<div id="container">

<?php if($this->params["controller"] != 'reviewers'){?>
<p class="breadcrumb"><?php echo $breadcrumbList->get_breadcrumb_list()?></p>
<?php }?>

<div id="main">


<div id="main_left">
    <table>
<tr>
<th>デリヘルランキング</th>
</tr>
<tr>
<td>
<ul class="left_ul">
	<?php foreach($m_shops_business_categories as $key => $record){?>
	<li class="left_li"><a href="<?php echo $linkCommon->get_ranking($parent_area, $record['MShopsBusinessCategory']['id'])?>"><?php echo $record['MShopsBusinessCategory']['name']?> </a></li>
	<?php }?>
</ul>
</td>
</tr>
</table>

<table>
<tr>
<th>お店一覧を見る</th>
</tr>
<tr>
	<td style="text-align:center">
		<a href="<?php echo $linkCommon->get_shops($parent_area)?>#kana1" class="btn_shop">あ行のお店</a>
		<a href="<?php echo $linkCommon->get_shops($parent_area)?>#kana2" class="btn_shop">か行のお店</a>
		<a href="<?php echo $linkCommon->get_shops($parent_area)?>#kana3" class="btn_shop">さ行のお店</a>
		<a href="<?php echo $linkCommon->get_shops($parent_area)?>#kana4" class="btn_shop">た行のお店</a>
		<a href="<?php echo $linkCommon->get_shops($parent_area)?>#kana5" class="btn_shop">な行のお店</a>
		<a href="<?php echo $linkCommon->get_shops($parent_area)?>#kana6" class="btn_shop">は行のお店</a>
		<a href="<?php echo $linkCommon->get_shops($parent_area)?>#kana7" class="btn_shop">ま行のお店</a>
		<a href="<?php echo $linkCommon->get_shops($parent_area)?>#kana8" class="btn_shop">や行のお店</a>
		<a href="<?php echo $linkCommon->get_shops($parent_area)?>#kana9" class="btn_shop">ら行のお店</a>
		<a href="<?php echo $linkCommon->get_shops($parent_area)?>#kana10" class="btn_shop">わ行のお店</a>
	</td>
</tr>
</table>

<div class="qr_side">

<img src="/images/smart.png" />

		<div class="qr_code">
			<img src="<?php echo $qr_code_url ?>"  />
		</div>
		<div class="qr_text">
			<?php echo $parent_area['LargeArea']['name']?>デリヘル口コミランキングの情報を携帯電話・スマートフォンからチェック！<br />
		</div>

		<br class="clear" />

</div>



<!--
<p>
<a href="/boards"><img src="/images/bbs.jpg" /></a>
</p>
-->

<table>
<tr>
<th>更新情報</th>
</tr>
<tr>
<td>
<div id="news">
	<?php echo $this->element('sidebar/news'); ?>
</div>
</td>
</tr>
</table>

<!--
<p>
	<a href="https://twitter.com"><img src="/images/twitter.png" target="_blank" /></a>
</p>
 -->

<?php foreach($ranking_sidebar as $key => $record){?>
<table>
<tr>
	<th><?php echo $record['business_category_name']?>TOP3</th>
</tr>
<tr>
	<td>
		<?php foreach($record as $key2 => $record2){?>
		<?php if(!empty($record2['User']['id'])){?>
<div class="rank_left">
<img src="/images/rank<?php echo $key2+1?>.png" />
</div>
<div class="rank_right">
	<a href="<?php echo $linkCommon->get_user_shop($record2)?>" class="rank_link">

<?php echo mb_strimwidth($record2['Shop']['name'], 0, 22, "...","UTF-8");?>

</a>
</div>
<?php if($pcount['top_rankings'] != $key2+1){?>
<hr class="rank_line clear" />
<?php }?>
		<?php }?>
		<?php }?>
<div align="right" class="clear">
	<a href="<?php echo $linkCommon->get_ranking($parent_area,$record['business_category_id'])?>" class="btn_rank">もっと見る</a>
</div>
	</td>
</tr>
</table>
<?php }?>

</div>
<!-- /main_left -->


<div id="main_center">

	<?php if($this->params["controller"] == 'users'){?>
	<?php echo $this->element('common/user_header'); ?>
	<?php }?>
	<?php if($this->params["controller"] == 'reviewers'){?>
	<?php echo $this->element('common/reviewer_header'); ?>
	<?php }?>
	<?php echo $content_for_layout; ?>

<div style="margin-bottom:20px"/><br /></div>
</div>
<!-- /main_center -->


<div id="main_right">
<?php foreach($sidebar_banner as $record){ ?>
<div class="right_banner">
	<?php if($record['User']['is_official_link_display'] == 1 && !empty($record['Shop']['url_pc'])){?>
	<a href="<?php echo $record['Shop']['url_pc']?>" target="_blank"><?php echo $imgCommon->get_bannar($record); ?></a>
	<?php }else{?>
	<a href="<?php echo $linkCommon->get_user_shop($record)?>"><?php echo $imgCommon->get_bannar($record); ?></a>
	<?php }?>
</div>
<?php }?>
</div>
<!-- /main_right -->

<br class="clear" />

</div>
<!-- /main -->


<div id="footer">

<?php echo $this->element('common/footer'); ?>

</div>

</div>
<!--/container-->

<?php echo $this->element('sql_dump'); ?>

</body>
</html>
