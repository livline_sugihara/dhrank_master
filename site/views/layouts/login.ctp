<head>
<?php echo $this->Html->charset(); ?>
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><?php echo $title_for_layout; ?></title>

<?php
if(!empty($meta_keywords)){
echo $this->Html->meta('keywords', null, array('content' => $meta_keywords), true);
}
if(!empty($meta_description)){
echo $this->Html->meta('description', null, array('content' => $meta_description), true);
}
?>

<?php
echo $this->Html->css(array(
	'add',
	'main',
	'normalize',
	'style_new',
	'jquery.bxslider'
	)
);

//echo $scripts_for_layout;
?>

<?php echo $this->Html->script(array(
	'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
	'main.js',
	'jquery.mousewheel.js',
	'jquery.easing.1.3.js',
	'jquery.bxslider.js',
	'jquery.lightbox_me.js',
	'jquery.scrollUp.js'
	)
);
?>

</head>
<!-- / head -->

<body>

<?php echo $this->element('common/header', array('searchbar' => false)); ?>
<?php echo $this->element('common/breadcrumb', array("breadcrumb" => $breadcrumb)); ?>

<!-- contents -->
<section id="contents" class="container clearfix">

	<?php echo $content_for_layout; ?>

    <!-- 女の子選択ダイアログ用タグ -->
    <div id="frame"></div>
    <!-- //女の子選択ダイアログ用タグ -->

</section>
<!-- /contents -->

<?php echo $this->element('common/footer'); ?>

</body>

<script>
$(document).ready(function(){

	$(function () {
		$.scrollUp({
			animation: 'fade',
		});
	});

	//slider
	$('#slider1').bxSlider({
		auto:true,
		speed:1000,
		infiniteLoop: true,
		hideControlOnEnd: false,
		slideWidth: 320,
 		pager: false,
		controls: true,
	});

	$('#slider2').bxSlider({
		auto:true,
		speed:1000,
		infiniteLoop: true,
		hideControlOnEnd: false,
 		pager: false,
		controls: true,
	});

	$('#slider3').bxSlider({
		auto:true,
		speed:1000,
		infiniteLoop: true,
		hideControlOnEnd: false,
 		pager: false,
		controls: false,
	});

	$('#topicSlider').bxSlider({
		auto:true,
		speed:1000,
		infiniteLoop: true,
		hideControlOnEnd: false,
 		pager: false,
		controls: false,
		pause: 8000,
	});

	$('#girlsSlider').bxSlider({
		auto:true,
		speed:1000,
		infiniteLoop: true,
		hideControlOnEnd: false,
		slideWidth: 615,
 		pager: false,
		controls: true,
	});

	//slideshow
	$('#newSlideShow').bxSlider({
		minSlides: 6,
	  	maxSlides: 6,
	  	slideWidth: 590,
	  	ticker: true,
	  	speed: 24000
	});
	//slideshow
	$('#newSlideShow2').bxSlider({
		minSlides: 6,
	  	maxSlides: 6,
	  	slideWidth: 590,
	  	ticker: true,
	  	speed: 24000
	});

	$('#girlsSlideShow').bxSlider({
		minSlides: 4,
	  	maxSlides: 4,
	  	slideWidth: 606,
	  	ticker: true,
	  	speed: 7000
	});
})
</script>
</html>
