お問い合わせフォームから送信されました。
確認を行い、対応して下さい。

■お問い合せ内容
---------------------------------------
掲載希望エリア：　<?php echo $data['Contact']['insert_area']."\n"; ?>
業種：　<?php echo $data['Contact']['category_name']."\n"; ?>
店舗名：　<?php echo $data['Contact']['shop_name']."\n"; ?>
ご担当者名：　<?php echo $data['Contact']['charge_name']."\n"; ?>
ご担当者様のご連絡先：　<?php echo $data['Contact']['charge_contact']."\n"; ?>
ご連絡先メールアドレス：　<?php echo $data['Contact']['emailto']."\n"; ?>
ご連絡可能な時間帯：　<?php echo $data['Contact']['contact_time']."\n"; ?>
店舗URL：　<?php echo $data['Contact']['shop_url']."\n"; ?>
届出確認書or許可書：    <?php if(!empty($data['imgfile']))
{
    echo 'ファイルが添付されております。下記のリンクからダウンロードしてください。' . "\n";    echo $data['imgfile'] . "\n";
}
else {
    echo '添付ファイルなし'."\n"; }
?>
ご質問・ご要望等：
<?php echo $data['Contact']['comment']."\n"; ?>
---------------------------------------
