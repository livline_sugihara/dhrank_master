以下の口コミがレビュワーに更新されました。
総合管理画面より「口コミ情報一覧より」確認を行い、掲載/非掲載を選択して下さい

---------------------------------------
大エリア： <?php echo $area_data['LargeArea']['name']; ?> 
更新日： <?php echo date('Y/m/d H:i'); ?> 
店舗名： <?php echo $review['shop_name']; ?> 
女の子名： <?php echo $review['girl_name']; ?> 
女の子評価： <?php echo round(($review['score_girl_character'] + $review['score_girl_service'] + $review['score_girl_looks']) / 3, 2); ?> 
投稿者： <?php echo $review['post_name']; ?> 
コメント： <?php echo $review['comment']; ?> 
タイトル： <?php echo $review['post_title']; ?> 
---------------------------------------
