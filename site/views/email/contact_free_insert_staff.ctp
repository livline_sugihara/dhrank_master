お問い合わせフォームから送信されました。
確認を行い、対応して下さい。

■お問い合せ内容
---------------------------------------
掲載希望エリア：　<?php echo $data['Contact']['insert_area2']. "\n"; ?>業種：　<?php echo $data['Contact']['category_name2']. "\n"; ?>店舗名：　<?php echo $data['Contact']['shop_name2']. "\n"; ?>ご担当者名：　<?php echo $data['Contact']['charge_name2']. "\n"; ?>
ご担当者様のご連絡先：　<?php echo $data['Contact']['charge_contact2']. "\n"; ?>
ご連絡先メールアドレス：　<?php echo $data['Contact']['emailto2']. "\n"; ?>
店舗URL：　<?php echo $data['Contact']['shop_url2']. "\n"; ?>
店舗電話番号：　<?php echo $data['Contact']['shop_tel2']. "\n"; ?>
住所or（…発）：　<?php echo $data['Contact']['shop_address2']. "\n"; ?>
営業時間：　<?php echo $data['Contact']['shop_business_hours2']. "\n"; ?>
最低料金：　<?php echo $data['Contact']['lowest_cost2']. "\n"; ?>
先行予約：　<?php echo $umu_select[$data['Contact']['reserve2']]. "\n"; ?>
領収書：　<?php echo $umu_select[$data['Contact']['receipt2']]. "\n"; ?>
待ち合わせ：　<?php echo $umu_select[$data['Contact']['layover2']]. "\n"; ?>
コスプレ：　<?php echo $umu_select[$data['Contact']['cosplay2']]. "\n"; ?>
クレジットカード：　<?php echo $umu_select[$data['Contact']['credit2']]. "\n"; ?>
届出確認書or許可書：
<?php
if(!empty($data['imgfile'])){
    echo 'ファイルが添付されております。下記のリンクからダウンロードしてください。' . "\n";
    echo $data['imgfile']. "\n";
} else {
    echo '添付ファイルなし'."\n";
}
?>
ご質問・ご要望等：
<?php echo $data['Contact']['comment2']. "\n"; ?>

---------------------------------------
