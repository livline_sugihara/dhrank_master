<section id="middle" class="full">
    
    <div id="entryArea">
        <!--  body -->
        <div class="body">
            <div class="listHeading">クチコミ投稿のご注意</div>
            <p>女の子のルックスや性格、サービスなどの良かったところをみんなでシェアするサイトです。女の子が傷つくような誹謗中傷などはお控えください。<br />
            該当する書き込みがあった場合、編集部にて削除させて頂くことがあります。
            </p>
            
            <div class="listHeading">（確認）クチコミ投稿フォーム</div>
            <div class="reviewForm">

                <?php echo $form->create(null,array('type'=>'post','action'=> '')); ?>
                
                    <div class="reviewForm">
                    <table cellspacing="0" cellpadding="0" border="0" class="noLCS">
                        <tr>
                            <th>店名</th>
                            <td>
                                <?php echo $data['Review']['shop_name']; ?>
                                <?php echo $form->hidden('Review.shop_name'); ?>
                            </td>
                            
                        </tr>
                        <tr>
                            <th>女の子</th>
                            <td>
                                <?php echo $data['Review']['girl_name']; ?>
                            </td>
                            
                        </tr>
                        <tr>
                            <th>コース／料金</th>
                            <td>
                                <?php echo $data['Review']['course_minute']; ?>分 <?php echo $data['Review']['course_cost']; ?>円
                            </td>
                        </tr>
                        <tr>
                            <th>ご利用場所</th>
                            <td>
                                <?php echo $m_reviews_place[$data['Review']['using_place_id']]; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>指名</th>
                            <td>
                                <?php echo $appointed_type[$data['Review']['appointed_type']]; ?>・<?php echo $appointed_sub[$data['Review']['appointed_sub']]; ?>
                            </td>
                        </tr>
                    </table>
                    </div>
                    
                    <div class="reviewFormRed">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th>ルックス</th>
                            <td>
                                <?php echo $score_list[$data['Review']['score_girl_looks']]; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>性格</th>
                            <td>
                                <?php echo $score_list[$data['Review']['score_girl_service']]; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>サービス</th>
                            <td>
                                <?php echo $score_list[$data['Review']['score_girl_character']]; ?>
                            </td>
                        </tr>
                    </table>
                    </div>
                    
                    <div class="reviewForm">
                    <table cellspacing="0" cellpadding="0" border="0" class="noLCS">
                        <tr>
                            <th>クチコミ</th>
                            <td>
                                <?php echo nl2br($data['Review']['comment']); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>この女の子の良さを一言でいうと<br>(※タイトルとして表示されます）</th>
                            <td><?php echo $data['Review']['post_title']; ?></td>
                        </tr>
                    </table>
                    </div>
                    <p class="align_c">
                        <?php echo $form->submit('入力内容を送信する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'register'));?>
                    </p>
                <?php echo $form->end(); ?>
            </div>
        </div>
        <!--  /body -->
    </div>
</section>