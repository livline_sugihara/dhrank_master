<div class="link_o"><h2>口コミ通報フォーム</h2></div>
<div class="label_3">口コミ通報について</div>

<p>この口コミを不審に感じた理由をお書き下さい。<br />
    対応につきましては「利用規約」に違反しているか、慎重に調査、検討を進めた上判断させて頂きます。<br />
    この機能の悪用も「利用規約」の違反となりますのでご注意下さい。<br />
    また通報へのご返答は出来かねますのでご了承ください。
 </p>

    <h3 class="label">（入力）口コミ通報フォーム</h3>
    
<section class="contact">

    <?php echo $form->create(null,array('type'=>'post','action'=> '')); ?>
    <?php echo $form->hidden('ReviewsReport.report_review_id', array('value' => $review_data['Review']['id'])); ?>

    <table style="padding:3%;">
        <tr>
            <th>レビュアー名</th>
            <td>
                <?php echo $review_data['Review']['post_name']; ?>
        	</td>
        </tr>
        <tr>
            <th>店名</th>
            <td>
                <?php echo $review_data['Review']['shop_name']; ?>
            </td>
        </tr>
        <tr>
            <th>女の子</th>
            <td>
                <?php echo $review_data['Review']['girl_name']; ?>
            </td>
        </tr>
        <tr>
            <th>レビュー内容</th>
            <td>
                <?php echo nl2br($review_data['Review']['comment']); ?>
            </td>
        </tr>
        <tr>
        	<th>通報の理由</th>
                <td>
                	<p>
                        <?php echo $form->radio('ReviewsReport.report_type_id', $m_reviews_reports_types, array('legend' => false, 'label' => true, 'separator' => '　')); ?>
                        <?php echo $form->error('ReviewsReport.report_type_id'); ?>
                    </p>
                        <?php echo $form->textarea('ReviewsReport.comment'); ?>
                    	<?php echo $form->error('ReviewsReport.comment'); ?>
                </td>
        </tr>
    </table>
                        
    <p class="align_c">
        <?php echo $form->submit('内容を送信する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'confirm'));?>
    </p>

    <?php echo $form->end(); ?>

</section>

<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../../top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">口コミ通報</span></a></li>
	</ul>
</nav>