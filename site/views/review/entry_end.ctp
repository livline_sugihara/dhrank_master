<section id="middle" class="full">
    
    <div id="entryArea">
        <!--  body -->
        <div class="body">
        	<div class="listHeading">口コミ投稿の完了</div>
        	<p>口コミの投稿が完了しました。<br />反映されるまで、1日〜2日程度お時間がかかる場合がありますので、ご了承ください。</p>
        </div>

        <div align="center" style="margin-top: 40px;">
			<a href="<?php echo $linkCommon->get_top(); ?>" class="css_btn_class2">トップへ戻る</a>
		</div>
    </div>
</section>
