<div data-role="content" data-theme="a">

<h2 class="midashih2">
<?php echo $m_rank_business_category['MShopsBusinessCategory']['name']?>ランキング
</h2>

<ul data-role="listview" data-divider-theme="a">
<?php $paginator->options(array('url' => array('business_category_id' => $this->params['business_category_id']))); ?>
<?php foreach($ranking as $key => $record){?>
	<li>
    	<a href="<?php echo $linkCommon->get_user_shop($record)?>">
        	<p style="margin-left:-12px"><img src="/images/s/crown.png" width="14px" /><?php echo (($paginator->counter() - 1) * $pcount['rankings'] + ($key + 1))?>位 - <?php echo $record['Shop']['name']?></p>
			<p style="margin-left:-12px"><span class="small">総合評価</span><?php for($i = 0; $i < 5; $i++){?><img src="/images/s/star_<?php echo ($i+1 <= round($record[0]['point']))?'on':'off';?>.png" width="16px" style="vertical-align:middle;" /><?php }?> <span class="spot4"><?php echo round($record[0]['point'],2)?></span> <span class="small">[口コミ <?php echo $record[0]['count']?>件]</span></p>
			<p style="margin-bottom:-5px;margin-left:-12px"><?php echo $imgCommon->get_rank($record,1,45,60)?><?php echo $imgCommon->get_rank($record,2,45,60)?><?php echo $imgCommon->get_rank($record,3,45,60)?><?php echo $imgCommon->get_rank($record,4,45,60)?><?php echo $imgCommon->get_rank($record,5,45,60)?><?php echo $imgCommon->get_rank($record,6,45,60)?></p>
</a>
    </li>



<?php }?>
</ul>


<?php echo $this->element('pagenate/s_numbers'); ?>

