<section id="middle" class="wide">

    <div class="categoryRankingArea">

        <div class="title title03">
            <h2>REVIEWER ranking <span class="small">口コミ投稿者ランキング</span></h2>
        </div>        <?php foreach ($reviewer as $key => $record): ?>

    		<?php
    			$rank_image = 'arrow_up2.png';
    			$record['Reviewer']['id'] = $record['rankings']['ranking_id'];
    			$record['Reviewer']['handle'] = $record['rankings']['ranking_name'];
    			echo $this->element('common/_ranking_reviewer', array(
    				'ii' => $key,
    				'record' => $record,
    				'rank_image' => $rank_image,
    			));
    		?>

    	<?php endforeach; ?>

    </div>
    <!-- /rankingArea -->

    <?php echo $this->element('common/top_review_bunner'); ?>

</section>

<section id="side" class="narrow">
	<?php echo $this->element('common/side_narrow'); ?>
</section>
