<?php //var_dump($this->getVars()); ?>
<?php //var_dump($parent_area); ?>
<?php // ログインネーム ?>
<?php echo $this->element('s_common/name_display'); ?>
<?php echo $this->element('s_common/breadcrumb'); ?>
<?php // Newサーチ ?>
<?php echo $this->element('s_common/top_new_search'); ?>
<?php //バナー ?>
<?php echo $this->element('s_common/top_banner'); ?>
<?php // 新着店舗 ?>
<?php echo $this->element('s_common/top_newshopslide'); ?>

<?php // 新着口コミ ?>
<?php echo $this->element('s_common/top_newest_review'); ?>

<?php //検索窓 ?>
<?php //echo $this->element('common/search'); ?>

<?php // 参考になった口コミ ?>
<?php echo $this->element('s_common/top_review_ranking'); ?>
<?php // このエリアのイチオシ ?>
<?php echo $this->element('s_common/top_ichioshi'); ?>
	<section id="rank_box">
		<p id="rank_box_tt" class="link_pi">各ランキング結果</p>
<!-- ispot アイスポット -->
		<div id="tabsArea">
			<ul id="tabs" class="clearfix">
				<li class="select">お店総合<br>ランキング</li>
				<li>アクセスの多い<br>女の子ランキング</li>
				<li>口コミ投稿者<br>ランキング</li>
			</ul>
<!-- content_wrap(お店総合) -->
			<div class="content_wrap">
<?php echo $this->element('s_common/top_ranking_shop'); ?>
			</div>
<!-- /content_wrap -->
<!-- content_wrap -->
			<div class="content_wrap" style="display:none">
<?php echo $this->element('s_common/top_ranking_girl_access'); ?>
			</div>
<!-- /content_wrap -->
<!-- content_wrap -->
			<div class="content_wrap" style="display:none">
<?php echo $this->element('s_common/top_ranking_reviewer'); ?>
			</div>
<!-- /content_wrap -->
		</div>
<!-- / ispot アイスポット -->
	</section>

<?php // ピックアップ口コミ ?>
<?php echo $this->element('s_common/top_pic_review'); ?>

<?php // レコメンドgirl ?>
	<div id="recommendGirlsArea">
<?php echo $this->element('s_common/top_recommend_girl'); ?>
	</div>

<?php // 店舗検索 ?>
<?php echo $this->element('s_common/top_search'); ?>

<?php // 店舗カテゴリ ?>
<?php //echo $this->element('s_common/top_shop_category'); ?>

<?php //エリアメニュー等 ?>
<?php echo $this->element('s_common/footer_menu'); ?>

<?php // エリア一覧 ?>
<!-- <?php echo $this->element('s_common/top_area_list'); ?> -->

<?php // クーポンから選択 ?>
<section class="top_coupon_search">
	<?php //echo $this->Form->create(); ?>
	<?php //echo $this->Form->radio('targetlist', array(1 => '新規', 2 => '会員', 9 => '全員')); ?>
	<?php //echo $this->Form->submit('選択'); ?>
	<?php //echo $this->Form->end(); ?>
</section>
<section class="top_coupon_display">
	<?php
		if(isset($coupons))
		{
			foreach($coupons as $c)
			{
				echo $c['Coupon']['content'];
			}
		}
	?>
</section>
<?php echo $this->element('s_common/footer_banner'); ?>

<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
	</ul>
</nav>

<?php //バナー ?>
<?php echo $this->element('s_common/bottom_banner'); ?>
