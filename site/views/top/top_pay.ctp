<section id="middle" class="narrow">


    <!-- new_shopArea -->
    <div id="new_shopArea">
        <?php echo $this->element('common/top_newshopslide'); ?>
    </div>
		<!-- heavy_reviewArea -->
		<div id="heavy_reviewArea">
	        <?php echo $this->element('common/top_newest_review'); ?>
		</div>
		<!-- /heavy_reviewArea -->

		<!-- ispotArea -->
		<div id="ispotArea">

			<!-- review ranking -->
			<?php echo $this->element('common/top_review_ranking'); ?>
			<!-- /review ranking -->

			<!-- PickUp -->
	        <?php echo $this->element('common/top_pic_review'); ?>
			<!-- /PickUp -->
		</div>
		<!-- /ispotArea -->

		<!-- tabArea -->
		<div id="tabsArea">
			<ul id="tabs" class="clearfix">
				<li class="select">お店総合</li>
				<li>アクセスの多い女の子</li>
				<li>クチコミ投稿者</li>
                <li class="rank_comment">週間ランキング | 毎週日曜日更新</li>
			</ul>

			<!-- content_wrap(お店総合) -->
			<div class="content_wrap">
				<?php echo $this->element('common/top_ranking_shop'); ?>
			</div>
			<!-- /content_wrap -->

			<!-- content_wrap -->
			<div class="content_wrap" style="display:none">
				<?php echo $this->element('common/top_ranking_girl_access'); ?>
			</div>
			<!-- /content_wrap -->

			<!-- content_wrap -->
			<div class="content_wrap" style="display:none">
				<?php echo $this->element('common/top_ranking_reviewer'); ?>
			</div>
			<!-- /content_wrap -->

		</div>
		<!-- /tabArea -->


		<?php echo $this->element('common/top_review_bunner'); ?>

		<!-- recommendGirlsArea -->
		<div id="recommendGirlsArea">
			<?php echo $this->element('common/top_recommend_girl'); ?>
		</div>
		<!-- /recommend01Area -->

		<!-- categoryArea -->
		<div id="categoryArea">
			<?php echo $this->element('common/top_category'); ?>
		</div>
		<!-- /categoryArea -->

        <!-- new_shopArea -->
        <div id="new_shopArea2">
            <?php echo $this->element('common/top_newshopslide02'); ?>
        </div>
            <!-- new_shopArea -->
            <?php echo $this->Html->image('top_foot_banner.png'); ?>
	</section>

	<section id="side" class="wide">
		<?php echo $this->element('common/side_wide'); ?>
	</section>
