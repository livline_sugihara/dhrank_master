<?php // ログインネーム ?>
<?php echo $this->element('s_common/name_display'); ?>
<?php // 新着店舗 ?>
<?php echo $this->element('s_common/top_newshopslide'); ?>
<?php // バナー ?>

<section id="banner">
	<p>
		バナー
	</p>
</section>

<?php // 新着口コミ ?>
<?php echo $this->element('s_common/top_newest_review'); ?>
<?php // 参考になった口コミ ?>
<?php echo $this->element('s_common/top_review_ranking'); ?>
<?php // このエリアのイチオシ ?>
<?php echo $this->element('s_common/top_ichioshi'); ?>
	<section id="rank_box">
		<p id="rank_box_tt" class="link_pi">各ランキング結果</p>
<!-- ispot アイスポット -->
		<div id="tabsArea">
			<ul id="tabs" class="clearfix">
				<li class="select">お店総合<br>ランキング</li>
				<li>アクセスの多い<br>女の子ランキング</li>
				<li>口コミ投稿者<br>ランキング</li>
			</ul>
<!-- content_wrap(お店総合) -->
			<div class="content_wrap">
<?php echo $this->element('s_common/top_ranking_shop'); ?>
			</div>
<!-- /content_wrap -->
<!-- content_wrap -->
			<div class="content_wrap" style="display:none">
<?php echo $this->element('s_common/top_ranking_girl_access'); ?>
			</div>
<!-- /content_wrap -->
<!-- content_wrap -->
			<div class="content_wrap" style="display:none">
<?php echo $this->element('s_common/top_ranking_reviewer'); ?>
			</div>
<!-- /content_wrap -->
		</div>
<!-- / ispot アイスポット -->
	</section>

<?php // ピックアップ口コミ ?>
<?php echo $this->element('s_common/top_pic_review'); ?>

<?php // レコメンドgirl ?>
	<div id="recommendGirlsArea">
<?php echo $this->element('s_common/top_recommend_girl'); ?>
	</div>

<?php // 店舗検索 ?>
<?php echo $this->element('s_common/top_search'); ?>

<?php // 店舗カテゴリ ?>
<?php echo $this->element('s_common/top_shop_category'); ?>

	</div>
</section>
<?php echo $this->element('s_common/footer_banner'); ?>

<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
	</ul>
</nav>
