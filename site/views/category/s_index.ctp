	<div class="link_o"><h2>検索結果一覧</h2></div>
	<div class="label2_2"><h3 class="label2_ttl"><?php echo $category['MShopsBusinessCategory']['name']; ?></h3></div>
	<div class="kensuu"><p>全<span class="font18"><strong><?php echo $paginator->counter(array('format' => '%count%</strong></span>件中 <span>%start%</span>～<span>%end%</span>件表示')); ?></p></div>




	<section id="rankingArea">
<?php
foreach($list as $key => $record) {
    $opentime = date('H:i', strtotime($record['System']['open_time']));
    $closetime = date('H:i', strtotime($record['System']['close_time']));

    if($record['UsersAuthority']['is_show_all_contents_by_categorylist'] == 1) {
//        print_r($record);
?>
		<div class="rank_box bigger underline">
			<p class="shop_tt"><?php echo $record['Shop']['name']; ?><span>UP</span></p>
			<ul class="shop_icon clearfix">
			<?php if($record['Shop']['service01'] == 1) { ?><li class="pay_yoyaku">先行予約OK</li><?php } ?>
			<?php if($record['Shop']['service02'] == 1) { ?><li class="pay_credit">クレジットOK</li><?php } ?>
			<?php if($record['Shop']['service03'] == 1) { ?><li class="pay_ryoshu">領収書OK</li><?php } ?>
			<?php if($record['Shop']['service04'] == 1) { ?><li class="pay_machi">待ち合わせOK</li><?php } ?>
			<?php if($record['Shop']['service05'] == 1) { ?><li class="pay_cosplay">コスプレあり</li><?php } ?>
			<?php if($record['Shop']['service06'] == 1) { ?><li class="pay_coupon">クーポンあり</li><?php } ?>
			</ul>
				<div class="clearfix">
					<div class="box_left">
						<a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo $imgCommon->get_shop_with_time($record, 'list_sp', array(160,210)); ?></a>
						<p class="btn_red" style="margin-top:5px;"><a href="<?php echo $linkCommon->get_user_shop($record); ?>">店舗詳細を見る</a></p>
					</div>
					<div class="box_right">
						<p class="rank_txt"><a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo nl2br($record['Shop']['about_title']); ?></a></p>
						<ul class="clearfix">
							<li class="icon_access"><?php echo $categories_value_list[$record['Shop']['shops_business_category_id']];?>/<?php echo $record['System']['address']; ?></li>
							<li class="icon_people">在籍数<br><?php echo $record[0]['girl_cnt']; ?>人</li>
							<li class="icon_price">最安値料金<br><?php echo $record['System']['price_cost']; ?>円～</li>
							<li class="icon_kuchikomi_up">口コミ<br><?php echo $record[0]['review_cnt']; ?>件</li>
							<li class="icon_time_up">営業時間<br><?php echo $opentime; ?>〜<?php echo $closetime; ?></li>
						</ul>
					</div>
				</div>
		</div><!-- /.rank_box-->
<?php
    } else {
?>

		<div class="rank_box bigger underline">
			<div class="clearfix">
				<p class="shop_tt f_left"><a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo $record['Shop']['name']; ?></a></p>
				<div class="box_right2 f_right">
					<ul>
						<li class="icon_kuchikomi_up">口コミ<br><?php echo $record[0]['review_cnt']; ?>件</li>
					</ul>
				</div>
			</div>
		</div><!-- /.rank_box-->
<?php
    }
}
?>

    </div>
    <!-- /pay_shopList -->

<?php echo $this->element('s_pagenate/footer_pagenate'); ?>
	</section>
<?php echo $this->element('s_common/footer_banner'); ?>