<!-- middle -->
<section id="middle" class="two">
    <div class="title_bar">お気に入りの女の子</div> 
    <div class="inner">
        <div class="girlsArea clearfix">
<?php
$cnt = 0;
foreach($favorite_girls as $key => $record) {
?>
            <div>
                <p class="img">
                    <a href="<?php echo $linkCommon->get_user_girl($record); ?>">
                        <?php echo $imgCommon->get_girl_with_time($record, array(240, 320)); ?>
                    </a>
                </p>
                <p class="name"><a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo $record['Girl']['name']; ?></a></p>
                <p><?php echo $record['LargeArea']['name']; ?></p>
                <p><?php echo $record['Shop']['name']; ?></p>
                <p class="btn bookmarkGirl" data="<?php echo $record['Girl']['id']; ?>" mode="delete">
                	<a href="#" class="css_btn_class" onClick="delete_favorite()">削除</a>
                </p>
            </div>

<?php
    if($cnt % 4 == 3) {
?>
            <p class="clearLine"></p>
<?php
    }
    $cnt++;
}
?>
        </div>
    </div>
</section>
<script>
function delete_favorite() {

    if(!confirm('女の子をお気に入りから外しても宜しいでしょうか？')) {
        return false;
    }

    // TODO 実装
}
</script>