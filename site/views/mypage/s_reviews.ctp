<!-- middle -->
<section id="middle" class="two">
    <div class="title_bar"><?php echo $reviewer['Reviewer']['handle']; ?>さん レビュー一覧</div> 
    <div id="reviewListArea" class="inner">
    
        <div class="new_reviewArea clearfix">
            
<?php
$cnt = count($list);
for ($ii = 0; $ii < $cnt; $ii++) {
            echo $this->element('s_common/_review', array(
            	"record" => $list[$ii],
            ));
}     
?>
            
        </div>
                        
    </div>
</section>

	<p class="btn_left"><a href="/s/reviewer/profile">My Top</a></p>
<!--	<p class="btn_left"><a href="/s/reviewer">自分の更新</a></p> -->
	<p class="btn_left"><a href="/s/reviewer/myreview">Myレビュー</a></p>
	<p class="btn_left"><a href="/s/reviewer/follow">フォローしている</a></p>
	<p class="btn_left"><a href="/s/reviewer/follower">フォローされている</a></p>
	<p class="btn_left"><a href="/s/reviewer/favorite_shop">お気に入りのお店</a></p>
	<p class="btn_left"><a href="/s/reviewer/favorite_girl">お気に入りの女の子</a></p>
	<p class="btn_left"><a href="/s/reviewer/favorite_review">お気に入りのレビュー</a></p>