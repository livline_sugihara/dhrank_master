<div class="label"><h2>プロフィール更新完了</h2></div>
<section id="account_form">

    <p>
    	プロフィールの更新が完了しました。
    </p>

</section>

	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile($record = null)?>">My Top</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_top($record = null)?>">自分の更新</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_myreview($record = null)?>">Myレビュー</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_follow($record = null)?>">フォローしている</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_follower($record = null)?>">フォローされている</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_shop($record = null)?>">お気に入りのお店</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_girl($record = null)?>">お気に入りの女の子</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_review($record = null)?>">お気に入りのレビュー</a></p>
<!--	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile_edit($record = null)?>">会員情報の確認・変更</a></p> -->