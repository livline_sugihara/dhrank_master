	<div class="label"><h2><?php echo $reviewer_data['Reviewer']['handle']; ?>さんのプロフィール</h2></div>
	<section id="account_area">
		<div id="account_area_profile">
			<div class="clearfix">
				<p class="img"><?php echo $imgCommon->get_reviewer_avatar($reviewer_data, array(100,100)); ?></p>
				<div class="summary">
					<p class="tt"><?php echo $reviewer_data['Reviewer']['handle']; ?>さん</p>
					<p class="data"><?php echo $reviewer_data['MReviewersAge']['name']; ?></p>
					<p class="data"><?php echo $reviewer_data['MReviewersJob']['name']; ?></p>

						<p class="css_btn_class followReviewer" data="<?php echo $reviewer_data['Reviewer']['id']; ?>" mode="add">
							<a href="#" class="css_btn_class"><img src="../../../img/s/follow_btn.png" alt="フォローする" height="20"></a>
						</p>
						
				</div>
			</div>
		</div>

		<p class="label_list">その他会員情報</p>
		<p class="label_content">地域：<?php echo $reviewer_data['LargeArea']['name']; ?></p>
		<p class="label_content">よく利用するカテゴリ：<?php echo $reviewer_data['MShopsBusinessCategory']['name']; ?></p>
		<p class="label_list">自己紹介</p>
		<p class="label_content"><?php echo $reviewer_data['Reviewer']['comment']; ?></p>

<!--     <div class="title_bar">プロフィール<span><a href="<?php echo $linkCommon->get_reviewer_profile_edit(); ?>">編集する</a></span></div>
		<dl>
			<dt>年代</dt>
			<dd><?php echo $reviewer_data['MReviewersAge']['name']; ?></dd>
			<dt>職業</dt>
			<dd><?php echo $reviewer_data['MReviewersJob']['name']; ?></dd>
			<dt>ニックネーム</dt>
			<dd><?php echo $reviewer_data['Reviewer']['handle']; ?></dd>
			<dt>地域</dt>
			<dd><?php echo $reviewer_data['LargeArea']['name']; ?></dd>
			<dt>よく使用するカテゴリー</dt>
			<dd><?php echo $reviewer_data['MShopsBusinessCategory']['name']; ?></dd>
			<dt>自己紹介</dt>
			<dd><?php echo $reviewer_data['Reviewer']['comment']; ?></dd>
		</dl> -->
	</section>
	
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile($record = null); ?>">My Top</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_profile($reviewer_data); ?>">プロフィール</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_reviews($reviewer_data); ?>">レビュー</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_follow($reviewer_data); ?>">フォローしている</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_follower($reviewer_data); ?>">フォローされている</a></p>
	
<!--
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile($record = null); ?>">My Top</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_profile($reviewer_data); ?>">プロフィール</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_reviews($reviewer_data); ?>">レビュー</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_follow($reviewer_data); ?>">フォローしている</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_follower($reviewer_data); ?>">フォローされている</a></p>
-->