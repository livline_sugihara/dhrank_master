	<div class="label"><h2><?php echo $reviewer_data['Reviewer']['handle']; ?>さんのレビュー一覧</h2></div>
	<section id="pickup_box">
<?php
	$cnt = count($list);
	for ($ii = 0; $ii < $cnt; $ii++) {
		echo $this->element('s_common/_review', array("record" => $list[$ii]));
	}
?>
	</section>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile($record = null); ?>">My Top</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_profile($reviewer_data); ?>">プロフィール</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_reviews($reviewer_data); ?>">レビュー</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_follow($reviewer_data); ?>">フォローしている</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_follower($reviewer_data); ?>">フォローされている</a></p>
