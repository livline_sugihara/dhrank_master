<section id="middle" class="two">
    <div class="title_bar">プロフィール</div> 
    <div class="inner profileTable">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <th>年代</th>
                <td><?php echo $reviewer_data['MReviewersAge']['name']; ?></td>
                <th>職業</th>
                <td><?php echo $reviewer_data['MReviewersJob']['name']; ?></td>
            </tr>
            <tr>
                <th>ニックネーム</th>
                <td><?php echo $reviewer_data['Reviewer']['handle']; ?></td>
                <th>地域</th>
                <td><?php echo $reviewer_data['LargeArea']['name']; ?></td>
            </tr>
            <tr>
                <th>よく利用する<br>カテゴリ</th>
                <td><?php echo $reviewer_data['MShopsBusinessCategory']['name']; ?></td>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th>自己紹介</th>
                <td colspan="3">
                    <?php echo $reviewer_data['Reviewer']['comment']; ?>
                </td>
            </tr>
        </table>        
    </div>
</section>