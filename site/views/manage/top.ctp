<!doctype php>
<?php require("fukuoka/parts/head.php"); ?>
<body>
<!-- header -->
<header>
	<div class="container">
		<h1>ランキングと口コミで探せるデリヘル情報サイト・店舗管理トップ</h1>


		<div class="header_form">
        ようこそ <a href="./fukuoka/shop_index.php" target="_blank">一夜妻</a>様　
        	<p class="redBtn barBtn"><a href="entrance.php">ログアウト</a></p>
        </div>
	</div>
</header>
<!-- / header -->
    
    
    <div id="topic_path">
        <a href="shop_top.php">店舗管理・トップページ</a>
    </div>

	<section class="container">

	<h2 class="heading">一夜妻様・店舗管理</h2>

    	<!-- menu -->
		<section id="menu" class="two">
        	<?php include("./fukuoka/parts/menu_shop.php"); ?>
        </section>

		<!-- middle -->
        <section id="middle" class="two">
        	<div class="title_bar">最新の更新情報</div> 
        	<div id="timeLineArea" class="inner">
            
            	<div class="new_reviewArea clearfix">
                	
                    <!-- timeLine-->
                    <div class="timeLine clearfix">
                    	<div><img src="common/img/icon_avatar.jpg"></div>
                        <div class="text">
                        	<p class="date">2014/10/10</p>
                        	<p>○○さんが△△さんのレビューをお気に入りに追加しました。</p>
                    	</div>
                    </div>
                    <?php
                    	include("./fukuoka/parts/_review.php");
						echo $review;   
					?>
                    
                    <!-- timeLine-->
                    <div class="timeLine clearfix">
                    	<div><img src="common/img/icon_avatar.jpg"></div>
                        <div class="text">
                        	<p class="date">2014/10/10</p>
                        	<p>○○さんが△△さんのレビューをお気に入りに追加しました。</p>
                    	</div>
                    </div>
                    <?php echo $review; ?>
                    
                <!-- timeLine-->
                <div class="timeLine clearfix">
                    <div><img src="common/img/icon_avatar.jpg"></div>
                    <div class="text">
                        <p class="date">2014/10/10</p>
                        <p>○○さんが「店名」の■■ちゃんをお気に入りに追加しました。</p>
                    </div>
                </div>
                <div class="girlsArea clearfix">
                    <div>
                        <p class="img"><a href="shop_detail.php"><img src="common/img/girl_image_big1.jpg" class="img_frame"></a></p>
                        <p class="name"><a href="shop_detail.php">名前名前名前</a></p>
                        <p>エリア名</p>
						<p>店名</p>
                    </div>
                </div>
                
                <!-- timeLine-->
                <div class="timeLine clearfix">
                    <div><img src="common/img/icon_avatar.jpg"></div>
                    <div class="text">
                        <p class="date">2014/10/10</p>
                        <p>○○さんが「店名」をお気に入りに追加しました。</p>
                    </div>
                </div>
                <!-- pay_shopList -->
                <div id="pay_shopList">
                                
                    <!--shop-->
                    <div class="shopList_frame">
                        <div>
                            <a href="shop_index.php" class="shopName">プリティガール</a>　<span class="kuchikomi">UP</span><a href="shop_index.php" class="css_btn_class">詳細を見る</a>
                        </div>
                        <div class="pay_mark clearfix">
                            <p class="pay_yoyaku">先行予約OK</p>
                            <p class="pay_credit">クレジットOK</p>
                            <p class="pay_ryoshu">領収書OK</p>
                            <p class="pay_machi">待ち合わせOK</p>
                            <p class="pay_cosplay">コスプレあり</p>
                            <p class="pay_coupon">クーポンあり</p>
                        </div>
                    </div>
                    
                    <div class="shopList_main narrow clearfix">
                        <div>
                            <p class="catchCopy"><b><strong>キャッチコピーが入りますキャッチコピーが入りますキャッチコピーが入りますキャッチコピーが入ります</strong></b></p>
                            <p>説明文が入ります説明文が入ります説明文が入ります説明文が入ります説明文が入ります説明文が入ります</p>
                            
                            <div class="shopListLeft">
                                <dl class="clearfix">
                                    <dt>営業時間</dt>
                                    <dd>12:00〜0:00</dd>
                                
                                    <dt>最低料金</dt>
                                    <dd>16,000円〜</dd>
        
                                    <dt>住所</dt>
                                    <dd>博多駅前発</dd>
                                </dl>   
                                <p class="shopNum">
                                    <span class="zaiseki">在籍数:</span><span class="num">25人</span>
                                    <span class="kuchikomi2">口コミ:</span><span class="num">90件</span>
                                </p>
                            </div>
                            <div class="shopListRight">
                                <p><img src="common/img/shop_banner.jpg" /></p>
                            </div>
                            <div class="shopListBottom clearfix">
                                <ul>
                                    <li><span class="type1">新規</span><a href="shop_coupon.php">ダミーテキストダミーテキストダミーテキストダミーテキスト</a></li>
                                    <li><span class="type2">会員</span><a href="shop_coupon.php">ダミーテキストダミーテキスト</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /shop -->
            	</div>
                    
        	</div>
    	</section>
        <!-- / middle -->
        
	</section>

    
    <?php require("fukuoka/parts/footer2.php"); ?>
</body>
</php>