	<section id="middle" class="wide">

<!-- 店舗トップ共通部分 -->
<?php echo $this->element('common/shop_top'); ?>
<!-- /店舗トップ共通部分 -->
<!-- tabInner -->
		<div class="tabInner">
<?php
	// 有料の場合のみ
	if($parent_shop['UsersAuthority']['is_show_all_contents_by_shop'] == 1) {
?>
<!-- 紹介ブロック -->
<?php echo $this->element('common/shop_profile'); ?>
<!-- /紹介ブロック -->
<?php
	}
?>

<!-- レビュー -->
<?php echo $this->element('common/shop_review'); ?>
<!-- /レビュー -->

<!-- おすすめ -->
<?php echo $this->element('common/shop_recommend'); ?>
<!-- /おすすめ -->

<?php
	// 有料の場合のみ
	if($parent_shop['UsersAuthority']['is_show_all_contents_by_shop'] == 1) {

	// 激押し売れっ子権限がある場合 -> 解除
	//	if($parent_shop['UsersAuthority']['is_recommend_girl'] == 1) {
?>

<?php
	//	}
?>
<!-- クーポン情報 -->
		<?php echo $this->element('common/shop_coupon'); ?>
<!-- /クーポン情報 -->

<!-- 店長からのお知らせ -->
		<?php echo $this->element('common/shop_owner_comment'); ?>
<!-- /店長からのお知らせ -->

<!-- 店舗からのお知らせ -->
		<?php echo $this->element('common/shop_info'); ?>
<!-- /店舗からのお知らせ -->

<?php
	}
?>






			</div>
<!-- /tabInner -->
	</section>

<section id="side" class="narrow">
<?php echo $this->element('common/side_narrow'); ?>
</section>
