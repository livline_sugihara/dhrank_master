<section id="middle" class="wide">

    <!-- 店舗トップ共通部分 -->
    <?php echo $this->element('common/shop_top'); ?>
    <!-- /店舗トップ共通部分 -->

    <!-- tabInner -->
    <div class="tabInner">

        <h2 class="heading">お店のシステム</h2>
        <div class="shopTable">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <th>店名</th>
                    <td colspan="3"><?php echo $parent_shop['Shop']['name']; ?></td>
                </tr>
                <tr>
                    <th>業種</th>
                    <td colspan="3"><?php echo $parent_shop['MShopsBusinessCategory']['name']; ?></td>
                </tr>
                <tr>
                    <th>電話番号</th>
                    <td colspan="3"><?php echo $parent_shop['Shop']['phone_number']; ?></td>
                </tr>
                <tr>
                    <th>住所</th>
                    <td colspan="3"><?php echo $parent_shop['System']['address']; ?></td>
                </tr>
                <tr>
                    <th>アクセス</th>
                    <td colspan="3"><?php echo $parent_shop['System']['access']; ?></td>
                </tr>
<?php
$opentime = date('H:i', strtotime($parent_shop['System']['open_time']));
$closetime = date('H:i', strtotime($parent_shop['System']['close_time']));
?>
                <tr>
                    <th>営業時間</th>
                    <td><?php echo $opentime; ?>〜<?php echo $closetime; ?></td>
                    <th>在籍数</th>
                    <td><?php echo $count_girls; ?>人</td>
                </tr>

                <tr>
                    <th>最低料金</th>
                    <td><?php echo $parent_shop['System']['price_cost']; ?>円～</td>
                    <th>掲載口コミ数</th>
                    <td><?php echo $review_data[0]['count']; ?>件</td>
                </tr>
<!--                 <tr class="price_row">
                    <th colspan="4">料金体系</th>
                </tr>
                <tr class="price_row">
                    <td colspan="4">
                        <table>
                            <tr>
                                <th>60分</th>
                                <td>10,000円</td>
                            </tr>
                            <tr>
                                <th>90分</th>
                                <td>20,000円</td>
                            </tr>
                            <tr>
                                <th>120分</th>
                                <td>400,000円</td>
                            </tr>
                            <tr>
                                <th>パネル指名料</th>
                                <td>10,000,000円</td>
                            </tr>
                            <tr>
                                <th>60分</th>
                                <td>10,000円</td>
                            </tr>
                            <tr>
                                <th>90分</th>
                                <td>20,000円</td>
                            </tr>
                            <tr>
                                <th>120分</th>
                                <td>400,000円</td>
                            </tr>
                            <tr>
                                <th>パネル指名料</th>
                                <td>10,000,000円</td>
                            </tr>
                        </table>
                    </td>
                </tr> -->
                <tr class="price_row">
                    <th>料金体系</th>
                    <td colspan="3">
                        <table>
                        <?php if (!is_null($system['System']['price_pattern'])): ?>
                            <?php foreach (explode(',', $system['System']['price_pattern']) as $key => $val): ?>
                                <?php if (($key % 2) == 0 || $key == 0): ?>
                                    <tr>
                                        <th>
                                            <?php echo $val . '分'; ?>
                                        </th>
                                    <?php else: ?>
                                        <td>
                                            <?php echo $val . '円'; ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                            <!-- <tr>
                                <th>60分</th>
                                <td>10,000円</td>
                            </tr>
                            <tr>
                                <th>90分</th>
                                <td>20,000円</td>
                            </tr>
                            <tr>
                                <th>120分</th>
                                <td>400,000円</td>
                            </tr>
                            <tr>
                                <th>パネル指名料</th>
                                <td>10,000,000円</td>
                            </tr>
                            <tr>
                                <th>60分</th>
                                <td>10,000円</td>
                            </tr>
                            <tr>
                                <th>90分</th>
                                <td>20,000円</td>
                            </tr>
                            <tr>
                                <th>120分</th>
                                <td>400,000円</td>
                            </tr>
                            <tr>
                                <th>パネル指名料</th>
                                <td>10,000,000円</td>
                            </tr> -->
                        </table>
                    </td>
                </tr>
                <tr>
                    <th>アクセス</th>
                    <td colspan="3"><?php echo $parent_shop['System']['access']; ?></td>
                </tr>
                <tr>
                    <th>クレジットカード</th>
                    <td colspan="3">
<?php
if($parent_shop['System']['credit_unavailable'] == 1) {
?>
						ご利用出来ません。
<?php
} else {
	$arrCredit = array(
		'credit_visa' => 'VISA',
		'credit_master' => 'MasterCard',
		'credit_jcb' => 'JCB',
		'credit_americanexpress' => 'AmericanExpress',
		'credit_diners' => 'Diners'
	);
	$cnt = 0;
	foreach($arrCredit AS $key => $cardName) {
		if($parent_shop['System'][$key] == 1) {
			if($cnt > 0) {
				echo ' / ';
			}

			echo $cardName;
			$cnt++;
		}
	}
}
?>
                    </td>
                </tr>

<?php if($parent_shop['UsersAuthority']['is_official_link_display'] == 1) { ?>
                <tr>
                    <th>お店のホームページ</th>                    <td colspan="3">

                        <a href="<?php echo $parent_shop['Shop']['url_pc']; ?>" target="_blank"><?php echo $parent_shop['Shop']['url_pc']; ?></a>
                    </td>
                </tr>
<?php } ?>
<?php
for($ii = 1; $ii <= 3; $ii++) {
	if(!empty($parent_shop['System']['comment' . $ii . '_title']) && !empty($parent_shop['System']['comment' . $ii . '_content'])) {
?>
                <tr>
                    <th><?php echo $parent_shop['System']['comment' . $ii . '_title']; ?></th>
                    <td colspan="3">
                        <?php echo $parent_shop['System']['comment' . $ii . '_content']; ?>
                    </td>
                </tr>
<?php
	}
}
?>
            </table>
        </div>

        <!-- 店長からのお知らせ -->
        <?php echo $this->element('common/shop_owner_comment'); ?>
        <!-- /店長からのお知らせ -->

    </div>
    <!-- /tabInner -->
</section>

<section id="side" class="narrow">
    <?php echo $this->element('common/side_narrow'); ?>
</section>

<?php //echo var_dump($system)?>
