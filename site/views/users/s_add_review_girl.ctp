
<h2 class="midashih2">女の子プロフィール</h2>

<?php echo $this->element('s_girl_profile'); ?>


<?php echo $form->create(null, array('type'=>'post', 'url' => '/s/' . $parent_shop['User']['id'] . '/add_review_girl/' . $girl['Girl']['id']));?>
<?php echo $form->hidden('Review.user_id', array('value' => $parent_shop['User']['id']));?>
<?php echo $form->hidden('Review.girl_id', array('value' => $girl['Girl']['id']));?>

<h2 class="midashih2">
	体験談を投稿する
</h2>

<h3 class="midashih3">
	お店
</h3>

<p><?php echo $parent_shop['Shop']['name']; ?></p>

<h3 class="midashih3">
	投稿者名
</h3>

<p>
	<?php if($parent_reviewer){?>
	<?php echo $form->hidden('Review.reviewer_id', array('value' => $parent_reviewer['Reviewer']['id']));?>
	<?php echo $parent_reviewer['Reviewer']['handle']?>
	<?php }else{?>
	<?php echo $form->text('Review.post_name'); ?>
	<?php echo $form->error('Review.post_name'); ?>
	<span class="spot4">※最大12文字まで入力できます。</span>
	<?php }?>
</p>

<h3 class="midashih3">
	お店採点
</h3>

<p>
	電話対応：
	<?php echo $form->select('Review.score_shop_tel', $m_score_s, (empty($data))?3:$data['Review']['score_shop_tel'], array('data-inline' => 'true', 'empty' => false)); ?><br />
	到着時間：
	<?php echo $form->select('Review.score_shop_time', $m_score_s, (empty($data))?3:$data['Review']['score_shop_time'], array('data-inline' => 'true', 'empty' => false)); ?><br />
	コスパ　：
	<?php echo $form->select('Review.score_shop_cost', $m_score_s, (empty($data))?3:$data['Review']['score_shop_cost'], array('data-inline' => 'true', 'empty' => false)); ?><br />
</p>

<h3 class="midashih3">
	女の子
</h3>

<p>
	<?php echo $girl['Girl']['name']?>
</p>

<h3 class="midashih3">
	見た目年齢
</h3>

<p>
	<?php echo $form->select('Review.reviews_age_id', $m_reviews_ages,null,array('data-inline' => 'true', 'empty'=>'選択してください')); ?>
	<?php echo $form->error('Review.reviews_age_id'); ?><br />
     <span class="spot4">※あくまで見た目です</span>
</p>

<h3 class="midashih3">
	女の子採点
</h3>

<p>
	第一印象：
	<?php echo $form->select('Review.score_girl_first_impression', $m_score_s, (empty($data))?3:$data['Review']['score_girl_first_impression'], array('data-inline' => 'true', 'empty' => false)); ?><br />
	言葉遣い：
	<?php echo $form->select('Review.score_girl_word', $m_score_s, (empty($data))?3:$data['Review']['score_girl_word'], array('data-inline' => 'true', 'empty' => false)); ?><br />
	サービス：
	<?php echo $form->select('Review.score_girl_service', $m_score_s, (empty($data))?3:$data['Review']['score_girl_service'], array('data-inline' => 'true', 'empty' => false)); ?><br />
	感度　　：
	<?php echo $form->select('Review.score_girl_sensitivity', $m_score_s, (empty($data))?3:$data['Review']['score_girl_sensitivity'], array('data-inline' => 'true', 'empty' => false)); ?><br />
	スタイル：
	<?php echo $form->select('Review.score_girl_style', $m_score_s, (empty($data))?3:$data['Review']['score_girl_style'], array('data-inline' => 'true', 'empty' => false)); ?><br />
	あえぎ声：
	<?php echo $form->select('Review.score_girl_voice', $m_score_s, (empty($data))?3:$data['Review']['score_girl_voice'], array('data-inline' => 'true', 'empty' => false)); ?><br />
</p>

<h3 class="midashih3">
	コメント
</h3>

<p>
	<?php echo $form->textarea('Review.comment',array('cols' => 30, 'rows' => 4)); ?>
	<?php echo $form->error('Review.comment'); ?>
</p>

<div style="text-align:center; margin-right:auto; margin-left:auto;">
	<p><?php echo $form->submit('確認する', array('data-role' => 'button', 'data-inline' => 'true','name' => 'confirm', 'div' => false, 'class' => 'btn_post')); ?></p>
</div>

<?php echo $form->end();?>





