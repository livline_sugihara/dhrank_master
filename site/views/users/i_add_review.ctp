
<div style="text-align:left;padding:5px;font-size:medium;background-color:#EFEBE3">
	口コミを投稿する
</div>

<?php echo $form->create(null, array('type'=>'post', 'url' => '/i/' . $parent_shop['User']['id'] . '/add_review' . '?guid=ON','encoding' => null));?>
<?php echo $form->hidden('Review.user_id', array('value' => $parent_shop['User']['id']));?>
<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	お店
</div>
<div style="text-align:left;padding:3px;">
	<?php echo $parent_shop['Shop']['name']; ?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	投稿者名
</div>
<div style="text-align:left;padding:3px;">
	<?php if($parent_reviewer){?>
	<?php echo $form->hidden('Review.reviewer_id', array('value' => $parent_reviewer['Reviewer']['id']));?>
	<?php echo $parent_reviewer['Reviewer']['handle']?>
	<?php }else{?>
	<?php echo $form->text('Review.post_name'); ?>
	<?php echo $form->error('Review.post_name'); ?><br />
	<span style="color:#BE0A26">※最大12文字まで入力できます。</span>
	<?php }?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	お店採点
</div>
<div style="text-align:left;padding:3px;">
	電話対応：<?php echo $form->select('Review.score_shop_tel', $m_score_i, (empty($data))?3:$data['Review']['score_shop_tel'], array('empty' => false)); ?><br />
	到着時間：<?php echo $form->select('Review.score_shop_time', $m_score_i, (empty($data))?3:$data['Review']['score_shop_time'], array('empty' => false)); ?><br />
	コスパ　：<?php echo $form->select('Review.score_shop_cost', $m_score_i, (empty($data))?3:$data['Review']['score_shop_cost'], array('empty' => false)); ?><br />
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	女の子
</div>
<div style="text-align:left;padding:3px;">
	<?php echo $form->select('Review.girl_id', $girls,null,array('empty' => '選択してください。')); ?>
	<?php //start---2013/3/8 障害No.2-0022修正 ?>
	<?php echo $form->error('Review.girl_id'); ?>
	<?php //end---2013/3/8 障害No.2-0022修正 ?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	見た目年齢
</div>
<div style="text-align:left;padding:3px;">
	<?php echo $form->select('Review.reviews_age_id', $m_reviews_ages,null,array('empty'=>'選択してください')); ?>
	<?php echo $form->error('Review.reviews_age_id'); ?><br />
     <span style="color:#BE0A26">※あくまで見た目です</span>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	女の子採点
</div>
<div style="text-align:left;padding:3px;">
	第一印象：<?php echo $form->select('Review.score_girl_first_impression', $m_score_i, (empty($data))?3:$data['Review']['score_girl_first_impression'], array('empty' => false)); ?><br />
	言葉遣い：<?php echo $form->select('Review.score_girl_word', $m_score_i, (empty($data))?3:$data['Review']['score_girl_word'], array('empty' => false)); ?><br />
	サービス：<?php echo $form->select('Review.score_girl_service', $m_score_i, (empty($data))?3:$data['Review']['score_girl_service'], array('empty' => false)); ?><br />
	感度　　：<?php echo $form->select('Review.score_girl_sensitivity', $m_score_i, (empty($data))?3:$data['Review']['score_girl_sensitivity'], array('empty' => false)); ?><br />
	スタイル：<?php echo $form->select('Review.score_girl_style', $m_score_i, (empty($data))?3:$data['Review']['score_girl_style'], array('empty' => false)); ?><br />
	あえぎ声：<?php echo $form->select('Review.score_girl_voice', $m_score_i, (empty($data))?3:$data['Review']['score_girl_voice'], array('empty' => false)); ?><br />
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	コメント
</div>
<div style="text-align:left;padding:3px;">
	<?php echo $form->textarea('Review.comment',array('cols' => 30, 'rows' => 4)); ?>
	<?php echo $form->error('Review.comment'); ?>
</div>

<div style="text-align:center;padding:3px;">
	<?php echo $form->submit('確認する', array('name' => 'confirm', 'div' => false)); ?>
</div>
<?php echo $form->end();?>


<hr color="#DED7C8" />