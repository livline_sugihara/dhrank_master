<section id="middle" class="wide">

    <!-- 店舗トップ共通部分 -->
    <?php echo $this->element('common/shop_top'); ?>
    <!-- /店舗トップ共通部分 -->

    <!-- tabInner -->
    <div class="tabInner">

        <div class="detailArea clearfix">
            <div>
                <?php echo $imgCommon->get_girl_with_time($girl, array(240, 320), 1, 'main_image'); ?>
                <div class="imgList clearfix">
                    <?php echo $imgCommon->get_girl_with_time($girl, array(75, 100), 1, 'sub_image1', 'sub_image'); ?>
                    <?php echo $imgCommon->get_girl_with_time($girl, array(75, 100), 2, 'sub_image2', 'sub_image'); ?>
                    <?php echo $imgCommon->get_girl_with_time($girl, array(75, 100), 3, 'sub_image3', 'sub_image'); ?>
                </div>

<?php if(isset($review_flag) && $review_flag) { ?>
                <p class="whiteBtn btn bookmarkGirl" data="<?php echo $girl['Girl']['id']; ?>" mode="delete"><a href="#">お気に入りに解除</a></p>
<?php } else { ?>
                <p class="whiteBtn btn bookmarkGirl" data="<?php echo $girl['Girl']['id']; ?>" mode="add"><a href="#">お気に入りに保存</a></p>
<?php } ?>

            </div>
            <div>
                <dl class="comment">
                    <dt>店長からのコメント</dt>
                    <dd><?php echo nl2br($girl['Girl']['owner_comment']); ?></dd>
                </dl>

                <p class="redBtn entryreviewBtn"><a href="<?php echo $linkCommon->get_post_review($girl['Shop']['id'], $girl['Girl']['id']); ?>">口コミを投稿する</a></p>

                <dl>
                    <dt>プロフィール</dt>
                    <dd>
                        <table cellspacing="0" cellpadding="0" border="0" class="list">
                            <tr>
                                <th>名前</th>
                                <td><?php echo $girl['Girl']['name']; ?></td>
                            </tr>
                            <tr>
                                <th>年齢</th>
                                <td><?php echo $girl['Girl']['age']; ?>歳</td>
                            </tr>
                        </table>
                    </dd>
                </dl>
                <dl>
                    <dt>スタイル</dt>
                    <dd>
                        <table cellspacing="0" cellpadding="0" border="0" class="list">
                            <tr>
                                <th>身長</th>
                                <td><?php echo $girl['Girl']['body_height']; ?>cm</td>
                            </tr>
                            <tr>
                                <th>3サイズ</th>
                                <td>B<?php echo $girl['Girl']['bust']; ?>(<?php echo $girl['MGirlsCup']['name2']; ?>) - W<?php echo $girl['Girl']['waist']; ?> - H<?php echo $girl['Girl']['hip']; ?></td>
                            </tr>
                        </table>
                    </dd>
                </dl>
                <div class="supplement">
                    <p class="tt">女の子からのコメント</p>
                    <p><?php echo nl2br($girl['Girl']['self_comment']); ?></p>
                </div>
            </div>

        </div>

        <!-- 新着レビュー -->
        <?php echo $this->element('common/girl_review', array('new_shop_review' => $reviews)); ?>
        <!-- /新着レビュー -->

        <h2 class="heading">他の女の子の口コミ</h2>
        <div id="girlsSliderWrapper">
            <div id="girlsSlider">
<?php
$cnt = count($other_girls);
foreach($other_girls AS $key => $record) {
    if($key % 6 == 0) {
?>
                <div>
<?php
    }
?>
                    <a href="<?php echo $linkCommon->get_user_girl($record); ?>">
                        <?php echo $imgCommon->get_girl_with_time($record, array(90, 120), 1); ?>
                    </a>
<?php
    if($key % 6 == 5 || $key == $cnt - 1) {
?>
                </div>
<?php
    }
}
?>
            </div>
        </div>
    </div>
    <!-- /tabInner -->

</section>

<section id="side" class="narrow">
    <?php echo $this->element('common/side_narrow'); ?>
</section>

<script type="text/javascript">
<!--
$(document).ready(function() {
    function chageImg($this){

    }

    $(".sub_image").hover(function() {        var main = $('#main_image');        var sub = $(this);        main.animate(            {'opacity': 0},            {duration:"fast",            easing:"linear",            queue: false,            complete: function(){                    main.attr('src', sub.attr('src'));                    main.animate({                        'opacity': 1                    });                }            }        );        //$('#main_image').attr('src', $(this).attr('src'));
    });
});
-->
</script>