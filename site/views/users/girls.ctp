<section id="middle" class="wide">

    <!-- 店舗トップ共通部分 -->
    <?php echo $this->element('common/shop_top'); ?>
    <!-- /店舗トップ共通部分 -->

    <!-- tabInner -->
    <div class="tabInner">
        <!-- <div class="title title05">
            <h2>RECOMMEND GIRLS<span class="small">当店激押し売れっ子</span></h2>
        </div>

        <?php //if (empty($recommend_girls)): ?>
            <span>このエリアで登録されている激押し売れっ子はいません。</span>
        <?php //else: ?>

            <article class="recommend_shop">
                <h3><?php //echo $recommend_girls[0]['Shop']['name']; ?></h3>
                <ul>
            <?php //foreach ($recommend_girls as $girl_record): ?>
                    <li>
                        <a href="<?php //echo $linkCommon->get_user_girl($girl_record); ?>">
                            <?php //echo $imgCommon->get_girl_with_time($girl_record, array(91, 121), 1, null, 'img_frame'); ?>
                        </a>
                        <p class="name"><?php //echo $girl_record['Girl']['name']; ?></p>
                        <p class="copy"><?php //echo mb_strimwidth($girl_record['Girl']['owner_comment'], 0, 15, "..."); ?></p>
                        <p class="data"><?php //echo $girl_record['Girl']['age']; ?>歳・<?php //echo $girl_record['Girl']['body_height']; ?>cm</p>
                    </li>
            <?php //endforeach; ?>
                </ul>

            </article>
        <?php //endif; ?> -->


<?php
if(count($girls) > 0) {
?>
        <div class="pageCounter">
        	<span class="num"><?php echo $count_girls; ?>人</span><span>の在籍者がいます。</span>
        <div>
        <p class="clearLine"></p>
        <div class="girlsArea clearfix">

<?php
	foreach($girls AS $key => $record) {
?>
            <div>
                <div class="img">
                	<a href="<?php echo $linkCommon->get_user_girl($record); ?>">
                		<?php echo $imgCommon->get_girl_with_time($record, array(75, 75)); ?>
                	</a>
                    <p class="icon_review">                        <?php if ($record[0]['count'] > 0): ?>                                <?php echo $this->Html->image('icon_review.png'); ?>                        <?php endif; ?>                    </p>                </div>
                <p class="name"><a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo $record['Girl']['name']; ?></a></p>
                <p class="review">
<?php if($record[0]['count'] > 0) { ?>
                	<img src="/img/icon_review.jpg">
<?php } ?>
                	クチコミ <?php echo $record[0]['count']; ?>件
                </p>
               <!-- <p class="whiteBtn btn"><a href="#">お気に入りに保存</a></p>	-->

<?php if(isset($review_flag) && $review_flag) { ?>
        <p class="whiteBtn btn bookmarkGirl" data="<?php echo $record['Girl']['id']; ?>" mode="delete"><a href="#">お気に入りに解除</a></p>
<?php } else { ?>
        <p class="whiteBtn btn bookmarkGirl" data="<?php echo $record['Girl']['id']; ?>" mode="add"><a href="#">お気に入りに保存</a></p>
<?php } ?>

            </div>
<?php
	}
?>
            <p class="clearLine"></p>
        </div>
<?php
} else {
?>
        <div class="pageCounter">
        	在籍者が登録されていません
        <div>
        <p class="clearLine"></p>
<?php
}
?>

    </div>
    <!-- /tabInner -->

</section>

<section id="side" class="narrow">
    <?php echo $this->element('common/side_narrow'); ?>
</section>

<?php // echo $this->element('pagenate/numbers'); ?>
