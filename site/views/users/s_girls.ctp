	<?php echo $this->element('s_common/shop_top'); ?>

	<section id="shop_form">
		<div class="label2_2 tab_top_grade"><h3 class="label2_ttl"><?php echo $parent_shop['Shop']['name']; ?>在籍</h3></div>
		<div id="search_box" style="text-align:center;">
			<input type="text" size="20" id="Search1" placeholder="絞り込み検索" style="width:93%;margin:0 auto;">
		</div>

		<div class="girls_list" id="MyBookmark1">
			<div class="clearfix inner">
<?php foreach($girls as $key => $record){ ?>
				<div class="col">
					<p class="img"><?php echo $imgCommon->get_girl($record, 's')?></p>
					<p class="tt"><a href="<?php echo $linkCommon->get_user_girl($record)?>"><?php echo $record['Girl']['name'] ?></a></p>
					<p class="caption"><?php echo $record['Girl']['age']; ?>歳-<?php echo $record['Girl']['body_height']; ?>-<?php echo $textCommon->get_3size($record)?></p>
					<p class="kuchikomi">
<?php if($record[0]['count'] > 0) { ?>
	<img src="/img/icon_review.jpg" style="width:6px">
<?php } ?>
口コミ:<?php echo $record[0]['count']; ?>件</p>
				</div>
<?php } ?>
			</div>
		</div>
	</section><!--/#shop_form-->

	<!-- ライン・メール・電話 -->
	<?php echo $this->element('s_common/line_mail'); ?>
	<!-- /ライン・メール・電話 -->

	<!-- 電話をする -->
	<?php echo $this->element('s_common/shop_tel'); ?>
	<!-- /電話をする -->

	<!-- メニュー -->
	<?php echo $this->element('s_common/shop_menu'); ?>
	<!-- /メニュー -->

<?php echo $this->element('pagenate/s_numbers'); ?>

<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../<?php echo $parent_shop['User']['id']; ?>" itemprop="url"><span itemprop="title"><?php echo $parent_shop['Shop']['name']; ?></span></a></li>
		
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./girls" itemprop="url"><span itemprop="title">在籍女性</a></li>
	</ul>
</nav>
