
<div style="text-align:left;padding:5px;font-size:medium;background-color:#EFEBE3">
	女の子レビュー
</div>

<div style="text-align:left;padding:3px;">
	<?php if(!empty($review['Review']['reviewer_id'])){?>
	<a href="<?php echo $linkCommon->get_reviews_post($review)?>"><?php echo $review['Reviewer']['handle']?></a><?php if(!empty($review['Reviewer']['age'])) echo '(' . $review['Reviewer']['age'] . ')';?>さんの口コミ<br />
	<?php }else{?>
	<?php echo $review['Review']['post_name'] ?>さんの口コミ<br />
	<?php }?>
	<div align="right"><?php echo date('Y年n月', strtotime($review['Review']['created'])) ?></div>
</div>


<div style="text-align:left;padding:3px;background-color:#FDF4E7">
	<span style="font-size:medium;"><?php echo $review['Girl']['name'] ?></span><?php echo $review['Girl']['age'] ?>歳<br />
    見た目年齢:<?php echo $review['MReviewsAge']['name'] ?> <br />
	<span style="font-size:medium;">総合評価</span><span style="color:#BE0A26;font-size:medium;"><?php echo round($review[0]['girl_avg'],2)?></span><br />
    第一印象：<span style="color:#BE0A26"><?php echo round($review['Review']['score_girl_first_impression'],1)?></span>|言葉遣い：<span style="color:#BE0A26"><?php echo round($review['Review']['score_girl_word'],1)?></span>|サービス：<span style="color:#BE0A26"><?php echo round($review['Review']['score_girl_service'],1)?></span>|感度：<span style="color:#BE0A26"><?php echo round($review['Review']['score_girl_sensitivity'],1)?></span>|スタイル：<span style="color:#BE0A26"><?php echo round($review['Review']['score_girl_style'],1)?></span>  | あえぎ声：<span style="color:#BE0A26"><?php echo round($review['Review']['score_girl_voice'],1)?></span>
</div>

<div style="text-align:left;padding:3px;">
<?php echo nl2br($review['Review']['comment']);?>
</div>

<div style="text-align:right;padding:3px;">
	この口コミは参考になりましたか？：
	<span style="color:#BE0A26;font-size:medium;">
	<?php //start---2013/4/30 障害No.4-0002修正 ?>
	<?php //echo $review[0]['reviews_good_count']?>
	<?php echo $reviews_good_count[0]['reviews_good_count_sum']?>
	<?php //end---2013/4/30 障害No.4-0002修正 ?>
	</span>票<br />
    [<a href="<?php echo $linkCommon->get_user_add_good_count_end($review)?>">投票する</a>]
</div>

<hr color="#DED7C8" />
