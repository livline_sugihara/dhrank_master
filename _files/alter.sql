ALTER TABLE `admins` ADD `is_auth_catebanner_toggle` int(11) NOT NULL COMMENT 'バナーon,off切り替え権限';

DROP TABLE IF EXISTS `counts_tmps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `counts_tmps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(255) DEFAULT NULL,
  `area_id` varchar(50) DEFAULT '_0',
  `shops_count` int(11) DEFAULT '0',
  `category_1` int(11) DEFAULT '0',
  `category_2` int(11) DEFAULT '0',
  `category_3` int(11) DEFAULT '0',
  `category_4` int(11) DEFAULT '0',
  `category_5` int(11) DEFAULT '0',
  `category_6` int(11) DEFAULT '0',
  `category_7` int(11) DEFAULT '0',
  `category_1000` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

ALTER TABLE `girls` ADD `catch_phrase` varchar(255) DEFAULT NULL COMMENT 'キャッチコピー';
ALTER TABLE `girls` ADD `access_count` int(11) NOT NULL DEFAULT '0' COMMENT 'アクセスカウント';

DROP TABLE IF EXISTS `m_reviewers_positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_reviewers_positions` (
  `id` int(11) NOT NULL DEFAULT '0' COMMENT 'ID',
  `name` varchar(255) NOT NULL DEFAULT '0' COMMENT '役職名',
  `reviews_count` int(11) NOT NULL DEFAULT '0' COMMENT '口コミ最低必要数',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `rankings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rankings` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `ranking_id` int(11) NOT NULL COMMENT '対象者のID',
  `ranking_name` varchar(255) NOT NULL COMMENT '対象者の名前',
  `ranking_comment` varchar(1024) DEFAULT NULL COMMENT '対象者のコメント',
  `count` varchar(11) DEFAULT NULL COMMENT 'カウント数',
  `standing` int(11) NOT NULL COMMENT '順位',
  `before_standing` int(11) NOT NULL COMMENT '前回の順位',
  `avatar_id` int(11) DEFAULT NULL COMMENT 'アバターID',
  `large_areas_id` int(11) DEFAULT NULL COMMENT 'エリアID',
  `category` int(11) NOT NULL COMMENT 'ランキングのカテゴリ',
  `created` datetime NOT NULL COMMENT '登録日時',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `review_replies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `review_id` int(11) NOT NULL COMMENT 'レビューID',
  `reply_flg` tinyint(4) NOT NULL COMMENT '返信者の種類(1,スタッフ、2,女の子)',
  `comment` text COMMENT '返事',
  `delete_flg` tinyint(4) DEFAULT '0' COMMENT '削除フラグ',
  `created` datetime NOT NULL COMMENT '登録日時',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `reviewer_counts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviewer_counts` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `reviewer_id` int(11) NOT NULL DEFAULT '0' COMMENT 'レビュワーID',
  `reviews_count` int(11) NOT NULL DEFAULT '0' COMMENT '口コミ数',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


ALTER TABLE `reviewers` ADD `position_id` int(11) NOT NULL DEFAULT '0' COMMENT '役職ID';
ALTER TABLE `reviewers` ADD `access_count` int(11) NOT NULL DEFAULT '0' COMMENT 'アクセスカウント';

ALTER TABLE `reviews` ADD `view_count` int(11) NOT NULL DEFAULT '0' COMMENT '閲覧回数';
ALTER TABLE `reviews` ADD `not_publish` tinyint(4) NOT NULL DEFAULT '0' COMMENT '非掲載フラグ';

DROP TABLE IF EXISTS `shop_boards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_boards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) NOT NULL COMMENT 'userID',
  `post_no` int(11) NOT NULL COMMENT '投稿番号',
  `res_post_no` int(11) DEFAULT NULL COMMENT 'レスを行った番号',
  `name` varchar(255) NOT NULL COMMENT '投稿者名',
  `comment` text NOT NULL COMMENT '投稿内容',
  `media` varchar(255) DEFAULT NULL COMMENT '投稿したデバイス',
  `ip_address` varchar(255) DEFAULT NULL COMMENT 'IPアドレス',
  `created` datetime NOT NULL COMMENT '登録日時',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `shop_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `body` text,
  `trigger` tinyint(4) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


ALTER TABLE `shops` ADD `about_info` text COMMENT 'お店のお知らせ';

DROP TABLE IF EXISTS `small_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primarykey',
  `large_area_id` int(11) NOT NULL COMMENT 'ラージエリアID',
  `name` varchar(255) DEFAULT NULL COMMENT 'エリア名',
  `large_area_name` varchar(255) DEFAULT NULL COMMENT 'ラージエリア名(アソシエーション代用)',
  `url` varchar(255) DEFAULT NULL COMMENT 'URL',
  `display_grade` int(11) NOT NULL DEFAULT '0' COMMENT '表示グレード',
  `area_banner_name` varchar(100) DEFAULT NULL COMMENT '地域バナー(PC)',
  `area_banner_sp_name` varchar(100) DEFAULT NULL COMMENT '地域バナー(スマホ)',
  `area_banner_url` varchar(255) DEFAULT NULL COMMENT '地域バナーURL(PC)',
  `area_banner_sp_url` varchar(255) DEFAULT NULL COMMENT '地域バナーURL(スマホ)',
  `created` datetime DEFAULT NULL COMMENT '登録日時',
  `modified` datetime DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `url` (`url`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


ALTER TABLE `systems` ADD `price_pattern` varchar(255) DEFAULT NULL;


DROP TABLE IF EXISTS `test_boards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_boards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `comment` text COLLATE utf8_bin,
  `media` varchar(255) COLLATE utf8_bin NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


ALTER TABLE `users` ADD `small_area_id` varchar(255) NOT NULL DEFAULT '0' COMMENT 'メイン活動小エリアID(カンマ区切り)';

ALTER TABLE `users_authorities` ADD `is_show_category_banner` tinyint(4) NOT NULL DEFAULT '0' COMMENT '権限-カテゴリページバナーのON/OFF';
ALTER TABLE `users_authorities` ADD `is_board_shop` tinyint(4) NOT NULL DEFAULT '0' COMMENT '権限-掲示板の有無（０表示,１非表示)';
