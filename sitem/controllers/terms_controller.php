<?php

class TermsController extends AppController {
	var $name = 'Terms';

	function index() {
		//head
		$this->set('title_for_layout', $this->title_tag_common . ' ご利用規約');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',ご利用規約');
		$this->set('meta_description', $this->meta_description_common . 'ご利用規約');
		$this->set('header_one', $this->h1_tag_common . 'ご利用規約');

	}
	function i_index() {
		$this->index();
	}
	function s_index() {
		$this->index();
	}

}
