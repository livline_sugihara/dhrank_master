<?php
class TestController extends AppController{
	var $name = 'Test';
	var $uses = array('Girl','Shop','Review','Test');

	// 女の子検索を出す
	function index(){
		$params = $this->params['form']; // POSTデータ

		// POSTデータを条件に組み込む
		$conditions_arr = array();

		// 名前
		if(isset($params['girl_name']) && $params['girl_name'] != '')
			$conditions_arr[]['OR']['Girl.name LIKE'] = '%' . $params['girl_name'] . '%';

		// 年齢
		if(isset($params['arr_age'])){
			$age_arr = array();
			foreach($params['arr_age'] as $key => $val){
				switch($val){
					case '1' :
						$age_arr[] = array('Girl.age >=' => 18, 'Girl.age <=' => 19);
						break;
					case '2' :
						$age_arr[] = array('Girl.age >=' => 20, 'Girl.age <=' => 24);
						break;
					case '3' :
						$age_arr[] = array('Girl.age >=' => 25, 'Girl.age <=' => 29);
						break;
					case '4' :
						$age_arr[] = array('Girl.age >=' => 30, 'Girl.age <=' => 34);
						break;
					case '5' :
						$age_arr[] = array('Girl.age >=' => 35);
						break;
				}
			}
			$conditions_arr[]['OR'] = $age_arr;
		}

		// 身長
		if(isset($params['arr_tall'])){
			$height_arr = array();
			foreach($params['arr_tall'] as $key => $val){
				switch($val){
					case '1' :
						$height_arr[] = array('Girl.body_height <=' => 149);
						break;
					case '2' :
						$height_arr[] = array('Girl.body_height >=' => 150, 'Girl.body_height <=' => 154);
						break;
					case '3' :
						$height_arr[] = array('Girl.body_height >=' => 155, 'Girl.body_height <=' => 159);
						break;
					case '4' :
						$height_arr[] = array('Girl.body_height >=' => 160, 'Girl.body_height <=' => 164);
						break;
					case '5' :
						$height_arr[] = array('Girl.body_height >=' => 165, 'Girl.body_height <=' => 169);
						break;
					case '6' :
						$height_arr[] = array('Girl.body_height >=' => 170);
						break;
				}
			}
			$conditions_arr[]['OR'] = $height_arr;
		}

		// カップ数
		$cup_arr = array();
		if(isset($params['arr_cup'])){
			foreach($params['arr_cup'] as $key => $val){
				$cup_arr[] = $val;
			}
			$conditions_arr[]['OR']['MGirlsCup.id'] = $cup_arr;
		}

		// バスト
		if(isset($params['arr_bust'])){
			$bust_arr = array();
			foreach($params['arr_bust'] as $key => $val){
				switch($val){
					case '1' :
						$bust_arr[] = array('Girl.bust <=' => 79);
						break;
					case '2' :
						$bust_arr[] = array('Girl.bust >=' => 80, 'Girl.bust <=' => 84);
						break;
					case '3' :
						$bust_arr[] = array('Girl.bust >=' => 85, 'Girl.bust <=' => 89);
						break;
					case '4' :
						$bust_arr[] = array('Girl.bust >=' => 90, 'Girl.bust <=' => 94);
						break;
					case '5' :
						$bust_arr[] = array('Girl.bust >' => 95, 'Girl.bust <=' => 99);
						break;
					case '6' :
						$bust_arr[] = array('Girl.bust >=' => 100);
						break;
				}
			}
			$conditions_arr[]['OR'] = $bust_arr;
		}

		// ウエスト

		if(isset($params['arr_waist'])){
			$waist_arr = array();
			foreach($params['arr_waist'] as $key => $val){
				switch($val){
					case '1' :
						$waist_arr[] = array('Girl.waist <=' => 54);
						break;
					case '2' :
						$waist_arr[] = array('Girl.waist >=' => 55, 'Girl.waist <=' => 59);
						break;
					case '3' :
						$waist_arr[] = array('Girl.waist >=' => 60, 'Girl.waist <=' => 64);
						break;
					case '4' :
						$waist_arr[] = array('Girl.waist >=' => 65, 'Girl.waist <=' => 69);
						break;
					case '5' :
						$waist_arr[] = array('Girl.waist >=' => 70);
						break;
				}
			}
			$conditions_arr[]['OR'] = $waist_arr;
		}

		// ヒップ
		if(isset($params['arr_hip'])){
			$hip_arr = array();
			foreach($params['arr_hip'] as $key => $val){
				switch($val){
					case '1' :
						$hip_arr[] = array('Girl.hip <=' => 79);
						break;
					case '2' :
						$hip_arr[] = array('Girl.hip >=' => 80, 'Girl.hip <=' => 84);
						break;
					case '3' :
						$hip_arr[] = array('Girl.hip >=' => 85, 'Girl.hip <=' => 89);
						break;
					case '4' :
						$hip_arr[] = array('Girl.hip >=' => 90, 'Girl.hip <=' => 94);
						break;
					case '5' :
						$hip_arr[] = array('Girl.hip >=' => 95, 'Girl.hip <=' => 99);
						break;
					case '6' :
						$hip_arr[] = array('Girl.hip >=' => 100);
						break;

				}
			}
			$conditions_arr[]['OR'] = $hip_arr;
		}

		// カテゴリ
		if(isset($params['arr_cate']))
		{
			$cate_arr = array();
			foreach($params['arr_cate'] as $key => $val){
				$cate_arr[] = $val;
			}
			$conditions_arr[]['OR']['Shop.shops_business_category_id'] = $cate_arr;
		}

		// テーブルからデータ取得
		$girls_data = $this->Girl->find('all', array(
				'fields' => array('*'), // IDとお店の名前、名前
				'conditions' => $conditions_arr,
				'limit' => 30,
			)
		);

		// 取得結果を表示
		$this->set('girls_data',$girls_data);
//		var_dump($girls_data);

	}

	// 電話帳検索
	function tel(){
		$shops_data = $this->Shop->find('all', array(
				'fields' => array('Shop.id',
								  'Shop.name',
								  'Shop.phone_number',
								  'Shop.url_pc',
								  'Shop.url_mobile',
								  'Shop.url_smartphone',
				), // ID,お店の番号、ＵＲＬ
//				'conditions' => $conditions_arr,
				'limit' => 30,
			)
		);

		$this->set('shops_data',$shops_data);
//	var_dump($shops_data);

	}

	// カテゴリ検索
	function category(){
		$params = $this->params['form']; // POSTデータ
		$category_arr = array();
		if(isset($params['category'])){
			foreach($params['category'] as $key => $val){
				$category_arr[] = $val;
			}
			$category_arr[]['OR']['MShopsBusinessCategory.id'] = $category_arr;
		}

		$shops_data = $this->Shop->find('all', array(
			'fields' => array(
				'*'
			),
			'conditions' => $category_arr,
			'limit' => 50,
			)
		);
		$this->set('shops_data', $shops_data);

	}

	// 料金
	function price(){
		$price_arr = array();
		$params = $this->params['form']; // POSTデータ
		if (isset($params['price_from']) && $params['price_from'] > 0) $price_arr['System.price_cost >= '] = $params['price_from'];
		if (isset($params['price_to']) && $params['price_to'] > 0) $price_arr['System.price_cost <= '] = $params['price_to'];
		$price_data = $this->Shop->find('all', array(
			'fields' => array(
				'*'
			),
			'conditions' => $price_arr,
			'order' => array('System.price_cost'),
			)
		);
		$this->set('price_data', $price_data);

	}

	// エリア検索
	function area($large_area_id = 1)
	{
		$this->Shop->recursive = '-1'; // アソシエーション解除
		$shop_datas = $this->Shop->find('all', array(
			'fields' => array(
				'Shop.name',
				'Shop.user_id',
			),
			'joins' => array(
							array(
								'table' => 'users',
				        		'alias' => 'User',
				        		'type' => 'INNER',
				        		'conditions' => array(
				        			'Shop.user_id = User.id',
								)
							),
						),
			'conditions' => array('User.large_area_id' => $large_area_id),
			)
		);
		$this->set('shop_datas', $shop_datas);
	}


	// Ajax用
	function update(){
		$this->index();
		$this->layout = "";

	}
}
