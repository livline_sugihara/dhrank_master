<?php
class AccountsController extends AppController {
	var $name = 'Accounts';
	var $uses = array('ReviewerRegister','ReviewerChangepassword','ReviewerPassport','SendLoginid','ReviewerChangeemail','Reviewer');
	public $expires = 2592000; //60 * 60 * 24 * 30

	function beforeFilter(){
		parent::beforeFilter();
		$this->layout = 'login';

		switch($this->params['action'])
		{
			case 'login':
				$this->addBreadCrumbs("レビュアー会員様ログイン" , "/accounts/login");
				break;
			case 's_login':
				$this->addBreadCrumbs("レビュアー会員様ログイン" , "/s/accounts/login");
				break;
			case 'register':
			case 'register_end':
				$this->addBreadCrumbs("レビュアー会員様新規登録" , "/accounts/register");
				break;
			case 's_register':
			case 's_register_end':
				$this->addBreadCrumbs("レビュアー会員様新規登録" , "/s/accounts/register");
				break;
			case 'register_input':
				$this->addBreadCrumbs("レビュアー会員様新規登録" , "/accounts/register_input/" . join('/', $this->params['pass']));
				break;
			case 's_register_input':
				$this->addBreadCrumbs("レビュアー会員様新規登録" , "/s/accounts/register_input/" . join('/', $this->params['pass']));
				break;
			case 'register_input_end':
				$this->addBreadCrumbs("レビュアー会員様新規登録" , "#");
				break;
			case 's_register_input_end':
				$this->addBreadCrumbs("レビュアー会員様新規登録" , "#");
				break;
			case 'changepassword_send':
			case 'changepassword_send_end':
				$this->addBreadCrumbs("パスワード変更手続" , "/accounts/changepassword_send");
				break;
			case 's_changepassword_send':
			case 's_changepassword_send_end':
				$this->addBreadCrumbs("パスワード変更手続" , "/accounts/changepassword_send");
				break;
			case 'changepassword_input':
				$this->addBreadCrumbs("パスワード変更手続" , "/accounts/changepassword_input/" . join('/', $this->params['pass']));
				break;
			case 's_changepassword_input':
				$this->addBreadCrumbs("パスワード変更手続" , "/accounts/changepassword_input/" . join('/', $this->params['pass']));
				break;
			case 'changepassword_input_end':
				$this->addBreadCrumbs("パスワード変更手続" , "#");
				break;
			case 's_changepassword_input_end':
				$this->addBreadCrumbs("パスワード変更手続" , "#");
				break;
			case 'error':
				$this->addBreadCrumbs("エラー" , "#");
				break;
		}
	}

	function index(){
	}
	function i_index() {
		$this->index();
	}
	function s_index() {
		$this->index();
	}

	//レビュアー登録用URL送信
	function register(){
		//head

		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー会員・新規登録');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',レビュアー,新規登録');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー会員・新規登録');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー会員・新規登録');


		if(!empty($this->data)){
			$this->ReviewerRegister->set($this->data);

			//同一メールアドレス登録済みの場合はエラー
			if(0 < $this->Reviewer->find('count', array('conditions' => array('Reviewer.email' => $this->data['ReviewerRegister']['email'],)))){
				$this->ReviewerRegister->invalidate('email', 'このメールアドレスは既に登録済みです。');
			}

			if($this->ReviewerRegister->validates()) {
				//uid生成
				$uid = '';
				$sCharList = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
				mt_srand();
				for($i = 0; $i < 16; $i++){
					$uid .= $sCharList{mt_rand(0, strlen($sCharList) - 1)};
				}

				//これまでのレビュアー作成データを無効にする
				$reviewer_list = $this->ReviewerRegister->find('all',  array('conditions' => array(
						'ReviewerRegister.email' => $this->data['ReviewerRegister']['email'],
//						'ReviewerRegister.large_area_id' => $this->parent_area['LargeArea']['id'],
						'ReviewerRegister.is_created' => 0)));
				foreach($reviewer_list as $key => $record){
					$reviewer_list[$key]['ReviewerRegister']['is_created'] = 1;
				}
				//空データが登録されるため空チェックを行う
				if(!empty($reviewer_list)){
					$this->ReviewerRegister->saveAll($reviewer_list);
				}

				//レビュアーデータ保存
				$this->ReviewerRegister->create();
				$this->ReviewerRegister->email = $this->data['ReviewerRegister']['email'];
				$this->ReviewerRegister->large_area_id = $this->parent_area['LargeArea']['id'];
				$this->ReviewerRegister->uid = $uid;
				$this->ReviewerRegister->is_created = 0;
				$this->ReviewerRegister->save($this->ReviewerRegister);

				//保存データ取得
				$id = $this->ReviewerRegister->id;
				$this->ReviewerRegister->create();
				$pass_data = $this->ReviewerRegister->findById($id);

				//メール送信
				$register_input_url = '';
				$terminate_url = 'accounts/register_input/';
				if(isset($this->params['prefix'])){
					if($this->params['prefix'] == 'i'){
						$terminate_url = 'i/accounts/register_input/';
					} else if($this->params['prefix'] == 's') {
						$terminate_url = 's/accounts/register_input/';
					}
				}
				$register_input_url = $this->parent_area['LargeArea']['url'] . $terminate_url . $pass_data['ReviewerRegister']['id'] . '/' .
						date('YmdHis', strtotime($pass_data['ReviewerRegister']['created'])) . '/' . $uid;

				$mailOptions = array(
					'to' => $this->data['ReviewerRegister']['email'],
					'from' => VALID_REGISTER_URLSEND_EMAIL,
					'subject' => 'レビュアー登録',
					'smtpOptionsKey' => 'smtpOptionRegisterUrlsend',
					'params' => array(
						'register_input_url' => $register_input_url,
					),
					'template' => 'accounts_register_reviewer',
				);
				$this->send_mail($mailOptions);

				$this->redirect($this->device_path . '/accounts/register_end');
			}
		}
	}
	function i_register() {
		$this->register();
	}
	function s_register() {
		$this->register();
	}

	//レビュアー登録用URL送信完了
	function register_end(){
		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー登録URL送信完了');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',レビュアー,登録,URL,送信');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー登録URL送信完了');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー登録URL送信完了');


	}
	function i_register_end() {
		$this->register_end();
	}
	function s_register_end() {
		$this->register_end();
	}

	//レビュアー登録画面
	function register_input($param_id = null, $param_time = null, $param_uid = null) {
		//head

		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー登録画面');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',レビュアー,登録');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー登録画面');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー登録画面');

		if (!empty($this->data)){

			//同一ログインID登録済みの場合はエラー
			if(0 < $this->Reviewer->find('count', array('conditions' => array('Reviewer.username' => $this->data['Reviewer']['username'],'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id'],)))){
				$this->Reviewer->invalidate('username', 'このログインIDは使用出来ません。');
			}
			//同一ニックネーム登録済みの場合はエラー
			if(0 < $this->Reviewer->find('count', array('conditions' => array('Reviewer.handle' => $this->data['Reviewer']['handle'],'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id'],)))){
				$this->Reviewer->invalidate('handle', 'このニックネームは使用出来ません。');
			}

			// 追加（この時だけ、ID重複チェックを行う）
			$this->Reviewer->create();
			$this->Reviewer->set($this->data);

			if($this->Reviewer->validates()){
				//メールアドレス取得
				$register_data = $this->ReviewerRegister->find('first', array('conditions' => array('ReviewerRegister.id' => $this->data['ReviewerRegister']['id'],)));
				//データ保存
				$ins = array('Reviewer' => array(
					'email' => $register_data['ReviewerRegister']['email'],
					'large_area_id' => $this->parent_area['LargeArea']['id'],
					'password' => $this->Auth->password($this->data['Reviewer']['password_confirm']),
				));
				$this->Reviewer->set($ins);
				$this->Reviewer->save();

				//本URLを無効に設定
				$pass_data = $this->ReviewerRegister->find('first', array('conditions' => array('ReviewerRegister.id' => $this->data['ReviewerRegister']['id'])));
				$pass_data['ReviewerRegister']['is_created'] = 1;
				$this->ReviewerRegister->save($pass_data);

				//メール送信（お客様控え）
				$mailOptions = array(
					'to' => $register_data['ReviewerRegister']['email'],
					'from' => VALID_REGISTER_END_EMAIL,
					'subject' => '【' . $this->parent_area['LargeArea']['name'] . 'デリヘル口コミランキング】レビュアー登録を行いました。',
					'smtpOptionsKey' => 'smtpOptionRegisterEnd',
					'params' => array(
						'area_name' => $this->parent_area['LargeArea']['name'],
						'email' => $register_data['ReviewerRegister']['email'],
						'username' => $this->data['Reviewer']['username'],
						'handle' => $this->data['Reviewer']['handle'],
						'password' => $this->data['Reviewer']['password_confirm'],
					),
					'template' => 'accounts_register_input_reviewer',
				);
				$this->send_mail($mailOptions);

				//メール送信（管理者へ）
				$mailOptions = array(
					'to' => VALID_REGISTER_END_EMAIL,
					'from' => VALID_REGISTER_END_EMAIL,
					'subject' => '【' . $this->parent_area['LargeArea']['name'] . 'デリヘル口コミランキング】レビュアー登録を行いました。',
					'smtpOptionsKey' => 'smtpOptionRegisterEnd',
					'params' => array(
						'area_name' => $this->parent_area['LargeArea']['name'],
						'email' => $register_data['ReviewerRegister']['email'],
						'username' => $this->data['Reviewer']['username'],
						'handle' => $this->data['Reviewer']['handle'],
						'password' => $this->data['Reviewer']['password_confirm'],
					),
					'template' => 'accounts_register_input_reviewer',
				);
				$this->send_mail($mailOptions);

				$this->redirect($this->device_path . '/accounts/register_input_end');
			}
		} else {
			//URL検証
			$pass_data = $this->ReviewerRegister->find('first', array('conditions' => array(
					'ReviewerRegister.id' => $param_id,
					'ReviewerRegister.large_area_id' => $this->parent_area['LargeArea']['id'],
					'ReviewerRegister.uid' => $param_uid,
					'ReviewerRegister.is_created' => 0)));
			if((empty($pass_data)) ||
					(date('YmdHis', strtotime($pass_data['ReviewerRegister']['created'])) != $param_time) ||
					(strtotime($pass_data['ReviewerRegister']['created']) < strtotime('-2 hour'))){
				$this->redirect($this->device_path . '/accounts/error');
			}

			//start---2013/3/10 障害No.1-7-0002修正
			//当該地域に同一メールアドレスで既に登録がないか確認
			if(0 != $this->Reviewer->find('count', array('conditions' => array(
					'Reviewer.email' => $pass_data['ReviewerRegister']['email'],
					// 'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id'],
			)))) {
					$this->redirect($this->device_path . '/accounts/error');
			}

			//end---2013/3/10 障害No.1-7-0002修正

			$this->data['ReviewerRegister']['id'] = $param_id;
		}
		$this->set('data',$this->data);

	}
	//start---2013/3/8 障害No.1-8-0001修正
	//function i_register_input() {
	//$this->register_input();
	//}
	function i_register_input($param_id = null, $param_time = null, $param_uid = null) {
		$this->register_input($param_id, $param_time, $param_uid);
	}
	function s_register_input($param_id = null, $param_time = null, $param_uid = null) {
		$this->register_input($param_id, $param_time, $param_uid);
	}
	//end---2013/3/8 障害No.1-8-0001修正

	//レビュアー登録完了画面
	function register_input_end() {
		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー登録完了画面');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',レビュアー,登録,完了');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー登録完了画面');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー登録完了画面');
	}
	function i_register_input_end() {
		$this->register_input_end();
	}
	function s_register_input_end() {
		$this->register_input_end();
	}

	function login() {
		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー会員・ログイン');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',レビュアー,ログイン');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー会員・ログイン');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー会員・ログイン');

		$reviewer = $this->Auth->user();

		if ($reviewer){
			if (!empty($this->data) && !empty($reviewer)){
				//モバイルの場合はパスポート保存処理をしない
				if(!empty($this->data['Reviewer']['remember_me'])){
					//パスポートを保存するか
					if ($this->data['Reviewer']['remember_me']) {
						$this->passportWrite($reviewer);
					} else {
						$this->passportDelete($reviewer);
					}
					unset($this->data['Reviewer']['remember_me']);
				}
			}
			$this->redirect($this->device_path . '/mypage');
		}else{
			$cookie_reviewer = $this->Cookie->Read('Reviewer');

			if ($cookie_reviewer) {
				//パスポートでログイン
				$options = array(
						'conditions' => array(
								'ReviewerPassport.passport'   => $cookie_reviewer['passport'],
								'ReviewerPassport.modified >' => date('Y-m-d H:i:s', $this->expires),
						));
				$passport = $this->ReviewerPassport->find('first', $options);

				if ($passport){
					$reviewer['username'] = $passport['Reviewer']['username'];
					$reviewer['password'] = $passport['Reviewer']['password'];

					if ($this->Auth->login($reviewer)) {
						$this->passportWrite($passport);

						// FIXME 仮に転送先を設定
						$this->redirect($this->device_path . '/mypage');
					}
				}
			}
		}
		//start---2013/3/10 障害No.1-7-0004修正
		//}
		//end---2013/3/10 障害No.1-7-0004修正
	}
	function s_login() {
		$this->login();
	}

	function logout() {
		$reviewer = $this->Auth->user();
		$this->passportDelete($reviewer);
		$this->Auth->logout();
		$this->redirect($this->device_path . '/top');
	}
	function s_logout() {
		$this->logout();
	}

	/* パスポート発行 */
	private function passportWrite($reviewer){
		$passport = array();
		$passport['reviewer_id'] = $reviewer['Reviewer']['id'];
		$passport['passport'] = Security::generateAuthKey();

		if (isset($reviewer['ReviewerPassport']['id'])) {
			$passport['id'] = $reviewer['ReviewerPassport']['id'];
		}
		$this->ReviewerPassport->save($passport);

		$cookie = array(
				'passport' => $passport['passport']
		);
		$this->Cookie->write('Reviewer', $cookie, true, $this->expires);
	}

	/* パスポート削除 */
	private function passportDelete($reviewer){
		$this->Cookie->delete('Reviewer');

		$conditions = array(
				'ReviewerPassport.reviewer_id' => $reviewer['Reviewer']['id']
		);
		$this->ReviewerPassport->deleteAll($conditions);
	}

	//ログインID送信画面
	function send_loginid() {
		//head
		$this->set('title_for_layout', $this->title_tag_common . ' ログインIDの再送信');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',ログインID');
		$this->set('meta_description', $this->meta_description_common . 'ログインIDの再送信');
		$this->set('header_one', $this->h1_tag_common . 'ログインIDの再送信');

		if(!empty($this->data)){
			$this->SendLoginid->set($this->data);
			if($this->Reviewer->find('count', array('conditions' => array(
					'Reviewer.email' => $this->data['SendLoginid']['email'],
					'Reviewer.handle' => $this->data['SendLoginid']['handle'],
					// 'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id']
				))) == 0) {
					$this->SendLoginid->invalidate('email', 'メールアドレスもしくはニックネームもしくはその両方が正しくありません。');
			}
			if($this->SendLoginid->validates()){

				$reviewer = $this->Reviewer->find('first', array(
					'conditions' => array(
						'Reviewer.email' => $this->data['SendLoginid']['email'],
						'Reviewer.handle' => $this->data['SendLoginid']['handle'],
						// 'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id'],
					)
				));

				//メール送信
				$this->Email->to = $this->data['SendLoginid']['email'];
				$this->Email->from = VALID_LOGINID_SEND_EMAIL;
				$this->Email->subject = 'ID通知';
				$this->Email->smtpOptions = Configure::read('smtpOptionLoginidSend');
				$this->Email->delivery = 'smtp';
				$this->Email->lineLength = 1024;

				//本文作成
				$message = array(
						'あなたのログインIDは以下の通りです。',
						$reviewer['Reviewer']['username'],
						'',
						'----------------------',
						'このメールアドレスは送信専用です。',
				);
				$this->Email->send($message);

				$this->redirect($this->device_path . '/accounts/send_loginid_end');
			}
		}
	}
	function i_send_loginid() {
		$this->send_loginid();
	}

	//ログインID送信完了画面
	function send_loginid_end() {
		//head

		$this->set('title_for_layout', $this->title_tag_common . ' ログインIDの再送信完了画面');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',ログインID');
		$this->set('meta_description', $this->meta_description_common . 'ログインIDの再送信完了画面');
		$this->set('header_one', $this->h1_tag_common . 'ログインIDの再送信完了画面');
	}
	function i_send_loginid_end() {
		$this->send_loginid_end();
	}

	//パスワード再設定URL送信画面
	function changepassword_send() {
		//head

		$this->set('title_for_layout', $this->title_tag_common . ' パスワード変更手続');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',パスワード');
		$this->set('meta_description', $this->meta_description_common . 'パスワード変更手続');
		$this->set('header_one', $this->h1_tag_common . 'パスワード変更手続');

		if(!empty($this->data)){
			$this->ReviewerChangepassword->set($this->data);
			if($this->Reviewer->find('count', array(
				'conditions' => array(
					'Reviewer.email' => $this->data['ReviewerChangepassword']['email'],
					'Reviewer.username' => $this->data['ReviewerChangepassword']['username'],
					// 'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id'],
				)
			)) == 0) {
					$this->ReviewerChangepassword->invalidate('email', 'メールアドレスもしくはログインIDもしくはその両方が正しくありません。');
			}
			if($this->ReviewerChangepassword->validates()){

				//uid生成
				$uid = '';
				$sCharList = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
				mt_srand();
				for($i = 0; $i < 16; $i++){
					$uid .= $sCharList{mt_rand(0, strlen($sCharList) - 1)};
				}
				//レビュアーデータ取得
				$reviewer = $this->Reviewer->find('first', array('conditions' => array(
						'Reviewer.email' => $this->data['ReviewerChangepassword']['email'],
						// 'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id'],
				)));

				//これまでのパスワード変更データを無効にする
				$passwordlist = $this->ReviewerChangepassword->find('all',  array('conditions' => array(
						'ReviewerChangepassword.reviewer_id' => $reviewer['Reviewer']['id'],
						'ReviewerChangepassword.is_changed' => 0)));
				foreach($passwordlist as $key => $record){
					$passwordlist[$key]['ReviewerChangepassword']['is_changed'] = 1;
				}
				//空データが登録されるため空チェックを行う
				if(!empty($passwordlist)){
					$this->ReviewerChangepassword->saveAll($passwordlist);
				}

				//パスワード変更データ保存
				$this->ReviewerChangepassword->create();
				$this->ReviewerChangepassword->reviewer_id = $reviewer['Reviewer']['id'];
				$this->ReviewerChangepassword->uid = $uid;
				$this->ReviewerChangepassword->is_changed = 0;
				$this->ReviewerChangepassword->save($this->ReviewerChangepassword);

				//保存データ取得
				$id = $this->ReviewerChangepassword->id;
				$this->ReviewerChangepassword->create();
				$pass_data = $this->ReviewerChangepassword->findById($id);

				//メール送信
				$this->Email->to = $this->data['ReviewerChangepassword']['email'];
				$this->Email->from = VALID_REVIEWER_CHANGEPASSWORD_URLSEND_EMAIL;
				$this->Email->subject = 'ID通知・パスワード変更依頼';
				$this->Email->smtpOptions = Configure::read('smtpOptionReviewerChangepasswordUrlsend');
				$this->Email->delivery = 'smtp';
				$this->Email->lineLength = 1024;

				$changepassword_input_url = '';
				$terminate_url = 'accounts/changepassword_input/';
				if(isset($this->params['prefix'])){
					if($this->params['prefix'] == 'i'){
						$terminate_url = 'i/accounts/changepassword_input/';
					} else if($this->params['prefix'] == 's') {
						$terminate_url = 's/accounts/changepassword_input/';
					}
				}
				$changepassword_input_url = $this->parent_area['LargeArea']['url'] .
				$terminate_url .
				$reviewer['Reviewer']['id'] . '/' .
				date('YmdHis', strtotime($pass_data['ReviewerChangepassword']['created'])) .
				'/' .
				$uid;

				//本文作成
				$message = array(
						'あなたのログインIDは以下の通りです。',
						$reviewer['Reviewer']['username'],
						'',
						'パスワードを忘れた方は以下のURLからパスワード変更をおこなってください。',
						$changepassword_input_url,
						'なお、このURLは送信されて2時間有効です。',
						'----------------------',
						'このメールアドレスは送信専用です。',
				);
				$this->Email->send($message);

				$this->redirect($this->device_path . '/accounts/changepassword_send_end');
			}
		}
	}
	function i_changepassword_send() {
		$this->changepassword_send();
	}
	function s_changepassword_send() {
		$this->changepassword_send();
	}


	//パスワード再設定URL送信完了画面
	function changepassword_send_end() {
		//head

		$this->set('title_for_layout', $this->title_tag_common . ' パスワード変更URL 送信完了画面');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',パスワード');
		$this->set('meta_description', $this->meta_description_common . 'パスワード変更URL 送信完了画面');
		$this->set('header_one', $this->h1_tag_common . 'パスワード変更URL 送信完了画面');

	}
	function i_changepassword_send_end() {
		$this->changepassword_send_end();
	}
	function s_changepassword_send_end() {
		$this->changepassword_send_end();
	}

	//パスワード再設定画面
	function changepassword_input($param_reviewer_id = null, $param_time = null, $param_uid = null) {
		//head
		$this->set('title_for_layout', $this->title_tag_common . ' パスワード変更再設定画面');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',パスワード,変更');
		$this->set('meta_description', $this->meta_description_common . 'パスワード変更再設定画面');
		$this->set('header_one', $this->h1_tag_common . 'パスワード変更再設定画面');



		if (!empty($this->data)){
			$this->Reviewer->set($this->data);
			if($this->Reviewer->validates()){
				//パスワード保存
				$this->data['Reviewer']['password'] = $this->data['Reviewer']['new_password'];
				$this->Reviewer->saveField('password',$this->Auth->password($this->data['Reviewer']['new_password']));

				//本URLを無効に設定
				$pass_data = $this->ReviewerChangepassword->find('first', array('conditions' => array('ReviewerChangepassword.id' => $this->data['ReviewerChangepassword']['id'])));
				$pass_data['ReviewerChangepassword']['is_changed'] = 1;
				$this->ReviewerChangepassword->save($pass_data);

				$reviewer = $this->Reviewer->findById($this->data['Reviewer']['id']);

				// お客様へのメール送信
				$this->Email->to = $reviewer['Reviewer']['email'];
				$this->Email->from = VALID_REVIEWER_CHANGEPASSWORD_END_EMAIL;
				$this->Email->subject = '【' . $this->parent_area['LargeArea']['name'] . 'デリヘル口コミランキング】パスワードの変更を行いました。';
				$this->Email->smtpOptions = Configure::read('smtpOptionReviewerChangepasswordEnd');
				$this->Email->delivery = 'smtp';
				$this->Email->lineLength = 1024;

				$message = array(
						'【' . $this->parent_area['LargeArea']['name'] . 'デリヘル口コミランキング】パスワードの変更を行いました。',
						'新しいパスワードは以下になります。大切に保管してください。',
						'新しいパスワード：' . $this->data['Reviewer']['new_password'],
						'----------------------',
						'このメールアドレスは送信専用です。',
				);
				$this->Email->send($message);

				// 管理者控え
				$this->Email->to = VALID_REVIEWER_CHANGEPASSWORD_END_EMAIL;
				$this->Email->send($message);

				$this->redirect($this->device_path . '/accounts/changepassword_input_end');
			}
		} else {
			//URL検証
			$pass_data = $this->ReviewerChangepassword->find('first', array('conditions' => array(
					'ReviewerChangepassword.reviewer_id' => $param_reviewer_id,
					'ReviewerChangepassword.uid' => $param_uid,
					'ReviewerChangepassword.is_changed' => 0)));
			if((empty($pass_data)) ||
					(date('YmdHis', strtotime($pass_data['ReviewerChangepassword']['created'])) != $param_time) ||
					(strtotime($pass_data['ReviewerChangepassword']['created']) < strtotime('-2 hour'))){
				$this->redirect($this->device_path . '/accounts/error');
			}

			$this->Reviewer->id = $param_reviewer_id;
			$this->data = $this->Reviewer->read();
			$this->data['ReviewerChangepassword']['id'] = $pass_data['ReviewerChangepassword']['id'];
		}
		$this->set('data',$this->data);

	}
	function i_changepassword_input($param_reviewer_id = null, $param_time = null, $param_uid = null) {
		$this->changepassword_input($param_reviewer_id, $param_time, $param_uid);
	}
	function s_changepassword_input($param_reviewer_id = null, $param_time = null, $param_uid = null) {
		$this->changepassword_input($param_reviewer_id, $param_time, $param_uid);
	}

	//パスワード再設定完了画面
	function changepassword_input_end() {
		//head

		$this->set('title_for_layout', $this->title_tag_common . ' パスワード変更完了画面');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',パスワード,変更');
		$this->set('meta_description', $this->meta_description_common . 'パスワード変更完了画面');
		$this->set('header_one', $this->h1_tag_common . 'パスワード変更完了画面');

	}
	function i_changepassword_input_end() {
		$this->changepassword_input_end();
	}
	function s_changepassword_input_end() {
		$this->changepassword_input_end();
	}

	//メールアドレス変更完了画面
	function changeemail_end($param_reviewer_id = null, $param_time = null, $param_uid = null) {
		//head

		$this->set('title_for_layout', $this->title_tag_common . ' メールアドレス変更完了画面');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',メールアドレス,変更');
		$this->set('meta_description', $this->meta_description_common . 'メールアドレス変更完了画面');
		$this->set('header_one', $this->h1_tag_common . 'メールアドレス変更完了画面');


		//URL検証
		$pass_data = $this->ReviewerChangeemail->find('first', array('conditions' => array(
				'ReviewerChangeemail.reviewer_id' => $param_reviewer_id,
				'ReviewerChangeemail.uid' => $param_uid,
				'ReviewerChangeemail.is_changed' => 0)));
		if((empty($pass_data)) ||
				(date('YmdHis', strtotime($pass_data['ReviewerChangeemail']['created'])) != $param_time) ||
				(strtotime($pass_data['ReviewerChangeemail']['created']) < strtotime('-2 hour'))){
			$this->redirect($this->device_path . '/accounts/error');
		}

		// 同一メールアドレスで既に登録がないか確認
		if(0 != $this->Reviewer->find('count', array('conditions' => array(
				'Reviewer.email' => $pass_data['ReviewerChangeemail']['email'],
				// 'Reviewer.large_area_id' => $this->parent_area['LargeArea']['id'],
			)))) {
				$this->redirect($this->device_path . '/accounts/error');
		}

		//メールアドレス変更
		$this->Reviewer->create();
		$this->Reviewer->id = $param_reviewer_id;
		$this->Reviewer->email = $pass_data['ReviewerChangeemail']['email'];
		$this->Reviewer->save($this->Reviewer);

		//本URLを無効に設定
		$pass_data['ReviewerChangeemail']['is_changed'] = 1;
		$this->ReviewerChangeemail->save($pass_data);

	}
	//start---2013/3/8 障害No.1-8-0001修正
	//function i_changeemail_end() {
	//$this->changeemail_end();
	//}
	function i_changeemail_end($param_reviewer_id = null, $param_time = null, $param_uid = null) {
		$this->changeemail_end($param_reviewer_id, $param_time, $param_uid);
	}
	//end---2013/3/8 障害No.1-8-0001修正


	//エラー画面
	function error() {
		//head
		$this->set('title_for_layout', $this->title_tag_common . ' エラー画面');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',エラー');
		$this->set('meta_description', $this->meta_description_common . 'エラー画面');
		$this->set('header_one', $this->h1_tag_common . 'エラー画面');

	}
	function i_error() {
		$this->error();
	}
	function s_error() {
		$this->error();
	}
}
?>
