<?php
class CategoryController extends AppController {
	var $name = 'Category';
	var $uses = array('Shop', 'Review', 'Girl', 'Coupon', 'MShopsBusinessCategory');

	function beforeFilter() {
		parent::beforeFilter();

		// 自カテゴリ情報を取得
		$category = $this->MShopsBusinessCategory->find('first', array(
			'conditions' => array(
				'MShopsBusinessCategory.id' => $this->params['business_category_id'],
			)
		));
		$this->set('category', $category);

		// クーポンリスト
		$this->set('m_target', Coupon::$targetList);

		switch($this->params['action'])
		{
			case 'index':
				$this->addBreadCrumbs($category['MShopsBusinessCategory']['name'] , "/category/" . $this->params['business_category_id']);
				break;
			case 's_index':
				$this->addBreadCrumbs($category['MShopsBusinessCategory']['name'] , "/category/" . $this->params['business_category_id']);
				break;
		}
	}


	function index() {

		// ピックアップ口コミ情報取得
		$this->get_pickup_review();

/*
		// 女の子の在籍数をJOIN
		$this->Shop->bindModel(array('hasOne' => array('Girl' => array(
			'className'=>'Girl',
			'fields' => array('COUNT(Girl.id) AS girls_count'),
			'conditions' => 'Girl.user_id = User.id',
			'foreignKey' => false
		))), false);

		//ショップデータに口コミデータをバインド
		$this->Shop->bindModel(array('hasOne' => array('Review' => array(
			'className'=>'Review',
			'fields' => array('COUNT(Review.id) AS reviews_count'),
			'conditions' => 'Review.user_id = User.id',
			'foreignKey' => false
		))), false);
*/

		//ショップデータに口コミデータをバインド
		$this->Shop->bindModel(array('hasMany' => array('Coupon' => array(
			'className'=>'Coupon',
			'foreignKey' => 'user_id',
			'limit' => 2
		))), false);

		// FIXME 「*」をやめたほうが良い
		$this->paginate = array('Shop' => array(
			'fields' => array('User.*','UsersAuthority.*','Shop.*', 'System.*', 'LargeArea.url',
				'(SELECT count(*) FROM girls WHERE User.id = girls.user_id) AS girl_cnt',
				'(SELECT count(*) FROM reviews WHERE User.id = reviews.user_id) AS review_cnt',
			),
			'conditions' => array(
				'Shop.is_created' => 1,
				'Shop.shops_business_category_id' => $this->params['business_category_id'],
			),
			'group' => 'User.id',
			'limit' => $this->pcount['categorylist'],
			'order' => 'UsersAuthority.order_rank ASC, rand()'
		));
		$this->set('list', $this->paginate($this->Shop));

		//head
		$this->set('title_for_layout', $this->title_tag_common . 'お店一覧');
		$this->set('meta_keywords', $this->meta_keywords_common .  ',お店一覧');
		$this->set('meta_description', $this->meta_description_common . 'お店一覧');
		$this->set('header_one', $this->h1_tag_common . 'お店一覧');
	}

	function s_index() {
		$this->index();
	}
}
?>
