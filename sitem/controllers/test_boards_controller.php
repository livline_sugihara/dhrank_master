<?php

class TestBoardsController extends AppController {
	var $name = 'TestBoards';
	var $uses = array('TestBoards','VetoIpAddressBoard');
	var $components = array('MediaInfo');

	//掲示板TOP
	function index()
	{
		// 投稿禁止IPアドレス判定
		$ip_count = $this->VetoIpAddressBoard->find('count',
			array('conditions' => array(
					'VetoIpAddressBoard.name' => $this->MediaInfo->getClientIP()
				)
			)
		);

		// 条件に合致すれば、エラーにリダイレクト
		if($ip_count != 0)
			$this->redirect($this->device_path . '/test_boards/error');

		// 表示
//		$boards_data = $this->TestBoards->find('all',
		$this->paginate = array(
//			array(
				'fields' => array(
						'TestBoards.id',
						'TestBoards.name',
						'TestBoards.comment',
				),
				'order' => array('TestBoards.id DESC'),
				'limit' => 20,
//			)
		);
		$boards_data = $this->paginate('TestBoards');
		// セッション付加
		$this->Session->write('boards','boards');
		$this->set('boards_data', $boards_data);
	}

	// 書き込み完了
	function add($media = NULL)
	{

		// 直接来たら、掲示板トップにリダイレクト
		if($this->Session->read('boards') == NULL)
		{
			$this->redirect(
				array(
					'controller'=>'test_boards',
					'action'=>'index',
				)
			);
		}

		// 投稿禁止IPアドレス判定
		$ip_count = $this->VetoIpAddressBoard->find('count',
			array('conditions' => array(
					'VetoIpAddressBoard.name' => $this->MediaInfo->getClientIP()
				)
			)
		);

		// 条件に合致すれば、エラーにリダイレクト
		if($ip_count != 0)
			$this->redirect($this->device_path . '/test_boards/error');

		$params = $this->params['form'];

		// 書き込み
		$data = array(
					'name' => $params['name'],
					'comment' => $params['comment'],
					'media' => $this->MediaInfo->getMedia(),
					'ip_address' => $this->MediaInfo->getClientIP(),
					'created' => date('Y/m/d H:i:s',strtotime('now')),
					'modified' => date('Y/m/d H:i:s',strtotime('now')),
		);

		// 書き込みができたかチェックを行い、チェックにひっかれば、エラーメッセージを渡す
		if ($this->TestBoards->save($data) == false)
		{
			$boards_data = $this->TestBoards->find('all',
				array(
					'fields' => array(
							'TestBoards.id',
							'TestBoards.name',
							'TestBoards.comment',
				),
					'order' => array('TestBoards.id DESC'),
				)
			);
			$this->set('boards_data', $boards_data);
			$this->set('validationErrors', $this->TestBoards->validationErrors);
			$render = 'index';
			if ($media == 'mb') $render = 'i_' . $render; // 携帯なら
			if ($media == 'sp') $render = 's_' . $render; // スマホなら
			$this->render($render);
		}
	}

	// エラー画面
	function error()
	{
		// 直接来たら、掲示板トップにリダイレクト
		if($this->Session->read('boards') == NULL)
		{
			$this->redirect(
				array(
					'controller'=>'test_boards',
					'action'=>'index',
				)
			);
		}

		$boards_data = $this->TestBoards->find('all',
			array(
				'fields' => array(
						'TestBoards.id',
						'TestBoards.name',
						'TestBoards.comment',
				),
				'order' => array('TestBoards.id DESC'),
			)
		);

		//head(ここは名前をどうするか？)
		$this->set('title_for_layout', $this->title_tag_common . '炎上掲示板'.' 投稿エラー');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',炎上,掲示板,BBS');
		$this->set('meta_description', $this->meta_description_common . '炎上掲示板'.' 投稿エラー');
		$this->set('header_one', $this->h1_tag_common . '炎上掲示板'.' 投稿エラー');
		$this->set('validationErrors', $this->TestBoards->validationErrors);
		// body
		$this->set('boards_data', $boards_data);
	}

	// 携帯(必要？)
	function i_index()
	{
		$this->index();
	}
	function i_add()
	{
		$this->add('mb');
	}

	function i_error()
	{
		$this->error();
	}

	// スマホ
	function s_index()
	{
		$this->index();
	}

	function s_add()
	{
		$this->add('sp');
	}

	function s_error()
	{
		$this->error();
	}

}
?>
