<?php

class TestRankingController extends AppController {
	var $name = 'TestRanking';
	var $uses = array('TestRanking','Shop','Review','LargeArea','Reviewer');

	// エリア内店舗、カテゴリ別ランキング取得
	function index($category_id = '' ,$limit = 5)
	{
		$looks_datas = $this->TestRanking->looks('', $category_id, $this->parent_area['LargeArea']['id'], $limit);
		$service_datas = $this->TestRanking->service('', $category_id, $this->parent_area['LargeArea']['id'], $limit);
		$character_datas = $this->TestRanking->character('', $category_id, $this->parent_area['LargeArea']['id'], $limit);

		$this->set('looks_datas', $looks_datas);
		$this->set('service_datas', $service_datas);
		$this->set('character_datas', $character_datas);
	}

	// お店の総合ランキングを取得
	function shops_count()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->TestRanking->shops_count();

	}

	// アクセスの多い女の子集計
	function girls_count()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->TestRanking->girls_count();
	}

	// 口コミ数集計
	function reviewer_count()
	{
		$this->layout = '';
		$this->autoRender = false;
		$this->TestRanking->reviewer_count();
	}

	// お店の総合評価データ取得
	function get_shops_ranking()
	{
		$this->layout = '';
		$this->autoRender = false;

		$datas = $this->TestRanking->getShopRankingData();
		var_dump($datas);
	}

	// 女の子のアクセスカウントランキングデータ取得
	function get_girl_ranking()
	{
		$this->layout = '';
		$this->autoRender = false;

		$datas = $this->TestRanking->getGirlRankingData();
		var_dump($datas);

	}

	// 口コミランキングデータ取得
	function get_reviewer_ranking()
	{
		$this->layout = '';
		$this->autoRender = false;

		$datas = $this->TestRanking->getReviewerRankingData();
		var_dump($datas);
	}

	// cron用テスト関数
	function test_r()
	{
		$this->layout = '';
		$this->autoRender = false;


		// レビュワーランキング作成処理
		$areas = $this->LargeArea->find('all');
		foreach ($areas as $area)
		{
			$this->TestRanking->reviewer_count($area['LargeArea']['id'], 5, 7, true);
		}
	}

}
?>
