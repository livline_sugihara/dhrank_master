<?php

class TestReviewsController extends AppController {
	var $name = 'TestReviews';
	var $uses = array('TestReviews', 'Review','Shop');

	// 口コミページ
	function index()
	{
		// 女の子のおすすめ内容を取得
		$recommend_type = $this->TestReviews->getMGirlsRecommendTypesData();
		$this->set('recommend_type', $recommend_type); // おすすめ内容をセット

	}

	// 店舗ごとの口コミ
	function shops($id = 1)
	{
		$conditions_arr = array();
		$like_arr = array();

		switch($id)
		{
			case '1': $kana_line = 'あ'; $kana_where = array("あ","い","う","え","お"); break;
			case '2': $kana_line = 'か'; $kana_where = array("か","き","く","け","こ","が","ぎ","ぐ","げ","ご"); break;
			case '3': $kana_line = 'さ'; $kana_where = array("さ","し","す","せ","そ","ざ","じ","ず","ぜ","ぞ"); break;
			case '4': $kana_line = 'た'; $kana_where = array("た","ち","つ","て","と","だ","ぢ","づ","で","ど"); break;
			case '5': $kana_line = 'な'; $kana_where = array("な","に","ぬ","ね","の"); break;
			case '6': $kana_line = 'は'; $kana_where = array("は","ひ","ふ","へ","ほ","ば","び","ぶ","べ","ぼ","ぱ","ぴ","ぷ","ぺ","ぽ"); break;
			case '7': $kana_line = 'ま'; $kana_where = array("ま","み","む","め","も"); break;
			case '8': $kana_line = 'や'; $kana_where = array("や","ゆ","よ"); break;
			case '9': $kana_line = 'ら'; $kana_where = array("ら","り","る","れ","ろ"); break;
			case '10': $kana_line = 'わ'; $kana_where = array("わ","を","ん"); break;
		}

		// LIKE文
		$like_arr = array();
		foreach($kana_where as $val)
		{
			 $like_arr[] = array('User.kana LIKE' => $val . '%');
		}

		$conditions_arr['OR'] = $like_arr;

		$review_datas = $this->Review->find('all',
			array(
				'fields' => array(
					'*','count(Review.id) AS cnt'
				),
				'conditions' => $conditions_arr,
				'group' => array('Shop.user_id'),
			)
		);

		$this->set('review_datas',$review_datas); //お店のデータ
		$this->set('kana_line', $kana_line); // かな行

	}

	// 口コミ非掲載理由
	function reason()
	{
		// 非掲載一覧　ページャー付ける
//		$datas = $this->TestReviews->getReasonData();
		$this->paginate = $this->TestReviews->getReasonData;



		$this->set('datas', $datas);

	}

	// 検索条件で閲覧
	function search(){
		$params = $this->params['form'];

		// 条件
		$conditions_arr = array();
		// 名前
		if(isset($params['girl_name']) && $params['girl_name'] != '')
			$conditions_arr[]['OR']['Girl.name LIKE'] = '%' . $params['girl_name'] . '%';

		// 年齢
		if(isset($params['arr_age'])){
			$age_arr = array();
			foreach($params['arr_age'] as $key => $val){
				switch($val){
					case '1' :
						$age_arr[] = array('Girl.age >=' => 18, 'Girl.age <=' => 19);
						break;
					case '2' :
						$age_arr[] = array('Girl.age >=' => 20, 'Girl.age <=' => 24);
						break;
					case '3' :
						$age_arr[] = array('Girl.age >=' => 25, 'Girl.age <=' => 29);
						break;
					case '4' :
						$age_arr[] = array('Girl.age >=' => 30, 'Girl.age <=' => 34);
						break;
					case '5' :
						$age_arr[] = array('Girl.age >=' => 35);
						break;
				}
			}
			$conditions_arr[]['OR'] = $age_arr;
		}

		// 身長
		if(isset($params['arr_tall'])){
			$height_arr = array();
			foreach($params['arr_tall'] as $key => $val){
				switch($val){
					case '1' :
						$height_arr[] = array('Girl.body_height <=' => 149);
						break;
					case '2' :
						$height_arr[] = array('Girl.body_height >=' => 150, 'Girl.body_height <=' => 154);
						break;
					case '3' :
						$height_arr[] = array('Girl.body_height >=' => 155, 'Girl.body_height <=' => 159);
						break;
					case '4' :
						$height_arr[] = array('Girl.body_height >=' => 160, 'Girl.body_height <=' => 164);
						break;
					case '5' :
						$height_arr[] = array('Girl.body_height >=' => 165, 'Girl.body_height <=' => 169);
						break;
					case '6' :
						$height_arr[] = array('Girl.body_height >=' => 170);
						break;
				}
			}
			$conditions_arr[]['OR'] = $height_arr;
		}

		// カップ数
		$cup_arr = array();
		if(isset($params['arr_cup'])){
			foreach($params['arr_cup'] as $key => $val){
				$cup_arr[] = $val;
			}
			$conditions_arr[]['OR']['Girl.girls_cup_id'] = $cup_arr;
		}

		// バスト
		if(isset($params['arr_bust'])){
			$bust_arr = array();
			foreach($params['arr_bust'] as $key => $val){
				switch($val){
					case '1' :
						$bust_arr[] = array('Girl.bust <=' => 79);
						break;
					case '2' :
						$bust_arr[] = array('Girl.bust >=' => 80, 'Girl.bust <=' => 84);
						break;
					case '3' :
						$bust_arr[] = array('Girl.bust >=' => 85, 'Girl.bust <=' => 89);
						break;
					case '4' :
						$bust_arr[] = array('Girl.bust >=' => 90, 'Girl.bust <=' => 94);
						break;
					case '5' :
						$bust_arr[] = array('Girl.bust >' => 95, 'Girl.bust <=' => 99);
						break;
					case '6' :
						$bust_arr[] = array('Girl.bust >=' => 100);
						break;
				}
			}
			$conditions_arr[]['OR'] = $bust_arr;
		}

		// ウエスト

		if(isset($params['arr_waist'])){
			$waist_arr = array();
			foreach($params['arr_waist'] as $key => $val){
				switch($val){
					case '1' :
						$waist_arr[] = array('Girl.waist <=' => 54);
						break;
					case '2' :
						$waist_arr[] = array('Girl.waist >=' => 55, 'Girl.waist <=' => 59);
						break;
					case '3' :
						$waist_arr[] = array('Girl.waist >=' => 60, 'Girl.waist <=' => 64);
						break;
					case '4' :
						$waist_arr[] = array('Girl.waist >=' => 65, 'Girl.waist <=' => 69);
						break;
					case '5' :
						$waist_arr[] = array('Girl.waist >=' => 70);
						break;
				}
			}
			$conditions_arr[]['OR'] = $waist_arr;
		}

		// ヒップ
		if(isset($params['arr_hip'])){
			$hip_arr = array();
			foreach($params['arr_hip'] as $key => $val){
				switch($val){
					case '1' :
						$hip_arr[] = array('Girl.hip <=' => 79);
						break;
					case '2' :
						$hip_arr[] = array('Girl.hip >=' => 80, 'Girl.hip <=' => 84);
						break;
					case '3' :
						$hip_arr[] = array('Girl.hip >=' => 85, 'Girl.hip <=' => 89);
						break;
					case '4' :
						$hip_arr[] = array('Girl.hip >=' => 90, 'Girl.hip <=' => 94);
						break;
					case '5' :
						$hip_arr[] = array('Girl.hip >=' => 95, 'Girl.hip <=' => 99);
						break;
					case '6' :
						$hip_arr[] = array('Girl.hip >=' => 100);
						break;

				}
			}
			$conditions_arr[]['OR'] = $hip_arr;
		}

		// カテゴリ
		if(isset($params['arr_cate']))
		{
			$cate_arr = array();
			foreach($params['arr_cate'] as $key => $val){
				$cate_arr[] = $val;
			}
			$conditions_arr[]['OR']['Shop.shops_business_category_id'] = $cate_arr;
		}
		// 女の子のおすすめ内容
		if(isset($params['recommend'])){
			foreach($params['recommend'] as $key => $val){
				$recommend_arr[] = $val;
			}
			$conditions_arr[]['OR']['Girl.recommend_type_id'] = $recommend_arr;
		}

		// レビュワー名
		if(isset($params['reviewer_name']) && $params['reviewer_name'] != '')
			$conditions_arr[]['OR']['Reviewer.handle LIKE'] = '%' . $params['reviewer_name'] . '%';

		// コメントで検索
		if(isset($params['comment']) && $params['comment'] != '')
			$conditions_arr[]['OR']['Review.comment LIKE'] = '%' . $params['comment'] . '%';

		$review_datas = $this->Review->find('all',
			array(
				'fields' => array(
					'Shop.name',
					'Girl.name',
					'Review.comment',
					'Reviewer.handle'
				),
				'conditions' => $conditions_arr,
			)
		);

		$this->set('review_datas',$review_datas);

	}
}
?>
