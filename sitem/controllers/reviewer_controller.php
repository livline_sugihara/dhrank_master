<?php
class ReviewerController extends ReviewersController {
	var $name = 'Reviewer';
	var $uses = array('Review','Reviewer','Message','MReviewersAge', 'MReviewersJob','ReviewerChangeemail','MReviewsAge','MReviewsPlace','ReviewsGoodCount','BookmarkShop','BookmarkGirl','Message');

	function beforeFilter(){
		parent::beforeFilter();
		$this->layout = 'reviewer';

		// 主とするレビュアーデータ取得
		$reviewer_data = $this->Reviewer->findbyId($this->params['reviewer_id']);
		$this->set('reviewer_data',$reviewer_data);

		// フォロワー情報取得
		$this->getFollower($this->params['reviewer_id']);

		// パンくずに追加
		$this->clearBreadCrumbs();
		$this->addBreadCrumbs("全国" , APP_ALLAREA_URL);
		$this->addBreadCrumbs("Myページ" , "/mypage");

		switch($this->params['action'])
		{
			case 'profile':
				$this->addBreadCrumbs("レビュアー(" . $reviewer_data['Reviewer']['handle'] . "さん プロフィール)" , "/reviewer/profile/" . $this->params['reviewer_id']);
			//	$this->set('side_select', 'profile');
				break;
			case 's_profile':
				$this->addBreadCrumbs("レビュアー(" . $reviewer_data['Reviewer']['handle'] . "さん プロフィール)" , "/s/reviewer/profile/" . $this->params['reviewer_id']);
				break;
			case 'reviews':
				$this->addBreadCrumbs("レビュアー(" . $reviewer_data['Reviewer']['handle'] . "さん レビュー一覧)" , "/reviewer/reviews/" . $this->params['reviewer_id']);
			//	$this->set('side_select', 'reviews');
				break;
			case 's_reviews':
				$this->addBreadCrumbs("レビュアー(" . $reviewer_data['Reviewer']['handle'] . "さん レビュー一覧)" , "/s/reviewer/reviews/" . $this->params['reviewer_id']);
			//	$this->set('side_select', 'reviews');
				break;
			case 'follow':
				$this->addBreadCrumbs("レビュアー(" . $reviewer_data['Reviewer']['handle'] . "さん フォロー一覧)" , "/reviewer/follow/" . $this->params['reviewer_id']);
			//	$this->set('side_select', '');
				break;
			case 's_follow':
				$this->addBreadCrumbs("レビュアー(" . $reviewer_data['Reviewer']['handle'] . "さん フォロー一覧)" , "/s/reviewer/follow/" . $this->params['reviewer_id']);
			//	$this->set('side_select', '');
				break;
			case 'follower':
				$this->addBreadCrumbs("レビュアー(" . $reviewer_data['Reviewer']['handle'] . "さん フォロワー一覧)" , "/reviewer/follower/" . $this->params['reviewer_id']);
			//	$this->set('side_select', '');
				break;
			case 's_follower':
				$this->addBreadCrumbs("レビュアー(" . $reviewer_data['Reviewer']['handle'] . "さん フォロワー一覧)" , "/s/reviewer/follower/" . $this->params['reviewer_id']);
			//	$this->set('side_select', '');
				break;
			case 'index':
			default:
				$this->set('side_select', 'profile');
				break;
		}
	}

	function follow(){

		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 フォロー一覧');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルランキング,フォロー一覧');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 フォロー一覧');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 フォロー一覧');

		// データは共通で取得

	}
	function s_follow() {
		$this->follow();
	}

	function follower(){

		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 フォロワー一覧');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルランキング,フォロワー一覧');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 フォロワー一覧');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 フォロワー一覧');

		// データは共通で取得

	}
	function s_follower() {
		$this->follower();
	}

	function reviews(){

		// 閲覧しているユーザーのアクセスカウントを引っ張る
		$reviewer_datas = $this->Reviewer->find('first',
			array(
				'fields' => array('Reviewer.access_count', 'Reviewer.handle'),
				'conditions' => array('Reviewer.id' => $this->params['reviewer_id']),
			)
		);
		$access_count = ++$reviewer_datas['Reviewer']['access_count'];

		// アクセスカウント１プラス
		$this->Reviewer->save(
			array(
				'id' => $this->params['reviewer_id'],
				'access_count' => $access_count,
			)
		);

		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 レビュー');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルランキング,レビュー');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 レビュー');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 レビュー');

		// レビュー情報を取得する
		$conditions = array(
			'Shop.is_created' => 1,
			'Review.reviewer_id' => $this->params['reviewer_id'],
		);
		$list = $this->_getReviews($conditions, 'Review.created DESC');
		$this->set('list', $list);

	}
	function s_reviews() {
		$this->reviews();
	}

	function profile(){
		//head
		$this->set('title_for_layout', $this->title_tag_common . ' レビュアー管理 プロフィール表示');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',デリヘルガイド,プロフィール,表示');
		$this->set('meta_description', $this->meta_description_common . 'レビュアー管理 プロフィール表示');
		$this->set('header_one', $this->h1_tag_common . 'レビュアー管理 プロフィール表示');
	}
	function profile_image(){
		$this->render('profile_image');
	}
	function i_profile() {
		$this->profile();
	}
	function s_profile() {
		$this->profile();
	}
}
?>
