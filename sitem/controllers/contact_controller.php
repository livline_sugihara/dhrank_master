<?php
class ContactController extends AppController {
	var $name = 'Contact';
	var $uses = array('Contact');
	const SESSION_CONTACT_DATA_KEY = 'ContactController.index.contact';

	function beforeFilter(){
		parent::beforeFilter();
		$this->layout = 'login';
		switch($this->params['action'])
		{
			case 'index':
				$this->addBreadCrumbs("お問い合わせ" , "/contact");
				break;
		}
	}

	function index(){
		$umu_select = array(0 => '有', 1 => '無');
		$this->set('umu_select', $umu_select);
		//head
		$this->set('title_for_layout', $this->title_tag_common . ' お問い合わせ');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',お問い合わせ');
		$this->set('meta_description', $this->meta_description_common . 'お問い合わせ');
		$this->set('header_one', $this->h1_tag_common . 'お問い合わせ');
		if(!empty($this->data)) {
			$mode = $this->data['Contact']['mode'];
			switch($mode) {
				// 有料掲載希望
				case 'payment_insert':
					$form_name = '有料掲載希望';
					$this->Contact->validate = $this->Contact->validate_payment_insert;
					break;

				// 無料掲載希望
					case 'free_insert':
					$this->Contact->validate = $this->Contact->validate_free_insert;
					$form_name = '無料掲載希望';
					break;

				// 代理店契約
					case 'agency_agreement':
					$this->Contact->validate = $this->Contact->validate_agency_agreement;
					$form_name = '代理店契約';
					break;

				// その他
					case 'other':
					$this->Contact->validate = $this->Contact->validate_other;
					$form_name = 'その他';
					break;

				default:
					$this->cakeError('error404');
					break;

			}
			if(isset($this->params["form"]["confirm"])) {
				$this->Contact->set($this->data);
				if($this->Contact->validates()) {
					// 一旦Sessionに持つ
					$this->Session->write(self::SESSION_CONTACT_DATA_KEY, $this->data);
					// 確認画面をレンダリング
					$this->render($this->device_file_name . 'confirm');
				}

			} else if(isset($this->params["form"]["register"])) {
				if($this->Session->check(self::SESSION_CONTACT_DATA_KEY)) {
					$this->data = $this->Session->read(self::SESSION_CONTACT_DATA_KEY);
				}

				//投稿画面へ戻る
				$this->set('data', $this->data);

				// 送信者へ確認メール
				var_dump($this->data['Contact']);
				exit;

				$mailOptions = array(
					'to' => $this->data['Contact']['emailto'],
					'from' => VALID_CONTACTS_EMAIL,
					'subject' => 'お問い合わせ（' . $form_name . '）を承りました。',
					'smtpOptionsKey' => 'smtpOptionRegisterUrlsend',
					'params' => array(
						'data' =>  $this->data,
						'umu_select' => $umu_select,
					),
					'template' => 'contact_' . $mode . '_customer',

				);
				$this->send_mail($mailOptions);

				// 管理者へメール
				$mailOptions = array(
//					'to' => VALID_CONTACTS_EMAIL,
					'to' => 'kazuya.misono@livline.jp',
					'from' => VALID_CONTACTS_EMAIL,
					'subject' => 'お問い合わせ（' . $form_name . '）を受信しました',
					'smtpOptionsKey' => 'smtpOptionRegisterUrlsend',
					'params' => array(
						'data' =>  $this->data,
						'umu_select' => $umu_select,
					),
					'template' => 'contact_' . $mode . '_staff',
				);
				$this->send_mail($mailOptions);
				$this->redirect($this->device_path . '/contact/end');
			}
		}
	}

	function s_index() {
		$this->index();
	}

	function end() {		//head
		$this->set('title_for_layout', $this->title_tag_common . ' お問い合わせ送信完了画面');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',お問い合わせ,送信完了');
		$this->set('meta_description', $this->meta_description_common . 'お問い合わせ送信完了画面');
		$this->set('header_one', $this->h1_tag_common . 'お問い合わせ送信完了画面');
	}

	function s_end() {
		$this->end();
	}

}
?>
