<?php

class ShopBoardsController extends AppController {
	var $name = 'ShopBoards';
	var $uses = array('ShopBoards','Shop','UsersAuthority','VetoIpAddressBoard');
	var $components = array('MediaInfo');

	//掲示板TOP
	function index($user_id = '')
	{
		$this->layout = 'shop_board';

		// 店舗が掲示板を非表示にしているかチェック
		$authority_boards = $this->UsersAuthority->find('all',
			array(
				'fields' => array('UsersAuthority.is_board_shop'),
				'conditions' => array('UsersAuthority.user_id = ' . $user_id),
			)
		);
		$is_board_shop = false;
		if (isset($authority_boards[0]['UsersAuthority']['is_board_shop']) && $authority_boards[0]['UsersAuthority']['is_board_shop'] == 1)
			$is_board_shop = true;

		if ($is_board_shop == true)
			$this->redirect($this->device_path . '/shop_boards/error/');

		// 投稿禁止IPアドレス判定
		$ip_count = $this->VetoIpAddressBoard->find('count',
			array('conditions' => array(
					'VetoIpAddressBoard.name' => $this->MediaInfo->getClientIP()
				)
			)
		);

		// 店舗の名前を取得
		$shop_name = $this->Shop->find('first',
			array(
				'fields' => array('Shop.name', '*'),
				'conditions' => array('Shop.user_id = ' . $user_id),
			)
		);

		// 条件に合致すれば、エラーにリダイレクト
		if($ip_count != 0 || $user_id == '' || $shop_name == false)
			$this->redirect($this->device_path . '/shop_boards/error/');

		// 表示
		$this->paginate = array(
				'fields' => array(
						'LPAD(ShopBoards.post_no, 3, "0") AS post_no',
						'LPAD(ShopBoards.res_post_no, 3, "0") AS res_post_no',
						'ShopBoards.name',
						'ShopBoards.comment',
						'ShopBoards.created',
				),
				'conditions' => array('ShopBoards.user_id = ' . $user_id),
//				'group' => array('ShopBoards.post_no'),
				'order' => array('ShopBoards.post_no DESC'),
				'limit' => 5,
		);
		$boards_data = $this->paginate('ShopBoards');

		// セッション付加
		$this->Session->write('boards','boards');
		$this->set('user_id', $user_id);
		$this->set('shop_name', $shop_name['Shop']['name']);
		$this->set('boards_data', $boards_data);
		$this->set('boards_count', count($boards_data));
	}

	// 書き込み完了
	function add($user_id = '', $res_post_no = '', $media = 'pc')
	{
		$this->layout = 'shop_board';

		// 直接来たら、掲示板エラーにリダイレクト
		if($user_id == '')
			$this->redirect($this->device_path . '/shop_boards/error/');

		// 投稿禁止IPアドレス判定
		$ip_count = $this->VetoIpAddressBoard->find('count',
			array('conditions' => array(
					'VetoIpAddressBoard.name' => $this->MediaInfo->getClientIP()
				)
			)
		);

		// 店舗が掲示板を非表示にしているかチェック
		$authority_boards = $this->UsersAuthority->find('all',
			array(
				'fields' => array('UsersAuthority.is_board_shop'),
				'conditions' => array('UsersAuthority.user_id = ' . $user_id),
			)
		);
		if (isset($authority_boards[0]['UsersAuthority']['is_board_shop']) && $authority_boards[0]['UsersAuthority']['is_board_shop'] == 0)
			$is_board_shop = true;
		else
			$is_board_shop = false;

		// 条件に合致すれば、エラーにリダイレクト
		if($ip_count != 0 || $is_board_shop == false)
			$this->redirect($this->device_path . '/shop_boards/error/' . $user_id);

		$params = $this->params['form'];

		// 書き込み
		// 番号の最大値を取得する
		$max_count = $this->ShopBoards->find('count',
			array('conditions' => array(
					'ShopBoards.user_id' => $user_id,
				)
			)
		);

		$data = array(
					'user_id' => $user_id,
					'post_no' => $max_count + 1,
					'name' => $params['postname'],
					'comment' => $params['postcomment'],
					'media' => $this->MediaInfo->getMedia(),
					'ip_address' => $this->MediaInfo->getClientIP(),
					'created' => date('Y/m/d H:i:s',strtotime('now')),
					'modified' => date('Y/m/d H:i:s',strtotime('now')),
		);

		if ($params['postres'] != '' && $media == 'pc') $data['res_post_no'] = $params['postres'];
		if ($res_post_no != '' && $media != 'pc') $data['res_post_no'] = $res_post_no;

		$this->set('user_id', $user_id);

		if ($this->ShopBoards->save($data) == false)
		{
			$boards_data = $this->ShopBoards->find('all',
				array(
					'fields' => array(
							'ShopBoards.id',
							'ShopBoards.name',
							'ShopBoards.comment',
				),
					'order' => array('ShopBoards.id DESC'),
				)
			);

			$this->set('boards_data', $boards_data);
			$this->set('validationErrors', $this->ShopBoards->validationErrors);
			$render = 'add';
			if ($media == 'mb') $render = 'i_' . $render; // 携帯なら
			if ($media == 'sp') $render = 's_' . $render; // スマホなら
			$this->render($render);
		}
	}

	// エラー画面
	function error()
	{
		$this->layout = 'shop_board';

		//head(ここは名前をどうするか？)
		$this->set('title_for_layout', $this->title_tag_common . '炎上掲示板'.' 投稿エラー');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',炎上,掲示板,BBS');
		$this->set('meta_description', $this->meta_description_common . '炎上掲示板'.' 投稿エラー');
		$this->set('header_one', $this->h1_tag_common . '炎上掲示板'.' 投稿エラー');
		$this->set('validationErrors', $this->ShopBoards->validationErrors);
	}

	// 投稿用画面
	function form($user_id, $res_post_no = '')
	{
		$this->layout = 'shop_board';
		$this->set('user_id', $user_id); // 店舗ID
		$this->set('res_post_no', $res_post_no); // レス番号
	}

	// レス詳細画面
	function res($user_id, $post_no){
		$this->layout = 'shop_board';
		$boards_data = $this->ShopBoards->find('first',
			array(
				'fields' => array(
					'LPAD(ShopBoards.post_no, 3, "0") AS post_no',
					'LPAD(ShopBoards.res_post_no, 3, "0") AS res_post_no',
					'ShopBoards.id',
					'ShopBoards.name',
					'ShopBoards.comment',
					'ShopBoards.created',
			),
				'conditions' => array(
					'ShopBoards.user_id' => $user_id,
					'ShopBoards.post_no' => (int)$post_no,
				),
				'order' => array('ShopBoards.id DESC'),
			)
		);

		$this->set('user_id', $user_id); // 店舗ID
		$this->set('boards_data', $boards_data); // 掲示板のデータ
		$this->set('title_for_layout', $this->title_tag_common . '炎上掲示板'.' 投稿エラー');
		$this->set('meta_keywords', $this->meta_keywords_common . $this->parent_area['LargeArea']['name'] . ',炎上,掲示板,BBS');
		$this->set('meta_description', $this->meta_description_common . '炎上掲示板'.' 投稿エラー');
		$this->set('header_one', $this->h1_tag_common . '炎上掲示板'.' 投稿エラー');
		$this->set('validationErrors', $this->ShopBoards->validationErrors);
	}

	// 携帯(必要？)
	function i_index()
	{
		$this->index();
	}
	function i_add()
	{
		$this->add('mb');
	}

	function i_error()
	{
		$this->error();
	}

	// スマホ
	function s_index($user_id)
	{
		$this->index($user_id);
	}

	function s_add($user_id, $res_post_no = '')
	{
		$this->add($user_id, $res_post_no, 'sp');
	}

	function s_error()
	{
		$this->error();
	}

	// スマホ投稿用画面
	function s_form($user_id, $res_post_no = '')
	{
		$this->form($user_id, $res_post_no);
	}

	// レス詳細画面
	function s_res($user_id, $post_id){
		$this->res($user_id, $post_id);
	}


}
?>
