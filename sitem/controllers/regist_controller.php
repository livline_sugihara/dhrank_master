<?php
class RegistController extends AppController {
	var $name = 'Regist';
	var $uses = array('Shop');

	function index(){
		//かな読み込み、セット
		$kana_list = Configure::read('kana');
		$this->set('kana_list',$kana_list);

		//ショップデータに口コミデータをバインド
		$bind = array('hasOne' => array('Review' => array(
				'className'=>'Review',
				'fields' => array('COUNT(Review.id) AS reviews_count'),
				'conditions' => 'Review.user_id = User.id',
				'foreignKey' => false)));
		//あかさたな別にショプデータ取得
		foreach($kana_list as $key => $value){
			$this->Shop->bindModel($bind,false);
			$shops = $this->Shop->find('all',array(
					'fields' => array('Shop.name','MShopsBusinessCategory.name', 'User.id', 'LargeArea.url','COUNT(Review.id) AS reviews_count'),
					'conditions' => array(
							$this->getCurrentAreaCondition(),
							'SUBSTRING(User.kana,1,1)' => $value,),
					'group' => array('Shop.user_id'),
					'order' => 'User.kana ASC'));
			$list[$key] = $shops;
		}
		$this->set('list',$list);
		$this->get_shops_index_common();
	}	
		
	//start---2013/3/13 障害No.1-6-0003修正
	function s_index(){
		$this->redirect('/s/top');
	}
	//end---2013/3/13 障害No.1-6-0003修正

	//meta取得
	private function get_shops_index_common(){
		//head
		$this->set('title_for_layout', $this->title_tag_common . 'お店一覧');
		$this->set('meta_keywords', $this->meta_keywords_common .  ',お店一覧');
		$this->set('meta_description', $this->meta_description_common . 'お店一覧');
		$this->set('header_one', $this->h1_tag_common . 'お店一覧');
	}

	//start---2013/3/13 障害No.1-6-0003修正
	function kana(){
		if(!empty($this->params['pass'][0])){
			$this->redirect('/shops#kana' . $this->params['pass'][0]);
		}
	}
	//end---2013/3/13 障害No.1-6-0003修正


	//かな別お店一覧表示画面（携帯）
	function i_kana($param){

		//ショップデータに口コミデータをバインド
		$bind = array('hasOne' => array('Review' => array(
				'className'=>'Review',
				'fields' => array('COUNT(Review.id) AS reviews_count'),
				'conditions' => 'Review.user_id = User.id',
				'foreignKey' => false)));
		$this->Shop->bindModel($bind,false);

		//かな読み込み
		$kana_list = Configure::read('kana');
		$list = $this->Shop->find('all',array(
				'fields' => array('Shop.name','MShopsBusinessCategory.name', 'User.id', 'LargeArea.url','COUNT(Review.id) AS reviews_count'),
				'conditions' => array(
						$this->getCurrentAreaCondition(),
						'LEFT(User.kana, 1)' => $kana_list[$param]),
				'group' => array('Shop.id'),
				'order' => 'User.kana ASC'));
		$this->set('list',$list);
		$this->set('kana',$kana_list[$param][0]);

		//head
		$this->set('title_for_layout', $this->title_tag_common . 'お店一覧 ' . $kana_list[$param][0] . '行');
		$this->set('meta_keywords', $this->meta_keywords_common .  ',お店一覧');
		$this->set('meta_description', $this->meta_description_common . 'お店一覧 ' . $kana_list[$param][0] . '行');
		$this->set('header_one', $this->h1_tag_common . 'お店一覧 ' . $kana_list[$param][0] . '行');
	}

	function s_kana($param){
		$this->i_kana($param);
	}
}
?>
