<?php

class SearchComponent extends Object{

    var $_controller;

    //array('Girl','Shop','Review','Test');



    public function load(& $controller)
    {
        $this->_controller = $controller;
    }


	// 電話帳検索
	public function tel($shops_data){
		$shops_data = $this->_controller->Shop->find('all', array(
				'fields' => array('Shop.id',
								  'Shop.name',
								  'Shop.phone_number',
								  'Shop.url_pc',
								  'Shop.url_mobile',
								  'Shop.url_smartphone',
				), // ID,お店の番号、ＵＲＬ
                //	'conditions' => $conditions_arr,
				'limit' => 30,
			)
		);

        return $shops_data;
		//$this->set('shops_data',$shops_data);
	}

	// カテゴリ検索
	function category($params){
		//$params = $this->params['form']; // POSTデータ
		$category_arr = array();
		if(isset($params['category'])){
			foreach($params['category'] as $key => $val){
				$category_arr[] = $val;
			}
			$category_arr[]['OR']['MShopsBusinessCategory.id'] = $category_arr;
		}

		$shops_data = $this->Shop->find('all', array(
			'fields' => array(
				'*'
			),
			'conditions' => $category_arr,
			'limit' => 50,
			)
		);

        return $shops_data;
		//$this->set('shops_data', $shops_data);

	}

	// 料金
	function price($params){
		$price_arr = array();
		//$params = $this->params['form']; // POSTデータ
		if (isset($params['price_from']) && $params['price_from'] > 0) $price_arr['System.price_cost >= '] = $params['price_from'];
		if (isset($params['price_to']) && $params['price_to'] > 0) $price_arr['System.price_cost <= '] = $params['price_to'];
		$price_data = $this->Shop->find('all', array(
			'fields' => array(
				'*'
			),
			'conditions' => $price_arr,
			'order' => array('System.price_cost'),
			)
		);

        return $price_data;
		//$this->set('price_data', $price_data);

	}
	// エリア


	// 口コミ検索
	function review(){
		$reviews_data = $this->Shop->find('all', array(
			'fields' => array(
				'*'
			),
			'limit' => 300,
			)
		);

        return $reviews_data;
//		var_dump($reviews_data);
	}


	// Ajax用
	function update(){
		$this->layout = "";
		var_dump($this->params);




	}


}
