<?php
// 円グラフ表示用データ取得
class PieChartComponent extends Object{

    var $_controller;

    public function load(& $controller)
    {
        $this->_controller = $controller;
    }

	// データ取得
	function getData($reviews = null)
    {
        if ($reviews == null)
            return false;

        $score_girl_looks = 0;     // ルックス
        $score_girl_service = 0;   // サービス
        $score_girl_character = 0; // 性格

        $reviews_count = ($reviews != null) ? count($reviews) : 0; // 口コミ数

        if ($reviews_count == 0)
            return false;

		foreach($reviews as $key => $val){
            // ルックス合計
			$score_girl_looks += (int)$val['Review']['score_girl_looks'];
            // サービス合計
			$score_girl_service += (int)$val['Review']['score_girl_service'];
			// キャラクター合計
			$score_girl_character += (int)$val['Review']['score_girl_character'];
		}

		// 円グラフのパーセントを取得
        $pie_chart_data_arr = array();
		$sum = $score_girl_character + $score_girl_service + $score_girl_looks; // 合計値
        $looks_parsent = round(($score_girl_looks / $sum) * 100);
		$service_parsent = round(($score_girl_service / $sum) * 100);
        $character_parsent = round(($score_girl_character / $sum) * 100); // 100分率割合
        $avg_looks =  round($score_girl_looks / $reviews_count,1); // 5段階評価での平均値
        $avg_service = round($score_girl_service / $reviews_count,1);
		$avg_character = round($score_girl_character / $reviews_count,1);

		// viewに渡す
		$pie_chart_data_arr['reviews_count'] = $reviews_count;
        $pie_chart_data_arr['looks_parsent'] = $looks_parsent;
        $pie_chart_data_arr['service_parsent'] = $service_parsent;
		$pie_chart_data_arr['character_parsent'] = $character_parsent;
        $pie_chart_data_arr['avg_looks'] = $avg_looks;
        $pie_chart_data_arr['avg_service'] = $avg_service;
		$pie_chart_data_arr['avg_character'] = $avg_character;

        return $pie_chart_data_arr;
	}


}
