// ------------------------------------------------------------
// modal
// ------------------------------------------------------------
$( function(){
	var tgt,resNum,openFlag,modalH;
	$(document).on('click',"a.modal",function(e){
		e.preventDefault();
		$('[name="postres"]').remove();

		tgt   = $(this).attr("data-href");
		resNum =$(this).find(".num").html();
		var tgtHtml =$('#'+tgt).clone();
		var iframeText;
		if(openFlag){
			$(".modal_contents .modal_block").remove();
			$(".modal_contents").append(tgtHtml);
			modalH=parseInt($(".modal_contents").css('height'));
			$(".modal_contents").css({'margin-top':-modalH/2});
			if(resNum){
				$(".boards_overlay textarea.comment").val('>>'+resNum+'\n');
			}
		}else{
			iframeText  = '<div class="modal_contents"><div class="btn_close"><a href="#"><i class="fa fa-times"></i></a></div></div></div>';
			$("body").append('<div class="boards_overlay"></div>');
			$(".boards_overlay").append(iframeText);
			$(".modal_contents").append(tgtHtml);
			if(resNum){
				$(".boards_overlay textarea.comment").val('>>'+resNum+'\n');
			}
			modalH=parseInt($(".modal_contents").css('height'));
			$(".modal_contents").css({'margin-top':-modalH/2});
			$(".boards_overlay").stop().animate({'opacity':1},500,'oX2');
			openFlag=true;
		}
	});
    $(document).on('click',".btn_close",function(e){
        e.preventDefault();
        $(".boards_overlay").html("").stop().animate({'opacity':0},1000,'oX2',
            function(){
                $(this).remove();
                openFlag=null;
            });
    });
    $(document).on('click',"a.page_list",function(e){
    	e.preventDefault();
    	$(".page_jump").slideToggle(100);
    });

	$(document).on('click',".btn_post", function(e){
		var name = $('[name="formname"]')[1].value; // この書き方で取得する
		var comment = $('[name="formcomment"]')[1].value;
		console.log(comment);
		var errmsg = '';

		if (name == '')
		{
			errmsg = errmsg + 'お名前を入力してください' + "\n";
		}
		else if(name.length > 255)
		{
			errmsg = errmsg + 'お名前は255文字以内で入力してください' + "\n";
		}

		if (comment == '')
		{
			errmsg = errmsg + '投稿内容を入力してください' + "\n";
		}

		// エラーメッセージ
		if (errmsg != '')
		{
			alert(errmsg);
			return false;
		}

		// レスが付いているかチェック
		var slice_comment = comment.slice(0,5);
		var reg = /\d{3}/g;
		var mat = slice_comment.match(reg);

		// POST送信を行う
		$('[name="postform"]').prepend('<input type="hidden" name="postname" value="' + name + '" />');
		$('[name="postform"]').prepend('<input type="hidden" name="postcomment" value="' + comment + '" />');
		if (mat != '')
			$('[name="postform"]').prepend('<input type="hidden" name="postres" value="' + mat[0] + '" />');

		$('[name="postform"]').submit();
		return false;

	})

});
