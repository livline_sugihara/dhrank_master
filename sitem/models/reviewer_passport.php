<?php

class ReviewerPassport extends AppModel {
	public $name = 'ReviewerPassport';

	public $belongsTo = array(
		'Reviewer' => array(
			'className' => 'Reviewer',
			'foreignKey' => 'reviewer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
 	);
}

?>
