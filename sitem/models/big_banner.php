<?php
class BigBanner extends AppModel {
	public $name = 'BigBanner';

	public $belongsTo = array(
		"LargeArea" => array(
			'className' => 'LargeArea',
			'conditions' => '',
			'order' => '',
			'foreignKey' => 'large_area_id'
		),
	);
}
?>