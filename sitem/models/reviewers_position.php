<?php
class ReviewersPosition extends AppModel {
	var $name = 'ReviewersPosition';
	var $useTable = false;

	public $validate = array(

	);

	// レビュワーの口コミ数を集計する
	function index()
	{
		// 集計テーブルをdelete
		$sql = ' DELETE FROM reviewer_counts';
		$this->query($sql);

		// レビューの数を習得
		$sql = '';
		$sql .= ' SELECT reviews.reviewer_id, count(reviews.id) AS cnt';
		$sql .= ' FROM reviews';
		$sql .= ' WHERE reviews.is_publish = 1';
		$sql .= ' AND reviews.not_publish = 0';
		$sql .= ' AND reviews.delete_flag = 0';
		$sql .= ' GROUP BY reviews.reviewer_id';

		$data = $this->query($sql);

		// 口コミ集計数テーブルにINSERT
		$values_arr = array();
		$sql = '';
		$sql .= ' INSERT INTO reviewer_counts';
		$sql .= ' (reviewer_id, reviews_count, created, modified)';
		$sql .= ' VALUES ';
		foreach ($data as $key => $val)
		{
			$values_arr[$key] = ' (';
			$values_arr[$key] .= ' "' . $val['reviews']['reviewer_id'] . '",';
			$values_arr[$key] .= ' "' . $val[0]['cnt'] . '",';
			$values_arr[$key] .= ' now(),';
			$values_arr[$key] .= ' now()';
			$values_arr[$key] .= ' )';
		}

		$sql .= implode(',', $values_arr);
		$this->query($sql);
	}

}
?>
