<?php

class User extends AppModel {

	var $name = 'User';

	var $hasOne = array(
		'UsersAuthority' => array(
			'className' => 'UsersAuthority',
			'type' => 'inner',
			'foreignKey' => '',
			'conditions' => array('UsersAuthority.user_id = User.id'),
			'fields' => '',
			'order' => ''
		),
	);

}