<?php
class TestBoards extends AppModel {
	var $name = 'TestBoards';
/*
	var $hasOne = array(
			'LargeArea' => array(
					'className' => 'LargeArea',
					'foreignKey' => '',
					'conditions' => array('Board.large_area_id = LargeArea.id'),
					'fields' => '',
					'order' => ''
			),
	);

	var $hasMany = array(
			'BoardRes' => array(
					'className' => 'BoardRes',
					'foreignKey' => '',
					'conditions' => '',
					'foreignKey' => 'board_id',
					'fields' => '',
					'order' => 'sub_id asc'
			),
	);
*/

	// バリデーションチェック
	public $validate = array(
		'name' => array(
			'rule1' => array(
				// 空白は拒否
				'rule' => 'notEmpty',
				'message' => 'お名前を入力してください',
			),
			'rule2' => array(
				'rule' => array('maxLength',255),
				'message'=>'255文字以下で入力してください。',
			),
		),
		'comment' => array(
			'rule1' => array(
				// 空白は拒否
				'rule' => 'notEmpty',
				'message' => '内容を入力してください',
			),
		),
	);

}
?>
