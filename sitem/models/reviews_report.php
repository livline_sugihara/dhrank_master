<?php
class ReviewsReport extends AppModel {
	var $name = 'ReviewsReport';

	var $belongsTo = array(
		'Reviewer' => array(
			'className' => 'Reviewer',
			'foreignKey' => '',
			'conditions' => array('Reviewer.id = ReviewsReport.report_reviewer_id'),
			'fields' => '',
			'order' => ''
		),
		'Review' => array(
			'className' => 'Review',
			'foreignKey' => '',
			'conditions' => array('Review.id = ReviewsReport.report_review_id'),
			'fields' => '',
			'order' => ''
		),
		'MReviewsReportsType' => array(
			'className' => 'MReviewsReportsType',
			'foreignKey' => '',
			'conditions' => array('MReviewsReportsType.id = ReviewsReport.report_type_id'),
			'fields' => '',
			'order' => ''
		),
	);

	public $validate = array(
			'report_reviewer_id'=>array(
				array('rule' => 'notEmpty','message'=>'レビュアーIDを入力してください。'),
			),
			'report_review_id'=>array(
				array('rule' => 'notEmpty','message'=>'レビューIDを入力してください。'),
			),
			'report_type_id'=>array(
				array('rule' => 'notEmpty','message'=>'通報の種類を選択してください。'),
			),
	);
}
?>