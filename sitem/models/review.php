<?php
class Review extends AppModel {
	var $name = 'Review';

	public static $appointed_type = array(
		1 => '新規',
		2 => 'リピート',
	);

	public static $appointed_sub = array(
		1 => '本指名',
		2 => '写真指名',
		3 => 'フリー',
		4 => 'おすすめ',
	);

	public static $score = array(
		1 => '★☆☆☆☆ 1点',
		2 => '★★☆☆☆ 2点',
		3 => '★★★☆☆ 3点',
		4 => '★★★★☆ 4点',
		5 => '★★★★★ 5点',
	);

	/** 削除フラグ(削除なし) */
	const DELETE_FLAG_NONE = 0;
	/** 削除フラグ(コメントのみ削除) */
	const DELETE_FLAG_COMMENT_ONLY = 1;
	/** 削除フラグ(点数のみ残す) */
	const DELETE_FLAG_SAVE_POINTS = 2;
	/** 削除フラグ(全削除) */
	const DELETE_FLAG_ALL = 3;

	var $hasOne = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => '',
			'conditions' => array('User.id = Review.user_id'),
			'fields' => '',
			'order' => ''
		),
		'LargeArea' => array(
			'className' => 'LargeArea',
			'type' => 'inner',
			'foreignKey' => '',
			'conditions' => array('User.large_area_id = LargeArea.id'),
			'fields' => '',
			'order' => ''
		),
		'Shop' => array(
			'className' => 'Shop',
			'foreignKey' => '',
			'conditions' => array('Shop.user_id = User.id'),
			'fields' => '',
			'order' => ''
		),
		'Girl' => array(
			'className' => 'Girl',
			'foreignKey' => '',
			'conditions' => array('Girl.id = Review.girl_id'),
			'fields' => '',
			'order' => ''
		),
		'MReviewersAge' => array(
			'className' => 'MReviewersAge',
			'foreignKey' => '',
			'conditions' => array('MReviewersAge.id = Review.reviewer_age_id'),
			'fields' => '',
			'order' => ''
		),
		'MReviewsPlace' => array(
			'className' => 'MReviewsPlace',
			'foreignKey' => '',
			'conditions' => array('MReviewsPlace.id = Review.using_place_id'),
			'fields' => '',
			'order' => ''
		),
		'Reviewer' => array(
			'className' => 'Reviewer',
			'foreignKey' => '',
			'conditions' => array('Review.reviewer_id = Reviewer.id'),
			'fields' => '',
			'order' => ''
		),
	);

	var $validate = array(
		'shop_name' => array(
			array('rule' => array('isNoTag','shop_name'),'message'=>'タグは入力できません。'),
			array('rule' => 'notEmpty','message'=>'選択してください。'),
			array('rule' => array('maxLength', 120),'message'=>'120文字以下で入力してください。')
		),
		'girl_name' => array(
			array('rule' => array('isNoTag','girl_name'),'message'=>'タグは入力できません。'),
			array('rule' => 'notEmpty','message'=>'選択してください。'),
			array('rule' => array('maxLength', 120),'message'=>'120文字以下で入力してください。')
		),
		'course_minute' => array(
			array('rule' => 'notEmpty','message'=>'分を選択してください。'),
		),
		'course_cost' => array(
			array('rule' => 'notEmpty','message'=>'金額を選択してください。'),
		),
		'using_place_id' => array(
			array('rule' => 'notEmpty','message'=>'選択してください。'),
		),
		'appointed_type' => array(
			array('rule' => 'notEmpty','message'=>'指名回数(新規／リピート)を選択してください。'),
		),
		'appointed_sub' => array(
			array('rule' => 'notEmpty','message'=>'指名種類を選択してください。'),
			array('rule' => 'appointedRule', 'message' => '新規の場合には「本指名」は選択できません。')
		),
		'score_girl_looks' => array(
			array('rule' => 'notEmpty','message'=>'選択してください。'),
		),
		'score_girl_service' => array(
			array('rule' => 'notEmpty','message'=>'選択してください。'),
		),
		'score_girl_character' => array(
			array('rule' => 'notEmpty','message'=>'選択してください。'),
		),
		'comment'=>array(
			array('rule' => array('isNoTag','comment'),'message'=>'タグは入力できません。'),
			array('rule' => 'notEmpty','message'=>'入力してください。'),
			array('rule' => array('maxLength', 1024),'message'=>'1024文字以下で入力してください。')
		),
		'post_title'=>array(
			array('rule' => array('isNoTag','post_title'),'message'=>'タグは入力できません。'),
			array('rule' => 'notEmpty','message'=>'入力してください。'),
			array('rule' => array('maxLength', 120),'message'=>'120文字以下で入力してください。')
		),
	);

	function appointedRule($data) {
		// 新規の場合
		if($this->data['Review']['appointed_type'] == 1 && $this->data['Review']['appointed_sub'] == 1) {
			return false;
		}

		return true;
	}

	// 口コミを削除する
	function delete($review_id)
	{
		$this->save(array(
		    'id'=> $review_id,
			'is_publish'=>0, // 公開フラグを０
		    'delete_flag'=>1,// 削除フラグを１
		));

	}
}
?>
