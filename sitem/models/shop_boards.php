<?php
class ShopBoards extends AppModel {
	var $name = 'ShopBoards';

	var $hasOne = array(
			'User' => array(
					'className' => 'User',
					'foreignKey' => '',
					'conditions' => array('User.id = ShopBoards.user_id'),
					'fields' => '',
					'order' => ''
			),
	);


	// バリデーションチェック
	public $validate = array(
		'name' => array(
			'rule1' => array(
				// 空白は拒否
				'rule' => 'notEmpty',
				'message' => 'お名前を入力してください',
			),
			'rule2' => array(
				'rule' => array('maxLength',255),
				'message'=>'255文字以下で入力してください。',
			),
		),
		'comment' => array(
			'rule1' => array(
				// 空白は拒否
				'rule' => 'notEmpty',
				'message' => '内容を入力してください',
			),
		),
	);

}
?>
