<?php
class BoardRes extends AppModel {
	var $name = 'BoardRes';

	public $validate = array(
			'name'=>array(
					//start---2013/3/5 障害No.2-0019修正
					array('rule' => array('isNoTag','name'),'message'=>'タグは入力できません。'),
					//end---2013/3/5 障害No.2-0019修正
					array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			),
			'comment'=>array(
					//start---2013/3/5 障害No.2-0019修正
					array('rule' => array('isNoTag','comment'),'message'=>'タグは入力できません。'),
					//end---2013/3/5 障害No.2-0019修正
					array('rule' => 'notEmpty','message'=>'コメントを記入してください。'),
					array('rule' => array('maxLength',1024),'message'=>'1024文字以下で入力してください。')
			),
	);


}
?>