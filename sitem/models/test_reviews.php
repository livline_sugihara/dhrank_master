<?php
class TestReviews extends AppModel {
	var $name = 'TestReviews';
	var $useTable = false;

	// あいうえおで検索
	function getKanaLine($id)
	{
		// ひらがなで、何行かチェック
		$kana_line = ''; // かな行
		switch($id)
		{
			case '1': $kana_line = 'あ'; $kana_where = '"あ","い","う","え","お"'; break;
			case '2': $kana_line = 'か'; $kana_where = '"か","き","く","け","こ","が","ぎ","ぐ","げ","ご"'; break;
			case '3': $kana_line = 'さ'; $kana_where = '"さ","し","す","せ","そ","ざ","じ","ず","ぜ","ぞ"'; break;
			case '4': $kana_line = 'た'; $kana_where = '"た","ち","つ","て","と","だ","ぢ","づ","で","ど"'; break;
			case '5': $kana_line = 'な'; $kana_where = '"な","に","ぬ","ね","の"'; break;
			case '6': $kana_line = 'は'; $kana_where = '"は","ひ","ふ","へ","ほ","ば","び","ぶ","べ","ぼ","ぱ","ぴ","ぷ","ぺ","ぽ"'; break;
			case '7': $kana_line = 'ま'; $kana_where = '"ま","み","む","め","も"'; break;
			case '8': $kana_line = 'や'; $kana_where = '"や","ゆ","よ"'; break;
			case '9': $kana_line = 'ら'; $kana_where = '"ら","り","る","れ","ろ"'; break;
			case '10': $kana_line = 'わ'; $kana_where = '"わ","を","ん"'; break;
		}

		// 店舗取得
		$sql = ' SELECT shops.name, shops.user_id, count(reviews.user_id) AS cnt';
		$sql .= ' FROM users';
		$sql .= ' INNER JOIN shops';
		$sql .= ' ON users.id = shops.user_id';
		$sql .= ' LEFT JOIN reviews';
		$sql .= ' ON users.id = reviews.user_id';
		$sql .= ' WHERE is_created = 1';
		$sql .= ' AND LEFT(users.kana, 1) IN ('. $kana_where . ')';
		$sql .= ' AND shops.modified IS NOT NULL';
		$sql .= ' GROUP BY reviews.user_id';
		$sql .= ' ORDER BY users.kana ASC';

		$shops_data['shop_datas'] = $this->query($sql);
		$shops_data['kana_line'] = $kana_line;

		// 口コミ件数入れる
		foreach($shops_data['shop_datas'] as $key => $val){
			$shops_data['shop_datas'][$key]['shops']['cnt'] = $val[0]['cnt'];
		}

		return $shops_data;
	}

	// 非掲載（通報）の口コミを取得
	function getReasonData()
	{
		$sql =  ' SELECT reviews.id, reviews.comment, reviews_reports.comment, m_reviews_reports_types.name, reviews.created';
		$sql .= ' FROM reviews' ;
		$sql .= ' INNER JOIN reviews_reports';
		$sql .= ' ON reviews_reports.report_review_id = reviews.id';
		$sql .= ' LEFT JOIN m_reviews_reports_types';
		$sql .= ' ON m_reviews_reports_types.id = reviews_reports.report_type_id';
		$sql .= ' WHERE reviews.not_publish = 1';
		$sql .= ' ORDER BY reviews.id ASC';

		$datas = $this->query($sql);

		return $datas;
	}

	// 非掲載用口コミページネーション
	function reviews_paginate(){
		$extra = func_get_arg(6);
		var_dump($extra);
	}

	// 女の子のおすすめ内容のマスタデータ取得
	function getMGirlsRecommendTypesData()
	{
		$sql = ' SELECT id, name';
		$sql .= ' FROM m_girls_recommend_types';
		$datas = $this->query($sql);
		return $datas;
	}

}
?>
