<?php
if (!isset($_SESSION)) {
    if (function_exists('ini_set')) {
		if(defined('i')){ //ケータイ用だったら
            ini_set('session.use_trans_sid', 1); //GETリクエストにセッションIDを自動的に埋め込む設定を有効
            ini_set('session.use_cookies', 0);   //Cookieの使用を無効
        }else{
            ini_set('session.use_trans_sid', 0);  //無効
            ini_set('session.use_cookies', 1);    //有効
        }
    }
    ini_set('session.referer_check', '');
    ini_set('session.use_only_cookies', 0);
}
?>