<?php
//画像論理パス
//define('APP_IMG_URL','http://img.dhrank.com/');
define('APP_IMG_URL','http://img.19800210.com/');

//店舗管理システム
//define('APP_MEMBER_URL','http://member.dhrank.com/');
define('APP_MEMBER_URL','http://member.19800210.com/');
//全国版
//define('APP_ALLAREA_URL','http://dhrank.com/');
define('APP_ALLAREA_URL','http://www.19800210.com/');

Configure::write('change', array(
1 => '不可',
2 => '無料',
3 => '有料',
));

Configure::write('cancel', array(
1 => '不可',
2 => '無料',
3 => '有料',
));

Configure::write('receipt', array(
1 => '発行可',
2 => '発行不可',
));

Configure::write('sex', array(
1 => '男性',
2 => '女性',
));

Configure::write('m_score_i', array(
5 => '5点',
4 => '4点',
3 => '3点',
2 => '2点',
1 => '1点',
));

Configure::write('m_score_s', array(
5 => '笙･笙･笙･笙･笙･　5点',
4 => '笙･笙･笙･笙･笙｡　4点',
3 => '笙･笙･笙･笙｡笙｡　3点',
2 => '笙･笙･笙｡笙｡笙｡　2点',
1 => '笙･笙｡笙｡笙｡笙｡　1点',
));

Configure::write('weekday', array( "日", "月", "火", "水", "木", "金", "土" ));

Configure::write('kana', array(
	1 => array('あ','い','う','え','お','ぁ','ぃ','ぅ','ぇ','ぉ',),
	2 => array('か','き','く','け','こ','が','ぎ','ぐ','げ','ご',),
	3 => array('さ','し','す','せ','そ','ざ','じ','ず','ぜ','ぞ',),
	4 => array('た','ち','つ','て','と','だ','ぢ','づ','で','ど','っ',),
	5 => array('な','に','ぬ','ね','の',),
	6 => array('は','ひ','ふ','へ','ほ','ば','び','ぶ','べ','ぼ','ぱ','ぴ','ぷ','ぺ','ぽ',),
	7 => array('ま','み','む','め','も',),
	8 => array('や','ゆ','よ','ゃ','ゅ','ょ',),
	9 => array('ら','り','る','れ','ろ',),
	10 => array('わ','を','ん','ゎ',),
));

// 正の整数
define('VALID_POSITIVE_NUMBER', '/^[0-9]+$/');
// 半角英数記号
define('VALID_HALF_ALPHANUMERICCODE', '/^[!-~]+$/');
// 全角ひらがな
define('VALID_HIRAGANA','/^(?:\xE3\x81[\x81-\xBF]|\xE3\x82[\x80-\x93])+$/');
// 電話番号
//start---2012/4/6障害No.3-0012修正
//define('VALID_PHONE','/0\d{1,4}-\d{1,4}-\d{4}/');
define('VALID_PHONE','/0\d{1,4}-\d{1,4}-\d{3,4}/');
//end---2012/4/6障害No.3-0012修正
// メールアドレス
define('VALID_EMAIL','/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/');



//画像定義
//バナー
define('APP_IMG_BANNER_WIDTH',200);
define('APP_IMG_BANNER_HEIGHT',40);
//女の子画像
define('APP_IMG_RANK_WIDTH',120);
define('APP_IMG_RANK_HEIGHT',160);
//女の子画像L
define('APP_IMG_GIRL_L_WIDTH',240);
define('APP_IMG_GIRL_L_HEIGHT',320);
//女の子画像M
define('APP_IMG_GIRL_M_WIDTH',120);
define('APP_IMG_GIRL_M_HEIGHT',160);
//女の子画像S
define('APP_IMG_GIRL_S_WIDTH',60);
define('APP_IMG_GIRL_S_HEIGHT',80);

//-----
// 店舗アイコン画像
define('APP_IMG_SHOP_ICON_WIDTH', 200);
define('APP_IMG_SHOP_ICON_HEIGHT', 200);
// おすすめ店舗画像
define('APP_IMG_SHOP_RECOMMEND_WIDTH', 291);
define('APP_IMG_SHOP_RECOMMEND_HEIGHT', 86);
// 店舗リスト画像(PC)
define('APP_IMG_SHOP_LIST_WIDTH', 300);
define('APP_IMG_SHOP_LIST_HEIGHT', 400);
// 店舗ヘッダー(PC)
define('APP_IMG_SHOP_HEADER_WIDTH', 1350);
define('APP_IMG_SHOP_HEADER_HEIGHT', 0);
// 店舗紹介画像
define('APP_IMG_SHOP_PROFILE_WIDTH', 350);
define('APP_IMG_SHOP_PROFILE_HEIGHT', 240);
// 店舗ヘッダー(SP)
define('APP_IMG_SHOP_HEADER_SP_WIDTH', 880);
define('APP_IMG_SHOP_HEADER_SP_HEIGHT', 0);
// 店舗リスト画像(SP)
define('APP_IMG_SHOP_LIST_SP_WIDTH', 528);
define('APP_IMG_SHOP_LIST_SP_HEIGHT', 380);

//テキスト抜粋
//TOP->口コミ
define('APP_CSIZE_TOP_REVIEW_COMMENT',72);
define('APP_CSIZE_TOP_REVIEW_COMMENT_I',30);
define('APP_CSIZE_TOP_REVIEW_COMMENT_S',30);
//店舗TOP->レビュー
define('APP_CSIZE_SHOPS_TOP_REVIEW_COMMENT',72);
define('APP_CSIZE_SHOPS_TOP_REVIEW_COMMENT_I',30);
define('APP_CSIZE_SHOPS_TOP_REVIEW_COMMENT_S',30);
//店舗別女の子->レビュー
define('APP_CSIZE_SHOPS_GIRL_REVIEW_COMMENT',48);
define('APP_CSIZE_SHOPS_GIRL_REVIEW_COMMENT_I',30);
define('APP_CSIZE_SHOPS_GIRL_REVIEW_COMMENT_S',30);
//投稿者別レビュー->レビュー
define('APP_CSIZE_REVIEWS_LIST_POST_COMMENT',48);
define('APP_CSIZE_REVIEWS_LIST_POST_COMMENT_I',30);
define('APP_CSIZE_REVIEWS_LIST_POST_COMMENT_S',30);
//レビュアー管理システム内口コミ一覧->レビュー
define('APP_CSIZE_REVIEWERS_REVIEWS_COMMENT',48);
define('APP_CSIZE_REVIEWERS_REVIEWS_COMMENT_I',30);

//レビュアー登録URL送信
define('VALID_REGISTER_URLSEND_EMAIL','register@dhrank.com');
Configure::write('smtpOptionRegisterUrlsend', array(
		'port'=>'587',
 		'host' => 'dhrank.com',
 		'username'=>'register',
 		'password'=>'lCO4EiGy',
));

//レビュアー登録完了送信
define('VALID_REGISTER_END_EMAIL','register@dhrank.com');

Configure::write('smtpOptionRegisterEnd', array(
 		'port'=>'587',
 		'host' => 'dhrank.com',
 		'username'=>'register',
 		'password'=>'lCO4EiGy',
));

//ログインID送信
define('VALID_LOGINID_SEND_EMAIL','sendid@dhrank.com');

Configure::write('smtpOptionLoginidSend', array(
 		'port'=>'587',
 		'host' => 'dhrank.com',
 		'username'=>'sendid',
 		'password'=>'lCO4EiGy',
));

//メールアドレス変更URL送信
define('VALID_CHANGEEMAIL_URLSEND_EMAIL','changeemail@dhrank.com');

Configure::write('smtpOptionChangeemailUrlsend', array(
 		'port'=>'587',
 		'host' => 'dhrank.com',
 		'username'=>'changeemail',
 		'password'=>'lCO4EiGy',
));

//パスワード変更URL送信
define('VALID_REVIEWER_CHANGEPASSWORD_URLSEND_EMAIL','reminder@dhrank.com');

Configure::write('smtpOptionReviewerChangepasswordUrlsend', array(
 		'port'=>'587',
 		'host' => 'dhrank.com',
 		'username'=>'reminder',
 		'password'=>'lCO4EiGy',
));

//パスワード変更完了送信
define('VALID_REVIEWER_CHANGEPASSWORD_END_EMAIL','reminder@dhrank.com');

Configure::write('smtpOptionReviewerChangepasswordEnd', array(
 		'port'=>'587',
 		'host' => 'dhrank.com',
 		'username'=>'reminder',
 		'password'=>'lCO4EiGy',
));

//お問い合わせ送信
define('VALID_CONTACTS_EMAIL','contact@dhrank.com');

Configure::write('smtpOptionContacts', array(
 		'port'=>'587',
 		'host' => 'dhrank.com',
 		'username'=>'contact',
 		'password'=>'lCO4EiGy',
));

// レビュー投稿通知
define('VALID_POST_REVIEW','contact@dhrank.com');
Configure::write('smtpOptionPostReview', array(
 		'port'=>'587',
 		'host' => 'dhrank.com',
 		'username'=>'contact',
 		'password'=>'lCO4EiGy',
));

// レビュー不正通報
define('VALID_REVIEW_REPORT','contact@dhrank.com');
Configure::write('smtpOptionReviewReport', array(
 		'port'=>'587',
 		'host' => 'dhrank.com',
 		'username'=>'contact',
 		'password'=>'lCO4EiGy',
));

//ページング取得件数定義
//新着情報
define('APP_PCOUNT_NEWS_LIST',1000);
define('APP_PCOUNT_NEWS_LIST_I',20);
define('APP_PCOUNT_NEWS_LIST_S',100);
//TOP口コミ表示件数
define('APP_PCOUNT_TOP_REVIEWS',10);
define('APP_PCOUNT_TOP_REVIEWS_I',10);
define('APP_PCOUNT_TOP_REVIEWS_S',10);
//TOPランキング表示件数
define('APP_PCOUNT_TOP_RANKINGS',3);
//ランキング表示件数
define('APP_PCOUNT_RANKINGS',25);
define('APP_PCOUNT_RANKINGS_I',25);
define('APP_PCOUNT_RANKINGS_S',25);
//投稿者別口コミ一覧表示件数
define('APP_PCOUNT_REVIEWS_LIST_POST',5);
define('APP_PCOUNT_REVIEWS_LIST_POST_I',5);
define('APP_PCOUNT_REVIEWS_LIST_POST_S',5);
//店舗TOP口コミ一覧表示件数
define('APP_PCOUNT_USERS_TOP_REVIEWS',10);
define('APP_PCOUNT_USERS_TOP_REVIEWS_I',10);
define('APP_PCOUNT_USERS_TOP_REVIEWS_S',10);
//店舗別女の子一覧表示件数
define('APP_PCOUNT_USERS_GIRLS',1000);
define('APP_PCOUNT_USERS_GIRLS_I',1000);
define('APP_PCOUNT_USERS_GIRLS_S',1000);
//掲示板TOPスレッド一覧
define('APP_PCOUNT_BOARDS_LIST',30);
//掲示板TOPスレッド詳細
define('APP_PCOUNT_BOARDS_DETAIL',10);
define('APP_PCOUNT_BOARDS_DETAIL_I',10);
define('APP_PCOUNT_BOARDS_DETAIL_S',10);
//掲示板TOPスレッドレス一覧
define('APP_PCOUNT_BOARDS_DETAIL_RES',10);
//掲示板内容表示画面レス一覧
define('APP_PCOUNT_BOARDS_VIEW_RES_I',10);
define('APP_PCOUNT_BOARDS_VIEW_RES_S',10);

//投票ボタン指定時間制御
define('APP_HOUR_GOODCOUNT_ENTRY',24);

// ランキング統計の対象レビュー範囲
// define('APP_SUMMARY_REVIEW_RANGE', '1 month');
define('APP_SUMMARY_REVIEW_RANGE', '15 day');

// レビューの自動公開
define('APP_REVIEW_AUTO_PUBLISH', false);
?>
