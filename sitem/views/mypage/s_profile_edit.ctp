	<div class="label"><h2>会員情報の確認・変更</h2></div>
	<section id="account_form">

<?php echo $form->create(null,array('type'=>'post','action'=> '')); ?>
<?php echo $form->hidden('Reviewer.id'); ?>

		<dl>
			<dt>年代</dt>
			<dd>
                        <?php echo $form->select('Reviewer.age_id', $m_reviewers_ages, null,array('empty'=>false)); ?>
                        <?php echo $form->error('Reviewer.age_id'); ?>
			</dd>
			<dt>職業</dt>
			<dd>
                        <?php echo $form->select('Reviewer.job_id', $m_reviewers_jobs, null,array('empty'=>false)); ?>
                        <?php echo $form->error('Reviewer.job_id'); ?>

			</dd>
			<dt>ニックネーム</dt>
			<dd>
			<?php echo $form->text('Reviewer.handle',array('style' => 'width:200px')); ?>
			<?php echo $form->error('Reviewer.handle'); ?>
			</dd>
			<dt>地域</dt>
			<dd>
                        <?php echo $form->select('Reviewer.large_area_id', $large_areas, null,array('empty'=>false)); ?>
                        <?php echo $form->error('Reviewer.large_area_id'); ?>
			<dt>よく使用するカテゴリー</dt>
			<dd>
                        <?php echo $form->select('Reviewer.favorite_category_id', $m_shops_business_categories, null,array('empty'=>false)); ?>
                        <?php echo $form->error('Reviewer.favorite_category_id'); ?>
            </dd>			<dt>役職</dt>
			<dd>
				<?php echo $reviewers_position; ?>

			</dd>

			<dt>自己紹介</dt>
			<dd>
                        <?php echo $form->textarea('Reviewer.comment', array('style' => 'width:450px;height:90px;')); ?>
                        <?php echo $form->error('Reviewer.comment'); ?>
			<dd>
			<dt>プロフィール画像変更</dt>

<div style="margin-top:10px;">

<?php
for($ii = 1; $ii <= 10; $ii++) {
    $selected = $this->data['Reviewer']['avatar_id'] == $ii ? 'checked="checked"' : '';
    if($ii % 5 == 1) {
?>
<?php } ?>
<div style="float:left;text-align:center;margin:2px;">
                        <img src="/img/tab_topictoukou_img<?php echo $ii; ?>.png" width="100px" /><br />
                        <label><input type="radio" name="data[Reviewer][avatar_id]" value="<?php echo $ii; ?>" <?php echo $selected; ?></label></div>
<?php
}
?>


</div>

            <p class="align_c" style="margin-top: 50px">
                <?php echo $form->submit('編集する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'confirm'));?>
            </p>

		<?php echo $form->end(); ?>

    </div>
</section>

	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile($record = null)?>">My Top</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_top($record = null)?>">自分の更新</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_myreview($record = null)?>">Myレビュー</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_follow($record = null)?>">フォローしている</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_follower($record = null)?>">フォローされている</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_shop($record = null)?>">お気に入りのお店</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_girl($record = null)?>">お気に入りの女の子</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_review($record = null)?>">お気に入りのレビュー</a></p>
<!--	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile_edit($record = null)?>">会員情報の確認・変更</a></p> -->
