	<div class="label"><h2>Myレビュー編集</h2></div>
    
	<section id="postArea">
	
            <?php echo $form->create(null,array('type'=>'post','action'=> '')); ?>
                <?php echo $form->hidden('Review.id'); ?>

                <table>
                    <tr>
                        <th>店名</th>
                        <td>
							<?php echo $this->data['Review']['shop_name']; ?>
                        </td>
                    </tr>
                    <tr>
                        <th>女の子</th>
                        <td>
							<?php echo $this->data['Review']['girl_name']; ?>
                        </td>
                    </tr>
                    <tr>
                        <th>コース／料金</th>
                        <td>
<?php
$minites_list = array();
for($ii = 10; $ii <= 120; $ii += 5) { $minites_list[$ii] = $ii; }
$costs_list = array();
for($ii = 2000; $ii <= 100000; $ii += 1000) { $costs_list[$ii] = $ii; }
?>
                            <?php echo $form->select('Review.course_minute', $minites_list,null,array('empty'=>'---')); ?> 分
                            <?php echo $form->select('Review.course_cost', $costs_list,null,array('empty'=>'------')); ?> 円

                            <?php echo $form->error('Review.course_minute'); ?>
                            <?php echo $form->error('Review.course_cost'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ご利用場所</th>
                        <td>
                            <?php echo $form->select('Review.using_place_id', $m_reviews_place,null,array('empty'=>false)); ?>
                            <?php echo $form->error('Review.using_place_id'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>指名</th>
                        <td>
                            <?php echo $form->select('Review.appointed_type', $appointed_type,null,array('empty'=>false)); ?>
                            ・
                            <?php echo $form->select('Review.appointed_sub', $appointed_sub,null,array('empty'=>false)); ?>

                            <?php echo $form->error('Review.appointed_type'); ?>
                            <?php echo $form->error('Review.appointed_sub'); ?>
                        </td>
                    </tr>
                </table>
                </div>
                
                <div class="reviewFormRed">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <th>L(ルックス)</th>
                        <td>
                            <?php echo $form->select('Review.score_girl_looks', $score_list,null,array('empty'=>'採点して下さい')); ?>
                            <?php echo $form->error('Review.score_girl_looks'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>S(サービス)</th>
                        <td>
                            <?php echo $form->select('Review.score_girl_service', $score_list,null,array('empty'=>'採点して下さい')); ?>
                            <?php echo $form->error('Review.score_girl_service'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>C(性格)</th>
                        <td>
                            <?php echo $form->select('Review.score_girl_character', $score_list,null,array('empty'=>'採点して下さい')); ?>
                            <?php echo $form->error('Review.score_girl_character'); ?>
                        </td>
                    </tr>
                </table>
                </div>
                
                <div class="reviewForm">
                <table cellspacing="0" cellpadding="0" border="0" class="noLCS">
                    <tr>
                        <th>口コミ</th>
                        <td>
                            <?php echo $form->textarea('Review.comment',array('rows' => '20')); ?>
                            <?php echo $form->error('Review.comment'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>この女の子の良さを一言でいうと<br>(※タイトルとして表示されます）</th>
                        <td>
                            <?php echo $form->text('Review.post_title',array('size' => '30')); ?>
                            <?php echo $form->error('Review.post_title'); ?>
                        </td>
                    </tr>
                </table>

                <p class="align_c">
                    <?php echo $form->submit('入力内容を更新する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'register'));?>
                </p>
            <?php echo $form->end(); ?>

</section>

<script>
$(document).ready(function(){
    $('#ReviewAppointedType').change(function(e) {
        refreshAppointed();
        e.preventDefault();
    });

    // ロード時に一度実行
    refreshAppointed();
});

function refreshAppointed() {
    var appointed_type_id = $('#ReviewAppointedType').children(':selected').val();

    // 新規の場合
    if(appointed_type_id == 1) {
        $('#ReviewAppointedSub').children('[value=1]').removeAttr('selected').hide();
        $('#ReviewAppointedSub').children('[value=2]').attr('selected', 'selected');
    } else {
        $('#ReviewAppointedSub').children('[value=2]').removeAttr('selected');
        $('#ReviewAppointedSub').children('[value=1]').attr('selected', 'selected').show();
    }
}
</script>