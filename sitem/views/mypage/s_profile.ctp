	<div class="label"><h2>プロフィール</h2></div>
	<section id="account_form">
<!--     <div class="title_bar">プロフィール<span><a href="<?php echo $linkCommon->get_reviewer_profile_edit(); ?>">編集する</a></span></div> -->

<div style="text-align:center;margin:10px;padding:2px;">
    <?php echo $imgCommon->get_reviewer_avatar($parent_reviewer, array(100,100)); ?>
</div>

		<dl>
			<dt>年代</dt>
			<dd><?php echo $parent_reviewer['MReviewersAge']['name']; ?></dd>
			<dt>職業</dt>
			<dd><?php echo $parent_reviewer['MReviewersJob']['name']; ?></dd>
			<dt>ニックネーム</dt>
			<dd><?php echo $parent_reviewer['Reviewer']['handle']; ?></dd>
			<dt>地域</dt>
			<dd><?php echo $parent_reviewer['LargeArea']['name']; ?></dd>
			<dt>よく使用するカテゴリー</dt>
			<dd><?php echo $parent_reviewer['MShopsBusinessCategory']['name']; ?></dd>
			<dt>役職</dt>
			<dd><?php echo $reviewers_position; ?></dd>
			<dt>自己紹介</dt>
			<dd><?php echo $parent_reviewer['Reviewer']['comment']; ?></dd>
		</dl>
	</section>

<!--	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile($record = null)?>">My Top</a></p> -->
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_top($record = null)?>">自分の更新</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_myreview($record = null)?>">Myレビュー</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_follow($record = null)?>">フォローしている</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_follower($record = null)?>">フォローされている</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_shop($record = null)?>">お気に入りのお店</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_girl($record = null)?>">お気に入りの女の子</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_review($record = null)?>">お気に入りのレビュー</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile_edit($record = null)?>">会員情報の確認・変更</a></p>
