<section id="middle" class="two">
    <div class="title_bar">プロフィール編集</div>    <div class="inner profileTable">
        <?php echo $form->create(null,array('type'=>'post','action'=> '')); ?>
            <?php echo $form->hidden('Reviewer.id'); ?>
        	<h3>プロフィール編集</h3>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <th>年代</th>
                    <td>
                        <?php echo $form->select('Reviewer.age_id', $m_reviewers_ages, null,array('empty'=>false)); ?>
                        <?php echo $form->error('Reviewer.age_id'); ?>
                    </td>
                    <th>職業</th>
                    <td>
                        <?php echo $form->select('Reviewer.job_id', $m_reviewers_jobs, null,array('empty'=>false)); ?>
                        <?php echo $form->error('Reviewer.job_id'); ?>
                    </td>
                </tr>
                <tr>
                    <th>ニックネーム</th>
                    <td>
                        <?php echo $form->text('Reviewer.handle',array('style' => 'width:200px')); ?>
                        <?php echo $form->error('Reviewer.handle'); ?>
                    </td>
                    <th>地域</th>
                    <td>
                        <?php echo $form->select('Reviewer.large_area_id', $large_areas, null,array('empty'=>false)); ?>
                        <?php echo $form->error('Reviewer.large_area_id'); ?>
                    </td>
                </tr>
                <tr>
                    <th>よく利用する<br>カテゴリ</th>
                    <td>
                        <?php echo $form->select('Reviewer.favorite_category_id', $m_shops_business_categories, null,array('empty'=>false)); ?>
                        <?php echo $form->error('Reviewer.favorite_category_id'); ?>
					</td>
                </tr>
                <tr>
                    <th>自己紹介</th>
                    <td colspan="3">
                        <?php echo $form->textarea('Reviewer.comment', array('style' => 'width:450px;height:90px;')); ?>
                        <?php echo $form->error('Reviewer.comment'); ?>
                    </td>
                </tr>
            </table>
            <h3>プロフィール画像変更</h3>
            <table border="0" cellpadding="0" cellspacing="0">
<?php

for($ii = 1; $ii <= 10; $ii++) {
    $selected = $this->data['Reviewer']['avatar_id'] == $ii ? 'checked="checked"' : '';
    if($ii % 5 == 1) {
?>
                <tr>
<?php } ?>
                    <td align="center">
                        <img src="/img/tab_topictoukou_img<?php echo $ii; ?>.png" width="100px" /><br />
                        <label><input type="radio" name="data[Reviewer][avatar_id]" value="<?php echo $ii; ?>" <?php echo $selected; ?>>この画像にする</label>
                    </td>
<?php
    if($ii % 5 == 0) {
?>
                </tr>
<?php
    }
}
?>
            </table>

            <p class="align_c" style="margin-top: 50px">
                <?php echo $form->submit('編集する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'confirm'));?>
            </p>

		<?php echo $form->end(); ?>

    </div>
</section>
