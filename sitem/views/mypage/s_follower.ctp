	<div class="label"><h2>フォローされている</h2></div>	
		<section id="topic_shop_area">
<?php
	foreach($followed_reviewer AS $ii => $record) {
?>
		<div class="list">
			<div class="top_new_review_box clearfix">
				<p class="img"><a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>"><?php echo $imgCommon->get_reviewer_avatar($record, array(80,80),'img_frame',null,$record['Reviewer']['handle']); ?></a></p>
				<div class="summary">
					<p class="name"><?php echo $record['Reviewer']['handle']; ?></p>
					<div style="float:right;">
						<p class="css_btn_class followReviewer" data="<?php echo $record['Reviewer']['id']; ?>" mode="add">
							<a href="#" class="css_btn_class"><img src="../../../img/s/follow_btn.png" alt="フォローする" height="20"></a>
						</p>
					</div>
					<p class="option_follow">フォロワー<span class="num"><?php echo $record[0]['follower_cnt']; ?></span>
					<p class="option_review">レビュー投稿<?php echo $record[0]['review_cnt']; ?>件</p>
					<div class="clearfix">
						<p><span class="caption">よく利用するカテゴリ</span><span class="cat"><?php echo $record['MShopsBusinessCategory']['name']; ?></span></p>
					</div>
				</div>
			</div>
		</div>
<?php } ?>

</section>

	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile($record = null)?>">My Top</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_top($record = null)?>">自分の更新</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_myreview($record = null)?>">Myレビュー</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_follow($record = null)?>">フォローしている</a></p>
<!--	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_follower($record = null)?>">フォローされている</a></p> -->
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_shop($record = null)?>">お気に入りのお店</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_girl($record = null)?>">お気に入りの女の子</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_favorite_review($record = null)?>">お気に入りのレビュー</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile_edit($record = null)?>">会員情報の確認・変更</a></p>