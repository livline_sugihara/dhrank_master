<!-- middle -->
<section id="middle" class="two">
    <div class="title_bar">レビュー</div> 
    <div id="reviewListArea" class="inner">
    
        <div class="new_reviewArea clearfix">
            
<?php
$cnt = count($favorite_reviews);
for ($ii = 0; $ii < $cnt; $ii++) {
            echo $this->element('common/_review', array(
            	"record" => $favorite_reviews[$ii],
            	"reviewer" => true,
            ));
}     
?>
            
        </div>
                        
    </div>
</section>
<script>
function delete_favorite() {

    if(!confirm('レビューをお気に入りから外しても宜しいでしょうか？')) {
        return false;
    }

    // TODO 実装
}
</script>