<!-- 店舗トップ共通部分 -->
<?php echo $this->element('s_common/shop_top'); ?>
<!-- /店舗トップ共通部分 -->

	<div class="label2_2"><h3 class="label2_ttl"><?php echo $parent_shop['Shop']['name']; ?>の口コミ</h3></div>
	<div class="kensuu">
		<p>全<span class="font18"><strong><?php echo $paginator->counter(array('format' => '%count%')); ?></strong></span>件中 <span><?php echo $paginator->counter(array('format' => '%start%')); ?></span>~<span><?php echo $paginator->counter(array('format' => '%end%')); ?></span>件表示</p></div>

	<section id="pickup_box2">

<?php
	// 有料の場合のみ
if($parent_shop['UsersAuthority']['is_show_all_contents_by_shop'] == 1) {
?>
	<?php echo $this->element('s_common/shop_pic_review'); ?>
<?php
	}
?>
	</section>

<?php
$paginator->options(array('url' => array('user_id' => $this->params['user_id'])));
?>

<?php // echo $this->element('s_pagenate/common_pagenate'); ?>

<!-- レビュー -->
<?php echo $this->element('s_common/shop_reviews', array('new_shop_review' => $shop_review)); ?>
<!-- /レビュー -->

<?php  echo $this->element('s_pagenate/common_pagenate'); ?>

<!-- ライン・メール・電話 -->
<?php echo $this->element('s_common/line_mail'); ?>
<!-- /ライン・メール・電話 -->

<!-- 電話をする -->
<?php echo $this->element('s_common/shop_tel'); ?>
<!-- /電話をする -->

<!-- メニュー -->
<?php echo $this->element('s_common/shop_menu'); ?>
<!-- /メニュー -->

<?php //echo $this->element('pagenate/s_numbers'); ?>

<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../<?php echo $parent_shop['User']['id']; ?>" itemprop="url"><span itemprop="title"><?php echo $parent_shop['Shop']['name']; ?></span></a></li>
		
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./reviews" itemprop="url"><span itemprop="title">口コミ</a></li>
	</ul>
</nav>