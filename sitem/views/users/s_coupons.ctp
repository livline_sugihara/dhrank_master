
    <!-- 店舗トップ共通部分 -->
	<?php echo $this->element('s_common/shop_top'); ?>
	<!-- /店舗トップ共通部分 -->

<div class="label2_2"><h3 class="label2_ttl">お得なクーポン情報</h3></div>

        <!-- クーポン情報 -->
        <?php echo $this->element('s_common/shop_coupon'); ?>
        <!-- /クーポン情報 -->
        
	<!-- ライン・メール・電話 -->
	<?php echo $this->element('s_common/line_mail'); ?>
	<!-- /ライン・メール・電話 -->

	<!-- 電話をする -->
	<?php echo $this->element('s_common/shop_tel'); ?>
	<!-- /電話をする -->

	<!-- メニュー -->
	<?php echo $this->element('s_common/shop_menu'); ?>
	<!-- /メニュー -->

<?php echo $this->element('pagenate/s_numbers'); ?>


<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../<?php echo $parent_shop['User']['id']; ?>" itemprop="url"><span itemprop="title"><?php echo $parent_shop['Shop']['name']; ?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./coupons" itemprop="url"><span itemprop="title">クーポン</a></li>
	</ul>
</nav>