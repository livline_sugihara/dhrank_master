<section id="middle" class="wide">

    <!-- 店舗トップ共通部分 -->
    <?php echo $this->element('common/shop_top'); ?>
    <!-- /店舗トップ共通部分 -->

    <!-- tabInner -->
    <div class="tabInner">
    
<?php
if(count($girls) > 0) {
?>
        <div class="pageCounter">
        	<span class="num"><?php echo $count_girls; ?>人</span><span>の在籍者がいます。</span>
        <div>
        <p class="clearLine"></p>
        <div class="girlsArea clearfix">

<?php
	foreach($girls AS $key => $record) {
?>
            <div>
                <p class="img">
                	<a href="<?php echo $linkCommon->get_user_girl($record); ?>">
                		<?php echo $imgCommon->get_girl_with_time($record, array(75, 75)); ?>
                	</a>
                </p>
                <p class="name"><a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo $record['Girl']['name']; ?></a></p>
                <p class="review">
<?php if($record[0]['count'] > 0) { ?>
                	<img src="/img/icon_review.jpg">
<?php } ?>
                	クチコミ <?php echo $record[0]['count']; ?>件
                </p>
               <!-- <p class="whiteBtn btn"><a href="#">お気に入りに保存</a></p>	-->

<?php if($review_flag) { ?>
        <p class="whiteBtn btn bookmarkGirl" data="<?php echo $record['Girl']['id']; ?>" mode="delete"><a href="#">お気に入りに解除</a></p>
<?php } else { ?>
        <p class="whiteBtn btn bookmarkGirl" data="<?php echo $record['Girl']['id']; ?>" mode="add"><a href="#">お気に入りに保存</a></p>
<?php } ?>

            </div>
<?php
	}
?>
            <p class="clearLine"></p>
        </div>
<?php
} else {
?>
        <div class="pageCounter">
        	在籍者が登録されていません
        <div>
        <p class="clearLine"></p>
<?php
}
?>

    </div>
    <!-- /tabInner -->

</section>

<section id="side" class="narrow">
    <?php echo $this->element('common/side_narrow'); ?>
</section>

<?php // echo $this->element('pagenate/numbers'); ?>