<section id="middle" class="wide">

    <!-- 店舗トップ共通部分 -->
    <?php echo $this->element('common/shop_top'); ?>
    <!-- /店舗トップ共通部分 -->

    <!-- tabInner -->
    <div class="tabInner">
			
		<!-- PickUp -->        <?php echo $this->element('common/shop_pic_review'); ?>
		<!-- /PickUp -->

        <!-- レビュー -->
        <?php echo $this->element('common/shop_reviews', array('new_shop_review' => $shop_review)); ?>
        <!-- /レビュー -->

	</div>
    <!-- /tabInner -->
    
</section>

<section id="side" class="narrow">
    <?php echo $this->element('common/side_narrow'); ?>
</section>