<script>
function push_button(button)
{
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("button_pusher").innerHTML=xmlhttp.responseText;
	document.getElementById("shop_img").innerHTML="";

	if (button == '1') {
//		document.getElementById("shop_img").innerHTML="<img src=\"http://img.dhrank.com/girl/<?php echo $girl['User']['id']?>/<?php echo $girl['Girl']['id']?>/1_l.jpg\" name=\"ph01\" alt=\"\" height=\"320\" width=\"240\">";
		document.getElementById("shop_img").innerHTML='<?php echo $imgCommon->get_girl_with_time($girl, array(240,320), 1, null, null, 'ph01'); ?>';
	}
	if (button == '2') {
//		document.getElementById("shop_img").innerHTML="<img src=\"http://img.dhrank.com/girl/<?php echo $girl['User']['id']?>/<?php echo $girl['Girl']['id']?>/2_l.jpg\" name=\"ph01\" alt=\"\" height=\"320\" width=\"240\">";
		document.getElementById("shop_img").innerHTML='<?php echo $imgCommon->get_girl_with_time($girl, array(240,320), 2, null, null, 'ph02'); ?>';
	}
	if (button == '3') {
//		document.getElementById("shop_img").innerHTML="<img src=\"http://img.dhrank.com/girl/<?php echo $girl['User']['id']?>/<?php echo $girl['Girl']['id']?>/3_l.jpg\" name=\"ph01\" alt=\"\" height=\"320\" width=\"240\">";
		document.getElementById("shop_img").innerHTML='<?php echo $imgCommon->get_girl_with_time($girl, array(240,320), 3, null, null, 'ph03'); ?>';
		}

    }
  }

xmlhttp.open("GET","<?php echo (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"];?>/img/s/button_change.php?button="+ button ,true);
xmlhttp.send();
}
</script>

<!-- 店舗トップ共通部分 -->
<?php echo $this->element('s_common/shop_top'); ?>
<!-- /店舗トップ共通部分 -->

	<section id="shop_form">
		<div class="label2_2"><h3 class="label2_ttl"><?php echo $girl['Girl']['name'] ?></h3></div>

<p id="shop_img" class="center"><?php echo $imgCommon->get_girl_with_time($girl, array(240,320), 1, null, null, 'ph01'); ?></p>

<div id="button_pusher">
	<div class="photo_number2 clearfix center">
		<a href="javascript:void(0);" class="btns2_push" id="button1" onclick="push_button('1'); return false">First Sheet</a>
    	<a href="javascript:void(0);" class="btns2" id="button2" onclick="push_button('2'); return false">2nd</a>
    	<a href="javascript:void(0);" class="btns2" id="button3" onclick="push_button('3'); return false">3rd</a>
	</div>
</div>


<?php if(isset($review_flag) && $review_flag) { ?>
                <p class="btn_gr bookmarkGirl" data="<?php echo $girl['Girl']['id']; ?>" mode="delete" style="margin-bottom:10px"><a href="#">お気に入りに解除</a></p>
<?php } else { ?>
                <p class="btn_gr bookmarkGirl" data="<?php echo $girl['Girl']['id']; ?>" mode="add" style="margin-bottom:10px"><a href="#">お気に入りの女の子に追加</a></p>
<?php } ?>
		<!-- p class="btn_gr" style="margin-bottom:10px"><a href="#">お気に入りの女の子に追加</a></p -->

		<h3 class="label_3">プロフィール</h3>
		<table>
			<tr>
				<th>名前</th>
				<td><?php echo $girl['Girl']['name']?></td>
			</tr>
			<tr>
				<th>年齢</th>
				<td><?php echo $girl['Girl']['age']?>歳</td>
			</tr>
		</table>

		<p class="btn_pi"><a href="<?php echo $linkCommon->s_get_post_review($girl['Shop']['id'], $girl['Girl']['id']); ?>">口コミを投稿する</a></p>

		<h3 class="label_3">スタイル</h3>
		<table>
			<tr>
				<th>身長</th>
				<td><?php echo $girl['Girl']['body_height']?>cm</td>
			</tr>
			<tr>
				<th>3サイズ</th>
				<td><?php echo $textCommon->get_3size_on_profile($girl)?></td>
			</tr>
		</table>

		<h3 class="label_3">店長からのコメント</h3>
		<p class="txt"><?php echo $girl['Girl']['owner_comment']?></p>

		<h3 class="label">新着口コミ(<?php echo $girl['Girl']['name']?>)</h3>

<?php
$paginator->options(array('url' => array(
	'user_id' => $this->params['user_id'],
	'girl_id' => $this->params['girl_id'],
)));
?>

<?php // echo $this->element('s_pagenate/common_pagenate'); ?>

        <!-- 新着レビュー -->
        <?php echo $this->element('s_common/shop_girl_review', array('new_shop_review' => $reviews)); ?>
        <!-- /新着レビュー -->

    <br style="cloar:both;" />

<?php  echo $this->element('s_pagenate/common_pagenate'); ?>

           <br style="cloar:both;" />

		<h3 class="label_3">他の女の子の口コミ</h3>
		<section>
			<div id="newSlideShow2" class="osusume_girl" >
<?php
$cnt = count($other_girls);
foreach($other_girls AS $key => $record) {
    if($key % 6 == 0) {
?>
<?php
    }
?>
                    <div><a href="<?php echo $linkCommon->get_user_girl($record); ?>">
                        <?php echo $imgCommon->get_girl_with_time($record, array(90, 120), 1); ?>
                    </a></div>
<?php
    if($key % 6 == 5 || $key == $cnt - 1) {
?>

<?php
    }
}
?>
			</div>
		</section>

</section>
	<!-- ライン・メール・電話 -->
	<?php echo $this->element('s_common/line_mail'); ?>
	<!-- /ライン・メール・電話 -->

	<!-- 電話をする -->
	<?php echo $this->element('s_common/shop_tel'); ?>
	<!-- /電話をする -->

	<!-- メニュー -->
	<?php echo $this->element('s_common/shop_menu'); ?>
	<!-- /メニュー -->

<?php echo $this->element('pagenate/s_numbers'); ?>


<script type="text/javascript">
<!--
$(document).ready(function() {
    $(".sub_image").hover(function() {
        $('#main_image').attr('src', $(this).attr('src'));
    });
});
-->
</script>


<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../../top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../../<?php echo $parent_shop['User']['id']; ?>" itemprop="url"><span itemprop="title"><?php echo $parent_shop['Shop']['name']; ?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../girls" itemprop="url"><span itemprop="title">在籍女性</a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./<?php echo $girl['Girl']['id']?>" itemprop="url"><span itemprop="title"><?php echo $girl['Girl']['name']?></a></li>
	</ul>
</nav>