<?php echo $this->element('s_common/shop_top'); ?>

<?php echo $this->element('s_common/shop_profile'); ?>

<?php
	// 有料の場合のみ
	if($parent_shop['UsersAuthority']['is_show_all_contents_by_shop'] == 1) {
?>
<?php echo $this->element('s_common/shop_pic_review'); ?>
<?php
	}
?>
<?php echo $this->element('s_common/shop_review'); ?>

<?php
	// 有料の場合のみ
	if($parent_shop['UsersAuthority']['is_show_all_contents_by_shop'] == 1) {

	// 激押し売れっ子権限がある場合
		if($parent_shop['UsersAuthority']['is_recommend_girl'] == 1) {
?>
<!-- おすすめ -->
<?php echo $this->element('s_common/shop_recommend'); ?>
<!-- /おすすめ -->
<?php
		}
?>

<?php
	}
?>
<p><br /></p>
<!-- お店のデーター -->
<?php echo $this->element('s_common/shop_data'); ?>
<!-- /お店のデーター -->
<?php
	// 有料の場合のみ
	if($parent_shop['UsersAuthority']['is_show_all_contents_by_shop'] == 1) {
?>
	<h3 class="label">クーポンデーター</h3>
	<section id="coupon">
		<div class="coupon_exp">
			<ul>
				<li><img src="/img/shop_coupon_icon03.gif" width="20" height="20" alt="全員">全員の方</li>
				<li><img src="/img/shop_coupon_icon02.gif" width="20" height="20" alt="会員">会員の方</li>
				<li><img src="/img/shop_coupon_icon01.gif" width="20" height="20" alt="新規">新規の方</li>
			</ul>
		</div><!-- /.coupon_exp-->
	<!-- お店のデーター -->
	<?php echo $this->element('s_common/shop_coupon'); ?>
	<!-- /お店のデーター -->

	<!-- 店長からのお知らせ -->
	<?php echo $this->element('s_common/shop_owner_comment'); ?>
	<!-- /店長からのお知らせ -->
<?php
	}
?>
	<!-- ライン・メール・電話 -->
	<?php echo $this->element('s_common/line_mail'); ?>
	<!-- /ライン・メール・電話 -->

	<!-- 電話をする -->
	<?php echo $this->element('s_common/shop_tel'); ?>
	<!-- /電話をする -->

	<!-- メニュー -->
	<?php echo $this->element('s_common/shop_menu'); ?>
	<!-- /メニュー -->

<?php echo $this->element('pagenate/s_numbers'); ?>



<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../s/top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./<?php echo $parent_shop['User']['id']; ?>/" itemprop="url"><span itemprop="title"><?php echo $parent_shop['Shop']['name']; ?></span></a></li>
	</ul>
</nav>
