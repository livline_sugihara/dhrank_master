
<div style="text-align:left;padding:5px;font-size:medium;background-color:#EFEBE3">
	口コミ内容確認
</div>

<?php echo $form->create(null, array('type'=>'post', 'url' => '/i/' . $parent_shop['User']['id'] . '/add_review' . '?guid=ON','encoding' => null));?>
<?php echo $form->hidden('Review.user_id', array('value' => $parent_shop['User']['id']));?>
<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	お店
</div>
<div style="text-align:left;padding:3px;">
	<?php echo $parent_shop['Shop']['name']; ?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	投稿者名
</div>
<div style="text-align:left;padding:3px;">
	<?php if($parent_reviewer){?>
	<?php echo $form->hidden('Review.reviewer_id', array('value' => $parent_reviewer['Reviewer']['id']));?>
	<?php echo $parent_reviewer['Reviewer']['handle']?>
	<?php }else{?>
	<?php echo $data['Review']['post_name']?>
	<?php echo $form->hidden('Review.post_name');?>
	<?php }?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	お店採点
</div>
<div style="text-align:left;padding:3px;">
	電話対応：<?php echo $data['Review']['score_shop_tel']?>点<?php echo $form->hidden('Review.score_shop_tel');?><br />
	到着時間：<?php echo $data['Review']['score_shop_time']?>点<?php echo $form->hidden('Review.score_shop_time');?><br />
	コスパ　：<?php echo $data['Review']['score_shop_cost']?>点<?php echo $form->hidden('Review.score_shop_cost');?><br />
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	女の子
</div>
<div style="text-align:left;padding:3px;">
	<?php echo $girl['Girl']['name']?>
	<?php echo $form->hidden('Review.girl_id');?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	見た目年齢
</div>
<div style="text-align:left;padding:3px;">
	<?php echo $m_reviews_ages[$data['Review']['reviews_age_id']]?>
	<?php echo $form->hidden('Review.reviews_age_id');?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	女の子採点
</div>
<div style="text-align:left;padding:3px;">
	第一印象：<?php echo $data['Review']['score_girl_first_impression']?>点<?php echo $form->hidden('Review.score_girl_first_impression');?><br />
	言葉遣い：<?php echo $data['Review']['score_girl_word']?>点<?php echo $form->hidden('Review.score_girl_word');?><br />
	サービス：<?php echo $data['Review']['score_girl_service']?>点<?php echo $form->hidden('Review.score_girl_service');?><br />
	感度　　：<?php echo $data['Review']['score_girl_sensitivity']?>点<?php echo $form->hidden('Review.score_girl_sensitivity');?><br />
	スタイル：<?php echo $data['Review']['score_girl_style']?>点<?php echo $form->hidden('Review.score_girl_style');?><br />
	あえぎ声：<?php echo $data['Review']['score_girl_voice']?>点<?php echo $form->hidden('Review.score_girl_voice');?><br />
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	コメント
</div>
<div style="text-align:left;padding:3px;">
	<?php echo nl2br($data['Review']['comment'])?>
	<?php echo $form->hidden('Review.comment');?>
</div>

<div style="text-align:center;padding:3px;">
<?php echo $form->submit('戻る', array('name' => 'return', 'div' => false)); ?>
<?php echo $form->submit('投稿', array('name' => 'add', 'div' => false)); ?>
</div>
<?php echo $form->end();?>


<hr color="#DED7C8" />
