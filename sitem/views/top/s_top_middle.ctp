<?php // ログインネーム ?>
<?php echo $this->element('s_common/name_display'); ?>
<?php echo $this->element('s_common/breadcrumb'); ?>

<!-- shop list -->
<?php echo $this->element('s_common/middle_shop_list'); ?>
<!-- /shop list -->

<?php // このエリアのイチオシ ?>
<?php echo $this->element('s_common/top_ichioshi'); ?>

<?php // 店舗検索 ?>
<?php echo $this->element('s_common/top_search'); ?>

<?php // 店舗カテゴリ ?>
<?php echo $this->element('s_common/top_shop_category'); ?>

	</div>
</section>
<?php echo $this->element('s_common/footer_banner'); ?>

<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
	</ul>
</nav>
