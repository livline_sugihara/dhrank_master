<?php
class TextCommonHelper extends Helper {
	var $helpers = array('Html');

	//3サイズ取得
	function get_3size($record){
		return $this->output('B' . $record['Girl']['bust'] . '(' . $record['MGirlsCup']['name2'] . ') - W' .
		$record['Girl']['waist'] . ' - H' . $record['Girl']['hip']);
	}

	//3サイズ取得
	function get_3size_on_profile($record){
		return $this->output('<span class="small">B</span>' . $record['Girl']['bust'] . '<span class="small">(' . $record['MGirlsCup']['name2'] . ')</span> - <span class="small">W</span>' .
				$record['Girl']['waist'] . ' - <span class="small">H</span>' . $record['Girl']['hip']);
	}

	//営業時間取得
	function get_shop_hour($record){
		$open = '';
		$close = '';

		if(isset($record['Shop']['shop_hour_open'])){
			$open = date('G:i', strtotime($record['Shop']['shop_hour_open']));
		}else{
			$open = 'オープン';
		}
		if(isset($record['Shop']['shop_hour_close'])){
			$close = date('G:i', strtotime($record['Shop']['shop_hour_close']));
		}else{
			$close = 'ラスト';
		}
		return $open . '～' . $close;
	}

	//テキスト抜粋・タグ削除
	function get_substr_tagstrip_text($text, $size){
		//タグ削除
		$strip_tags_text = strip_tags($text);
		//テキスト抜粋
		$ret_text = mb_substr($strip_tags_text,0,$size);
		//抜粋文字数よりもテキスト文字数が大きい場合
		if(mb_strlen($strip_tags_text) > $size){
			//...を追加
			$ret_text .= '...';
		}
		return $ret_text;
	}

	//日付テキスト取得
	function get_datetimewithweek($date){
		$weekday = Configure::read('weekday');
		$week = $weekday[date('w',strtotime($date))];
		return date('Y/m/d',strtotime($date)) . '(' . $week . ')' . date('H:i:s',strtotime($date));
	}

	// 縦表示HTML作成
	function get_vertical_text($text) {

		$ret = '';
		$len = mb_strlen($text);

		for($ii = 0; $ii < $len; $ii++) {
			$ret .= mb_substr($text, $ii, 1);

			if($ii < $len - 1) {
				$ret .= '<br />';
			}
		}

		return $ret;
	}

	function escape_empty($arr, $key, $dummyText = '不明') {

		$arrKeys = explode('.', $key);

		$tmp = $arr;
		foreach($arrKeys AS $k) {
			if(isset($tmp[$k])) {
				if(!is_array($tmp[$k])) {
					if(!empty($tmp[$k])) {
						return $tmp[$k];
					} else {
						return $dummyText;
					}
				}
			} else {
				return $dummyText;
			}
			$tmp = $tmp[$k];
		}

		return $dummyText;
	}
}