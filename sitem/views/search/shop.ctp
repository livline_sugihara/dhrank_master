<section id="middle" class="narrow">

    <!-- pay_shopList -->
    <div id="pay_shopList">
    
        <div class="title title04">
            <h2>店舗 検索結果</h2>
        </div>

        <div class="pageCounter">
            <span class="num"><?php echo $paginator->counter(array('format' => '%count%')); ?></span><span>件ありました</span>
            <?php echo $this->element('pagenate/search_header_pagenate'); ?>
        </div>

<?php
foreach($list as $key => $record) {

    if($record['UsersAuthority']['is_show_all_contents_by_categorylist'] == 1) {
?>
        <?php echo $this->element('common/_shop_payment', array('record' => $record)); ?>
<?php
    } else {
?>
        <?php echo $this->element('common/_shop_free', array('record' => $record)); ?>
<?php
    }
}
?>
    </div>
    <!-- /pay_shopList -->

    <?php echo $this->element('pagenate/search_footer_pagenate'); ?>

    <!-- ispotArea -->
    <div id="ispotArea">
        <?php echo $this->element('common/top_pic_review'); ?>
    </div>
    <!-- /ispotArea -->

    <?php echo $this->element('common/top_review_bunner'); ?>

    <!-- categoryArea -->
    <div id="categoryArea">
        <?php echo $this->element('common/top_category'); ?>
    </div>

</section>

<section id="side" class="wide">
    <?php echo $this->element('common/side_wide'); ?>
</section>