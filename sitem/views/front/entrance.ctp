<div id="area" class="section clearfix">
    <h2>エリア選択</h2>
    
    <table id="area1">
        <tr>
            <th rowspan="2">北海道<br>・<br>東北</th>
            <td><a href="/top">北海道</a></td>
            <td><a href="/top">青森県</a></td>
            <td><a href="/top">岩手県</a></td>
            <td><a href="/top">宮城県</a></td>
            <td><a href="/top">秋田県</a></td>
        </tr>
        <tr>
            <td><a href="/top">山形県</a></td>
            <td><a href="/top">福島県</a></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
        </tr>
    </table>
    
    <table id="area4">
        <tr>
            <th rowspan="2">関西</th>
            <td><a href="/top">大阪府</a></td>
            <td><a href="/top">兵庫県</a></td>
            <td><a href="/top">京都府</a></td>
            <td>滋賀県</td>
            <td><a href="/top">奈良県</a></td>
        </tr>
        <tr>
            <td>和歌山県</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
        </tr>
    </table>
    
    <table id="area2">
        <tr>
            <th rowspan="2">関東</th>
            <td><a href="tokyo.php">東京都</a></td>
            <td><a href="/top">神奈川県</a></td>
            <td><a href="/top">埼玉県</a></td>
            <td><a href="/top">千葉県</a></td>
            <td><a href="/top">茨城県</a></td>
        </tr>
        <tr>
            <td>栃木県</td>
            <td>群馬県</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr></tr>
    </table>
    
    <table id="area5">
        <tr>
            <th rowspan="2">中国<br>・<br>四国</th>
            <td>鳥取県</td>
            <td>島根県</td>
            <td>岡山県</td>
            <td><a href="/top">広島県</a></td>
            <td>山口県</td>
        </tr>
        <tr>
            <td>徳島県</td>
            <td><a href="/top">香川県</a></td>
            <td>愛媛県</td>
            <td><a href="/top">高知県</a></td>
            <td></td>
        </tr>
        <tr></tr>
    </table>
    
    <table id="area3">
        <tr>
            <th rowspan="2">中部</th>
            <td><a href="/top">愛知県</a></td>
            <td>新潟県</td>
            <td><a href="/top">岐阜県</a></td>
            <td>長野県</td>
            <td>静岡県</td>
        </tr>
        <tr>
            <td>富山県</td>
            <td>三重県</td>
            <td><a href="/top">石川県</a></td>
            <td>福井県</td>
            <td><a href="/top">山梨県</a></td>
        </tr>
        <tr></tr>
    </table>
    
    <table id="area6">
        <tr>
            <th rowspan="2">九州<br>・<br>沖縄</th>
            <td><a href="/top">福岡県</a></td>
            <td><a href="/top">佐賀県</a></td>
            <td><a href="/top">長崎県</a></td>
            <td><a href="/top">熊本県</a></td>
            <td><a href="/top">大分県</a></td>
        </tr>
        <tr>
            <td><a href="/top">宮崎県</a></td>
            <td><a href="/top">鹿児島県</a></td>
            <td><a href="/top">沖縄県</a></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
        </tr>
    </table>

</div>