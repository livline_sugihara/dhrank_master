<div class="body">

    <div class="title title2">
        <h2>CHANGE PASSWORD <span class="small">パスワード変更完了</span></h2>
    </div>

    <p class="text" style="margin-bottom: 50px;">パスワードの変更が完了しました。</p>

    <div class="align_c">
    	<a class="css_btn_class2" href="<?php echo $linkCommon->get_accounts_login($parent_area)?>" class="btn_navi">ログイン画面へ</a>
    </div>

    <div style="height: 200px;">&nbsp;</div>

</div>