	<div style="border-bottom:2px solid #c03c61;padding-bottom:2px;">
		<h2 class="link_o2">パスワード変更完了</h2>
	</div>

    <p class="text" style="margin-bottom: 20px;">パスワードの変更が完了しました。</p>
    
    <p class="btn_gr" style="margin-bottom:10px">
    	<a href="/s/accounts/login">ログイン画面へ</a>
    </p>
    
    <div style="height: 20px;">&nbsp;</div>
    