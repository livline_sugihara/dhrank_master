お問い合わせフォームから送信されました。
確認を行い、対応して下さい。

■お問い合せ内容
---------------------------------------
掲載希望エリア：　<?php echo $data['Contact']['insert_area']; ?> 
業種：　<?php echo $data['Contact']['category_name']; ?> 
店舗名：　<?php echo $data['Contact']['shop_name']; ?> 
ご担当者名：　<?php echo $data['Contact']['charge_name']; ?> 
ご担当者様のご連絡先：　<?php echo $data['Contact']['charge_contact']; ?> 
ご連絡先メールアドレス：　<?php echo $data['Contact']['emailto']; ?> 
店舗URL：　<?php echo $data['Contact']['shop_url']; ?> 
店舗電話番号：　<?php echo $data['Contact']['shop_tel']; ?> 
住所or（…発）：　<?php echo $data['Contact']['shop_address']; ?> 
営業時間：　<?php echo $data['Contact']['shop_business_hours']; ?> 
最低料金：　<?php echo $data['Contact']['lowest_cost']; ?> 
先行予約：　<?php echo $umu_select[$data['Contact']['reserve']]; ?> 
領収書：　<?php echo $umu_select[$data['Contact']['receipt']]; ?> 
待ち合わせ：　<?php echo $umu_select[$data['Contact']['layover']]; ?> 
コスプレ：　<?php echo $umu_select[$data['Contact']['cosplay']]; ?> 
クレジットカード：　<?php echo $umu_select[$data['Contact']['credit']]; ?> 
届出確認書or許可書： <?php if(!empty($data['Contact']['sendfile']['tmp_name'])){ echo '添付ファイル有り'; } else { echo '添付ファイルなし'; } ?> 
ご質問・ご要望等：
<?php echo $data['Contact']['comment']; ?> 
---------------------------------------
