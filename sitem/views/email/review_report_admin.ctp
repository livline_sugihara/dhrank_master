以下の口コミに対して通報されました。
総合管理画面より「口コミ情報一覧より」確認を行って下さい。

---------------------------------------
大エリア： <?php echo $area_data['LargeArea']['name']; ?> 
更新日： <?php echo date('Y/m/d H:i'); ?> 

店舗名： <?php echo $review['shop_name']; ?> 
女の子名： <?php echo $review['girl_name']; ?> 
レビュー内容: <?php echo $review['comment']; ?> 
投稿者： <?php echo $review['post_name']; ?> 
通報の理由: <?php echo $m_reviews_reports_types[$review_report['report_type_id']]; ?> 

<?php echo $review_report['comment']; ?> 
---------------------------------------
