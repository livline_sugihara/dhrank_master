<section id="middle" class="wide">

    <div class="categoryRankingArea">
        
        <div class="title title03">
            <h2>REVIEWER ranking <span class="small">口コミ投稿者ランキング</span></h2>
        </div>

<?php
foreach($reviewer_ranking AS $ii => $record) {

    $rank_image = '';
    // 急上昇
    // if($record['SummaryData']['prev_rank'] > 5 && $record['SummaryData']['rank'] <= 3) {
    //     $rank_image = 'arrow_up2.png';
    // } else if($record['SummaryData']['prev_rank'] == $record['SummaryData']['rank']) {
    //     // ステイ
    //     $rank_image = 'arrow_stay.png';
    // } else if($record['SummaryData']['prev_rank'] < $record['SummaryData']['rank']) {
    //     // ダウン
    //     $rank_image = 'arrow_down.png';
    // } else {
    //     // アップ
    //     $rank_image = 'arrow_up.png';
    // }
    $rank_image = 'arrow_up2.png';
?>
       
    <?php echo $this->element('common/_ranking_reviewer', array(
        'ii' => $ii,
        'record' => $record,
        'rank_image' => $rank_image,
    )); ?>

<?php
}
?>
    </div>
    <!-- /rankingArea -->
    
    <?php echo $this->element('common/top_review_bunner'); ?>

</section>

<section id="side" class="narrow">
	<?php echo $this->element('common/side_narrow'); ?>
</section>