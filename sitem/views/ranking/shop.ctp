<section id="middle" class="wide">

    <div class="categoryRankingArea">
        
        <div class="title title03">
            <h2>SHOP ranking <span class="small">お店総合ランキング</span></h2>
        </div>
<?php if(!empty($parent_reviewer)) { ?>


<?php
foreach($shop_ranking AS $ii => $record) {

    $rank_image = '';
    // 急上昇
    if($record['SummaryData']['prev_rank'] > 5 && $record['SummaryData']['rank'] <= 3) {
        $rank_image = 'arrow_up2.png';
    } else if($record['SummaryData']['prev_rank'] == $record['SummaryData']['rank']) {
        // ステイ
        $rank_image = 'arrow_stay.png';
    } else if($record['SummaryData']['prev_rank'] < $record['SummaryData']['rank']) {
        // ダウン
        $rank_image = 'arrow_down.png';
    } else {
        // アップ
        $rank_image = 'arrow_up.png';
    }

?>

    <?php echo $this->element('common/_ranking_shop', array(
        'ii' => $ii,
        'record' => $record,
        'rank_image' => $rank_image,
    )); ?>

<?php
}
?>
<?php } else { ?>
	<p>会員登録すると表示されます。</p>
<?php } ?>

    </div>
    <!-- /rankingArea -->
    
    <?php echo $this->element('common/top_review_bunner'); ?>

</section>

<section id="side" class="narrow">
	<?php echo $this->element('common/side_narrow'); ?>
</section>