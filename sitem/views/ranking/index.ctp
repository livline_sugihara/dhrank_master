<h2 class="rank_h2"><?php echo $m_rank_business_category['MShopsBusinessCategory']['name']?>ランキング</h2>

<?php $paginator->options(array('url' => array('business_category_id' => $this->params['business_category_id']))); ?>
<?php foreach($ranking as $key => $record){?>
<h3 class="rank_midashi">
			<span class="rank_midashi_left">
				<?php echo (($paginator->counter() - 1) * $pcount['rankings'] + ($key + 1))?>
			</span>

			<span class="rank_midashi_middle">
				<?php echo $record['Shop']['name']?>
			</span>

			<span class="rank_midashi_right">総合評価：
				<?php for($i = 0; $i < 5; $i++){?><img src="/images/star_<?php echo ($i+1 <= round($record[0]['point']))?'on':'off';?>.png" class="kuchikomi_heart" /><?php }?>
        	<span class="spot4 score"><?php echo round($record[0]['point'],2)?></span>
                [口コミ <?php echo $record[0]['count']?>件]
            </span>
</h3>

<table border="0" cellspacing="0" cellpadding="0" width="720px">
<tr height="160">
<td><a href="<?php echo $linkCommon->get_user_shop($record)?>"><?php echo $imgCommon->get_rank($record,1)?></a></td>
<td><a href="<?php echo $linkCommon->get_user_shop($record)?>"><?php echo $imgCommon->get_rank($record,2)?></a></td>
<td><a href="<?php echo $linkCommon->get_user_shop($record)?>"><?php echo $imgCommon->get_rank($record,3)?></a></td>
<td><a href="<?php echo $linkCommon->get_user_shop($record)?>"><?php echo $imgCommon->get_rank($record,4)?></a></td>
<td><a href="<?php echo $linkCommon->get_user_shop($record)?>"><?php echo $imgCommon->get_rank($record,5)?></a></td>
<td><a href="<?php echo $linkCommon->get_user_shop($record)?>"><?php echo $imgCommon->get_rank($record,6)?></a></td>
</tr>
</table>
<?php }?>

<?php echo $this->element('pagenate/numbers'); ?>
