	<div class="link_o"><h2>お店総合ランキング</h2></div>
	<div class="kensuu"><p>お店●BEST<span>10</span>※会員限定<span style="float:right">毎週日曜日更新！</p></div>

	<section id="all_shop_rank_area">

<?php
$i = 0;

if(!empty($parent_reviewer)) {
	$hyouji = 10;
} else { 
	$hyouji = 3;
}
foreach($shop_ranking AS $ii => $record) {
if ($i >= $hyouji) {
	break;
} else {

    $rank_image = '';
    // 急上昇
    if($record['SummaryData']['prev_rank'] > 5 && $record['SummaryData']['rank'] <= 3) {
        $rank_image = 'arrow_up2.png';
    } else if($record['SummaryData']['prev_rank'] == $record['SummaryData']['rank']) {
        // ステイ
        $rank_image = 'arrow_stay.png';
    } else if($record['SummaryData']['prev_rank'] < $record['SummaryData']['rank']) {
        // ダウン
        $rank_image = 'arrow_down.png';
    } else {
        // アップ
        $rank_image = 'arrow_up.png';
    }

?>

    <?php echo $this->element('s_common/_ranking_shop', array(
        'ii' => $ii,
        'record' => $record,
        'rank_image' => $rank_image,
    )); ?>

<?php
	$i++;
	}
}
?>
<?php if(empty($parent_reviewer)) { ?>
<p class="btn_pi"><a href="<?php echo $linkCommon->get_accounts_login($record = null); ?>">続きを表示する</a></p>
<?php } ?>
    </div>
    <!-- /rankingArea -->


</section>
<?php echo $this->element('s_common/footer_banner'); ?>

<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./shop/" itemprop="url"><span itemprop="title">お店ランキング</span></a></li>
	</ul>
</nav>