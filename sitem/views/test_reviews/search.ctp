<h1>口コミ検索</h1>

<h2>口コミの検索結果</h2>
<?php
    $cnt = count($review_datas);
    if ($cnt < 1)
    {
        echo '<tr><td>検索結果はありませんでした</td><tr>';
    }
    else
    {
        echo $cnt . '件の検索結果に一致しました。<br />';
        echo "<table border='1'>";
        foreach($review_datas as $key => $val)
        {
            echo '<tr>';
            echo '<td>店名：</td><td>'.$val['Shop']['name'].'</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td style="background-color:#999999;">女の子の名前：</td><td>'.$val['Girl']['name'].'</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>口コミ内容:</td><td>'.nl2br($val['Review']['comment']).'</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>レビュワー:</td><td>'.$val['Reviewer']['handle'].'</td>';
            echo '</tr>';

        }
        echo "</table>";
    }
?>
