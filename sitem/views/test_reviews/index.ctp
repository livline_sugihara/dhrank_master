<h1>口コミ検索</h1>

<h2>お店別の口コミ</h2>
<table border="1">
<tr>
	<td><a href="http://osakam.19800210.com/test_reviews/shops/1/">あ行</a></td>
	<td><a href="http://osakam.19800210.com/test_reviews/shops/2/">か行</a></td>
	<td><a href="http://osakam.19800210.com/test_reviews/shops/3/">さ行</a></td>
	<td><a href="http://osakam.19800210.com/test_reviews/shops/4/">た行</a></td>
	<td><a href="http://osakam.19800210.com/test_reviews/shops/5/">な行</a></td>
	<td><a href="http://osakam.19800210.com/test_reviews/shops/6/">は行</a></td>
	<td><a href="http://osakam.19800210.com/test_reviews/shops/7/">ま行</a></td>
	<td><a href="http://osakam.19800210.com/test_reviews/shops/8/">や行</a></td>
	<td><a href="http://osakam.19800210.com/test_reviews/shops/9/">ら行</a></td>
	<td><a href="http://osakam.19800210.com/test_reviews/shops/10/">わ行</a></td>
</tr>
</table>

<h2>口コミの一覧</h2>
<table border="1">
<tr>
	<td><a href="http://osakam.19800210.com/test_reviews/lists/1/">口コミを全て見る</a></td>
	<td><a href="http://osakam.19800210.com/test_reviews/reason/">口コミの非掲載理由</a></td>
</tr>
</table>

<h2>
検索
</h2>
<form action="./search/" method="POST" name="review_search">
<h2>女の子のタイプ</h2>
<table>
	<tr>
		<th><p>名前</p></th>
		<td><p><input type="text" name="girl_name" class="girl_name" value="" class="search_name"></p></td>
		<th><p>年齢</p></th>
		<td>
			<ul class="list_s">
				<li><p><input type="checkbox" name="arr_age[]" class="arr_age" value="1">18～19歳</p></li>
				<li><p><input type="checkbox" name="arr_age[]" class="arr_age" value="2">20～24歳</p></li>
				<li><p><input type="checkbox" name="arr_age[]" class="arr_age" value="3">25～29歳</p></li>
				<li><p><input type="checkbox" name="arr_age[]" class="arr_age" value="4">30～34歳</p></li>
				<li><p><input type="checkbox" name="arr_age[]" class="arr_age" value="5">35歳以上</p></li>
			<ul>
		</td>
		<th><p>身長</p></th>
		<td>
			<ul class="list_s">
				<li><p><input type="checkbox" name="arr_tall[]" class="arr_tall" value="1">149cm以下</p></li>
				<li><p><input type="checkbox" name="arr_tall[]" class="arr_tall" value="2">150～154cm</p></li>
				<li><p><input type="checkbox" name="arr_tall[]" class="arr_tall" value="3">155～159cm</p></li>
				<li><p><input type="checkbox" name="arr_tall[]" class="arr_tall" value="4">160～164cm</p></li>
				<li><p><input type="checkbox" name="arr_tall[]" class="arr_tall" value="5">165～169cm</p></li>
				<li><p><input type="checkbox" name="arr_tall[]" class="arr_tall" value="6">170cm以上</p></li>
			<ul>
		</td>
		<th><p>カップ数</p></th>
		<td>
			<ul class="list_s">
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="1">Ａカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="2">Ｂカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="3">Ｃカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="4">Ｄカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="5">Ｅカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="6">Ｆカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="7">Ｇカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="8">Ｈカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="9">Ｉカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="10">Ｊカップ以上</p></li>
			<ul>
		</td>
		<th><p>バスト</p></th>
		<td>
			<ul class="list_s">
				<li><p><input type="checkbox" name="arr_bust[]" class="arr_bust" value="1">79cm以下</p></li>
				<li><p><input type="checkbox" name="arr_bust[]" class="arr_bust" value="2">80～84cm</p></li>
				<li><p><input type="checkbox" name="arr_bust[]" class="arr_bust" value="3">85～89cm</p></li>
				<li><p><input type="checkbox" name="arr_bust[]" class="arr_bust" value="4">90～94cm</p></li>
				<li><p><input type="checkbox" name="arr_bust[]" class="arr_bust" value="5">95～99cm</p></li>
				<li><p><input type="checkbox" name="arr_bust[]" class="arr_bust" value="6">100cm以上</p></li>
			<ul>
		</td>
		<th><p>ウエスト</p></th>
		<td>
			<ul class="list_s">
				<li><p><input type="checkbox" name="arr_waist[]" class="arr_waist" value="1">54cm以下</p></li>
				<li><p><input type="checkbox" name="arr_waist[]" class="arr_waist" value="2">55～59cm</p></li>
				<li><p><input type="checkbox" name="arr_waist[]" class="arr_waist" value="3">60～64cm</p></li>
				<li><p><input type="checkbox" name="arr_waist[]" class="arr_waist" value="4">65～69cm</p></li>
				<li><p><input type="checkbox" name="arr_waist[]" class="arr_waist" value="5">70cm以上</p></li>
			<ul>
		</td>
	</tr>
	<tr>
		<th><p>ヒップ</p></th>
		<td>
			<ul class="list_s">
				<li><p><input type="checkbox" name="arr_hip[]" class="arr_hip" value="1">79cm以下</p></li>
				<li><p><input type="checkbox" name="arr_hip[]" class="arr_hip" value="2">80～84cm</p></li>
				<li><p><input type="checkbox" name="arr_hip[]" class="arr_hip" value="3">85～89cm</p></li>
				<li><p><input type="checkbox" name="arr_hip[]" class="arr_hip" value="4">90～94cm</p></li>
				<li><p><input type="checkbox" name="arr_hip[]" class="arr_hip" value="5">95～99cm</p></li>
				<li><p><input type="checkbox" name="arr_hip[]" class="arr_hip" value="6">100cm以上</p></li>
			<ul>
		</td>
		<th><p>カテゴリ</p></th>
		<td>
			<ul class="list_m">
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="1">デリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="2">人妻デリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="3">待ち合わせデリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="4">高級デリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="5">アロマ・エステ・デリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="6">ぽっちゃりデリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="7">韓デリ・外国人・デリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="1000">その他</p></li>
			<ul>
		</td>
	</tr>
</table>
<h2>女の子のおすすめ内容</h2>
<table>
<?php
	foreach($recommend_type as $key => $val)
	{
		if ($key % 5 == 0) echo '<tr>';
		echo '<td><input type="checkbox" name="recommend[]" value="' . $val['m_girls_recommend_types']['id'] . '" />' . $val['m_girls_recommend_types']['name'] . '</td>';
		if ($key % 5 == 4) echo '</tr>';

	}
?>
</table>
<h2>投稿者で検索</h2>
<input type="text" name="reviewer_name" value="" />
<h2>コメントで検索</h2>
<input type="text" name="comment" value="" />
<br />
<input type="submit" name="submit" value="この条件で検索する" />

</form>
