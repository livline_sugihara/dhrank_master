<script>
$(document).ready(function(){
	searchShopData(true);
	$('[name=searchform]').change(function(){
		searchShopData(false);
		return false;
	});

	// リセット
	$('input:reset').click(function(){
		// フォームの値をリセット
		$('[name=searchform]')[0].reset();
		searchShopData(false);
		return false;
	});


});



function searchShopData(flg){
	var girl_name = $('[class="girl_name"]').val(); 	// 名前
	var arr_age = $('[class="arr_age"]:checked').map(function(){ return $(this).val();}).get(); // 年齢
	var arr_tall = $('[class="arr_tall"]:checked').map(function(){ return $(this).val();}).get(); // 身長
	var arr_cup = $('[class="arr_cup"]:checked').map(function(){ return $(this).val();}).get();	// カップ数
	var arr_bust = $('[class="arr_bust"]:checked').map(function(){ return $(this).val();}).get();// バスト
	var arr_waist = $('[class="arr_waist"]:checked').map(function(){ return $(this).val();}).get();// ウエスト
	var arr_hip = $('[class="arr_hip"]:checked').map(function(){ return $(this).val();}).get();// ヒップ
	var arr_cate = $('[class="arr_cate"]:checked').map(function(){ return $(this).val();}).get();// カテゴリ

	if (flg == true) data = '';

	$.ajax({
  		url: '/test_update/',
  		type:'post',
  		data:{
				girl_name : girl_name,
				arr_age : arr_age,
				arr_tall : arr_tall,
				arr_cup : arr_cup,
				arr_bust : arr_bust,
				arr_waist : arr_waist,
				arr_hip : arr_hip,
				arr_cate : arr_cate
		},
		//カンマ区切りで取得した文字列をセット
  		success: function(data){
			$('#girls_list').html(data);
  		},

	});
}
</script>
<form action ="." method="POST" name="searchform">
<table>
	<tr>
		<th><p>名前</p></th>
		<td><p><input type="text" name="girl_name" class="girl_name" value="" class="search_name"></p></td>
		<th><p>年齢</p></th>
		<td>
			<ul class="list_s">
				<li><p><input type="checkbox" name="arr_age[]" class="arr_age" value="1">18～19歳</p></li>
				<li><p><input type="checkbox" name="arr_age[]" class="arr_age" value="2">20～24歳</p></li>
				<li><p><input type="checkbox" name="arr_age[]" class="arr_age" value="3">25～29歳</p></li>
				<li><p><input type="checkbox" name="arr_age[]" class="arr_age" value="4">30～34歳</p></li>
				<li><p><input type="checkbox" name="arr_age[]" class="arr_age" value="5">35歳以上</p></li>
			<ul>
		</td>
		<th><p>身長</p></th>
		<td>
			<ul class="list_s">
				<li><p><input type="checkbox" name="arr_tall[]" class="arr_tall" value="1">149cm以下</p></li>
				<li><p><input type="checkbox" name="arr_tall[]" class="arr_tall" value="2">150～154cm</p></li>
				<li><p><input type="checkbox" name="arr_tall[]" class="arr_tall" value="3">155～159cm</p></li>
				<li><p><input type="checkbox" name="arr_tall[]" class="arr_tall" value="4">160～164cm</p></li>
				<li><p><input type="checkbox" name="arr_tall[]" class="arr_tall" value="5">165～169cm</p></li>
				<li><p><input type="checkbox" name="arr_tall[]" class="arr_tall" value="6">170cm以上</p></li>
			<ul>
		</td>
		<th><p>カップ数</p></th>
		<td>
			<ul class="list_s">
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="1">Ａカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="2">Ｂカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="3">Ｃカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="4">Ｄカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="5">Ｅカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="6">Ｆカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="7">Ｇカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="8">Ｈカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="9">Ｉカップ</p></li>
				<li><p><input type="checkbox" name="arr_cup[]" class="arr_cup" value="10">Ｊカップ以上</p></li>
			<ul>
		</td>
		<th><p>バスト</p></th>
		<td>
			<ul class="list_s">
				<li><p><input type="checkbox" name="arr_bust[]" class="arr_bust" value="1">79cm以下</p></li>
				<li><p><input type="checkbox" name="arr_bust[]" class="arr_bust" value="2">80～84cm</p></li>
				<li><p><input type="checkbox" name="arr_bust[]" class="arr_bust" value="3">85～89cm</p></li>
				<li><p><input type="checkbox" name="arr_bust[]" class="arr_bust" value="4">90～94cm</p></li>
				<li><p><input type="checkbox" name="arr_bust[]" class="arr_bust" value="5">95～99cm</p></li>
				<li><p><input type="checkbox" name="arr_bust[]" class="arr_bust" value="6">100cm以上</p></li>
			<ul>
		</td>
		<th><p>ウエスト</p></th>
		<td>
			<ul class="list_s">
				<li><p><input type="checkbox" name="arr_waist[]" class="arr_waist" value="1">54cm以下</p></li>
				<li><p><input type="checkbox" name="arr_waist[]" class="arr_waist" value="2">55～59cm</p></li>
				<li><p><input type="checkbox" name="arr_waist[]" class="arr_waist" value="3">60～64cm</p></li>
				<li><p><input type="checkbox" name="arr_waist[]" class="arr_waist" value="4">65～69cm</p></li>
				<li><p><input type="checkbox" name="arr_waist[]" class="arr_waist" value="5">70cm以上</p></li>
			<ul>
		</td>
	</tr>
	<tr>
		<th><p>ヒップ</p></th>
		<td>
			<ul class="list_s">
				<li><p><input type="checkbox" name="arr_hip[]" class="arr_hip" value="1">79cm以下</p></li>
				<li><p><input type="checkbox" name="arr_hip[]" class="arr_hip" value="2">80～84cm</p></li>
				<li><p><input type="checkbox" name="arr_hip[]" class="arr_hip" value="3">85～89cm</p></li>
				<li><p><input type="checkbox" name="arr_hip[]" class="arr_hip" value="4">90～94cm</p></li>
				<li><p><input type="checkbox" name="arr_hip[]" class="arr_hip" value="5">95～99cm</p></li>
				<li><p><input type="checkbox" name="arr_hip[]" class="arr_hip" value="6">100cm以上</p></li>
			<ul>
		</td>
		<th><p>カテゴリ</p></th>
		<td>
			<ul class="list_m">
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="1">デリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="2">人妻デリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="3">待ち合わせデリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="4">高級デリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="5">アロマ・エステ・デリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="6">ぽっちゃりデリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="7">韓デリ・外国人・デリヘル</p></li>
				<li><p><input type="checkbox" name="arr_cate[]" class="arr_cate" value="1000">その他</p></li>
			<ul>
		</td>
	</tr>
</table>
<input type="reset" value="検索をリセットする" />
<div id="girls_list"></div>
</form>
