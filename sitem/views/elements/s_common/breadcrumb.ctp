	<div id="pankuzu">
		<ul class="clearfix">
			<li>
<?php
foreach($breadcrumb as $val) {
	$option = (isset($val['option'])) ? $val['option'] : array();
	$link = (isset($val['link'])) ? $val['link'] : null;
	$html->addCrumb($val['name'], $link, $option);
}
if(!isset($separator)) $separator = "</li><li>";
if(!isset($top)) $top = '';
?>
				<?php echo $html->getCrumbs($separator, $top); ?>
			</li>
		</ul>
	</div>
