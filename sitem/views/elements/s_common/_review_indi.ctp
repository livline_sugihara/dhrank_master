			<div class="box_inner">
				<div class="pickup_box_summary">
					<div class="clearfix">
<?php if($record['Reviewer']['handle'] == null) { ?>
						<p class="profile_img"><?php echo $imgCommon->get_reviewer_avatar($record, array(30,30), 'img_reviewer'); ?></p>
						<p class="name"><?php echo $record['Review']['post_name']; ?>さん</p>
<?php } else { ?>
						<p class="profile_img"><a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>"><?php echo $imgCommon->get_reviewer_avatar($record, array(30,30), 'img_reviewer'); ?></a></p>
						<p class="name"><?php echo $record['Reviewer']['handle']; ?>さん</p>
<?php } ?>

						<p class="font08"><?php echo $textCommon->escape_empty($record, 'MReviewersAge.name', '年齢不明'); ?> | <?php if(empty($record['MReviewersJob']['name'])) { echo '職業不明'; } else { echo $record['MReviewersJob']['name']; } ?> | 口コミ:<?php if($record['Review']['reviewer_id'] == 0) { echo '非会員'; } else { echo $record[0]['review_cnt'] . '件'; } ?></p>
					</div>
				</div>
				<div class="clearfix review_detail_body">
					<div class="summary">
						<p class="review">
<?php
for($ii = 1; $ii <= 5; $ii++) {
    $suffix = ($ii <= $record[0]['girl_avg']) ? 'on' : 'off';
?>
							<img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>

						<!--	<span class="cow"><?php echo round($record[0]['girl_avg'], 2); ?></span> -->

<?php if(isset($review_flag) && $review_flag) { ?>
							<span class="whiteBtn favoriteBtn bookmarkReview" data="<?php echo $record['Review']['id']; ?>" mode="delete"><a href="#">このクチコミを削除</a></p>
<?php } else { ?>
							<span class="whiteBtn favoriteBtn bookmarkReview" data="<?php echo $record['Review']['id']; ?>" mode="add"><a href="#">このクチコミを保存</a></p>
<?php } ?>
						</p>
                  <!--      <p class="name"><a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo $review['Girl']['name'] ?></a></p> -->
<?php if($record['Review']['delete_flag'] == Review::DELETE_FLAG_COMMENT_ONLY) { ?>

<?php } else if($record['Review']['not_publish'] == 1) { ?>
	この口コミは非掲載です。
<?php } else if($record['Review']['delete_flag'] == Review::DELETE_FLAG_SAVE_POINTS) { ?>
						<p class="txt">このコメントは削除依頼により削除されました。</p>

<?php } else if($record['Review']['member_only'] == 1) { ?>
						<p class="tt mar_t_05"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>" style="text-decoration:none;">会員限定の記事です。</a></p>

<?php } else { ?>
						<p class="caption"><?php echo nl2br($review['Review']['post_title']);?></p>
						<p class="txt"><?php echo nl2br($review['Review']['comment']);?></p>
                        <p class="day"><?php echo date('Y年n月j日', strtotime($review['Review']['created'])) ?></p>
<?php } ?>


						<div class="course_mark">
							<span class="target"><?php echo $textCommon->escape_empty($record, 'Review.appointed_sub_name', '不明'); ?></span>
							<span class="new"><?php echo $textCommon->escape_empty($record, 'Review.appointed_type_name', '不明'); ?></span>
						</div>
					</div>
      				<div class="score">
					<p class="review"><span class="score_head"><span class="point">L</span> ルックス</span>
<?php
for($i1 = 1; $i1 <= 5; $i1++) {
    $suffix = ($i1 <= $record['Review']['score_girl_looks']) ? 'on' : 'off';
?>
                    <img src="/img/star_<?php echo $suffix; ?>.png">

<?php } ?>
					</p>
					</div>
      				<div class="score">
                    	<p class="review"><span class="score_head"><span class="point">S</span> サービス</span>
<?php
for($i2 = 1; $i2 <= 5; $i2++) {
    $suffix = ($i2 <= $record['Review']['score_girl_service']) ? 'on' : 'off';
?>
                    <img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>
					</p>
					</div>
      				<div class="score">
                    	<p class="review"><span class="score_head"><span class="point">C</span> 性　　格</span>
<?php
for($i3 = 1; $i3 <= 5; $i3++) {
    $suffix = ($i3 <= $record['Review']['score_girl_character']) ? 'on' : 'off';
?>
                    <img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>
					</p>
					</div>
				</div>

    <div style="font-size:80%;margin-bottom:15px;margin-top:8px;">
<?php if((isset($review_flag) && $review_flag) || (isset($myreview_flag) && $myreview_flag)) { ?>
<?php } else { ?>
		<p class="whiteBtn reportBtn"><a href="<?php echo $linkCommon->get_review_report($record); ?>">通報する</a></p>
		<div class="thanksBtn">
			<p>参考になりました</p>
			<p class="redBtn thanksBtn" data="<?php echo $record['Review']['id']; ?>"><a href="#">ありがとう</a></p>
		</div>
<?php } ?>
<?php /*
	<p class="whiteBtn reportBtn"><a href="<?php echo $linkCommon->s_get_review_report($record = null); ?>">通報する</a></p>
	<div class="thanksBtn">
		<p>参考になりました</p>
		<p class="redBtn thanksBtn" data="<?php echo $record['Review']['id']; ?>"><a href="#">ありがとう</a></p>
	</div>
*/ ?>
	<div>
</div>
