	<section id="rank_box">
		<p id="rank_box_tt" class="link_pi">各ランキング結果</p>
		<div id="tabsArea">
			<ul id="tabs" class="clearfix">
				<li class="select">お店総合<br>ランキング</li>
				<li>アクセスの多い<br>女の子ランキング</li>
				<li>口コミ投稿者<br>ランキング</li>
			</ul>

<!-- content_wrap(お店総合) -->
			<div class="content_wrap">
				<?php echo $this->element('s_common/top_ranking_shop'); ?>
			</div>
<!-- /content_wrap -->

<!-- content_wrap -->
			<div class="content_wrap" style="display:none">
				<?php echo $this->element('s_common/top_ranking_girl_access'); ?>
			</div>
<!-- /content_wrap -->

<!-- content_wrap -->
			<div class="content_wrap" style="display:none">
				<?php echo $this->element('s_common/top_ranking_reviewer'); ?>
			</div>
<!-- /content_wrap -->

		</div>
<!-- / ispot アイスポット -->
	</section>