	<header>
		<div class="clearfix">
			<div id="head_left">
				<h1><a href="<?php echo $linkCommon->get_top($record = null); ?>"><img src="/img/s/deri_logo.png" height="42"></a></h1>
				<p>国内最大級の<br>デリヘル口コミサイト</p>
			</div>

			<div id="head_account_right">
				<div class="button-toggle">&#9776;</div>
			</div>
		</div>
	</header><!--  / header -->
	<div class="menu">
		<ul>
			<li><a href="<?php echo $html->url('/', true) ?>s/newest">新着口コミ</a></li>
			<li><a href="<?php echo $html->url('/', true) ?>s/ranking/girl">アクセスが多い女の子</a></li>
			<li><a href="<?php echo $html->url('/', true) ?>s/ranking/review">参考になった口コミ</a></li>
			<li><a href="<?php echo $html->url('/', true) ?>s/ranking/reviewer">口コミ投稿者ランキング</a></li>
			<li><a href="<?php echo $html->url('/', true) ?>s/ranking/shop">お店総合ランキング</a></li>
			<li><a href="<?php echo $html->url('/', true) ?>s/topic">このエリアのイチオシのお店</a></li>
			<li><a href="<?php echo $html->url('/', true) ?>s/recommend/shop">おすすめショップ</a></li>
<?php
// ログイン時
	if(!empty($parent_reviewer)) {
?>
			<li class="menu_last"><a href="<?php echo $linkCommon->get_accounts_logout($record = null)?>">ログアウト</a></li>
<?php
// 未ログイン時
	} else {
?>
			<li class="menu_last"><a href="<?php echo $linkCommon->get_accounts_login($record = null)?>">ログイン</a></li>
<?php
}
?>
		</ul>
	</div>
<?php echo $this->element('s_common/name_display'); ?>
<?php echo $this->element('s_common/breadcrumb'); ?>
