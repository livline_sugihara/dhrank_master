<?php if(count($coupons) > 0) { ?>
<?php
	foreach($coupons AS $key => $record) {
?>
		<div class="coupon_link">
			<div class="coupon_inner">
				<div class="clearfix">
					<p class="otoku f_left">実質どれくらいお得なの？</p>
					<p class="price f_right"><?php echo $record['Coupon']['content']; ?></p>
				</div>
				<div class="clearfix">
<?php /*					<p class="icon f_left"><a href="#"><img src="/img/shop_coupon_icon0<?php echo $record['Coupon']['target']; ?>.gif" width="50" height="50" alt="<?php echo $record['Coupon']['target']; ?>"></a></p>	*/ ?>
					<p class="icon f_left"><img src="/img/shop_coupon_icon0<?php echo $record['Coupon']['target']; ?>.gif" width="50" height="50" alt="<?php echo $record['Coupon']['target']; ?>"></p>

					<p class="font14"><?php echo $record['Coupon']['condition']; ?></p>
				</div>
				<div class="detail_box clearfix">
					<p>提示条件：予約時</p>
					<p>利用条件：<?php echo $m_target_caption_list[$record['Coupon']['target']]; ?></p>
					<p>有効期限：<?php if($record['Coupon']['expire_unlimited'] == 1) { ?>期限なし<?php } else { echo date('Y年m月d日まで', strtotime($record['Coupon']['expire_date'])); } ?></p>
				</div>
			</div>
		</div>
<?php
	}
?>
<?php
	} else {
?>
		<p>割引クーポンは発行されていません。</p>
<?php
	}
?>
	</section>
