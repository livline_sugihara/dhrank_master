<!-- rankingArea -->
<div class="categoryRankingArea">

<?php
foreach($girl_ranking AS $ii => $record) {

	$rank_image = '';
	// 急上昇
	if($record['SummaryData']['prev_rank'] > 5 && $record['SummaryData']['rank'] <= 3) {
		$rank_image = 'arrow_up2.png';
	} else if($record['SummaryData']['prev_rank'] == $record['SummaryData']['rank']) {
		// ステイ
		$rank_image = 'arrow_stay.png';
	} else if($record['SummaryData']['prev_rank'] < $record['SummaryData']['rank']) {
		// ダウン
		$rank_image = 'arrow_down.png';
	} else {
		// アップ
		$rank_image = 'arrow_up.png';
	}

?>

	<?php echo $this->element('common/_ranking_girl', array(
		'ii' => $ii,
		'record' => $record,
		'rank_image' => $rank_image,
	)); ?>

<?php
}
?>

<?php if(!empty($parent_reviewer)) { ?>
	<p class="redBtn"><a href="/ranking/girl">もっと見る</a></p>
<?php } else { ?>
	<p class="redBtn"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>">もっと見る</a></p>
<?php } ?>

</div>
<!-- /rankingArea-->
<?php // print_r($girl_ranking); ?>