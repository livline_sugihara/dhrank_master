
<div style="text-align:left;padding:3px;background-color:#EFEBE3">
	<span style="font-size:medium"><?php echo $parent_shop['Shop']['name']?></span><br />
    [0]<a href="tel:<?php echo str_replace('-','',$parent_shop['Shop']['phone_number'])?>" accesskey="0"><?php echo $parent_shop['Shop']['phone_number']?></a><br />
    <?php echo $parent_shop['MShopsBusinessCategory']['name']?>
<div align="right">
<?php if($parent_shop['User']['is_official_link_display'] && !empty($parent_shop['Shop']['url_mobile'])){?>
[<a href="<?php echo $parent_shop['Shop']['url_mobile']?>">オフィシャル</a>]
<?php }?>
<?php if($parent_reviewer){?>
[<a href="<?php echo $linkCommon->get_user_add_shops_bookmark_end($parent_shop)?>"><?php if($bookmark_shop == 0){?>ブックマーク追加<?php }else{?>ブックマーク登録済<?php }?></a>]
<?php }else{?>
[<a href="<?php echo $linkCommon->get_accounts_login($parent_area);?>">ブックマーク追加</a>]
<?php }?>
</div>
<hr color="#C6C7C8" />
<table width="240">
<tr><td width="50%" style="font-size:x-small;">総合評価：<span style="color:#BE0A26"><?php echo round($review_data[0]['total'],2)?></span></td><td width="50%" align="right" style="font-size:x-small;"><span style="color:#0B72B5">[口コミ：<?php echo $review_data[0]['count']?>件]</span></td></tr>
</table>
<hr color="#C6C7C8" />
■お店評価<br />
電話対応：<span style="color:#BE0A26"><?php echo round($review_data[0]['score_shop_tel'],1)?></span>  | 到着時間：<span style="color:#BE0A26"><?php echo round($review_data[0]['score_shop_time'],1)?></span>  | コスパ：<span style="color:#BE0A26"><?php echo round($review_data[0]['score_shop_cost'],1)?></span>
<hr color="#C6C7C8" />
■女の子評価<br />
第一印象：<span style="color:#BE0A26"><?php echo round($review_data[0]['score_girl_first_impression'],1)?></span>|言葉遣い：<span style="color:#BE0A26"><?php echo round($review_data[0]['score_girl_word'],1)?></span>|サービス：<span style="color:#BE0A26"><?php echo round($review_data[0]['score_girl_service'],1)?></span>|感度：<span style="color:#BE0A26"><?php echo round($review_data[0]['score_girl_sensitivity'],1)?></span>|スタイル：<span style="color:#BE0A26"><?php echo round($review_data[0]['score_girl_style'],1)?></span>  | あえぎ声：<span style="color:#BE0A26"><?php echo round($review_data[0]['score_girl_voice'],1)?></span>
</div>

<div style="text-align:center;padding:3px;">
	[1]<a href="<?php echo $linkCommon->get_user_shop($parent_shop)?>" accesskey="1">トップ</a> [2]<a href="<?php echo $linkCommon->get_user_girls($parent_shop)?>" accesskey="2">女の子</a> [3]<a href="<?php echo $linkCommon->get_user_systems($parent_shop)?>" accesskey="3">システム</a><br />
<?php //start---2013/3/6 障害No.3-0003修正 ?>
    [4]<a href="<?php echo $linkCommon->get_user_add_review($parent_shop)?>?guid=ON" accesskey="4">口コミを投稿する</a>
<?php //end---2013/3/6 障害No.3-0003修正 ?>
</div>

<hr color="#DED7C8" />
