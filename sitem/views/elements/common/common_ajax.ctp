<script>
$(document).ready(function(){

	$(function () {
		$.scrollUp({
			animation: 'fade',
		});
	});

	//slider
	$('#slider1').bxSlider({
		auto:true,
		speed:1000,
		infiniteLoop: true,
		hideControlOnEnd: false,
		slideWidth: 320,
 		pager: false,
		controls: true,
	});

	$('#slider2').bxSlider({
		auto:true,
		speed:1000,
		infiniteLoop: true,
		hideControlOnEnd: false,
 		pager: false,
		controls: true,
	});

	$('#slider3').bxSlider({
		auto:true,
		speed:1000,
		infiniteLoop: true,
		hideControlOnEnd: false,
 		pager: false,
		controls: false,
	});

	$('#topicSlider').bxSlider({
		auto:true,
		speed:1000,
		infiniteLoop: true,
		hideControlOnEnd: false,
 		pager: false,
		controls: false,
		pause: 8000,
	});

	$('#girlsSlider').bxSlider({
		auto:true,
		speed:1000,
		infiniteLoop: true,
		hideControlOnEnd: false,
		slideWidth: 615,
 		pager: false,
		controls: true,
	});

	//slideshow
	$('#newSlideShow').bxSlider({
		minSlides: 6,
	  	maxSlides: 6,
	  	slideWidth: 590,
	  	ticker: true,
	  	speed: 24000
	});

	$('#girlsSlideShow').bxSlider({
		minSlides: 4,
	  	maxSlides: 4,
	  	slideWidth: 606,
	  	ticker: true,
	  	speed: 70000
	});

	$(function() {

		var logined = <?php echo (empty($parent_reviewer) ? 'false' : 'true'); ?>;

		//tabs
		$(".content_wrap").hide();
		$("#tabs li:first").addClass("").show();
		$(".content_wrap:first").show();

		$("#tabs li").click(function() {
			$("#tabs li").removeClass("select");
			$(this).addClass("select");

			var num = $("#tabs li").index(this);
			$(".content_wrap").not(num).fadeOut(1000).eq(num).fadeIn(1000);
			return false;
		});

	    // 女の子選択ダイアログ
	    $('.frameOpen').click(function(e) {
	        var shop_id = $(this).attr('data');

	        if(!logined) {
	        	alert('口コミの投稿にはログインが必要です。');
	        	return;
	        }

	        if(shop_id == undefined || shop_id == '') {
	        	return;
	        }

	        // 女の子一覧構築
	        jQuery.ajax({
				type: "POST",
				url: "/review/ajax_get_girls",
				data: "data[Shop][shop_id]=" + shop_id,
				dataType: "html",
				cache: false,
				success: function(html){

					$("#frame").empty();
					$("#frame").append(html);

			        $('#frame').lightbox_me({
			            centered: true,
			            onLoad: function() {
			                $('#frame').find('input:first').focus()
			            }
			        });
				},
				error: function() {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});

	        e.preventDefault();
	    });

	    // ありがとうボタン
		$('.thanksBtn').click(function(e) {	        var review_id = $(this).attr('data');

	        if(review_id == undefined) {
	        	return;
	        }

	        // ありがとうリクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_good/"　+ review_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					if(data.success) {
						alert('投稿されました');
					} else {
						alert(data.message);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});

	    // お気に入り店舗ボタン
		$('.bookmarkShop').click(function(e) {
			var mode = $(this).attr('mode');
	        var user_id = $(this).attr('data');

	        if(user_id == undefined) {
	        	return;
	        }

	        if(mode == 'delete') {
	        	if(!confirm('お気に入りを削除します。宜しいですか？')) {
	        		return;
	        	}
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_bookmark_shop/"　+ mode + '/' + user_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					alert(data.message);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});

	    // お気に入り女の子ボタン
		$('.bookmarkGirl').click(function(e) {
			var mode = $(this).attr('mode');
	        var girl_id = $(this).attr('data');

	        if(girl_id == undefined) {
	        	return;
	        }

	        if(mode == 'delete') {
	        	if(!confirm('お気に入りを削除します。宜しいですか？')) {
	        		return;
	        	}
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_bookmark_girl/"　+ mode + '/' + girl_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					alert(data.message);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});

	    // お気に入りレビューボタン
		$('.bookmarkReview').click(function(e) {
			var mode = $(this).attr('mode');
	        var review_id = $(this).attr('data');

	        if(review_id == undefined) {
	        	return;
	        }

	        if(mode == 'delete') {
	        	if(!confirm('お気に入りを削除します。宜しいですか？')) {
	        		return;
	        	}
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_bookmark_review/"　+ mode + '/' + review_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					alert(data.message);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
				}
			});
	        e.preventDefault();
		});

	    // フォローボタン
		$('.followReviewer').click(function(e) {
			var mode = $(this).attr('mode');
	        var reviewer_id = $(this).attr('data');

	        if(reviewer_id == undefined) {
	        	return;
	        }

	        if(mode == 'delete') {
	        	if(!confirm('レビュワーを解除します。宜しいですか？')) {
	        		return;
	        	}
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/update_follow/"　+ mode + '/' + reviewer_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){
					alert(data.message);
					location.reload();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});

		// 自分の口コミを削除する
		$('.ReviewDelete').click(function(e){
			if (!confirm('この口コミを削除します。削除して宜しいですか？'))
			{
				return false;
			}
			var mode = $(this).attr('mode');
			var review_id = $(this).attr('data');
	        jQuery.ajax({
				type: "get",
				url: "/delete_review/"　+ mode + '/' + review_id,
				data: "",
				dataType: "html",
				cache: false,
				success: function(data){
					alert('口コミを削除いたしました。');
					location.reload();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
				}
				//e.preventDefault();
			});
		});



	});
});
</script>
