<h2 class="pickup_heading">このお店の<span>P</span>ick<span>U</span>p口コミ</h2>
	<div class="new_reviewArea clearfix">
		<div class="pickup clearfix">	<div class="reviewer clearfix">
		<div class="mark"><img src="/img/mark_pickup.png"></div>
		<div class="icon"><a href="#"><img src="/img/tab_topictoukou_img2.png" alt="" height="30" width="30" class="img_reviewer"></a></div>
		<div>

<?php if($record['Review']['reviewer_id'] == 0) { ?>
            <p><?php echo $record['Review']['post_name']; ?>さん</p>
<?php } else { ?>
            <p><a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>"><?php echo $record['Review']['post_name']; ?></a>さん</p>
<?php } ?>
            <p><?php echo $textCommon->escape_empty($record, 'MReviewersAge.name', '不明'); ?>
<?php if($record['Review']['reviewer_id'] == 0) { ?>
／口コミ投稿：非会員
<?php } else { ?>
／<a href="<?php echo $linkCommon->get_reviewer_reviews($record); ?>">口コミ投稿：<?php echo $record['0']['review_cnt'] . '件'; ?></a>
<?php } ?>		</div>
		<p class="whiteBtn favoriteBtn"><a href="#">このクチコミを保存</a></p>
	</div>
	<div class="reviewText clearfix">
		<div class="clearfix">
            <p class="img_girl">
                <?php echo $imgCommon->get_girl_with_time($record, 's', 1); ?>
            </p>
			<div class="summary">
                <p class="shop"><?php echo $record['Shop']['name']; ?></p>
                <p class="name"><?php echo $record['Girl']['name']; ?></p>
                <p class="scoreInfo">
<?php
for($ii = 1; $ii <= 5; $ii++) {
    $suffix = ($ii <= $record[0]['girl_avg']) ? 'on' : 'off';
?>
                    <img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>
                    <span class="score"><?php echo round($record[0]['girl_avg'], 2); ?></span>
                </p>
                <p class="scoreType">[<span class="point">L</span>ルックス -- /<span class="point">S</span>サービス -- /<span class="point">C</span>性格 -- ]</p>
                <div class="course">
                    <span>コース料金／--分 --円</span>
                    <span>ご利用場所／--</span>
                </div>
                <div class="course_mark">
                    <span class="target">写真指名</span>
                    <span class="new">新規</span>
                </div>
			</div>
		</div>
        <div>


<?php if($record['Review']['delete_flag'] == Review::DELETE_FLAG_COMMENT_ONLY) { ?>

<?php } else if($record['Review']['delete_flag'] == Review::DELETE_FLAG_SAVE_POINTS) { ?>
						<p class="txt">このコメントは削除依頼により削除されました。</p>

<?php } else if($record['Review']['member_only'] == 1) { ?>
						<p class="tt mar_t_05"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>" style="text-decoration:none;">会員限定の記事です。</a></p>

<?php } else { ?>
		<!-- <p class="tt">【レビュー】<?php echo $record['Girl']['name']; ?>ちゃん 口コミ報告</p> -->			<p class="tt"><?php echo $record['Review']['post_title']; ?></p>
            <p class="txt"><?php echo $record['Review']['comment']; ?></p>
<?php } ?>



        </div>
    </div>
    <p class="whiteBtn reportBtn"><a href="#">通報する</a></p>
    <div class="thanksBtn">
        <p>参考になりました</p>
        <p class="redBtn"><a href="#">ありがとう</a></p>
    </div>
</div><div style="background-color:#ddddff;padding:10px 10px 10px 10px;">店舗からのお返事:<br /><?php if(isset($record['ReviewReply'][0]['ReviewReply']) && count($record['ReviewReply'][0]['ReviewReply']) > 0 && $record['ReviewReply'][0]['ReviewReply']['comment'] != ''){ ?><?php echo nl2br(htmlspecialchars($record['ReviewReply'][0]['ReviewReply']['comment'])); ?><?php } else { ?>まだお返事はありません<br /><?php } ?></div><div style="background-color:#ffdddd;padding:10px 10px 10px 10px;">女の子からのお返事:<br /><?php if(isset($record['ReviewReply'][1]['ReviewReply']) && count($record['ReviewReply'][1]['ReviewReply']) > 0 && $record['ReviewReply'][1]['ReviewReply']['comment'] != ''){ ?><?php echo nl2br(htmlspecialchars($record['ReviewReply'][1]['ReviewReply']['comment'])); ?><?php } else { ?>まだお返事はありません<br /><?php } ?></div>