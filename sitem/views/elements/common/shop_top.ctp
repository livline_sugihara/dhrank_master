<?php if($parent_shop['UsersAuthority']['is_shop_header_image'] == 1) { ?>
    <div id="shopMainImg">
        <?php echo $imgCommon->get_shop_with_time($parent_shop, 'header', true, 'width: 675px;'); ?>
    </div>
<?php } ?>

<?php // print_r($parent_shop); ?>

    <!-- shopInfo -->
    <div class="shopInfo" class="clearfix">
                    
        <p class="img"><?php echo $imgCommon->get_shop_with_time($parent_shop, 'icon'); ?></p>
        <div class="clearfix">
            <div class="summary">
                <p class="tt"><?php echo $parent_shop['Shop']['name']; ?></p>
                <p class="caption"><?php echo $parent_shop['MShopsBusinessCategory']['name']; ?></p>
                
                <div class="pay_mark clearfix">
                    <?php if($parent_shop['Shop']['service01'] == 1) { ?><p class="pay_yoyaku">先行予約OK</p><?php } ?>
                    <?php if($parent_shop['Shop']['service02'] == 1) { ?><p class="pay_credit">クレジットOK</p><?php } ?>
                    <?php if($parent_shop['Shop']['service03'] == 1) { ?><p class="pay_ryoshu">領収書OK</p><?php } ?>
                    <?php if($parent_shop['Shop']['service04'] == 1) { ?><p class="pay_machi">待ち合わせOK</p><?php } ?>
                    <?php if($parent_shop['Shop']['service05'] == 1) { ?><p class="pay_cosplay">コスプレあり</p><?php } ?>
                    <?php if($parent_shop['Shop']['service06'] == 1) { ?><p class="pay_coupon">クーポンあり</p><?php } ?>
                </div>
                
                <div class="biz">
<?php
$opentime = date('H:i', strtotime($parent_shop['System']['open_time']));
$closetime = date('H:i', strtotime($parent_shop['System']['close_time']));
?>
                    <p class="caption">営業時間・<?php echo $opentime; ?>〜<?php echo $closetime; ?>　最低料金・<?php echo $parent_shop['System']['price_cost']; ?>円〜<br />住所・<?php echo $parent_shop['System']['address']; ?></p>
                </div>
                <div class="shopNum">
                    <p class="zaiseki">在籍数:<span class="num"><?php echo $count_girls; ?>人</span></p>
                    <p class="kuchikomi2">口コミ:<span class="num"><?php echo $review_data[0]['count']; ?>件</span></p>
                </div>              
            </div>
            <div class="btn">
                <p class="redBtn entryreviewBtn frameOpen" data='<?php echo $parent_shop['Shop']['id']; ?>'><a href="#">口コミを投稿する</a></p>
<?php if(isset($review_flag) && $review_flag) { ?>
        <p class="whiteBtn favoriteBtn bookmarkShop" data="<?php echo $parent_shop['User']['id']; ?>" mode="delete"><a href="#">お気に入りに解除</a></p>
<?php } else if(isset($myreview_flag) && $myreview_flag) { ?>
        <p class="whiteBtn favoriteBtn bookmarkShop"><a href="#">このクチコミを編集</a></p>
<?php } else { ?>
        <p class="whiteBtn favoriteBtn bookmarkShop" data="<?php echo $parent_shop['User']['id']; ?>" mode="add"><a href="#">お気に入りに保存</a></p>
<?php } ?>

            </div>                  
        </div>
            
    </div>
    <!-- /shopInfo -->

    <!-- tab -->
    <?php echo $this->element('common/shop_tabs'); ?>
    <!-- /tab -->
