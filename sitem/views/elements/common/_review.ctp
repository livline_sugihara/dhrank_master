<?php
// レビューページからの呼び出しかどうか
$review_flag = isset($reviewer) && $reviewer;
// Myレビューからの呼び出しかどうか
$myreview_flag = isset($myreview) && $myreview;
?><div class="box clearfix">

    <div class="reviewer clearfix">
        <div class="icon">
<?php if($record['Review']['reviewer_id'] == 0) { ?>
	<?php echo $imgCommon->get_reviewer_avatar($record, array(40,40), 'img_reviewer'); ?>
<?php } else { ?>
	<a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>"><?php echo $imgCommon->get_reviewer_avatar($record, array(40,40), 'img_reviewer'); ?></a>
<?php } ?>
        </div>
        <div>
<?php if($record['Review']['reviewer_id'] == 0) { ?>
            <p><?php echo $record['Review']['post_name']; ?>さん</p>
<?php } else { ?>
            <p><a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>"><?php echo $record['Review']['post_name']; ?></a>さん</p>
<?php } ?>
            <p><?php echo $textCommon->escape_empty($record, 'MReviewersAge.name', '不明'); ?>
<?php if($record['Review']['reviewer_id'] == 0) { ?>
／口コミ投稿：非会員
<?php } else { ?>
／<a href="<?php echo $linkCommon->get_reviewer_reviews($record); ?>">口コミ投稿：<?php echo $record['0']['review_cnt'] . '件'; ?></a>
<?php } ?>
        </div>
<?php if($review_flag) { ?>
        <!-- <p class="whiteBtn favoriteBtn bookmarkReview" data="<?php echo $record['Review']['id']; ?>" mode="delete"><a href="#">このクチコミを削除</a></p> -->
<?php } else if($myreview_flag) { ?>
        <p class="whiteBtn favoriteBtn ReviewDelete" data="<?php echo $record['Review']['id']; ?>" mode="delete"><a href="#">このクチコミを削除</a></p>
        <p class="whiteBtn favoriteBtn"><a href="<?php echo $linkCommon->get_mypage_edit_review($record); ?>">このクチコミを編集</a></p>
<?php } else { ?>
        <p class="whiteBtn favoriteBtn bookmarkReview" data="<?php echo $record['Review']['id']; ?>" mode="add"><a href="#">このクチコミを保存</a></p>
<?php } ?>
    </div>
    <div class="reviewText clearfix">
        <div class="clearfix">
            <p class="img_girl">
                <a href="<?php echo $linkCommon->get_user_girl($record); ?>"><?php echo $imgCommon->get_girl_with_time($record, 's', 1); ?></a>
            </p>
            <div class="summary">
<?php /* エラー修正タカモト
                <p class="shop"><?php echo $record['Review']['shop_name']; ?></p>
                <p class="name"><?php echo $record['Review']['girl_name']; ?></p>
*/ ?>
							<p class="shop"><?php echo $record['Shop']['name']; ?>				<span style="float:right;font-size:80%;color:#000000;"><?php echo date('Y.m.d', strtotime($record['Review']['created'])); ?></span>
</p>
							<p class="name"><?php echo $record['Girl']['name']; ?></p>

                <p class="scoreInfo">
<?php
for($ii = 1; $ii <= 5; $ii++) {
    $suffix = ($ii <= $record[0]['girl_avg']) ? 'on' : 'off';
?>
                    <img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>
                    <span class="score"><?php echo round($record[0]['girl_avg'], 2); ?></span>
                </p>
                <p class="scoreType">[<span class="point">L</span>ルックス <?php echo $textCommon->escape_empty($record, 'Review.score_girl_looks', '-'); ?> /<span class="point">S</span>サービス <?php echo $textCommon->escape_empty($record, 'Review.score_girl_service', '-'); ?> /<span class="point">C</span>性格 <?php echo $textCommon->escape_empty($record, 'Review.score_girl_character', '-'); ?> ]</p>
                <div class="course">
                    <span>コース料金／<?php echo $textCommon->escape_empty($record, 'Review.course_minute', '--'); ?>分 <?php echo $textCommon->escape_empty($record, 'Review.course_cost', '--'); ?>円</span>
                    <span>ご利用場所／<?php echo $textCommon->escape_empty($record, 'MReviewsPlace.name', '不明'); ?></span>
                </div>
                <div class="course_mark">
                    <span class="target"><?php echo $textCommon->escape_empty($record, 'Review.appointed_sub_name', '不明'); ?></span>
                    <span class="new"><?php echo $textCommon->escape_empty($record, 'Review.appointed_type_name', '不明'); ?></span>
                </div>
            </div>
        </div>
        <div>


<?php if($record['Review']['delete_flag'] == Review::DELETE_FLAG_COMMENT_ONLY) { ?>

<?php } else if($record['Review']['not_publish'] == 1) { ?>
            <p class="txt">この口コミは非掲載です。</p>
<?php } else if($record['Review']['delete_flag'] == Review::DELETE_FLAG_SAVE_POINTS) { ?>
			<p class="txt">このコメントは削除依頼により削除されました。</p>
<?php } else if($record['Review']['member_only'] == 1) { ?>
						<p class="tt mar_t_05"><a href="<?php echo $linkCommon->get_accounts_register(null); ?>" style="text-decoration:none;">会員限定の記事です。</a></p>
<?php } else { ?>
            <!-- <p class="tt">【レビュー】<?php echo $record['Girl']['name']; ?>ちゃん 口コミ報告</p> -->
            <p class="tt"><?php echo $record['Review']['post_title']; ?></p>
            <p class="txt"><?php echo nl2br($record['Review']['comment']); ?></p>
<?php } ?>
        </div>
    </div>
<?php if($review_flag || $myreview_flag) { ?>

<?php } else { ?>
    <p class="whiteBtn reportBtn"><a href="<?php echo $linkCommon->get_review_report($record); ?>">通報する</a></p>
    <div class="thanksBtn">
        <p>参考になりました</p>
        <p class="redBtn thanksBtn" data="<?php echo $record['Review']['id']; ?>">
            <a href="#">
                ありがとう
            </a>
        </p>
    </div>
<?php } ?>
</div>
<!--
<div style="background-color:#ddddff;padding:10px 10px 10px 10px;">
店舗からのお返事:<br />
<?php if(isset($record['ReviewReply'][0]['ReviewReply']) && count($record['ReviewReply'][0]['ReviewReply']) > 0 && $record['ReviewReply'][0]['ReviewReply']['comment'] != ''){ ?>
<?php echo nl2br(htmlspecialchars($record['ReviewReply'][0]['ReviewReply']['comment'])); ?>
<?php } else { ?>
まだお返事はありません<br />
<?php } ?>
</div>
<div style="background-color:#ffdddd;padding:10px 10px 10px 10px;">
女の子からのお返事:<br />
<?php if(isset($record['ReviewReply'][1]['ReviewReply']) && count($record['ReviewReply'][1]['ReviewReply']) > 0 && $record['ReviewReply'][1]['ReviewReply']['comment'] != ''){ ?>
<?php echo nl2br(htmlspecialchars($record['ReviewReply'][1]['ReviewReply']['comment'])); ?>
<?php } else { ?>
まだお返事はありません<br />
<?php } ?>
</div>
-->
