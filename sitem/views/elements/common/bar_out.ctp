<!-- info_bar -->
<div id="info_bar" class="out">
	<div class="container">
		<div class="box clearfix">
			<div class="list">
				<ul class="clearfix">
<?php
// ログイン時
if(!empty($parent_reviewer)) {
?>
					<li class="nickname">こんにちは <?php echo $parent_reviewer['Reviewer']['handle']; ?>さん </li>
					<li class="redBtn barBtn"><a href="<?php echo $linkCommon->get_mypage_top($parent_area); ?>">Myページ</a></li>
					<li class="redBtn barBtn"><a href="<?php echo $linkCommon->get_accounts_logout($parent_area)?>">ログアウト</a></li>
<?php
// 未ログイン時
} else {
?>
					<li class="redBtn barBtn"><a href="<?php echo $linkCommon->get_accounts_login($parent_area)?>">ログイン</a></li>
					<li class="redBtn barBtn"><a href="<?php echo $linkCommon->get_accounts_register($parent_area)?>">新規レビュアー会員登録</a></li>
<?php } ?>
                </ul>
			</div>
		</div>
	</div>
</div>
<!-- / info_bar -->