<h2 class="shop_h2">
<?php echo $parent_shop['Shop']['name']?> <span class="shop_h2_genre">(<?php echo $parent_shop['MShopsBusinessCategory']['name']?>)</span>
<span class="shop_h2_bookmark">
<?php if($parent_reviewer){?>
<?php
$this->Html->script('jquery');

$this->Js->get('#bookmark_shop');
$request = $this->Js->request('/' . $parent_shop['User']['id'] . '/getAjax_update_bookmark_shop/',
		array(
				'method' => 'get',
				'sync' => true,
				'update' => '#bookmark_shop',
		)
);
$this->Js->event('click',$request);
echo $this->Js->writeBuffer();
?>
<a href="#">
<span id="bookmark_shop">
	<?php if($bookmark_shop == 0){?>
	<img src="/images/bookmark.png" />
	<?php }else{?>
	<img src="/images/bookmark_off.png" />
	<?php }?>
</span>
</a>
<?php }else{?>
<a href="<?php echo $linkCommon->get_accounts_login($parent_area);?>"><img src="/images/bookmark.png" /></a>
<?php }?>
</span>
</h2>


<div class="shop_top">
	<div class="shop_top_left">
    	<div style="font-weight:bold;font-size:14px;;line-height:18px;margin-bottom:4px;">総合評価</div>
    	<div class="shop_top_left_score1">
    	<div class="shop_top_left_score2">
        	<div style="margin:6px;">
        	<?php for($i = 0; $i < 5; $i++){?><img src="/images/star_<?php echo ($i+1 <= round($review_data[0]['total']))?'on':'off';?>.png" class="kuchikomi_heart" /><?php }?>
            </div>
				<div class="shop_top_left_score3">
                	<span class="spot5"><?php echo round($review_data[0]['total'],2)?></span>
                </div>
            <span style="float:left;margin-left:4px;margin-top:3px;">口コミ総数：</span><span style="float:right;margin-right:4px;margin-top:3px;"><?php echo $review_data[0]['count']?>件</span>
    	</div>
    	</div>
	</div>
	<div class="shop_top_center">
    <table class="shop_top_table">
		<?php for($i = 5; $i > 0; $i--){?>
		<?php $review_data_mark = Set::extract ('/0[avg=' . $i . ']', $review_data_marks);?>
		<tr>
			<?php if($i%2 == 1){?>
			<td class="shop_top_td1">
			<?php }else{?>
			<td class="shop_top_td2">
			<?php }?>
			<?php for($j = 0; $j < 5; $j++){?><img src="/images/star_<?php echo ($j+1 <= $i)?'on':'off';?>.png" class="kuchikomi_heart" /><?php }?></td>
			<?php if($i%2 == 1){?>
			<td class="shop_top_td1_1">
			<?php }else{?>
			<td class="shop_top_td2">
			<?php }?>
				<?php if(!empty($review_data_mark)){?>
				<img src="/images/graph5.png" width="<?php echo round(100 / $review_data_mark[0][0]['max'] * $review_data_mark[0][0]['count']) ?>px" height="10px" />
				<?php }?>
			</td>
			<td class="shop_top_td3">
				<?php if(!empty($review_data_mark)){?>
				<?php echo '(' . $review_data_mark[0][0]['count'] . ')'?>
				<?php }else{?>
				(0)
				<?php }?>
			</td>
		</tr>
		<?php }?>
    </table>
	</div>
	<div class="shop_top_right">
    	<div class="shop_top_otoiawase">お問い合わせはこちらから</div>
        <img src="/images/tel.png" style="margin-left:10px;margin-bottom:0px;" /><span class="shop_tel"><?php echo $parent_shop['Shop']['phone_number']?></span>
      	<div class="shop_top_otsutae">
        ※お問い合わせの際に<br />
		「<span class="spot6"><?php echo $parent_area['LargeArea']['name']?>デリヘルランキングを見た</span>」とお伝えください。
        </div>
		<?php if($parent_shop['User']['is_official_link_display'] && !empty($parent_shop['Shop']['url_pc'])){?>
		<div style="margin-top:13px;"><center><a href="<?php echo $parent_shop['Shop']['url_pc']?>" target="_blank" class="btn_official">オフィシャルサイトへリンク</a></center></div>
		<?php }?>

	</div>
</div>

<div class="shop_score1">
<img src="/images/icon_shop.png" style="margin-right:2px;vertical-align: top;" />
<span class="shop_score3">電話対応<span class="spot6"><?php echo round($review_data[0]['score_shop_tel'],1)?></span>|到着時間<span class="spot6"><?php echo round($review_data[0]['score_shop_time'],1)?></span>|コスパ<span class="spot6"><?php echo round($review_data[0]['score_shop_cost'],1)?></span></span>
<img src="/images/icon_girl.png" style="margin-right:2px;vertical-align:top;" />
<span class="shop_score4">第一印象  <?php echo round($review_data[0]['score_girl_first_impression'],1)?> | 言葉遣い <?php echo round($review_data[0]['score_girl_word'],1)?> | サービス  <?php echo round($review_data[0]['score_girl_service'],1)?> | 感度 <?php echo round($review_data[0]['score_girl_sensitivity'],1)?> | スタイル  <?php echo round($review_data[0]['score_girl_style'],1)?> | あえぎ声  <?php echo round($review_data[0]['score_girl_voice'],1)?></span>
</div>



<?php if($this->params["action"] == 'index' || $this->params["action"] == 'add_review_error'){?>
<ul class="shop_menu">
	<li class="shop_menu_li_on"><a href="<?php echo $linkCommon->get_user_shop($parent_shop)?>" class="shop_menu_text">店舗トップ</a></li>
	<li class="shop_menu_li"><a href="<?php echo $linkCommon->get_user_girls($parent_shop)?>" class="shop_menu_text">在籍の女の子</a></li>
	<li class="shop_menu_li"><a href="<?php echo $linkCommon->get_user_systems($parent_shop)?>" class="shop_menu_text">料金・システム</a></li>
	<li class="shop_menu_li_green"><a href="<?php echo $linkCommon->get_user_add_review($parent_shop)?>" class="shop_menu_text_green">口コミを投稿する</a></li>
</ul>
<?php }elseif($this->params["action"] == 'systems'){?>
<ul class="shop_menu">
	<li class="shop_menu_li"><a href="<?php echo $linkCommon->get_user_shop($parent_shop)?>" class="shop_menu_text">店舗トップ</a></li>
	<li class="shop_menu_li"><a href="<?php echo $linkCommon->get_user_girls($parent_shop)?>" class="shop_menu_text">在籍の女の子</a></li>
	<li class="shop_menu_li_on"><a href="<?php echo $linkCommon->get_user_systems($parent_shop)?>" class="shop_menu_text">料金・システム</a></li>
	<li class="shop_menu_li_green"><a href="<?php echo $linkCommon->get_user_add_review($parent_shop)?>" class="shop_menu_text_green">口コミを投稿する</a></li>
</ul>
<?php }elseif($this->params["action"] == 'add_review' || $this->params["action"] == 'add_review_confirm'){?>
<ul class="shop_menu">
	<li class="shop_menu_li"><a href="<?php echo $linkCommon->get_user_shop($parent_shop)?>" class="shop_menu_text">店舗トップ</a></li>
	<li class="shop_menu_li"><a href="<?php echo $linkCommon->get_user_girls($parent_shop)?>" class="shop_menu_text">在籍の女の子</a></li>
	<li class="shop_menu_li"><a href="<?php echo $linkCommon->get_user_systems($parent_shop)?>" class="shop_menu_text">料金・システム</a></li>
	<li class="shop_menu_li_green_on"><a href="<?php echo $linkCommon->get_user_add_review($parent_shop)?>" class="shop_menu_text_green">口コミを投稿する</a></li>
</ul>
<?php }else{?>
<ul class="shop_menu">
	<li class="shop_menu_li"><a href="<?php echo $linkCommon->get_user_shop($parent_shop)?>" class="shop_menu_text">店舗トップ</a></li>
	<li class="shop_menu_li_on"><a href="<?php echo $linkCommon->get_user_girls($parent_shop)?>" class="shop_menu_text">在籍の女の子</a></li>
	<li class="shop_menu_li"><a href="<?php echo $linkCommon->get_user_systems($parent_shop)?>" class="shop_menu_text">料金・システム</a></li>
	<li class="shop_menu_li_green"><a href="<?php echo $linkCommon->get_user_add_review($parent_shop)?>" class="shop_menu_text_green">口コミを投稿する</a></li>
</ul>
<?php }?>

<br class="clear" />