<footer>
	<div id="footer_nav">
		<div class="container">
			<ul class="clearfix">
				<li><a href="<?php echo $linkCommon->get_accounts_login(); ?>">レビュアー会員</a></li>
				<li><a href="<?php echo APP_MEMBER_URL; ?>">店舗会員</a></li>
				<li><a href="/front/about">当サイトについて</a></li>
				<li><a href="/front/term">ご利用規約</a></li>
				<li><a href="/contact">お問い合わせ</a></li>
			</ul>
		</div>
	</div>

	<div id="footer_main">
		<div class="container_footer clearfix">
			<div id="footer_main01">
				<p id="footer_logo"><a href="<?php echo APP_ALLAREA_URL; ?>"><img src="/img/footer_logo.png" alt="Deri" height="38" width="96"></a></p>

				<p id="copyright">Copyright © <br /><?php echo $parent_area['LargeArea']['name']?>デリヘルクチコミランキング<br />All Rights Reserved.<br>
				転載禁止</p>
			</div>

			<div id="footer_main02">
            
				<div class="list">
					<div class="float_l">
                    	<p class="tt">北海道</p>
						<p class="txt mb10"><a href="http://hokkaido.dhrank.com/top">北海道(札幌)</a> </p>
						<p class="tt">北信越</p>
						<p class="txt mb10"><a href="http://toyama.dhrank.com/top">富山</a> | <a href="http://ishikawa.dhrank.com/top">石川</a> | <a href="http://fukui.dhrank.com/top">福井</a> | <a href="http://nagano.dhrank.com/top">長野</a> | <a href="http://niigata.dhrank.com/top">新潟</a> | <a href="http://yamanashi.dhrank.com/top">山梨</a></p>
						<p class="tt">関東</p>
						<p class="txt mb10"><a href="http://shinjyuku.dhrank.com/top">新宿</a> | <a href="http://ikebukuro.dhrank.com/top">池袋</a> | <a href="http://shibuya.dhrank.com/top">渋谷</a> | <a href="http://shinagawagotanda.dhrank.com/top">品川・五反田</a> | <a href="http://nishitokyo.dhrank.com/top">西東京</a><br>
						<a href="http://kinshicho.dhrank.com/top">錦糸町</a> | <a href="http://uenouguisudani.dhrank.com/top">上野・鴬谷</a> | <a href="http://kanagawa.dhrank.com/top">神奈川</a> | <a href="http://saitama.dhrank.com/top">埼玉</a> | <a href="http://chiba.dhrank.com/top">千葉</a></p>
						<p class="tt">関西</p>
						<p class="txt mb10"><a href="http://osaka.dhrank.com/top">大阪</a> | <a href="http://kyoto.dhrank.com/top">京都</a> | <a href="http://hyogo.dhrank.com/top">兵庫</a> | <a href="http://shiga.dhrank.com/top">滋賀</a> | <a href="http://nara.dhrank.com/top">奈良</a> | <a href="http://wakayama.dhrank.com/top">和歌山</a></p>
						<p class="tt">四国</p>
						<p class="txt"><a href="http://ehime.dhrank.com/top">愛媛</a> | <a href="http://kagawa.dhrank.com/top">香川</a> | <a href="http://kochi.dhrank.com/top">高知</a> | <a href="http://tokushima.dhrank.com/top">徳島</a></p>
					</div>
					<div class="float_r">
						<p class="tt">東北</p>
						<p class="txt mb10"><a href="http://aomori.dhrank.com/top">青森</a> | <a href="http://iwate.dhrank.com/top">岩手</a> | <a href="http://miyagi.dhrank.com/top">宮城</a> |  <a href="http://akita.dhrank.com/top">秋田</a> | <a href="http://yamagata.dhrank.com/top">山形</a> | <a href="http://fukushima.dhrank.com/top">福島</a></p>
						<p class="tt">北関東</p>
						<p class="txt mb10">
						<a href="http://ibaraki.dhrank.com/top">茨城</a> | <a href="http://tochigi.dhrank.com/top">栃木</a> | <a href="http://gunma.dhrank.com/top">群馬</a></p>
						<p class="tt">東海</p>
						<p class="txt mb10"><a href="http://aichi.dhrank.com/top">愛知(名古屋)</a> | <a href="http://gifu.dhrank.com/top">岐阜</a> | <a href="http://mie.dhrank.com/top">三重</a> | <a href="http://shizuoka.dhrank.com/top">静岡</a></p>
						<p class="tt">中国</p>
						<p class="txt mb10"><a href="http://tottori.dhrank.com/top">鳥取</a> | <a href="http://shimane.dhrank.com/top">島根</a> | <a href="http://okayama.dhrank.com/top">岡山</a> | <a href="http://hiroshima.dhrank.com/top">広島</a> | <a href="http://yamaguchi.dhrank.com/top">山口</a></p>
						<p class="tt">九州</p>
						<p class="txt mb10"><a href="http://fukuoka.dhrank.com/top">福岡</a> | <a href="http://saga.dhrank.com/top">佐賀</a> | <a href="http://oita.dhrank.com/top">大分</a> | <a href="http://nagasaki.dhrank.com/top">長崎</a> | <a href="http://kumamoto.dhrank.com/top">熊本</a> | <a href="http://miyazaki.dhrank.com/top">宮崎</a><br /><a href="http://kagoshima.dhrank.com/top">鹿児島</a> | <a href="http://okinawa.dhrank.com/top">沖縄</a></p>
					</div>
				</div>
			</div>

		</div>
	</div>
</footer>

<?php /*?><?php //start---2013/3/13 障害No.1-6-0001修正 ?>
<?php //if(isset($mediaCategory) && $mediaCategory == 's'){?>
<?php if(isset($mediaCategory) && $mediaCategory == 's' && $this->params["controller"] != 'reviewers' && $this->params["controller"] != 'accounts'){?>
<?php //end---2013/3/13 障害No.1-6-0001修正 ?>
<div>
	PC版 ｜
	<a href="<?php echo substr($html->webroot,0,-1) . '/s' . $str = rtrim($base_url, '/') . '/view:s';?>">スマートフォン版</a>
</div>
<?php }?>
<div>
	レビュアー会員・<a href="<?php echo $linkCommon->get_accounts_login($parent_area)?>">ログイン</a>/<a href="<?php echo $linkCommon->get_accounts_register($parent_area)?>">新規登録</a> |
	<a href="<?php echo APP_MEMBER_URL?>">店舗会員ログイン</a> |
	<a href="<?php echo $linkCommon->get_abouts($parent_area)?>">当サイトについて</a> |
	<a href="<?php echo $linkCommon->get_terms($parent_area)?>">ご利用規約</a> |
	<a href="<?php echo $linkCommon->get_contacts($parent_area)?>">お問い合わせ</a>
</div>

<div class="footer_link">
	<?php foreach($large_area_list as $key => $record) {?>
	<a href="<?php echo $record['LargeArea']['url']?>" target="_blank">
		<?php echo $record['LargeArea']['name']?>
	</a>
	<?php if(count($large_area_list) != $key + 1){?>
	 |
	<?php }?>
	<?php } ?>
</div>
<p class="copy">Copyright(c) <?php echo $parent_area['LargeArea']['name']?>デリヘル口コミランキング All Rights Reverved. 無断転載禁止</p><?php */?>