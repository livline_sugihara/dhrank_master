<div class="box clearfix">
    <div class="reviewer clearfix">
        <div class="icon"><a href="#"><img src="/img/tab_topictoukou_img2.png" alt="" height="30" width="30" class="img_reviewer"></a></div>
        <div>
            <p><a href="#">レビュアー名前レビュアー名前</a>さん</p>
            <p>29歳／<a href="#">口コミ投稿：200件</a></p>
        </div>
       
    </div>
    <div class="reviewText clearfix">
        <div class="clearfix">
            <p class="img_girl"><img src="/img/heavy_review_test_img2.png" alt="" height="100" width="75"></p>
            <div class="summary">
                <p class="shop">店名店名</p>
                <p class="name">ももかちゃん</p>
                <p class="scoreInfo">
                    <img src="/img/star_on.png">
                    <img src="/img/star_on.png">
                    <img src="/img/star_on.png">
                    <img src="/img/star_off.png">
                    <img src="/img/star_off.png">
                    <span class="score">3.15</span>
                </p>
                <p class="scoreType">[<span class="point">L</span>ルックス3/<span class="point">S</span>サービス3/<span class="point">C</span>性格3]</p>
                <div class="course">
                    <span>コース料金／90分 25,000円</span>
                    <span>ご利用場所／ラブホテル</span>
                </div>
                <div class="course_mark">
                    <span class="target">写真指名</span>
                    <span class="new">新規</span>
                </div>
            </div>
        </div>
        <div>
            <p class="tt">【レビュー】ももかちゃん 口コミ報告</p>
            <p class="txt">自分の彼女かと勘違いさせる魔性の女の子なんだろうね。特別可愛いわけでもなく、<br>
            スタイルいいわけでもないが、ぶりっ子で甘えてくるので、デリってことを忘れてました。完全にはまりました。</p>
        </div>
    </div>
    <!-- <p class="whiteBtn reportBtn"><a href="review_report.php">通報する</a></p>
    <div class="redBtn deleteBtn_edit">
         <a href="#">削除する</a>
    </div> -->
</div>