<h2 class="heading">口コミレビュー</h2>
<div class="new_reviewArea clearfix">
<!-- pie_chart -->
<!--
    <script type="text/javascript">
    var data;
    var plot1;

    data = [
			["ルックス",<?php echo $pie_chart_data['looks_parsent']?>],
			["サービス",<?php echo $pie_chart_data['service_parsent']?>],
			["性格",<?php echo $pie_chart_data['character_parsent']?>]
	];

    $(document).ready(function(){
        plot1 = $.jqplot(
            'chart1',
            [data],
            {
                seriesDefaults: {
                    renderer: jQuery.jqplot.DonutRenderer, // どのグラフを使うか
                    rendererOptions: {
                        dataLabels: ['サービス','ルックス','性格'],
                        showDataLabels: true, // 何のデータかを表示
                        dataLabelThreshold: 0,
                        dataLabelNudge: 12, // ラベルの位置
                        startAngle: 90, //円の角度
                        diameter: 150, // グラフの大きさ
                        innerDiameter: 70,　// ドーナツ内の大きさ
                        shadowAlpha: 0 // グラフに影を付ける場合
                    },

                    // 凡例表示有無・表示位置
                    legend: {
                        show: false,
                        placement: 'insideGrid',
                        background: 'transparent',
                        border: 'solid 0pt'
                    },
                    grid: {
                        borderWidth: 0,
                        background: 'transparent',
                        shadow: false
                    }
                }
            }
        );

        // plot1.series[0]._center[0] : ドーナツグラフの中心座標(x)（<div>内の座標なので、相対座標として扱う）
        // 総回答数をドーナツの中心に表示する
        $('#q1').css("left", (plot1.series[0]._center[0] + document.getElementById('chart1').offsetLeft - (document.getElementById('q1').offsetWidth) / 2.5) + "px");
        $('#q1').css("top", ((document.getElementById('chart1').offsetHeight / 2) + document.getElementById('chart1').offsetTop - (document.getElementById('q1').offsetHeight / 2)) + "px");
    });

    </script>
<!-- 背景 -->
<!-- <div id="chart1" title="ドーナツグラフ" style="width:200px; height:200px; background-color:#ffffff;"></div> -->
<!-- グラフ -->
<!-- <div id="q1" style="text-align:center;
					font-size:5pt;
					color:#000000;
					font-weight:bold;
					position:absolute;
					z-index:100; filter:
					alpha(opacity=100); opacity:1.0;"
>
口コミ数
<br>
<span style="font-size:3pt;"><?php echo $pie_chart_data['reviews_count']; ?></span></div>

<!-- pie_chart -->

<?php
$cnt = 0;$cnt = count($new_shop_review);
for ($ii = 0; $ii < $cnt; $ii++) {
    echo $this->element('common/_review', array("record" => $new_shop_review[$ii]));
}
?>
</div>
