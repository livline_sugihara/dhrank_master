<div class="reviewerIconArea">
    <?php echo $imgCommon->get_reviewer_avatar($reviewer_data, array(100,100)); ?>
</div>
<ul>
    <li<?php if($side_select == 'profile') { ?> class="select"<?php } ?>><a href="<?php echo $linkCommon->get_reviewer_profile($reviewer_data); ?>">プロフィール</a></li>
    <li<?php if($side_select == 'reviews') { ?> class="select"<?php } ?>><a href="<?php echo $linkCommon->get_reviewer_reviews($reviewer_data); ?>">レビュー一覧</a></li>
</ul>
<div class="followBox">
    <p>
        <a href="<?php echo $linkCommon->get_reviewer_follow($reviewer_data); ?>">
            フォローしている(<?php echo count($follow_reviewer);?>)
        </a>
    </p>
    <div class="img clearfix">
<?php
foreach($follow_reviewer AS $ii => $record) {
    if($ii >= 8) break;
?>
        <p>
            <a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>">
                <?php echo $imgCommon->get_reviewer_avatar($record, array(50,50),'img_frame',null,$record['Reviewer']['handle']); ?>
            </a>
        </p>
<?php } ?>
    </div>
</div>
<div class="followBox">
    <p>
        <a href="<?php echo $linkCommon->get_reviewer_follower($reviewer_data); ?>">
            フォローされている(<?php echo count($followed_reviewer);?>)
        </a>
    </p>
    <div class="img clearfix">
<?php
foreach($followed_reviewer AS $ii => $record) {
    if($ii >= 8) break;
?>
        <p>
            <a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>">
                <?php echo $imgCommon->get_reviewer_avatar($record, array(50,50),'img_frame',null,$record['Reviewer']['handle']); ?>
            </a>
        </p>
<?php } ?>
    </div>
</div>
