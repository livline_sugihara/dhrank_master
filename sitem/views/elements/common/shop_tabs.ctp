<ul class="shopTab clearfix tab">
    <li<?php if($this->params['action'] == 'index') { ?> class="selected"<?php } ?>><a href="<?php echo $linkCommon->get_user_shop($parent_shop); ?>">お店情報</a></li>
    <li<?php if($this->params['action'] == 'girls' || $this->params['action'] == 'girl') { ?> class="selected"<?php } ?>><a href="<?php echo $linkCommon->get_user_girls($parent_shop); ?>">女の子一覧</a></li>
<?php
// 有料の場合のみ
if($parent_shop['UsersAuthority']['is_show_all_contents_by_shop'] == 1) {
?>
    <li<?php if($this->params['action'] == 'coupons') { ?> class="selected"<?php } ?>><a href="<?php echo $linkCommon->get_user_coupons($parent_shop); ?>">割引クーポン</a></li>
<?php } ?>
    <li<?php if($this->params['action'] == 'systems') { ?> class="selected"<?php } ?>><a href="<?php echo $linkCommon->get_user_systems($parent_shop); ?>">システム</a></li>
<?php
// 有料の場合のみ
if($parent_shop['UsersAuthority']['is_show_all_contents_by_shop'] == 1) {
?>
    <li class="review<?php if($this->params['action'] == 'reviews' || $this->params['action'] == 'review') { ?> selected<?php } ?>"><a href="<?php echo $linkCommon->get_user_reviews($parent_shop); ?>">口コミ</a></li>
<?php } ?>
</ul>
