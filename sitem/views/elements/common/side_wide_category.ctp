<!-- rankingMenuArea -->
<div class="rankingMenuArea">
	<div class="inner">
		<p class="side_tt"><img src="/img/side_left_label_rank.jpg" alt="デリヘルランキング" height="45" width="240"></p>
		<ul class="list">
<?php
foreach($categories AS $key => $record) {
?>
			<li><a href="<?php echo $linkCommon->get_category_detail($record['MShopsBusinessCategory']['id']); ?>"><?php echo $record['MShopsBusinessCategory']['name']; ?></a></li>
<?php } ?>
		</ul>
	</div>
</div>
<!-- /rankingArea -->
