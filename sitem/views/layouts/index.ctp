<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<?php echo $this->Html->charset(); ?>
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><?php echo $title_for_layout; ?>!</title>

<?php
if(!empty($meta_keywords)){
echo $this->Html->meta('keywords', null, array('content' => $meta_keywords), true);
}
if(!empty($meta_description)){
echo $this->Html->meta('description', null, array('content' => $meta_description), true);
}
?>
<script type="text/javascript" src="<?php echo $parent_area['LargeArea']['url'] ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $parent_area['LargeArea']['url'] ?>js/jquery.rating.js"></script>
</head>
<!-- / head -->

<?php
echo $this->Html->css(array(
	'add',
	'main',
	'normalize',
	'style_new',
	'jquery.bxslider'
	)
);

//echo $scripts_for_layout;
?>

<body>

<?php echo $this->element('common/header', array('searchbar' => false)); ?>

<div id="container">

<div id="contents">


<div id="main_index">

<?php  echo $content_for_layout;  ?>

</div>
<!--/main-->


<div id="footer">

<?php echo $this->element('common/footer'); ?>

</div>
<!--/footer-->


</div>
<!--/contents-->


</div>
<!--/container-->

	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
