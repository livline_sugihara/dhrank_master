<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title><?php echo $title_for_layout; ?></title>
<?php
	if(!empty($meta_keywords)){
		echo $this->Html->meta('keywords', null, array('content' => $meta_keywords), true);
	}
	if(!empty($meta_description)){
		echo $this->Html->meta('description', null, array('content' => $meta_description), true);
	}
?>
	<link href="/css/s/style.css" rel="stylesheet" type="text/css" media="all">
	<link href="/css/s/base.css" rel="stylesheet" type="text/css" media="all">
	<link href="/css/s/top.css" rel="stylesheet" type="text/css" media="all">
	<link href="/css/s/jquery.bxslider.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="/js/s/jquery.js"></script>
	<script type="text/javascript" src="/js/s/jquery.bxslider.js"></script>
	<script type="text/javascript" src="/js/s/jquery.biggerlink.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
	<script type="application/javascript">
	// ajax機能無効化
	$(document).bind("mobileinit", function(){
		$.mobile.ajaxEnabled = false;
		$.mobile.hashListeningEnabled = false;
	});
	</script>
</head>
<body>
	<header>
		<div class="clearfix">
			<div id="head_left">
				<h1><a href="index.html"><img src="/img/s/deri_logo.png" height="42"></a></h1>
				<p>国内最大級の<br>デリヘル口コミサイト</p>
			</div>
			<div id="head_right">
				<ul class="clearfix">
					<li><a href="<?php echo $linkCommon->get_accounts_register($record = null)?>"><img src="/img/s/header_btn01.gif" alt="会員登録" width="44" height="44"></a></li>
					<li><a href="<?php echo $linkCommon->get_accounts_login($record = null)?>"><img src="/img/s/header_btn02.gif" alt="ログイン" width="44" height="44"></a></li>
				</ul>
			</div>
		</div>
	</header>
	<section>
<?php echo $content_for_layout; ?>
	</section>
	<section id="top_banner_area">
		<ul class="list">
			<li><a href="<?php echo $linkCommon->get_accounts_register($record = null)?>"><img src="/img/s/top_banner_01.gif" width="100%"></a></li>
		</ul>
	</section>
	<footer>
		<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left"><!-- 
		--><ul class="bc-inr"><!-- 
		--><li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">HOME</span></a></li><!-- 
		--><li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"></li><!-- 
		--></ul><!-- 
		--></nav>
		<p class="btn_or"><a href="<?php echo $linkCommon->get_accounts_register($record = null)?>">ログイン・新規会員登録</a></p>
		<ul class="fot-link">
			<li><a href="<?php echo $linkCommon->get_abouts($record = null)?>">当サイトについて</a></li>
			<li><a href="<?php echo $linkCommon->get_terms($record = null)?>">ご利用規約</a></li>
			<li><a href="<?php echo $linkCommon->get_contacts($record = null)?>">お問い合わせ</a></li>
		</ul>
<?php /* 非表示
	<div style="text-align:center; margin:5px;">
<?php if(isset($mediaCategory) && $mediaCategory == 's'){?>
		表示：スマートフォン <a href="<?php echo substr($html->webroot,0,-1) . substr(rtrim($base_url, '/'),2) . '/view:pc';?>">PC</a>
<?php }?>
	</div>
<!-- 「LINEで送る」部分ここから -->
	<div style="text-align:center; margin:5px;">
		<script type="text/javascript" src="http://media.line.naver.jp/js/line-button.js" ></script>
		<script type="text/javascript">
			new jp.naver.line.media.LineButton({"pc":false,"lang":"ja","type":"a"});
		</script>
	</div>
<!-- 「LINEで送る」部分ここまで -->

*/ ?>
	</footer>
	<div id="copyright">Copyright © 口コミデリヘルランキング All Rights Reserved.</div>
	<?php /* echo $this->element('sql_dump'); */ ?>
</body>
</html>
