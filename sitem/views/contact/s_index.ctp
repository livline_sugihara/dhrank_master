<div class="link_o"><h2>お問い合わせ</h2></div>

<section class="contact">

	<p class="txt">
    	デリヘル口コミランキングをご覧いただき、ありがとうございます。<br />
		まずはWEBでのお問い合わせ、またはお客様受付窓口までお電話ください。<br />
		各エリアを専門に担当する営業をご手配させていただきます。 
    </p>

</section>
    
<p><br /></p>
    
   
    <h3 class="label">有料掲載希望</h3>
    
<section class="contact">

              <?php echo $form->create(null,array('type'=>'post','action'=> '','enctype' => 'multipart/form-data')); ?>
                <?php echo $form->hidden('Contact.mode', array('value' => 'payment_insert')); ?>
                        
                        <table style="padding:3%;">
                            <tr>
                                <th>掲載希望エリア<br /><span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.insert_area',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.insert_area'); ?>
                                </td>
                                
                        	</tr>
                            <tr>
                                <th>業種 <span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.category_name',array('size' => '30')); ?>
                           			<?php echo $form->error('Contact.category_name'); ?>
                                </td>
                                
                        	</tr>
                            <tr>
                                <th>店舗名 <span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.shop_name',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.shop_name'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご担当者名 <span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.charge_name',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.charge_name'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご担当者様のご連絡先 <span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.charge_contact',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.charge_contact'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご連絡先メールアドレス <span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.emailto',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.emailto'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご連絡可能な時間帯 <span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.contact_time',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.contact_time'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>店舗URL</th>
                                <td>
                                	<?php echo $form->text('Contact.shop_url',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.shop_url'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>届出確認書or許可書</th>
                                <td>
                                	<?php echo $form->file('Contact.sendfile'); ?>
                            		<?php echo $form->error('Contact.sendfile'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご質問・ご要望など</th>
                                <td>
                                	<?php echo $form->textarea('Contact.comment'); ?>
                            		<?php echo $form->error('Contact.comment'); ?>
                                </td>
                        	</tr>
                        </table>
                        
                        <p class="align_c">
							<?php echo $form->submit('入力内容を確認する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'confirm'));?>
						</p>
                        
                        <?php echo $form->end(); ?>
                   
    </section>
    
 <p><br /></p>
 
 
    <h3 class="label">無料掲載希望</h3>
    
<section class="contact">

	<p class="txt" style="color:#cc0000">
    	全ての項目にご記入下さい。
    </p>

        	<?php echo $form->create(null,array('type'=>'post','action'=> '','enctype' => 'multipart/form-data')); ?>
                <?php echo $form->hidden('Contact.mode', array('value' => 'free_insert')); ?>
                        
                        <table style="padding:3%;">
                            <tr>
                                <th>掲載希望エリア</th>
                                <td>
                            		<?php echo $form->text('Contact.insert_area',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.insert_area'); ?>
                                </td>
                                
                        	</tr>
                            <tr>
                                <th>業種</th>
                                <td>
                                	<?php echo $form->text('Contact.category_name',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.category_name'); ?>
                                </td>
                                
                        	</tr>
                            <tr>
                                <th>店舗名</th>
                                <td>
                                	<?php echo $form->text('Contact.shop_name',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.shop_name'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご担当者名</th>
                                <td>
                                	<?php echo $form->text('Contact.charge_name',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.charge_name'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご担当者様のご連絡先</th>
                                <td>
                                	<?php echo $form->text('Contact.charge_contact',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.charge_contact'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご連絡先メールアドレス</th>
                                <td>
                                	<?php echo $form->text('Contact.emailto',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.emailto'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>店舗URL</th>
                                <td>
                                	<?php echo $form->text('Contact.shop_url',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.shop_url'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>店舗電話番号</th>
                                <td>
                                	<?php echo $form->text('Contact.shop_tel',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.shop_tel'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>住所or（…発）</th>
                                <td>
                                	<?php echo $form->text('Contact.shop_address',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.shop_address'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>営業時間</th>
                                <td>
                                	<?php echo $form->text('Contact.shop_business_hours',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.shop_business_hours'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>最低料金</th>
                                <td>
                                	<?php echo $form->text('Contact.lowest_cost',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.lowest_cost'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>先行予約</th>
                                <td>
                                	<?php echo $form->radio('Contact.reserve', $umu_select, array('legend' => false, 'div' => false, 'separator' => '　')); ?>
                            		<?php echo $form->error('Contact.reserve'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>領収者</th>
                                <td>
                                	<?php echo $form->radio('Contact.receipt', $umu_select, array('legend' => false, 'div' => false, 'separator' => '　')); ?>
                            		<?php echo $form->error('Contact.receipt'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>待ち合わせ</th>
                                <td>
                                	<?php echo $form->radio('Contact.layover', $umu_select, array('legend' => false, 'div' => false, 'separator' => '　')); ?>
                            		<?php echo $form->error('Contact.layover'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>コスプレ</th>
                                <td>
                                	<?php echo $form->radio('Contact.cosplay', $umu_select, array('legend' => false, 'div' => false, 'separator' => '　')); ?>
                            		<?php echo $form->error('Contact.cosplay'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>クレジットカード</th>
                                <td>
                                	<?php echo $form->radio('Contact.credit', $umu_select, array('legend' => false, 'div' => false, 'separator' => '　')); ?>
                            		<?php echo $form->error('Contact.credit'); ?>
                                </td>
                        	</tr>
                            
                            <tr>
                                <th>届出確認書or許可書</th>
                                <td>
                                	<?php echo $form->file('Contact.sendfile'); ?>
                            		<?php echo $form->error('Contact.sendfile'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご質問・ご要望など</th>
                                <td>
                                	<?php echo $form->textarea('Contact.comment'); ?>
                            		<?php echo $form->error('Contact.comment'); ?>
                                </td>
                        	</tr>
                        </table>
                        
                        <p class="align_c">
							<?php echo $form->submit('入力内容を確認する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'confirm'));?>
						</p>
                        
                        <?php echo $form->end(); ?>

    </section>

<p><br /></p>



    <h3 class="label">代理店契約</h3>
    
<section class="contact">

            <?php echo $form->create(null,array('type'=>'post','action'=> '','enctype' => 'multipart/form-data')); ?>
                <?php echo $form->hidden('Contact.mode', array('value' => 'agency_agreement')); ?>
                        
                        <table style="padding:3%;">
                            <tr>
                                <th>会社名 <span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.corp_name',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.corp_name'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご担当者名 <span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.charge_name',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.charge_name'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご担当者様のご連絡先 <span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.charge_contact',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.charge_contact'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご連絡先メールアドレス <span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.emailto',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.emailto'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ホームページアドレス <span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.corp_url',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.corp_url'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご質問・ご要望など</th>
                                <td>
                                	<?php echo $form->textarea('Contact.comment'); ?>
                            		<?php echo $form->error('Contact.comment'); ?>
                                </td>
                        	</tr>
                        </table>
                        
                        <p class="align_c">
							<?php echo $form->submit('入力内容を確認する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'confirm'));?>
						</p>
                        
                        <?php echo $form->end(); ?>
                        
                        <p><br /></p>
   
    </section>
    
       
   <p><br /></p>
  


    <h3 class="label">その他</h3>
    
<section class="contact">

            <?php echo $form->create(null,array('type'=>'post','action'=> '','enctype' => 'multipart/form-data')); ?>
                <?php echo $form->hidden('Contact.mode', array('value' => 'other')); ?>
                        
                        <table style="padding:3%;">
                            <tr>
                                <th>メールアドレス<br /><span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.emailto',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.emailto'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>お名前 <span>必須</span></th>
                                <td>
                                	<?php echo $form->text('Contact.charge_name',array('size' => '30')); ?>
                            		<?php echo $form->error('Contact.charge_name'); ?>
                                </td>
                        	</tr>
                            <tr>
                                <th>ご質問・ご要望など</th>
                                <td>
                                	<?php echo $form->textarea('Contact.comment'); ?>
                            		<?php echo $form->error('Contact.comment'); ?>
                                </td>
                        	</tr>
                        </table>
                        
                        <p class="align_c">
							<?php echo $form->submit('入力内容を確認する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'confirm'));?>
						</p>
                        
                <?php echo $form->end(); ?>
   
    </section>


<p><br /></p>

<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./" itemprop="url"><span itemprop="title">お問い合わせ</span></a></li>
	</ul>
</nav>