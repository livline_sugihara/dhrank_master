<div class="link_o"><h2>お問い合わせ</h2></div>

<section class="contact">

   		<?php echo $form->create(null,array('type'=>'post','action'=> '')); ?>
            <?php echo $form->hidden('Contact.mode'); ?>
				<?php if($this->data['Contact']['mode'] == 'payment_insert') { ?>
            
			<table style="padding:3%;">
                <tr>
                    <th>掲載希望エリア</th>
                    <td>
                        <?php echo $this->data['Contact']['insert_area']; ?>
                    </td>
                    
                </tr>
                <tr>
                    <th>業種</th>
                    <td>
                        <?php echo $this->data['Contact']['category_name']; ?>
                    </td>
                    
                </tr>
                <tr>
                    <th>店舗名</th>
                    <td>
                        <?php echo $this->data['Contact']['shop_name']; ?>
                    </td>
                </tr>
                <tr>
                    <th>ご担当者名</th>
                    <td>
                        <?php echo $this->data['Contact']['charge_name']; ?>
                    </td>
                </tr>
                <tr>
                    <th>ご担当者様のご連絡先</th>
                    <td>
                        <?php echo $this->data['Contact']['charge_contact']; ?>
                    </td>
                </tr>
                <tr>
                    <th>ご連絡先メールアドレス</th>
                    <td>
                        <?php echo $this->data['Contact']['emailto']; ?>
                    </td>
                </tr>
                <tr>
                    <th>ご連絡可能な時間帯</th>
                    <td>
                        <?php echo $this->data['Contact']['contact_time']; ?>
                    </td>
                </tr>
                <tr>
                    <th>店舗URL</th>
                    <td>
                        <?php echo $this->data['Contact']['shop_url']; ?>
                    </td>
                </tr>
                <tr>
                    <th>届出確認書or許可書</th>
                    <td>
                        <?php echo (!empty($this->data['Contact']['sendfile']['tmp_name']) ? '添付有り' : '添付なし');  ?>
                    </td>
                </tr>
                <tr>
                    <th>ご質問・ご要望など</th>
                    <td>
                        <?php echo nl2br($this->data['Contact']['comment']); ?>
                    </td>
                </tr>
            </table>

<?php } else if($this->data['Contact']['mode'] == 'free_insert') { ?>

            <table style="padding:3%;">
                <tr>
                    <th>掲載希望エリア</th>
                    <td>
                        <?php echo $this->data['Contact']['insert_area']; ?>
                    </td>
                </tr>
                <tr>
                    <th>業種</th>
                    <td>
                        <?php echo $this->data['Contact']['category_name']; ?>
                    </td>
                    
                </tr>
                <tr>
                    <th>店舗名</th>
                    <td>
                        <?php echo $this->data['Contact']['shop_name']; ?>
                    </td>
                </tr>
                <tr>
                    <th>ご担当者名</th>
                    <td>
                        <?php echo $this->data['Contact']['charge_name']; ?>
                    </td>
                </tr>
                <tr>
                    <th>ご担当者様のご連絡先</th>
                    <td>
                        <?php echo $this->data['Contact']['charge_contact']; ?>
                    </td>
                </tr>
                <tr>
                    <th>ご連絡先メールアドレス</th>
                    <td>
                        <?php echo $this->data['Contact']['emailto']; ?>
                    </td>
                </tr>
                <tr>
                    <th>店舗URL</th>
                    <td>
                        <?php echo $this->data['Contact']['shop_url']; ?>
                    </td>
                </tr>
                <tr>
                    <th>店舗電話番号</th>
                    <td>
                        <?php echo $this->data['Contact']['shop_tel']; ?>
                    </td>
                </tr>
                <tr>
                    <th>住所or（…発）</th>
                    <td>
                        <?php echo $this->data['Contact']['shop_address']; ?>
                    </td>
                </tr>
                <tr>
                    <th>営業時間</th>
                    <td>
                        <?php echo $this->data['Contact']['shop_business_hours']; ?> 
                    </td>
                </tr>
                <tr>
                    <th>最低料金</th>
                    <td>
                        <?php echo $this->data['Contact']['lowest_cost']; ?> 
                    </td>
                </tr>
                <tr>
                    <th>先行予約</th>
                    <td>
                        <?php echo $umu_select[$this->data['Contact']['reserve']]; ?> 
                    </td>
                </tr>
                <tr>
                    <th>領収書</th>
                    <td>
                        <?php echo $umu_select[$this->data['Contact']['receipt']]; ?> 
                    </td>
                </tr>
                <tr>
                    <th>待ち合わせ</th>
                    <td>
                        <?php echo $umu_select[$this->data['Contact']['layover']]; ?> 
                    </td>
                </tr>
                <tr>
                    <th>コスプレ</th>
                    <td>
                        <?php echo $umu_select[$this->data['Contact']['cosplay']]; ?> 
                    </td>
                </tr>
                <tr>
                    <th>クレジットカード</th>
                    <td>
                        <?php echo $umu_select[$this->data['Contact']['credit']]; ?> 
                    </td>
                </tr>
                <tr>
                    <th>届出確認書or許可書</th>
                    <td>
                        <?php echo (!empty($this->data['Contact']['sendfile']['tmp_name']) ? '添付有り' : '添付なし');  ?>
                    </td>
                </tr>
                <tr>
                    <th>ご質問・ご要望など</th>
                    <td>
                        <?php echo nl2br($this->data['Contact']['comment']); ?>
                    </td>
                </tr>
            </table>

<?php } else if($this->data['Contact']['mode'] == 'agency_agreement') { ?>

            <table style="padding:3%;">
                <tr>
                    <th>会社名</th>
                    <td>
                        <?php echo $this->data['Contact']['corp_name']; ?>
                    </td>
                </tr>
                <tr>
                    <th>ご担当者名</th>
                    <td>
                        <?php echo $this->data['Contact']['charge_name']; ?>
                    </td>
                </tr>
                <tr>
                    <th>ご担当者様のご連絡先</th>
                    <td>
                        <?php echo $this->data['Contact']['charge_contact']; ?>
                    </td>
                </tr>
                <tr>
                    <th>ご連絡先メールアドレス</th>
                    <td>
                        <?php echo $this->data['Contact']['emailto']; ?>
                    </td>
                </tr>
                <tr>
                    <th>ホームページアドレス</th>
                    <td>
                        <?php echo $this->data['Contact']['corp_url']; ?>
                    </td>
                </tr>
                <tr>
                    <th>ご質問・ご要望など</th>
                    <td>
                        <?php echo nl2br($this->data['Contact']['comment']); ?>
                    </td>
                </tr>
            </table>

<?php } else if($this->data['Contact']['mode'] == 'other') { ?>

            <table style="padding:3%;">
                <tr>
                    <th>メールアドレス</th>
                    <td>
                        <?php echo $this->data['Contact']['emailto']; ?>
                    </td>
                </tr>
                <tr>
                    <th>お名前</th>
                    <td>
                        <?php echo $this->data['Contact']['charge_name']; ?>
                    </td>
                </tr>
                <tr>
                    <th>ご質問・ご要望など</th>
                    <td>
                        <?php echo nl2br($this->data['Contact']['comment']); ?>
                    </td>
                </tr>
            </table>

<?php } ?>
            <p class="align_c">
                <?php echo $form->submit('送信する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'register'));?>
            </p>
    
    	<?php echo $form->end(); ?>
    
    </section>