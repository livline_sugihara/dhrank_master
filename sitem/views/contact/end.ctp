<div class="body">
    <div class="title title2">
        <h2>INQUIRY SENDED <span class="small">お問い合わせ送信完了</span></h2>
    </div>

    <p class="text" style="margin-bottom: 50px;">お問い合わせの送信が完了しました。</p>

    <div class="align_c">
    	<a class="css_btn_class2" href="<?php echo $linkCommon->get_top($parent_area)?>" class="btn_navi">トップページへ</a>
    </div>
    <div style="height: 200px;">&nbsp;</div>
</div>