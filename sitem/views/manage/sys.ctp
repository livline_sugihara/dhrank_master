<!doctype php>
<?php require("fukuoka/parts/head.php"); ?>
<body>
<!-- header -->
<header>
	<div class="container">
		<h1>ランキングと口コミで探せるデリヘル情報サイト・店舗情報管理</h1>


		<div class="header_form">
        ようこそ <a href="./fukuoka/shop_index.php" target="_blank">一夜妻</a>様　
        	<p class="redBtn barBtn"><a href="entrance.php">ログアウト</a></p>
        </div>
	</div>
</header>
<!-- / header -->
    
    
    <div id="topic_path">
        <a href="shop_top.php">店舗管理・トップページ</a>
        <a href="shop_systems.php">システム管理</a>
    </div>

	<section class="container">

	<h2 class="heading">一夜妻様・店舗システム管理</h2>

    	<!-- menu -->
		<section id="menu" class="two">
        	<?php include("./fukuoka/parts/menu_shop.php"); ?>
        </section>

		<!-- middle -->
        <section id="middle" class="two">
        	<div class="title_bar">システム管理</div> 
        	<div id="timeLineArea" class="inner">
            
            	<div class="new_reviewArea clearfix">

	<form id="ShopEditSystemsForm" method="post" action="/shops/edit_systems" accept-charset="utf-8"><div style="display:none;"><input type="hidden" name="_method" value="POST" /></div>	<input type="hidden" name="data[System][id]" id="SystemId" />	<input type="hidden" name="data[System][user_id]" value="26" id="SystemUserId" />	<table class="profileedit_table">
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />営業時間</td>
            <td class="profileedit_td2">
				<select name="data[System][hour_open_id]" id="SystemHourOpenId">
<option value="">選んで下さい</option>
<option value="1">日の出</option>
<option value="2">0:00</option>
<option value="3">1:00</option>
<option value="4">2:00</option>
<option value="5">3:00</option>
<option value="6">4:00</option>
<option value="7">5:00</option>
<option value="8">6:00</option>
<option value="9">7:00</option>
<option value="10">8:00</option>
<option value="11">9:00</option>
<option value="12">10:00</option>
<option value="13">11:00</option>
<option value="14">12:00</option>
<option value="15">13:00</option>
<option value="16">14:00</option>
<option value="17">15:00</option>
<option value="18">16:00</option>
<option value="19">17:00</option>
<option value="20">18:00</option>
<option value="21">19:00</option>
<option value="22">20:00</option>
<option value="23">21:00</option>
<option value="24">22:00</option>
<option value="25">23:00</option>
<option value="26">24:00</option>
</select>				～
				<select name="data[System][hour_close_id]" id="SystemHourCloseId">
<option value="">選んで下さい</option>
<option value="1">0:00</option>
<option value="2">1:00</option>
<option value="3">2:00</option>
<option value="4">3:00</option>
<option value="5">4:00</option>
<option value="6">5:00</option>
<option value="7">6:00</option>
<option value="8">7:00</option>
<option value="9">8:00</option>
<option value="10">9:00</option>
<option value="11">10:00</option>
<option value="12">11:00</option>
<option value="13">12:00</option>
<option value="14">13:00</option>
<option value="15">14:00</option>
<option value="16">15:00</option>
<option value="17">16:00</option>
<option value="18">17:00</option>
<option value="19">18:00</option>
<option value="20">19:00</option>
<option value="21">20:00</option>
<option value="22">21:00</option>
<option value="23">22:00</option>
<option value="24">23:00</option>
<option value="25">24:00</option>
<option value="26">LAST</option>
</select>											</td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />最安値料金</td>
            <td class="profileedit_td2">
				<input name="data[System][price_minutes]" type="text" size="20" id="SystemPriceMinutes" />分
				<input name="data[System][price_cost]" type="text" size="35" id="SystemPriceCost" />円～
												<span class="small spot8">半角数字のみ</span>
			</td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />定休日</td>
            <td class="profileedit_td2">
				<input type="hidden" name="data[System][closed_365open]" id="SystemClosed365open_" value="0" /><input type="checkbox" name="data[System][closed_365open]" onClick="DeleteHoliday();" value="1" id="SystemClosed365open" /><label>年中無休</label><br />
				<input type="hidden" name="data[System][closed_monday]" id="SystemClosedMonday_" value="0" /><input type="checkbox" name="data[System][closed_monday]" onClick="Delete365Open(this);" value="1" id="SystemClosedMonday" /><label>月</label>
				<input type="hidden" name="data[System][closed_tuesday]" id="SystemClosedTuesday_" value="0" /><input type="checkbox" name="data[System][closed_tuesday]" onClick="Delete365Open(this);" value="1" id="SystemClosedTuesday" /><label>火</label>
				<input type="hidden" name="data[System][closed_wednesday]" id="SystemClosedWednesday_" value="0" /><input type="checkbox" name="data[System][closed_wednesday]" onClick="Delete365Open(this);" value="1" id="SystemClosedWednesday" /><label>水</label>
				<input type="hidden" name="data[System][closed_thursday]" id="SystemClosedThursday_" value="0" /><input type="checkbox" name="data[System][closed_thursday]" onClick="Delete365Open(this);" value="1" id="SystemClosedThursday" /><label>木</label>
				<input type="hidden" name="data[System][closed_friday]" id="SystemClosedFriday_" value="0" /><input type="checkbox" name="data[System][closed_friday]" onClick="Delete365Open(this);" value="1" id="SystemClosedFriday" /><label>金</label>
				<input type="hidden" name="data[System][closed_saturday]" id="SystemClosedSaturday_" value="0" /><input type="checkbox" name="data[System][closed_saturday]" onClick="Delete365Open(this);" value="1" id="SystemClosedSaturday" /><label>土</label>
				<input type="hidden" name="data[System][closed_sunday]" id="SystemClosedSunday_" value="0" /><input type="checkbox" name="data[System][closed_sunday]" onClick="Delete365Open(this);" value="1" id="SystemClosedSunday" /><label>日</label>
				<input type="hidden" name="data[System][closed_public_holiday]" id="SystemClosedPublicHoliday_" value="0" /><input type="checkbox" name="data[System][closed_public_holiday]" onClick="Delete365Open(this);" value="1" id="SystemClosedPublicHoliday" /><label>祝日</label><br />
				<input type="hidden" name="data[System][closed_other]" id="SystemClosedOther_" value="0" /><input type="checkbox" name="data[System][closed_other]" value="1" id="SystemClosedOther" /><label>その他</label><input name="data[System][closed_other_comment]" type="text" size="40" id="SystemClosedOtherComment" />												<script language="JavaScript">
				function DeleteHoliday() {
				    var input = document.getElementById("SystemClosed365open");
				    if(input.checked){
				    	document.getElementById("SystemClosedMonday").checked = false;
				    	document.getElementById("SystemClosedTuesday").checked = false;
				    	document.getElementById("SystemClosedWednesday").checked = false;
				    	document.getElementById("SystemClosedThursday").checked = false;
				    	document.getElementById("SystemClosedFriday").checked = false;
				    	document.getElementById("SystemClosedSaturday").checked = false;
				    	document.getElementById("SystemClosedSunday").checked = false;
				    	document.getElementById("SystemClosedPublicHoliday").checked = false;
				    }
				}
				function Delete365Open(holiday) {
				    if(holiday.checked){
				    	document.getElementById("SystemClosed365open").checked = false;
				    }
				}
				</script>
            	<span class="small spot8">半角200文字/全角100文字以内</span>
			</td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />クレジットカード</td>
            <td class="profileedit_td2">
				<input type="hidden" name="data[System][credit_unavailable]" id="SystemCreditUnavailable_" value="0" /><input type="checkbox" name="data[System][credit_unavailable]" onClick="DeleteCredit();" value="1" id="SystemCreditUnavailable" /><label>利用不可</label><br />
				<input type="hidden" name="data[System][credit_visa]" id="SystemCreditVisa_" value="0" /><input type="checkbox" name="data[System][credit_visa]" onClick="DeleteUnavailable(this);" value="1" id="SystemCreditVisa" /><label>VISA</label>
				<input type="hidden" name="data[System][credit_master]" id="SystemCreditMaster_" value="0" /><input type="checkbox" name="data[System][credit_master]" onClick="DeleteUnavailable(this);" value="1" id="SystemCreditMaster" /><label>MASTER</label>
				<input type="hidden" name="data[System][credit_jcb]" id="SystemCreditJcb_" value="0" /><input type="checkbox" name="data[System][credit_jcb]" onClick="DeleteUnavailable(this);" value="1" id="SystemCreditJcb" /><label>JCB</label>
				<input type="hidden" name="data[System][credit_americanexpress]" id="SystemCreditAmericanexpress_" value="0" /><input type="checkbox" name="data[System][credit_americanexpress]" onClick="DeleteUnavailable(this);" value="1" id="SystemCreditAmericanexpress" /><label>American Express</label>
				<input type="hidden" name="data[System][credit_diners]" id="SystemCreditDiners_" value="0" /><input type="checkbox" name="data[System][credit_diners]" onClick="DeleteUnavailable(this);" value="1" id="SystemCreditDiners" /><label>Diners</label><br />
				<input type="hidden" name="data[System][credit_other]" id="SystemCreditOther_" value="0" /><input type="checkbox" name="data[System][credit_other]" value="1" id="SystemCreditOther" /><label>その他</label><input name="data[System][credit_other_comment]" type="text" size="40" id="SystemCreditOtherComment" />												<script language="JavaScript">
				function DeleteCredit() {
				    var input = document.getElementById("SystemCreditUnavailable");
				    if(input.checked){
				    	document.getElementById("SystemCreditVisa").checked = false;
				    	document.getElementById("SystemCreditMaster").checked = false;
				    	document.getElementById("SystemCreditJcb").checked = false;
				    	document.getElementById("SystemCreditAmericanexpress").checked = false;
				    	document.getElementById("SystemCreditDiners").checked = false;
				    	document.getElementById("SystemCreditOther").checked = false;
				    	document.getElementById("SystemCreditOtherComment").value = '';
				    }
				}
				function DeleteUnavailable(holiday) {
				    if(holiday.checked){
				    	document.getElementById("SystemCreditUnavailable").checked = false;
				    }
				}
				</script>
				<span class="small spot8">半角200文字/全角100文字以内</span>
			</td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />チェンジ</td>
            <td class="profileedit_td2">
				<input type="hidden" name="data[System][change_id]" id="SystemChangeId_" value="" /><input type="radio" name="data[System][change_id]" id="SystemChangeId1" value="1"  /><label for="SystemChangeId1">不可<br /></label><input type="radio" name="data[System][change_id]" id="SystemChangeId2" value="2"  /><label for="SystemChangeId2">無料</label><input type="radio" name="data[System][change_id]" id="SystemChangeId3" value="3"  /><label for="SystemChangeId3">有料</label>				<input name="data[System][change_price]" type="text" size="15" value="0" id="SystemChangePrice" />円
				<input type="hidden" name="data[System][change_transportation_expenses]" id="SystemChangeTransportationExpenses_" value="0" /><input type="checkbox" name="data[System][change_transportation_expenses]" value="1" id="SystemChangeTransportationExpenses" />				<span class="small spot8">別途交通費が発生する場合はチェックして下さい</span>
								            </td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />キャンセル</td>
            <td class="profileedit_td2">
				<input type="hidden" name="data[System][cancel_id]" id="SystemCancelId_" value="" /><input type="radio" name="data[System][cancel_id]" id="SystemCancelId1" value="1"  /><label for="SystemCancelId1">不可<br /></label><input type="radio" name="data[System][cancel_id]" id="SystemCancelId2" value="2"  /><label for="SystemCancelId2">無料</label><input type="radio" name="data[System][cancel_id]" id="SystemCancelId3" value="3"  /><label for="SystemCancelId3">有料</label>				<input name="data[System][cancel_price]" type="text" size="15" value="0" id="SystemCancelPrice" />円
				<input type="hidden" name="data[System][cancel_transportation_expenses]" id="SystemCancelTransportationExpenses_" value="0" /><input type="checkbox" name="data[System][cancel_transportation_expenses]" value="1" id="SystemCancelTransportationExpenses" />				<span class="small spot8">別途交通費が発生する場合はチェックして下さい</span>
								            </td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />領収書</td>
            <td class="profileedit_td2">
				<input type="hidden" name="data[System][receipt_id]" id="SystemReceiptId_" value="" /><input type="radio" name="data[System][receipt_id]" id="SystemReceiptId1" value="1"  /><label for="SystemReceiptId1">発行可</label><input type="radio" name="data[System][receipt_id]" id="SystemReceiptId2" value="2"  /><label for="SystemReceiptId2">発行不可</label>				            </td>
        </tr>
		<tr>
			<td class="profileedit_td1"><img src="/images/icon_table.jpg" />出張可能場所</td>
			<td class="profileedit_td2">
				<input type="hidden" name="data[System][place_love_hotel]" id="SystemPlaceLoveHotel_" value="0" /><input type="checkbox" name="data[System][place_love_hotel]" value="1" id="SystemPlaceLoveHotel" /><label>ラブホテル</label>
				<input type="hidden" name="data[System][place_business_hotel]" id="SystemPlaceBusinessHotel_" value="0" /><input type="checkbox" name="data[System][place_business_hotel]" value="1" id="SystemPlaceBusinessHotel" /><label>ビジネスホテル</label>
				<input type="hidden" name="data[System][place_home]" id="SystemPlaceHome_" value="0" /><input type="checkbox" name="data[System][place_home]" value="1" id="SystemPlaceHome" /><label>自宅</label>
				<input type="hidden" name="data[System][place_meeting]" id="SystemPlaceMeeting_" value="0" /><input type="checkbox" name="data[System][place_meeting]" value="1" id="SystemPlaceMeeting" /><label>待ち合わせ</label>
			</td>
		</tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />コメント1タイトル</td>
            <td class="profileedit_td2">
				<input name="data[System][comment1_title]" type="text" size="40" id="SystemComment1Title" />				            </td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />コメント1</td>
            <td class="profileedit_td2">
				<textarea name="data[System][comment1_content]" cols="52" rows="8" id="SystemComment1Content" ></textarea>				            </td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />コメント2タイトル</td>
            <td class="profileedit_td2">
				<input name="data[System][comment2_title]" type="text" size="40" id="SystemComment2Title" />				            </td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />コメント2</td>
            <td class="profileedit_td2">
				<textarea name="data[System][comment2_content]" cols="52" rows="8" id="SystemComment2Content" ></textarea>				            </td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />コメント3タイトル</td>
            <td class="profileedit_td2">
				<input name="data[System][comment3_title]" type="text" size="40" id="SystemComment3Title" />				            </td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />コメント3</td>
            <td class="profileedit_td2">
				<textarea name="data[System][comment3_content]" cols="52" rows="8" id="SystemComment3Content" ></textarea>				            </td>
        </tr>
    </table>

<div align="center" style="margin-top:40px;">
<div class="submit"><input class="btn_navi" type="submit" value="保存する" /></div></div>
</form>
<div style="margin-bottom:20px"><br /></div>

            	</div>
                    
        	</div>
    	</section>
        <!-- / middle -->
        
	</section>

    
    <?php require("fukuoka/parts/footer2.php"); ?>
</body>
</php>