	<div class="label">
		<h2><?php // echo count($follow_reviewer); ?><?php echo $reviewer_data['Reviewer']['handle']; ?>さんのフォローしている</h2>
	</div>
	<section id="topic_shop_area">
<?php
	foreach($follow_reviewer AS $ii => $record) {
?>
		<div class="list">
			<div class="top_new_review_box clearfix">
				<p class="img"><a href="<?php echo $linkCommon->get_reviewer_profile($record); ?>"><?php echo $imgCommon->get_reviewer_avatar($record, array(80,80),'img_frame',null,$record['Reviewer']['handle']); ?></a></p>
				<div class="summary">
				<p class="name"><?php echo $record['Reviewer']['handle']; ?>さん</p>
					<div style="float:right;">
						<p class="css_btn_class followReviewer" data="<?php echo $record['Reviewer']['id']; ?>" mode="add">
							<a href="#" class="css_btn_class"><img src="../../../img/s/follow_btn.png" alt="フォローする" height="20"></a>
						</p>
					</div>
				<p class="option_follow">フォロワー<?php echo $record[0]['follower_cnt']; ?></p>
				<p class="option_review">レビュー投稿<?php echo $record[0]['review_cnt']; ?>件</p>
				<p>
					<span class="caption">よく利用するカテゴリ</span>
					<span class="cat"><?php echo $record['MShopsBusinessCategory']['name']; ?></span>
				</p> 
					</div>
				</div>
			</div>
<?php
	}
?>
	</section>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_mypage_profile($record = null); ?>">My Top</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_profile($reviewer_data); ?>">プロフィール</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_reviews($reviewer_data); ?>">レビュー</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_follow($reviewer_data); ?>">フォローしている</a></p>
	<p class="btn_left"><a href="<?php echo $linkCommon->get_reviewer_follower($reviewer_data); ?>">フォローされている</a></p>
