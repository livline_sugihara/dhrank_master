<!-- middle -->
<section id="middle" class="two">    <div class="title_bar"><?php echo $reviewer_data['Reviewer']['handle']; ?>さん レビュー一覧</div>

    <div id="reviewListArea" class="inner">
        <div class="new_reviewArea clearfix">
<?php
$cnt = count($list);
for ($ii = 0; $ii < $cnt; $ii++) {
	echo $this->element('common/_review', array("record" => $list[$ii]));
}
?>
        </div>
    </div>
</section>