<div id="modal_post" class="modal_block post_block">	<div class="boards_area">
		<form action="/s/shop_boards/add/<?php echo $user_id; ?>" method="POST" name="postform">
		<dl>
			<dt>お名前</dt>
			<dd>
				<div class="input_item"><input type="text" class="name" name="postname" placeholder="お名前"></div>
			</dd>
			<dt>投稿内容</dt>
			<dd>
				<div class="input_item"><textarea class="comment" name="postcomment" placeholder="投稿内容を入力してください。"><?php if($res_post_no != '') { echo '>>'.$res_post_no;} ?></textarea></div>
			</dd>
		</dl>
		<div class="caption">※注意</div>
		<ol class="caution_list">
			<li><span class="txt_red">すべてのIPアドレスは保存しております。</span>（削除されてもIPアドレスは残ります。）<br>尚、IPアドレスの個人的公開は絶対いたしません。</li>
			<li>電話番号などの個人情報を書き込むのは犯罪です。</li>
			<li>個人の権利を侵害した書き込みに対しても同様です。</li>
			<li><span class="txt_red">すべての書き込みの責任は書き込み者に帰属されます。</span></li>
		</ol>
	</div>
	<div class="btn btn_post"><a href="">同意して投稿</a></div>
	</form>
</div>
<script>
	$('.btn_post').click(function(){
			var name = $('[name="postname"]').val();
			var comment = $('[name="postcomment"]').val();
			var errmsg = '';

			// エラーチェック
			if (name == '')
				errmsg = errmsg + 'お名前を入力してください' + "\n";
			else if(name.length > 256 )
				errmsg = errmsg + 'お名前は255文字以内で入力してください' + "\n";

			if (comment == '')
				errmsg = errmsg + '投稿内容を入力してください' + "\n";

			// エラーメッセージ
			if (errmsg != '')
			{
				alert(errmsg);
				return false;
			}

			$('[name="postform"]').submit();
			return false;
	});
</script>
