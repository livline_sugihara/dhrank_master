<div id="modal_tree" class="modal_block tree_block">
	<div class="boards_area">
		<div class="res_block">
			<div class="res_num"><a href="#" data-href="modal_post" class="modal"><i class="fa fa-comment"></i><span class="num"><?php echo $boards_data[0]['post_no']; ?></span></a></div>
			<?php if ($boards_data[0]['res_post_no'] != null) { ?>
			<div class="res_num"><a href="#" data-href="modal_tree" class="modal">&gt;&gt;<?php echo $boards_data[0]['res_post_no']; ?></a></div>
			<?php } ?>
			<div class="res_body"><?php echo nl2br(htmlspecialchars($boards_data['ShopBoards']['comment'])); ?></div>
			<div class="res_info"><span class="post_name"><i class="fa fa-user"></i><?php echo htmlspecialchars($boards_data['ShopBoards']['name']); ?></span><span class="post_date"><?php ?></span></div>
		</div><!-- /.res_block -->
	</div>
	<div class="boards_area nostyle">
		<div class="btn btn_thread_top"><a href="/s/shop_boards/index/<?php echo $user_id;?>"><i class="fa fa-list-ol fa-2x"></i>この掲示板のトップへ</a></div>
	</div>
</div>
