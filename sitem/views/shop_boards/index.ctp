<h1><?php echo $shop_name; ?>の炎上掲示板</h1>
<h1>投稿内容はありません</h1>
<!--
<?php
	echo $this->Paginator->numbers(array(
		'before' => $paginator->hasPrev() ? $paginator->first('<<').' | ' : '', // 最初のページへ
        'after' => $paginator->hasNext() ? ' | '.$paginator->last('>>') : '', // 最後のページへ
		'separator' => '　|　'
	));
?>
</table>
-->
<div class="boards_area">
	<!--
	<div class="thread_top">
		<div class="res_block">
			<div class="thread_header">
				<div class="res_num"><i class="fa fa-comment"></i>000</div>
				<div class="thread_title">スレッドタイトル</div>
			</div>
			<div class="res_body">本文本文本文本文本文本文本文本文本文</div>
			<div class="res_info"><span class="post_name"><i class="fa fa-user"></i>スレ主</span><span class="post_date">2015/01/01/ 00:00</span></div>
		</div>
	</div>
	-->
	<!-- /.res_block -->
	<!-- /.thread_top -->

	<div class="thread_nav">
		<div class="link_top"><a href="#"><i class="fa fa-list-alt fa-lg"></i>最初から表示</a></div>
		<ul>
			<li class="link_prev"><a href="#"><i class="fa fa-arrow-left fa-lg"></i>前へ</a></li><li class="link_next"><a href="#">次へ<i class="fa fa-arrow-right fa-lg"></i></a></li>
		</ul>
	</div><!-- /.thread_nav -->
	<div class="thread_main">
		<div class="display_page">
			<div class="res_nums"><i class="fa fa-comment"></i>000〜100(全<?php echo $boards_count; ?>)</div>
			<div class="thread_pages"><a href="#" class="page_list"><i class="fa fa-file-text-o"></i>10/10<i class="fa fa-sort-desc fa-lg"></i></a></div>
			<ul class="page_jump">
				<li><a href="#"><i class="fa fa-file-text-o"></i>10/10</a></li>
				<li><a href="#"><i class="fa fa-file-text-o"></i>09/10</a></li>
				<li><a href="#"><i class="fa fa-file-text-o"></i>08/10</a></li>
				<li><a href="#"><i class="fa fa-file-text-o"></i>07/10</a></li>
				<li><a href="#"><i class="fa fa-file-text-o"></i>06/10</a></li>
				<li><a href="#"><i class="fa fa-file-text-o"></i>05/10</a></li>
				<li><a href="#"><i class="fa fa-file-text-o"></i>04/10</a></li>
				<li><a href="#"><i class="fa fa-file-text-o"></i>03/10</a></li>
				<li><a href="#"><i class="fa fa-file-text-o"></i>02/10</a></li>
				<li><a href="#"><i class="fa fa-file-text-o"></i>01/10</a></li>
			</ul>
		</div><!-- /.display_page -->
		<?php foreach($boards_data as $key => $val) { ?>
		<div class="res_block">
			<div class="res_num"><a href="#" data-href="modal_post" class="modal" ><i class="fa fa-comment"></i><span class="num"><?php echo $val[0]['post_no']; ?></span></a></div>
			<?php if ($val[0]['res_post_no'] != null) { ?>
			<div class="res_num"><a href="#" data-href="modal_tree" class="modal" data="<?php echo $val[0]['res_post_no']; ?>">&gt;&gt;<?php echo $val[0]['res_post_no']; ?></a></div>
			<?php } ?>
			<div class="res_body"><?php echo nl2br(htmlspecialchars($val['ShopBoards']['comment'])); ?></div>
			<div class="res_info">
				<span class="post_name"><i class="fa fa-user"></i>
					<?php echo htmlspecialchars($val['ShopBoards']['name']); ?>
				さん</span>
				<span class="post_date">
					<?php echo date('Y/m/d/ H:i', strtotime($val['ShopBoards']['created'])); ?>
				</span>
				</div>
		</div><!-- /.res_block -->
		<?php } ?>
<!--
		<div class="res_block">
			<div class="res_num"><a href="#" data-href="modal_post" class="modal"><i class="fa fa-comment"></i><span class="num">002</span></a></div>
			<div class="res_body">本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文</div>
			<div class="res_info"><span class="post_name"><i class="fa fa-user"></i>スレ主</span><span class="post_date">2015/01/01/ 00:00</span></div>
		</div>
 /.res_block
		<div class="res_block">
			<div class="res_num"><a href="#" data-href="modal_post" class="modal"><i class="fa fa-comment"></i><span class="num">003</span></a></div>
			<div class="res_num"><a href="#" data-href="modal_tree" class="modal">&gt;&gt;135</a></div>
			<div class="res_body">本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文</div>
			<div class="res_info"><span class="post_name"><i class="fa fa-user"></i>スレ主</span><span class="post_date">2015/01/01/ 00:00</span></div>
		</div>
-->
		<!-- /.res_block -->
	</div><!-- /.thread_main -->
	<div class="thread_nav">
		<div class="link_top"><a href="#"><i class="fa fa-list-alt fa-lg"></i>最初から表示</a></div>
		<ul>
			<li class="link_prev"><a href="#"><i class="fa fa-arrow-left fa-lg"></i>前へ</a></li><li class="link_next"><a href="">次へ<i class="fa fa-arrow-right fa-lg"></i></a></li>
		</ul>
	</div><!-- /.thread_nav -->
	<div class="btn btn_comment"><a href="#" data-href="modal_post" class="modal">コメントする</a></div>

	<!-- 投稿用モーダル -->
	<div id="modal_post" class="modal_block post_block">
		<div class="boards_area">
			<dl>
				<dt>お名前</dt>
				<dd>
					<div class="input_item"><input type="text" class="name" name="formname" placeholder="お名前"></div>
				</dd>
				<dt>投稿内容</dt>
				<dd>
					<div class="input_item"><textarea class="comment" name="formcomment" placeholder="投稿内容を入力してください。"></textarea></div>
				</dd>
			</dl>
			<div class="caption">※注意</div>
			<ol class="caution_list">
				<li><span class="txt_red">すべてのIPアドレスは保存しております。</span>（削除されてもIPアドレスは残ります。）<br>尚、IPアドレスの個人的公開は絶対いたしません。</li>
				<li>電話番号などの個人情報を書き込むのは犯罪です。</li>
				<li>個人の権利を侵害した書き込みに対しても同様です。</li>
				<li><span class="txt_red">すべての書き込みの責任は書き込み者に帰属されます。</span></li>
			</ol>
			<div class="btn btn_post"><a href="/">同意して投稿</a></div>
		</div>
	</div>
	<form action="/shop_boards/add/<?php echo $user_id;?>/" method="POST" name="postform"></form>
	<!-- モーダル -->
<!--
	<div id="modal_tree" class="modal_block tree_block">
		<div class="boards_area">
			<div class="res_block">
				<div class="res_num"><a href="#" data-href="modal_post" class="modal"><i class="fa fa-comment"></i><span class="num">003</span></a></div>
				<div class="res_num"><a href="#" data-href="modal_tree" class="modal">&gt;&gt;135</a></div>
				<div class="res_body">本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文</div>
				<div class="res_info"><span class="post_name"><i class="fa fa-user"></i>スレ主</span><span class="post_date">2015/01/01/ 00:00</span></div>
			</div>
		</div>
	</div>
-->
</div><!-- /#boards_block -->
