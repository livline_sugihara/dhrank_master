<h1><?php echo $shop_name; ?>の炎上掲示板</h1>
<form action="/s/shop_boards/add/<?php echo $user_id; ?>" method="POST">
<div class="boards_area">
<!-- /.thread_top -->
	<div class="thread_nav">
		<div class="link_top"><a href="/s/shop_boards/index/<?php echo $user_id; ?>"><i class="fa fa-list-alt fa-lg"></i>最初から表示</a></div>
		<ul>
			<?php if ($paginator->hasPrev()) { ?>
				<li class="link_prev" style="float:left;"><i class="fa fa-arrow-left fa-lg"></i>
				<?php echo $this->Paginator->prev(
					'前へ',array(),null,array('class' => 'prev disabled', 'tag' => false)
				); ?>
				</li>
				<?php if (!$paginator->hasNext()) { ?>
				<?php } ?>
<!--				<li class="link_prev"><a href="#"><i class="fa fa-arrow-left fa-lg"></i>前へ</a></li>-->
			<?php } ?>
			<?php if ($paginator->hasNext()) { ?>
				<li class="link_next">
				<?php echo $this->Paginator->next(
					'次へ',array(),null,array('class' => 'next disabled', 'tag' => false)
				); ?>
				<i class="fa fa-arrow-right fa-lg"></i></li>
<!--				<li class="link_next"><a href="#">次へ<i class="fa fa-arrow-right fa-lg"></i></a></li> -->
			<?php } ?>
		</ul>
	</div><!-- /.thread_nav -->
	<div class="thread_main">
		<div class="display_page">
			<div class="res_nums"><i class="fa fa-comment"></i>
				<?php
					$count = $this->Paginator->counter(array('format' => '%count%'));
					$start = $this->Paginator->counter(array('format' => '%start%')) - 1;
					$s_count = $count - $start;
					if ($count == 0)
						echo str_pad($count, 3, '0', STR_PAD_LEFT);
					else
						echo str_pad($s_count, 3, '0', STR_PAD_LEFT);
				?>
				〜
				<?php
					$end = $this->Paginator->counter(array('format' => '%end%')) - 1;
					$e_count = $count - $end;
					if ($count == 0)
						echo str_pad($count, 3, '0', STR_PAD_LEFT);
					else
						echo str_pad($e_count, 3, '0', STR_PAD_LEFT);
				?>
				(全<?php echo str_pad($count, 3, '0', STR_PAD_LEFT);?>件)</div>
			<?php
				// リスト作成
				$pages = (int)$this->Paginator->counter(array('format' => '%pages%'));
				$str_pad_pages = str_pad($pages, 3, '0', STR_PAD_LEFT);
			?>
			<div class="thread_pages"><a href="#" class="page_list">
				<i class="fa fa-file-text-o"></i>
				<?php echo $str_pad_pages . '/' . $str_pad_pages; ?>
				<i class="fa fa-sort-desc fa-lg"></i></a></div>
			<ul class="page_jump">
				<?php
					$count = 1;
					for($i = $pages; $i > 0; $i--){
				?>
				<li>
					<a href="/s/shop_boards/index/<?php echo $user_id; ?>/<?php echo 'page:' . $count; ?>">
						<i class="fa fa-file-text-o"></i>
						<?php
							echo str_pad($i, 3, '0', STR_PAD_LEFT) . '/' .$str_pad_pages;
						?>
					</a>
				</li>
				<?php
						$count++; // URLパラメータのpage:用
					}
				?>
			</ul>
		</div><!-- /.display_page -->
		<?php
			if (count($boards_data) > 0)
			{
				foreach($boards_data as $key => $val) {
		?>
			<div class="res_block">
				<div class="res_num"><a href="/s/shop_boards/form/<?php echo $user_id; ?>/<?php echo $val[0]['post_no']; ?>" data-href="modal_post" class="modal"><i class="fa fa-comment"></i><span class="num"><?php echo $val[0]['post_no']; ?></span></a></div>
				<?php if ($val[0]['res_post_no'] != null){ ?>
				<div class="res_num"><a href="/s/shop_boards/res/<?php echo $user_id; ?>/<?php echo $val[0]['res_post_no']; ?>/" data-href="modal_tree" class="modal">&gt;&gt;<?php echo $val[0]['res_post_no']; ?></a></div>
				<?php } ?>
				<div class="res_body"><?php echo nl2br(htmlspecialchars($val['ShopBoards']['comment']));?></div>
				<div class="res_info"><span class="post_name"><i class="fa fa-user"></i><?php echo htmlspecialchars($val['ShopBoards']['name']);?>さん</span><span class="post_date">2015/01/01/ 00:00</span></div>
			</div><!-- /.res_block -->
		<?php
				}
			}
			else
			{
				echo "投稿データは存在しません。";
			}
		?>
	</div><!-- /.thread_main -->
	<div class="thread_nav">
		<div class="link_top"><a href="/s/shop_boards/index/<?php echo $user_id; ?>"><i class="fa fa-list-alt fa-lg"></i>最初から表示</a></div>
		<ul>
			<?php if ($paginator->hasPrev()) { ?>
				<li class="link_prev" style="float:left;"><i class="fa fa-arrow-left fa-lg"></i>
				<?php echo $this->Paginator->prev(
					'前へ',array(),null,array('class' => 'prev disabled', 'tag' => false)
				); ?>
				</li>
				<?php if (!$paginator->hasNext()) { ?>
				<?php } ?>
<!--				<li class="link_prev"><a href="#"><i class="fa fa-arrow-left fa-lg"></i>前へ</a></li>-->
			<?php } ?>
			<?php if ($paginator->hasNext()) { ?>
				<li class="link_next">
				<?php echo $this->Paginator->next(
					'次へ',array(),null,array('class' => 'next disabled', 'tag' => false)
				); ?>
				<i class="fa fa-arrow-right fa-lg"></i></li>
<!--				<li class="link_next"><a href="#">次へ<i class="fa fa-arrow-right fa-lg"></i></a></li> -->
			<?php } ?>
		</ul>
	</div><!-- /.thread_nav -->
	<div class="btn btn_comment"><a href="/s/shop_boards/form/<?php echo $user_id; ?>" data-href="modal_post" class="modal">コメントする</a></div>
</div><!-- /#boards_block -->
