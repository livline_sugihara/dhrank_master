<div class="link_o"><h2>オススメショップ</h2></div>

			<section id="coupon">

<?php
	foreach($recommend_shop AS $key => $record) {
?>
			<div class="coupon_link links">
				<div class="coupon_inner">
					<p class="img"><a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo $imgCommon->get_shop_with_time($record, 'recommend', array(440)); ?></a></p>
					<ul class="shop_icon clearfix">
						<?php if($record['Shop']['service01'] == 1) { ?><li class="pay_yoyaku">先行予約OK</li><?php } ?>
						<?php if($record['Shop']['service02'] == 1) { ?><li class="pay_credit">クレジットOK</li><?php } ?>
						<?php if($record['Shop']['service03'] == 1) { ?><li class="pay_ryoshu">領収書OK</li><?php } ?>
						<?php if($record['Shop']['service04'] == 1) { ?><li class="pay_machi">待ち合わせOK</li><?php } ?>
						<?php if($record['Shop']['service05'] == 1) { ?><li class="coupon">コスプレあり</li><?php } ?>
						<?php if($record['Shop']['service06'] == 1) { ?><li class="mensok">クーポンあり</li><?php } ?>
					</ul>
					<p class="shop_title"><a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo $record['Shop']['name']; ?></a></p>
<?php
	$opentime = date('H:i', strtotime($record['System']['open_time']));
	$closetime = date('H:i', strtotime($record['System']['close_time']));
?>
					<p>営業時間・<?php echo $opentime; ?>〜<?php echo $closetime; ?>　最低料金・<?php echo $record['System']['price_cost']; ?>円〜</p>
					<div class="detail_box clearfix">
						<p class="copy"><?php echo $record['Shop']['about_title']; ?></p>
						<p><?php echo $record['Shop']['about_contents']; ?></p>
					</div>
				</div>
			</div>
<?php
		}
?>
		</div>
<!-- /recommendShopArea -->
	</section>
<?php echo $this->element('s_common/footer_banner'); ?>

<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left">
	<ul class="bc-inr">
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="../top/" itemprop="url"><span itemprop="title"><?php echo $parent_area['LargeArea']['name']?></span></a></li>
		<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="./shop/" itemprop="url"><span itemprop="title">イチオシのお店</span></a></li>
	</ul>
</nav>