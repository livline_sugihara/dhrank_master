<section id="middle" class="full">
    
    <div id="entryArea">
<?php /*
        <div class="head">
        </div>
*/ ?>
        <!--  body -->
        <div class="body">
            <p>女の子のルックスや正確、サービスなどの良かった所をみんなでシェアするサイトです。<br />女の子が傷つくような誹謗中傷などはお控え下さい。<br />該当する書き込みがあった場合、編集部にて削除させていただくことが有ります。</p>
            <div class="listFilter" style="margin-top: 10px;">
                <?php echo $form->create(null, array('type'=>'post','action'=>'')); ?>
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <th>カテゴリー</th>
                        <td>
                            <?php echo $form->select('Search.category_id', $m_shops_business_categories,null,array('empty'=>' ')); ?>
                            <?php echo $form->error('MShopBusinessCategory.id'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ショップ名</th>
                        <td>
                            <?php echo $form->text('Search.shop_name',array('style' => 'width:200px;')); ?>
                            <?php echo $form->error('Search.shop_name'); ?>
                        </td>
                    </tr>
                </table>
                <p class="align_c">
                    <input type="submit" class="submitBtn redBtn" value="検索する">
                </p>
                <?php echo $form->end(); ?>
            </div>
            
            <div class="listHeading">クチコミ投稿先一覧</div>
            
            
            <div class="align_c idk">
            お店が無い場合はこちらから…  <a href="<?php echo $linkCommon->get_post_review(); ?>" class="css_btn_class2">クチコミを書く</a>
            </div>   
            
            
            <div class="shopList">
                <table cellspacing="0" cellpadding="0" border="0">
<?php
if(!empty($shop_list)) {
    foreach($shop_list AS $ii => $record) {
        $opentime = date('H:i', strtotime($record['System']['open_time']));
        $closetime = date('H:i', strtotime($record['System']['close_time']));
?>
                    <tr>
                        <th><?php echo $record['MShopsBusinessCategory']['name']; ?></th>
                        <td>
                            <p class="tt">
                                <a href="<?php echo $linkCommon->get_user_shop($record); ?>"><?php echo $record['Shop']['name']; ?></a>
                            </p>
                            <p>営業時間・<?php echo $opentime; ?>〜<?php echo $closetime; ?>　最低料金・<?php echo $record['System']['price_cost']; ?>円〜　住所・<?php echo $record['System']['address']; ?></p>
                        </td>
                        <td>
                            <p class="whiteBtn btn frameOpen" data='<?php echo $record['Shop']['id']; ?>'><a href="#">クチコミを書く</a></p>
                        </td>
                    </tr>
<?php
    }
} else {
?>
                <div class="align_c" style="margin-top: 50px;">
                    検索条件にあう店舗がありませんでした。
                </div>      
<?php } ?>
                </table>

<?php if(!empty($shop_list)) { ?>
                <div class="align_c idk">
                お店が無い場合はこちらから…  <a href="#" class="css_btn_class2">クチコミを書く</a>
                </div>      
<?php } ?>
            </div>
                                    
                                    
                                                    
        </div>
        <!--  /body -->
        
    </div>
    
</section>
