<div data-role="content" data-theme="a">

    <div class="reviewer_top">

	<div style="font-size:16px;">
		<?php if(!empty($reviewer['Reviewer']['title'])){?> <?php echo $reviewer['Reviewer']['title']?> <?php }else{?> <?php echo $reviewer['Reviewer']['handle']?>のデリヘルガイド<?php }?>
	</div>

	<div style="text-align:right;">
		女の子の口コミ：<span class="spot1"><?php echo $rcommon_review_count[0]['review_count']?></span>件 &nbsp;
		口コミの投票数：<span class="spot1">
		<?php //start---2013/4/30 障害No.4-0002修正 ?>
		<?php //echo $rcommon_review_count[0]['reviews_good_count_sum']?>
		<?php echo $rcommon_reviews_good_count[0]['reviews_good_count_sum']?>
		<?php //end---2013/4/30 障害No.4-0002修正 ?>
		</span>票
	</div>

<hr style="border:1px solid #9C9E9F;" />


<p>
<?php echo $reviewer['Reviewer']['handle']?>
		<?php if(!empty($reviewer['Reviewer']['age']) || !empty($reviewer['Reviewer']['sex_id']) || !empty($reviewer['MAddress']['name'])){?>
		(
		<?php if(!empty($reviewer['Reviewer']['age'])){?>
		<?php echo $reviewer['Reviewer']['age'] . '歳'?>
		<?php if(!empty($reviewer['Reviewer']['sex_id']) || !empty($reviewer['MAddress']['name'])) echo '・'?>
		<?php }?>
		<?php if(!empty($reviewer['Reviewer']['sex_id'])){?>
		<?php echo $sex[$reviewer['Reviewer']['sex_id']]?>
		<?php if(!empty($reviewer['MAddress']['name'])) echo '・'?>
		<?php }?>
		<?php if(!empty($reviewer['MAddress']['name'])){?>
		<?php echo $reviewer['MAddress']['name']?>
		<?php }?>
		)
		<?php }?>
		<br />
		<?php if(!empty($reviewer['Reviewer']['comment'])){?>
		<?php //start---2013/3/11 障害No.1-4-0003修正 ?>
		<?php //echo $reviewer['Reviewer']['comment']?>
		<?php echo nl2br($reviewer['Reviewer']['comment'])?>
		<?php //end---2013/3/11 障害No.1-4-0003修正 ?>
		<?php }?>
</p>

</div>

<ul data-role="listview" data-divider-theme="a">
<?php foreach($reviews as $key => $record){?>
	<li style="height:82px">
    	<a href="<?php echo $linkCommon->get_user_girl($record)?>">
			<?php echo $imgCommon->get_girl($record, 's')?>
            <p><span class="spot4"><?php echo $record['Shop']['name'] ?></span> - <?php echo $record['Girl']['name'] ?> <?php echo $record['Girl']['age'] ?>歳 </p>
			<p><span class="small">評価</span><?php for($i = 0; $i < 5; $i++){?><img src="/images/s/star_<?php echo ($i+1 <= round($record[0]['girl_avg']))?'on':'off';?>.png" class="kuchikomi_heart" width="16px" style="vertical-align:middle;" /><?php }?> <span class="spot4"><?php echo round($record[0]['girl_avg'],2)?></span> <span class="small"><?php echo date('Y年n月', strtotime($record['Review']['created'])) ?></span></p>
			<p>
            <?php //start---2013/3/6 障害No.3-0004修正 ?>
			<?php if($record['Review']['comment_deleted'] != 1){?>
			<?php echo $textCommon->get_substr_tagstrip_text($record['Review']['comment'],APP_CSIZE_REVIEWS_LIST_POST_COMMENT_S);?>
			<?php }else{?>
			削除依頼により削除されました。
			<?php }?>
			<?php //end---2013/3/6 障害No.3-0004修正 ?>
			</p>
		</a>
    </li>
<?php }?>
</ul>


</div>



<?php echo $this->element('pagenate/numbers'); ?>

<div style="text-align:center; margin-right:auto; margin-left:auto;">
	<a href="/s/top" data-role="button" data-inline="true">戻る</a>
</div>