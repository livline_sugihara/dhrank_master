<div class="link_o"><h2>口コミ投稿の完了</h2></div>

<div class="label_3">口コミの投稿が完了しました。</div>
    <p>反映されるまで、1日〜2日程度お時間がかかる場合がありますので、ご了承ください。</p>
</div>

<section class="contact">

        <div align="center" style="margin: 20px;">
			<a href="<?php echo $linkCommon->get_top(); ?>" class="css_btn_class2">トップへ戻る</a>
		</div>

</section>