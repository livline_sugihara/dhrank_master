<div class="link_o"><h2>口コミ通報フォーム</h2></div>
<div class="label_3">口コミ通報について</div>

<p>この口コミを不審に感じた理由をお書き下さい。<br />
    対応につきましては「利用規約」に違反しているか、慎重に調査、検討を進めた上判断させて頂きます。<br />
    この機能の悪用も「利用規約」の違反となりますのでご注意下さい。<br />
    また通報へのご返答は出来かねますのでご了承ください。
 </p>

    <h3 class="label">（確認）口コミ通報フォーム</h3>
    
    <section class="contact">
    
            <div class="reviewForm">
                <?php echo $form->create(null,array('type'=>'post','action'=> '')); ?>
                    <?php echo $form->hidden('ReviewsReport.report_review_id'); ?>
                    
                    <table style="padding:3%;">
                        <tr>
                            <th>レビュアー名</th>
                            <td><?php echo $review_data['Review']['post_name']; ?></td>
                        </tr>
                        <tr>
                            <th>店名</th>
                            <td><?php echo $review_data['Review']['shop_name']; ?></td>
                            
                        </tr>
                        <tr>
                            <th>女の子</th>
                            <td><?php echo $review_data['Review']['girl_name']; ?></td>
                        </tr>
                        <tr>
                            <th>レビュー内容</th>
                            <td>
                                <?php echo nl2br($review_data['Review']['comment']); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>通報の理由</th>
                            <td>
                            <p>
                                <?php echo $m_reviews_reports_types[$this->data['ReviewsReport']['report_type_id']]; ?>
                            </p><br />
                                <?php echo nl2br($this->data['ReviewsReport']['comment']); ?>
                            </td>
                        </tr>
                    </table>

                    <p class="align_c">
                        <?php echo $form->submit('入力内容を送信する',array('class' => 'submitBtn redBtn', 'div' => false, 'name' => 'register'));?>
                    </p>
                    
                <?php echo $form->end(); ?>

	</section>