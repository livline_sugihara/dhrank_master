<!DOCTYPE html>
<html lang="ja">
<head>
<?php echo $this->Html->charset(); ?>

<title>ランキングと口コミで探せるデリヘル情報サイト[デリヘル人気口コミランキング]</title>
<meta name="description" content="ランキングと口コミで探せるデリヘル情報サイト[デリヘル人気口コミランキング]" />
<meta name="keywords" content="ランキング,口コミ,デリヘル" />

	<link href="/css/s/style.css" rel="stylesheet" type="text/css" media="all">
	<link href="/css/s/base.css" rel="stylesheet" type="text/css" media="all">
	<link href="/css/s/top.css" rel="stylesheet" type="text/css" media="all">
	<link href="/css/s/index.css" rel="stylesheet" type="text/css" media="all">
	<link href="/css/s/jquery.bxslider.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="/js/s/jquery.js"></script>
	<script type="text/javascript" src="/js/s/jquery.bxslider.js"></script>
	<script type="text/javascript" src="/js/s/jquery.biggerlink.min.js"></script>
    <script type="text/javascript" src="/js/s/index.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<script type="application/javascript">
	// ajax機能無効化
	$(document).bind("mobileinit", function(){
		$.mobile.ajaxEnabled = false;
		$.mobile.hashListeningEnabled = false;
	});
	</script>

</head>

<body>

	<header>
		<div class="clearfix">
			<div id="head_left">
				<h1><a href="./"><img src="/images/s/deri_logo.png" height="42"></a></h1>
				<p>国内最大級の<br>デリヘル口コミサイト</p>
			</div>
			<div id="head_right">
				<ul class="clearfix">
					<li><a href="http://shinjyuku.dhrank.com/s/accounts/register/"><img src="/images/s/header_btn01.gif" alt="会員登録" height="52" width="66"></a></li>
					<li><a href="http://shinjyuku.dhrank.com/s/accounts/login/"><img src="/images/s/header_btn02.gif" alt="ログイン" height="52" width="66"></a></li>
				</ul>
			</div>
		</div>
	</header>

<div style="background-color:#988888;text-align:center;color:#ffffff;padding:10px;font-size:125%;font-weight:bold;">
	全国のデリヘル店の口コミを検索
</div>
<img src="../images/s/messe.png" width="100%" />
<div class="banner">
	<?php //echo $imgCommon->get_top_link_banner(1, 'shop', array(440,70)); ?>
	<img src="<?php echo APP_IMG_URL. 'shop/' .$banner['Shop']['user_id']. '/'. $banner['Shop']['image_recommend']; ?>" alt="カテゴリバナー" width="320"/>
	</div>

<ul class="accordion_ul">
    <li>
        <section>
        <h1>北海道版<br /><span style="font-size:60%;color:#777777;">北海道（札幌）</span></h1>
        <ul>
          <li><a href="http://hokkaido.dhrank.com/s/top">北海道（札幌）</a></li>
        </ul>
      </section>
    </li>
    <li>
        <section>
        <h1>東北版<br /><span style="font-size:60%;color:#777777;">青森・岩手・宮城・秋田・山形・福島</span></h1>
        <ul>
		  <li><a href="http://aomori.dhrank.com/s/top">青森</a></li>
            <li><a href="http://iwate.dhrank.com/s/top">岩手</a></li>
            <li><a href="http://miyagi.dhrank.com/s/top">宮城</a></li>
            <li><a href="http://akita.dhrank.com/s/top">秋田</a></li>
            <li><a href="http://yamagata.dhrank.com/s/top">山形</a></li>
            <li><a href="http://fukushima.dhrank.com/s/top">福島</a></li>
        </ul>
      </section>
    </li>
    <li>
        <section>
        <h1>北信越版<br /><span style="font-size:60%;color:#777777;">富山・石川・福井・長野・新潟・山梨</span></h1>
        <ul>
			<li><a href="http://toyama.dhrank.com/s/top">富山</a></li>
			<li><a href="http://ishikawa.dhrank.com/s/top">石川</a></li>
            <li><a href="http://fukui.dhrank.com/s/top">福井</a></li>
            <li><a href="http://nagano.dhrank.com/s/top">長野</a></li>
            <li><a href="http://niigata.dhrank.com/s/top">新潟</a></li>
            <li><a href="http://yamanashi.dhrank.com/s/top">山梨</a></li>
        </ul>
      </section>
    </li>
    <li>
        <section>
        <h1>北関東版<br /><span style="font-size:60%;color:#777777;">茨城・栃木・群馬</span></h1>
        <ul>
			<li><a href="http://ibaraki.dhrank.com/s/top">茨城</a></li>
			<li><a href="http://tochigi.dhrank.com/s/top">栃木</a></li>
            <li><a href="http://gunma.dhrank.com/s/top">群馬</a></li>
        </ul>
      </section>
    </li>
    <li>
        <section>
        <h1>関東版<br /><span style="font-size:60%;color:#777777;">東京・神奈川・埼玉・千葉</span></h1>
        <ul>
			<li><a href="http://shinjyuku.dhrank.com/s/top">新宿</a></li>
			<li><a href="http://ikebukuro.dhrank.com/s/top">池袋</a></li>
			<li><a href="http://shibuya.dhrank.com/s/top">渋谷</a></li>
			<li><a href="http://shinagawagotanda.dhrank.com/s/top">品川・五反田</a></li>
			<li><a href="http://nishitokyo.dhrank.com/s/top">西東京</a></li>
			<li><a href="http://kinshicho.dhrank.com/s/top">錦糸町</a></li>
			<li><a href="http://uenouguisudani.dhrank.com/s/top">上野・鴬谷</a></li>
            <li><a href="http://kanagawa.dhrank.com/s/top">神奈川</a></li>
            <li><a href="http://saitama.dhrank.com/s/top">埼玉</a></li>
            <li><a href="http://chiba.dhrank.com/s/top">千葉</a></li>
        </ul>
      </section>
    </li>
    <li>
        <section>
        <h1>東海版<br /><span style="font-size:60%;color:#777777;">愛知(名古屋)・岐阜・三重・静岡</span></h1>
        <ul>
			<li><a href="http://aichi.dhrank.com/s/top">愛知(名古屋)</a></li>
            <li><a href="http://gifu.dhrank.com/s/top">岐阜</a></li>
            <li><a href="http://mie.dhrank.com/s/top">三重</a></li>
            <li><a href="http://shizuoka.dhrank.com/s/top">静岡</a></li>
        </ul>
      </section>
    </li>
    <li>
        <section>
        <h1>関西版<br /><span style="font-size:60%;color:#777777;">大阪・京都・兵庫・滋賀・奈良・和歌山</span></h1>
        <ul>
          <li><a href="http://osaka.dhrank.com/s/top">大阪</a></li>
            <li><a href="http://kyoto.dhrank.com/s/top">京都</a></li>
            <li><a href="http://hyogo.dhrank.com/s/top">兵庫</a></li>
            <li><a href="http://shiga.dhrank.com/s/top">滋賀</a></li>
            <li><a href="http://nara.dhrank.com/s/top">奈良</a></li>
            <li><a href="http://wakayama.dhrank.com/s/top">和歌山</a></li>
        </ul>
      </section>
    </li>
    <li>
        <section>
        <h1>中国版<br /><span style="font-size:60%;color:#777777;">鳥取・島根・岡山・広島・山口</span></h1>
        <ul>
            <li><a href="http://tottori.dhrank.com/s/top">鳥取</a></li>
            <li><a href="http://shimane.dhrank.com/s/top">島根</a></li>
            <li><a href="http://okayama.dhrank.com/s/top">岡山</a></li>
			<li><a href="http://hiroshima.dhrank.com/s/top">広島</a></li>
            <li><a href="http://yamaguchi.dhrank.com/s/top">山口</a></li>
        </ul>
      </section>
    </li>
    <li>
        <section>
        <h1>四国版<br /><span style="font-size:60%;color:#777777;">愛媛・香川・高知・徳島</span></h1>
        <ul>
            <li><a href="http://ehime.dhrank.com/s/top">愛媛</a></li>
            <li><a href="http://kagawa.dhrank.com/s/top">香川</a></li>
            <li><a href="http://kochi.dhrank.com/s/top">高知</a></li>
			<li><a href="http://tokushima.dhrank.com/s/top">徳島</a></li>
        </ul>
      </section>
    </li>
<li>
        <section>
        <h1>九州・沖縄版<br /><span style="font-size:60%;color:#777777;">福岡・佐賀・大分・長崎・熊本・宮崎・鹿児島・沖縄</span></h1>
        <ul>
			<li><a href="http://fukuoka.dhrank.com/s/top">福岡</a></li>
            <li><a href="http://saga.dhrank.com/s/top">佐賀</a></li>
            <li><a href="http://oita.dhrank.com/s/top">大分</a></li>
            <li><a href="http://nagasaki.dhrank.com/s/top">長崎</a></li>
            <li><a href="http://kumamoto.dhrank.com/s/top">熊本</a></li>
            <li><a href="http://miyazaki.dhrank.com/s/top">宮崎</a></li>
            <li><a href="http://kagoshima.dhrank.com/s/top">鹿児島</a></li>
            <li><a href="http://okinawa.dhrank.com/s/top">沖縄</a></li>
        </ul>
    </section>
  </li>
</ul>




	<footer>
		<nav id="cmn-bread" class="jq-app-bc" data-bc-no-edge-scroll="left"><!--
		--><ul class="bc-inr"><!--
		--><li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://dhrank.com/s/" itemprop="url"><span itemprop="title">HOME</span></a></li><!--
		--><li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"></li><!--
		--></ul><!--
	--></nav>

		<p class="btn_or"><a href="http://shinjyuku.dhrank.com/s/accounts/login/">ログイン・新規会員登録</a></p>

		<ul class="fot-link">
			<li><a href="http://shinjyuku.dhrank.com/s/front/about">当サイトについて</a></li>
			<li><a href="http://shinjyuku.dhrank.com/s/front/term">ご利用規約</a></li>
			<li><a href="http://shinjyuku.dhrank.com/s/contact">お問い合わせ</a></li>
		</ul>
	</footer>

	<div id="copyright">Copyright © 口コミデリヘルランキング All Rights Reserved.</div>


<script type="text/javascript">
	$(function() {
    $("#tabs li").click(function() {
        var num = $("#tab li").index(this);
        $(".content_wrap").addClass('disnon');
        $(".content_wrap").eq(num).removeClass('disnon');
        $("#tabs li").removeClass('select');
        $(this).addClass('select')
    });
     $('.bxslider').bxSlider();
    $('.links').biggerlink();

});

$(document).ready(function(){


	//tabs
	$(function() {

		$(".content_wrap").hide();
		$("#tabs li:first").addClass("").show();
		$(".content_wrap:first").show();

		$("#tabs li").click(function() {
			$("#tabs li").removeClass("select");
			$(this).addClass("select");

			var num = $("#tabs li").index(this);
			$(".content_wrap").not(num).fadeOut(1000).eq(num).fadeIn(1000);
			return false;
		});
	});

	//lightbox_me
	$('.frameOpen').click(function(e) {
    $('#frame').lightbox_me({
        centered: true,
        onLoad: function() {
            $('#frame').find('input:first').focus()
            }
        });
    e.preventDefault();
	});

})

$('.accordion_ul ul').hide();
    $('.accordion_ul h1').click(function(e){
    $(this).toggleClass("active");
    $(this).next("ul").slideToggle();
});

$('.accordion_ul ul').hide();
    $('.accordion_ul h2').click(function(e){
    $(this).toggleClass("active");
    $(this).next("ul").slideToggle();
});


</script>

<?php echo $this->element('sql_dump'); ?>

</body>
</html>
