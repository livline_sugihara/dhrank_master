<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<?php echo $this->Html->charset(); ?>
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title>ランキングと口コミで探せるデリヘル情報サイト[デリヘル人気口コミランキング] トップページ</title>
<meta name="description" content="ランキングと口コミで探せるデリヘル情報サイト[デリヘル人気口コミランキング]" />
<meta name="keywords" content="トップ,ランキング,口コミ,デリヘル" />
<?php echo $this->Html->css('style');?>
</head>


<body>

<div id="header_top">
<h1>ランキングと口コミで探せるデリヘル情報サイト[デリヘル人気口コミランキング] トップページ</h1>

<div id="header_wrap">
<p class="header_left">
<a href="top"><img src="images/logo_top.png" /></a><span>ランキングと口コミで探せるデリヘル情報サイト</span>
</p>
<p class="header_right">

</p>
</div>

</div>
<!--/header-->

<div id="container">


<div id="contents">


<div id="main_index">

<p class="breadcrumb"><?php echo $breadcrumbList->get_breadcrumb_list()?></p>

<?php echo $content_for_layout; ?>

</div>
<!--/main-->



<div id="footer">


<?php //start---2013/3/11 障害No.3-0010修正 ?>
<?php if(isset($mediaCategory) && $mediaCategory == 's'){?>
<div>
	PC版 ｜
	<a href="<?php echo substr($html->webroot,0,-1) . '/s' . $str = rtrim($base_url, '/') . '/view:s';?>">スマートフォン版</a>
</div>
<?php }?>
<?php //end---2013/3/11 障害No.3-0010修正 ?>

<!-- レビュアー会員・<a href="#">ログイン</a>/<a href="#">新規登録</a> ｜  -->
<a href="<?php echo APP_MEMBER_URL?>">店舗会員ログイン</a>
<!--  ｜ <a href="#">当サイトについて</a> ｜ <a href="#">ご利用規約</a>  ｜ <a href="#">お問い合わせ</a> -->

<p class="copy">Copyright(c) デリヘル人気口コミランキング All Rights Reverved. 無断転載禁止</p>
</div>
<!--/footer-->


</div>
<!--/contents-->


</div>
<!--/container-->

<?php echo $this->element('sql_dump'); ?>
</body>
</html>
