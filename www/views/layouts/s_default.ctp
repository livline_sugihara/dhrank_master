<!DOCTYPE html>
<html lang="ja" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja">
<head>
<?php echo $this->Html->charset(); ?>

<title>ランキングと口コミで探せるデリヘル情報サイト[デリヘル人気口コミランキング] トップページ</title>
<meta name="description" content="ランキングと口コミで探せるデリヘル情報サイト[デリヘル人気口コミランキング]" />
<meta name="keywords" content="トップ,ランキング,口コミ,デリヘル" />

<meta name="viewport" content="width=device-width, initial-scale=1">

<?php echo $this->Html->css('/css/s/style');?>
<?php echo $this->Html->css('/css/s/jquery.mobile.structure-1.2.0');?>
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
	<script type="application/javascript">
	// ajax機能無効化
	$(document).bind("mobileinit", function(){
		$.mobile.ajaxEnabled = false;
		$.mobile.hashListeningEnabled = false;
	});
	</script>

<script src="/js/s/jquery.mobile-1.2.0.js"></script>

</head>

<body>

<div data-role="page" data-theme="a">
<div data-role="header" data-theme="a">
<h1>ランキングと口コミで探せるデリヘル情報サイト[デリヘル人気口コミランキング] トップページ</h1>
</div>

<div class="header">
	<div class="head_inner">
		<img src="/images/s/title.png" width="210px" />
	</div>
</div>

<div data-role="content" data-theme="a">

<?php echo $content_for_layout; ?>

</div>

<div data-role="footer" data-theme="a">

<div style="text-align:center; margin:5px;">
<?php if(isset($mediaCategory) && $mediaCategory == 's'){?>
表示：スマートフォン <a href="<?php echo substr($html->webroot,0,-1) . substr(rtrim($base_url, '/'),2) . '/view:pc';?>">PC</a>
<?php }?>
</div>

<!-- 「LINEで送る」部分ここから -->
<div style="text-align:center; margin:5px;">
<script type="text/javascript" src="http://media.line.naver.jp/js/line-button.js" ></script>
<script type="text/javascript">
new jp.naver.line.media.LineButton({"pc":false,"lang":"ja","type":"a"});
</script>
</div>
<!-- 「LINEで送る」部分ここまで -->

<h5 style="font-size:9px;">デリヘル人気口コミランキング</h5>
</div>


</div>

<?php echo $this->element('sql_dump'); ?>

</body>
</html>

