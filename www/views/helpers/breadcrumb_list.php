<?php
class BreadcrumbListHelper extends Helper {
	var $helpers = array('Html');

	//店舗情報ヘッダ(PC)
	function get_breadcrumb_list(){

		$this->Html->addCrumb('年齢認証', '/');
		$this->Html->addCrumb('全国デリヘル情報', '/top');
		return $this->Html->getCrumbs(' &gt; ','');

	}
}
