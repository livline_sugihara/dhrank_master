<?php

Router::connect('/', array('controller' => 'top', 'action' => 'index'));
// Router::connect('/top/*', array('controller' => 'top', 'action' => 'top'));

Router::connect('/i/', array('controller' => 'top', 'action' => 'index', 'prefix' => 'i', 'i'=>true));
Router::connect('/i/top', array('controller' => 'top', 'action' => 'top', 'prefix' => 'i', 'i'=>true));
Router::connect('/i/', array('prefix' => 'i', 'i'=>true));

Router::connect('/s/', array('controller' => 'top', 'action' => 'index', 'prefix' => 's', 's'=>true));
Router::connect('/s/top/*', array('controller' => 'top', 'action' => 'top', 'prefix' => 's', 's'=>true));
Router::connect('/s/', array('prefix' => 's', 's'=>true));


//start---2013/3/11 障害No.3-0009修正
Router::connect('/view:pc', array('controller' => 'top', 'action' => 'index'));
Router::connect('/s/view:s', array('controller' => 'top', 'action' => 'index', 'prefix' => 's', 's'=>true));
//end---2013/3/11 障害No.3-0009修正
