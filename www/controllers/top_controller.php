<?php

class TopController extends AppController {
	var $name = 'Top';
	var $uses = array('Meta','AllareaLink', 'BigBanner', 'Shop', 'User');
	var $helpers = array('ImgCommon');

	function index() {
		$this->layout = 'index';
	}
	function i_index() {
		$this->index();
	}
	function s_index() {
		$this->index();

		$banner = $this->Shop->find('all', array(
			'conditions' => array( 'NOT' => array('image_recommend' => null)),
			'fields' => array('user_id', 'image_recommend'),
			'desc' => 'rand()',
		));
		$key = array_rand($banner);
		$this->set('banner', $banner[$key]);


		// 注）ランダム生成
		// $max = $this->BigBanner->find('count');
		// $banner = $this->BigBanner->find('first',array(
		// 	'conditions' => array('BigBanner.id' => mt_rand(0,$max))
		// ));
 	// 	$this->set('banner', $banner);



	}

	function top() {


		//全国リンク集

		$link_order = '';

		//表示順取得

		$link_order_array = $this->Meta->find('first', array('conditions' => array('name' => 'allarea_link_order')));

		if($link_order_array['Meta']['value'] == 1){
			//start---2013/3/6 障害No.3-0005修正

			//$link_order = 'AllareaLink.show_order ASC';
			$link_order = 'AllareaLink.show_order ASC, AllareaLink.created ASC';
			//end---2013/3/6 障害No.3-0005修正

		}elseif($link_order_array['Meta']['value'] == 2){

			$link_order = 'rand()';

		}

		//リンク集取得

		$this->set('list', $this->AllareaLink->find('all', array('order' => $link_order)));

	}
	function i_top(){
		$this->top();
	}
	function s_top(){
		$this->top();
	}

	function kyushu() {
		$this->layout = 'kyushu';
	}
	function s_kyushu() {
		$this->kyushu();
	}

}
?>
