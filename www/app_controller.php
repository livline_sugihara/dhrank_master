<?php
class AppController extends Controller {
	var $uses = array();
	var $helpers = array('Html','Form','Text','BreadcrumbList');
	var $components = array('Cookie','MediaInfo');

	var $device_file_name = '';
	var $device_path = '';
	var $parent_area = '';
	var $start_time;

	function beforefilter(){
		$this->start_time = microtime(TRUE);

		//base_url設定
		$base_url = Router::url(null, false);
		$this->set('base_url', $base_url);

		//端末振り分け
		$mediaCategory = $this->MediaInfo->getMediaCategory();
		$this->set('mediaCategory', $mediaCategory);
		//携帯もしくは携帯クローラーからのアクセス
		//ユーザーエージェントが取れない場合のエラー対応
		if($mediaCategory == 'i' || (!empty($_SERVER['HTTP_USER_AGENT']) && preg_match("/(Googlebot-Mobile|Y\!J-SRD\/1.0|Y\!J-MBS\/1.0|mobile goo)/i",$_SERVER['HTTP_USER_AGENT']))){
			if(isset($this->params['prefix'])){
				//スマートフォンサイトにアクセスしている場合
				if($this->params['prefix'] == 's'){
					$this->redirect('/i' . substr($base_url,2));
				}
			}else{
				//PCサイトにアクセスしている場合
				$this->redirect('/i' . $base_url);
			}
		}elseif($mediaCategory == 's'){
			//スマートフォンからのアクセス
			if(isset($this->params['prefix'])){
				//携帯サイトにアクセスしている場合
				if($this->params['prefix'] == 'i'){
					$this->redirect('/s' . substr($base_url,2));
				}elseif($this->params['prefix'] == 's'){
					//スマートフォンサイトにアクセスしている場合
					//PC→スマートフォンサイト移動時
					if(preg_match("/view:s/", Router::url(null, false))){
						//クッキー削除
						$this->Cookie->delete('view');
						//start---2013/3/11 障害No.3-0009修正
						//$this->redirect(str_replace('/view:s','',$base_url));
						$redirect_to = str_replace('/view:s','',$base_url);
						if($redirect_to != '/s'){
							$this->redirect($redirect_to);
						}else{
							$this->redirect('/s/');
						}
						//end---2013/3/11 障害No.3-0009修正
					}elseif($this->Cookie->read('view') == 'pc'){
						//PCサイト状態時
						$this->redirect(substr($base_url,2));
					}
				}
			}else{
				//PCサイトへアクセスしている場合
				//スマートフォン→PCサイト移動時
				if(preg_match("/view:pc/", Router::url(null, false))){
					//クッキー作成
					$this->Cookie->write('view', 'pc', false, 30 * 24 * 60 * 60);
					//start---2013/3/11 障害No.3-0009修正
					//$this->redirect(str_replace('/view:pc','',$base_url));
					$redirect_to = str_replace('/view:pc','',$base_url);
					if($redirect_to != ''){
						$this->redirect($redirect_to);
					}else{
						$this->redirect('/');
					}
					//end---2013/3/11 障害No.3-0009修正
				}elseif($this->Cookie->read('view') != 'pc'){
					//PCサイト状態時でない
					$this->redirect('/s' . $base_url);
				}
			}
		}

		//prefix設定
		if(isset($this->params['prefix'])){
			if($this->params['prefix'] == 'i'){
				$this->device_file_name = 'i_';
				$this->device_path = '/i';
			}elseif($this->params['prefix'] == 's'){
				$this->device_file_name = 's_';
				$this->device_path = '/s';
			}
		}
		// 入力データの文字コード変換
		if(isset($this->params['prefix'])){
			if($this->params['prefix'] == 'i'){
				if(!empty($this->data)){
					array_walk_recursive($this->data, 'AppController::encode_mobile');
				}
			}
		}

	}
	//文字コード変換←ココ
	function encode_mobile(&$item, $key){
		//echo "$key holds $item\n";
		$item = mb_convert_encoding($item, 'UTF-8', 'SJIS');
		//echo "$key =&gt; $item\n";
	}
	function beforeRender(){
		if(isset($this->params['prefix'])){
			if($this->params['prefix'] == 'i'){
				$this->layout = 'i_'.$this->layout;
			}elseif($this->params['prefix'] == 's'){
				$this->layout = 's_'.$this->layout;
			}
		}
	}
	function afterFilter(){
		parent::afterFilter();
		if(isset($this->params['prefix'])){
			//携帯の場合
			if($this->params['prefix'] == 'i'){
				// 全角文字の変換
				$this->output = mb_convert_kana($this->output, "rak", 'UTF-8');
				// 出力文字コードの変換
				$this->output = mb_convert_encoding($this->output, "SJIS", 'UTF-8');
			}elseif($this->params['prefix'] == 's'){
			}
		}
		echo (Configure::read('debug'))?(microtime(TRUE) - $this->start_time)*1000:'';
	}
}

