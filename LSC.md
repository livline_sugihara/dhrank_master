# LSC 構成・必要情報など
-----

## FTP・ドメイン
-----

_**FTP**_  
_HOST_ : 19800210.com  
_ID_ : liv1980ftp  
_Pass_ : #Cq5vfaJM  

_**Basic**_  
_ID_ : dhrank  
_Pass_ : dhpass  

注）  
サブドメイン分けされているのでアクセスは  
****.19800210.com/  
になります。  
基本  
osaka.19800210.com  
でOK  




## 作業依頼箇所
-----

1. お店情報TOPのメインビジュアルをスライドショー化  
  ３枚を自動で回してください。  
  送りボタンなし + 下部に ● 付き  

    _URL_ :  
    http://osaka.19800210.com/12869  <-店舗id  
    _DIR_ :  
    /web/site/views/users/index.ctp  
    /web/site/views/elements/common/shop_profile.ctp  

    *スマホ参考  
    http://osaka.19800210.com/s/12869  
    /web/site/views/users/s_index.ctp  


1. 店舗料金システムの表示変更  
※デザイン来ていないので来たら対応お願いします。

    _URL_ :  
    http://osaka.19800210.com/12869/systems  
    _DIR_ :  
    /web/site/views/users/systems.ctp  


1. 割引のお知らせも  
※デザイン来ていないので来たら対応お願いします。  

    _URL_ :  
    http://osaka.19800210.com/12869/coupons  
    _DIR_ :  
    /web/site/views/users/coupons.ctp  


1. 掲示板作成  
※相原さんがデザインを上げてくれるのでそちらを作成ください。

    _URL_ :  
    http://osakam.19800210.com/boards  <-店舗id  
    _DIR_ :  
    /web/sitem/views/users/index.ctp  
    /web/sitem/views/elements/common/shop_profile.ctp  
