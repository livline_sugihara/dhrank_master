<?php
class AppController extends Controller {
	var $uses = array('Shop','User','UsersAuthority','Review','MNumericValue');
	var $components = array('Auth','Session','MediaInfo');
	//start---2013/4/25 障害No.4-0001修正
	//var $helpers = array('Html','Form','LinkCommon','StyleCommon','Session','TextCommon');
	var $helpers = array('Html','Form','LinkCommon','StyleCommon','Session','TextCommon','ImgCommon');
	//end---2013/4/25 障害No.4-0001修正

	var $parent_user = array();
	var $device_file_name = '';
	var $device_path = '';
	var $pcount = array();
	var $start_time;
	var $title_tag_common = '';
	var $h1_tag_common = '';

	// パンくず情報を保持
	public $breadcrumb = array();

	function beforeFilter() {
		$this->start_time = microtime(TRUE);

		//サブドメインのサイトの場合はリダイレクト先URLはそのまま、フォルダ形式の場合はリダイレクト先URLからフォルダ部分を除外
		$base_url = '';
		if($this->webroot == '/'){
			$base_url = Router::url(null, false);
		}else{
			$base_url = substr(Router::url(null, false),mb_strlen($this->webroot)-1);
		}
		$this->set('base_url', $base_url);

		//端末振り分け
		$mediaCategory = $this->MediaInfo->getMediaCategory();
		$this->set('mediaCategory', $mediaCategory);
		//携帯もしくは携帯クローラーからのアクセス
		//ユーザーエージェントが取れない場合のエラー対応
		if($mediaCategory == 'i' || (!empty($_SERVER['HTTP_USER_AGENT']) && preg_match("/(Googlebot-Mobile|Y\!J-SRD\/1.0|Y\!J-MBS\/1.0|mobile goo)/i",$_SERVER['HTTP_USER_AGENT']))){
			if(!isset($this->params['prefix'])){
				//PCサイトにアクセスしている場合
				$this->redirect('/i' . $base_url);
			}
		}

		if(isset($this->params['prefix'])){
			if($this->params['prefix'] == 'i'){
				$this->device_file_name = 'i_';
				$this->device_path = '/i';
			}
		}
		//レスポンスヘッダにキャッシュさせない情報を付加
		$this->disableCache();

		$this->Auth->authError = "ログインをお願いします。";
		$this->Auth->loginError = "ログインに失敗しました。";
		$this->layout = 'default';
		$this->setUserAuth();

		// ログイン済み＆ログアウト時でない（無限ループ対策）
		if($this->Auth->user() && ($this->name != 'Users' && $this->action != 'logout')){
			//店舗情報編集ページでない
			if($this->name != 'Shops' || ($this->name == 'Shops' && $this->action == 'edit_systems')){
				// 店舗情報登録がない場合
				$count = $this->Shop->find('count',array(
						'conditions' => array(
								'Shop.user_id' => $this->Auth->user('id'),
								'Shop.is_created' => 1)));
				if($count == 0){
					$this->Session->setFlash('まずはじめに店舗情報を登録してください。');
					$this->redirect($this->device_path . '/shops');
				}
			}
		}
		//店舗アカウントデータ
		$this->parent_user = $this->User->findbyId($this->Auth->user('id'));

		$users_authorities = $this->UsersAuthority->find('all', array(
			'conditions' => array(
				'UsersAuthority.user_id' => $this->Auth->user('id'),
			)
		));
		$this->parent_user['UsersAuthority'] = array(
			'is_shop_header_image' => 0,
			'is_recommend_girl' => 0,
			'is_pickup_review' => 0,
			'is_delete_review' => 0,
		);
		// どこかのエリアでフラグが立っていたら修正可能にする
		foreach($users_authorities AS $ii => $record) {
			if($record['UsersAuthority']['is_shop_header_image'] == 1) {
				$this->parent_user['UsersAuthority']['is_shop_header_image'] = 1;
			}
			if($record['UsersAuthority']['is_recommend_girl'] == 1) {
				$this->parent_user['UsersAuthority']['is_recommend_girl'] = 1;
			}
			if($record['UsersAuthority']['is_pickup_review'] == 1) {
				$this->parent_user['UsersAuthority']['is_pickup_review'] = 1;
			}
			if($record['UsersAuthority']['is_delete_review'] == 1) {
				$this->parent_user['UsersAuthority']['is_delete_review'] = 1;
			}
		}
		$this->set('parent_user',$this->parent_user);

		// 入力データの文字コード変換
		if(isset($this->params['prefix'])){
			if($this->params['prefix'] == 'i'){
				if(!empty($this->data)){
					array_walk_recursive($this->data, 'AppController::encode_mobile');
				}
			}
		}

		//表示件数セット
		if(!isset($this->params['prefix'])){
			//PC版
			$this->pcount['top_reviews'] = APP_PCOUNT_MEMBER_TOP_REVIEWS;
			$this->pcount['reviews_list'] = APP_PCOUNT_MEMBER_REVIEWS_LIST;
		}else{
			if($this->params['prefix'] == 'i'){
				//携帯版
				$this->pcount['top_reviews'] = APP_PCOUNT_MEMBER_TOP_REVIEWS_I;
				$this->pcount['reviews_list'] = APP_PCOUNT_MEMBER_REVIEWS_LIST_I;
			}
		}


		//口コミデータ
		$review_data = $this->Review->find('first', array(
				'fields' => array(
					'ifnull(SUM(Review.score_girl_character),0) / count(Review.id) as score_girl_character',
					'ifnull(SUM(Review.score_girl_service),0) / count(Review.id) as score_girl_service',
					'ifnull(SUM(Review.score_girl_looks),0) / count(Review.id) as score_girl_looks',
					'count(Review.id) as count'
				),
				'conditions' => array(
						'User.id' => $this->Auth->user('id'),
				),
				'group' => array('User.id'),
		));
		if(empty($review_data[0]['count'])){
			$review_data[0]['count'] = 0;
			$review_data[0]['total'] = 0;
			$review_data[0]['score_girl_character'] = 0;
			$review_data[0]['score_girl_service'] = 0;
			$review_data[0]['score_girl_looks'] = 0;
		}else{
			$review_data[0]['total'] = ($review_data[0]['score_girl_character']+$review_data[0]['score_girl_service']+
					$review_data[0]['score_girl_looks']) / 3;
		}
		$this->set('review_data', $review_data);

		//口コミデータ（点数別）
		// $review_data_marks = $this->Review->find('all', array(
		// 		'fields' => array(
		// 				'TRUNCATE((Review.score_shop_tel + Review.score_shop_time + Review.score_shop_cost + ' .
		// 				'Review.score_girl_first_impression + Review.score_girl_word + Review.score_girl_service + ' .
		// 				'Review.score_girl_sensitivity + Review.score_girl_style + Review.score_girl_voice) / 9 + 0.5, 0) as avg',
		// 				'count(Review.id) as count'
		// 		),
		// 		'conditions' => array(
		// 				'User.id' => $this->Auth->user('id'),
		// 		),
		// 		'group' => array(
		// 				'TRUNCATE((Review.score_shop_tel + Review.score_shop_time + Review.score_shop_cost + ' .
		// 				'Review.score_girl_first_impression + Review.score_girl_word + Review.score_girl_service + ' .
		// 				'Review.score_girl_sensitivity + Review.score_girl_style + Review.score_girl_voice) / 9 + 0.5, 0)',),
		// ));
		// $count = 0;
		// foreach($review_data_marks as $key => $record){
		// 	if($record[0]['count'] > $count){
		// 		$count = $record[0]['count'];
		// 	}
		// }
		// foreach($review_data_marks as $key => $record){
		// 	$review_data_marks[$key][0]['max'] = $count;
		// }
		// $this->set('review_data_marks', $review_data_marks);

		// //口コミ点数
		// $numeric_values = $this->MNumericValue->find('all');
		// $this->set('m_score',Set::Combine(Set::extract('/MNumericValue[id>=1][id<=5]', $numeric_values),'{n}.MNumericValue.id', null));

		//タイトルタグ・meta・H1設定
		$this->title_tag_common = 'ランキングと口コミで探せるデリヘル情報サイト[デリヘル人気口コミランキング] ';
		$this->h1_tag_common = 'ランキングと口コミで探せるデリヘル情報サイト[デリヘル人気口コミランキング] ';


	}
	//文字コード変換←ココ
	function encode_mobile(&$item, $key){
		$item = mb_convert_encoding($item, 'UTF-8', 'SJIS');
	}

	function setUserAuth() {
		$this->Auth->userModel = 'User';
		$this->Auth->loginRedirect = $this->device_path . '/';
		$this->Auth->autoRedirect = false;
	}


	function beforeRender(){
		if(isset($this->params['prefix'])){
			if($this->params['prefix'] == 'i'){
				$this->layout = 'i_'.$this->layout;
			}
		}

		// パンくず
		$this->set('breadcrumb', $this->breadcrumb);
	}
	function afterFilter(){
		parent::afterFilter();
		if(isset($this->params['prefix'])){
			//携帯の場合
			if($this->params['prefix'] == 'i'){
				// 全角文字の変換
				//$this->output = mb_convert_kana($this->output, "rak", 'UTF-8');
				// 出力文字コードの変換
				$this->output = mb_convert_encoding($this->output, "SJIS", 'UTF-8');
			}
		}
		echo (Configure::read('debug'))?(microtime(TRUE) - $this->start_time)*1000:'';
	}

	function __construct(){
		// /i/がURLの中にあった場合、MOBILE変数を設定。このフラグを各クラスで判別
		if(ereg("^/i/",$_SERVER['REQUEST_URI'])){ // もし、URLが/<em>servicename</em>/m/だったら、"^/<em>servicename</em>/m/"
			define('i',1);
		}
		parent::__construct();
	}

	//redirect メソッド書き換え
	function redirect($url,$status = null){
		if(defined('i')){
			$url = $url."?".session_name()."=".session_id(); //転送先URLにセッションIDを自動付加
		}
		parent::redirect($url,$status);
	}

	//conditions当該地域
	function getCurrentAreaCondition(){
		return array(
			'User.large_area_id' => $this->parent_user['LargeArea']['id'],
			'Shop.is_created' => 1,
		);
	}

// -------------------------------------------------------------------
// 追加機能分
// -------------------------------------------------------------------
	function addBreadCrumbs($name, $link) {
		$this->breadcrumb[] = array("name" => $name , "link" => $link);
	}

	function clearBreadCrumbs() {
		$this->breadcrumb = array();
	}

	/**
	 * レビュー情報取得共通関数
	*/
	function _getReviews($conditions, $order, $limit = -1, $add_fields = array(), $only_publish = true, $add_deleted = false) {

		if($only_publish) {
			if(!in_array('Review.is_publish', $conditions))
			{
				$conditions['Review.is_publish'] = 1;
			}
		}
		if(!$add_deleted) {
			if(!in_array('Review.comment_deleted', $conditions))
			{
				$conditions['Review.comment_deleted'] = 0;
			}
		}

		$fields = array(
			'LargeArea.url','User.id','MReviewersAge.name','MReviewsPlace.name',
			'Shop.*','Girl.*','Review.*','Reviewer.*',
			'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg',
			'ifnull((SELECT count(reviews.id) FROM reviews WHERE reviews.reviewer_id > 0 AND reviews.reviewer_id = Reviewer.id), 0) AS review_cnt',
		);

		if(!empty($add_fields)) {
			$fields = array_merge($fields, $add_fields);
		}

		$params = array(
			'fields' => $fields,
			'conditions' => $conditions,
			'group' => array('Review.id'),
			'order' => $order,
		);

		if($limit > 0) {
			$params['limit'] = $limit;
		}

		$reviews = $this->Review->find('all', $params);

		foreach($reviews AS $key => $record) {
			$reviews[$key]['Review']['appointed_type_name'] = $record['Review']['appointed_type'] == 0 ? '' : Review::$appointed_type[$record['Review']['appointed_type']];
			$reviews[$key]['Review']['appointed_sub_name'] = $record['Review']['appointed_sub'] == 0 ? '' : Review::$appointed_type[$record['Review']['appointed_sub']];
		}

		return $reviews;
	}

}
?>