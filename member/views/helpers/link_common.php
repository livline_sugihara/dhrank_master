<?php
class LinkCommonHelper extends Helper {
	var $helpers = array('Html');

	function get_device_path(){
		if(!empty($this->params['prefix'])){
			return '/' . $this->params['prefix'];
		}
	}

	function get_prefix_portal(){
		if(!empty($this->params['prefix'])){
			return $this->params['prefix'] . '/';
		}
	}

	//TOP
	function get_top(){
		return $this->output($this->get_device_path() . '/');
	}
	//ログイン
	function get_login(){
		return $this->output($this->get_device_path() . '/users/login');
	}
	//パスワード変更
	function get_changepassword_send(){
		return $this->output($this->get_device_path() . '/users/changepassword_send');
	}
	//ショップ編集画面
	function get_shops_index(){
		return $this->output($this->get_device_path() . '/shops');
	}
	//基本システム編集画面
	function get_systems_index(){
		return $this->output($this->get_device_path() . '/systems');
	}
	//女の子一覧
	function get_girls_index(){
		return $this->output($this->get_device_path() . '/girls');
	}
	//女の子登録
	function get_girls_add(){
		return $this->output($this->get_device_path() . '/girls/add');
	}
	//女の子編集
	function get_girls_edit($record){
		return $this->output($this->get_device_path() . '/girls/edit/' . $record['Girl']['id']);
	}
	//女の子削除
	function get_girls_delete($record){
		return $this->output($this->get_device_path() . '/girls/delete/' . $record['Girl']['id']);
	}
	//女の子画像投稿
	function get_girls_imageupload($record){
		return $this->output($this->get_device_path() . '/girls/imageupload/' . $record['Girl']['id']);
	}
	//女の子画像表示
	function get_girls_image($record,$filename){
		return $this->output($this->get_device_path() . '/girls/image/' . $record['Girl']['id'] . '/' . $filename);
	}
	//女の子並び順
	function get_girls_orders_index(){
		return $this->output($this->get_device_path() . '/girls/order');
	}
	//口コミ
	function get_reviews_index(){
		return $this->output($this->get_device_path() . '/reviews');
	}
	//口コミ削除
	function get_reviews_delete($record){
		return $this->output($this->get_device_path() . '/reviews/delete/' . $record['Review']['id']);
	}
	//口コミ内容表示・メッセージ送信
	function get_reviews_send_message($record){
		return $this->output($this->get_device_path() . '/reviews/send_message/' . $record['Review']['id']);
	}
	//クーポン編集
	function get_coupons_edit($record){
		return $this->output($this->get_device_path() . '/coupons/edit/' . $record['Coupon']['id']);
	}
	//女の子削除
	function get_coupons_delete($record){
		return $this->output($this->get_device_path() . '/coupons/delete/' . $record['Coupon']['id']);
	}
	//クーポン一覧
	function get_coupons_index(){
		return $this->output($this->get_device_path() . '/coupons');
	}

}
