<?php
class ImgCommonHelper extends Helper {
	var $helpers = array('Html');

	//GIRL
	function get_girl($record, $size, $number = 1, $name = null, $alt = null, $style= null){
		$width = '';
		$height = '';
		if($size == 's'){
			$width = APP_IMG_GIRL_S_WIDTH;
			$height = APP_IMG_GIRL_S_HEIGHT;
		}elseif($size == 'm'){
			$width = APP_IMG_GIRL_M_WIDTH;
			$height = APP_IMG_GIRL_M_HEIGHT;
		}elseif($size == 'l'){
			$width = APP_IMG_GIRL_L_WIDTH;
			$height = APP_IMG_GIRL_L_HEIGHT;
		}
		//女の子画像が存在する場合
		if($this->is_exist_girl_image($record, $size, $number)) {
			return $this->output($this->Html->image(
					APP_IMG_URL . 'girl/' . $record['User']['id']  . '/'  . $record['Girl']['id'] . '/' . $record['Girl']['image_' . $number . '_' . $size],
					array('width' => $width,'height' => $height, 'name' => $name, 'alt' => $alt, 'style' => $style)));
		} else {
			return $this->output($this->Html->image(
					APP_IMG_URL . 'common/girl_' . $size . '.jpg',
					array('width' => $width,'height' => $height, 'name' => $name, 'alt' => $alt, 'style' => $style)));
		}
	}

	//GIRL
	function get_girl_with_time($record, $size, $number = 1, $id = null, $cls = null, $name = null, $alt = null, $style= null){
		$width = '';
		$height = '';
		if($size == 's'){
			$width = APP_IMG_GIRL_S_WIDTH;
			$height = APP_IMG_GIRL_S_HEIGHT;
		}elseif($size == 'm'){
			$width = APP_IMG_GIRL_M_WIDTH;
			$height = APP_IMG_GIRL_M_HEIGHT;
		}elseif($size == 'l'){
			$width = APP_IMG_GIRL_L_WIDTH;
			$height = APP_IMG_GIRL_L_HEIGHT;
		}elseif(is_array($size) && count($size) == 2){
			list($width, $height) = $size;
			$size = 'l';
		}
		//女の子画像が存在する場合
		//start---2013/3/8 障害No.3-0006修正
		//if(!empty($record['Girl']['image_' . $number . '_' . $size])){
		//start---2013/4/25 障害No.4-0001修正
		//if(!empty($record['Girl']['image_' . $number . '_' . $size]) && file_exists(APP_IMG_PATH . 'girl' . DS . $record['Girl']['id'] . DS . $record['Girl']['image_' . $number . '_' . $size])){
		if(!empty($record['Girl']['image_' . $number . '_' . $size]) && file_exists(APP_IMG_PATH . 'girl' . DS . $record['User']['id']  . DS . $record['Girl']['id'] . DS . $record['Girl']['image_' . $number . '_' . $size])){
		//end---2013/4/25 障害No.4-0001修正
		//end---2013/3/8 障害No.3-0006修正
			return $this->output($this->Html->image(
					//start---2013/4/25 障害No.4-0001修正
					//APP_IMG_URL . 'girl/' . $record['Girl']['id'] . '/' . $record['Girl']['image_' . $number . '_' . $size] . '?' . time(),
					APP_IMG_URL . 'girl/' . $record['User']['id']  . '/' . $record['Girl']['id'] . '/' . $record['Girl']['image_' . $number . '_' . $size] . '?' . time(),
					//end---2013/4/25 障害No.4-0001修正
					array('width' => $width,'height' => $height, 'id' => $id, 'class' => $cls, 'name' => $name, 'alt' => $alt, 'style' => $style)));
		}else{
			return $this->output($this->Html->image(
					APP_IMG_URL . 'common/girl_' . $size . '.jpg',
					array('width' => $width,'height' => $height, 'id' => $id, 'class' => $cls, 'name' => $name, 'alt' => $alt, 'style' => $style)));
		}
	}

	// 女の子画像があるかどうかのチェック関数
	function is_exist_girl_image($record, $size, $number = 1) {

		if(!empty($record['Girl']['image_' . $number . '_' . $size]) &&
			file_exists(APP_IMG_PATH . 'girl' . DS . $record['User']['id']  . DS . $record['Girl']['id'] . DS . $record['Girl']['image_' . $number . '_' . $size])){
			return true;
		}

		return false;

	}

	// 店舗
	function get_shop_with_time($record, $type, $size = array(), $style= null, $name = null, $alt = null, $cls = null){

		$width = '';
		$height = '';

		if(!empty($size) && count($size) == 2) {
			$width = $size[0];
			$height = $size[1];
		} else if(!empty($size) && count($size) == 1) {
			$width = $size[0];
		} else {
			switch($type) {
				case 'icon':
					$width = APP_IMG_SHOP_ICON_WIDTH;
					$height = APP_IMG_SHOP_ICON_HEIGHT;
					break;
				case 'recommend':
					$width = APP_IMG_SHOP_RECOMMEND_WIDTH;
					$height = APP_IMG_SHOP_RECOMMEND_HEIGHT;
					break;
				case 'list':
					$width = APP_IMG_SHOP_LIST_WIDTH;
					$height = APP_IMG_SHOP_LIST_HEIGHT;
					break;
				case 'header':
					$width = APP_IMG_SHOP_HEADER_WIDTH;
					$height = APP_IMG_SHOP_HEADER_HEIGHT;
					break;
				case 'profile':
					$width = APP_IMG_SHOP_PROFILE_WIDTH;
					$height = APP_IMG_SHOP_PROFILE_HEIGHT;
					break;
				case 'header_sp':
					$width = APP_IMG_SHOP_HEADER_SP_WIDTH;
					$height = APP_IMG_SHOP_HEADER_SP_HEIGHT;
					break;
				case 'list_sp':
					$width = APP_IMG_SHOP_LIST_SP_WIDTH;
					$height = APP_IMG_SHOP_LIST_SP_HEIGHT;
					break;
			}
		}

		$param = array('name' => $name, 'alt' => $alt, 'style' => $style, 'class' => $cls);

		// 指定されている場合のみ設定
		if($width > 0) {
			$param['width'] = $width;
		}
		if($height > 0) {
			$param['height'] = $height;
		}

		//店舗画像が存在する場合
		if($this->is_exist_shop_image($record, $type))
		{
			return $this->output($this->Html->image(
					APP_IMG_URL . 'shop/' . $record['Shop']['user_id']  . '/' . $record['Shop']['image_' . $type] . '?' . time(),
					$param));
		} else {
			return $this->output($this->Html->image(
					APP_IMG_URL . 'common/noimage.jpg',
					$param));
		}
	}

	// 店舗画像があるかどうかのチェック関数
	function is_exist_shop_image($record, $type) {

		if(!empty($record['Shop']['image_' . $type]) &&
			file_exists(APP_IMG_PATH . 'shop' . DS . $record['Shop']['user_id']  . DS . $record['Shop']['image_' . $type])){
			return true;
		}

		return false;

	}

	//　Reviewer画像
	function get_reviewer_avatar($record, $size = null, $cls = null, $name = null, $alt = null, $style= null){
		$width = '';
		$height = '';

		if(is_array($size) && count($size) == 2){
			list($width, $height) = $size;
		}

		if(isset($record['Reviewer'])) {
			$filePath = WWW_ROOT . '/img/tab_topictoukou_img' . $record['Reviewer']['avatar_id'] . '.png';
			$fileURL = '/img/tab_topictoukou_img' . $record['Reviewer']['avatar_id'] . '.png';

			if(!empty($record['Reviewer']['avatar_id']) && file_exists($filePath)){
				return $this->output($this->Html->image(
						$fileURL,
						array('width' => $width,'height' => $height, 'class' => $cls, 'name' => $name, 'alt' => $alt, 'title' => $alt, 'style' => $style)));
			}
		}
		return $this->output($this->Html->image(
//				APP_IMG_URL . 'common/noimage.jpg',
				'/img/tab_topictoukou_img1.png',
				array('width' => $width,'height' => $height, 'class' => $cls, 'name' => $name, 'alt' => $alt, 'title' => $alt, 'style' => $style)));
	}

}
