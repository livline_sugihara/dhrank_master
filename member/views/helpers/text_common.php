<?php
class TextCommonHelper extends Helper {
	var $helpers = array('Html');

	//テキスト抜粋・タグ削除
	function get_substr_tagstrip_text($text, $size){
		//タグ削除
		$strip_tags_text = strip_tags($text);
		//テキスト抜粋
		$ret_text = mb_substr($strip_tags_text,0,$size);
		//抜粋文字数よりもテキスト文字数が大きい場合
		if(mb_strlen($strip_tags_text) > $size){
			//...を追加
			$ret_text .= '...';
		}
		return $ret_text;
	}

	// 縦表示HTML作成
	function get_vertical_text($text) {

		$ret = '';
		$len = mb_strlen($text);

		for($ii = 0; $ii < $len; $ii++) {
			$ret .= mb_substr($text, $ii, 1);

			if($ii < $len - 1) {
				$ret .= '<br />';
			}
		}

		return $ret;
	}


	function escape_empty($arr, $key, $dummyText = '不明') {

		$arrKeys = explode('.', $key);

		$tmp = $arr;
		foreach($arrKeys AS $k) {
			if(isset($tmp[$k])) {
				if(!is_array($tmp[$k])) {
					if(!empty($tmp[$k])) {
						return $tmp[$k];
					} else {
						return $dummyText;
					}
				}
			} else {
				return $dummyText;
			}
			$tmp = $tmp[$k];
		}

		return $dummyText;
	}
}