<!-- middle -->
<section id="middle" class="two">    <div class="title_bar">口コミ管理</div>
    <div id="reviewListArea" class="inner">
        <div class="pageCounter">
        	<span class="num" id="count_num"><?php echo $paginator->counter(array('format' => '%count%')); ?></span><span>件あります</span>
        	<?php echo $this->element('pagenate/reviews_header_pagenate'); ?>
        </div>
        <div class="new_reviewArea clearfix">
<?php

$cnt = count($list);for ($ii = 0; $ii < $cnt; $ii++) {
            echo $this->element('common/_review', array(
            	"record" => $list[$ii],
            	"reviewer" => true,
            ));
}
?>
        </div>
        <div class="pageCounter">
		    <?php echo $this->element('pagenate/reviews_footer_pagenate'); ?>
        </div>
    </div>
</section>
<?php echo $this->element('pagenate/numbers'); ?>

<?php //echo var_dump($list)?>