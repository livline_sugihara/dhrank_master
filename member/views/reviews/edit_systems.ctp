<section id="middle" class="two">
    <div class="title_bar">システム管理</div>
    
    <div id="timeLineArea" class="inner">
        <div class="new_reviewArea clearfix">

            <div class="shopTable">
    
                <form id="ShopEditSystemsForm" method="post" action="/manager/edit_sys" accept-charset="utf-8">
                <!--
                <div style="display:none;">
                	<input type="hidden" name="_method" value="POST" />
               	</div>
                -->

	<?php echo $form->create(null,array('type'=>'post','action'=>"")); ?>
	<?php echo $form->hidden('System.id'); ?>
	<?php echo $form->hidden('System.user_id'); ?>
                
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th>住所</th>
                            <td colspan="3"><input name="data[Shop][area]" type="text" style="width:450px;" id="ShopArea" placeholder="例）梅田駅前発" title="例）梅田駅前発" /></td>
                        </tr>
                        <tr>
                            <th>アクセス</th>
                            <td colspan="3"><input name="data[Shop][area]" type="text" style="width:450px;" id="ShopArea" placeholder="例）梅田駅より徒歩10分" title="例）梅田駅より徒歩10分" /></td>
                        </tr>
                        <tr>
                            <th>営業時間</th>
                            <td>
                                <?php echo $form->select('System.hour_open_id', $m_hour_opens,null,array('empty'=>'選んで下さい')); ?>
				～
				<?php echo $form->select('System.hour_close_id', $m_hour_closes,null,array('empty'=>'選んで下さい')); ?>
				<?php echo $form->error('System.hour_open_id'); ?>
				<?php echo $form->error('System.hour_close_id'); ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <th>最低料金</th>
                            <td>
				<?php echo $form->text('System.price_cost',array('size' => 35)); ?>円
				<?php echo $form->error('System.price_minutes'); ?>
				<?php echo $form->error('System.price_cost'); ?>
                                <span class="small spot8">半角数字のみ</span>
                            </td>
                        </tr>
                        <tr>
                            <th>クレジット<br />カード</th>
                            <td colspan="3">                            
				<?php echo $form->checkbox('System.credit_unavailable', array('onClick' => 'DeleteCredit();')); ?><label>利用不可</label><br />
				<?php echo $form->checkbox('System.credit_visa', array('onClick' => 'DeleteUnavailable(this);')); ?><label>VISA</label>
				<?php echo $form->checkbox('System.credit_master', array('onClick' => 'DeleteUnavailable(this);')); ?><label>MASTER</label>
				<?php echo $form->checkbox('System.credit_jcb', array('onClick' => 'DeleteUnavailable(this);')); ?><label>JCB</label>
				<?php echo $form->checkbox('System.credit_americanexpress', array('onClick' => 'DeleteUnavailable(this);')); ?><label>American Express</label>
				<?php echo $form->checkbox('System.credit_diners', array('onClick' => 'DeleteUnavailable(this);')); ?><label>Diners</label><br />
				<?php echo $form->checkbox('System.credit_other'); ?><label>その他</label><?php echo $form->text('System.credit_other_comment',array('size' => 40)); ?>
				<?php echo $form->error('System.credit_unavailable'); ?>
				<?php echo $form->error('System.credit_other_comment'); ?>
				<script language="JavaScript">
				function DeleteCredit() {
				    var input = document.getElementById("SystemCreditUnavailable");
				    if(input.checked){
				    	document.getElementById("SystemCreditVisa").checked = false;
				    	document.getElementById("SystemCreditMaster").checked = false;
				    	document.getElementById("SystemCreditJcb").checked = false;
				    	document.getElementById("SystemCreditAmericanexpress").checked = false;
				    	document.getElementById("SystemCreditDiners").checked = false;
				    	document.getElementById("SystemCreditOther").checked = false;
				    	document.getElementById("SystemCreditOtherComment").value = '';
				    }
				}
				function DeleteUnavailable(holiday) {
				    if(holiday.checked){
				    	document.getElementById("SystemCreditUnavailable").checked = false;
				    }
				}
				</script>
                                <br /><span class="small spot8">半角200文字/全角100文字以内</span>
                            </td>
                        </tr>
                        <tr>
                            <th>コメント1<br />タイトル</th>
                            <td colspan="3">
 				<?php echo $form->text('System.comment1_title', array('size' => 40)); ?>
				<?php echo $form->error('System.comment1_title'); ?>
                            </td>
                        <tr>
                            <th>コメント1</th>
                            <td colspan="3">
				<?php echo $form->textarea('System.comment1_content', array('cols' => 40, 'rows' => 8)); ?>
				<?php echo $form->error('System.comment1_content'); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>コメント2<br />タイトル</th>
                            <td colspan="3">
				<?php echo $form->text('System.comment2_title', array('size' => 40)); ?>
				<?php echo $form->error('System.comment2_title'); ?>
                            </td>
                        <tr>
                        <tr>
                            <th>コメント2</th>
                            <td colspan="3">
				<?php echo $form->textarea('System.comment2_content', array('cols' => 40, 'rows' => 8)); ?>
				<?php echo $form->error('System.comment2_content'); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>コメント3<br />タイトル</th>
                            <td colspan="3">
				<?php echo $form->text('System.comment3_title', array('size' => 40)); ?>
				<?php echo $form->error('System.comment3_title'); ?>
                            </td>
                        <tr>
                        <tr>
                            <th>コメント3</th>
                            <td colspan="3">
				<?php echo $form->textarea('System.comment3_content', array('cols' => 40, 'rows' => 8)); ?>
				<?php echo $form->error('System.comment3_content'); ?>
                            </td>
                        </tr>                       
                    </table>
        
<div align="center" style="margin-top:40px;">
<?php //start---2013/3/7 障害No.1-2-0002修正 ?>
<?php //echo $form->submit('登録する',array('class' => 'btn_navi'));?>
<?php echo $form->submit('保存する',array('class' => 'btn_navi'));?>
<?php //end---2013/3/7 障害No.1-2-0002修正 ?>
</div>
                    
<?php echo $form->end(); ?>

				<div style="margin-bottom:20px"><br /></div>
                        
        	</div>
                
        </div>
            
    </div>
</section>

