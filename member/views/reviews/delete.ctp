
<h2 class="shop_h2">口コミ管理</h2>

<h3 class="rank_h2"><?php echo $data['Shop']['name']?>・<?php echo $data['Girl']['name']?>さんの口コミ削除</h3>


<?php echo $form->create(null,array('type'=>'post','action'=>"")); ?>
<?php echo $form->hidden('Review.id'); ?>

<div class="kuchikomi_preview">
	<div class="kuchikomi_preview_wrap">
    	<div class="kuchikomi_preview_wrap_left">
    		<?php //start---2013/4/25 障害No.4-0001修正 ?>
			<?php echo $imgCommon->get_girl_with_time($data, 'm', 1)?>
			<?php /*
			<?php if(file_exists(APP_IMG_PATH . 'girl' . DS . $data['Girl']['id'] . DS . $data['Girl']['image_1_m']) && !empty($data['Girl']['image_1_m'])){ ?>
			<img src="<?php echo APP_IMG_URL . 'girl/' . $data['Girl']['id'] . '/' . $data['Girl']['image_1_m'] ?>?<?php echo time();?>" width="<?php echo APP_IMG_GIRL_M_WIDTH ?>"
				height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
			<?php }else{ ?>
			<img src="<?php echo APP_IMG_URL . 'common/girl_m.jpg' ?>" width="<?php echo APP_IMG_GIRL_M_WIDTH ?>" height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
			<?php } ?>
			*/ ?>
			<?php //end---2013/4/25 障害No.4-0001修正 ?>
        </div>
        <div class="kuchikomi_preview_wrap_right">
        	<h3 class="kuchikomi_preview_wrap_right_top">
            <?php echo $data['Shop']['name'] ?>
            </h3>
        	<div class="kuchikomi_preview_wrap_right_middle">
            	<span class="spot2"><?php echo $data['Girl']['name'] ?></span>
            	<?php echo $data['Girl']['age'] ?>歳 &nbsp; 見た目年齢:<?php echo $data['MReviewsAge']['name'] ?>
				<span class="profile_preview_right"><?php echo date('Y年n月', strtotime($data['Review']['created'])) ?></span>

            </div>
        	<div class="kuchikomi_preview_wrap_right_middle">
                評価
               	<?php for($i = 0; $i < 5; $i++){?><img src="/images/star_<?php echo ($i+1 <= round($data[0]['girl_avg']))?'on':'off';?>.png" class="kuchikomi_heart" /><?php }?>
        	<span class="spot4 score"><?php echo round($data[0]['girl_avg'],2)?></span>
               <span class="preview_right">[第一印象:<?php echo round($data['Review']['score_girl_first_impression'],1)?>　言葉遣い:<?php echo round($data['Review']['score_girl_word'],1)?>　サービス:<?php echo round($data['Review']['score_girl_service'],1)?>　感度:<?php echo round($data['Review']['score_girl_sensitivity'],1)?>　スタイル:<?php echo round($data['Review']['score_girl_style'],1)?>　あえぎ声:<?php echo round($data['Review']['score_girl_voice'],1)?>]</span>
            </div>
            <div class="kuchikomi_preview_wrap_right_bottom">
				<?php if($data['Review']['comment_deleted'] != 1){?>
				<?php echo nl2br($data['Review']['comment']);?>
				<?php }else{?>
				<center><span  class="spot2">削除依頼により削除されました。</span></center>
				<?php }?>
            </div>
        </div>
    </div>
	    <br class="clear" />
</div>


<div align="center" style="margin-top:40px;">
<?php
$msg = __('口コミを削除しますがよろしいですか？削除した口コミは元に戻りません。', true);
echo $form->submit(__('削除', true), array('name'=>'delete', 'onClick'=>"return confirm('$msg')",'class' => 'btn_navi'));
echo $form->end();
?>
</div>

<?php //echo var_dump($data)?>
