
<h2 class="shop_h2">口コミ管理</h2>

<div class="shop_review_preview">
	<div class="shop_review_preview_wrap">
    	<div class="shop_review_wrap_left">
			<?php //start---2013/4/25 障害No.4-0001修正 ?>
			<?php echo $imgCommon->get_girl_with_time($data, 'm', 1)?>
			<?php /*
			<?php if(file_exists(APP_IMG_PATH . 'girl' . DS . $data['Girl']['id'] . DS . $data['Girl']['image_1_m']) && !empty($data['Girl']['image_1_m'])){ ?>
			<img src="<?php echo APP_IMG_URL . 'girl/' . $data['Girl']['id'] . '/' . $data['Girl']['image_1_m'] ?>?<?php echo time();?>" width="<?php echo APP_IMG_GIRL_M_WIDTH ?>"
				height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
			<?php }else{ ?>
			<img src="<?php echo APP_IMG_URL . 'common/girl_m.jpg' ?>" width="<?php echo APP_IMG_GIRL_M_WIDTH ?>" height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
			<?php } ?>


			*/ ?>
			<?php //end---2013/4/25 障害No.4-0001修正 ?>
        </div>
        <div class="shop_review_wrap_right">
        	<h3 class="shop_review_wrap_right_top">
            <?php echo $data['Shop']['name'] ?>
            </h3>
        	<div class="shop_review_wrap_right_middle">
            	<span class="spot2"><?php echo $data['Girl']['name'] ?></span>
            	<?php echo $data['Girl']['age'] ?>歳 &nbsp; 見た目年齢：<?php echo $data['MReviewsAge']['name'] ?>
            	<span class="preview_right"><?php echo date('Y年n月', strtotime($data['Review']['created'])) ?></span>
            </div>
        	<div class="shop_review_wrap_right_middle">
                評価
               	<?php for($i = 0; $i < 5; $i++){?><img src="/images/star_<?php echo ($i+1 <= round($data[0]['girl_avg']))?'on':'off';?>.png" class="kuchikomi_heart" /><?php }?>
        	<span class="spot4 score"><?php echo round($data[0]['girl_avg'],2)?></span>
               <span class="preview_right">[第一印象:<?php echo round($data['Review']['score_girl_first_impression'],1)?>　言葉遣い:<?php echo round($data['Review']['score_girl_word'],1)?>　サービス:<?php echo round($data['Review']['score_girl_service'],1)?>　感度:<?php echo round($data['Review']['score_girl_sensitivity'],1)?>　スタイル:<?php echo round($data['Review']['score_girl_style'],1)?>　あえぎ声:<?php echo round($data['Review']['score_girl_voice'],1)?>]</span>
            </div>
            <div class="shop_review_wrap_right_bottom">
				<?php if($data['Review']['comment_deleted'] != 1){?>
				<?php echo nl2br($data['Review']['comment']);?>
				<?php }else{?>
				<center><span  class="spot2">削除依頼により削除されました。</span></center>
				<?php }?>
            </div>
        </div>
    </div>
    <br class="clear" />
</div>

<br />

<?php if(!empty($data['Review']['reviewer_id'])){?>
<h3 class="rank_h2">レビュアーにメッセージを送る</h3>

	<?php echo $form->create(null,array('type'=>'post','action'=>"")); ?>
	<?php echo $form->hidden('Message.review_id', array('value' => $data['Review']['id'])); ?>
	<?php echo $form->hidden('Message.reviewer_id', array('value' => $data['Reviewer']['id'])); ?>
	<table class="profileedit_table">
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />店名</td>
            <td class="profileedit_td2"><?php echo $data['Shop']['name'] ?></td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />レビュアー</td>
            <td class="profileedit_td2"><?php echo $data['Reviewer']['handle'] ?></td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />タイトル</td>
            <td class="profileedit_td2">
				<?php echo $form->text('Message.title'); ?>
				<?php echo $form->error('Message.title'); ?>
            </td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />メッセージ</td>
            <td class="profileedit_td2">
				<?php echo $form->textarea('Message.comment', array('cols' => 52, 'rows' => 8)); ?>
				<?php echo $form->error('Message.comment'); ?>
            </td>
        </tr>
    </table>

<div align="center" style="margin:20px;">
<?php echo $form->submit('送信する',array('class' => 'btn_navi'));?>
</div>
<?php echo $form->end(); ?>

<h3 class="rank_h2">送信メッセージ一覧</h3>

<?php foreach($messages as $key => $record){?>
<div class="message_shop_more">
	<div class="message_shop_wrap">
    	<div class="message_shop_wrap_top">
			「<?php echo $record['Message']['title'] ?>」<span class="small" style="float:right"><?php echo $data['Shop']['name'] ?></span>
        </div>
        <div style="text-align:right">送信：<?php echo date('Y/m/d H:i', strtotime($record['Message']['created'])) ?></div>
        <div class="message_shop_wrap_bottom">
			<?php echo nl2br($record['Message']['comment']) ?>
        </div>
    </div>
</div>
<br />
<?php }?>

<?php }?>

<?php //echo var_dump($data)?>



