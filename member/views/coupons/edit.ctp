<section id="middle" class="two">
    <div class="title_bar">割引クーポン編集</div>
    <div id="timeLineArea" class="inner">
        <div class="new_reviewArea clearfix">
            <div class="shopTable">
                <?php echo $form->create(null,array('type'=>'post','enctype' => 'multipart/form-data','action'=>"")); ?>
                    <?php echo $form->hidden('Coupon.id'); ?>

                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th>対象</th>
                            <td colspan="3">
                                <?php echo $form->select('Coupon.target', $m_target,null,array('empty' => '-')); ?><br />
                                <span class="small spot8">割引クーポンの対象となるお客様をお選び下さい。</span>
                                <?php echo $form->error('Coupon.target');?>
                            </td>
                        </tr>
                        <tr>
                            <th>割引条件</th>
                            <td colspan="3">
                                <?php echo $form->text('Coupon.condition',array('style' => 'width:450px;', 'placeholder' => '例：「デリヘル口コミランキングを見た、で」', 'title' => '例：「デリヘル口コミランキングを見た、で」')); ?><br />
                                <!-- <span class="small spot8">全角文字で40文字まで。</span> -->
                                <?php echo $form->error('Coupon.condition');?>
                            </td>
                        </tr>
                        <tr>
                            <th>割引内容</th>
                            <td colspan="3">
                                <?php echo $form->text('Coupon.content',array('style' => 'width:210px;', 'placeholder' => '例：「¥2,000OFF」「指名料無料」', 'title' => '例：「¥2,000OFF」「指名料無料」')); ?><br />
                                <!-- <span class="small spot8">全角文字で8文字まで。</span> -->
                                <?php echo $form->error('Coupon.content');?>
                            </td>
                        </tr>
                        <tr>
                            <th>有効期限</th>
                            <td colspan="3">
                                <label><?php echo $form->checkbox('Coupon.expire_unlimited',array('value' => '1', 'onClick' => 'checkUnlimited();')); ?> 期限なし</label><br />                            

                                <?php echo $form->select('Coupon.expire_date_year', $m_year,null,array('empty' => '-')); ?> 年 
                                <?php echo $form->select('Coupon.expire_date_month', $m_month,null,array('empty' => '-')); ?> 月 
                                <?php echo $form->select('Coupon.expire_date_day', $m_day,null,array('empty' => '-')); ?> 日まで<br />
                                <span class="small spot8">期限の無い場合は、「期限なし」にチェックを入れて下さい</span>
                                <?php echo $form->error('Coupon.expire_date');?>
                            </td>
                        </tr>
                    </table>
                
                    <div align="center">
                        <div class="submit">
                            <?php echo $form->submit('編集する',array('class' => 'btn_navi'));?>
                        </div>
                    </div>
    
                <?php echo $form->end(); ?>
            </div>

            <div style="margin-bottom:20px"><br /></div>

        </div>
    </div>
</section>

<script type="text/javascript">
<!--
function checkUnlimited() {

    var input = document.getElementById("CouponExpireUnlimited").checked;

    document.getElementById("CouponExpireDateYear").disabled = input;
    document.getElementById("CouponExpireDateMonth").disabled = input;
    document.getElementById("CouponExpireDateDay").disabled = input;
    
}

// -->
</script>