<section id="middle" class="two">
    <div class="title_bar">割引クーポン管理</div>
    <div id="timeLineArea" class="inner">
        <div class="new_reviewArea clearfix">
        	<h3>割引クーポン入力</h3>
            <div class="shopTable">
                <?php echo $form->create(null,array('type'=>'post','enctype' => 'multipart/form-data','action'=>"")); ?>
                    <?php echo $form->hidden('Coupon.id'); ?>

                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th>対象</th>
                            <td colspan="3">
                                <?php echo $form->select('Coupon.target', $m_target,null,array('empty' => '-')); ?><br />
                                <span class="small spot8">割引クーポンの対象となるお客様をお選び下さい。</span>
                                <?php echo $form->error('Coupon.target');?>
                            </td>
                        </tr>
                        <tr>
                            <th>割引条件</th>
                            <td colspan="3">
                                <?php echo $form->text('Coupon.condition',array('style' => 'width:450px;', 'placeholder' => '例：「デリヘル口コミランキングを見た、で」', 'title' => '例：「デリヘル口コミランキングを見た、で」')); ?><br />
                                <!-- <span class="small spot8">全半角文字で40文字まで。</span> -->
                                <?php echo $form->error('Coupon.condition');?>
                            </td>
                        </tr>
                        <tr>
                            <th>割引内容</th>
                            <td colspan="3">
                                <?php echo $form->text('Coupon.content',array('style' => 'width:210px;', 'placeholder' => '例：「¥2,000OFF」「指名料無料」', 'title' => '例：「¥2,000OFF」「指名料無料」')); ?><br />
                                <!-- <span class="small spot8">全半角文字で8文字まで。</span> -->
                                <?php echo $form->error('Coupon.content');?>
                            </td>
                        </tr>
                        <tr>
                            <th>有効期限</th>
                            <td colspan="3">
                                <label><?php echo $form->checkbox('Coupon.expire_unlimited',array('value' => '1', 'onClick' => 'checkUnlimited();')); ?> 期限なし</label><br />                            

                                <?php echo $form->select('Coupon.expire_date_year', $m_year,null,array('empty' => '-')); ?> 年 
                                <?php echo $form->select('Coupon.expire_date_month', $m_month,null,array('empty' => '-')); ?> 月 
                                <?php echo $form->select('Coupon.expire_date_day', $m_day,null,array('empty' => '-')); ?> 日まで<br />
                                <span class="small spot8">期限の無い場合は、「期限なし」にチェックを入れて下さい</span>
                                <?php echo $form->error('Coupon.expire_date');?>
                            </td>
                        </tr>
                    </table>
                
                    <div align="center">
                        <div class="submit">
                            <?php echo $form->submit('追加する',array('class' => 'btn_navi'));?>
                        </div>
                    </div>
    
                <?php echo $form->end(); ?>
                
                <br />
    
            	<h3>割引クーポン編集</h3>
            
           		<div class="couponArea">
            
<?php
if(!empty($list)) {
    foreach($list as $key => $record) {
?>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th class="type<?php echo $record['Coupon']['target']; ?>"><?php echo $textCommon->get_vertical_text($m_target[$record['Coupon']['target']]); ?></th>
                            <td>
                                <div>
                                    <p><?php echo $record['Coupon']['condition']; ?></p>
                                    <ul>
                                        <li>提示条件：<span><? /* php echo $record['Coupon']['condition']; */ ?>予約時</span></li>
                                        <li>利用条件：<span><?php echo $m_target_caption[$record['Coupon']['target']]; ?></span></li>
                                        <li>有効期限：<span><?php if($record['Coupon']['expire_unlimited'] == 1) { ?>期限なし<?php } else { echo date('Y年m月d日まで', strtotime($record['Coupon']['expire_date'])); } ?></span></li>
                                    </ul>
                                </div>
                            </td>
                            <td class="price"><?php echo $record['Coupon']['content']; ?></td>
                        </tr>
                    </table>

                    <div align="center">
                        <div class="submit">
                            <a href="<?php echo $linkCommon->get_coupons_edit($record) ?>" class="member_girls_button">編集</a>
                            <a href="<?php echo $linkCommon->get_coupons_delete($record) ?>" class="member_girls_button" onClick="return delete_button();">削除</a>
                        </div>
                    </div>
                    <br />
<?php
    }
} else {
?>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td>
                                <div>
                                    <p>発行されたクーポンはありません。</p>
                                </div>
                            </td>
                        </tr>
                    </table>
<?php
}
?>
            	</div>
        	</div>

			<div style="margin-bottom:20px"><br /></div>

        </div>
    </div>
</section>

<script type="text/javascript">
<!--
function checkUnlimited() {

    var input = document.getElementById("CouponExpireUnlimited").checked;

    document.getElementById("CouponExpireDateYear").disabled = input;
    document.getElementById("CouponExpireDateMonth").disabled = input;
    document.getElementById("CouponExpireDateDay").disabled = input;
    
}

function delete_button(){
    if(window.confirm("選択されたクーポンが削除され、復元出来ません。\n本当に削除しますか？")){
        return true;
    }
    return false;
}

// -->
</script>