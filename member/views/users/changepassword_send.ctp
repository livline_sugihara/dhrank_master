<div class="body">

    <div class="title title2">
        <h2>SHOP LOGIN <span class="small">店舗会員・ログインIDとパスワード変更</span></h2>
    </div>


<div class="login_reviewer">

    <p class="text">
		登録されましたメールアドレスをご入力ください。<br />
		ログインIDとパスワード再発行のためのURLをお送りいたします。
	</p>

    <div class="formArea">
		<?php echo $form->create(null,array('type'=>'post','action'=>''));?>

		<table cellspacing="0" cellpadding="0" border="0">
    	<tr>
        	<th>メールアドレス</td>
            <td>
            	<?php echo $form->text('Changepassword.email'); ?>
            	<?php echo $form->error('Changepassword.email'); ?>
            </td>
        </tr>
	    </table>

	    <div class="align_c">
	    	<?php echo $form->submit('送信', array('class' => 'redBtn','value'=>'送信')); ?>
	    </div>
		
		<?php echo $form->end(); ?>
	</div>
	<!-- /formArea -->

</div>
<!-- login_reviewer -->