<div class="body">

    <div class="title title2">
        <h2>SHOP LOGIN <span class="small">店舗会員・パスワード変更</span></h2>
    </div>


<?php echo $form->create(null,array('type'=>'post','action'=>'')); ?>
<?php echo $form->hidden('User.id'); ?>
<?php echo $form->hidden('Changepassword.id'); ?>
    <div class="formArea">

	<table>
    	<tr>
        	<th>新パスワード</th>
            <td><?php echo $form->password('User.new_password'); ?><?php echo $form->error('User.new_password'); ?></td>
        </tr>
    	<tr>
        	<th>新パスワード(確認)</th>
            <td><?php echo $form->password('User.new_password_confirm'); ?><?php echo $form->error('User.new_password_confirm'); ?></td>
        </tr>
    </table>

</div>


<div align="center" style="margin-top:40px;">
<?php echo $form->submit('変更', array('class' => 'btn_back','value'=>'変更')); ?>
</div>
<?php echo $form->end(); ?>

	</div>
	<!-- /formArea -->

</div>
<!-- login_reviewer -->