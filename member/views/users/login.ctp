<div class="body">

    <div class="title title2">
        <h2>SHOP LOGIN <span class="small">店舗会員様ログイン</span></h2>
    </div>

    <p class="text">
    ログインIDとパスワードを入力して、ログインして下さい。 
    </p>

    <div class="formArea">
        <?php echo $form->create("User", array('action' => 'login')); ?>
        <?php if ($session->check('Message.auth')){ ?> 
        <?php echo $session->flash('auth'); ?>
        <?php }?>
        
            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <th>ログインID</th>
                    <td>
                        <?php echo $form->text('username'); ?>
                    </td>
                    
                </tr>
                <tr>
                    <th>パスワード</th>
                    <td>
                        <?php echo $form->password('password'); ?><?php echo $form->error('password'); ?>
                    </td>
                </tr>
            </table>
        
            <?php echo $form->checkbox('remember_me')?>
            <label for="login-check" style="font-size:12px;">パスワードを記憶する</label>

            <div style="text-align:right">
            <img src="/img/footer_nav_square.png" style="margin-right:3px" /><a href="<?php echo $linkCommon->get_changepassword_send();?>">ID・パスワードを忘れた方はこちら</a>
            </div>

            <div class="align_c">
                <?php echo $form->submit('ログイン', array('class' => 'redBtn','value'=>'ログイン')); ?>
            </div>
            
        <?php echo $form->end(); ?>
        
    </div>
    <!-- /rankingArea-->
</div>