<!-- middle -->
<section id="middle" class="two">

	<?php echo $this->element('common/girls_submenu'); ?>

	<div class="title_bar">女の子一覧</div>
    <div id="timeLineArea" class="inner">

<?php // print_r($list); ?>

	<?php echo $this->Form->create('Girl', array('action' => 'picall', 'type' => 'file')); ?>

        <div id="drop_girl" class="member_girls clearfix sortable">

<?php

	foreach($rec_list as $key => $record) {

		$href = $parent_user['LargeArea']['url'] . $record['Girl']['user_id'] . '/girls/' . $record['Girl']['id'];

		$name = $record['Girl']['name'] . '(' . $record['Girl']['age'] . ')';

?>

            <!-- <div id="drag_rec_girl" draggable='true' ondragstart="f_dragstart(event)" ondragover="f_dragover(event)" ondrop="f_drop(event)"> -->
			<div>

				<span class="tab_bar">_____</span><span class="check_box"><?php echo $this->Form->checkbox('', array('value' => $record['Girl']['id'], 'div' => false, 'class' => 'girl_check')) ?></span>

                <p class="img"><?php echo $imgCommon->get_girl_with_time($record, 'm', 1)?></p>

                <p class="name"><a href="<?php echo $href ?>" target="_blank"><?php echo $name ?></a></p>

                <p class="recommend_on">激押し売れっ子</p>

                <p>

                	<a href="<?php echo $linkCommon->get_girls_edit($record) ?>" class="member_girls_button">編集</a>

                	<a href="<?php echo $linkCommon->get_girls_delete($record) ?>" class="member_girls_button" onClick="return delete_button();">削除</a>

					<?php //echo $this->Form->file('image_1_m][]'); ?>
					<input type="hidden" name="g_id[]" value="<?php echo $record['Girl']['id']; ?>">
					<!--<input name="image_1_m[]" type="file" multiple="multiple" class="test1"/> -->

				</p>

            </div>


<?php } ?>


<?php
	foreach($list as $key => $record) {
		$href = $parent_user['LargeArea']['url'] . $record['Girl']['user_id'] . '/girls/' . $record['Girl']['id'];
		$name = $record['Girl']['name'] . '(' . $record['Girl']['age'] . ')';
?>
            <div class="drag_girl">
				<span class="tab_bar">_____</span><span class="check_box"><?php echo $this->Form->checkbox('id][]', array('value' => $record['Girl']['id'], 'div' => false, 'class' => 'girl_check')) ?></span>
                <p class="img"><?php echo $imgCommon->get_girl_with_time($record, 'm', 1)?></p>
                <p class="name"><a href="<?php echo $href ?>" target="_blank"><?php echo $name ?></a></p>
                <p class="recommend_on">&nbsp;</p>
                <p>
                	<a href="<?php echo $linkCommon->get_girls_edit($record) ?>" class="member_girls_button">編集</a>
                	<a href="<?php echo $linkCommon->get_girls_delete($record) ?>" class="member_girls_button" onClick="return delete_button();">削除</a></p>

					<input type="hidden" name="g_id[]" value="<?php echo$record['Girl']['id']; ?>">
					<!-- <input name="image_1_m[]" type="file" multiple="multiple" class="test1"/> -->

            </div>
<?php } ?>
        </div>

<?php echo $this->Form->end('一括写真変更'); ?>

		<div style="margin-bottom:20px"><br /></div>

	</div>
</section>
<!-- /middle -->

<script type="text/javascript">
<!--

function delete_button(){
	if(window.confirm("女の子に関する全ての情報が削除され、復元出来ません。\n本当に削除しますか？")){
		return true;
	}
	return false;
}

// -->
</script>


<!-- 以下オリジナル
<h2 class="shop_h2">女の子管理</h2>

<div class="tabs box">
	<ul>
		<li><a href="<?php echo $linkCommon->get_girls_add() ?>"><span style="color: #fff;">女の子登録</span></a></li>
		<li><a href="<?php echo $linkCommon->get_girls_index() ?>"><span style="color: #fff;">女の子一覧</span></a></li>
		<li><a href="<?php echo $linkCommon->get_girls_orders_index() ?>"><span style="color: #fff;">表示順位編集</span></a></li>
	</ul>
</div> <!-- /tabs ==>

<h3 class="rank_h2">女の子一覧</h3>

	<table class="girls_table">
    	<tr>
			<?php foreach($list as $key => $record){?>
			<?php if(($key % 4 == 3) && ($key + 1 != count($list))){ //4つ目のレコードかつ最終レコードでない?>
			<td class="girls_table_td1">
			<?php }else{?>
			<td class="girls_table_td">
			<?php }?>
        		<?php //start---2013/4/25 障害No.4-0001修正 ?>
        		<?php echo $imgCommon->get_girl_with_time($record, 'm', 1)?>
        		<?php /*
				<?php if(file_exists(APP_IMG_PATH . 'girl' . DS . $record['Girl']['id'] . DS . $record['Girl']['image_1_m']) && !empty($record['Girl']['image_1_m'])){ ?>
				<img src="<?php echo APP_IMG_URL . 'girl/' . $record['Girl']['id'] . '/' . $record['Girl']['image_1_m'] ?>?<?php echo time();?>" width="<?php echo APP_IMG_GIRL_M_WIDTH ?>"
					height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
				<?php }else{ ?>
				<img src="<?php echo APP_IMG_URL . 'common/girl_m.jpg' ?>" width="<?php echo APP_IMG_GIRL_M_WIDTH ?>" height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
				<?php } ?>
				*/ ?>
				<?php //end---2013/4/25 障害No.4-0001修正 ?>
				<br />
				<a href="<?php echo $parent_user['LargeArea']['url'] . $record['Girl']['user_id'] . '/girls/' . $record['Girl']['id']?>" target="_blank"><?php echo $record['Girl']['name'] . '(' . $record['Girl']['age'] . ')' ?></a><br />
				口コミ <?php echo $record[0]['count']?>件<br />
				<a href="<?php echo $linkCommon->get_girls_edit($record) ?>" class="btn_more">編集</a><a href="<?php echo $linkCommon->get_girls_delete($record) ?>" class="btn_more">削除</a>
			</td>
			<?php if(($key % 4 == 3) && ($key + 1 != count($list))){ //4つ目のレコードかつ最終レコードでない?>
		</tr>
		<tr>
			<?php }?>
			<?php } ?>
        </tr>
    </table>
    -->
