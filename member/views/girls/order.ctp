<section id="middle" class="two">

    <?php echo $this->element('common/girls_submenu'); ?>

    <div class="title_bar">女の子表示順位編集</div> 
    <div id="timeLineArea" class="inner">
        <div class="shopTable">
			<?php echo $form->create(null, array('type'=>'post','enctype' => 'multipart/form-data', 'action' => "order"));?>

	                <table cellspacing="0" cellpadding="0" border="0">
<?php
foreach($list as $key => $record) {
	$name = $record['Girl']['name'] . '(' . $record['Girl']['age'] . ')';
?>
                    <tr>
                        <th><?php echo $name; ?></th>
                        <td colspan="3">
                        	<?php echo $form->select('Girl.' . $key . '.show_order', $m_show_order, $record['Girl']['show_order'], array('empty'=>'－')); ?> 番目に表示
                        	<?php echo $form->hidden('Girl.' . $key . '.id', array('value' => $record['Girl']['id']));?>
                            <?php echo $form->error('Girl.' . $key . '.show_order'); ?>
                        </td>
                    </tr>
<?php } ?>
                </table>

                <div align="center">
					<?php echo $form->submit('表示順位変更',array('class' => 'btn_navi'));?>
                </div>

            <?php echo $form->end(); ?>
                    
            <div style="margin-bottom:20px"><br /></div>
        </div>
    </div>
</section>

<?php // echo var_dump($list) ?>