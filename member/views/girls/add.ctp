<section id="middle" class="two">

    <?php echo $this->element('common/girls_submenu'); ?>

    <div class="title_bar">女の子登録</div> 
    <div id="timeLineArea" class="inner">
        <div class="shopTable">
            <h3>女の子情報</h3>

            <?php echo $form->create(null, array('type'=>'post','enctype' => 'multipart/form-data', 'action' => "add"));?>
                <?php echo $form->hidden('Girl.user_id');?>
        
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <th>名前</th>
                        <td colspan="3">
                            <?php echo $form->text('Girl.name',array('style' => 'width:450px;')); ?>
                            <?php echo $form->error('Girl.name'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>年齢</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.age',$m_age,null,array('empty'=>array(0 => '－'))); ?>歳
                            <?php echo $form->error('Girl.age'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>バスト</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.bust',$m_bust,null,array('empty'=>array(0 => '－'))); ?>cm
                            <?php echo $form->error('Girl.bust'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>カップ</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.girls_cup_id',$m_girls_cups,null,array('empty'=>array(0 => '－'))); ?>
                            <?php echo $form->error('Girl.girls_cup_id'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ウエスト</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.waist', $m_waist,null,array('empty'=>array(0 => '－'))); ?>cm
                            <?php echo $form->error('Girl.waist'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ヒップ</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.hip', $m_hip,null,array('empty'=>array(0 => '－'))); ?>cm
                            <?php echo $form->error('Girl.hip'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>身長</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.body_height', $m_body_height,null,array('empty'=>array(0 => '－'))); ?>cm
                            <?php echo $form->error('Girl.body_height'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>女の子コメント</th>
                        <td colspan="3">
                            <?php echo $form->textarea('Girl.self_comment',array('style' => 'width:450px;height:90px;')); ?>
                            <?php echo $form->error('Girl.self_comment'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>店長コメント</th>
                        <td colspan="3">
                            <?php echo $form->textarea('Girl.owner_comment',array('style' => 'width:450px;height:90px;')); ?>
                            <?php echo $form->error('Girl.owner_comment'); ?>
                         </td>
                    </tr>
                </table>

<?php if($parent_user['UsersAuthority']['is_recommend_girl'] == 1) { ?>
                <h3>激押し売れっ子</h3>
                <p class="small spot8">6人まで「激押し売れっ子」として登録する事が出来ます。</p>
        
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <th>激押し売れっ子として表示する</th>
                        <td colspan="3">
                            <?php echo $form->checkbox('Girl.recommend_check',array('value' => '1')); ?> 登録する
                            <?php echo $form->error('Girl.recommend_check'); ?>
                         </td>
                    </tr>
                    <tr>
                        <th>おすすめ内容</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.recommend_type_id', $m_girls_recommend_types,null,array('empty'=>array(0 => '－'))); ?> がおすすめ
                            <?php echo $form->error('Girl.recommend_type_id'); ?>
                         </td>
                    </tr>
                </table>
<?php } ?>
    
                <h3>女の子画像アップロード</h3>
                <p class="small spot8">「横幅300px」、「高さ400px」の画像を推奨しております。<br />拡張子「jpg」「gif」形式の画像のみ、ファイルサイズ「3MB」まで。 </p>
        
                <table>
                    <tr>
                        <th>女の子画像1枚目</th>
                        <td>
                            <?php echo $form->file('Girl.image2_1');?>
                            <?php echo $form->error('Girl.image2_1'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>女の子画像2枚目</th>
                        <td>
                            <?php echo $form->file('Girl.image2_2');?>
                            <?php echo $form->error('Girl.image2_2'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>女の子画像3枚目</th>
                        <td>
                            <?php echo $form->file('Girl.image2_3');?>
                            <?php echo $form->error('Girl.image2_3'); ?>
                        </td>
                    </tr>
                </table>
        
                <div align="center" style="margin-top:40px;">
                    <div class="submit">
                        <?php echo $form->submit('登録する',array('class' => 'btn_navi'));?>
                    </div>
                </div>
                
            <?php echo $form->end(); ?>
    
            <div style="margin-bottom:20px"></div>
        </div>
    </div>
</section>
