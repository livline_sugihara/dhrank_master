<section id="middle" class="two">

    <?php echo $this->element('common/girls_submenu'); ?>

    <div class="title_bar">女の子編集</div>
    <div id="timeLineArea" class="inner">
        <div class="shopTable">
            <h3>女の子情報</h3>

            <?php echo $form->create(null, array('type'=>'post','enctype' => 'multipart/form-data', 'action' => "edit"));?>
                <?php echo $form->hidden('Girl.id');?>
                <?php echo $form->hidden('Girl.user_id');?>

                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <th>名前</th>
                        <td colspan="3">
                            <?php echo $form->text('Girl.name',array('style' => 'width:450px;')); ?>
                            <?php echo $form->error('Girl.name'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>年齢</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.age',$m_age,null,array('empty'=>'－')); ?>歳
                            <?php echo $form->error('Girl.age'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>身長</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.body_height', $m_body_height,null,array('empty'=>'－')); ?>cm
                            <?php echo $form->error('Girl.body_height'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>バスト</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.bust',$m_bust,null,array('empty'=>'－')); ?>cm
                            <?php echo $form->error('Girl.bust'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>カップ</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.girls_cup_id',$m_girls_cups,null,array('empty'=>'－')); ?>
                            <?php echo $form->error('Girl.girls_cup_id'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ウエスト</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.waist', $m_waist,null,array('empty'=>'－')); ?>cm
                            <?php echo $form->error('Girl.waist'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>ヒップ</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.hip', $m_hip,null,array('empty'=>'－')); ?>cm
                            <?php echo $form->error('Girl.hip'); ?>
                        </td>
                    </tr>

                    <tr>
                        <th>女の子コメント</th>
                        <td colspan="3">
                            <?php echo $form->textarea('Girl.self_comment',array('style' => 'width:450px;height:90px;')); ?>
                            <?php echo $form->error('Girl.self_comment'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>店長コメント</th>
                        <td colspan="3">
                            <?php echo $form->textarea('Girl.owner_comment',array('style' => 'width:450px;height:90px;')); ?>
                            <?php echo $form->error('Girl.owner_comment'); ?>
                         </td>
                    </tr>
                    <tr>
                        <th>キャッチコピー</th>
                        <td colspan="3">
                            <?php echo $form->textarea('Girl.catch_phrase',array('style' => 'width:450px;height:90px;')); ?>
                            <?php echo $form->error('Girl.catch_phrase'); ?>
                            </td>
                            </tr>
                            </table>
<?php if($parent_user['UsersAuthority']['is_recommend_girl'] == 1) { ?>
                <h3>激押し売れっ子</h3>
                <p class="small spot8">6人まで「激押し売れっ子」として登録する事が出来ます。</p>
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <th>激押し売れっ子として表示する</th>
                        <td colspan="3">
                            <?php echo $form->checkbox('Girl.recommend_check',array('value' => '1')); ?> 登録する

                            <?php echo $form->error('Girl.recommend_check'); ?>
                         </td>
                    </tr>
                    <tr>
                        <th>おすすめ内容</th>
                        <td colspan="3">
                            <?php echo $form->select('Girl.recommend_type_id', $m_girls_recommend_types,null,array('empty'=>'－')); ?> がおすすめ
                            <?php echo $form->error('Girl.recommend_type_id'); ?>
                         </td>
                    </tr>
                </table>


<?php } ?>

                <h3>女の子画像アップロード</h3>
                <p class="small spot8">「横幅300px」、「高さ400px」の画像を推奨しております。<br />拡張子「jpg」「gif」形式の画像のみ、ファイルサイズ「3MB」まで。 </p>

                <table>
<?php for($ii = 1; $ii <= 3; $ii++) { ?>
                    <tr>
                        <th>女の子画像<?php echo $ii; ?>枚目</th>
                        <td>
                        	<?php echo $imgCommon->get_girl_with_time($data, 'm', $ii)?><br />
<?php if($imgCommon->is_exist_girl_image($data, 'm', $ii)) { ?>
							<label><?php echo $form->checkbox('Girl.delete_image_' . $ii);?> 削除</label>
<?php } ?>
							<br/><br />
                            <?php echo $form->file('Girl.image2_' . $ii);?><br />
                            <?php echo $form->error('Girl.image2_' . $ii); ?>
                        </td>
                    </tr>
<?php } ?>
                </table>

                <div align="center" style="margin-top:40px;">
                    <div class="submit">
                        <?php echo $form->submit('編集する',array('class' => 'btn_navi'));?>
                    </div>
                </div>

            <?php echo $form->end(); ?>

            <div style="margin-bottom:20px"></div>

        </div>

    </div>
</section>



<!-- 以下オリジナル
	<?php echo $form->create(null, array('type'=>'post','enctype' => 'multipart/form-data', 'action' => "edit"));?>
	<?php echo $form->hidden('Girl.id');?>
	<?php //start---2013/4/25 障害No.4-0001修正 ?>
	<?php echo $form->hidden('User.id');?>
	<?php //end---2013/4/25 障害No.4-0001修正 ?>
	<?php //start---2013/3/5 障害No.2-0012修正 ?>
	<?php echo $form->hidden('Girl.image_1_m'); ?>
	<?php echo $form->hidden('Girl.image_2_m'); ?>
	<?php echo $form->hidden('Girl.image_3_m'); ?>
	<?php echo $form->hidden('Girl.image_4_m'); ?>
	<?php //end---2013/3/5 障害No.2-0012修正 ?>

	<table class="profileedit_table">
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />名前</td>
            <td class="profileedit_td2">
				<?php echo $form->text('Girl.name',array('size' => 60)); ?>
				<?php echo $form->error('Girl.name'); ?>
			</td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />年齢</td>
            <td class="profileedit_td2">
				<?php echo $form->select('Girl.age', $m_age,null,array('empty'=>'－')); ?>歳
				<?php echo $form->error('Girl.age'); ?>
			</td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />バスト</td>
            <td class="profileedit_td2">
				<?php echo $form->select('Girl.bust', $m_bust,null,array('empty'=>'－')); ?>cm
				<?php echo $form->error('Girl.bust'); ?>
			</td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />カップ</td>
            <td class="profileedit_td2">
				<?php echo $form->select('Girl.girls_cup_id', $m_girls_cups,null,array('empty'=>'－')); ?>
				<?php echo $form->error('Girl.girls_cup_id'); ?>
			</td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />ウエスト</td>
            <td class="profileedit_td2">
				<?php echo $form->select('Girl.waist', $m_waist,null,array('empty'=>'－')); ?>cm
				<?php echo $form->error('Girl.waist'); ?>
			</td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />ヒップ</td>
            <td class="profileedit_td2">
				<?php echo $form->select('Girl.hip', $m_hip,null,array('empty'=>'－')); ?>cm
				<?php echo $form->error('Girl.hip'); ?>
			</td>
        </tr>
    	<tr>
        	<td class="profileedit_td1"><img src="/images/icon_table.jpg" />身長</td>
            <td class="profileedit_td2">
				<?php echo $form->select('Girl.body_height', $m_body_height,null,array('empty'=>'－')); ?>cm
				<?php echo $form->error('Girl.body_height'); ?>
			</td>
        </tr>
    </table>

<h3 class="rank_h2">女の子画像のアップロード</h3>

	<table class="girls_table">
    	<tr>
        	<th class="girls_table_th">女の子画像</th>
            <th class="girls_table_th">女の子画像2枚目</th>
        </tr>
    	<tr>
        	<td class="girls_table_td">
        		<?php //start---2013/4/25 障害No.4-0001修正 ?>
        		<?php echo $imgCommon->get_girl_with_time($data, 'm', 1)?>
        		<?php /*
				<?php if(file_exists(APP_IMG_PATH . 'girl' . DS . $data['Girl']['id'] . DS . $data['Girl']['image_1_m']) && !empty($data['Girl']['image_1_m'])){ ?>
				<img src="<?php echo APP_IMG_URL . 'girl/' . $data['Girl']['id'] . '/' . $data['Girl']['image_1_m'] ?>?<?php echo time();?>"
					width="<?php echo APP_IMG_GIRL_M_WIDTH ?>" height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
				<?php }else{ ?>
				<img src="<?php echo APP_IMG_URL . 'common/girl_m.jpg' ?>" width="<?php echo APP_IMG_GIRL_M_WIDTH ?>" height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
				<?php } ?>
				*/ ?>
				<?php //end---2013/4/25 障害No.4-0001修正 ?>
				<br />
				<?php echo $form->file('Girl.image2_1');?><?php echo $form->error('Girl.image2_1'); ?><br />
				<?php echo $form->checkbox('Girl.delete_image_1');?>削除<?php echo $form->error('Girl.delete_image_1');?>
			</td>

            <td class="girls_table_td1">
        		<?php //start---2013/4/25 障害No.4-0001修正 ?>
        		<?php echo $imgCommon->get_girl_with_time($data, 'm', 2)?>
        		<?php /*
				<?php if(file_exists(APP_IMG_PATH . 'girl' . DS . $data['Girl']['id'] . DS . $data['Girl']['image_2_m']) && !empty($data['Girl']['image_2_m'])){ ?>
				<img src="<?php echo APP_IMG_URL . 'girl/' . $data['Girl']['id'] . '/' . $data['Girl']['image_2_m'] ?>?<?php echo time();?>"
					width="<?php echo APP_IMG_GIRL_M_WIDTH ?>" height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
				<?php }else{ ?>
				<img src="<?php echo APP_IMG_URL . 'common/girl_m.jpg' ?>" width="<?php echo APP_IMG_GIRL_M_WIDTH ?>" height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
				<?php } ?>
				*/ ?>
				<?php //end---2013/4/25 障害No.4-0001修正 ?>
				<br />
				<?php echo $form->file('Girl.image2_2');?><?php echo $form->error('Girl.image2_2'); ?><br />
				<?php echo $form->checkbox('Girl.delete_image_2');?>削除<?php echo $form->error('Girl.delete_image_2');?>
			</td>

        </tr>

    	<tr>
            <th class="girls_table_th">女の子画像3枚目</th>
            <th class="girls_table_th1">女の子画像4枚目</th>
        </tr>
    	<tr>
        	<td class="girls_table_td">
        		<?php //start---2013/4/25 障害No.4-0001修正 ?>
        		<?php echo $imgCommon->get_girl_with_time($data, 'm', 3)?>
        		<?php /*
				<?php if(file_exists(APP_IMG_PATH . 'girl' . DS . $data['Girl']['id'] . DS . $data['Girl']['image_3_m']) && !empty($data['Girl']['image_3_m'])){ ?>
				<img src="<?php echo APP_IMG_URL . 'girl/' . $data['Girl']['id'] . '/' . $data['Girl']['image_3_m'] ?>?<?php echo time();?>"
					width="<?php echo APP_IMG_GIRL_M_WIDTH ?>" height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
				<?php }else{ ?>
				<img src="<?php echo APP_IMG_URL . 'common/girl_m.jpg' ?>" width="<?php echo APP_IMG_GIRL_M_WIDTH ?>" height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
				<?php } ?>
				*/ ?>
				<?php //end---2013/4/25 障害No.4-0001修正 ?>
				<br />
				<?php echo $form->file('Girl.image2_3');?><?php echo $form->error('Girl.image2_3'); ?><br />
				<?php echo $form->checkbox('Girl.delete_image_3');?>削除<?php echo $form->error('Girl.delete_image_3');?>
			</td>
            <td class="girls_table_td1">
        		<?php //start---2013/4/25 障害No.4-0001修正 ?>
        		<?php echo $imgCommon->get_girl_with_time($data, 'm', 4)?>
        		<?php /*
				<?php if(file_exists(APP_IMG_PATH . 'girl' . DS . $data['Girl']['id'] . DS . $data['Girl']['image_4_m']) && !empty($data['Girl']['image_4_m'])){ ?>
				<img src="<?php echo APP_IMG_URL . 'girl/' . $data['Girl']['id'] . '/' . $data['Girl']['image_4_m'] ?>?<?php echo time();?>"
					width="<?php echo APP_IMG_GIRL_M_WIDTH ?>" height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
				<?php }else{ ?>
				<img src="<?php echo APP_IMG_URL . 'common/girl_m.jpg' ?>" width="<?php echo APP_IMG_GIRL_M_WIDTH ?>" height="<?php echo APP_IMG_GIRL_M_HEIGHT ?>" />
				<?php } ?>
				*/ ?>
				<?php //end---2013/4/25 障害No.4-0001修正 ?>
				<br />
				<?php echo $form->file('Girl.image2_4');?><?php echo $form->error('Girl.image2_4'); ?><br />
				<?php echo $form->checkbox('Girl.delete_image_4');?>削除<?php echo $form->error('Girl.delete_image_4');?>
			</td>
        </tr>

    </table>


<span class="small spot8" style="font-size:120%">「横幅240px」、「高さ320px」の画像を推奨しております。<br />
拡張子「jpg」「gif」形式の画像のみ、ファイルサイズ「3MB」まで。 </span>


<div align="center" style="margin-top:40px;">
<?php echo $form->submit('編集する',array('class' => 'btn_navi'));?>
</div>
<?php echo $form->end(); ?>
-->
