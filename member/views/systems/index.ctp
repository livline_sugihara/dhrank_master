<section id="middle" class="two">
    <div class="title_bar">システム管理</div>
    <div id="timeLineArea" class="inner">
        <div class="shopTable">
            <?php echo $form->create(null,array('type'=>'post','enctype' => 'multipart/form-data','action'=>"")); ?>
                <?php echo $form->hidden('System.id'); ?>

                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <th>住所</th>
                        <td colspan="3">
                             <?php echo $form->text('System.address',array('style' => 'width:450px;', 'placeholder' => '例）梅田駅前発', 'title' => '例）梅田駅前発')); ?><br />
                            <?php echo $form->error('System.address');?>
                        </td>
                    </tr>
                    <tr>
                        <th>アクセス</th>
                        <td colspan="3">
                             <?php echo $form->text('System.access',array('style' => 'width:450px;', 'placeholder' => '例）梅田駅より徒歩10分', 'title' => '例）梅田駅より徒歩10分')); ?><br />
                            <?php echo $form->error('System.access');?>
                        </td>
                    </tr>
                    <tr>
                        <th>営業時間</th>
                        <td>
                            <?php echo $form->select('System.open_time_hour', $m_hour,null,array('empty' => '-')); ?>
                            :
                            <?php echo $form->select('System.open_time_minute', $m_minute,null,array('empty' => '-')); ?>
                             ～
                            <?php echo $form->select('System.close_time_hour', $m_hour,null,array('empty' => '-')); ?>
                            :
                            <?php echo $form->select('System.close_time_minute', $m_minute,null,array('empty' => '-')); ?><br />
                            <?php echo $form->error('System.open_time');?>
                            <?php echo $form->error('System.close_time');?>
                        </td>
                    </tr>

                    <tr>
                        <th>最低料金<br><span class="small spot8">※半角数字のみ</span></th>
                        <td>
                            <?php echo $form->text('System.price_cost',array('style' => 'width:300px;')); ?>円～<br />
                            <br />
                            <?php echo $form->error('System.price_cost');?>
                        </td>
                    </tr>
                    <tr class="price_pattern">

                        <th>料金体系<br><span class="small spot8">※半角数字のみ</span></th>

                        <td>
                            <?php if (!is_null($this->data['System']['price_pattern'])): ?>

                                <?php foreach ($this->data['System']['price_pattern'] as $key => $val): ?>

                                    <?php if (($key % 2)==0 || $key==0): ?>

                                    <div class="forms">

                                    <?php echo $form->text('System.price_pattern][]',array('style' => 'width:150px;', 'div' => false, 'default' => $val)); ?>

                                        <?php echo '分'; ?>

                                        <?php echo $form->error('System.price_pattern][]');?>

                                    <?php else: ?>

                                    <?php echo $form->text('System.price_pattern][]',array('style' => 'width:150px;', 'div' => false, 'default' => $val)); ?>

                                        <?php echo '円'; ?>

                                        <?php echo $form->error('System.price_pattern][]');?>

                                        <button type="button" name="del_btn" data-role="del">x</button>

                                        <?php echo '<br><br>'; ?>

                                    </div>

                                    <?php endif; ?>

                                <?php endforeach; ?>

                            <?php endif; ?>

                            <!-- <?php echo $form->text('System.price_pattern][]',array('style' => 'width:150px;', 'div' => false)); ?>分

                            <?php echo $form->error('System.price_pattern][]');?>

                            <?php echo $form->text('System.price_pattern][]',array('style' => 'width:150px;', 'div' => false)); ?>円

                            <?php echo $form->error('System.price_pattern][]');?> -->

                            <button type="button" name="add_btn" data-role="add">+新規追加</button><br>

                        </td>

                    </tr>

                    <tr>
                        <th>クレジット<br />カード</th>
                        <td colspan="3">
                            <label><?php echo $form->checkbox('System.credit_unavailable',array('value' => '1', 'onClick' => 'DeleteCredit();')); ?> 利用不可</label><br />

                            <label><?php echo $form->checkbox('System.credit_visa',array('value' => '1', 'onClick' => 'DeleteUnavailable(this);')); ?> VISA</label>
                            <label><?php echo $form->checkbox('System.credit_master',array('value' => '1', 'onClick' => 'DeleteUnavailable(this);')); ?> MASTER</label>
                            <label><?php echo $form->checkbox('System.credit_jcb',array('value' => '1', 'onClick' => 'DeleteUnavailable(this);')); ?> JCB</label>
                            <label><?php echo $form->checkbox('System.credit_americanexpress',array('value' => '1', 'onClick' => 'DeleteUnavailable(this);')); ?> American Express</label>
                            <label><?php echo $form->checkbox('System.credit_diners',array('value' => '1', 'onClick' => 'DeleteUnavailable(this);')); ?> Diners</label><br />

                            <label><?php echo $form->checkbox('System.credit_other',array('value' => '1', 'onClick' => 'DeleteUnavailable(this);')); ?> その他</label>
                            <?php echo $form->text('System.credit_other_comment',array('style' => 'width:450px;')); ?><br />
                            <span class="small spot8">半角200文字/全角100文字以内</span><br />
                            <?php echo $form->error('System.credit_unavailable');?>
                            <?php echo $form->error('System.credit_other_comment');?>
                        </td>
                    </tr>
                    <tr>
                        <th>コメント1<br />タイトル</th>
                        <td colspan="3">
                            <?php echo $form->text('System.comment1_title',array('style' => 'width:450px;')); ?><br />
                            <?php echo $form->error('System.comment1_title');?>
                        </td>
                    <tr>
                        <th>コメント1</th>
                        <td colspan="3">
                            <?php echo $form->textarea('System.comment1_content',array('style' => 'width:450px;height:90px;')); ?><br />
                            <?php echo $form->error('System.comment1_content');?>
                        </td>
                    </tr>
                    <tr>
                        <th>コメント2<br />タイトル</th>
                        <td colspan="3">
                            <?php echo $form->text('System.comment2_title',array('style' => 'width:450px;')); ?><br />
                            <?php echo $form->error('System.comment2_title');?>
                        </td>
                    <tr>
                    <tr>
                        <th>コメント2</th>
                        <td colspan="3">
                            <?php echo $form->textarea('System.comment2_content',array('style' => 'width:450px;height:90px;')); ?><br />
                            <?php echo $form->error('System.comment2_content');?>
                        </td>
                    </tr>
                    <tr>
                        <th>コメント3<br />タイトル</th>
                        <td colspan="3">
                            <?php echo $form->text('System.comment3_title',array('style' => 'width:450px;')); ?><br />
                            <?php echo $form->error('System.comment3_title');?>
                        </td>
                    <tr>
                    <tr>
                        <th>コメント3</th>
                        <td colspan="3">
                            <?php echo $form->textarea('System.comment3_content',array('style' => 'width:450px;height:90px;')); ?><br />
                            <?php echo $form->error('System.comment3_content');?>
                        </td>
                    </tr>
                </table>

                <div align="center">
                    <div class="submit">
                        <?php echo $form->submit('保存する',array('class' => 'btn_navi'));?>
                    </div>
                </div>
            <?php echo $form->end(); ?>

			<div style="margin-bottom:20px"><br /></div>

        	</div>
        </div>
</section>

<script language="JavaScript">
function DeleteCredit() {
    var input = document.getElementById("SystemCreditUnavailable").checked;

    if(input)
    {
        document.getElementById("SystemCreditVisa").checked = false;
        document.getElementById("SystemCreditMaster").checked = false;
        document.getElementById("SystemCreditJcb").checked = false;
        document.getElementById("SystemCreditAmericanexpress").checked = false;
        document.getElementById("SystemCreditDiners").checked = false;
        document.getElementById("SystemCreditOther").checked = false;
        document.getElementById("SystemCreditOtherComment").value = '';
    }

    document.getElementById("SystemCreditVisa").disabled = input;
    document.getElementById("SystemCreditMaster").disabled = input;
    document.getElementById("SystemCreditJcb").disabled = input;
    document.getElementById("SystemCreditAmericanexpress").disabled = input;
    document.getElementById("SystemCreditDiners").disabled = input;
    document.getElementById("SystemCreditOther").disabled = input;
    document.getElementById("SystemCreditOtherComment").disabled = input;
}
function DeleteUnavailable(input) {
    if(input.checked){
        document.getElementById("SystemCreditUnavailable").checked = false;
    }
}
</script>
