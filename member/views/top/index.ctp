<section id="middle" class="two">
    <div class="title_bar">最新の更新情報</div> 
    <div id="timeLineArea" class="inner">

        <div class="new_reviewArea clearfix">

<?php
foreach($list AS $datetime => $data) {
    $type = $data['type'];
    $record = $data['record'];
?>

            <!-- timeLine-->
            <div class="timeLine clearfix">
                <div>
                    <?php echo $imgCommon->get_reviewer_avatar($parent_user, array(40,40), 'img_reviewer'); ?>
                </div>
<?php
    if($type == 'girl') {
?>
                <div class="text">
                    <p class="date"><?php echo date('Y/m/d H:i', strtotime($datetime)); ?></p>
                    <p><?php echo $record['Reviewer']['handle']; ?>さんが「<?php echo $record['Shop']['name']; ?>」の<?php echo $record['Girl']['name']; ?>ちゃんをお気に入りに追加しました。</p>
                </div>
            </div>
            <div class="girlsArea clearfix">
                <div>
                    <p class="img">
                        <?php echo $imgCommon->get_girl_with_time($record, 'l', 1, null, 'img_frame'); ?>
                    </p>
                    <p class="name">
                     <!--   <a href="<?php echo $linkCommon->get_user_girl($record); ?>"> -->
                            <?php echo $record['Girl']['name']; ?>
                     <!--   </a> -->
                    </p>
                    <p><?php echo $record['LargeArea']['name']; ?></p>
                    <p><?php echo $record['Shop']['name']; ?></p>
                </div>
            </div>
<?php
    } else if($type == 'shop') {
?>        
                <div class="text">
                    <p class="date"><?php echo date('Y/m/d H:i', strtotime($datetime)); ?></p>
                    <p><?php echo $record['Reviewer']['handle']; ?>さんが「<?php echo $record['Shop']['name']; ?>」をお気に入りに追加しました。</p>
                </div>
            </div>
            <?php echo $this->element('common/_shop_payment', array('record' => $record, 'reviewer' => true)); ?>
<?php
    }
}
?>
        </div>
<!-- / middle -->
    </div>
</section>

<?php //echo var_dump($shop);?>
<?php //echo var_dump($today_girls);?>
