<?php
    $opentime = date('H:i', strtotime($record['System']['open_time']));
    $closetime = date('H:i', strtotime($record['System']['close_time']));
?>
        <!-- shop -->
        <div class="shopList_frame">
            <div>
                <a href="<?php echo $linkCommon->get_user_shop($record); ?>" class="shopName"><?php echo $record['Shop']['name']; ?></a>
                <span class="kuchikomi">口コミ</span><span class="c_red"><?php echo $record[0]['review_cnt']; ?>件</span>
                <a href="<?php echo $linkCommon->get_user_shop($record); ?>" class="css_btn_class">詳細を見る</a>
            </div>
            <div>
            営業時間・<?php echo $opentime; ?>〜<?php echo $closetime; ?>　最低料金・<?php echo $record['System']['price_cost']; ?>円〜　住所・<?php echo $record['System']['address']; ?>
            </div>
        </div>
        <!-- /shop -->
