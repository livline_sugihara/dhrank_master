<!-- コマンドボタン -->
<div id="shop_tabs" class="out">
	<div class="container">
		<div class="box clearfix">

					<?php echo $this->Form->create('Girl', array('action' => 'deleteall')); ?>
           	<div class="list">
				<ul class="clearfix">
					<li class="redBtn barBtn"><a href="<?php echo $linkCommon->get_girls_add() ?>">女の子登録</a></li>
					<li class="redBtn barBtn"><a href="<?php echo $linkCommon->get_girls_index() ?>">女の子一覧</a></li>
					<li class="redBtn barBtn"><a href="<?php echo $linkCommon->get_girls_orders_index() ?>">表示順位変更</a></li>
					&nbsp
					<?php echo $this->Form->input('all_select', array('type' => 'checkbox', 'label' => false, 'div' => false)) ?><li class="">一括選択</li>
					<?php echo $this->Form->input('del_ids', array('type' => 'hidden', 'div' => false)); ?>
					<li><a href=""><?php echo $this->Form->submit('一括削除', array('div' => false, 'class' => 'redBtn barBtn', 'id' => 'del_btn')) ?></a></li>
					<li><?php echo $this->Form->button('一括表示変更', array('type' => 'button', 'id' => 'changeAll')) ?></li>
					<!--<li><?php echo $this->Form->button('一括写真変更', array('type' => 'button', 'id' => 'picAll')) ?></li>-->
                </ul>
			</div>
					<?php echo $this->Form->end(); ?>
        </div>
	</div>
</div>
<!-- /コマンドボタン -->
