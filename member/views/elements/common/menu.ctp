<div class="reviewerIconArea">
    <?php echo $imgCommon->get_shop_with_time($parent_user, 'icon', array(100,100))?>
    <p><?php echo $parent_user['Shop']['name']; ?> 様</p>
</div>
<ul>
    <li<?php if($this->params["controller"] == 'top') { ?> class="select"<?php } ?>><a href="/">トップページ</a></li>
    <li<?php if($this->params["controller"] == 'shops') { ?> class="select"<?php } ?>><a href="/shops">店舗情報管理</a></li>
    <li<?php if($this->params["controller"] == 'girls') { ?> class="select"<?php } ?>><a href="/girls">女の子管理</a></li>
    <li<?php if($this->params["controller"] == 'systems') { ?> class="select"<?php } ?>><a href="/systems">システム管理</a></li>
    <li<?php if($this->params["controller"] == 'coupons') { ?> class="select"<?php } ?>><a href="/coupons">クーポン管理</a></li>
    <li<?php if($this->params["controller"] == 'reviews') { ?> class="select"<?php } ?>><a href="/reviews">口コミ管理</a></li>
</ul>
