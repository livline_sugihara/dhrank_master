<?php
// レビューページからの呼び出しかどうか
$review_flag = isset($reviewer) && $reviewer;
// Myレビューからの呼び出しかどうか
$myreview_flag = isset($myreview) && $myreview;
?>
<div class="box clearfix" id="review_box_<?php echo $record['Review']['id']; ?>">
    <div class="reviewer clearfix">
        <div class="icon">
            <a href="#">
                <?php echo $imgCommon->get_reviewer_avatar($record, array(40,40), 'img_reviewer'); ?>
            </a>
        </div>
        <div>
            <p><a href="#"><?php echo $record['Review']['post_name']; ?></a>さん</p>
            <p><?php echo $textCommon->escape_empty($record, 'MReviewersAge.name', '不明'); ?>／<a href="#">口コミ投稿： <?php if($record['Review']['reviewer_id'] == 0) { echo '非会員'; } else { echo $record[0]['review_cnt'] . '件'; } ?></a></p>
        </div>
        <span id="pickup_mark_<?php echo $record['Review']['id']; ?>">
<?php if($record['Review']['pickup_shop'] == 1) { ?>
            <p class="pickup_icon">PickUp口コミ</p>
<?php } ?>
        </span>
    </div>
    <div class="reviewText clearfix">
        <div class="clearfix">
            <p class="img_girl">
                <?php echo $imgCommon->get_girl_with_time($record, 's', 1); ?>
            </p>
            <div class="summary">
                <p class="shop"><?php echo $record['Shop']['name']; ?></p>
                <p class="name"><?php echo $record['Girl']['name']; ?></p>
                <p class="scoreInfo">
<?php
for($ii = 1; $ii <= 5; $ii++) {
    $suffix = ($ii <= $record[0]['girl_avg']) ? 'on' : 'off';
?>
                    <img src="/img/star_<?php echo $suffix; ?>.png">
<?php } ?>
                    <span class="score"><?php echo round($record[0]['girl_avg'], 2); ?></span>
                </p>
                <p class="scoreType">[<span class="point">L</span>ルックス <?php echo $textCommon->escape_empty($record, 'Review.score_girl_looks', '-'); ?> /<span class="point">S</span>サービス <?php echo $textCommon->escape_empty($record, 'Review.score_girl_service', '-'); ?> /<span class="point">C</span>性格 <?php echo $textCommon->escape_empty($record, 'Review.score_girl_character', '-'); ?> ]</p>
                <div class="course">
                    <span>コース料金／<?php echo $textCommon->escape_empty($record, 'Review.course_minute', '--'); ?>分 <?php echo $textCommon->escape_empty($record, 'Review.course_cost', '--'); ?>円</span>
                    <span>ご利用場所／<?php echo $textCommon->escape_empty($record, 'MReviewsPlace.name', '不明'); ?></span>
                </div>
                <div class="course_mark">
                    <span class="target">
                        <?php echo ($record['Review']['appointed_sub'] == 0) ? '不明' : $appointed_sub[$record['Review']['appointed_sub']]; ?>
                    </span>
                    <span class="new">
                        <?php echo ($record['Review']['appointed_type'] == 0) ? '不明' : $appointed_type[$record['Review']['appointed_type']]; ?>
                    </span>
                </div>
            </div>
        </div>
        <div>

<?php if($record['Review']['delete_flag'] == Review::DELETE_FLAG_COMMENT_ONLY) { ?>

<?php } else if($record['Review']['delete_flag'] == Review::DELETE_FLAG_SAVE_POINTS) { ?>
						<p class="txt">このコメントは削除依頼により削除されました。</p>
<?php } else { ?>
            <p class="tt">【レビュー】<?php echo $record['Girl']['name']; ?>ちゃん 口コミ報告</p>
            <p class="txt"><?php echo nl2br($record['Review']['comment']); ?></p>
<?php } ?>
        </div>
    </div>
<!--
<div style="background-color:#ddddff;padding:10px 10px 10px 10px;">
店舗からのお返事:<br /><?php if (isset($record['ReviewReply'][0]) && count($record['ReviewReply'][0]) > 0 && $record['ReviewReply'][0]['ReviewReply']['comment'] != ''){ ?>
    <?php echo nl2br(htmlspecialchars($record['ReviewReply'][0]['ReviewReply']['comment'])); ?>
<?php } else { ?>
    まだお返事はありません<br />
<?php } ?>
</div>
<div style="background-color:#ffdddd;padding:10px 10px 10px 10px;">
    女の子からのお返事：<br />    <?php if (isset($record['ReviewReply'][1]) && count($record['ReviewReply'][1]) > 0 && $record['ReviewReply'][1]['ReviewReply']['comment'] != ''){ ?>
        <?php echo nl2br(htmlspecialchars($record['ReviewReply'][1]['ReviewReply']['comment'])); ?>

    <?php } else { ?>
        まだお返事はありません<br />
    <?php } ?>
</div>
-->
    <div class="align_c" style="padding: 20px 0 5px 0;">
<?php if($parent_user['UsersAuthority']['is_pickup_review'] == 1) { ?>
        <a class="member_girls_button pickupReview" href="#" data="<?php echo $record['Review']['id']; ?>">PickUp口コミ選択</a>
<?php } ?>
<?php if($parent_user['UsersAuthority']['is_delete_review'] == 1) { ?>
        <a class="member_girls_button deleteReview" href="#" data="<?php echo $record['Review']['id']; ?>">削除する</a>
<?php } ?>
<!--
        <div class="reply_review" style="margin:10px 0 10px 0; ">            <textarea name="reply_review_<?php echo $record['Review']['id']; ?>" cols="81" rows="5"></textarea><br />
            <a class="member_girls_button replyReview" style="float:right;" href="#" data="<?php echo $record['Review']['id']; ?>">口コミに返信する</a>
            <select name="reply_written_<?php echo $record['Review']['id']; ?>" style="float:right;">
                <option value="1">店舗から</option>
                <option value="2">女の子から</option>
            </select>
        </div>
-->
    </div>





</div>
