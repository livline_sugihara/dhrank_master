<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<?php echo $this->Html->charset(); ?>
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />

<?php
if(!empty($meta_keywords)){
echo $this->Html->meta('keywords', null, array('content' => $meta_keywords), true);
}
if(!empty($meta_description)){
echo $this->Html->meta('description', null, array('content' => $meta_description), true);
}
?>
<?php
echo $this->Html->css(array(
	'add',
	'main',
	'normalize',
	'style_new',
	'jquery.bxslider'
	)
);

//echo $scripts_for_layout;
?>
<!--
<script type="text/javascript">
   function cancelSubmit(e){
       if (!e) var e = window.event;

       if(e.keyCode == 13)
           return false;
   }

   window.onload = function (){
   var list = document.getElementsByTagName("input");
       for(var i=0; i<list.length; i++){
           if(list[i].type == 'text' || list[i].type == 'checkbox' ||
list[i].type == 'radio' || list[i].type == 'select' || list[i].type ==
'file')
               {
               list[i].onkeypress = function (event){
               return cancelSubmit(event);
               };
           }
       }
   }
</script>

<?php //start---2013/3/10 障害No.1-7-0003修正 ?>
<script type="text/javascript">
$.ajaxSetup({ cache: false });
</script>
<?php //end---2013/3/10 障害No.1-7-0003修正 ?>
-->

</head>


<body>

<?php echo $this->element('common/header'); ?>
<?php echo $this->element('common/breadcrumb', array("breadcrumb" => $breadcrumb)); ?>

<!-- contents -->
<section id="contents" class="container clearfix">

	<h2 class="heading"><?php echo $parent_user['Shop']['name']; ?> 様・店舗管理</h2>

	<!-- サイドメニュー -->
	<section id="menu" class="two">
	    <?php echo $this->element('common/menu'); ?>
	</section>

	<?php echo $content_for_layout; ?>

</section>
<!-- /contents -->

<?php echo $this->element('common/footer'); ?>
<?php echo $this->Html->script(array(
	'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
	'main.js',
	'jquery.mousewheel.js',
//	'jquery.easing.1.3.js',
	'jquery.bxslider.js',
	'jquery.lightbox_me.js',
	'jquery.scrollUp.js',
	)
);
?>

<script>
//
// 	function f_dragstart(event){
// 		//ドラッグするデータのid名をDataTransferオブジェクトにセット
// 		event.dataTransfer.setData("text", event.target.id);
// 	}
//
// 	/***** ドラッグ要素がドロップ要素に重なっている間の処理 *****/
// 	function f_dragover(event){
// 		var name = $(event.currentTarget).find('p.name').text();
// 		var index = $('#drop_girl div').index(event.currentTarget);
// 		$div =
// 		$('div').eq(2).prepend('')
//
//
// //		 ondragover="f_dragover(event)" ondrop="f_drop(event)"
// 		console.log(index);
// 		//console.log($('#drop_girl > #drag_girl > div').index(event.currentTarget));
// 		//dragoverイベントをキャンセルして、ドロップ先の要素がドロップを受け付けるようにする
// 		event.preventDefault();
// 	}
//
// 	/***** ドロップ時の処理 *****/
// 	function f_drop(event){
// 		//ドラッグされたデータのid名をDataTransferオブジェクトから取得
// 		var id_name = event.dataTransfer.getData("text");
// 		//id名からドラッグされた要素を取得
// 		var drag_elm =document.getElementById(id_name);
// 		//ドロップ先にドラッグされた要素を追加
// 		event.currentTarget.appendChild(drag_elm);
// 		//エラー回避のため、ドロップ処理の最後にdropイベントをキャンセルしておく
// 		event.preventDefault();
// 	}
//


// jquery.sortable.js
(function($) {
var dragging, placeholders = $();
$.fn.sortable = function(options) {
	var method = String(options);
	options = $.extend({
		connectWith: false
	}, options);
	return this.each(function() {
		if (/^enable|disable|destroy$/.test(method)) {
			var items = $(this).children($(this).data('items')).attr('draggable', method == 'enable');
			if (method == 'destroy') {
				items.add(this).removeData('connectWith items')
					.off('dragstart.h5s dragend.h5s selectstart.h5s dragover.h5s dragenter.h5s drop.h5s');
			}
			return;
		}
		var isHandle, index, items = $(this).children(options.items);
		var placeholder = $('<' + (/^ul|ol$/i.test(this.tagName) ? 'li' : 'div') + ' class="sortable-placeholder">');
		items.find(options.handle).mousedown(function() {
			isHandle = true;
		}).mouseup(function() {
			isHandle = false;
		});
		$(this).data('items', options.items)
		placeholders = placeholders.add(placeholder);
		if (options.connectWith) {
			$(options.connectWith).add(this).data('connectWith', options.connectWith);
		}
		items.attr('draggable', 'true').on('dragstart.h5s', function(e) {
			if (options.handle && !isHandle) {
				return false;
			}
			isHandle = false;
			var dt = e.originalEvent.dataTransfer;
			dt.effectAllowed = 'move';
			dt.setData('Text', 'dummy');
			index = (dragging = $(this)).addClass('sortable-dragging').index();
		}).on('dragend.h5s', function() {
			dragging.removeClass('sortable-dragging').show();
			placeholders.detach();
			if (index != dragging.index()) {
				items.parent().trigger('sortupdate', {item: dragging});
			}
			dragging = null;
		}).not('a[href], img').on('selectstart.h5s', function() {
			this.dragDrop && this.dragDrop();
			return false;
		}).end().add([this, placeholder]).on('dragover.h5s dragenter.h5s drop.h5s', function(e) {
			if (!items.is(dragging) && options.connectWith !== $(dragging).parent().data('connectWith')) {
				return true;
			}
			if (e.type == 'drop') {
				e.stopPropagation();
				placeholders.filter(':visible').after(dragging);
				return false;
			}
			e.preventDefault();
			e.originalEvent.dataTransfer.dropEffect = 'move';
			if (items.is(this)) {
				if (options.forcePlaceholderSize) {
					placeholder.height(dragging.outerHeight());
				}
				dragging.hide();
				$(this)[placeholder.index() < $(this).index() ? 'after' : 'before'](placeholder);
				placeholders.not(placeholder).detach();
			} else if (!placeholders.is(this) && !$(this).children(options.items).length) {
				placeholders.detach();
				$(this).append(placeholder);
			}
			return false;
		});
	});
};
})(jQuery);


$(function(){

	// add or del form
	$('tr.price_pattern button').on('click', function(){
		var role = $(this).data('role');
		switch(role){
			case 'del':
			$(this).closest('div').remove();
			break;
			case 'add':
			var t = $(this).closest('td').find('div.forms:first');
console.log(t);
			var new_t = t.clone(true);
			t.after(new_t[0]);

			break;

		}
    });


	//sortable
	$('.sortable').sortable({
		handle: 'span'
	});

	//select_all
	$('input#GirlAllSelect').on('change', function(){
		$('input[class="girl_check"]').prop('checked', function(i, val){
			return !val;
		});
	});

	$(function () {
		$.scrollUp({
			animation: 'fade',
		});
	});


	//func deleteAll
	$('#del_btn').on('click', function(){
		var ret = confirm('本当に削除しますか？');
		if (ret == true) {
			ids = {};
			$('div[draggable="true"]').find('input:checked').each(function(){
				ids[$(this).val()] = $(this).val();
			});
			$.ajax({
				type: 'post',
				url: '/girls/deleteall',
				data: ids,
				success: function(data){
					//console.log(data);
					alert('削除しました。');
					location.reload(true);
				},
				error: function(){
					alert('error');
					console.log('err');
				}

			});
			return false;
		}
		return false;
	});

	$('button#changeAll').on('click', function(){
		var ret = confirm('以下の順に変更しますか？');
		if (ret == true) {
			//順番をインデックスで取得
			var ids = [];
			var indexes = [];
			var orders = {};
			$('input[class="girl_check"]').prop('checked', function(){
				id = $(this).val();
				index = $(this).index('#drop_girl input[class="girl_check"]');
				orders[id] = index;
			});
	//return console.log(orders);
			if(orders){
				$.ajax({
					type: 'post',
					url: '/girls/changeall',
					data: orders,
					success: function(data){
						location.reload(true);
					},
					error: function(XMLHttpRequest,textStatus){
						alert('エラー');
					}
				});
			}
		}
		return false;
	});

	//tabs
	$(function() {

	    // Pickup口コミ
		$('.pickupReview').click(function(e) {
	        var review_id = $(this).attr('data');

	        if(review_id == undefined) {
	        	return;
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/toggle_pickup_review/"　+ review_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){

					if(data.success) {

						// 口コミPickupがONの場合
						if(data.flag) {
							$('#pickup_mark_' + review_id).html('<p class="pickup_icon">PickUp口コミ</p>');
						} else {
							$('#pickup_mark_' + review_id).empty();
						}

					}

					alert(data.message);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});


	    // 記事削除
		$('.deleteReview').click(function(e) {
	        var review_id = $(this).attr('data');

	        if(review_id == undefined) {
	        	return;
	        }

	        if(!confirm('このレビューを本当に削除しても宜しいですか？')) {
	        	return;
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/delete_review/"　+ review_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){

					// 削除成功の場合
					if(data.success) {
						$('#review_box_' + review_id).remove();

						var cnt = parseInt($('#count_num').text());
						$('#count_num').text(cnt - 1);
					}
					alert(data.message);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});

		// 口コミの返信
		$('.replyReview').click(function(e) {
			var review_id = $(this).attr('data'); // レビューID取得

	        if(review_id == undefined) {
	        	return;
	        }

			if(!confirm('レビューに返信をおこなってよいでしょうか？')) {
	        	return;
	        }

			// スタッフか、女の子かを取得
			var reply_written = $('[name=reply_written_' + review_id +  ']').val();

			// コメントを取得
			var reply_review = $('[name=reply_review_' + review_id + ']').val();

			// リクエスト送信
	        jQuery.ajax({
				type: "post",
//				url: "/reply_review/"　+ review_id + "/" + reply_written + "/" + reply_review,
				url: "/reply_review/"　+ review_id + "/" + reply_written,
				data: {"reply_review" : reply_review},
				dataType: "html",
				cache: false,
				success: function(data){
					alert("口コミの返信を行いました。");
					location.reload(true);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					//alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();

		});


	});

});
</script>

</body>
</html>
