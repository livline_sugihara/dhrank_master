<!doctype php>
<!-- head -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<!--[if lt IE 9]>
<script src="/js/html5shiv.js"></script>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<head>
<?php echo $this->Html->charset(); ?>
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><?php echo $title_for_layout; ?></title>

<?php
if(!empty($meta_keywords)){
echo $this->Html->meta('keywords', null, array('content' => $meta_keywords), true);
}
if(!empty($meta_description)){
echo $this->Html->meta('description', null, array('content' => $meta_description), true);
}
?>    

<script type="text/javascript">
   function cancelSubmit(e){
       if (!e) var e = window.event;

       if(e.keyCode == 13)
           return false;
   }

   window.onload = function (){
   var list = document.getElementsByTagName("input");
       for(var i=0; i<list.length; i++){
           if(list[i].type == 'text' || list[i].type == 'checkbox' ||
list[i].type == 'radio' || list[i].type == 'select' || list[i].type ==
'file')
               {
               list[i].onkeypress = function (event){
               return cancelSubmit(event);
               };
           }
       }
   }
</script>
</head>
<!-- / head -->

<?php
echo $this->Html->css(array(
  'add',
  'main',
  'normalize',
  'style_new',
  'jquery.bxslider'
  )
);
?>

<body>
<?php echo $this->element('common/header_login'); ?>

<!--
<div id="topic_path">
  <a href="/users/login">ログイン</a>
</div>
-->
<?php echo $this->element('common/breadcrumb', array("breadcrumb" => $breadcrumb)); ?>

<!-- contents -->
<section id="contents" class="container clearfix">

  <?php echo $content_for_layout; ?>

</section>
<!-- /contents -->

<?php echo $this->element('common/footer'); ?>

</body>

<?php echo $this->Html->script(array(
  'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
  'main.js',
  'jquery.mousewheel.js',
  'jquery.easing.1.3.js',
  'jquery.bxslider.js',
  'jquery.lightbox_me.js',
  'jquery.scrollUp.js'
  )
);
?>

<script>
$(document).ready(function(){
  
  $(function () {
    $.scrollUp({
      animation: 'fade',
    });
  });
  
  //slider
  $('#slider1').bxSlider({
    auto:true,
    speed:1000,
    infiniteLoop: true,
    hideControlOnEnd: false,
    slideWidth: 320,
    pager: true,
    controls: true,
  });

  $('#slider2').bxSlider({
    auto:true,
    speed:1000,
    infiniteLoop: true,
    hideControlOnEnd: false,
    pager: false,
    controls: true,
  });
  
  $('#slider3').bxSlider({
    auto:true,
    speed:1000,
    infiniteLoop: true,
    hideControlOnEnd: false,
    pager: false,
    controls: false,
  });
  
  $('#topicSlider').bxSlider({
    auto:true,
    speed:1000,
    infiniteLoop: true,
    hideControlOnEnd: false,
    pager: false,
    controls: false,
    pause: 8000,
  });
  
  $('#girlsSlider').bxSlider({
    auto:true,
    speed:1000,
    infiniteLoop: true,
    hideControlOnEnd: false,
    slideWidth: 615,
    pager: false,
    controls: true,
  });
  
  //slideshow
  $('#newSlideShow').bxSlider({
    minSlides: 4,
      maxSlides: 4,
      slideWidth: 590,
      ticker: true,
      speed: 7000
  });
  
  $('#girlsSlideShow').bxSlider({
    minSlides: 4,
      maxSlides: 4,
      slideWidth: 606,
      ticker: true,
      speed: 7000
  });
  
  //tabs
  $(function() {
    
    $(".content_wrap").hide();
    $("#tabs li:first").addClass("").show();
    $(".content_wrap:first").show();
     
    $("#tabs li").click(function() {
      $("#tabs li").removeClass("select");
      $(this).addClass("select");
        
      var num = $("#tabs li").index(this);
      $(".content_wrap").not(num).fadeOut(1000).eq(num).fadeIn(1000);
      return false;
    });
  });
  
  //lightbox_me
  $('.frameOpen').click(function(e) {
    $('#frame').lightbox_me({
        centered: true,
        onLoad: function() { 
            $('#frame').find('input:first').focus()
            }
        });
    e.preventDefault();
  });

})
</script>
</html>


