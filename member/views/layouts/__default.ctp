<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<?php echo $this->Html->charset(); ?>
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><?php echo $title_for_layout; ?></title>

<?php
if(!empty($meta_keywords)){
echo $this->Html->meta('keywords', null, array('content' => $meta_keywords), true);
}
if(!empty($meta_description)){
echo $this->Html->meta('description', null, array('content' => $meta_description), true);
}
?>
</head>
<!-- / head -->

<?php
echo $this->Html->css(array(
	'add',
	'main',
	'normalize',
	'style_new',
	'jquery.bxslider'
	)
);

//echo $scripts_for_layout;
?>

<!--
<script type="text/javascript">
   function cancelSubmit(e){
       if (!e) var e = window.event;

       if(e.keyCode == 13)
           return false;
   }

   window.onload = function (){
   var list = document.getElementsByTagName("input");
       for(var i=0; i<list.length; i++){
           if(list[i].type == 'text' || list[i].type == 'checkbox' ||
list[i].type == 'radio' || list[i].type == 'select' || list[i].type ==
'file')
               {
               list[i].onkeypress = function (event){
               return cancelSubmit(event);
               };
           }
       }
   }
</script>

<?php //start---2013/3/10 障害No.1-7-0003修正 ?>
<script type="text/javascript">
$.ajaxSetup({ cache: false });
</script>
<?php //end---2013/3/10 障害No.1-7-0003修正 ?>
-->

<body>

<?php echo $this->element('common/header'); ?>
<?php echo $this->element('common/breadcrumb', array("breadcrumb" => $breadcrumb)); ?>

<!-- contents -->
<section id="contents" class="container clearfix">

	<h2 class="heading"><?php echo $parent_user['Shop']['name']; ?> 様・店舗管理</h2>

	<!-- サイドメニュー -->
	<section id="menu" class="two">
	    <?php echo $this->element('common/menu'); ?>
	</section>

	<?php echo $content_for_layout; ?>

</section>
<!-- /contents -->

<?php echo $this->element('common/footer'); ?>

</body>

<?php echo $this->Html->script(array(
	'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
	'main.js',
	'jquery.mousewheel.js',
//	'jquery.easing.1.3.js',
	'jquery.bxslider.js',
	'jquery.lightbox_me.js',
	'jquery.scrollUp.js'
	)
);
?>

<script>
$(document).ready(function(){
/***** ドラッグ開始時の処理 *****/alert('ready');    $(function f_dragstart(event){    	//ドラッグするデータのid名をDataTransferオブジェクトにセット    	event.dataTransfer.setData("text", event.target.id);    });    /***** ドラッグ要素がドロップ要素に重なっている間の処理 *****/    $(function f_dragover(event){    	//dragoverイベントをキャンセルして、ドロップ先の要素がドロップを受け付けるようにする    	event.preventDefault();    });    /***** ドロップ時の処理 *****/    $(function f_drop(event){    	//ドラッグされたデータのid名をDataTransferオブジェクトから取得    	var id_name = event.dataTransfer.getData("text");    	//id名からドラッグされた要素を取得    	var drag_elm =document.getElementById(id_name);    	//ドロップ先にドラッグされた要素を追加    	event.currentTarget.appendChild(drag_elm);        //エラー回避のため、ドロップ処理の最後にdropイベントをキャンセルしておく    	event.preventDefault();    });
	$(function () {
		$.scrollUp({
			animation: 'fade',
		});
	});

	//tabs
	$(function() {

	    // Pickup口コミ
		$('.pickupReview').click(function(e) {
	        var review_id = $(this).attr('data');

	        if(review_id == undefined) {
	        	return;
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/toggle_pickup_review/"　+ review_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){

					if(data.success) {

						// 口コミPickupがONの場合
						if(data.flag) {
							$('#pickup_mark_' + review_id).html('<p class="pickup_icon">PickUp口コミ</p>');
						} else {
							$('#pickup_mark_' + review_id).empty();
						}

					}

					alert(data.message);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});


	    // 記事削除
		$('.deleteReview').click(function(e) {
	        var review_id = $(this).attr('data');

	        if(review_id == undefined) {
	        	return;
	        }

	        if(!confirm('このレビューを本当に削除しても宜しいですか？')) {
	        	return;
	        }

	        // リクエスト送信
	        jQuery.ajax({
				type: "get",
				url: "/delete_review/"　+ review_id,
				data: "",
				dataType: "json",
				cache: false,
				success: function(data){

					// 削除成功の場合
					if(data.success) {
						$('#review_box_' + review_id).remove();

						var cnt = parseInt($('#count_num').text());
						$('#count_num').text(cnt - 1);
					}
					alert(data.message);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert('エラーが発生しました。もう一度ボタンを押して下さい。');
					// alert('status[' + XMLHttpRequest.status + ':' + textStatus +'] => ' + errorThrown);
				}
			});
	        e.preventDefault();
		});

	});

});
</script>
</html>
