<!DOCTYPE html PUBLIC "-//i-mode group (ja)//DTD XHTML i-XHTML(Locale/Ver.=ja/2.0) 1.0//EN" "i-xhtml_4ja_10.dtd">
<html lang="ja" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja">
<head>
	<meta name="robots" content="noindex,nofollow">
	<meta http-equiv="Content-Language" content="ja" />
	<meta http-equiv="Content-type" content="text/html; charset=Shift_JIS" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title><?php echo $title_for_layout; ?></title>
</head>

<body style="text-align:center; margin:0 auto; color:#1A171B;" link="#E7511E" vlink="#E7511E" bgcolor="#FFFFFF" text="#1A171B">

<div style="width:240px; text-align:left; margin:0 auto; font-size:x-small;">
	<div style="display: -wap-marquee; -wap-marquee-style: scroll; -wap-marquee-loop: infinite; -wap-marquee-speed: normal; background-color:#FDF4E7;">
		<h1 style="line-height:1em;margin:0;font-size:small;"><?php echo ((!empty($header_one))? $header_one:'')?></h1>
	</div>

	<img src="/images/i/logo.jpg" />

<div style="text-align:right;padding:3px;">
	ようこそ <?php echo $parent_user['Shop']['name']?>様
</div>

<?php echo $this->Session->flash(); ?>
<?php echo $content_for_layout; ?>


<hr color="#DED7C8" />

<div style="text-align:left;padding:3px;">
	<?php if(!($this->params["controller"] == 'top' && $this->params["action"] == 'i_index')){?>
	<img src="/images/i/home.jpg" /><?php echo $html->link('管理メニュー', $linkCommon->get_device_path() . '/',array('escape' => false)); ?><br />
	<?php }?>
	<img src="/images/i/logout.jpg" /><?php echo $html->link('ログアウト',$linkCommon->get_device_path() . '/users/logout',array('id' => 'logout')); ?>
</div>

<hr color="#DED7C8" />

<div style="text-align:center;margin-top:20px;">
	Copyright(c)<br  />
	<a href="/i/">デリヘル人気口コミランキング</a><br />
	All Rights Reverved. 無断転載禁止
</div>

</div>

<?php echo $this->element('sql_dump'); ?>

</body>
</html>

