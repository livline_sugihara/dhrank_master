<section id="middle" class="two">
    <div class="title_bar">店舗基本情報管理</div>
    <div id="timeLineArea" class="inner">
        <div class="shopTable">
            <h3>店舗基本情報</h3>
            <div class="shopTable">
				<?php echo $form->create(null,array('type'=>'post','enctype' => 'multipart/form-data','action'=>"")); ?>
					<?php echo $form->hidden('Shop.id'); ?>
                    
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th>店名</th>
                            <td colspan="3"><?php echo $parent_user['Shop']['name']?><br />
                                <span class="small spot8">「店名」は変更出来ません。変更をご希望される場合はお問合せ窓口よりご連絡下さい。</span>
                            </td>
                        </tr>
                        <tr>
                            <th>電話番号</th>
                            <td colspan="3"><?php echo $parent_user['Shop']['phone_number']?><br />
                            <span class="small spot8">「電話番号」は変更出来ません。変更をご希望される場合はお問合せ窓口よりご連絡下さい。</span></td>
                        </tr>
                        <tr>
                            <th>業種</th>
                            <td colspan="3">
                            	<?php echo $form->select('Shop.shops_business_category_id', $m_shops_business_categories,null,array('empty'=>'選んで下さい')); ?>
                            	<?php echo $form->error('Shop.shops_business_category_id'); ?>
                            </td>
                        </tr>

<?php if($parent_user['User']['is_official_link_display'] == 1) { ?>
                        <tr>
                            <th>ホームページ<br />アドレス</th>
                            <td colspan="3">
                                PC版<br />
									<?php echo $form->text('Shop.url_pc',array('style' => 'width:450px;')); ?>
									<?php echo $form->error('Shop.url_pc'); ?><br />
                                携帯版<br />
									<?php echo $form->text('Shop.url_mobile',array('style' => 'width:450px;')); ?>
									<?php echo $form->error('Shop.url_mobile'); ?><br />
                                スマートフォン版<br />
									<?php echo $form->text('Shop.url_smartphone',array('style' => 'width:450px;')); ?>
									<?php echo $form->error('Shop.url_smartphone'); ?></td>
                            </td>
                        </tr>
<?php } ?>
                        <tr>
                            <th>サービス</th>
                            <td colspan="3">
                                <label><?php echo $form->checkbox('Shop.service01',array('value' => '1')); ?> 先行予約OK</label><br />
                                <label><?php echo $form->checkbox('Shop.service02',array('value' => '1')); ?> クレジットOK</label><br />
                                <label><?php echo $form->checkbox('Shop.service03',array('value' => '1')); ?> 領収書OK</label><br />
                                <label><?php echo $form->checkbox('Shop.service04',array('value' => '1')); ?> 待ち合わせOK</label><br />
                                <label><?php echo $form->checkbox('Shop.service05',array('value' => '1')); ?> コスプレあり</label><br />
                                <label><?php echo $form->checkbox('Shop.service06',array('value' => '1')); ?> クーポンあり</label>
                            </td>
                        </tr>
                        <tr>
                            <th>店長からの<br />おしらせ</th>
                            <td colspan="3">
                            	<?php echo $form->textarea('Shop.infomation',array('style' => 'width:450px;height:90px;')); ?>
                            	<?php echo $form->error('Shop.infomation'); ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <th>お店について</th>
                            <td colspan="3">
                            	<?php echo $form->text('Shop.about_title',array('style' => 'width:450px;', 'placeholder' => 'お店についての紹介文タイトル', 'title' => 'お店についての紹介文タイトル')); ?>
                            	<?php echo $form->textarea('Shop.about_contents',array('style' => 'width:450px;height:90px;', 'placeholder' => 'お店についての紹介文をお書き下さい', 'title' => 'お店についての紹介文をお書き下さい')); ?>
                            	<?php echo $form->error('Shop.infomation'); ?>
                            	<?php echo $form->error('Shop.about_contents'); ?>
                            </td>
                        </tr>
                        
                    </table>
                    
                    <br />
                    
                    <h3>画像アップロード(PC)</h3>
                    
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th>店舗アイコン画像</th>
                            <td colspan="3">
                                <?php echo $imgCommon->get_shop_with_time($data, 'icon'); ?><br />
                                <?php echo $form->file('Shop.image_icon_file');?>
                                <?php echo $form->error('Shop.image_icon_file');?><br />
                                <span class="small spot8">店舗ページに表示される画像です。<?php echo APP_IMG_SHOP_ICON_WIDTH; ?>×<?php echo APP_IMG_SHOP_ICON_HEIGHT; ?>ピクセル</span>
                            </td>
                        </tr>
                        <tr>
                            <th>おすすめ店舗画像</th>
                            <td colspan="3">
                                <?php echo $imgCommon->get_shop_with_time($data, 'recommend'); ?><br />
                                <?php echo $form->file('Shop.image_recommend_file');?>
                                <?php echo $form->error('Shop.image_recommend_file');?><br />

                                <span class="small spot8">「おすすめ店舗」画像です。<?php echo APP_IMG_SHOP_RECOMMEND_WIDTH; ?>×<?php echo APP_IMG_SHOP_RECOMMEND_HEIGHT; ?>ピクセル</span>
                            </td>
                        </tr>
                        <tr>
                            <th>店舗リスト画像</th>
                            <td colspan="3">
                                <?php echo $imgCommon->get_shop_with_time($data, 'list'); ?><br />
                                <?php echo $form->file('Shop.image_list_file');?>
                                <?php echo $form->error('Shop.image_list_file');?><br />
                                <span class="small spot8">PC版店舗リストに表示される画像です。<?php echo APP_IMG_SHOP_LIST_WIDTH; ?>×<?php echo APP_IMG_SHOP_LIST_HEIGHT; ?>ピクセル</span>
                            </td>
                        </tr>
<?php if($parent_user['User']['is_shop_header_image'] == 1) { ?>
                        <tr>
                            <th>店舗ヘッダー画像</th>
                            <td colspan="3">
                                <?php echo $imgCommon->get_shop_with_time($data, 'header', false); ?><br />
                                <?php echo $form->file('Shop.image_header_file');?>
                                <?php echo $form->error('Shop.image_header_file');?><br />
                                <span class="small spot8">PC版店舗ページ最上部に表示される画像です。横幅<?php echo APP_IMG_SHOP_HEADER_WIDTH; ?>ピクセル・縦幅制限無し</span>
                            </td>
                        </tr>
<?php } ?>
                        <tr>
                            <th>店舗紹介画像</th>
                            <td colspan="3">
                                <?php echo $imgCommon->get_shop_with_time($data, 'profile'); ?><br />
                                <?php echo $form->file('Shop.image_profile_file');?>
                                <?php echo $form->error('Shop.image_profile_file');?><br />
                                <span class="small spot8">PC版店舗ページ上部に表示される画像です。<?php echo APP_IMG_SHOP_PROFILE_WIDTH; ?>×<?php echo APP_IMG_SHOP_PROFILE_HEIGHT; ?>ピクセル</span>
                            </td>
                        </tr>
                    </table>
                    
                    <br />
                    
                    <h3>画像アップロード(スマートフォン)</h3>
                    
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th>店舗ヘッダー画像</th>
                            <td colspan="3">
                                <?php echo $imgCommon->get_shop_with_time($data, 'header_sp', false); ?><br />
                                <?php echo $form->file('Shop.image_header_sp_file');?>
                                <?php echo $form->error('Shop.image_header_sp_file');?><br />
                                <span class="small spot8">スマートフォン版店舗ページ最上部に表示される画像です。横幅<?php echo APP_IMG_SHOP_HEADER_SP_WIDTH; ?>ピクセル・縦幅制限無し</span>
                            </td>
                        </tr>
                        <tr>
                            <th>店舗リスト画像</th>
                            <td colspan="3">
                                <?php echo $imgCommon->get_shop_with_time($data, 'list_sp'); ?><br />
                                <?php echo $form->file('Shop.image_list_sp_file');?>
                                <?php echo $form->error('Shop.image_list_sp_file');?><br />
                                <span class="small spot8">スマートフォン版店舗リストに表示される画像です。<?php echo APP_IMG_SHOP_LIST_SP_WIDTH; ?>×<?php echo APP_IMG_SHOP_LIST_SP_HEIGHT; ?>ピクセル</span>
                            </td>
                        </tr>
                    </table>
                    
                    <div align="center">
                        <div class="submit">
                            <?php echo $form->submit('保存する',array('class' => 'btn_navi'));?>
                        </div>
                    </div>
        
                <?php echo $form->end(); ?>

				<div style="margin-bottom:20px"><br /></div>
            
			</div>
        
		</div>
            
    </div>
</section>

<?php //echo var_dump($data)?>
