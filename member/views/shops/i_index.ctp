
<div style="text-align:left;padding:5px;font-size:medium;background-color:#EFEBE3">
	店舗情報管理
</div>

<?php echo $form->create(null,array('type'=>'post','action'=>"",'encoding' => null)); ?>
<?php echo $form->hidden('Shop.id'); ?>
<?php echo $form->hidden('Shop.user_id'); ?>
<?php echo $form->hidden('Shop.name'); ?>
<?php echo $form->hidden('Shop.phone_number'); ?>
<?php echo $form->hidden('Shop.email'); ?>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	店名
</div>
<div style="text-align:left;padding:3px;">
	<?php echo $parent_user['Shop']['name']?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	電話番号
</div>
<div style="text-align:left;padding:3px;">
	<?php echo $parent_user['Shop']['phone_number']?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	ジャンル
</div>
<div style="text-align:left;padding:3px;">
	<?php echo $form->select('Shop.shops_business_category_id', $m_shops_business_categories,null,array('empty'=>'選んで下さい')); ?>
	<?php echo $form->error('Shop.shops_business_category_id'); ?>
</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	メールアドレス
</div>
<div style="text-align:left;padding:3px;">
	<?php echo $parent_user['Shop']['email']?><br />
     <span style="color:#E2001A">※パスワード等の変更の際に使います。<br />
	携帯のメールアドレスでご登録の方はsugamo-club.comからのメールを受信できるようドメイン設定をお願いします。</span>

</div>

<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	女の子画像用携帯メールアドレス
</div>
<div style="text-align:left;padding:3px;">
	<?php echo $form->text('Shop.girls_mobile_email'); ?>
	<?php echo $form->error('Shop.girls_mobile_email'); ?><br />
	<span style="color:#E2001A">モバイル用管理画面から女の子画像をアップロードする場合はこちらにアップロードを行う携帯電話のメールアドレスをご入力ください。 </span>
</div>

<?php if($parent_user['User']['is_official_link_display'] == 1){?>
<div style="text-align:left;padding:3px;background-color:#F4E7D1">
	ホームページアドレス
</div>
<div style="text-align:left;padding:3px;">
	PC版：<br />
	<?php echo $form->text('Shop.url_pc'); ?>
	<?php echo $form->error('Shop.url_pc'); ?><br />
    携帯版：<br />
	<?php echo $form->text('Shop.url_mobile'); ?>
	<?php echo $form->error('Shop.url_mobile'); ?><br />
    スマートフォン版：<br />
	<?php echo $form->text('Shop.url_smartphone'); ?>
	<?php echo $form->error('Shop.url_smartphone'); ?>
</div>
<?php }?>

<div style="text-align:center;padding:3px;">
	<?php echo $form->submit('保存する');?>
</div>


<?php echo $form->end(); ?>
<?php //echo var_dump($data)?>
