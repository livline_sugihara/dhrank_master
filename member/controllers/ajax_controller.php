<?php
class AjaxController extends AppController {
	var $name = 'Ajax';
	var $uses = array('User','Shop','Girl','Review','ReviewsGoodCount','BookmarkShop','BookmarkGirl','BookmarkReview','FollowReviewer','ReviewReply');

	function beforeFilter() {
		parent::beforeFilter();
		$this->layout = false;
		$this->autoRender = false;
	}

	// ブックマークレビュー更新
	function toggle_pickup_review(){

		// FIXME デバッグ
		$this->log('update_pickup_review() : ' . $this->params['review_id'], 'debug');

		$this->uses = null;
		Configure::write('debug',0);

		// ログインしていない場合
		if(empty($this->parent_user)) {
			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 ログインが必要です。',
			));
			return;
		}

		// 権限がない場合
		if($this->parent_user['UsersAuthority']['is_pickup_review'] == 0) {
			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 権限がありません。',
			));
			return;
		}

		// レビュー取得
		$pickup_review = $this->Review->find('first', array(
			'conditions' => array(
				'Review.id' => $this->params['review_id'],
				'Review.user_id' => $this->parent_user['User']['id'],
				'Review.delete_flag <=' => Review::DELETE_FLAG_COMMENT_ONLY,
			)
		));

		// ログインしていない場合
		if(empty($pickup_review)) {
			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 対象の口コミデータがありません',
			));
			return;
		}

		// 既にPickupの場合は解除する
		if($pickup_review['Review']['pickup_shop'] == 1) {

			// 更新する
			$this->Review->create();
			$this->Review->id = $pickup_review['Review']['id'];
			$pickup_review['Review']['pickup_shop'] = 0;
			$this->Review->save($pickup_review, false, array('pickup_shop'));

			// echo '<img src="/images' . $this->device_path . '/bookmark_off.png" />';
			echo json_encode(array(
				'success' => true,
				'flag' => false,
				'message' => 'Pickupを解除しました。',
			));
			return;
		} else {

			// 更新する
			$this->Review->create();
			$this->Review->id = $pickup_review['Review']['id'];
			$pickup_review['Review']['pickup_shop'] = 1;
			$this->Review->save($pickup_review, false, array('pickup_shop'));

			// echo '<img src="/images' . $this->device_path . '/bookmark_off.png" />';
			echo json_encode(array(
				'success' => true,
				'flag' => true,
				'message' => 'Pickupに追加しました。',
			));
			return;
		}

		// 通常はありえない
		echo json_encode(array(
			'success' => false,
			'message' => '【エラー】 登録が完了できませんでした',
		));
		return;
	}

	// クチコミ削除
	function delete_review(){

		// FIXME デバッグ
		$this->log('delete_review() : ' . $this->params['review_id'], 'debug');

		$this->uses = null;
		Configure::write('debug',0);

		// ログインしていない場合
		if(empty($this->parent_user)) {
			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 ログインが必要です。',
			));
			return;
		}

		// 権限がない場合
		if($this->parent_user['UsersAuthority']['is_delete_review'] == 0) {
			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 権限がありません。',
			));
			return;
		}

		// レビュー取得
		$delete_review = $this->Review->find('first', array(
			'conditions' => array(
				'Review.user_id' => $this->parent_user['User']['id'],
				'Review.id' => $this->params['review_id'],
				'Review.delete_flag <=' => Review::DELETE_FLAG_COMMENT_ONLY,
			)
		));

		// ログインしていない場合
		if(empty($delete_review)) {
			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 対象の口コミデータがありません',
			));
			return;
		}

		// 更新する
		$this->Review->create();
		$this->Review->id = $delete_review['Review']['id'];
		$delete_review['Review']['delete_flag'] = Review::DELETE_FLAG_SAVE_POINTS;
		$this->Review->save($delete_review, false, array('delete_flag'));

		// echo '<img src="/images' . $this->device_path . '/bookmark_off.png" />';
		echo json_encode(array(
			'success' => true,
			'message' => '口コミを削除しました',
		));
		return;
	}

	// 口コミに返信
	function reply_review()
	{
		// FIXME デバッグ
		$this->log('reply_review() : ' . $this->params['review_id'], 'debug');
		$this->uses = null;
		Configure::write('debug',0);


		// ログインしていない場合
		if(empty($this->parent_user)) {
			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 ログインが必要です。',
			));
			return;
		}

		// 権限がない場合
		if($this->parent_user['UsersAuthority']['is_delete_review'] == 0) {
			echo json_encode(array(
				'success' => false,
				'message' => '【エラー】 権限がありません。',
			));
			return;
		}

		// 口コミ返信テーブルにデータを書き込む
		$params_arr = array(
			'review_id' => $this->params['review_id'],
			'reply_flg' => $this->params['pass'][0],
			'comment' => $this->params['form']['reply_review'],
		);
		// review_repliesテーブルに、データがすでに存在した場合は更新
		$review_reply = $this->ReviewReply->find('first', array(
			'conditions' => array(
				'ReviewReply.review_id' => $this->params['review_id'],
				'ReviewReply.reply_flg' => $this->params['pass'][0],
			)
		));
		// 結果がなければfalse。
		if ($review_reply !== false) $params_arr['id'] = $review_reply['ReviewReply']['id'];
		$this->ReviewReply->save($params_arr, false, array('review_id', 'reply_flg','comment'));
		echo json_encode(array(
			'success' => true,
			'message' => '口コミに返信を行いました',
		));
		return;
	}

}
?>