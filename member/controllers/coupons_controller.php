<?php
class CouponsController extends AppController {
	var $name = 'Coupons';
	var $uses = array('Coupon','User');
	var $components = array('Auth','CompFile');

	function beforeFilter() {
		parent::beforeFilter();

		// パンくずに基本情報を設定
		$this->addBreadCrumbs("クーポン管理" , "/coupons");

		switch($this->params['action'])
		{
			case 'edit':
			case 'editend':
				$this->addBreadCrumbs("編集" , "/coupons/edit/" . $this->params['pass'][0]);
				break;
		}
	}

	function index()
	{
		//送信ボタン押下時
		if (!empty($this->data)) {
			
			$this->data['Coupon']['user_id'] = $this->Auth->user('id');
			$this->Coupon->set($this->data);

			if($this->Coupon->validates())
			{
				//データ保存処理
				$this->Coupon->save($this->data);
				$this->redirect($this->device_path . '/coupons/addend');
			}

		} else {
			// 有効期限のデフォルト値を設定
			list($year, $month, $day) = explode('-', date('Y-m-d'));
			$this->data['Coupon']['expire_date_year'] = $year;
			$this->data['Coupon']['expire_date_month'] = $month;
			$this->data['Coupon']['expire_date_day'] = $day;
		}

		// クーポンリストを取得
		$this->set('list', $this->Coupon->find('all', array(
				'conditions' => array(
					'Coupon.user_id' => $this->Auth->user('id')),
					'order' => 'Coupon.expire_unlimited ASC, Coupon.expire_date DESC, Coupon.created DESC')));

		// 対象リスト
		$this->set('m_target', Coupon::$targetList);
		$this->set('m_target_caption', Coupon::$targetCaptionList);

		// 年月日リスト
		$this->setYMDList();

		//head
		$this->set('title_for_layout', $this->title_tag_common.'クーポン管理');
		$this->set('header_one', $this->h1_tag_common.'クーポン管理');
	}

	//追加完了画面
	function addend(){
		//head
		$this->set('title_for_layout', $this->title_tag_common.'クーポン追加完了');
		$this->set('header_one', $this->h1_tag_common.'クーポン追加完了');
	}

	//編集完了画面
	function edit($param)
	{
		//存在しない女の子の場合はエラー画面へ
		$present = $this->Coupon->find('count', array('conditions' => array(
			'Coupon.user_id' => $this->Auth->user('id'),
			'Coupon.id' => $param
		)));

		if($present == 0){
			$this->cakeError('error404');
		}

		//送信ボタン押下時
		if (!empty($this->data)) {
			
			$this->data['Coupon']['user_id'] = $this->Auth->user('id');
			$this->Coupon->set($this->data);

			if($this->Coupon->validates())
			{
				//データ保存処理
				$this->Coupon->save($this->data);
				$this->redirect($this->device_path . '/coupons/editend/' . $this->data['Coupon']['id']);
			}

		} else {
			$this->data = $this->Coupon->findbyId($param);
		}

		// 対象リスト
		$this->set('m_target', Coupon::$targetList);

		// 年月日リスト
		$this->setYMDList();

		// データ
		$this->set('data', $this->data);
		//head
		$this->set('title_for_layout', $this->title_tag_common.'クーポン編集');
		$this->set('header_one', $this->h1_tag_common.'クーポン編集');
	}

	//編集完了画面
	function editend(){
		//head
		$this->set('title_for_layout', $this->title_tag_common.'クーポン編集完了');
		$this->set('header_one', $this->h1_tag_common.'クーポン編集完了');
	}

	// 削除
	function delete($param)
	{
		//存在しない女の子の場合はエラー画面へ
		$present = $this->Coupon->find('count',array('conditions' => array(
			'Coupon.user_id' => $this->Auth->user('id'),
			'Coupon.id' => $param
		)));

		if($present == 0){
			$this->cakeError('error404');
		}

		//女の子削除
		$this->Coupon->delete($param);
		$this->redirect($this->device_path . '/coupons/deleteend');

		//head
//		$this->set('title_for_layout', $this->title_tag_common.'クーポン削除');
//		$this->set('header_one', $this->h1_tag_common.'クーポン削除');
	}

	function deleteend() {
		//head
		$this->set('title_for_layout', $this->title_tag_common.'クーポン削除完了');
		$this->set('header_one', $this->h1_tag_common.'クーポン削除完了');
	}

	// 年月日リストを作成する
	private function setYMDList() {

		$m_year = array();
		$m_month = array();
		$m_day = array();

		for($ii = 1; $ii <= 31; $ii++) {

			$val = ($ii < 10 ? '0' : '') . $ii;

			if($ii <= 3) {
				$y = intval(date('Y')) + $ii - 1;
				$m_year[$y] = $y;
			}
			if($ii <= 12) {
				$m_month[$val] = $ii;
			}

			$m_day[$val] = $ii;
		}
		$this->set('m_year', $m_year);
		$this->set('m_month', $m_month);
		$this->set('m_day', $m_day);
	}
}