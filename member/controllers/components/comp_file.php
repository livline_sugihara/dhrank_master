<?php
class CompFileComponent extends Object
{
	var $uses = array('Folder','File');

	//画像アップロード(圧縮無し)
	function uploadImage($name, $tmp_name, $path) {
		// 画像アップロード
		if (!empty($tmp_name)) {
			$filename = "";
			$imginfo = getimagesize($tmp_name);
			clearstatcache();
			$upload_path = APP_IMG_PATH . $path . DS;
			switch ($imginfo[2]) {
				case 2: // jpeg
					$filename = sprintf("%s.jpg", $name);
					move_uploaded_file($tmp_name, $upload_path . $filename);
					chmod($upload_path . $filename, 0777);
					break;
				case 1: // gif
					$filename = sprintf("%s.gif", $name);
					move_uploaded_file($tmp_name, $upload_path . $filename);
					chmod($upload_path . $filename, 0777);
					break;
				Default:
					break;
			}
			return $filename;
		} else {
			return  false;
		}
	}

	//画像アップロード(圧縮有り)
	function uploadImageCompress($name, $tmp_name, $path, $width, $height) {
		// 画像アップロード
		if (!empty($tmp_name)) {
			$filename = "";
			$imginfo = getimagesize($tmp_name);
			$width_old  = $imginfo[0];
			$height_old = $imginfo[1];
			//start---2013/3/5 障害No.2-0009修正
			if($width_old == 0 || $height_old == 0){
				return  false;
			}
			//end---2013/3/5 障害No.2-0009修正

			//リサイズのサイズを計算
			$resize_width = $width;
			$resize_height = $height;
			//余白を計算
			$margin_width = 0;
			$margin_height = 0;
			if($width / $height > $width_old / $height_old){
				//横をリサイズ
				$resize_width = $height * $width_old / $height_old;
				//横余白
				$margin_width = ($width - $resize_width) / 2;
			}elseif($width / $height < $width_old / $height_old){
				//縦をリサイズ
				$resize_height = $width * $height_old / $width_old;
				//縦余白
				$margin_height = ($height - $resize_height) / 2;
			}

			clearstatcache();
			$upload_path = APP_IMG_PATH . $path . DS;
			switch ($imginfo[2]) {
				case 2: // jpeg
					$filename = sprintf("%s.jpg", $name);
					$jpeg = imagecreatefromjpeg($tmp_name);
					$jpeg_new = imagecreatetruecolor($width, $height);
					imagefill($jpeg_new , 0 , 0 , 0xFFFFFF);
					imagecopyresampled($jpeg_new,$jpeg,$margin_width,$margin_height,0,0,$resize_width,$resize_height,$width_old,$height_old);
					imagejpeg($jpeg_new, $upload_path . $filename, 100);
					chmod($upload_path . $filename, 0777);
					break;
				case 1: // gif
					$filename = sprintf("%s.gif", $name);
					$gif = imagecreatefromgif($tmp_name);
					$gif_new = imagecreatetruecolor($width, $height);
					imagefill($gif_new , 0 , 0 , 0xFFFFFF);
					imagecopyresampled($gif_new,$gif,$margin_width,$margin_height,0,0,$resize_width,$resize_height,$width_old,$height_old);
					imagegif($gif_new, $upload_path . $filename, 100);
					chmod($upload_path . $filename, 0777);
					break;
				Default:
					break;
			}
			return $filename;
		} else {
			return  false;
		}
	}

	// 幅を基準にして圧縮する
	function uploadImageCompressPrecedenceWidth($name, $tmp_name, $path, $width) {

		// 引数チェック
		if (empty($tmp_name)) {
			return false;
		}

		$imginfo = getimagesize($tmp_name);
		$width_image  = $imginfo[0];
		$height_image = $imginfo[1];

		if($width_image == 0 || $height_image == 0){
			return  false;
		}

		// 幅が指定された値の場合、圧縮する必要なし
		if($width == $width_image)
		{
			return $this->uploadImage($name, $tmp_name, $path);
		}

		// 幅を基準にして高さを変更する
		$ratio = $width / $width_image;

		return $this->uploadImageCompress($name, $tmp_name, $path, $width, (int)($height_image * $ratio));
	}

	//LMSサイズ生成
	function uploadImageCompressLMS($name, $tmp_name, $path, $width_l, $height_l, $width_m, $height_m, $width_s, $height_s) {
		// 画像アップロード
		if (!empty($tmp_name)) {
			$imginfo = getimagesize($tmp_name);
			$width_old  = $imginfo[0];
			$height_old = $imginfo[1];
			//start---2013/3/5 障害No.2-0009修正
			if($width_old == 0 || $height_old == 0){
				return  false;
			}
			//end---2013/3/5 障害No.2-0009修正

			clearstatcache();
			$upload_path = APP_IMG_PATH . $path . DS;
			switch ($imginfo[2]) {
				case 2: // jpeg
					$image = imagecreatefromjpeg($tmp_name);
					$extension = 'jpg';
					break;
				case 1: // gif
					$image = imagecreatefromgif($tmp_name);
					$extension = 'gif';
					break;
				Default:
					break;
			}
			//リサイズのサイズを計算
			$resize_width_l = $width_l;
			$resize_height_l = $height_l;
			$resize_width_m = $width_m;
			$resize_height_m = $height_m;
			$resize_width_s = $width_s;
			$resize_height_s = $height_s;
			//余白を計算
			$margin_width_l = 0;
			$margin_height_l = 0;
			$margin_width_m = 0;
			$margin_height_m = 0;
			$margin_width_s = 0;
			$margin_height_s = 0;
			//Lサイズのりサイズ、余白取得
			if($width_l / $height_l > $width_old / $height_old){
				$resize_width_l = $height_l * $width_old / $height_old;
				$margin_width_l = ($width_l - $resize_width_l) / 2;
			}elseif($width_l / $height_l < $width_old / $height_old){
				$resize_height_l = $width_l * $height_old / $width_old;
				$margin_height_l = ($height_l - $resize_height_l) / 2;
			}
			//Mサイズのりサイズ、余白取得
			if($width_m / $height_m > $width_old / $height_old){
				$resize_width_m = $height_m * $width_old / $height_old;
				$margin_width_m = ($width_m - $resize_width_m) / 2;
			}elseif($width_m / $height_m < $width_old / $height_old){
				$resize_height_m = $width_m * $height_old / $width_old;
				$margin_height_m = ($height_m - $resize_height_m) / 2;
			}
			//Sサイズのりサイズ、余白取得
			if($width_s / $height_s > $width_old / $height_old){
				$resize_width_s = $height_s * $width_old / $height_old;
				$margin_width_s = ($width_s - $resize_width_s) / 2;
			}elseif($width_s / $height_s < $width_old / $height_old){
				$resize_height_s = $width_s * $height_old / $width_old;
				$margin_height_s = ($height_s - $resize_height_s) / 2;
			}
			//保存ファイル名
			$filename_l = sprintf("%s_l." . $extension, $name);
			$filename_m = sprintf("%s_m." . $extension, $name);
			$filename_s = sprintf("%s_s." . $extension, $name);
			//新しいイメージを作成
			$image_new_l = imagecreatetruecolor($width_l, $height_l);
			$image_new_m = imagecreatetruecolor($width_m, $height_m);
			$image_new_s = imagecreatetruecolor($width_s, $height_s);
			imagefill($image_new_l , 0 , 0 , 0xFFFFFF);
			imagefill($image_new_m , 0 , 0 , 0xFFFFFF);
			imagefill($image_new_s , 0 , 0 , 0xFFFFFF);
			//新しいイメージにリサンプルしてコピー
			imagecopyresampled($image_new_l,$image,$margin_width_l,$margin_height_l,0,0,$resize_width_l,$resize_height_l,$width_old,$height_old);
			imagecopyresampled($image_new_m,$image,$margin_width_m,$margin_height_m,0,0,$resize_width_m,$resize_height_m,$width_old,$height_old);
			imagecopyresampled($image_new_s,$image,$margin_width_s,$margin_height_s,0,0,$resize_width_s,$resize_height_s,$width_old,$height_old);
			switch ($imginfo[2]) {
				case 2: // jpeg
					imagejpeg($image_new_l, $upload_path . $filename_l, 100);
					imagejpeg($image_new_m, $upload_path . $filename_m, 100);
					imagejpeg($image_new_s, $upload_path . $filename_s, 100);
					break;
				case 1: // gif
					imagegif($image_new_l, $upload_path . $filename_l, 100);
					imagegif($image_new_m, $upload_path . $filename_m, 100);
					imagegif($image_new_s, $upload_path . $filename_s, 100);
					break;
				Default:
					break;
			}
			chmod($upload_path . $filename_l, 0777);
			chmod($upload_path . $filename_m, 0777);
			chmod($upload_path . $filename_s, 0777);
			return array($filename_l, $filename_m, $filename_s);
		} else {
			return  false;
		}
	}

	function makeFolder($path){
		$folder = APP_IMG_PATH . $path ;
		if( !file_exists($folder) ){
			mkdir($folder, 0777);
			chmod($folder, 0777);
		}
	}
	function deleteFolder($path){
		$folder = APP_IMG_PATH . $path ;
		if( file_exists($folder) ){
			$f = new Folder();
			$f->delete($folder);
		}
	}
	function deleteFile($path){
		$file = APP_IMG_PATH . $path ;
		if( file_exists($file) ){
			$f = new File($file);
			$f->delete();
		}
	}

	//LMSのサイズ調整した画像の削除関数
	function deleteFileLMS($name, $path){
		$delete_path = APP_IMG_PATH . $path . DS ;
		$filename = array(
				sprintf("%s_l.jpg", $name),
				sprintf("%s_m.jpg", $name),
				sprintf("%s_s.jpg", $name),
				sprintf("%s_l.gif", $name),
				sprintf("%s_m.gif", $name),
				sprintf("%s_s.gif", $name),
		);
		foreach($filename as $r_file){
			if( file_exists($delete_path . $r_file) ){
				$f = new File($delete_path . $r_file);
				$f->delete();
			}
		}
	}

}