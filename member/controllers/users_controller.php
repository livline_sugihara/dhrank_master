<?php
class UsersController extends AppController {
	var $name = 'Users';
	var $uses = array('Changepassword','Shop','Passport');
	var $components = array('Cookie','Email');
	public $expires = 2592000; //60 * 60 * 24 * 30

	function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow($this->device_file_name . 'changepassword_send');
		$this->Auth->allow($this->device_file_name . 'changepassword_send_end');
		$this->Auth->allow($this->device_file_name . 'changepassword_input');
		$this->Auth->allow($this->device_file_name . 'changepassword_input_end');
		$this->Auth->allow($this->device_file_name . 'error');

		// パンくずに基本情報を設定
		$this->addBreadCrumbs("店舗会員様ログイン" , "/users/login");
	}

	function index(){
	}
	function i_index() {
		$this->index();
	}

	//ログイン
	function login() {
		$this->layout = 'login';

		$user = $this->Auth->user();
		if (!empty($this->data) && !empty($user)){
			//モバイルの場合はパスポート保存処理をしない
			if(!empty($this->data['User']['remember_me'])){
				//パスポートを保存するか
				if ($this->data['User']['remember_me']) {
					$this->passportWrite($user);
				} else {
					$this->passportDelete($user);
				}
				unset($this->data['User']['remember_me']);
			}
		}
		if ($user){
			$this->redirect($this->device_path . '/');
		}else{
			$cookie_user = $this->Cookie->Read('User');

			if ($cookie_user) {
				//パスポートでログイン
				$options = array(
						'conditions' => array(
								'Passport.passport'   => $cookie_user['passport'],
								'Passport.modified >' => date('Y-m-d H:i:s', $this->expires)
						));
				$passport = $this->Passport->find('first', $options);
				if ($passport){
					$user['username'] = $passport['User']['username'];
					$user['password'] = $passport['User']['password'];

					if ($this->Auth->login($user)) {
						$this->passportWrite($passport);
						$this->redirect($this->device_path . '/');
					}
				}
			}
		}
		//head
		$this->set('title_for_layout', $this->title_tag_common.'店舗会員・ログイン');
		$this->set('header_one', $this->h1_tag_common.'店舗会員・ログイン');
	}
	function i_login() {
		$this->login();
	}

	//ログアウト
	function logout() {
		$user = $this->Auth->user();
		$this->passportDelete($user);
		$this->Auth->logout();
		$this->redirect($this->device_path . '/');
	}
	function i_logout() {
		$this->logout();
	}

	//パスワード再設定URL送信画面
	function changepassword_send() {
		$this->layout = 'login';

		// パンくず追加
		$this->addBreadCrumbs("ログインIDとパスワード変更", "/users/changepassword_send");

		if(!empty($this->data)){
			$this->Changepassword->set($this->data);
			if($this->Shop->find('count', array('conditions' => array('Shop.email' => $this->data['Changepassword']['email']))) == 0){
				$this->Changepassword->invalidate('email', 'メールアドレスが正しくありません。');
			}
			if($this->Changepassword->validates()){

				//uid生成
				$uid = '';
				$sCharList = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
				mt_srand();
				for($i = 0; $i < 16; $i++){
					$uid .= $sCharList{mt_rand(0, strlen($sCharList) - 1)};
				}
				//ショップデータ取得
				$shop = $this->Shop->find('first', array('conditions' => array('Shop.email' => $this->data['Changepassword']['email'])));

				//これまでのパスワード変更データを無効にする
				$passwordlist = $this->Changepassword->find('all',  array('conditions' => array(
						'Changepassword.user_id' => $shop['Shop']['user_id'],
						'Changepassword.is_changed' => 0)));
				foreach($passwordlist as $key => $record){
					$passwordlist[$key]['Changepassword']['is_changed'] = 1;
				}
				//空データが登録されるため空チェックを行う
				if(!empty($passwordlist)){
					$this->Changepassword->saveAll($passwordlist);
				}

				//パスワード変更データ保存
				$this->Changepassword->create();
				$this->Changepassword->user_id = $shop['Shop']['user_id'];
				$this->Changepassword->uid = $uid;
				$this->Changepassword->is_changed = 0;
				$this->Changepassword->save($this->Changepassword);

				//保存データ取得
				$id = $this->Changepassword->id;
				$this->Changepassword->create();
				$pass_data = $this->Changepassword->findById($id);

				//メール送信
				$this->Email->to = $this->data['Changepassword']['email'];
				$this->Email->from = VALID_CHANGEPASSWORD_URLSEND_EMAIL;
				$this->Email->subject = 'ID通知・パスワード変更依頼';
				$this->Email->smtpOptions = Configure::read('smtpOptionChangepasswordUrlsend');
				$this->Email->delivery = 'smtp';
				$this->Email->lineLength = 1024;

				$changepassword_input_url = '';
				$terminate_url = 'users/changepassword_input/';
				if(isset($this->params['prefix'])){
					if($this->params['prefix'] == 'i'){
						$terminate_url = 'i/users/changepassword_input/';
					}
				}
				$changepassword_input_url = APP_MEMBER_URL .
				$terminate_url .
				$shop['Shop']['user_id'] . '/' .
				date('YmdHis', strtotime($pass_data['Changepassword']['created'])) .
				'/' .
				$uid;

				//本文作成
				$message = array(
						'あなたのログインIDは以下の通りです。',
						$shop['User']['username'],
						'',
						'パスワードを忘れた方は以下のURLからパスワード変更をおこなってください。',
						$changepassword_input_url,
						'なお、このURLは送信されて2時間有効です。',
						'----------------------',
						'このメールアドレスは送信専用です。',
				);
				$this->Email->send($message);

				$this->redirect($this->device_path . '/users/changepassword_send_end');
			}
		}
		//head
		$this->set('title_for_layout', $this->title_tag_common.'ログインIDとパスワード変更');
		$this->set('header_one', $this->h1_tag_common.'ログインIDとパスワード変更');
	}
	function i_changepassword_send() {
		$this->changepassword_send();
	}

	//パスワード再設定URL送信完了画面
	function changepassword_send_end() {
		$this->layout = 'login';

		//head
		$this->set('title_for_layout', $this->title_tag_common.'ログインIDとパスワード変更URL 送信完了画面');
		$this->set('header_one', $this->h1_tag_common.'ログインIDとパスワード変更URL 送信完了画面');
	}
	function i_changepassword_send_end() {
		$this->changepassword_send_end();
	}

	//パスワード再設定画面
	function changepassword_input($param_user_id = null, $param_time = null, $param_uid = null) {
		$this->layout = 'login';


		if (!empty($this->data)){
			$this->User->set($this->data);
			if($this->User->validates()){
				//パスワード保存
				$this->data['User']['password'] = $this->data['User']['new_password'];
				$this->User->saveField('password',$this->Auth->password($this->data['User']['new_password']));

				//本URLを無効に設定
				$pass_data = $this->Changepassword->find('first', array('conditions' => array('Changepassword.id' => $this->data['Changepassword']['id'])));
				$pass_data['Changepassword']['is_changed'] = 1;
				$this->Changepassword->save($pass_data);

				$shop = $this->User->findById($this->data['User']['id']);
				//メール送信（店舗）
				$this->Email->to = $shop['Shop']['email'];
				$this->Email->from = VALID_CHANGEPASSWORD_END_EMAIL;
				$this->Email->subject = '【デリヘル人気口コミランキング】パスワードの変更を行いました。';
				$this->Email->smtpOptions = Configure::read('smtpOptionChangepasswordEnd');
				$this->Email->delivery = 'smtp';
				$this->Email->lineLength = 1024;

				$message = array(
						'【デリヘル人気口コミランキング】パスワードの変更を行いました。',
						'新しいパスワードは以下になります。大切に保管してください。',
						'新しいパスワード：' . $this->data['User']['new_password'],
						'----------------------',
						'このメールアドレスは送信専用です。',
				);
				$this->Email->send($message);

				//メール送信（総合管理者）
				$this->Email->to = VALID_CHANGEPASSWORD_END_EMAIL;
				$this->Email->from = VALID_CHANGEPASSWORD_END_EMAIL;
				$this->Email->subject = '店舗パスワード変更通知';
				$this->Email->smtpOptions = Configure::read('smtpOptionChangepasswordEnd');
				$this->Email->delivery = 'smtp';
				$this->Email->lineLength = 1024;

				$message = array(
						'以下店舗がパスワードの変更を行いました。',
						'店舗ID：' . $this->data['User']['id'],
						'店名：' . $shop['Shop']['name'],
						'大エリア：' . $shop['LargeArea']['name'],
						'新しいパスワード：' . $this->data['User']['new_password'],
				);
				$this->Email->send($message);

				$this->redirect($this->device_path . '/users/changepassword_input_end');
			}
		} else {
			//URL検証
			$pass_data = $this->Changepassword->find('first', array('conditions' => array(
					'Changepassword.user_id' => $param_user_id,
					'Changepassword.uid' => $param_uid,
					'Changepassword.is_changed' => 0)));
			if((empty($pass_data)) ||
					(date('YmdHis', strtotime($pass_data['Changepassword']['created'])) != $param_time) ||
					(strtotime($pass_data['Changepassword']['created']) < strtotime('-2 hour'))){
				$this->redirect($this->device_path . '/users/error');
			}

			$this->User->id = $param_user_id;
			$this->data = $this->User->read();
			$this->data['Changepassword']['id'] = $pass_data['Changepassword']['id'];
		}
		$this->set('data',$this->data);
		//head
		$this->set('title_for_layout', $this->title_tag_common.'パスワード変更画面');
		$this->set('header_one', $this->h1_tag_common.'パスワード変更画面');
	}
	function i_changepassword_input($param_user_id = null, $param_time = null, $param_uid = null) {
		$this->changepassword_input($param_user_id, $param_time, $param_uid);
	}

	//パスワード再設定完了画面
	function changepassword_input_end() {
		$this->layout = 'login';
		//head
		$this->set('title_for_layout', $this->title_tag_common.'パスワード再設定完了');
		$this->set('header_one', $this->h1_tag_common.'パスワード再設定完了');
	}
	function i_changepassword_input_end() {
		$this->changepassword_input_end();
	}

	//エラー画面
	function error() {
		$this->layout = 'login';
		//head
		$this->set('title_for_layout', $this->title_tag_common.'パスワード変更 エラー画面');
		$this->set('header_one', $this->h1_tag_common.'パスワード変更 エラー画面');
	}
	function i_error() {
		$this->error();
	}

	/* パスポート発行 */
	private function passportWrite($user){
		$passport = array();
		$passport['user_id'] = $user['User']['id'];
		$passport['passport'] = Security::generateAuthKey();

		if (isset($user['Passport']['id'])) {
			$passport['id'] = $user['Passport']['id'];
		}
		$this->Passport->save($passport);

		$cookie = array(
				'passport' => $passport['passport']
		);
		$this->Cookie->write('User', $cookie, true, $this->expires);
	}

	/* パスポート削除 */
	private function passportDelete($user){
		$this->Cookie->delete('User');

		$conditions = array(
				'Passport.user_id' => $user['User']['id']
		);
		$this->Passport->deleteAll($conditions);
	}
}
?>