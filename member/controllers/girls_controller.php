<?php
class GirlsController extends AppController {
	var $name = 'Girls';
	var $uses = array('Girl','User','MNumericValue','MGirlsCup','Review','ReviewsGoodCount','BookmarkGirl','MGirlsRecommendType');
	var $components = array('Auth','CompFile','RequestHandler');

	function beforeFilter() {
		parent::beforeFilter();

		// パンくずに基本情報を設定
		$this->addBreadCrumbs("女の子管理" , "/girls");

		switch($this->params['action'])
		{
			case 'index':
				break;
			case 'add':
			case 'addend':
				$this->addBreadCrumbs("登録" , "/girls/add");
				break;
			case 'edit':
			case 'editend':
				$this->addBreadCrumbs("編集" , "/girls/edit/" . $this->params['pass'][0]);
				break;
			case 'deleteend':
				$this->addBreadCrumbs("削除" , "#");
				break;
			case 'order':
			case 'orderend':
				$this->addBreadCrumbs("表示順位編集" , "/girls/order");
				break;

		}
	}

	// 女の子一覧
	function index()
	{
		//レビュー情報BIND
		$this->Girl->bindModel(array('hasOne' => array(
			'Review' => array(
				'className'=>'Review',
				'fields' => array('ifnull(count(Review.id),0) as count'),
				'conditions' => array('Review.girl_id = Girl.id'),
				'foreignKey' => false,
			)
		)),false);
		$this->set('rec_list', $this->Girl->find('all', array(
				'conditions' => array(
					'Girl.user_id' => $this->Auth->user('id'),
					'Girl.is_deleted' => 0,
					'NOT' => array('Girl.recommend_type_id' => 0)
				),
				'group' => 'Girl.id',
				'order' => 'Girl.show_order ASC, Girl.created ASC')));
		$this->set('list', $this->Girl->find('all', array(
				'conditions' => array(
					'Girl.user_id' => $this->Auth->user('id'),
					'Girl.is_deleted' => 0,
					'Girl.recommend_type_id' => 0
				),
				'group' => 'Girl.id',
				'order' => 'Girl.show_order ASC, Girl.created ASC')));
		//head
		$this->set('title_for_layout', $this->title_tag_common.'女の子一覧');
		$this->set('header_one', $this->h1_tag_common.'女の子一覧');
	}

	// 女の子登録
	function add()
	{
		// 登録処理時
		if (!empty($this->data))
		{
			$this->Girl->set($this->data);

			// 激押し売れっ子にチェックが入っている場合、お勧め内容は必須。それ以外はなし
			if(empty($this->data['Girl']['recommend_check'])) {
				unset($this->Girl->validate['recommend_type_id']);
			}

			if($this->Girl->validates())
			{
				$this->Girl->user_id = $this->parent_user['User']['id'];
				$this->data['Girl']['show_order'] = 10000;
				$this->Girl->save($this->data);
				//フォルダ作成
				//start---2013/4/25 障害No.4-0001修正
				//$this->CompFile->makeFolder('girl' . DS . $this->Girl->id);
				$this->CompFile->makeFolder('girl' . DS . $this->parent_user['User']['id']);
				$this->CompFile->makeFolder('girl' . DS . $this->parent_user['User']['id'] . DS . $this->Girl->id);
				//end---2013/4/25 障害No.4-0001修正

				//女の子画像処理
				for($i = 0; $i < 4; $i++)
				{
					if(!empty($this->data['Girl']['image2_' . (string)($i+1)]['tmp_name']))
					{
						//画像アップロード(1.ファイル名(extなし),2.tmp_name,3.path)
						$upload_name = $this->CompFile->uploadImageCompressLMS(
								(string)($i+1),
								$this->data['Girl']['image2_' . (string)($i+1)]['tmp_name'],
								//start---2013/4/25 障害No.4-0001修正
								//'girl' . DS . $this->Girl->id,
								'girl' . DS . $this->parent_user['User']['id'] . DS . $this->Girl->id,
								//end---2013/4/25 障害No.4-0001修正
								APP_IMG_GIRL_L_WIDTH,APP_IMG_GIRL_L_HEIGHT,
								APP_IMG_GIRL_M_WIDTH,APP_IMG_GIRL_M_HEIGHT,
								APP_IMG_GIRL_S_WIDTH,APP_IMG_GIRL_S_HEIGHT
						);
						if(!empty($upload_name[0]) && !empty($upload_name[1]) && !empty($upload_name[2])){
							$this->data_update['Girl']['image_' . (string)($i+1) . '_l'] = $upload_name[0];
							$this->data_update['Girl']['image_' . (string)($i+1) . '_m'] = $upload_name[1];
							$this->data_update['Girl']['image_' . (string)($i+1) . '_s'] = $upload_name[2];
						}
					}
				}
				$this->data_update['Girl']['id'] = $this->Girl->id;
				$this->Girl->save($this->data_update);
				$this->redirect($this->device_path . '/girls/addend/' . $this->Girl->id);
			}
		}


		$numeric_values = $this->MNumericValue->find('all');
		$this->set('m_age',Set::Combine(Set::extract('/MNumericValue[id>=18][id<=99]', $numeric_values),'{n}.MNumericValue.id', '{n}.MNumericValue.value'));
		$this->set('m_body_height',Set::Combine(Set::extract('/MNumericValue[id>=130][id<=200]', $numeric_values),'{n}.MNumericValue.id', '{n}.MNumericValue.value'));
		$this->set('m_bust',Set::Combine(Set::extract('/MNumericValue[id>=70][id<=150]', $numeric_values),'{n}.MNumericValue.id', '{n}.MNumericValue.value'));
		$this->set('m_waist',Set::Combine(Set::extract('/MNumericValue[id>=50][id<=150]', $numeric_values),'{n}.MNumericValue.id', '{n}.MNumericValue.value'));
		$this->set('m_hip',Set::Combine(Set::extract('/MNumericValue[id>=70][id<=150]', $numeric_values),'{n}.MNumericValue.id', '{n}.MNumericValue.value'));
		$this->set('m_girls_cups',Set::Combine($this->MGirlsCup->find('all'),'{n}.MGirlsCup.id', '{n}.MGirlsCup.name'));
		$this->set('m_girls_recommend_types',Set::Combine($this->MGirlsRecommendType->find('all'),'{n}.MGirlsRecommendType.id', '{n}.MGirlsRecommendType.name'));

		$this->data['Girl']['user_id'] = $this->Auth->user('id');
		$this->set('data',$this->data);
		//head
		$this->set('title_for_layout', $this->title_tag_common.'女の子登録');
		$this->set('header_one', $this->h1_tag_common.'女の子登録');
	}

	function addend($param) {

		//女の子データ
		$girl = $this->Girl->findbyId($param);
		$this->set('girl',$girl);
		//head
		$this->set('title_for_layout', $this->title_tag_common.'女の子登録完了');
		$this->set('header_one', $this->h1_tag_common.'女の子登録完了');
	}

	//女の子編集
	function edit($param)
	{
		//存在しない女の子の場合はエラー画面へ
		$present = $this->Girl->find('count', array('conditions' => array(
			'Girl.id' => $param,
			'Girl.user_id' => $this->Auth->user('id'),
			'Girl.is_deleted' => 0,
		)));

		if($present == 0){
			$this->cakeError('error404');
		}

		if (!empty($this->data))
		{
			$this->Girl->set($this->data);

			// 激押し売れっ子にチェックが入っている場合、お勧め内容は必須。それ以外はなし
			if(empty($this->data['Girl']['recommend_check'])) {
				unset($this->Girl->validate['recommend_type_id']);
			}

			if($this->Girl->validates()){
				//フォルダ作成
				//start---2013/4/25 障害No.4-0001修正
				//$this->CompFile->makeFolder('girl' . DS . $this->Girl->id);
				$this->CompFile->makeFolder('girl' . DS . $this->parent_user['User']['id']);
				$this->CompFile->makeFolder('girl' . DS . $this->parent_user['User']['id'] . DS . $this->Girl->id);
				//end---2013/4/25 障害No.4-0001修正

				// 画像ファイル処理
				for($i = 0; $i < 4; $i++){
					//女の子画像削除（アップロード前に削除処理を行う）
					if(!empty($this->data['Girl']['delete_image_' . (string)($i+1)]) && $this->data['Girl']['delete_image_' . (string)($i+1)] == true){
						$this->CompFile->deleteFileLMS((string)($i+1),
								//start---2013/4/25 障害No.4-0001修正
								//'girl' . DS . $this->Girl->id
								'girl' . DS . $this->parent_user['User']['id'] . DS . $this->Girl->id
								//end---2013/4/25 障害No.4-0001修正
								);
						$this->data['Girl']['image_' . (string)($i+1) . '_l'] = null;
						$this->data['Girl']['image_' . (string)($i+1) . '_m'] = null;
						$this->data['Girl']['image_' . (string)($i+1) . '_s'] = null;
					}
					//女の子画像処理
					else if(!empty($this->data['Girl']['image2_' . (string)($i+1)]['tmp_name']) ){
						//画像アップロード(1.ファイル名(extなし),2.tmp_name,3.path)
						$upload_name = $this->CompFile->uploadImageCompressLMS(
								(string)($i+1),
								$this->data['Girl']['image2_' . (string)($i+1)]['tmp_name'],
								//start---2013/4/25 障害No.4-0001修正
								//'girl' . DS . $this->Girl->id,
								'girl' . DS . $this->parent_user['User']['id'] . DS . $this->Girl->id,
								//end---2013/4/25 障害No.4-0001修正
								APP_IMG_GIRL_L_WIDTH,APP_IMG_GIRL_L_HEIGHT,
								APP_IMG_GIRL_M_WIDTH,APP_IMG_GIRL_M_HEIGHT,
								APP_IMG_GIRL_S_WIDTH,APP_IMG_GIRL_S_HEIGHT
						);
						if(!empty($upload_name[0]) && !empty($upload_name[1]) && !empty($upload_name[2])){
							$this->data['Girl']['image_' . (string)($i+1) . '_l'] = $upload_name[0];
							$this->data['Girl']['image_' . (string)($i+1) . '_m'] = $upload_name[1];
							$this->data['Girl']['image_' . (string)($i+1) . '_s'] = $upload_name[2];
						}
					}
				}
				$this->Girl->save($this->data);
				$this->redirect($this->device_path . '/girls/editend/' . $this->Girl->id);
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Girl->findbyId($param);
		}

		$numeric_values = $this->MNumericValue->find('all');
		$this->set('m_age',Set::Combine(Set::extract('/MNumericValue[id>=18][id<=99]', $numeric_values),'{n}.MNumericValue.id', '{n}.MNumericValue.value'));
		$this->set('m_body_height',Set::Combine(Set::extract('/MNumericValue[id>=130][id<=200]', $numeric_values),'{n}.MNumericValue.id', '{n}.MNumericValue.value'));
		$this->set('m_bust',Set::Combine(Set::extract('/MNumericValue[id>=70][id<=150]', $numeric_values),'{n}.MNumericValue.id', '{n}.MNumericValue.value'));
		$this->set('m_waist',Set::Combine(Set::extract('/MNumericValue[id>=50][id<=150]', $numeric_values),'{n}.MNumericValue.id', '{n}.MNumericValue.value'));
		$this->set('m_hip',Set::Combine(Set::extract('/MNumericValue[id>=70][id<=150]', $numeric_values),'{n}.MNumericValue.id', '{n}.MNumericValue.value'));
		$this->set('m_girls_cups',Set::Combine($this->MGirlsCup->find('all'),'{n}.MGirlsCup.id', '{n}.MGirlsCup.name'));
		$this->set('m_girls_recommend_types',Set::Combine($this->MGirlsRecommendType->find('all'),'{n}.MGirlsRecommendType.id', '{n}.MGirlsRecommendType.name'));
		$this->set('data',$this->data);
		//head
		$this->set('title_for_layout', $this->title_tag_common.'女の子編集');
		$this->set('header_one', $this->h1_tag_common.'女の子編集');
	}

	function editend($param) {

		//女の子データ
		$girl = $this->Girl->findbyId($param);
		$this->set('girl',$girl);
		//head
		$this->set('title_for_layout', $this->title_tag_common.'女の子編集完了');
		$this->set('header_one', $this->h1_tag_common.'女の子編集完了');
	}

	function delete($param)
	{
		//存在しない女の子の場合はエラー画面へ
		$present = $this->Girl->find('count', array(
			'conditions' => array(
				'Girl.id' => $param,
				'Girl.user_id' => $this->Auth->user('id'),
				'Girl.is_deleted' => 0,
			)
		));
		if($present == 0){
			$this->cakeError('error404');
		}

		// 画像フォルダ削除
		$this->CompFile->deleteFolder('girl' . DS . $this->parent_user['User']['id'] . DS . $param);

/*
		// 口コミ削除
		$reviews = $this->Review->find('all',array(
			'fields' => 'id',
			'conditions' => array('Review.girl_id' => $param)
		));

		foreach($reviews as $review){
			//投票削除
			$this->ReviewsGoodCount->deleteAll(array('ReviewsGoodCount.review_id' => $review['Review']['id']));
		}
		$this->Review->deleteAll(array('Review.girl_id' => $param));
*/
		//ブックマーク削除
		$this->BookmarkGirl->deleteAll(array('BookmarkGirl.girl_id' => $param));

		//女の子論理削除
		$this->Girl->create();
		$this->Girl->id = $param;
		$data['Girl']['is_deleted'] = 1;
		$this->Girl->save($data, false, array('is_deleted'));
		$this->redirect($this->device_path . '/girls/deleteend');

		//head
		$this->set('title_for_layout', $this->title_tag_common.'女の子削除');
		$this->set('header_one', $this->h1_tag_common.'女の子削除');
	}

	function deleteall(){

		if ($this->RequestHandler->isAjax()) {
			$this->autoRender = false;

			$posts = $_POST;

			//$data = $this->data['Girl'];

			//foreach($data['id'] as $id)
			foreach($posts as $k => $id)
			{
				if($id !== '0')
				{
					$data['id'] = $id;
					$data['is_deleted'] = 1;
					$this->Girl->save($data);
				}
			}
		}
		$this->redirect('/girls');
	}

	function changeall(){
		$this->autoRender = false;

		$girls = $this->params['form'];
//var_dump($this->params);exit;
		foreach($girls as $id => $index)
		{
			$data['id'] = $id;
			$data['show_order'] = $index+1;
			$this->Girl->save($data);
		}

		return var_dump($data);
	}

	function picall(){
		$this->autoRender = false;
		$posts = $_POST;
//pr($posts);pr($_FILES);exit;
		$img_names = $_FILES['image_1_m']['name'];
		$files = $_FILES['image_1_m']['tmp_name'];
		$girls = $posts['g_id'];

		// 正しく取れているかチェック
		$flag = (count($img_names) == count($files) && count($files) == count($girls)) ? true : false;
		if($flag == false)
		{
			$this->redirect('index');
		}
		else
		{
			for($i=0; $i<count($girls); $i++)
			{
				//まずはsave
				$data['id'] = $girls[$i];
				$data['image_1_m'] = $img_names[$i];
				// $this->Girl->save($data);
				//続いて画像保存
				$target = $this->Girl->find('all', array(
					'conditions' => array('Girl.id' => $data['id']),
					'fields' => array('Girl.id', 'Girl.user_id')
				));
//				var_dump($target);exit;
				move_uploaded_file($files,APP_IMG_URL,'girl/'. $target[0]['Girl']['user_id']. '/' .$target[0]['Girl']['id']);

			}


		}

		$this->redirect('index');


	}


	function deleteend() {
		//head
		$this->set('title_for_layout', $this->title_tag_common.'女の子削除完了');
		$this->set('header_one', $this->h1_tag_common.'女の子削除完了');
	}

	// 表示順位編集
	function order()
	{
		if (!empty($this->data)){
			foreach ($this->data['Girl'] as $key => $data) {
				if(empty($data['show_order'])) {
					$data['show_order'] = Girl::SHOW_ORDER_NEW;
				}
				$this->Girl->save($data);
			}

			$this->redirect($this->device_path . '/girls/orderend');
		}
		$list = $this->Girl->find('all', array(
			'conditions' => array(
				'Girl.user_id' => $this->Auth->user('id'),
				'Girl.is_deleted' => 0,
			),
			'order' => 'Girl.show_order ASC, Girl.created ASC'));

		$ii = 1;
		foreach($list AS $key => $record) {
			$list[$key]['Girl']['show_order'] = $ii++;
		}

		$this->set('list', $list);

		$cnt = count($list);
		// $numeric_values = $this->MNumericValue->find('all');
		// $this->set('m_show_order',Set::Combine(Set::extract('/MNumericValue[id>=1][id<=' . $cnt . ']', $numeric_values),'{n}.MNumericValue.id', '{n}.MNumericValue.value'));
		$m_show_order = array();
		for($ii = 1; $ii <= $cnt; $ii++)
		{
			$m_show_order[$ii] = $ii;
		}
		$this->set('m_show_order', $m_show_order);

		//head
		$this->set('title_for_layout', $this->title_tag_common.'女の子表示順位編集');
		$this->set('header_one', $this->h1_tag_common.'女の子表示順位編集');
	}

	function orderend() {

		//head
		$this->set('title_for_layout', $this->title_tag_common.'女の子表示順位編集完了');
		$this->set('header_one', $this->h1_tag_common.'女の子表示順位編集完了');

	}
}
?>
