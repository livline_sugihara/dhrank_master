<?php
class SystemsController extends AppController {
	var $name = 'Systems';
	var $uses = array('System','User');
	var $components = array('Auth','CompFile');

	function beforeFilter() {
		parent::beforeFilter();

		// パンくずに基本情報を設定
		$this->addBreadCrumbs("システム管理" , "/systems");

/*
		switch($this->params['action'])
		{
			case 'index':
			case 'editend':
				$this->addBreadCrumbs("登録" , "/girls/add");
				break;
		}
*/
	}

	function index()
	{
		//送信ボタン押下時
		if (!empty($this->data)) {
			$this->data['System']['user_id'] = $this->Auth->user('id');			$this->data['System']['price_pattern'] = implode(',', $this->data['System']['price_pattern']);			$this->System->set($this->data);
			if($this->System->validates())
			{
				//データ保存処理
				$this->System->save($this->data);
				$this->redirect($this->device_path . '/systems/editend');
			}

		} else {
			$this->data = $this->System->findbyUserId($this->Auth->user('id'));
			$this->data['System']['user_id'] = $this->Auth->user('id');			$this->data['System']['price_pattern'] = explode(',', $this->data['System']['price_pattern']);			// 営業時間を変換
			// FIXME modelに追加スべき？？
			if(!empty($this->data['System']['open_time'])) {
				list($this->data['System']['open_time_hour'], $this->data['System']['open_time_minute']) = explode(':', $this->data['System']['open_time']);
			}
			if(!empty($this->data['System']['close_time'])) {
				list($this->data['System']['close_time_hour'], $this->data['System']['close_time_minute']) = explode(':', $this->data['System']['close_time']);
			}
		}

		//営業時間セレクトボックス
		$m_hour = array();
		$m_minute = array();
		for($ii = 0; $ii < 60; $ii++) {
			$val = ($ii < 10 ? '0' : '') . $ii;

			if(0 <= $ii && $ii <= 23) {
				$m_hour[$val] = $val;
			}
			if(0 <= $ii && $ii <= 55 && ($ii % 5 == 0)) {
				$m_minute[$val] = $val;
			}
		}
		$this->set('m_hour', $m_hour);
		$this->set('m_minute', $m_minute);
		//表示用
		$this->set('data',$this->data);

		//head
		$this->set('title_for_layout', $this->title_tag_common.'システム管理');
		$this->set('header_one', $this->h1_tag_common.'システム管理');
	}
	function i_index() {
		$this->index();
	}

	//編集完了画面
	function editend(){
		//head
		$this->set('title_for_layout', $this->title_tag_common.'システム編集完了');
		$this->set('header_one', $this->h1_tag_common.'システム編集完了');
	}
	function i_editend() {
		$this->editend();
	}
}
