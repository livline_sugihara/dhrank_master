<?php
class ShopsController extends AppController {
	var $name = 'Shops';
	var $uses = array('Shop','User','MShopsBusinessCategory', 'ShopInfo', 'LargeArea', 'SmallArea');
	var $components = array('Auth','CompFile');

	function beforeFilter() {
		parent::beforeFilter();

		// パンくずに基本情報を設定
		$this->addBreadCrumbs("店舗情報管理" , "/shops");

/*
		switch($this->params['action'])
		{
			case 'index':
			case 'editend':
				$this->addBreadCrumbs("登録" , "/girls/add");
				break;
		}
*/
	}

	function index(){

		//送信ボタン押下時
		if (!empty($this->data)){
//    pr($this->data);exit;
			$this->Shop->set($this->data['Shop']);
			$this->ShopInfo->set($this->data['ShopInfo']);
			//ランキング画像バリデート
			$this->Shop->imageSizeValidate($this->data['Shop']);
			if($this->Shop->validates()){

				// 保存前のデータを取得
				$current_data = $this->Shop->findbyUserId($this->Auth->user('id'));

				//保存フォルダ
				$savefolder = 'shop' . DS . $this->parent_user['Shop']['user_id'];

				//フォルダ作成
				$this->CompFile->makeFolder($savefolder);

				foreach(Shop::$imageFieldsKey AS $fieldkey => $imagesize)
				{
					// 画像削除（削除チェックが入っているものに関して、削除処理を行う）
					if(!empty($this->data['Shop'][$fieldkey . '_delete']) && $this->data['Shop'][$fieldkey . '_delete'] == true) {
						$this->CompFile->deleteFile($savefolder . DS . $current_data['Shop'][$fieldkey]);
						$this->data['Shop'][$fieldkey] = null;

						// FIXME デバッグ
						$this->log('field[' . $fieldkey . ']の画像[' . $savefolder . DS . $current_data['Shop'][$fieldkey] . ']を削除しました', 'debug');
					}
					// それ以外で、アップロードされている物を登録する
					else if(!empty($this->data['Shop'][$fieldkey . '_file']['tmp_name']))
					{

/* アニメーションGIFの対応のため、とりあえずリサイズは行わない
						// 縦幅指定が0の場合は、横幅優先指定で圧縮
						if($imagesize[1] == 0) {
							$upload_name = $this->CompFile->uploadImageCompressPrecedenceWidth(
								$fieldkey,
								$this->data['Shop'][$fieldkey . '_file']['tmp_name'],
								$savefolder,
								$imagesize[0]
							);

							// FIXME デバッグ
							$this->log('field[' . $fieldkey . ']を横幅優先圧縮します。(' . $imagesize[0] . ')', 'debug');
						} else {
							//画像アップロード
							$upload_name = $this->CompFile->uploadImageCompress(
								$fieldkey,
								$this->data['Shop'][$fieldkey . '_file']['tmp_name'],
								$savefolder,
								$imagesize[0],
								$imagesize[1]
							);

							// FIXME デバッグ
							$this->log('field[' . $fieldkey . ']を通常圧縮します。(' . $imagesize[0] . 'x' . $imagesize[1] . ')', 'debug');
						}
*/
						$upload_name = $this->CompFile->uploadImage(
							$fieldkey,
							$this->data['Shop'][$fieldkey . '_file']['tmp_name'],
							$savefolder
						);

						// FIXME デバッグ
						$this->log('field[' . $fieldkey . ']を無圧縮で保存します。', 'debug');

						if($upload_name){
							$this->data['Shop'][$fieldkey] = $upload_name;

							// FIXME デバッグ
							$this->log('field[' . $fieldkey . ']の画像をファイル名[' . $upload_name . ']で保存しました', 'debug');
						}
					}
				}
				//データ保存処理
				$this->data['Shop']['is_created'] = 1;
				$this->Shop->save($this->data);
// pr($this->data);exit;
				//小エリア追加
				$small_area_ids = $this->data['User']['small_area_id'];
				$small_area_ids = (is_array($small_area_ids)) ? implode(',', $small_area_ids) : $small_area_ids;
				$this->data['User']['small_area_id'] = $small_area_ids;
				$this->User->save($this->data);
				//お店のお知らせ追加
				$id = $this->ShopInfo->find('first', array(
					'conditions' => array('user_id' => $this->data['ShopInfo']['user_id']),
					'fields' => 'id'
				));
				$this->data['ShopInfo']['id'] = $id['ShopInfo']['id'];
				$this->ShopInfo->save($this->data);
				$this->redirect($this->device_path . '/shops/editend');
			}
		}else{
			//店舗情報取得
			$this->data = $this->Shop->findbyUserId($this->Auth->user('id'));
		}
		//業種セレクトボックス
		$this->set('m_shops_business_categories',Set::Combine($this->MShopsBusinessCategory->find('all'),'{n}.MShopsBusinessCategory.id', '{n}.MShopsBusinessCategory.name'));
		$this->set('data',$this->data);

		//小エリア用チェックボックス
		$area = $this->LargeArea->find('all', array(
			'conditions' => array('id' => $this->data['LargeArea']['id'])
		));
		$this->set('area', $area);

		//お店のお知らせテキスト
		$info = $this->ShopInfo->find('first', array(
			'conditions' => array('user_id' => $this->data['User']['id'])
		));
		$this->set('info', $info);



		//head
		$this->set('title_for_layout', $this->title_tag_common.'店舗情報管理');
		$this->set('header_one', $this->h1_tag_common.'店舗情報管理');
	}
	function i_index() {
		$this->index();
	}

	//編集完了画面
	function editend(){
		//head
		$this->set('title_for_layout', $this->title_tag_common.'店舗情報編集完了画面');
		$this->set('header_one', $this->h1_tag_common.'店舗情報編集完了画面');
	}
	function i_editend() {
		$this->editend();
	}
}
?>
