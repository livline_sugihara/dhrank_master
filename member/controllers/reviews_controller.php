<?php
class ReviewsController extends AppController {
	var $name = 'Reviews';
	var $uses = array('Review','ReviewsGoodCount','Message', 'ReviewReply');

	function beforeFilter() {
		parent::beforeFilter();

		//ログインしていない場合はログイン画面へ
		if(!$this->Auth->user()){
			$this->redirect($this->device_path . '/');
		}

	}

	function index(){
		//データ取得
		$this->paginate = array('Review' => array(
				'fields' => array(
					'LargeArea.url','MReviewersAge.name','MReviewsPlace.name',
					'Shop.*','Girl.*','Review.*','Reviewer.*','User.*',
					'(Review.score_girl_character + Review.score_girl_service + Review.score_girl_looks) / 3  as girl_avg',
					'ifnull((SELECT count(reviews.id) FROM reviews WHERE reviews.reviewer_id > 0 AND reviews.reviewer_id = Reviewer.id), 0) AS review_cnt',
				),
				'conditions' => array(
					'Review.user_id' => $this->Auth->user('id'),
					'Review.delete_flag <=' => Review::DELETE_FLAG_COMMENT_ONLY,
				),
				'group' => array('Review.id'),
				'order' => 'Review.created DESC',
				'limit' => $this->pcount['reviews_list'],
		));

		$review_reply = $this->ReviewReply->find('all',
			'*'
		);

		// 口コミのデータに、口コミ返事を紐付ける
		$paginate_list = $this->paginate($this->Review);
		foreach($paginate_list as $key => $val)
		{
			$find_data = $this->ReviewReply->find('all',
				array(
					'fields' => '*',
					'conditions' => array(
						'ReviewReply.review_id' => $val['Review']['id'],
					),
					'order' => 'ReviewReply.reply_flg ASC',
				)
			);
			$paginate_list[$key]['ReviewReply'] = $find_data;
		}

		$this->set('list', $paginate_list);
		$this->set('appointed_type',Review::$appointed_type);
		$this->set('appointed_sub',Review::$appointed_sub);

		//head
		$this->set('title_for_layout', $this->title_tag_common.'口コミ管理');
		$this->set('header_one', $this->h1_tag_common.'口コミ管理');
	}
	function i_index() {
		$this->index();
	}

/* ---- 以下はAjaxに移行したので不要
	// 削除
	function delete($param){

		//存在しない口コミの場合はエラー画面へ
		$present = $this->Review->find('count',array('conditions' => array('Review.user_id' => $this->Auth->user('id'), 'Review.id' => $param)));
		if($present == 0){
			$this->cakeError('error404');
		}

		//口コミ削除権限なしはエラー画面へ
		if($this->parent_user['User']['is_review_delete_enabled'] != 1){
			$this->cakeError('error404');
		}

		if (!empty($this->data)){
			//テーブル削除
			$this->ReviewsGoodCount->deleteAll(array('ReviewsGoodCount.review_id' => $this->data['Review']['id']));
			$this->Review->delete($this->data['Review']['id']);
			$this->redirect($this->device_path . '/reviews/deleteend');
		} else {
			$this->data = $this->Review->find('first', array(
					'fields' => array('LargeArea.url','User.id','Girl.id','Girl.name','Girl.age','Review.id','Review.comment','Shop.name','Review.created','Girl.image_1_s',
							'Review.score_girl_first_impression','Review.score_girl_word','Review.score_girl_service','Review.score_girl_sensitivity','Review.score_girl_style','Review.score_girl_voice',
							'(Review.score_girl_first_impression + Review.score_girl_word + Review.score_girl_service + Review.score_girl_sensitivity + Review.score_girl_style + Review.score_girl_voice) / 6  as girl_avg',
							'Girl.image_1_m','Review.comment_deleted','MReviewsAge.name'),
					'conditions' => array(
							'Review.id' => $param,
					),
					'group' => array('Review.id'),));
			$this->set('data',$this->data);
		}
		//head
		$this->set('title_for_layout', $this->title_tag_common.'口コミ削除');
		$this->set('header_one', $this->h1_tag_common.'口コミ削除');
	}
	function i_delete($param) {
		$this->delete($param);
	}

	//削除完了画面
	function deleteend(){
		//head
		$this->set('title_for_layout', $this->title_tag_common.'口コミ削除完了');
		$this->set('header_one', $this->h1_tag_common.'口コミ削除完了');
	}
	function i_deleteend() {
		$this->deleteend();
	}
	//口コミ内容表示・メッセージ送信画面
	function send_message($param){

		//存在しない口コミの場合はエラー画面へ
		$present = $this->Review->find('count',array('conditions' => array('Review.user_id' => $this->Auth->user('id'), 'Review.id' => $param)));
		if($present == 0){
			$this->cakeError('error404');
		}
		//start---2013/4/1 障害No.1-2-0004修正
		//口コミ削除権限なしはエラー画面へ
		//if($this->parent_user['User']['is_review_delete_enabled'] != 1){
		//	$this->cakeError('error404');
		//}
		//end---2013/4/1 障害No.1-2-0004修正

		if (!empty($this->data)){
			//メッセージ送信
			$this->Message->set($this->data);
			if($this->Message->validates()){
				$this->data['Message']['is_unread'] = 1;
				$this->Message->save($this->data);
				$this->redirect($this->device_path . '/reviews/send_message_end');
			}
		}
		//口コミデータ取得
		$this->data = $this->Review->find('first', array(
				'fields' => array('LargeArea.url','User.id','Girl.id','Girl.name','Girl.age','Review.id','Review.comment','Shop.name','Review.created','Girl.image_1_s',
						'Review.score_girl_first_impression','Review.score_girl_word','Review.score_girl_service','Review.score_girl_sensitivity','Review.score_girl_style','Review.score_girl_voice',
						'(Review.score_girl_first_impression + Review.score_girl_word + Review.score_girl_service + Review.score_girl_sensitivity + Review.score_girl_style + Review.score_girl_voice) / 6  as girl_avg',
						'Girl.image_1_m','Review.comment_deleted','MReviewsAge.name','Review.reviewer_id','Reviewer.handle','Reviewer.id'),
				'conditions' => array(
						'Review.id' => $param,
				),
				'group' => array('Review.id'),));
		$this->set('data',$this->data);

		//メッセージデータ取得
		$this->set('messages',$this->Message->find('all', array(
				'conditions' => array('Message.review_id' => $param,),
				'order' => 'Message.created DESC')));
		//head
		$this->set('title_for_layout', $this->title_tag_common.'口コミ管理');
		$this->set('header_one', $this->h1_tag_common.'口コミ管理');

	}
	function i_send_message($param) {
		$this->send_message($param);
	}

	//メッセージ送信完了画面
	function send_message_end(){
		//head
		$this->set('title_for_layout', $this->title_tag_common.'メッセージ送信完了');
		$this->set('header_one', $this->h1_tag_common.'メッセージ送信完了');
	}
	function i_send_message_end() {
		$this->send_message_end();
	}
*/
}
?>
