<?php
class TopController extends AppController {
	var $name = 'Top';
	var $uses = array('Girl', 'Shop');

	function beforeFilter(){
		parent::beforeFilter();

		// パンくずに基本情報を設定
		$this->addBreadCrumbs("店舗管理トップ" , "/");
	}


	function index() {

		$shop_datas = $this->_get_favorite_shop_data();
		$girl_datas = $this->_get_favorite_girl_data();
		$list_data = array();

		foreach($shop_datas AS $ii => $record) {
			$list_data[$record['BookmarkShop']['created']] = array(
				'record' => $record,
				'type' => 'shop',
			);
		}
		foreach($girl_datas AS $ii => $record) {
			$list_data[$record['BookmarkGirl']['created']] = array(
				'record' => $record,
				'type' => 'girl',
			);
		}

		krsort($list_data);
		$this->set('list', $list_data);

		//head
		$this->set('title_for_layout', $this->title_tag_common.'店舗管理トップ');
		$this->set('header_one', $this->h1_tag_common.'店舗管理トップ');
	}
	function i_index() {
		$this->index();
	}

	// ----------------------------------------------------------------------------------
	// Private関数
	// ----------------------------------------------------------------------------------
	// お気に入りのお店 情報取得
	function _get_favorite_shop_data() {

		$this->Shop->unbindModel(array('hasOne' => array('Review')));
		$this->Shop->bindModel(array('belongsTo' => array('BookmarkShop' => array(
			'className'=>'BookmarkShop',
			'type' => 'inner',
			'conditions' => array(
				'BookmarkShop.user_id' => $this->parent_user['User']['id'],
				'Shop.user_id = BookmarkShop.user_id',
			),
			'foreignKey' => false,))),false);

		$this->Shop->bindModel(array('belongsTo' => array('Reviewer' => array(
			'className'=>'Reviewer',
			'type' => 'inner',
			'conditions' => array(
				'Reviewer.id = BookmarkShop.reviewer_id',
			),
			'foreignKey' => false,))),false);

		$favorite_shops = $this->Shop->find('all', array(
			'fields' => array(
				'User.*','Shop.*', 'System.*', 'Reviewer.*','LargeArea.url','BookmarkShop.created',
				'(SELECT count(*) FROM girls WHERE User.id = girls.user_id) AS girl_cnt',
				'(SELECT count(*) FROM reviews WHERE User.id = reviews.user_id) AS review_cnt',
			),
			'conditions' => array(
				'Shop.user_id' => $this->parent_user['User']['id'],
			),
			//'order' =>'User.order_rank ASC',
		));
		$this->Shop->unbindModel(array('belongsTo' => array('BookmarkShop', 'Reviewer')), false);

		return $favorite_shops;
	}

	// お気に入りの女の子 情報取得
	function _get_favorite_girl_data() {

//		$this->Girl->unbindModel(array('hasOne' => array('Review')));
		$this->Girl->bindModel(array('belongsTo' => array('BookmarkGirl' => array(
			'className'=>'BookmarkGirl',
			'type' => 'inner',
			'conditions' => array(
				'Girl.id = BookmarkGirl.girl_id',
			),
			'foreignKey' => false,))),false);

		$this->Girl->bindModel(array('belongsTo' => array('Reviewer' => array(
			'className'=>'Reviewer',
			'type' => 'inner',
			'conditions' => array(
				'Reviewer.id = BookmarkGirl.reviewer_id',
			),
			'foreignKey' => false,))),false);

		$favorite_girls = $this->Girl->find('all', array(
			'fields' => array(
				'Girl.*','User.*','Shop.*', 'Reviewer.*','LargeArea.*','BookmarkGirl.created',
			),
			'conditions' => array(
				'Shop.user_id' => $this->parent_user['User']['id'],
				'Girl.is_deleted' => 0,
			),
			//'order' =>'User.order_rank ASC, Girl.show_order ASC',
		));
		$this->Girl->unbindModel(array('belongsTo' => array('BookmarkGirl', 'Reviewer')), false);

		return $favorite_girls;
	}

}
?>
