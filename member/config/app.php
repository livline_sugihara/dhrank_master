<?php
//画像論理パス
//define('APP_IMG_URL','http://img.dhrank.com/');
define('APP_IMG_URL','http://img.19800210.com/');
//店舗管理システム
//define('APP_MEMBER_URL','http://member.dhrank.com/');
define('APP_MEMBER_URL','http://member.19800210.com/');

Configure::write('change', array(
1 => '不可',
2 => '無料',
3 => '有料',
));

Configure::write('cancel', array(
1 => '不可',
2 => '無料',
3 => '有料',
));

Configure::write('receipt', array(
1 => '発行可',
2 => '発行不可',
));

Configure::write('weekday', array( "日", "月", "火", "水", "木", "金", "土" ));

Configure::write('kana', array(
	1 => array('あ','い','う','え','お','ぁ','ぃ','ぅ','ぇ','ぉ',),
	2 => array('か','き','く','け','こ','が','ぎ','ぐ','げ','ご',),
	3 => array('さ','し','す','せ','そ','ざ','じ','ず','ぜ','ぞ',),
	4 => array('た','ち','つ','て','と','だ','ぢ','づ','で','ど','っ',),
	5 => array('な','に','ぬ','ね','の',),
	6 => array('は','ひ','ふ','へ','ほ','ば','び','ぶ','べ','ぼ','ぱ','ぴ','ぷ','ぺ','ぽ',),
	7 => array('ま','み','む','め','も',),
	8 => array('や','ゆ','よ','ゃ','ゅ','ょ',),
	9 => array('ら','り','る','れ','ろ',),
	10 => array('わ','を','ん','ゎ',),
));

// 正の整数
define('VALID_POSITIVE_NUMBER', '/^[0-9]+$/');
// 半角英数記号
define('VALID_HALF_ALPHANUMERICCODE', '/^[!-~]+$/');
// 全角ひらがな
define('VALID_HIRAGANA','/^(?:\xE3\x81[\x81-\xBF]|\xE3\x82[\x80-\x93])+$/');
// 電話番号
//start---2012/4/6障害No.3-0012修正
//define('VALID_PHONE','/0\d{1,4}-\d{1,4}-\d{4}/');
define('VALID_PHONE','/0\d{1,4}-\d{1,4}-\d{3,4}/');
//end---2012/4/6障害No.3-0012修正
// メールアドレス
define('VALID_EMAIL','/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/');


//画像定義
//バナー
define('APP_IMG_BANNER_WIDTH',200);
define('APP_IMG_BANNER_HEIGHT',40);
//ランキング画像
define('APP_IMG_RANK_WIDTH',120);
define('APP_IMG_RANK_HEIGHT',160);
//女の子画像L
define('APP_IMG_GIRL_L_WIDTH',240);
define('APP_IMG_GIRL_L_HEIGHT',320);
//女の子画像M
define('APP_IMG_GIRL_M_WIDTH',120);
define('APP_IMG_GIRL_M_HEIGHT',160);
//女の子画像S
define('APP_IMG_GIRL_S_WIDTH',60);
define('APP_IMG_GIRL_S_HEIGHT',80);

//-----
// 店舗アイコン画像
define('APP_IMG_SHOP_ICON_WIDTH', 200);
define('APP_IMG_SHOP_ICON_HEIGHT', 200);
// おすすめ店舗画像
define('APP_IMG_SHOP_RECOMMEND_WIDTH', 291);
define('APP_IMG_SHOP_RECOMMEND_HEIGHT', 86);
// 店舗リスト画像(PC)
define('APP_IMG_SHOP_LIST_WIDTH', 300);
define('APP_IMG_SHOP_LIST_HEIGHT', 400);
// 店舗ヘッダー(PC)
define('APP_IMG_SHOP_HEADER_WIDTH', 1350);
define('APP_IMG_SHOP_HEADER_HEIGHT', 400);
// 店舗紹介画像
define('APP_IMG_SHOP_PROFILE_WIDTH', 350);
define('APP_IMG_SHOP_PROFILE_HEIGHT', 240);
// 店舗ヘッダー(SP)
define('APP_IMG_SHOP_HEADER_SP_WIDTH', 880);
define('APP_IMG_SHOP_HEADER_SP_HEIGHT', 460);
// 店舗リスト画像(SP)
define('APP_IMG_SHOP_LIST_SP_WIDTH', 528);
define('APP_IMG_SHOP_LIST_SP_HEIGHT', 380);


//テキスト抜粋
//トップ：口コミコメント
define('APP_CSIZE_MEMBER_TOP_REVIEWS_COMMENT',30);
define('APP_CSIZE_MEMBER_TOP_REVIEWS_COMMENT_I',30);
//口コミ一覧：口コミコメント
define('APP_CSIZE_MEMBER_REVIEWS_COMMENT',30);
define('APP_CSIZE_MEMBER_REVIEWS_COMMENT_I',30);

//パスワード変更URL送信
define('VALID_CHANGEPASSWORD_URLSEND_EMAIL','reminder@19800210.com');

Configure::write('smtpOptionChangepasswordUrlsend', array(
 		'port'=>'587',
 		'host' => '19800210.com',
 		'username'=>'reminder',
 		'password'=>'lCO4EiGy',
));

//パスワード変更完了送信
define('VALID_CHANGEPASSWORD_END_EMAIL','reminder@19800210.com');

Configure::write('smtpOptionChangepasswordEnd', array(
 		'port'=>'587',
 		'host' => '19800210.com',
 		'username'=>'reminder',
 		'password'=>'lCO4EiGy',
));

define('VALID_GIRLS_IMAGE_UPLOAD_EMAIL','imageupload@dhrank.com');


//取得件数一括管理対応
//トップページ口コミ一覧
define('APP_PCOUNT_MEMBER_TOP_REVIEWS',20);
define('APP_PCOUNT_MEMBER_TOP_REVIEWS_I',20);
//口コミ一覧画面
define('APP_PCOUNT_MEMBER_REVIEWS_LIST',20);
define('APP_PCOUNT_MEMBER_REVIEWS_LIST_I',20);
?>
