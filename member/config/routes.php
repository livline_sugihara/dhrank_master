<?php

Router::connect('/', array('controller' => 'top', 'action' => 'index'));

// ----- Ajax
// Pickupレビュー追加／解除
Router::connect('/toggle_pickup_review/:review_id/*', array('controller' => 'ajax', 'action' => 'toggle_pickup_review'), array('review_id' => '[0-9]+'));

// 口コミ削除
Router::connect('/delete_review/:review_id/*', array('controller' => 'ajax', 'action' => 'delete_review'), array('review_id' => '[0-9]+'));

// 口コミの返信
Router::connect('/reply_review/:review_id/*', array('controller' => 'ajax', 'action' => 'reply_review'), array('review_id' => '[0-9]+'));
