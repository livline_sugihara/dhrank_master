<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.app
 */
class AppModel extends Model {

	//タグチェック
	function isNoTag($data,$input_name){
		return ($data[$input_name] == strip_tags($data[$input_name]));
	}

	//画像選択有無チェック
	function isExistImage($data,$image_name){
		return (!empty($data[$image_name]['tmp_name']));
	}
	//拡張子チェック（JPEG or GIF）
	function isExtentionJpegOrGif($data,$image_name){
		if(!empty($data[$image_name]['tmp_name'])){
			return ($data[$image_name]['type'] == 'image/jpeg' || $data[$image_name]['type'] == 'image/pjpeg' || $data[$image_name]['type'] == 'image/gif');
		}else{
			return true;
		}
	}
	//拡張子チェック（JPEG）
	function isExtentionJpeg($data,$image_name){
		if(!empty($data[$image_name]['tmp_name'])){
			return ($data[$image_name]['type'] == 'image/jpeg' || $data[$image_name]['type'] == 'image/pjpeg');
		}else{
			return true;
		}
	}
	//ファイルサイズチェック
	function isNotOverFileSize($data,$image_name,$size){
		if(!empty($data[$image_name]['tmp_name'])){
			return ($data[$image_name]['size'] <= $size);
		}else{
			return true;
		}
	}

	//オーバーライド
	function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
		$parameters = compact('conditions');
		$this->recursive = $recursive;
		$count = $this->find('count', array_merge($parameters, $extra));
		if (isset($extra['group'])) {
			$count = $this->getAffectedRows();
		}
		return $count;
	}

	// 過去日だとエラー
	function isNotPastDateTime($data, $input_name)
	{
		if(!empty($data[$input_name]))
		{
			$timestamp = strtotime($data[$input_name]);

			if($timestamp < time())
			{
				return false;
			}
		}

		return true;
	}

	// おすすめしている女性が６人以上だとエラー
	function isRecommendValueCheck($data)
	{
		if ($data['recommend_check'] != 1)
			return true;

		// 激押し売れっ子６人かどうかチェック
		$sql = '';
		$sql .= ' SELECT count(*) as cnt FROM girls';
		$sql .= ' WHERE is_deleted = 0';
		$sql .= ' AND user_id = "' . $this->data['Girl']['user_id'] . '"';
		$sql .= ' AND recommend_type_id != 0';
		$recommend_count_data = $this->query($sql);
		if ($recommend_count_data[0][0]['cnt'] >= 6)
			return false;
		else
			return true;
	}
}
