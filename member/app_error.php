<?php
class AppError extends ErrorHandler {
	public function dispatchMethod($method, $messages) {
		//ユーザー情報取得
		//店舗アカウントデータ
		$this->controller->set('parent_user', $this->controller->User->findbyId($this->controller->Auth->user('id')));
		
		parent::dispatchMethod($method, $messages);
	}
}	
