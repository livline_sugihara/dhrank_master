<?php

class Passport extends AppModel {
	public $name = 'Passport';
	
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
 	);
}

?>
