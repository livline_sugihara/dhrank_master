<?php
class Coupon extends AppModel {
	public $name = 'Coupon';
	public static $targetList = array(
		1 => '新規',
		2 => '会員',
		9 => '全員',
	);
	public static $targetCaptionList = array(
		1 => '新規様のみ',
		2 => '会員様のみ',
		9 => '全員OK',
	);

	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => '',
			'conditions' => 'Coupon.user_id = User.id',
			'fields' => '',
			'order' => ''
		),
	);

	public $validate = array(
		'target'=>array(
				array('rule' => 'numeric', 'message' => '対象を選択してください'),
				array('rule' => 'notEmpty', 'message' => '対象を選択してください'),
				array('rule' => array('comparison', '>=', 1), 'message'=>'対象を選択してください'),
		),
		'condition'=>array(
				array('rule' => array('isNoTag','condition'),'message'=>'タグは入力できません。'),
				array('rule' => array('maxLength',40),'message'=>'40文字以下で入力してください。'),
		),
		'content'=>array(
				array('rule' => array('isNoTag','content'),'message'=>'タグは入力できません。'),
				array('rule' => array('maxLength',8),'message'=>'8文字以下で入力してください。'),
		),
		'expire_date'=>array(
//				array('rule' => 'datetime','message'=>'正しく日付を選択してください', 'allowEmpty' => true),
				array('rule' => array('custom', '/[0-9]{4}\-[0-9]{2}\-[0-9]{2}\s*[0-9]{2}\:[0-9]{2}\:[0-9]{2}$/i'),'message'=>'正しく日付を選択してください', 'allowEmpty' => true),
				array('rule' => 'isSettingExpire','message'=>'「期限なし」にチェックを入れない場合は有効期限を設定してください'),
				array('rule' => array('isNotPastDateTime', 'expire_date'),'message'=>'過去日の入力は出来ません'),
		),
	);

	//findメソッド実行後のオーバーライド
	public function afterFind($results)
	{
		foreach ($results as $key => $record){
			//count取得ではない
			if (isset($results[$key]['Coupon']['id'])){
				// 有効期限
				if(isset($results[$key]['Coupon']['expire_date']) && !empty($results[$key]['Coupon']['expire_date']))
				{
					list($year, $month, $day) = sscanf($results[$key]['Coupon']['expire_date'], "%d-%d-%d 23:59:59");

					$results[$key]['Coupon']['expire_date_year'] = $year;
					$results[$key]['Coupon']['expire_date_month'] = $month;
					$results[$key]['Coupon']['expire_date_day'] = $day;
				}
			}
		}

		return $results;
	}

	/** Validate前にコールバックされる関数 */
	public function beforeValidate($options = array())
	{
		// 有効期限なしの場合
		if(isset($this->data['Coupon']['expire_unlimited']) && $this->data['Coupon']['expire_unlimited'] == 1) {
			$this->data['Coupon']['expire_date'] = null;
		} else {
			// 有効期限の変換
			if((isset($this->data['Coupon']['expire_date_year']) && !empty($this->data['Coupon']['expire_date_year'])) &&
				(isset($this->data['Coupon']['expire_date_month']) && !empty($this->data['Coupon']['expire_date_month'])) &&
				(isset($this->data['Coupon']['expire_date_day']) && !empty($this->data['Coupon']['expire_date_day'])))
			{
				$this->data['Coupon']['expire_date'] = $this->data['Coupon']['expire_date_year'] . '-' . $this->data['Coupon']['expire_date_month'] . '-' . $this->data['Coupon']['expire_date_day'] . ' 23:59:59';
			}
		}

		return true;
	}

	//有効期限チェック
	function isSettingExpire($data)
	{
		if(empty($this->data['Coupon']['expire_unlimited']) || $this->data['Coupon']['expire_unlimited'] == 0) {
			return !empty($this->data['Coupon']['expire_date']);
		} else {
			return true;
		}
	}
}
