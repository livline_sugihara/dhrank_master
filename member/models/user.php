<?php

class User extends AppModel {
	public $name = 'User';

	var $belongsTo = array(
		'LargeArea' => array(
			'className' => 'LargeArea',
			'foreignKey' => 'large_area_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Shop' => array(
			'className' => 'Shop',
			'foreignKey' => '',
			'conditions' => 'Shop.user_id = User.id',
			'fields' => '',
			'order' => ''
		),
	);


	public $validate = array(
		'new_password'=>array(
			array('rule' => 'equalToPasswordNew','message'=>'新しいパスワードが一致しません。'),
			array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			array('rule' => 'notEmpty','message'=>'新しいパスワードを記入してください。'),
		),
		'new_password_confirm'=>array(
			array('rule' => array('custom',VALID_HALF_ALPHANUMERICCODE),'message'=>'半角英数記号で入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			array('rule' => array('minLength',4),'message'=>'4文字以上で入力してください。'),
			array('rule' => 'notEmpty','message'=>'新しいパスワード(確認)を記入してください。')
		)
	);

	function equalToPasswordNew($data){
		return ($this->data['User']['new_password'] == $this->data['User']['new_password_confirm']);
	}
}

?>
