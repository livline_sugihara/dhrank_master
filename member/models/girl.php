<?php
class Girl extends AppModel
{
	/** 激押し売れっ子 OFF */
	const RECOMMEND_TYPE_ID_NONE = 0;

	/** 並び順(新規) */
	const SHOW_ORDER_NEW = 10000;

	var $name = 'Girl';


	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => '',
			'conditions' => array('User.id = Girl.user_id'),
			'fields' => '',
			'order' => ''
		),
		'LargeArea' => array(
			'className' => 'LargeArea',
			'foreignKey' => '',
			'conditions' => array('User.large_area_id = LargeArea.id'),
			'fields' => '',
			'order' => ''
		),
		'Shop' => array(
			'className' => 'Shop',
			'foreignKey' => '',
			'conditions' => array('Shop.user_id = Girl.user_id'),
			'fields' => '',
			'order' => ''
		),
		'MGirlsCup' => array(
			'className' => 'MGirlsCup',
			'foreignKey' => '',
			'conditions' => array('MGirlsCup.id = Girl.girls_cup_id'),
			'fields' => '',
			'order' => ''
		),
		'MShopsBusinessCategory' => array(
			'className' => 'MShopsBusinessCategory',
			'foreignKey' => '',
			'conditions' => array('Shop.shops_business_category_id = MShopsBusinessCategory.id'),
			'fields' => '',
			'order' => ''
		),
		'MGirlsRecommendType' => array(
			'className' => 'MGirlsRecommendType',
			'foreignKey' => '',
			'conditions' => array('Girl.recommend_type_id = MGirlsRecommendType.id'),
			'fields' => '',
			'order' => ''
		),
	);

	var $validate = array(
		'name'=>array(
			array('rule' => array('isNoTag','name'),'message'=>'タグは入力できません。'),
			array('rule' => 'notEmpty','message'=>'名前を記入してください。'),
			array('rule' => array('minLength',1),'message'=>'1文字以上で入力してください。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。')
		),
/*
		'age' => array(
			array('rule' => 'notEmpty','message'=>'選択してください。'),
		),
		'body_height' => array(
				array('rule' => 'notEmpty','message'=>'選択してください。'),
		),
		'girls_cup_id' => array(
				array('rule' => 'notEmpty','message'=>'選択してください。'),
		),
		'bust' => array(
				array('rule' => 'notEmpty','message'=>'選択してください。'),
		),
		'waist' => array(
				array('rule' => 'notEmpty','message'=>'選択してください。'),
		),
		'hip' => array(
				array('rule' => 'notEmpty','message'=>'選択してください。'),
		),
*/
		// おすすめ内容
		// 6人以上登録されていないかどうか？
		'recommend_check' => array(
			array('rule' => array('isRecommendValueCheck', 'user_id'), 'message'=>'現在６人登録されております。このチェックを外す、または別の女の子のチェックを外してください。'),

		),

		'recommend_type_id' => array(
			array('rule' => 'notEmpty','message'=>'おすすめ内容を選択してください'),
		),

		//start---2013/3/5 障害No.2-0008修正
/*
		'image2_1'=>array(
		 		array('rule' => array('isNotOverFileSize','image_1',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
		 		array('rule' => array('isExtentionJpegOrGif','banner2'),'message'=>'Jpeg画像またはGif画像を選択してください。')
		 ),
		'image2_2'=>array(
				array('rule' => array('isNotOverFileSize','image_2',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
				array('rule' => array('isExtentionJpegOrGif','banner2'),'message'=>'Jpeg画像またはGif画像を選択してください。')
		),
		'image2_3'=>array(
				array('rule' => array('isNotOverFileSize','image_3',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
				array('rule' => array('isExtentionJpegOrGif','banner2'),'message'=>'Jpeg画像またはGif画像を選択してください。')
		),
		'image2_4'=>array(
				array('rule' => array('isNotOverFileSize','image_4',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
				array('rule' => array('isExtentionJpegOrGif','banner2'),'message'=>'Jpeg画像またはGif画像を選択してください。')
		),
*/
		'image2_1'=>array(
				array('rule' => array('isNotOverFileSize','image2_1',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
				array('rule' => array('isExtentionJpegOrGif','image2_1'),'message'=>'Jpeg画像またはGif画像を選択してください。')
		),
		'image2_2'=>array(
				array('rule' => array('isNotOverFileSize','image2_2',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
				array('rule' => array('isExtentionJpegOrGif','image2_2'),'message'=>'Jpeg画像またはGif画像を選択してください。')
		),
		'image2_3'=>array(
				array('rule' => array('isNotOverFileSize','image2_3',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
				array('rule' => array('isExtentionJpegOrGif','image2_3'),'message'=>'Jpeg画像またはGif画像を選択してください。')
		),
		'image2_4'=>array(
				array('rule' => array('isNotOverFileSize','image2_4',3145728),'message'=>'ファイルサイズオーバーです。3MB以下にしてください。'),
				array('rule' => array('isExtentionJpegOrGif','image2_4'),'message'=>'Jpeg画像またはGif画像を選択してください。')
		),
		//end---2013/3/5 障害No.2-0008修正
	);

	//findメソッド実行後のオーバーライド
	public function afterFind($results){
		foreach ($results as $key => $record){
			//count取得ではない
			if (isset($results[$key]['Girl']['id'])){
				//年齢
				if (!isset($results[$key]['Girl']['age'])){
					$results[$key]['Girl']['age'] = '－';
				}
				//身長
				if (!isset($results[$key]['Girl']['body_height'])){
					$results[$key]['Girl']['body_height'] = '－';
				}
				//カップ数
				if (!isset($results[$key]['MGirlsCup']['name'])){
					$results[$key]['MGirlsCup']['name'] = '－';
				}
				if (!isset($results[$key]['MGirlsCup']['name2'])){
					$results[$key]['MGirlsCup']['name2'] = '－';
				}
				//バスト
				if (!isset($results[$key]['Girl']['bust'])){
					$results[$key]['Girl']['bust'] = '－';
				}
				//ウエスト
				if (!isset($results[$key]['Girl']['waist'])){
					$results[$key]['Girl']['waist'] = '－';
				}
				//ヒップ
				if (!isset($results[$key]['Girl']['hip'])){
					$results[$key]['Girl']['hip'] = '－';
				}
				// 激押し売れっ子
				if (isset($results[$key]['Girl']['recommend_type_id']) && $results[$key]['Girl']['recommend_type_id'] > 0){
					$results[$key]['Girl']['recommend_check'] = '1';
				}
			}
		}
		return $results;
	}

	/** Validate前にコールバックされる関数 */
	public function beforeValidate($options = array())
	{
		if(isset($this->data['Girl']['recommend_type_id']))
		{
			// 激押し売れっ子の登録チェックが入っていない場合の処理
			if(!isset($this->data['Girl']['recommend_check']) || empty($this->data['Girl']['recommend_check']))
			{
				$this->data['Girl']['recommend_type_id'] = self::RECOMMEND_TYPE_ID_NONE;
			}
		}

		return true;
	}

}
?>
