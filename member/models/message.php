<?php
class Message extends AppModel {
	var $name = 'Message';

	public $validate = array(
			'title'=>array(
					array('rule' => array('isNoTag','title'),'message'=>'タグは入力できません。'),
					array('rule' => 'notEmpty','message'=>'タイトルを入力してください。'),
					array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			),
			'comment'=>array(
					array('rule' => array('isNoTag','comment'),'message'=>'タグは入力できません。'),
					array('rule' => 'notEmpty','message'=>'メッセージを入力してください。'),
					array('rule' => array('maxLength',1024),'message'=>'1024文字以下で入力してください。')
			),
	);

}
?>