<?php
class System extends AppModel {
	public $name = 'System';

	var $belongsTo = array(
			'User' => array(
					'className' => 'User',
					'foreignKey' => '',
					'conditions' => 'System.user_id = User.id',
					'fields' => '',
					'order' => ''
			),
	);

	public $validate = array(
			'open_time'=>array(
					array('rule' => 'notEmpty', 'message'=>'営業時間(開始)を選択してください。'),
			),
			'close_time'=>array(
					array('rule' => 'notEmpty', 'message'=>'営業時間(終了)を選択してください。'),
			),
			'price_cost'=>array(
					array('rule' => 'numeric','message'=>'数値を入力してください。'),
			),
			'credit_unavailable'=>array(
					array('rule' => 'notCreditDoublication','message'=>'利用不可と各カードブランドは同時にチェック出来ません。'),
					array('rule' => 'notEmptyForCredit','message'=>'選択してください。'),
			),
			'credit_other_comment'=>array(
					array('rule' => array('isNoTag','credit_other_comment'),'message'=>'タグは入力できません。'),
					array('rule' => 'notEmptyForCreditOtherComment','message'=>'その他チェック時は必須です。'),
					array('rule' => array('maxLength',200),'message'=>'半角200文字/全角100文字以下で入力してください。'),
			),
			'comment1_title'=>array(
					array('rule' => array('isNoTag','comment1_title'),'message'=>'タグは入力できません。'),
					array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			),
			'comment1_content'=>array(
					array('rule' => array('isNoTag','comment1_content'),'message'=>'タグは入力できません。'),
					array('rule' => array('maxLength',1024),'message'=>'1024文字以下で入力してください。'),
			),
			'comment2_title'=>array(
					array('rule' => array('isNoTag','comment2_title'),'message'=>'タグは入力できません。'),
					array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			),
			'comment2_content'=>array(
					array('rule' => array('isNoTag','comment2_content'),'message'=>'タグは入力できません。'),
					array('rule' => array('maxLength',1024),'message'=>'1024文字以下で入力してください。'),
			),
			'comment3_title'=>array(
					array('rule' => array('isNoTag','comment3_title'),'message'=>'タグは入力できません。'),
					array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。'),
			),
			'comment3_content'=>array(
					array('rule' => array('isNoTag','comment3_content'),'message'=>'タグは入力できません。'),
					array('rule' => array('maxLength',1024),'message'=>'1024文字以下で入力してください。'),
			),

	);

	/** Validate前にコールバックされる関数 */
	public function beforeValidate($options = array())
	{
		// 営業時間の変換
		if((isset($this->data['System']['open_time_hour']) && !empty($this->data['System']['open_time_hour'])) &&
			(isset($this->data['System']['open_time_minute']) && !empty($this->data['System']['open_time_minute']))) {
			$this->data['System']['open_time'] = $this->data['System']['open_time_hour'] . ':' . $this->data['System']['open_time_minute'] . ':00';
		}
		if((isset($this->data['System']['close_time_hour']) && !empty($this->data['System']['close_time_hour'])) &&
			(isset($this->data['System']['close_time_minute']) && !empty($this->data['System']['close_time_minute']))) {
			$this->data['System']['close_time'] = $this->data['System']['close_time_hour'] . ':' . $this->data['System']['close_time_minute'] . ':00';
		}

		return true;
	}


	//クレジットカード重複チェック
	function notCreditDoublication($data){
		if(!empty($this->data['System']['credit_unavailable'])){
			return (empty($this->data['System']['credit_visa']) && empty($this->data['System']['credit_master']) &&
					empty($this->data['System']['credit_jcb']) && empty($this->data['System']['credit_americanexpress']) &&
					empty($this->data['System']['credit_diners']) && empty($this->data['System']['credit_other']));
		} else {
			return true;
		}
	}
	//クレジットカード必須チェック
	function notEmptyForCredit($data){
		return !(empty($this->data['System']['credit_visa']) && empty($this->data['System']['credit_master']) &&
				empty($this->data['System']['credit_jcb']) && empty($this->data['System']['credit_americanexpress']) &&
				empty($this->data['System']['credit_diners']) &&
				empty($this->data['System']['credit_unavailable']) && empty($this->data['System']['credit_other']));
	}
	//クレジットカードその他テキスト必須チェック
	function notEmptyForCreditOtherComment($data){
		if(!empty($this->data['System']['credit_other'])){
			return !empty($this->data['System']['credit_other_comment']);
		}else{
			return true;
		}
	}
}
?>
