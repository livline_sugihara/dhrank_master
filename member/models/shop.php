<?php

class Shop extends AppModel {
	public $name = 'Shop';
	public static $imageFieldsKey = array(
		'image_icon' => array(APP_IMG_SHOP_ICON_WIDTH, APP_IMG_SHOP_ICON_HEIGHT),
		'image_recommend' => array(APP_IMG_SHOP_RECOMMEND_WIDTH, APP_IMG_SHOP_RECOMMEND_HEIGHT),
		'image_list' => array(APP_IMG_SHOP_LIST_WIDTH, APP_IMG_SHOP_LIST_HEIGHT),
		'image_header' => array(APP_IMG_SHOP_HEADER_WIDTH, APP_IMG_SHOP_HEADER_HEIGHT),
		'image_profile' => array(APP_IMG_SHOP_PROFILE_WIDTH, APP_IMG_SHOP_PROFILE_HEIGHT),
		'image_header_sp' => array(APP_IMG_SHOP_HEADER_SP_WIDTH, APP_IMG_SHOP_HEADER_SP_HEIGHT),
		'image_list_sp' => array(APP_IMG_SHOP_LIST_SP_WIDTH, APP_IMG_SHOP_LIST_SP_HEIGHT)
	);

/*
	var $hasOne = array(
		'UserAuthority' => array(
		'className' => 'UserAuthoritiy',
		'foreignKey' => '',
		'conditions' => 'Shop.user_id = UserAuthority.id',
		'fields' => '',
		'order' => ''
		)
	);
*/
	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => '',
			'conditions' => 'Shop.user_id = User.id',
			'fields' => '',
			'order' => ''
		),
		'System' => array(
			'className' => 'System',
			'foreignKey' => '',
			'conditions' => array('System.user_id = Shop.user_id'),
			'fields' => '',
			'order' => ''
		),
		'LargeArea' => array(
			'className' => 'LargeArea',
			'foreignKey' => '',
			'conditions' => array('User.large_area_id = LargeArea.id'),
			'fields' => '',
			'order' => ''
		),
		'MShopsBusinessCategory' => array(
			'className' => 'MShopsBusinessCategory',
			'foreignKey' => '',
			'conditions' => array('Shop.shops_business_category_id = MShopsBusinessCategory.id'),
			'fields' => '',
			'order' => ''
		),
	);

	// var $hasMany = array(
	// 	'SmallArea' => array(
	// 		'className' => 'SmallArea',
	// 		'conditions' => array('Shop.LargeArea.id' => 'SmallArea.large_area_id')
	// 	)
	// );

	public $validate = array(
		'shops_business_category_id'=>array(
			array('rule' => 'notEmpty','message'=>'選択してください。')
		),
		'url_pc'=>array(
//			array('rule' => 'url','allowEmpty'=>true,'message'=>'URLを入力してください。'),
			array('rule' => array('isNoTag','url_pc'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。')
		),
		'url_mobile'=>array(
//			array('rule' => 'url','allowEmpty'=>true,'message'=>'URLを入力してください。'),
			array('rule' => array('isNoTag','url_mobile'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。')
		),
		'url_smartphone'=>array(
//			array('rule' => 'url','allowEmpty'=>true,'message'=>'URLを入力してください。'),
			array('rule' => array('isNoTag','url_smartphone'),'message'=>'タグは入力できません。'),
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。')
		),
		'about_title'=>array(
			array('rule' => array('maxLength',255),'message'=>'255文字以下で入力してください。')
		),
	);

	//ランキング画像バリデート
    function imageSizeValidate($data) {

		foreach(array_keys(self::$imageFieldsKey) AS $field){
			if(!empty($data[$field]['tmp_name'])){
				if($this->isNotOverFileSize($data, $field, 3145728) == false){
					$this->invalidate($field, 'ファイルサイズオーバーです。3MB以下にしてください。');
				}
				if($this->isExtentionJpegOrGif($data, $field) == false){
					$this->invalidate($field, 'Jpeg/Gif画像を選択してください。');
				}
			}
		}
    }

}

?>
