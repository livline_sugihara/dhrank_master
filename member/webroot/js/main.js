/*
======== table of content. =================================

Description: fadein/out button image when mouse overed.
Update:  2010/07/1-
Author:  Japan Electronic Industrial Arts Co.Ltd.
         http://www.jeia.co.jp/
License: licensed under the MIT (MIT-LICENSE.txt) license.
Using:   using jQuery
         http://jquery.com/
         using DD_belatedPNG for IE6-8
         http://www.dillerdesign.com/experiment/DD_belatedPNG/

============================================================
*/

new function() {

	var fadeInTime = 0;	// msec
	var fadeOutTime = 400;	// msec
	var offClass = 'off';
	var onClass = 'on';

	if ( typeof jQuery == 'undefined' ) {
		return;
	}

	jQuery(document).ready( function() {
		init();
	});

	/**
	 * initialize
	 */
	function init() {

		jQuery( 'a img' ).each( function() {

			var src = jQuery(this).attr( 'src' );
			var fadePatern = new RegExp( /.*_off\.[^.]+/ );
			var pngPatern = new RegExp( /.*\.png$/ );
			var onImage;

			if ( src.match( fadePatern ) ) {
				onImage = jQuery(this).clone();
				onImage.
					attr( 'src', src.replace( '_off.', '_on.' ) ).
					addClass( onClass ).
					fadeTo( 0.1, 0 ).
					css({
						'position': 'absolute',
						'left': '0px',
						'top': '0px',
					});

				jQuery(this).
					addClass( offClass ).
					css({
						'position': 'absolute',
						'left': '0px',
						'top': '0px'
					}).
					parent().
						append( onImage ).
						mouseover( onMouseOver ).
						mouseout( onMouseOut ).
						css({
							'display': 'block',
							'position': 'relative'
						}).
						width( jQuery(this).width() ).
						height( jQuery(this).height() );

				if ( typeof( DD_belatedPNG ) != 'undefined' ) {
					if ( src.match( pngPatern ) ) {
						DD_belatedPNG.fixPng( this );
						DD_belatedPNG.fixPng( onImage.get(0) );
					}
				}
			}
		});
	}


	/**
	 * mouseover event( fadein )
	 */
	function onMouseOver( e ) {

		var src = jQuery(this).children( 'img.' + offClass ).attr( 'src' );
		var pngPatern = new RegExp( /.*\.png$/ );

		jQuery(this).unbind( 'mouseover', onMouseOver );

		if ( src.match( pngPatern ) ) {
			jQuery(this).
				children( 'img.' + offClass ).
				end().
				children( 'img.' + onClass ).
					fadeTo( fadeInTime, 1, function(){
						jQuery(this).parent().mouseover( onMouseOver );
					});
		}
		else {
			jQuery(this).
				children( 'img.' + onClass ).
					fadeTo( fadeInTime, 1, function(){
						jQuery(this).parent().mouseover( onMouseOver );
					});
		}
	}

	/**
	 * mouseout event( fadeout )
	 */
	function onMouseOut( e ) {

		var src = jQuery(this).children( 'img.' + offClass ).attr( 'src' );
		var pngPatern = new RegExp( /.*\.png$/ );

		if ( src.match( pngPatern ) ) {
			jQuery(this).
				children( 'img.' + offClass ).
					fadeTo( fadeOutTime, 1 ).
				end().
				children( 'img.' + onClass ).
					fadeTo( fadeOutTime, 0 );
		}
		else {
			jQuery(this).
				children( 'img.' + onClass ).
					fadeTo( fadeOutTime, 0 );
		}
	}
}


$(function() {
    var topBtn = $('.backtotop');

    //500の数字を大きくするとスクロール速度が遅くなる
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});


$(document).ready(
  function(){
    $("a img,.related-case div,.report-list,.sub-content,.sub-owner-content,input[type=image]").hover(function(){
       $(this).fadeTo("fast", 0.6); // マウスオーバー時にmormal速度で、透明度を60%にする
    },function(){
       $(this).fadeTo("fast", 1.0); // マウスアウト時にmormal速度で、透明度を100%に戻す
    });





  });



// チェックボックス選択で要素が半透明
$('.girl_check').click(function(){
		if(this.checked){
      $(this).parent().parent().css('opacity', '0.5');
		}	else {
      $(this).parent().parent().css('opacity', '1');
		};
});

// type="file"の代替要素作成
$(function(){
			$('.test1').after('<input type="button" value="ファイル選択" class="test2">');
});

$(function(){
			$('.test2').after('<input class="test3" readonly="readonly" type="text" value="">');
});

$(function() {
  $('.test2').click(function() {
    $(this).prev('.test1').trigger('click');
  });
});

$('.test1').change(function() {
    $(this).siblings('.test3').val($(this).val())
});
