<?php
App::import('Vendor', 'pear_ini', array('file' => 'pear_ini.php'));
require_once('Mail' . DS . 'mimeDecode.php');

App::import('Component', 'CompFile');
class ImageuploadShell extends Shell {

	var $uses = array('Girl');

	var $girl_id = 0;
	var $image_no = 0;
	//start---2013/4/25 障害No.4-0001修正
	var $user_id = 0;
	//end---2013/4/25 障害No.4-0001修正

	function main() {

		$this->CompFile = new CompFileComponent($this);
		$mail = "";
		$from = array();
		$headers = array();
		$subject = "";

		// 標準入力から受け取ったメールを取得
		$stdin=$this->Dispatch->stdin;
		while( !feof($stdin) ){
			$mail .= fgets($stdin,4096);
		}

		// デコード方法の指定
		$params['include_bodies'] = true;
		$params['decode_bodies']  = true;
		$params['decode_headers'] = true;
		// デコード処理
		$decoder = new Mail_mimeDecode($mail);
		$decoded = $decoder->decode($params);

		//ヘッダ情報取得
		$headers = $decoded->headers;
		//タイトル取得
		$subject = mb_convert_encoding($headers['subject'], 'UTF-8','ISO-2022-JP,UTF-8');
		$subject_array = explode('_',$subject);

		$this->log('タイトル取得までOK', LOG_DEBUG);
		//配列数が2でなければ終了
		if(count($subject_array) != 2){
			$this->log('配列数が2でない', LOG_DEBUG);
			return;
		}
		//女の子IDをセット
		$this->girl_id = $subject_array[0];
		//画像番号をセット
		$this->image_no = $subject_array[1];

		//女の子情報取得
		$from_text = mb_convert_encoding($headers['from'], 'UTF-8','ISO-2022-JP,UTF-8');
		ereg("[0-9a-zA-Z_\.\-]+@[0-9a-zA-Z_\.\-]+",$from_text,$from);
		$from = $from[0];
		//携帯メールアドレスが一致している女の子を取得
		$this->Girl->bindModel(array('hasOne' => array('Shop' => array(
				'className'=>'Shop',
				'conditions' => array('Shop.user_id = Girl.user_id'),
				'foreignKey' => false))));
		$girl = $this->Girl->find('first', array(
				'conditions' => array(
						'Shop.girls_mobile_email' => $from,
						'Girl.id' => $this->girl_id)));
		//一致なしは終了
		if(empty($girl)){
			$this->log('メールアドレス：' . $from, LOG_DEBUG);
			$this->log('女の子ID：' . $this->girl_id, LOG_DEBUG);
			$this->log('女の子情報一致無し', LOG_DEBUG);
			return;
		}
		$this->log('女の子情報一致あり', LOG_DEBUG);

		//フォルダ作成
		//start---2013/4/25 障害No.4-0001修正
		//$this->CompFile->makefolder('girl' . DS . $this->girl_id);
		$this->user_id = $girl['Girl']['user_id'];
		$this->CompFile->makefolder('girl' . DS . $this->user_id);
		$this->CompFile->makefolder('girl' . DS . $this->user_id . $this->girl_id);
		//end---2013/4/25 障害No.4-0001修正

		// 添付ファイルの有無で処理を分岐
		if ( !empty($decoded->parts) ) {

			// 再帰的データ取得処理
			$this->pullMailDataRecursive($decoded);
			//start---2013/3/8 障害No.1-3-0003修正
		}else{
			$this->log('multipartでない', LOG_DEBUG);
			//データ形式の情報が取れる
			if(!empty($decoded->ctype_secondary)){
				// 実際のデータ取得処理
				$this->pullMailData($decoded);
			}
			//end---2013/3/8 障害No.1-3-0003修正
		}
	}

	// 再帰的データ取得処理
	function pullMailDataRecursive($decoded){
		$this->log('再帰的データ取得処理', LOG_DEBUG);
		//　parts配列数分ループ
		for($idx = 0; $idx < count($decoded->parts); $idx++) {
			if ( !empty($decoded->parts[$idx]->parts) ) {
				$this->pullMailDataRecursive($decoded->parts[$idx]);
			}else{
				$this->pullMailData($decoded->parts[$idx]);
			}
		}
	}


	// 実際のデータ取得処理
	function pullMailData($parts){
		$this->log('実際のデータ取得処理', LOG_DEBUG);
		$parts->ctype_secondary = strtolower($parts->ctype_secondary);
		if($parts->ctype_secondary == "plain"){
			//テキストデータ
		}elseif($parts->ctype_secondary == "html"){
			//HTMLデータ
		}elseif($parts->ctype_secondary == "jpeg" || $parts->ctype_secondary == "gif"){
			//画像
			$this->log('実際のデータ取得処理：画像', LOG_DEBUG);
			$file = $parts->body;
			if(!empty($parts->headers['content-id'])){
				//デコメ画像
			}elseif($parts->ctype_secondary == "jpeg" || $parts->ctype_secondary == "gif"){
				$this->log('実際のデータ取得処理：jpeg,gif', LOG_DEBUG);
				if($parts->ctype_secondary == "jpeg"){
					//一時ファイル名を作成
					//start---2013/4/25 障害No.4-0001修正
					//$image_filename = APP_IMG_PATH . 'girl' . DS . $this->girl_id . DS . $this->image_no . '_tmp.jpg';
					$image_filename = APP_IMG_PATH . 'girl' . DS . $this->user_id . DS . $this->girl_id . DS . $this->image_no . '_tmp.jpg';
					//end---2013/4/25 障害No.4-0001修正
				}else{
					//一時ファイル名を作成
					//start---2013/4/25 障害No.4-0001修正
					//$image_filename = APP_IMG_PATH . 'girl' . DS . $this->girl_id . DS . $this->image_no . '_tmp.gif';
					$image_filename = APP_IMG_PATH . 'girl' . DS . $this->user_id . DS . $this->girl_id . DS . $this->image_no . '_tmp.gif';
					//end---2013/4/25 障害No.4-0001修正
				}
				//jpgを一時ファイルとして保存
				$fp = fopen($image_filename,"w");
				fputs($fp,$file);
				fclose($fp);
				chmod($image_filename, 0777);

				if(filesize($image_filename) <= 3145728 ){
					$girl = array();
					//画像アップロード(1.ファイル名(extなし),2.tmp_name,3.path)
					$upload_name = $this->CompFile->uploadImageCompressLMS((string)$this->image_no,
							$image_filename,
							//start---2013/4/25 障害No.4-0001修正
							//'girl' . DS . $this->girl_id,
							'girl' . DS . $this->user_id . DS . $this->girl_id,
							//end---2013/4/25 障害No.4-0001修正
							APP_IMG_GIRL_L_WIDTH,APP_IMG_GIRL_L_HEIGHT,
							APP_IMG_GIRL_M_WIDTH,APP_IMG_GIRL_M_HEIGHT,
							APP_IMG_GIRL_S_WIDTH,APP_IMG_GIRL_S_HEIGHT
					);
					if(!empty($upload_name[0]) && !empty($upload_name[1]) && !empty($upload_name[2])){
						$girl['Girl']['image_' . (string)($this->image_no) . '_l'] = $upload_name[0];
						$girl['Girl']['image_' . (string)($this->image_no) . '_m'] = $upload_name[1];
						$girl['Girl']['image_' . (string)($this->image_no) . '_s'] = $upload_name[2];
					}
					$girl['Girl']['id'] = $this->girl_id;
					$this->Girl->save($girl);
				}

				//start---2013/4/25 障害No.4-0001修正
				//$this->CompFile->deleteFile('girl' . DS . $this->girl_id . DS . $this->image_no . '_tmp.jpg');
				$this->CompFile->deleteFile('girl' . DS . $this->user_id . DS . $this->girl_id . DS . $this->image_no . '_tmp.jpg');
				//end---2013/4/25 障害No.4-0001修正
				return;
			}else{
				return;
			}
		}

	}

}
?>
